<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestor documental

function estadisticasGestorDocumental(){
	$res=array();

	conexionBD();
	
	$datos=consultaBD("SELECT COUNT(codigo) AS total FROM espacios_trabajo;",false,true);
	$res['espaciosTrabajo']=$datos['total'];

	$datos=consultaBD("SELECT COUNT(codigo) AS total FROM documentos;",false,true);
	$res['documentos']=$datos['total'];
	
	cierraBD();

	return $res;
}

function operacionesEspaciosTrabajo(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('espacios_trabajo');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('espacios_trabajo');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('espacios_trabajo');
	}

	mensajeResultado('nombre',$res,'Categoría');
	mensajeResultado('elimina',$res,'Categoría', true);
}



function gestionEspacioTrabajo(){
	operacionesEspaciosTrabajo();

	abreVentanaGestion('Gestión de carpetas','index.php','','icon-edit','margenAb');
	$datos=compruebaDatos('espacios_trabajo');

	campoTexto('nombre','Nombre',$datos);
	campoFecha('fecha','Fecha de creación',$datos);
	areaTexto('descripcion','Descripción',$datos);

	campoOculto($datos,'codigoUsuario',$_SESSION['codigoU']);
	

	cierraVentanaGestion('index.php',true);
}

function imprimeEspaciosTrabajo(){
	global $_CONFIG;

	$consulta=consultaBD("SELECT espaciosTrabajo.codigo, espaciosTrabajo.nombre, espaciosTrabajo.fecha, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS propietario, COUNT(documentos_pertenecen_espaciosTrabajo.codigoEspacioTrabajo) AS numDocumentos, MAX(fechaModificacion) AS fechaModificacion FROM ((espacios_trabajo AS espaciosTrabajo LEFT JOIN usuarios ON espaciosTrabajo.codigoUsuario=usuarios.codigo) LEFT JOIN documentos_pertenecen_espacios_trabajo AS documentos_pertenecen_espaciosTrabajo ON documentos_pertenecen_espaciosTrabajo.codigoEspacioTrabajo=espaciosTrabajo.codigo) LEFT JOIN documentos ON documentos_pertenecen_espaciosTrabajo.codigoDocumento=documentos.codigo GROUP BY espaciosTrabajo.codigo",true);
	while($datos=mysql_fetch_assoc($consulta)){
		$fecha='-';
		if($datos['fechaModificacion']!=NULL){
			$fecha=formateaFechaWeb($datos['fechaModificacion']);
		}

		echo "
			<tr>
				<td><a href='".$_CONFIG['raiz']."categorias/?codigoEspacioTrabajo=".$datos['codigo']."'><i class='icon-folder-o'></i> ".$datos['nombre']."</a></td>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['propietario']."</td>
				<td>".$datos['numDocumentos']."</td>
				<td>".$fecha."</td>
				<td class='centro'>
					<div class='btn-group'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."documentos-internos/gestion.php?codigo=".$datos['codigo']."'><i class='icon-edit'></i> Editar</i></a></li>
						    <li class='divider'></li>
						    <li><a href='".$_CONFIG['raiz']."categorias/?codigoEspacioTrabajo=".$datos['codigo']."'><i class='icon-search-plus'></i> Ver documentos</i></a></li>
						</ul>
					</div>
				</a>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
}

function imprimeDocumentos(){
	global $_CONFIG;
	$where='';
	if(isset($_POST['codigoEtiqueta'])){
		if($_POST['tipoFiltro'] == 'AND'){
			$where = "WHERE codigoEtiqueta = ".$_POST['codigoEtiqueta'][0];
		} else {
			$first = true;
			foreach ($_POST['codigoEtiqueta'] as $codigo) {
				if($first){
					$where = "WHERE codigoEtiqueta = ".$codigo;
					$first = false;
				} else {
					$where .= " OR codigoEtiqueta = ".$codigo;
				}
			}
		}
	}


	$res = "<div class='span12 documentos'>        
            <div class='widget widget-table action-table'>
              <div class='widget-header'> <i class='icon-list'></i>
                <h3>Documentos registrados</h3>
              </div>
              <!-- /widget-header -->
              <div class='widget-content'>
                <table class='table table-striped table-bordered'>
                  <thead>
                    <tr>
                      <th> Nombre </th>
                      <th> Fecha de creación </th>
                      <th> Autor </th>
                      <th> Última modificación </th>
                      <th> Etiquetas </th>
                      <th class='centro'></th>
                    </tr>
                  </thead>
                  <tbody>";
	$extesiones=array('zip, rar, 7z'=>'icon-file-archive-o','mp3, 3gp, midi, aac, wav, wma'=>'icon-file-audio-o','php, html, css, js, java, c, cpp, py, xml, xhtml, htm'=>'icon-file-code-o','xls, xlsx, ods, ots'=>'icon-file-excel-o','png, jpg, jpeg, gif, bmp, psd'=>'icon-file-image-o','mp4, mkv, avi, wmm, flv, mov'=>'icon-file-movie-o','pdf'=>'icon-file-pdf-o','odp, ppt, pptx'=>'icon-file-powerpoint-o','doc, docx, odt, ott'=>'icon-file-word-o');

	conexionBD();
	$consulta=consultaBD("SELECT DISTINCT documentos.codigo, documentos.nombre, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS autor, fichero, fecha, fechaModificacion FROM (documentos INNER JOIN usuarios ON documentos.codigoUsuario=usuarios.codigo) INNER JOIN etiquetas_documentos ON documentos.codigo=etiquetas_documentos.codigoDocumento ".$where);
	while($datos=mysql_fetch_assoc($consulta)){
		$icono=obtieneIconoDocumento($extesiones,$datos['fichero']);
		$etiquetas=obtieneEtiquetasDocumento($datos['codigo']);
		$botonesExtra=obtieneBotonesExtra($datos['codigo'],$datos['fichero']);

		$mostrar = true;
		if(isset($_POST['codigoEtiqueta'])){
			if($_POST['tipoFiltro'] == 'AND'){
				for($i=1;$i<count($_POST['codigoEtiqueta']) && $mostrar == true;$i++){
					$documento = consultaBD("SELECT * FROM etiquetas_documentos WHERE codigoDocumento=".$datos['codigo']." AND codigoEtiqueta=".$_POST['codigoEtiqueta'][$i],false,true);
					if(!$documento){
						$mostrar = false;
					}
				}
			}
		}

		if($mostrar){
			$res.= "
				<tr>
					<td><a href='".$_CONFIG['raiz']."categorias/gestion.php?codigo=".$datos['codigo']."'><i class='".$icono."'></i> ".$datos['nombre']."</a></td>
					<td>".formateaFechaWeb($datos['fecha'])."</td>
					<td>".$datos['autor']."</td>
					<td>".formateaFechaWeb($datos['fechaModificacion'])."</td>
					<td> ".$etiquetas."</td>
					<td class='centro'>
						<div class='btn-group'>
							<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
					  		<ul class='dropdown-menu' role='menu'>
						    	<li><a href='".$_CONFIG['raiz']."categorias/gestion.php?codigo=".$datos['codigo']."'><i class='icon-edit'></i> Editar</i></a></li>
						    	$botonesExtra
							</ul>
						</div>
					</td>
				</tr>";
		}
	}
	cierraBD();

	$res.="</tbody>
                </table>
              </div>
          <!-- /widget-content-->
        </div>";

    echo $res;
}

function obtieneBotonesExtra($codigo,$documento){
	global $_CONFIG;
	$res='';

	if($documento!='NO'){
		$res="<li class='divider'></li>
			  <li><a href='".$_CONFIG['raiz']."categorias/envio.php?codigoDocumento=".$codigo."'><i class='icon-send'></i> Enviar</i></a></li>
			  <li class='divider'></li>
			  <li><a href='".$_CONFIG['raiz']."documentos/documentos-internos/".$documento."' class='noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar fichero</i></a></li>";
	}

	return $res;
}

function obtieneIconoDocumento($extensiones,$nombre){
	$res='icon-file-text-o';
	$extensionFichero=explode('.',$nombre);
	$extensionFichero=end($extensionFichero);

	foreach($extensiones as $extension => $icono) {
		if(substr_count($extension,$extensionFichero)==1){
			$res=$icono;
		}
	}

	return $res;
}

function obtieneEtiquetasDocumento($codigoDocumento){
	$res="";

	$consulta=consultaBD("SELECT codigo, nombre, color FROM etiquetas INNER JOIN etiquetas_documentos AS etiquetasDocumentos ON etiquetas.codigo=etiquetasDocumentos.codigoEtiqueta WHERE codigoDocumento='$codigoDocumento'");
	while($datosE=mysql_fetch_assoc($consulta)){
		$res.="<span class='label' style='background-color:".$datosE['color'].";'><i class='icon-tag'></i> ".$datosE['nombre']."</span> ";
	}

	return $res;
}
//Fin parte de gestor documental