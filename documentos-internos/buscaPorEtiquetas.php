<?php
  $seccionActiva=17;
  include_once('../cabecera.php');
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row content">

        <div class="span12">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class='icon-tags'></i><i class='icon-chevron-right'></i><i class="icon-search"></i>
              <h3>Búsqueda por etiquetas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">A continuación, seleccione por favor una o varias etiquetas y pulse en Buscar para encontrar sus documentos asociados:</h6>
                  
                      <?php
                        conexionBD();
                          abreColumnaCampos('span12');
                            campoRadio('tipoFiltro','Tipo de búsqueda','','OR',array('Alguna etiqueta seleccionada','Todas las etiquetas seleccionadas'),array('OR','AND'));
                          cierraColumnaCampos(true);
                          $consulta=consultaBD("SELECT * FROM etiquetas");
                          while($etiqueta=mysql_fetch_assoc($consulta)){
                            abreColumnaCampos('span3');
                              campoCheckEtiqueta('codigoEtiqueta[]',$etiqueta['nombre'],$etiqueta['color'],$etiqueta['codigo']);
                            cierraColumnaCampos();
                          }
                        cierraBD();
                      ?>
                      <br clear="all">
                      <br />
                      <div style='margin-left:33px;'>
                        <a href='index.php' class='btn btn-default'><i class='icon-chevron-left'></i> Volver </a>
                        <a href="javascript:void()" id="buscaDocumentos" class="btn btn-propio noAjax"><i class="icon-search"></i> Buscar</a>
                      </div>
                  
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
          </div>
            <?php
              imprimeDocumentos();
            ?>
 
         
        </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-select.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){

    $("#buscaDocumentos").click(function() {
        $(".documentos").remove();
        var tipoFiltro = $('input[name="tipoFiltro"]:checked').val();
        var codigoEtiqueta = new Array();
        $('input[name="codigoEtiqueta[]"]:checked').each(function() {
          codigoEtiqueta.push($(this).val());
        });
        var consultaDocumentos=$.post('obtieneDocumentos.php',{'codigoEtiqueta':codigoEtiqueta,'tipoFiltro':tipoFiltro});
        consultaDocumentos.done(function(documentos){
          actualizaTabla(documentos);
        });
    });
});

  function actualizaTabla(documentos){
    var content = $(".content").html() + documentos;
    $(".content").append(documentos);//select
  }
</script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>