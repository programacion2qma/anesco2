<?php
  $seccionActiva=17;
  include_once('../cabecera.php');
  
  $res=operacionesEspaciosTrabajo();
  $estadisticas=estadisticasGestorDocumental();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de documentación interna:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-folder-open-o"></i> <span class="value"><?php echo $estadisticas['espaciosTrabajo']?></span> <br>Carpeta/s</div>
                     <div class="stat"> <i class="icon-file-text-o"></i> <span class="value"><?php echo $estadisticas['documentos']?></span> <br>Documento/s</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de carpetas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="<?php echo $_CONFIG['raiz']; ?>documentos-internos/gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva carpeta</span> </a>
                <a href="<?php echo $_CONFIG['raiz']; ?>etiquetas/" class="shortcut"><i class="shortcut-icon icon-tags"></i><span class="shortcut-label">Etiquetas</span> </a>
                <a href="<?php echo $_CONFIG['raiz']; ?>documentos-internos/buscaPorEtiquetas.php" class="shortcut"><i class="shortcut-icon icon-search"></i><span class="shortcut-label">Busca por etiquetas</span> </a>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Carpetas registradas</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Nombre </th>
                  <th> Fecha de creación </th>
                  <th> Propietario </th>
                  <th> Documentos </th>
                  <th> Última modificación </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeEspaciosTrabajo();
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>