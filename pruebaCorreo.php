<?php

enviaEmail('webmaster@qmaconsultores.com','Prueba','Prueba de funcionamiento desde Anesco con la cuenta de noreply','img/logo.png');

function enviaEmail($destinatario,$asunto,$mensaje,$adjunto=false){
    /**
     * This example shows settings to use when sending via Google's Gmail servers.
     * This uses traditional id & password authentication - look at the gmail_xoauth.phps
     * example to see how to use XOAUTH2.
     * The IMAP section shows how to save this message to the 'Sent Mail' folder using IMAP commands.
     */
    //Import PHPMailer classes into the global namespace
    require_once('phpmailer-personalizado/PHPMailer.php');
    require_once('phpmailer-personalizado/SMTP.php');
    require_once('phpmailer-personalizado/Exception.php');

    //$asunto="=?ISO-8859-1?B?".base64_encode($asunto)."=?=";//Para tildes;

    //Create a new PHPMailer instance
    $mail = new PHPMailer\PHPMailer\PHPMailer;
    //Tell PHPMailer to use SMTP
    $mail->isSMTP();
    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    $mail->SMTPDebug = 0;


    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );

    //Set the hostname of the mail server
    $mail->Host = 'smtp.strato.com';
    // use
    // $mail->Host = gethostbyname('smtp.gmail.com');
    // if your network does not support SMTP over IPv6
    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    $mail->Port = 465;
    //Set the encryption system to use - ssl (deprecated) or tls
    $mail->SMTPSecure = 'ssl';
    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;
    //Username to use for SMTP authentication - use full email address for gmail
    $mail->Username = "noreply@anescoprl.es";
    //Password to use for SMTP authentication
    $mail->Password = "Eddc45vff341";
    //Set who the message is to be sent from
    $mail->setFrom('noreply@anescoprl.es','Avisos ANESCO');
    //Set an alternative reply-to address
    //$mail->addReplyTo($remitente,'Avisos ANESCO');
    //Set who the message is to be sent to
    $mail->addAddress($destinatario);
    // Activa la condificacción utf-8
    $mail->CharSet = 'UTF-8';
    //Set the subject line
    $mail->Subject = $asunto;
    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    $mail->msgHTML($mensaje);
    //Replace the plain text body with one created manually
    //$mail->AltBody = 'This is a plain-text message body';
    //Attach an image file
    
    if($adjunto!=false){
        $mail->addAttachment($adjunto);
    }

    //send the message, check for errors
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
        $res=false;
    } else {
        $res=true;
    }
    
    return $res;
}