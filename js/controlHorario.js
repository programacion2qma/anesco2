/*
	Horarios de técnicos
	********************

	Adán: Lunes, Martes y Jueves de 8 a 14 y de 16 a 19. Miércoles y Viernes de 8 a 14.
	Ángeles: Lunes a Jueves de 9 a 14 y de 16 a 19. Viernes de 9 a 13.
*/

//Para /tareas/gestion.php
function compruebaHorariosTecnicosGestion(){
	var res=true;
	
	if($('input[name=todoDia]:checked').val()=='false'){//Solo se valida si el campo "Día completo" está a "No"

		var tecnico=$('#codigoUsuario').val();
		var horaInicio=$('#horaInicio').val();
		var horaFin=$('#horaFin').val();
		var fecha=formateaFechaCalculo($('#fechaInicio').val());
		var fechaFin=formateaFechaCalculo($('#fechaFin').val());
		
		fecha=new Date(fecha); 
		fechaFin=new Date(fechaFin);
		horaInicio=horaInicio.split(':');
		horaFin=horaFin.split(':');

		while (fecha.getTime()<=fechaFin.getTime()){

			if(tecnico==145){//Adán
				res=res && compruebaHorarioAdan(fecha,horaInicio,horaFin);
			}
			else if(tecnico==146){//Ángeles
				res=res && compruebaHorarioAngeles(fecha,horaInicio,horaFin);
			}
			
			//Incremento de 86400000 milisegundos, que equivale a 1 día
			fecha.setTime(parseInt(fecha.getTime())+86400000);
		}

		if(!res){
			alert('Error: la tarea está fuera del horario del técnico.');
		}

	}

	return res;
}

//Para /tareas/index.php (agenda)
function compruebaHorariosTecnicosAgenda(fecha,fechaFin,horaInicio,horaFin,todoDia){
	var res=true;

	if(!todoDia){

		var tecnico=$('#codigoUsuario').val();
		
		fecha=formateaFechaCalculo(fecha);
		fechaFin=formateaFechaCalculo(fechaFin);
		fecha=new Date(fecha); 
		fechaFin=new Date(fechaFin);
		horaInicio=horaInicio.split(':');
		horaFin=horaFin.split(':');

		while (fecha.getTime()<=fechaFin.getTime()){

			if(tecnico==145){//Adán
				res=res && compruebaHorarioAdan(fecha,horaInicio,horaFin);
			}
			else if(tecnico==146){//Ángeles
				res=res && compruebaHorarioAngeles(fecha,horaInicio,horaFin);
			}
			
			//Incremento de 86400000 milisegundos, que equivale a 1 día
			fecha.setTime(parseInt(fecha.getTime())+86400000);
		}

		if(!res){
			alert('Error: la tarea está fuera del horario del técnico.');
		}

	}

	return res;
}


//Esta función coge una fecha dd/mm/aaaa y la pasa a aaaa-mm-dd
function formateaFechaCalculo(fecha){
	var arrayFecha=fecha.split('/');

	return arrayFecha[2]+'-'+arrayFecha[1]+'-'+arrayFecha[0];
}

//Esta función hace lo inverso a la anterior
function formateaFechaWeb(fecha){
	var arrayFecha=fecha.split('-');

	return arrayFecha[2]+'/'+arrayFecha[1]+'/'+arrayFecha[0];
}

//Horario de Adán: Lunes, Martes y Jueves de 8 a 14 y de 16 a 19. Miércoles y Viernes de 8 a 14.
function compruebaHorarioAdan(fecha,horaInicio,horaFin){
	var res=true;
	var diaSemana=fecha.getDay();

	if(diaSemana==1 || diaSemana==2 || diaSemana==4){

		if(
			!(
				((parseInt(horaInicio[0])>=8 && parseInt(horaFin[0])<14) || (parseInt(horaInicio[0])>=8 && parseInt(horaFin[0])==14 && parseInt(horaFin[1])==0))
				||
				((parseInt(horaInicio[0])>=16 && parseInt(horaFin[0])<19) || (parseInt(horaInicio[0])>=16 && parseInt(horaFin[0])==19 && parseInt(horaFin[1])==0))
			)
		){
			res=false;
		}

	}
	else if(diaSemana==3 || diaSemana==5){
		if(
			!(
				((parseInt(horaInicio[0])>=8 && parseInt(horaFin[0])<14) || (parseInt(horaInicio[0])>=8 && parseInt(horaFin[0])==14 && parseInt(horaFin[1])==0))
			)
		){
			res=false;
		}
	}

	return res;
}

//Horario de Ángeles: Lunes a Jueves de 9 a 14 y de 16 a 19. Viernes de 9 a 13.
function compruebaHorarioAngeles(fecha,horaInicio,horaFin){
	var res=true;
	var diaSemana=fecha.getDay();

	if(diaSemana==1 || diaSemana==2 || diaSemana==3 || diaSemana==4){

		if(
			!(
				((parseInt(horaInicio[0])>=9 && parseInt(horaFin[0])<14) || (parseInt(horaInicio[0])>=9 && parseInt(horaFin[0])==14 && parseInt(horaFin[1])==0))
				||
				((parseInt(horaInicio[0])>=16 && parseInt(horaFin[0])<19) || (parseInt(horaInicio[0])>=16 && parseInt(horaFin[0])==19 && parseInt(horaFin[1])==0))
			)
		){
			res=false;
		}

	}
	else if(diaSemana==5){
		if(
			!(
				((parseInt(horaInicio[0])>=9 && parseInt(horaFin[0])<13) || (parseInt(horaInicio[0])>=9 && parseInt(horaFin[0])==13 && parseInt(horaFin[1])==0))
			)
		){
			res=false;
		}
	}

	return res;
}