var mensajeMostrado = 0;

$('#EMPIBAN').focusout(function(){
  if(!fn_ValidateIBAN($(this).val())){
    alert('EL IBAN no es valido');
    $(this).css('border','1px solid red');
  } else {
    $(this).css('border','1px solid #cccccc');
  }
});

$('#validarIban').click(function(){
  var iban='';
  $('.iban').each(function(){
    iban+=$(this).val();
  });
  if(!fn_ValidateIBAN(iban)){
    alert('EL IBAN no es valido');
    $('.iban').css('border','1px solid red');
  } else {
    $('.iban').css('border','1px solid #cccccc');
  }
});

$('#EMPNTRAB,input[type=number]').change(function(e){
    var valor=$(this).val();
    
    return validaNumero($(this),valor);
});

/*
$('input[type=number]').keydown(function(e){
  var keynum = window.event ? window.event.keyCode : e.which;
  if(!validaNumero(keynum)){
    alert('Este campo solo acepta números');
    return false;
  }
});
*/

function validaNumero(campo,valor){
  /*var res=false;
  if ((tecla == 8) || (tecla == 46)){
    res=true;
  } else {
    res=/\d/.test(String.fromCharCode(tecla));
  }
  return res;*/

    var res=true

    if(isNaN(valor)){
        alert('Este campo solo acepta números');
        res=false;
        campo.focus();
    }

    return res;
}

// Función que devuelve los números correspondientes a cada letra
function getNumIBAN(letra){
   var letras = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
   return letras.search(letra) + 10;
}

function fn_ValidateIBAN(IBAN) {

    //Se pasa a Mayusculas
    IBAN = IBAN.toUpperCase();
    //Se quita los blancos de principio y final.
    IBAN = IBAN.trim();
    IBAN = IBAN.replace(/\s/g, ""); //Y se quita los espacios en blanco dentro de la cadena

    var letra1,letra2,num1,num2;
    var isbanaux;
    var numeroSustitucion;
    //La longitud debe ser siempre de 24 caracteres
    if (IBAN.length != 24) {
        return false;
    }

    // Se coge las primeras dos letras y se pasan a números
    letra1 = IBAN.substring(0, 1);
    letra2 = IBAN.substring(1, 2);
    num1 = getnumIBAN(letra1);
    num2 = getnumIBAN(letra2);
    //Se sustituye las letras por números.
    isbanaux = String(num1) + String(num2) + IBAN.substring(2);
    // Se mueve los 6 primeros caracteres al final de la cadena.
    isbanaux = isbanaux.substring(6) + isbanaux.substring(0,6);

    //Se calcula el resto, llamando a la función modulo97, definida más abajo
    resto = modulo97(isbanaux);
    if (resto == 1){
        return true;
    }else{
        return false;
    }
}

function modulo97(iban) {
    var parts = Math.ceil(iban.length/7);
    var remainer = "";

    for (var i = 1; i <= parts; i++) {
        remainer = String(parseFloat(remainer+iban.substr((i-1)*7, 7))%97);
    }

    return remainer;
}

function getnumIBAN(letra) {
    ls_letras = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    return ls_letras.search(letra) + 10;
}


function formateaCampoObligatorio(campo,res){
  if(campo.hasClass('selectpicker') && res==0){
    campo.selectpicker('setStyle', 'btn-danger','remove');
  }
  else if(campo.hasClass('selectpicker') && res==1){
    campo.selectpicker('setStyle', 'btn-danger');
  }
  else if(res==0){
    campo.css('border','1px solid #cccccc');
  }
  else{
    campo.css('border','1px solid red');
  }
}

function marcarCampo(id){
  valor = $(id).text().trim()
  if(valor==''){
    $(id).text('No puede estar vacío');
  }
  if(res > 0){
    $(id).css('color','red');
  }
}


/*function validaCif(abc,doble=false){
	res = 0;
	par = 0;
	non = 0;
	letras = "ABCDEFGHJKLMNPQRSUVW";
	let = abc.charAt(0);
 	mensaje = '';
	if (abc.length!=9) {
		mensaje='El CIF debe tener 9 dígitos';
		res=1;
	}
 	
	if (letras.indexOf(let.toUpperCase())==-1) {
		mensaje="El comienzo del CIF no es válido";
		res=1;
	}

 	for (zz=2;zz<8;zz+=2) {
 		par = par+parseInt(abc.charAt(zz));
 	}
 	
 	for (zz=1;zz<9;zz+=2) {
 		nn = 2*parseInt(abc.charAt(zz));
 		if (nn > 9) nn = 1+(nn-10);
 		non = non+nn;
 	}
 	
 	parcial = par + non;
 	control = (10 - ( parcial % 10));
 	if (control==10) control=0;
 	
  alert(control);
  alert(abc.charAt(8));
 	if (control!=abc.charAt(8)) {
 		mensaje="El CIF no es válido";
 		res=1;
 	}

 	if(!doble){
 		if(mensaje!='' && mensajeMostrado == 0){
 			alert(mensaje);
 			mensajeMostrado = 1;
 		}
 	}
 	return res;
}*/

function validaCif(control,doble=false){ 
  res=0;
  var texto=control;
  var pares = 0; 
  var impares = 0; 
  var suma; 
  var ultima; 
  var unumero; 
  var uletra = new Array("J", "A", "B", "C", "D", "E", "F", "G", "H", "I"); 
  var xxx; 
  texto = texto.toUpperCase(); 
  mensaje='';
  var regular = new RegExp(/^[ABCDEFGHKLMNPQS]\d\d\d\d\d\d\d[0-9,A-J]$/g); 
  if (!regular.exec(texto)) {
      mensaje='El CIF no es valido';
      res=1;
  }
  ultima = texto.substr(8,1); 
 
  for (var cont = 1 ; cont < 7 ; cont ++){ 
    xxx = (2 * parseInt(texto.substr(cont++,1))).toString() + "0"; 
    impares += parseInt(xxx.substr(0,1)) + parseInt(xxx.substr(1,1)); 
    pares += parseInt(texto.substr(cont,1)); 
  } 
  xxx = (2 * parseInt(texto.substr(cont,1))).toString() + "0"; 
  impares += parseInt(xxx.substr(0,1)) + parseInt(xxx.substr(1,1)); 
          
  suma = (pares + impares).toString(); 
  unumero = parseInt(suma.substr(suma.length - 1, 1)); 
  unumero = (10 - unumero).toString(); 
  if(unumero == 10) unumero = 0; 
          
  if ((ultima == unumero) || (ultima == uletra[unumero])) {
              
  } else {
     mensaje='El CIF no es valido';
     res=1;
  }

  if(!doble){
    if(mensaje!='' && mensajeMostrado == 0){
      alert(mensaje);
      mensajeMostrado = 1;
    }
  }
  return res;
} 

function validaNif(dni,doble=false) {
  var res = 0;
  var mensaje = '';	
  var numero;
  var let;
  var letra
  var expresion_regular_dni
 
  expresion_regular_dni = /^\d{8}[a-zA-Z]$/;
  expresion_regular_nie = /^[xyzXYZ]{1}[0-9]{7}[a-zA-Z]{1}$/i;

  if(expresion_regular_dni.test (dni) == true){
    numero = dni.substr(0,dni.length-1);
    let = dni.substr(dni.length-1,1);
    numero = numero % 23;
    letra='TRWAGMYFPDXBNJZSQVHLCKET';
    letra=letra.substring(numero,numero+1);
    if (letra!=let.toUpperCase()) {
       mensaje='Dni erroneo, la letra del NIF no se corresponde';
       res=1;
    }
  }
  else if(expresion_regular_nie.test (dni) == false){
    mensaje='Dni erroneo, formato no válido';
    res=1;
  }

   	if(!doble){
 		  if(mensaje!='' && mensajeMostrado == 0){
 			  alert(mensaje);
 			  mensajeMostrado = 1;
 		  }
 	  }
 	return res;
}

function validarNifoCif(valor){
  var res = 0;
  valor = valor.trim()
  if(valor==''){
    res = 1;
    alert('No se puede modificar el contrato si el cliente no tiene CIF/DNI introducido');
    marcarCampo('#EMPCIF',res);
  } else {
	  res=validaNif(valor,true);
	  if(res > 0){
		  res=validaCif(valor,true);
	  }
	  if(res > 0){
		  if(mensajeMostrado == 0){
        marcarCampo('#EMPCIF',res);
 			  alert('CIF/NIF Erroneo, compruebe letras y formatos. No se puede modificar el contrato');
 			  mensajeMostrado = 1;
 		  }
 	  }
  }
 	return res;
}
  
$(document).ready(function(){
  /*if($("#bloqueado").length && $('#bloqueado').val()=='SI'){
      $('form input,select,textarea').attr('readonly','readonly');
      $('form input,select,textarea').attr('disabled','disabled');
      $('form button[type=submit]').addClass('hide');
      $('form button.submit').addClass('hide');
      $('form button:not("#mostrarClave")').addClass('hide');
      $(':submit,.widget-header .pull-right .btn-propio').addClass('hide');
      $('#contenedor-ficheroLogo a').removeClass('hide');
    }*/
});  
  


