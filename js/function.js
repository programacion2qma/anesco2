$(document).ready(function(){
    desplegable();
});

function isValidCif(cif){
    var res=false;
   //Quitamos el primer caracter y el ultimo digito
    var valueCif=cif.substr(1,cif.length-2);
    var suma=0;

    //Sumamos las cifras pares de la cadena
    for(i=1;i<valueCif.length;i=i+2){
        suma=suma+parseInt(valueCif.substr(i,1));
    }

    var suma2=0;

    //Sumamos las cifras impares de la cadena
    for(i=0;i<valueCif.length;i=i+2){

        var result=parseInt(valueCif.substr(i,1))*2;
        if(String(result).length==1){
            // Un solo caracter
            suma2=suma2+parseInt(result);
        }
        else{
            // Dos caracteres. Los sumamos...
            suma2=suma2+parseInt(String(result).substr(0,1))+parseInt(String(result).substr(1,1));
        }
    }

    // Sumamos las dos sumas que hemos realizado
    suma=suma+suma2;

    var unidad = String(suma).substr(String(suma).length - 1, 1);
    unidad=10-parseInt(unidad);

    var primerCaracter=cif.substr(0,1).toUpperCase();

    var lastchar=cif.substr(cif.length-1,1);
    var lastcharchar=lastchar;

    if(!isNaN(lastchar)) {
        lastcharchar=String.fromCharCode(64+parseInt(lastchar));
    }

    if(primerCaracter.match(/^[FJKNPQRSUVW]$/)) {
        //Empieza por .... Comparamos la ultima letra
        if(String.fromCharCode(64+unidad).toUpperCase()==lastcharchar) {
            res=true;
        }
    }
    else if(primerCaracter.match(/^[XYZ]$/)){
        //Se valida como un DNI (extranjero)
        var nuevoCIF;
        if(primerCaracter=="X"){
            nuevoCIF=cif.substr(1);
        }
        else if(primerCaracter=="Y"){
            nuevoCIF="1"+cif.substr(1);
        }
        else if(primerCaracter=="Z"){
            nuevoCIF="2"+cif.substr(1);
        }
        res=isValidNif('0'+nuevoCIF,cif);
    }
    else if(primerCaracter.match(/^[ABCDEFGHLM]$/)){
        //Se revisa que el último valor coincida con el cálculo
        if(unidad==10){
            unidad=0;
        }
        if(cif.substr(cif.length-1,1)==String(unidad)){
            res=true;
        }
        else{
            //Se valida como un DNI
            res=isValidNif(cif);
        }
    }
    else{
        res=isValidNif(cif);
    }
    
    return res;
}

function isValidNif(dni,dnie=0) {
  var res=true;
  var numero;
  var let;
  var letra;
  var expresion_regular_dni;

  expresion_regular_dni = /^\d{8}[a-zA-Z]$/;
  expresion_regular_nie = /^[xyzXYZ]{1}[0-9]{7}[a-zA-Z]{1}$/i;

  if(expresion_regular_dni.test (dni) == true){
    numero = dni.substr(0,dni.length-1);
    let = dni.substr(dni.length-1,1);
    numero = numero % 23;
    letra='TRWAGMYFPDXBNJZSQVHLCKET';
    letra=letra.substring(numero,numero+1);
    if (letra!=let.toUpperCase()) {
       res=false;
    }
  }
  else if(expresion_regular_nie.test (dnie) == false){
    res=false;
  }
  return res;
}

function isValidCifNif(campo){
  var res = false;
  res=isValidCif(campo);

  if(res == false){

      alert('CIF/NIF Erroneo, compruebe letras y formatos');
  }
  return res;
}

function desplegable(){
  if($('.mainnav.menu li:nth-child(4) > a > span').text()=='Área técnica'){

    var ul=$('.mainnav.menu li:nth-child(4) ul');
    ul.css('height','280px');
    ul.css('overflow','hidden');
    var elem=$('.mainnav.menu li:nth-child(4) ul li:nth-child(13) a');
    elem.removeClass('opcionBloqueadaMenu');
    elem.attr('onclick',false);
    elem.unbind();
    elem.mouseover(function(e){
      e.preventDefault();
      ul.css('height','auto');
    })
    elem.mouseout(function(e){
      e.preventDefault();
      ul.css('height','280px');
    })
  }
}