//Esta función crea un fomulario oculto y lo envía. Para su uso en combinación con los checks de los listados y los botones

function creaFormulario(destino, metodo, target) {
  metodo = metodo || "get";

  var form = document.createElement("form");
  form.setAttribute("method", metodo);
  form.setAttribute("action", destino);

  document.body.appendChild(form);

  if(target!=undefined){//Si se define el atributo target, se añade al formulario y éste se envía de forma normal
    form.setAttribute("target",target);    
  }
  else{//De lo contrario, el formulario se envía asíncronamente
    //cargaPagina(destino,contenido,false,$('#formularioElimina').serialize());
  }

  form.submit();
}
