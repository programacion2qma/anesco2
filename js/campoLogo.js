$(document).ready(function(){
	$(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});

	$('#elimina-ficheroLogo').click(function(){
		$('#contenedor-ficheroLogo').remove();
		$('#contenedor-ficheroLogoOculto').removeClass('hide');
		$('#ficheroLogoOculto').attr('name','ficheroLogo');
	});
});