$(document).ready(function(){

    setTimeout(function(){
        $('.selectInserccion .dropdown-menu ul li:not(:first)').append("<button type='button' class='eliminaOpcionDesplegable btn btn-small btn-danger'><i class='icon-trash'></i></button>");
    },2000);

    $('.selectInserccion .input-block-level.form-control').keydown(function(tecla){//Oyente de teclado para los campos de búsqueda de los selectpicker
        if(tecla.which==13 && $(this).parent().next().find('.no-results').length==1){//Si se pulsa la tecla Intro y no hay resultados para el término introducido...
            //Evito que se envíe el formulario al pulsar Intro
            tecla.preventDefault();
            var id=$(this).parent().parent().parent().parent().find('.selectInserccion').attr('id');
            var val=$(this).val();
            var res=registraDesplegable(id,val);
        }
    });

    $(document).on('click','.eliminaOpcionDesplegable',function(e){
        e.stopImmediatePropagation();
        e.preventDefault();
        e.stopPropagation();

        var id=$(this).parent().parent().parent().prev().attr('data-id');
        var numeroOpcion=$(this).parent().index()+1;
        var val=$('#'+id).find('option:nth-child('+numeroOpcion+')').val();
        
        console.log('ID: '+id+' - Opción nº: '+numeroOpcion+' - Valor: '+val);
        
        if(val!=null){
            var res=eliminaDesplegable(id,val);
        }

        return false;
    });

});

function registraDesplegable(campo,valor){
    var consulta=$.post('../listadoAjax.php?include=reconocimientos-medicos&funcion=registraDesplegable();',{
        'campo':campo,
        'valor':valor
    });

    consulta.done(function(respuesta){
        
        if(respuesta=='false'){
            alert('Ha ocurrido un error al registrar la opción. Vuelva a intentarlo. Si el problema persiste, contacte con webmaster@qmaconsultores.com');
        } 
        else {
            $('#'+campo).html(respuesta);
            $('#'+campo).parent().find('.btn-group').removeClass('open');
            $('#'+campo).selectpicker('refresh');
        }

    });
}

function eliminaDesplegable(campo,valor){
    if(confirm("¿Realmente desea eliminar esta entrada en el campo? La supresión será retroactiva")){
        var consulta=$.post('../listadoAjax.php?include=reconocimientos-medicos&funcion=eliminaDesplegable();',{
            'campo':campo,
            'valor':valor
        });

        consulta.done(function(respuesta){
            if(respuesta=='false'){
                alert('Ha ocurrido un error al eliminar la opción. Vuelva a intentarlo. Si el problema persiste, contacte con webmaster@qmaconsultores.com');
            } 
            else {
                $('#'+campo).html(respuesta);
                //$('#'+campo).parent().find('.btn-group').removeClass('open');
                $('#'+campo).selectpicker('refresh');

                $('.eliminaOpcionDesplegable').remove();
                $('.selectInserccion .dropdown-menu ul li:not(:first)').append("<button type='button' class='eliminaOpcionDesplegable btn btn-small btn-danger'><i class='icon-trash'></i></button>");
            }
        });
    }
}

function insertaPrueba(tabla){//Función común a recos y protocolización
    insertaFila(tabla);
    $('.selectInserccion .input-block-level.form-control').keydown(function(tecla){//Oyente de teclado para los campos de búsqueda de los selectpicker
        if(tecla.which==13 && $(this).parent().next().find('.no-results').length==1){//Si se pulsa la tecla Intro y no hay resultados para el término introducido...
          //Evito que se envíe el formulario al pulsar Intro
          tecla.preventDefault();
          var id=$(this).parent().parent().parent().parent().find('.selectInserccion').attr('id');
          var val=$(this).val();
          var res=registraDesplegable(id,val);
          //$(this).parent().parent().parent().removeClass('open');
        }
    });
}