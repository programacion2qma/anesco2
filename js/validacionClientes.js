$(document).ready(function(){
	$('.form-horizontal button[type=submit]').unbind();
	$('.form-horizontal button[type=submit]').click(function(e){
 		var boton=$(this);
 		e.preventDefault();
 		
 		if($('#usuario').val()=='' || $('#clave').val()==''){
 			alert('Por favor, introduzca unas credenciales para el usuario.');
 		} 
 		else {
 			var usuario=$('#usuario').val();
 			var codigoUsuario=$('#codigoUsuario').val();
	
			if(codigoUsuario==undefined){
				codigoUsuario=0;
			}
		
			var consulta=$.post('../listadoAjax.php?funcion=compruebaUsuario();',{'codigoUsuario':codigoUsuario,'usuario':usuario});
			consulta.done(function(respuesta){
				if(respuesta=='error'){
					alert('Nombre de usuario repetido. Por favor, elija otro nombre.');
					$('#usuario').focus();
				}
				else if(boton.attr('name')=='enviaCredenciales'){
					alert('El envío de credenciales está desactivado');
					//$('.form-horizontal').attr('action','index.php?enviaCredenciales').submit();
				}
				else {	
					$('.form-horizontal').submit();
				}
			});
		}

	});
	
});