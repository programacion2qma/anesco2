$(document).ready(function(){
    oyenteCambiosContrato();
    $('input[name=pregunta6]').change(function(){
        oyenteCambiosContrato();
    });

    $('.output').each(function(){
        var id=$(this).attr('id');
        var firma=$(this).val().trim();

        inicializaCampoFirma(id,firma);
    });

    $('.selectEmpleadoFunciones').change(function(){
        cambiaEmpleado($(this));
    });
});

function oyenteCambiosContrato(){
    if($('input[name=pregunta6]:checked').val()=='SI'){
        $('#cajaObservacionesDatosGenerales').removeClass('hide');
    }
    else{
        $('#cajaObservacionesDatosGenerales').addClass('hide');
    }
}

function inicializaCampoFirma(id,firma){
    var pregunta0=$('#pregunta0').val();//Uso pregunta0 para saber si estoy en la Toma de datos o en el formulario de detalles

    if(pregunta0!=undefined && firma==''){
        $('#contenedor-'+id).signaturePad({drawOnly:true,lineTop:'500',penColour:'000000',validateFields:false,output:'#'+id});
    }
    else if(pregunta0!=undefined){
        $('#contenedor-'+id).signaturePad({drawOnly:true,lineTop:'500',penColour:'000000',validateFields:false,output:'#'+id}).regenerate(firma);
    }
}

function cambiaEmpleado(elem){
    var id=elem.attr('id').replace('codigoEmpleadoFunciones','nombreEmpleadoFunciones');
    var texto=elem.find('option:selected').html();
    $('#'+id).val(texto);
}

function insertaFilaFunciones(tabla){
    insertaFila(tabla);
    insertaFila(tabla+'_2');
     $('.selectEmpleadoFunciones').change(function(){
        cambiaEmpleado($(this));
    });
}

function eliminaFilaFunciones(tabla){
    tabla2=tabla+'_2';
    if($('#'+tabla).find("tbody tr").length>1){//Si la tabla tiene más de 1 fila...
    var filasSeleccionadas=0;
    $('input[name="filasTabla[]"]:checked').each(function() {
        var fila=$(this).val();
        $('#'+tabla+' tr').eq(fila).remove();
        $('#'+tabla2+' tr').eq(fila).remove();
        filasSeleccionadas++;
    });

    if(filasSeleccionadas>0){
        //Renumeración de los índices de la tabla
        var numFilas=$('#'+tabla+' tr:not(:first)').length;
        for(i=0;i<numFilas;i++){
           $('#'+tabla+' tr:not(:first)').eq(i).find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
                var parts = this.name.split('_');
                return parts[0] +'_'+ i;
            }).attr("id", function(){
                return this.name;
            });
            $('#'+tabla2+' tr:not(:first)').eq(i).find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
                var parts = this.name.split('_');
                return parts[0] +'_'+ i;
            }).attr("id", function(){
                return this.name;
            });

            $('#'+tabla+' tr:not(:first)').eq(i).find("input[name='filasTabla[]']").attr("value", function(){
                var j=i+1
                return j;
            });
        }
        //Fin renumeración
    }
    else{
        alert('Para eliminar un registro, debe seleccionarlo utilizando el último check que tiene a su derecha.');
    }
  }
  else{
    alert('La tabla debe de contar con al menos 1 fila.');
  }
}