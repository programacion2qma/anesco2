function cargaOyentes(){
	var j=0;
	while($('#total'+j).val()!=undefined){
		$('#importe'+j).keyup(function(){
			var campo=$(this);
			cargaComprobacion(campo);
			var selector=$(this).attr('id').replace(/\D/g,'');
			pintaTotales(selector);
		});
		$('#peaje'+j).keyup(function(){
			var campo=$(this);
			cargaComprobacion(campo);
			var selector=$(this).attr('id').replace(/\D/g,'');
			pintaTotales(selector);
		});
		$('#taxi'+j).keyup(function(){
			var campo=$(this);
			cargaComprobacion(campo);
			var selector=$(this).attr('id').replace(/\D/g,'');
			pintaTotales(selector);
		});
		$('#parking'+j).keyup(function(){
			var campo=$(this);
			cargaComprobacion(campo);
			var selector=$(this).attr('id').replace(/\D/g,'');
			pintaTotales(selector);
		});
		$('#dietas'+j).keyup(function(){
			var campo=$(this);
			cargaComprobacion(campo);
			var selector=$(this).attr('id').replace(/\D/g,'');
			pintaTotales(selector);
		});
		$('#otros'+j).keyup(function(){
			var campo=$(this);
			cargaComprobacion(campo);
			var selector=$(this).attr('id').replace(/\D/g,'');
			pintaTotales(selector);
		});								
		j++;
	}
}

function cargaComprobacion(campo){
	if(campo.val()==''){
		campo.val('0');
	}else{
		if(parseFloat(campo.val())>=1){
			campo.val(campo.val().replace(/^0+/, ''));
		}
	}
}

function pintaTotales(selector){
	var importe=$('#importe'+selector).val();
	var peaje=$('#peaje'+selector).val();
	var taxi=$('#taxi'+selector).val();
	var parking=$('#parking'+selector).val();
	var dietas=$('#dietas'+selector).val();
	var otros=$('#otros'+selector).val();
	importe=importe.replace(',','.');
	peaje=peaje.replace(',','.');
	taxi=taxi.replace(',','.');
	parking=parking.replace(',','.');
	dietas=dietas.replace(',','.');
	otros=otros.replace(',','.');
	
	var total = parseFloat(importe) + parseFloat(peaje) + parseFloat(taxi) + parseFloat(parking) + parseFloat(dietas) + parseFloat(otros);	

	total=formateaNumeroWeb(total);
	$('#total'+selector).val(total);
}

function compruebaCampoTiempo(elem){
	var flag = false;
    var valor = elem.val();
    valor=valor.split(':');
    if(valor.length!=2){
    	flag=true
    } else {
    	if(!isNaN(valor[0]) && !isNaN(valor[1])){
    		valor[0]=parseInt(valor[0]);
        	valor[1]=parseInt(valor[1]);
        	if(valor[0]<10 || valor[1]<10){
        		var horas = valor[0]<10?'0'+valor[0]:valor[0];
            	var min = valor[1]<10?'0'+valor[1]:valor[1];
            	elem.val(horas+':'+min);
        	}
    	} else {
          flag = true;
        }
    }
    return flag;
}