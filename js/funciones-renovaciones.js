$(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    
    $(document).on('click','.confirmaContrato',function(e){
        var res=false;

        if(isValidCifNif($(this).attr('cif'))){
            res=true;
        }

        return res;
    });


    $(document).on('click','.generaContrato',function(e){
        if(isValidCifNif($(this).attr('cif'))){
            
            $('#codigoContrato').val($(this).attr('codigoContrato'));

            $('#fechaVigencia').val('');
            $('#fechaEmision').val('');
            $('#cajaFechas').modal({'show':true,'backdrop':'static','keyboard':false});
        }
    });

    $('#descargarContrato').click(function(){
        var codigoContrato=$('#codigoContrato').val();
        var vigenciaContrato=$('#vigenciaContrato').val();
        var renovar=$('input:radio[name=renovar]:checked').val()

        creaFormulario('generaDocumento.php',{'codigoContrato':codigoContrato,'vigenciaContrato':vigenciaContrato,'renovar':renovar},'post','_blank');
        $('#cajaFechas').modal('hide');
    });


    $(document).on('click','.renovar',function(e){            
    	var codigoContrato=$(this).attr('codigoContrato');
    	var fechaInicio=$(this).attr('fechaInicio');
    	var fechaFin=$(this).attr('fechaFin');

        $('#formularioRenovacion').attr('action','index.php?renovar='+codigoContrato);

        fechaInicio=fechaFin;
        fechaFin=fechaFin.split('/');
		var fecha=new Date(fechaFin[2],(fechaFin[1]-1),fechaFin[0]);//-1 para el mes!!
		fecha.setDate(fecha.getDate()+365);
		fechaFin=formateaFechaPrevistaCalculada(fecha);

		$('#fechaInicio').datepicker('setValue',fechaInicio);
		$('#fechaFin').datepicker('setValue',fechaFin);

        $('#cajaRenovacion').modal({'show':true,'backdrop':'static','keyboard':false});
    });

    $('#renovarContrato').click(function(){
        $('#cajaRenovacion').modal('hide');
        $('#formularioRenovacion').submit();
    });
});


function formateaFechaPrevistaCalculada(fecha){
	var res;
	var dia=fecha.getDate();
	var mes=fecha.getMonth()+1;//+1 para el mes!!

	if(dia<10){
		res='0'+dia;
	}
	else{
		res=dia;
	}

	res+='/';

	if(mes<10){
		res+='0'+mes;
	}
	else{
		res+=mes;
	}

	res+='/';

	res+=fecha.getFullYear();

	return res;
}