$(document).ready(function(){
    $('#reactivar').click(function(){//Eliminación de registros
        var valoresChecks=recorreChecks();
        if(valoresChecks['codigo0']==undefined){
            alert('Por favor, seleccione antes un registro del listado.');
        }
        else if(confirm('¿Está seguro/a de que desea restaurar los registros seleccionados?')){
            valoresChecks['elimina']='NO';
            creaFormulario(document.URL,valoresChecks,'post');//Cambiado el nombre del documento por la URL.
        }
    });
});