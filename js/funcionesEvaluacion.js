$(document).ready(function(){

    $('#tipoEvaluacion').change(function(){
        oyenteTipoEvaluacion(true);
    });

    $('#codigoCliente').change(function(){
        cargaPuestosCliente();
        cargaCentrosCliente();
    });

    $('#tipo').change(function(){
        oyenteTipoEvaluacion2(true);
    });
});

function oyenteTipoEvaluacion(carga){
    var tipo=$('#tipoEvaluacion').val();
    if(tipo=='PUESTOS'){
        $('#cajaPuestos').removeClass('hide');
        $('#cajaOtros').addClass('hide');
        $('#cajaCentros').addClass('hide');

        if(carga){
            cargaPuestosCliente();
        }
    }
    else if(tipo=='OTRAS'){
        $('#cajaPuestos').addClass('hide');
        $('#cajaOtros').removeClass('hide')
        $('#cajaCentros').addClass('hide');;
    }
    else if(tipo=='LUGAR'){
        $('#cajaPuestos').addClass('hide');
        $('#cajaOtros').addClass('hide');
        $('#cajaCentros').removeClass('hide');

        if(carga){
            cargaCentrosCliente();
        }
    }
    else{
        $('#cajaPuestos').addClass('hide');
        $('#cajaOtros').addClass('hide');
        $('#cajaCentros').addClass('hide');
    }
}

function oyenteTipoEvaluacion2(carga){
    var tipo=$('#tipo').val();
    if(tipo=='OTROS'){
        $('#divOtroTipo').removeClass('hide');
    } else {
        $('#divOtroTipo').addClass('hide');
    }
}


function cargaPuestosCliente(){
    var codigoCliente=$('#codigoCliente').val();

    if(codigoCliente!='NULL' && !$('#cajaPuestos').hasClass('hide')){
        var consulta=$.post('../listadoAjax.php?include=evaluacion-de-riesgos&funcion=obtienePuestosTrabajoCliente();',{'codigoCliente':codigoCliente});
        consulta.done(function(respuesta){
            $('#codigoPuestoTrabajo').html(respuesta).selectpicker('refresh');
        });
    }
}

function cargaCentrosCliente(){
    var codigoCliente=$('#codigoCliente').val();
    if(codigoCliente!='NULL' && !$('#cajaCentros').hasClass('hide')){
        var consulta=$.post('../listadoAjax.php?include=evaluacion-de-riesgos&funcion=obtieneCentrosTrabajoCliente();',{'codigoCliente':codigoCliente});
        consulta.done(function(respuesta){
            $('#codigoCentroTrabajo').html(respuesta).selectpicker('refresh');
        });
    }
}

function insertaFilaEvaluacion(tabla){
    var $tr = $('#'+tabla+" > tbody > tr:last").clone();

    var nombreDescarga=$tr.find('.descargaFichero').attr('nombre');
    $tr.find('.descargaFichero').after("<input type='file' name='"+nombreDescarga+"' id='"+nombreDescarga+"' />");

    $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        return obtenerNameEvaluacion(this.name);
    }).attr("id", function(){
        return this.name;
    });

    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker)").attr("id", function(){
        return obtenerNameEvaluacion(this.id);
    });

    $tr.find(".botonSelectAjax").attr("id", function(){
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find('.tablaImagenes').attr("id",function(){
        return obtenerIdTable(this.id);
    }).find('tbody tr:not(:first)').remove();

    $tr.find('.gestionMedida').attr("onclick",function(){
        return obtenerClickButton($(this).attr('onclick'));
    });

    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value","");
    $tr.find("input[type=checkbox]").removeAttr("checked");
    $tr.find("select").val("NULL");
    $tr.find('.bootstrap-select').remove();
    $tr.find('.bootstrap-filestyle').remove();
    $tr.find('.descargaFichero').remove();

    $('#'+tabla+" > tbody > tr:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){
        $tr.find(".selectpicker").selectpicker('refresh');
    }
    if(typeof jQuery.fn.filestyle=='function'){
        $tr.find(':file').filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    }
    if(typeof jQuery.fn.rating=='function'){
        var campoRating=$tr.find('.rating');
        $tr.find('.rating-input').replaceWith(campoRating);
        campoRating.rating();
    }
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
}

function eliminaFilaEvaluacion(tabla,elem){
  var partes=elem.id.split('_');
  var tamanio = $('#'+tabla+" > tbody > tr").length;
  if(tamanio>1){
    var fila = partes[2].match(/(\D+)(\d*)$/);
    $('#'+tabla+' > tbody > tr').eq(fila[2]).remove();
    var numFilas=$('#'+tabla+' > tbody > tr').length;
    for(i=0;i<numFilas;i++){
        $('#'+tabla+' > tbody > tr').eq(i).find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
            return obtenerNameEvaluacion(this.name,i);
        }).attr("id", function(){
            return this.name;
        });

        $('#'+tabla+' tr:not(:first)').eq(i).find("input[name='filasTabla[]']").attr("value", function(){
            var j=i+1
            return j;
        });
    }
  }
  else{
    alert('El riesgo debe de contar con al menos 1 bloque.');
  }
}

function insertaFilaImagenes(tabla){
    var $tr = $('#'+tabla+" > tbody > tr:last").clone();
    var nombreDescarga=$tr.find('.descargaFichero').attr('nombre');
    $tr.find('.descargaFichero').after("<input type='file' name='"+nombreDescarga+"' id='"+nombreDescarga+"' />");

    $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        var name=this.name.split('_');
        var last=name.length-1;
        name[last]=cambiarIndice(name[last]);
        return unirName(name);
    }).attr("id", function(){
        return this.name;
    });

    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    $tr.find('.bootstrap-filestyle').remove();
    $('#'+tabla+" > tbody > tr:last").after($tr);
    if(typeof jQuery.fn.filestyle=='function'){
        $tr.find(':file').filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    }
}

function eliminaFilaImagenes(tabla){
    if($('#'+tabla).find(".trDescripcionFicheros").length>1){//Si la tabla tiene más de 1 fila...
    var filasSeleccionadas=0;
    $('input[name="filasTabla[]"]:checked').each(function() {
        var fila=$(this).val();
        $('#'+tabla).find(".trDescripcionFicheros").eq(fila).remove();
        filasSeleccionadas++;
    });

    if(filasSeleccionadas>0){
        //Renumeración de los índices de la tabla
        var numFilas=$('#'+tabla).find(".trDescripcionFicheros").length;
        for(i=0;i<numFilas;i++){
           $('#'+tabla).find(".trDescripcionFicheros").eq(i).find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
                var name=this.name.split('_');
                var last=name.length-1;
                name[last]=cambiarIndice(name[last],i);
                return unirName(name);
            }).attr("id", function(){
                return this.name;
            });

            $('#'+tabla).find(".trDescripcionFicheros").eq(i).find("input[name='filasTabla[]']").attr("value", function(){
                var j=i
                return j;
            });
        }
        //Fin renumeración
    }
    else{
        alert('Para eliminar un registro, debe seleccionarlo utilizando el último check que tiene a su derecha.');
    }
  }
  else{
    alert('La tabla debe de contar con al menos 1 fila.');
  }
}

function obtenerNameEvaluacion(name,indice=''){
    var res='';
    name=name.split('_');
    var x=buscarIndice(name,'bloque');
    name[x]=cambiarIndice(name[x],indice);
    return unirName(name);
}

function buscarIndice(partes,texto){
    var res=0;
    for(var i=0;i<partes.length;i++){
        var parts = partes[i].match(/(\D+)(\d*)$/);
        if(parts[1]==texto){
            res=i;
        }
    }
    return res;
}

function cambiarIndice(parte,indice=''){
    var res='';
    var parts = parte.match(/(\D+)(\d*)$/);
    if(indice===''){
        res=parts[1] + ++parts[2]
    } else {
        res=parts[1] + indice;
    }
    return res;
}

function unirName(partes){
    var res='';
    for(var i=0;i<partes.length;i++){
        if(res!=''){
            res+='_';
        }
        res+=partes[i];
    }
    return res;
}

function obtenerIdTable(id){
    var res='';
    id=id.split('_');
    var parts = id[1].match(/(\D+)(\d*)$/);
    res=id[0]+'_'+parts[1] + ++parts[2];
    return res;
}

function obtenerClickButton(funcion){
    var res='';
    var partes=funcion.split('"');
    var tabla = obtenerIdTable(partes[1]);
    res=partes[0]+'"'+tabla+'"'+partes[2];
    return res;
}