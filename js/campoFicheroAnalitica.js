$(document).ready(function(){
	$(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});

	$('#elimina-ficheroAnalitica').click(function(){
		$('#contenedor-ficheroAnalitica').remove();
		$('#contenedor-ficheroAnaliticaOculto').removeClass('hide');
		$('#ficheroAnaliticaOculto').attr('name','ficheroAnalitica');
	});

	$('#elimina-ficheroRadiologia').click(function(){
		$('#contenedor-ficheroRadiologia').remove();
		$('#contenedor-ficheroRadiologiaOculto').removeClass('hide');
		$('#ficheroRadiologiaOculto').attr('name','ficheroRadiologia');
	});
});