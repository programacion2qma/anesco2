$(document).ready(function(){
	$('#boton-codigoEmpleado').click(function(){
		$('#cajaGestion').modal({'show':true,'backdrop':'static','keyboard':false});
	});

	 $('#enviar').click(function(){
    	registraEmpleado();
    });

	 $('#boton-codigoEmpleado-2').click(function(){
		var codigoEmpleado=$('#codigoEmpleado').val();

		if(codigoEmpleado!='NULL' && codigoEmpleado!=''){
			window.open('../empleados/gestion.php?codigo='+codigoEmpleado,'_blank');
		}

	});

});


function registraEmpleado(){
	$('#enviar').html("<i class='icon-refresh icon-spin'></i> Cargando...").attr('disabled',true);

	var codigoCliente=$('#codigoCliente').val();
	var dniCreacion=$('#dniCreacion').val();
	var consulta=$.post('../listadoAjax.php?include=empleados&funcion=compruebaEmpleado();',{'codigoEmpleado':0,'dni':dniCreacion,'codigoCliente':codigoCliente});
    consulta.done(function(respuesta){ 
        if(respuesta=='error'){
            alert('Este empleado ya está registrado en está empresa.');
            $('#enviar').html("<i class='icon-check'></i> Guardar").attr('disabled',false);
        } else {
			var nombreCreacion=$('#nombreCreacion').val();
			var apellidosCreacion=$('#apellidosCreacion').val();
			var fechaNacimientoCreacion=$('#fechaNacimientoCreacion').val();
			var telefonoCreacion=$('#telefonoCreacion').val();
			var emailCreacion=$('#emailCreacion').val();
			var usuarioCreacion=limpiarCadena($('#dniCreacion').val());
			var claveCreacion=limpiarCadena($('#apellidosCreacion').val());
			var direccionCreacion=$('#direccionCreacion').val();
			var direccion2Creacion=$('#direccion2Creacion').val();
			var cpCreacion=$('#cpCreacion').val();
			var ciudadCreacion=$('#ciudadCreacion').val();
			var provinciaCreacion=$('#provinciaCreacion').val();
			var codigoPuestoTrabajoCreacion=$('#codigoPuestoTrabajoCreacion').val();
			var trabajadorSensibleCreacion=$('input[name=trabajadorSensibleCreacion]:checked').val();
			var paisCreacion=$('#paisCreacion').val();
			var movilCreacion=$('#movilCreacion').val();
			var fechaAltaCreacion=$('#fechaAltaCreacion').val();
			var objetivoCreacion=$('#objetivoCreacion').val();
			var observacionesCreacion=$('#observacionesCreacion').val();
			var consulta=$.post('../listadoAjax.php?funcion=registraEmpleadoAJAX();',{
				'nombre':nombreCreacion,
				'apellidos':apellidosCreacion,
				'dni':dniCreacion,
				'fechaNacimiento':fechaNacimientoCreacion,
				'telefono':telefonoCreacion,
				'email':emailCreacion,
				'usuario':usuarioCreacion,
				'clave':claveCreacion,
				'codigoCliente':codigoCliente,
				'direccion':direccionCreacion,
				'direccion2':direccion2Creacion,
				'cp':cpCreacion,
				'ciudad':ciudadCreacion,
				'provincia':provinciaCreacion,
				'codigoPuestoTrabajo':codigoPuestoTrabajoCreacion,
				'trabajadorSensible':trabajadorSensibleCreacion,
				'pais':paisCreacion,
				'movil':movilCreacion,
				'fechaAlta':fechaAltaCreacion,
				'objetivo':objetivoCreacion,
				'observaciones':observacionesCreacion,
			});

			consulta.done(function(codigoEmpleado){
				
				if(isNaN(codigoEmpleado)){
					alert('Ha ocurrido un error al registrar el empleado. Revise los datos introducidos y vuelva a intentarlo. Si el problema persiste, contacte con webmaster@qmaconsultores.com');
				}
				else{
					$('#codigoEmpleado').append("<option selected='selected' value='"+codigoEmpleado+"'>"+nombreCreacion+" - "+dniCreacion+"</option>");
					$('#codigoEmpleado').selectpicker('refresh');
				}
				$('#enviar').html("<i class='icon-check'></i> Guardar").attr('disabled',false);
				$('#cajaGestion').modal('hide');
			});
		}
	});
}

function limpiarCadena(cadena){
   // Definimos los caracteres que queremos eliminar
   var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";

   // Los eliminamos todos
   for (var i = 0; i < specialChars.length; i++) {
       cadena= cadena.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
   }   

   // Quitamos espacios y los sustituimos por _ porque nos gusta mas asi
   cadena = cadena.replace(/ /g,"_");

   // Quitamos acentos y "ñ". Fijate en que va sin comillas el primer parametro
   cadena = cadena.replace(/á/gi,"a");
   cadena = cadena.replace(/é/gi,"e");
   cadena = cadena.replace(/í/gi,"i");
   cadena = cadena.replace(/ó/gi,"o");
   cadena = cadena.replace(/ú/gi,"u");
   cadena = cadena.replace(/ñ/gi,"n");
   return cadena;
}