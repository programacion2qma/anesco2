$(document).ready(function(){
	oyenteCuentasPropiasFormaPago();

	$('#codigoFormaPagoOferta').change(function(){
		oyenteCuentasPropiasFormaPago();
	});
});

function oyenteCuentasPropiasFormaPago(){
	var codigoFormaPago=$('#codigoFormaPago').val();
	if(codigoFormaPago==undefined){
		codigoFormaPago=$('#codigoFormaPagoOferta').val();
	}

	if(codigoFormaPago=='NULL'){
		$('#cajaCuentaPropia').addClass('hide');
	}
	else{
		var consulta=$.post('../listadoAjax.php?funcion=compruebaFormaPagoSelectorCuentas();',{'codigoFormaPago':codigoFormaPago});
		consulta.done(function(respuesta){
			if(respuesta=='SI'){
				$('#cajaCuentaPropia').removeClass('hide');			
			}
			else{
				$('#cajaCuentaPropia').addClass('hide');
			}
		});
	}
}