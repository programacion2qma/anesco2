<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('../api/phpexcel/PHPExcel.php');
	require_once('../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../api/phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("rrmm.xlsx");
	conexionBD();
	for($i=2;$i<=200;$i++){
		$fecha=$objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue();
		$nombre=$objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue();
		$cliente=$objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue();
		$dni=$objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue();
		$edad=$objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue();
		$apto=$objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue();
		$sexo=$objPHPExcel->getActiveSheet()->getCell('G'.$i)->getValue();
		$puesto=$objPHPExcel->getActiveSheet()->getCell('H'.$i)->getValue();
		$nlab=$objPHPExcel->getActiveSheet()->getCell('I'.$i)->getValue();
		$fact=$objPHPExcel->getActiveSheet()->getCell('J'.$i)->getValue();
		$nombre=explode(',', $nombre);
		$fecha=formateaFechaBD($fecha);
		$consulta=consultaBD('SELECT codigo FROM clientes WHERE EMPNOMBRE LIKE "'.$cliente.'" OR EMPMARCA LIKE "'.$cliente.'";',false,true);
		if($consulta){
			$codigoCliente=$consulta['codigo'];
		} else {
			$id=generaNumeroReferencia('clientes','EMPID');
			$codigoCliente="INSERT INTO clientes (codigo,EMPID,EMPNOMBRE,EMPMARCA,activo,baja,eliminado) VALUES(NULL,".$id.",'".$cliente."','".$cliente."','SI','NO','NO');";
			$codigoCliente=consultaBD($codigoCliente);
			$codigoCliente=mysql_insert_id();
			$res=consultaBD('INSERT INTO usuarios (codigo,nombre,usuario,clave,tipo) VALUES (NULL,"'.$cliente.'","usuario'.$id.'","pass'.$id.'","CLIENTE")');
			$codigoUsuario=mysql_insert_id();
			$res=consultaBD("INSERT INTO usuarios_clientes VALUES(NULL,".$codigoUsuario.",".$codigoCliente.")");
		}
		$fechaNacimiento=date('Y-m-d');
		$fechaNacimiento = strtotime ( '-'.$edad.' year',strtotime($fechaNacimiento));
		$fechaNacimiento = date ( 'Y-m-d',$fechaNacimiento);
		$apto=estado($apto);
		extract($apto);
		if($puesto!=''){
			$puesto=trim($puesto);
			$consulta=consultaBD('SELECT codigo FROM puestos_trabajo WHERE nombre LIKE "'.$puesto.'";',false,true);
			if($consulta){
				$codigoPuesto=$consulta['codigo'];
			} else {
				$codigoPuesto="INSERT INTO puestos_trabajo VALUES(NULL,'".$puesto."','','','','NO','NO','NO','NO','NO','NO','NO','NO','NO','NO','NO');";
				$codigoPuesto=consultaBD($codigoPuesto);
				$codigoPuesto=mysql_insert_id();
			}
		} else {
			$codigoPuesto='NULL';
		}
		/*echo 'Fecha: '.$fecha.'<br/>';
		echo 'Nombre: '.$nombre[1].'<br/>';
		echo 'Apellidos: '.$nombre[0].'<br/>';
		echo 'Cliente: '.$cliente.'<br/>';
		echo 'Código de cliente: '.$codigoCliente.'<br/>';
		echo 'DNI: '.$dni.'<br/>';
		echo 'Edad: '.$edad.'<br/>';
		echo 'Fecha de nacimiento: '.$fechaNacimiento.'<br/>';
		echo 'Estado: '.$apto.' ('.$codigoApto.')<br/>';
		echo 'Observaciones: '.$observaciones.'<br/>';
		echo 'Sexo: '.$sexo.'<br/>';
		echo 'Puesto de trabajo: '.$puesto.'<br/>';
		echo 'Código puesto: '.$codigoPuesto.'<br/>';
		echo 'NLAB: '.$nlab.'<br/>';
		echo 'FACT: '.$fact.'<br/>';*/
		$idE=generaNumeroEmpleado($codigoCliente);
		$sqlEmpleado='INSERT INTO empleados(codigo,codigoCliente,codigoInterno,dni,nombre,apellidos,fechaNacimiento,edad,ficheroFoto,trabajadorSensible,codigoPuestoTrabajo) VALUES (NULL,'.$codigoCliente.',"'.$idE.'","'.$dni.'","'.$nombre[1].'","'.$nombre[0].'","'.$fechaNacimiento.'","'.$edad.'","NO","NO",'.$codigoPuesto.');';
		$res=consultaBD($sqlEmpleado);
		$codigoEmpleado=mysql_insert_id();
		$res=consultaBD('INSERT INTO usuarios (codigo,nombre,apellidos,dni,usuario,clave,tipo) VALUES (NULL,"'.$nombre[1].'","'.$nombre[0].'","'.$dni.'","'.$codigoCliente.$idE.'","'.$codigoCliente.$idE.'","EMPLEADO")');
		$codigoUsuario=mysql_insert_id();
		$res=consultaBD("INSERT INTO usuarios_empleados VALUES(NULL,".$codigoUsuario.",".$codigoEmpleado.")");
		$sqlHistorico='INSERT INTO historicoVS VALUES (NULL,'.$codigoEmpleado.',"'.$codigoApto.'","'.$sexo.'","'.$fecha.'","'.$nlab.'","'.$fact.'","NO","'.$observaciones.'");';
		$res=consultaBD($sqlHistorico);
		echo '-------<br/>';
	}
	cierraBD();
	/*$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('../documentos/personal/Informe.xlsx');


	// Definir headers
	header("Content-Type: application/ms-xlsx");
	header("Content-Disposition: attachment; filename=Informe.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('../documentos/personal/Informe.xlsx');*/
function estado($texto){
	$res=array('apto'=>'APTO','observaciones'=>'','codigoApto'=>'1');

	if(strpos($texto, 'NO APTO TEMPORAL')!==false){
		$res['apto']='NO APTO TEMPORAL';
		$res['codigoApto']='3';
		$res['observaciones']=trim(str_replace('NO APTO TEMPORAL', '', $texto));
	} else if(strpos($texto, 'NO APTO')!==false){
		$res['apto']='NO APTO';
		$res['codigoApto']='2';
		$res['observaciones']=trim(str_replace('NO APTO', '', $texto));
	} else if(strpos($texto, 'APTO')!==false){
		$res['apto']='APTO';
		$res['codigoApto']='1';
		$res['observaciones']=trim(str_replace('APTO', '', $texto));
	} else if(strpos($texto, 'SIN CRITERIO DE APTITUD')!==false){
		$res['apto']='SIN CRITERIO DE APTITUD';
		$res['codigoApto']='4';
		$res['observaciones']=trim(str_replace('SIN CRITERIO DE APTITUD', '', $texto));
	}
	$res['observaciones']=str_replace('.', '', $res['observaciones']);
	$res['observaciones']=str_replace(':', '', $res['observaciones']);
	return $res;
}

function generaNumeroEmpleado($cliente){
	$consulta=consultaBD("SELECT MAX(codigoInterno) AS numero FROM empleados WHERE codigoCliente=".$cliente, true, true);
	
	return $consulta['numero']+1;
}
?>