<?php
	//SE DEBE DE PONER EN LA RAÍZ DEL PROYECTO PARA QUE FUNCIONE
	session_start();
	include_once("funciones.php");
  	//compruebaSesion();

	//Carga de PHP Excel
	require_once('../api/phpexcel/PHPExcel.php');
	require_once('../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../api/phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("Informacion/Importacion_contratos/Contratos_a_06_de_enero.xlsx");
	
	
	$res=true;
	$i=3;
	$bucle=true;
	$fecha=fechaBD();

	$codigoOferta=502;	
	$codigoContrato=388;
	$codigoFactura=124;
	$codigoFormularioPRL=84;

	conexionBD();
	
	while($bucle){
		$idEmpresa=trim(addslashes($objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue()));

		if($idEmpresa!=''){

			//Extracción de datos
			$numeroContrato=trim(addslashes($objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue()));
			$fechaInicio=trim(addslashes($objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue()));
			$fechaFin=trim(addslashes($objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue()));
			$opcion=trim(addslashes($objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue()));
			$subtotal=formateaNumeroExcel(trim(addslashes($objPHPExcel->getActiveSheet()->getCell('G'.$i)->getValue())));
			$importeRM=formateaNumeroExcel(trim(addslashes($objPHPExcel->getActiveSheet()->getCell('H'.$i)->getValue())));
			$total=formateaNumeroExcel(trim(addslashes($objPHPExcel->getActiveSheet()->getCell('I'.$i)->getValue())));
			$estadoFactura=trim(addslashes($objPHPExcel->getActiveSheet()->getCell('J'.$i)->getValue()));
			$observaciones=trim(addslashes($objPHPExcel->getActiveSheet()->getCell('K'.$i)->getValue()));
			$enVigor=trim(addslashes($objPHPExcel->getActiveSheet()->getCell('L'.$i)->getValue()));
			$tecnico=trim(addslashes($objPHPExcel->getActiveSheet()->getCell('M'.$i)->getValue()));

			//Consulta de datos adicionales
			$codigoCliente=obtieneCodigoClienteDesdeID($idEmpresa,$i);
			$fechaInicio=obtieneFechaInicioExcel($fechaInicio);
			$fechaFin=obtieneFechaInicioExcel($fechaFin);
			$especialidadTecnica=obtieneEspecialidadTecnica($opcion);//Importante que vaya antes de la función obtieneOpcionContratoDesdeNombre!
			$opcion=obtieneOpcionContratoDesdeNombre($opcion);
			$codigoUsuario=obtieneCodigoUsuarioPorNombre($tecnico);
			$iva=deduceIVA($subtotal,$total);
			$importeIVA=$total-$subtotal;
			$codigoInterno=obtieneCodigoInternoDesdeNumero($numeroContrato);
			$facturaCobrada=obtieneFacturaCobradaDesdeEstado($estadoFactura);

			//Creación de oferta
			$res=$res && consultaBD("INSERT INTO ofertas(codigo,codigoCliente,fecha,rechazada,subtotal,iva,importe_iva,total,codigoInterno,aceptado,opcion,fechaInicio,fechaFin,subtotalRM,
									 mostrar,eliminado,especialidadTecnica) 
									 VALUES($codigoOferta,$codigoCliente,'$fecha','NO','$subtotal','$iva','$importeIVA','$total','$codigoInterno','SI','$opcion','$fechaInicio','$fechaFin','$importeRM',
									 'SI','NO','$especialidadTecnica');");

			if($res){
				//Creación de contrato
				$res=$res && consultaBD("INSERT INTO contratos(codigo,codigoOferta,codigoInterno,fechaInicio,fechaFin,enVigor,tecnico,eliminado,renovado)
										 VALUES($codigoContrato,'$codigoOferta','$codigoInterno','$fechaInicio','$fechaFin','$enVigor','$codigoUsuario','NO','NO');");

				if($res && $enVigor=='SI'){
					//Creación de factura proforma
					$res=consultaBD("INSERT INTO facturas(codigo,fecha,codigoCliente,tipoFactura,baseImponible,total,activo,eliminado,codigoEmisor,facturaCobrada,codigoUsuarioFactura)
					 				 VALUES($codigoFactura,'$fecha','$codigoCliente','PROFORMA','$subtotal','$total','SI','NO',1,'$facturaCobrada',".$_SESSION['codigoU'].");");

					if($res){
						$res=$res && consultaBD("INSERT INTO contratos_en_facturas VALUES(NULL,'$codigoContrato','$codigoFactura');");
					}

					$res=$res && consultaBD("INSERT INTO formularioPRL(codigo,codigoInterno,codigoContrato,fecha,aprobado,fechaAprobada,eliminado,checkVisiblePlanPrev)
										     VALUES($codigoFormularioPRL,'$codigoInterno','$codigoContrato','$fecha','SI','$fecha','NO','NO');");

				}
			}

			$codigoOferta++;
			$codigoContrato++;
			$codigoFactura++;
			$codigoFormularioPRL++;

		}
		else{
			$bucle=false;
		}
		
		$i++;
	}
	
	cierraBD();

	if($res){
		$i=$i-4;
		echo "Todo ha salido bien. Contratos importados: $i";
	}
	else{
		echo "Algo ha fallado";
	}




function obtieneCodigoClienteDesdeID($idEmpresa,$i){
	$consulta=consultaBD("SELECT codigo FROM clientes WHERE EMPID='$idEmpresa';",false,true);//A fecha del 09/02/2018, los ID de empresa son únicos

	if(!$consulta){
		echo "Paren las rotativas! Este cliente no existe: ".$idEmpresa.' - Fila: '.$i.'<br />';
	}

	return $consulta['codigo'];
}

function obtieneOpcionContratoDesdeNombre($opcion){
	$nombres=array(
		'SPA 4 Especialidades'=>1,
		'SPA Vigilancia de la salud'=>2,
		'Otras actuaciones'=>4,
		'SPA Especialidades técnicas HIGIENE'=>3,
		'SPA Especialidades técnicas'=>3
	);

	return $nombres[$opcion];
}

function obtieneEspecialidadTecnica($opcion){
	$res='';
	
	if(substr_count($opcion,'HIGIENE')>0){
		$res='Higiene Industrial';
	}

	return $res;
}

function obtieneCodigoUsuarioPorNombre($tecnico){
	$nombres=array(
		'ÁNGELES LÓPEZ'=>146,
		'PATRICIO PEÑAS'=>872,
		'ADÁN SALADO'=>145,
		'MARTA MORA'=>150
	);

	return $nombres[$tecnico];
}

function deduceIVA($subtotal,$total){
	$res='NO';

	if($subtotal!=$total){
		$res='SI';
	}

	return $res;
}


function obtieneCodigoInternoDesdeNumero($numeroContrato){
	$arrayNumero=explode('-',$numeroContrato);
	$numero=end($arrayNumero);

	return intval($numero);
}


function obtieneFechaInicioExcel($fecha){
	$res='';

	if($fecha==''){
		$res='0000-00-00';
	}
	elseif(substr_count($fecha,'/')>0){
		$res=formateaFechaBD($fecha);
	}
	else{
		$timestamp=PHPExcel_Shared_Date::ExcelToPHP($fecha);
		$res=date("Y-m-d",$timestamp);
	}

	return $res;
}

function obtieneFacturaCobradaDesdeEstado($estadoFactura){
	$res='NO';

	if($estadoFactura=='PAGADO' || $estadoFactura=='PAGADA'){
		$res='SI';
	}

	return $res;
}


function formateaNumeroExcel($numero){
	$res=str_replace('€','',$numero);

	return trim($res);
}