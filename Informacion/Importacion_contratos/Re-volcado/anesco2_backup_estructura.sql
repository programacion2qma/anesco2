SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE `accesos` (
  `codigo` int(255) NOT NULL,
  `codigoUsuario` int(255) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `ip` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `navegador` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `sistema` varchar(255) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `accidentes` (
  `codigo` int(255) NOT NULL,
  `fechaComunicacion` date NOT NULL,
  `comunicacion` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `textoComunicacion` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `codigoEmpleado` int(255) DEFAULT NULL,
  `fechaInvestigacion` date NOT NULL,
  `codigoUsuarioTecnico` int(255) DEFAULT NULL,
  `fechaAccidente` date NOT NULL,
  `horaAccidente` time NOT NULL,
  `puesto` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` text COLLATE latin1_spanish_ci NOT NULL,
  `causas` text COLLATE latin1_spanish_ci NOT NULL,
  `gradoLesion` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `fechaRecepcionInforme` date NOT NULL,
  `testigo` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkVisible` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'SI'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `actividades` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `actividades_ofertas` (
  `codigo` int(255) NOT NULL,
  `codigoOferta` int(255) NOT NULL,
  `codigoActividad` int(255) NOT NULL,
  `importe` varchar(255) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `actividades_procesos` (
  `codigo` int(255) NOT NULL,
  `codigoActividad` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `actividad_parte_diario` (
  `codigo` int(255) NOT NULL,
  `codigoContrato` int(255) NOT NULL,
  `actividad` text COLLATE latin1_spanish_ci NOT NULL,
  `minutos` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `codigoParte` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `actuaciones_cliente` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `toma_datos` tinyint(1) NOT NULL DEFAULT '0',
  `generacion_documentos` tinyint(1) NOT NULL DEFAULT '0',
  `declaracion_ficheros` tinyint(1) NOT NULL DEFAULT '0',
  `auditoria` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `aplicaciones_cliente` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `nombre` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `finalidad` varchar(200) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `asistencia_a_inspecciones` (
  `codigo` int(255) NOT NULL,
  `fecha` date NOT NULL,
  `codigoCliente` int(255) DEFAULT NULL,
  `codigoUsuario` int(255) DEFAULT NULL,
  `tipoAsistencia` varchar(50) NOT NULL,
  `observaciones` longtext NOT NULL,
  `horasDedicadas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `auditorias_internas` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `lugar` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `fechaAuditoria` date NOT NULL,
  `email` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `auditor` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `cuestionario` longtext COLLATE latin1_spanish_ci NOT NULL,
  `finalizado` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `bie_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoFormularioPRL` int(255) NOT NULL,
  `numero` varchar(255) NOT NULL,
  `medida` enum('24','45') NOT NULL,
  `presion` varchar(255) NOT NULL,
  `senializado` enum('SI','NO') NOT NULL,
  `revisado` enum('SI','NO') NOT NULL,
  `ubicacion` varchar(255) NOT NULL,
  `codigoCentroTrabajo` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `botiquines_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoFormularioPRL` int(255) NOT NULL,
  `ubicacion` text NOT NULL,
  `senializado` enum('SI','NO') NOT NULL,
  `numero` int(50) NOT NULL,
  `revisado` enum('NO','SI') NOT NULL,
  `codigoCentroTrabajo` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `centros_medicos` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `observaciones` text NOT NULL,
  `eliminado` enum('NO','SI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `centros_prl` (
  `codigo` int(255) NOT NULL,
  `codigoTrabajo` int(255) NOT NULL,
  `nombre` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `delegado` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `comite` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `trabajador` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `consulta` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `citas` (
  `codigo` int(255) NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFin` date NOT NULL,
  `horaInicio` time NOT NULL,
  `horaFin` time NOT NULL,
  `estado` enum('SINCONFIRMAR','CONFIRMADA') NOT NULL,
  `codigoEmpleado` int(255) DEFAULT NULL,
  `codigoSala` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `clientes` (
  `codigo` int(255) NOT NULL,
  `fecha` date NOT NULL,
  `activo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `EMPID` int(255) NOT NULL,
  `EMPCIF` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `EMPNOMBRE` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `EMPMARCA` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `EMPNTRAB` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `EMPDIR` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `EMPLOC` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `EMPCP` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `EMPPROV` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `EMPPC` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `EMPRLTEL` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `EMPPCTEL` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `EMPRLEMAIL` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `EMPPCEMAIL` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `EMPMUTUA` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `EMPASESORIA` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `EMPACTIVIDAD` int(255) DEFAULT NULL,
  `EMPCNAE` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `EMPANEXO` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `baja` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `EMPRL` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `EMPRLDNI` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `EMPRLCARGO` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `EMPTELPRINC` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `EMPEMAILPRINC` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `EMPCENTROS` int(255) NOT NULL,
  `comercial` int(255) DEFAULT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `EMPIBAN` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `bic` varchar(11) COLLATE latin1_spanish_ci NOT NULL,
  `eliminado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `ficheroLogo` varchar(255) COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `clientes_centros` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `cp` varchar(10) NOT NULL,
  `localidad` varchar(255) NOT NULL,
  `provincia` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cobros_facturas` (
  `codigo` int(255) NOT NULL,
  `codigoFactura` int(255) DEFAULT NULL,
  `codigoFormaPago` int(255) DEFAULT NULL,
  `fechaCobro` date NOT NULL,
  `importe` double(20,2) NOT NULL,
  `codigoCuentaPropia` int(255) DEFAULT NULL,
  `codigoRemesa` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `colaboradores` (
  `codigo` int(255) NOT NULL,
  `razonSocial` varchar(255) NOT NULL,
  `cif` varchar(9) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `cp` varchar(5) NOT NULL,
  `localidad` varchar(255) NOT NULL,
  `provincia` varchar(100) NOT NULL,
  `contacto` varchar(255) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `comision` int(20) NOT NULL,
  `eliminado` enum('NO','SI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `contratos` (
  `codigo` int(255) NOT NULL,
  `codigoOferta` int(255) NOT NULL,
  `codigoInterno` int(255) NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFin` date NOT NULL,
  `enVigor` enum('SI','NO') NOT NULL,
  `horasPrevistas` varchar(255) NOT NULL,
  `tecnico` int(255) DEFAULT NULL,
  `eliminado` enum('NO','SI') NOT NULL,
  `renovado` enum('NO','SI') NOT NULL,
  `checkNoRenovar` enum('SI','NO') NOT NULL DEFAULT 'NO',
  `contratoRevisado` enum('NO','SI') NOT NULL,
  `codigoFormaPago` int(255) DEFAULT NULL,
  `codigoCuentaPropiaContrato` int(255) DEFAULT NULL,
  `codigoColaborador` int(255) DEFAULT NULL,
  `observaciones` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `contratos_en_facturas` (
  `codigo` int(255) NOT NULL,
  `codigoContrato` int(255) DEFAULT NULL,
  `codigoFactura` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `contratos_renovaciones` (
  `codigo` int(255) NOT NULL,
  `codigoContrato` int(255) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `copias_seguridad` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `periocidad` enum('DIARIA','SEMANAL','MENSUAL') COLLATE latin1_spanish_ci DEFAULT NULL,
  `encargado_realizar` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `encargado_custodiar` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `lugar_almacenamiento` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `cd_dvd` enum('SI','NO') COLLATE latin1_spanish_ci DEFAULT 'NO',
  `usb` enum('SI','NO') COLLATE latin1_spanish_ci DEFAULT 'NO',
  `disco_duro` enum('SI','NO') COLLATE latin1_spanish_ci DEFAULT 'NO',
  `disco_duro_externo` enum('SI','NO') COLLATE latin1_spanish_ci DEFAULT 'NO',
  `nube` enum('SI','NO') COLLATE latin1_spanish_ci DEFAULT 'NO',
  `otros` enum('SI','NO') COLLATE latin1_spanish_ci DEFAULT 'NO',
  `otros_soporte` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `correos` (
  `codigo` int(11) NOT NULL,
  `destinatarios` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `asunto` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` text COLLATE latin1_spanish_ci NOT NULL,
  `ficheroAdjunto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `codigoUsuario` int(255) NOT NULL,
  `tipo` varchar(100) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `cuentas_propias` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `razonSocial` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `cif` varchar(9) COLLATE latin1_spanish_ci NOT NULL,
  `iban` varchar(24) COLLATE latin1_spanish_ci NOT NULL,
  `bic` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `sufijo` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `referenciaAcreedor` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `activo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `declaraciones` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `fecha` date NOT NULL,
  `datosEncargado` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `identificador` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `numeroRegistro` varchar(30) COLLATE latin1_spanish_ci NOT NULL,
  `razon_s` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `cif_nif` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `nombre` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `apellido1` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `apellido2` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `nif` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `cargo` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `dir_postal` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `provincia` varchar(3) COLLATE latin1_spanish_ci DEFAULT NULL,
  `localidad` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `postal` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `telefono` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `fax` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `email` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `cif_responsableFichero` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `dir_postal_responsableFichero` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `provincia_responsableFichero` varchar(3) COLLATE latin1_spanish_ci DEFAULT NULL,
  `localidad_responsableFichero` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `postal_responsableFichero` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `telefono_responsableFichero` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `fax_responsableFichero` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `email_responsableFichero` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `n_razon` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `cap` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `datosDerecho` varchar(2) COLLATE latin1_spanish_ci DEFAULT NULL,
  `oficina` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `nif_cif` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `dir_postal_derecho` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `provincia_derecho` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `localidad_derecho` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `postal_derecho` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `telefono_derecho` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `fax_derecho` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `email_derecho` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `n_razon_encargado` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `cif_nif_encargado` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `dir_postal_encargado` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `provincia_encargado` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `localidad_encargado` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `postal_encargado` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `telefono_encargado` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `fax_encargado` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `email_encargado` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `fichero` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `desc_fin_usos` text COLLATE latin1_spanish_ci,
  `checkFinalidad0` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad1` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad2` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad3` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad4` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad5` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad6` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad7` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad8` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad9` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad10` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad11` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad12` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad13` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad14` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad15` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad16` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad17` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad18` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad19` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad20` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad21` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad22` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad23` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkFinalidad24` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkOrigen0` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkOrigen1` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkOrigen2` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkOrigen3` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkOrigen4` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkOrigen5` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkColectivo0` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkColectivo1` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkColectivo2` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkColectivo3` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkColectivo4` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkColectivo5` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkColectivo6` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkColectivo7` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkColectivo8` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkColectivo9` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkColectivo10` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkColectivo11` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkColectivo12` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkColectivo13` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `otro_col` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `nivel` varchar(1) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosProtegidos0` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosProtegidos1` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosProtegidos2` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosProtegidos3` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosEspeciales0` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosEspeciales1` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosEspeciales2` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosIdentificativos0` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosIdentificativos1` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosIdentificativos2` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosIdentificativos3` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosIdentificativos4` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosIdentificativos5` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosIdentificativos6` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosIdentificativos7` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosIdentificativos8` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosIdentificativos9` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosIdentificativos10` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosIdentificativos11` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDatosIdentificativos12` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `ODCI` text COLLATE latin1_spanish_ci,
  `checkOtrosDatos0` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkOtrosDatos1` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkOtrosDatos2` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkOtrosDatos3` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkOtrosDatos4` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkOtrosDatos5` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkOtrosDatos6` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkOtrosDatos7` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `desc_otros_tipos` text COLLATE latin1_spanish_ci,
  `tratamiento` varchar(2) COLLATE latin1_spanish_ci NOT NULL,
  `checkDestinatariosCesiones0` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones1` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones2` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones3` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones4` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones5` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones6` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones7` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones8` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones9` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones10` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones11` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones12` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones13` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones14` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones15` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones16` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones17` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones18` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones19` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones20` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `checkDestinatariosCesiones21` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `desc_otros` text COLLATE latin1_spanish_ci,
  `revision` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `nuevaAuditoria` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `detalles_cliente` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `actividad_principal` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `cnae` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `web` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `facebook` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `responsable_seguridad` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `nif_responsable` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `diferente_arco` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `direccion_arco` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `poblacion_arco` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `provincia_arco` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `cp_arco` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `email_arco` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `clausula_videovigilancia` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `clausula_trabajadores` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `clausula_trabajadorespracticas` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `clausula_cv` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `clausula_contratosclientes` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `clausula_derechosimagen` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `clausula_datosinternacionales` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `clausula_accesodatos` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `documentosvs` (
  `codigo` int(255) NOT NULL,
  `codigoContrato` int(255) NOT NULL,
  `tipoDocumento` int(255) NOT NULL,
  `formulario` text COLLATE utf8_unicode_ci NOT NULL,
  `enviado` enum('SI','NO') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO',
  `fechaCreacion` date NOT NULL,
  `fechaEnvio` date NOT NULL,
  `visible` enum('SI','NO') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `documentos_clientes` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `fichero` varchar(255) NOT NULL,
  `pertenece` enum('ADMON','TECNICO','VS') DEFAULT NULL,
  `codigoDocumento` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `area` varchar(255) NOT NULL,
  `visible` enum('SI','NO') NOT NULL DEFAULT 'SI'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `documentos_info` (
  `codigo` int(255) NOT NULL,
  `codigoFormulario` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `fichero` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `documentos_plan_prev` (
  `codigo` int(255) NOT NULL,
  `codigoFormulario` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `fichero` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `emisores` (
  `codigo` int(255) NOT NULL,
  `razonSocial` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `cif` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `domicilio` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `cp` varchar(7) COLLATE latin1_spanish_ci NOT NULL,
  `localidad` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `provincia` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `iban` varchar(25) COLLATE latin1_spanish_ci NOT NULL,
  `prefijo` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `ficheroLogo` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `telefono` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `fax` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `web` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `registro` text COLLATE latin1_spanish_ci NOT NULL,
  `referenciaAcreedor` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `activo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `empleados` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `codigoInterno` int(255) NOT NULL,
  `dni` varchar(20) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `edad` int(11) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `direccion2` varchar(255) NOT NULL,
  `cp` int(20) NOT NULL,
  `ciudad` varchar(255) NOT NULL,
  `provincia` varchar(255) NOT NULL,
  `pais` varchar(255) NOT NULL,
  `movil` varchar(20) NOT NULL,
  `fechaAlta` date NOT NULL,
  `objetivo` text NOT NULL,
  `observaciones` text NOT NULL,
  `ficheroFoto` varchar(255) NOT NULL,
  `trabajadorSensible` enum('NO','SI') NOT NULL,
  `codigoPuestoTrabajo` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `empleados_prl` (
  `codigo` int(255) NOT NULL,
  `codigoTrabajo` int(255) NOT NULL,
  `nombre` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `dni` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `puesto` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `funciones` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `salen` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `vehiculo` varchar(255) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `empleados_sensibles_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoFormularioPRL` int(255) DEFAULT NULL,
  `codigoEmpleado` int(255) DEFAULT NULL,
  `codigoCentroTrabajo` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `epis` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `epis_de_puesto_trabajo` (
  `codigo` int(255) NOT NULL,
  `codigoPuestoTrabajo` int(255) DEFAULT NULL,
  `codigoEPI` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `epis_en_reconocimiento_medico` (
  `codigo` int(255) NOT NULL,
  `codigoReconocimientoMedico` int(255) DEFAULT NULL,
  `codigoEPI` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `estados_recibos` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `observaciones` text NOT NULL,
  `eliminado` enum('NO','SI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `evaluacion_general` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) DEFAULT NULL,
  `fechaEvaluacion` date NOT NULL,
  `graficoBarraGlobal` longtext COLLATE latin1_spanish_ci NOT NULL,
  `ficheroEvaluacion` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tipoEvaluacion` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `codigoPuestoTrabajo` int(255) DEFAULT NULL,
  `otros` tinytext COLLATE latin1_spanish_ci NOT NULL,
  `checkActivo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'SI',
  `checkVisible` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'SI'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `extintores_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoFormularioPRL` int(255) NOT NULL,
  `numero` varchar(255) NOT NULL,
  `kg` double NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `eficacia` varchar(255) NOT NULL,
  `senializado` enum('SI','NO') NOT NULL,
  `revisado` enum('SI','NO') NOT NULL,
  `ubicacion` varchar(255) NOT NULL,
  `codigoCentroTrabajo` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `facturas` (
  `codigo` int(255) NOT NULL,
  `codigoEmisor` int(255) DEFAULT NULL,
  `fecha` date NOT NULL,
  `codigoSerieFactura` int(255) DEFAULT NULL,
  `numero` int(255) NOT NULL,
  `codigoCliente` int(255) DEFAULT NULL,
  `tipoFactura` enum('NORMAL','ABONO','PROFORMA') COLLATE latin1_spanish_ci NOT NULL,
  `baseImponible` double(20,2) NOT NULL,
  `total` double(20,2) NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `comentariosLlamada` text COLLATE latin1_spanish_ci NOT NULL,
  `activo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `codigoFacturaAsociada` int(255) DEFAULT NULL,
  `facturaCobrada` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `informeEnviado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `eliminado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `facturas_reconocimientos_medicos` (
  `codigo` int(255) NOT NULL,
  `codigoEmisor` int(255) DEFAULT NULL,
  `fecha` date NOT NULL,
  `fechaRRMM` date NOT NULL,
  `codigoSerieFactura` int(255) DEFAULT NULL,
  `numero` int(255) NOT NULL,
  `facturaContratos` enum('SI','NO') NOT NULL,
  `codigoContrato` int(255) DEFAULT NULL,
  `conceptoManualFactura` text NOT NULL,
  `codigoClienteParaConceptoManual` int(255) DEFAULT NULL,
  `total` double(20,2) NOT NULL,
  `observaciones` text NOT NULL,
  `facturaCobrada` enum('NO','SI') NOT NULL,
  `eliminado` enum('NO','SI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ficheros_cliente` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `clientes_y_proveedores` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `nominas_personal` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `videovigilancia` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `contacto_web` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `historial_clinico` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `arrendatarios` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `renta` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `gestion_vehiculos` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `comunidades` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `correduria_seguros` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `formas_pago` (
  `codigo` int(255) NOT NULL,
  `forma` varchar(255) NOT NULL,
  `numPagos` int(100) NOT NULL,
  `remesable` enum('NO','SI') NOT NULL,
  `mostrarCuentas` enum('NO','SI') NOT NULL,
  `eliminado` enum('NO','SI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `formularioPRL` (
  `codigo` int(255) NOT NULL,
  `codigoInterno` int(255) NOT NULL,
  `codigoContrato` int(255) DEFAULT NULL,
  `fecha` date NOT NULL,
  `aprobado` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `fechaAprobada` date NOT NULL,
  `visitada` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `fechaVisita` date NOT NULL,
  `numVisitas` int(11) NOT NULL,
  `planPrev` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `fechaPlanPrev` date NOT NULL,
  `pap` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `fechaPap` date NOT NULL,
  `info` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `fechaInfo` date NOT NULL,
  `memoria` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `fechaMemoria` date NOT NULL,
  `vs` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `fechaVs` date NOT NULL,
  `mail` varchar(255) COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `fechaMail` date NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `mesVencimiento` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fechaVisitaReal` date DEFAULT NULL,
  `fechaPlanPrevReal` date DEFAULT NULL,
  `fechaPapReal` date DEFAULT NULL,
  `fechaInfoReal` date DEFAULT NULL,
  `fechaMemoriaReal` date DEFAULT NULL,
  `fechaVsReal` date NOT NULL,
  `formulario` longtext COLLATE latin1_spanish_ci NOT NULL,
  `horasPrevistas` varchar(255) COLLATE latin1_spanish_ci NOT NULL DEFAULT '0',
  `horasVisita` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `horasPlanPrev` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `horasPap` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `horasInfo` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `horasMemoria` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `horasVs` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `totalHoras` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `eliminado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `codigoUsuarioTecnico` int(255) DEFAULT NULL,
  `ficheroOrganigrama` varchar(255) COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `checkVisiblePlanPrev` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'SI'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `fotos_accidentes` (
  `codigo` int(255) NOT NULL,
  `fichero` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `codigoAccidente` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `funciones_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoFormularioPRL` int(255) NOT NULL,
  `funciones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dni` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `codigoCentroTrabajo` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `gastos_parte_diario` (
  `codigo` int(255) NOT NULL,
  `codigoContratoGastos` int(255) NOT NULL,
  `itinerario` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `kilometraje` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `importe` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `peaje` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `taxi` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `parking` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `dietas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `otros` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `total` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `observacionesGastos` text COLLATE latin1_spanish_ci NOT NULL,
  `estado` enum('PENDIENTE','PAGADO') COLLATE latin1_spanish_ci NOT NULL,
  `codigoParte` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `gastos_tarea` (
  `codigo` int(255) NOT NULL,
  `codigoContratoGastos` int(255) NOT NULL,
  `itinerario` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `kilometraje` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `importe` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `peaje` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `taxi` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `parking` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `dietas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `otros` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `total` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `observacionesGastos` text COLLATE latin1_spanish_ci NOT NULL,
  `estado` enum('PENDIENTE','PAGADO') COLLATE latin1_spanish_ci NOT NULL,
  `codigoTarea` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `gestion_evaluacion_general` (
  `codigo` int(255) NOT NULL,
  `fecha` date NOT NULL,
  `graficoCircular` text NOT NULL,
  `graficoBarra` text NOT NULL,
  `documentacion` varchar(255) NOT NULL,
  `codigoEvaluacion` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `gestorias_cliente` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `razon_social_gestoria` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `cif_gestoria` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `domicilio_gestoria` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `telefono_gestoria` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `email_gestoria` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `responsable_gestoria` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `laboral_gestoria` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `contable_gestoria` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `fiscal_gestoria` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `juridica_gestoria` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `historicoVS` (
  `codigo` int(255) NOT NULL,
  `codigoEmpleado` int(255) NOT NULL,
  `estado` int(255) NOT NULL,
  `sexo` enum('V','M') COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `nlab` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `eliminado` enum('SI','NO') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO',
  `observaciones` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `incidencias` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `descripcion` text COLLATE latin1_spanish_ci NOT NULL,
  `departamento` enum('ADMINISTRACION','COMERCIAL','TECNICO') COLLATE latin1_spanish_ci NOT NULL,
  `prioridad` enum('ALTA','NORMAL','BAJA') COLLATE latin1_spanish_ci NOT NULL,
  `estado` enum('RESUELTA','PENDIENTE','ENPROCESO') COLLATE latin1_spanish_ci NOT NULL,
  `fechaResolucion` date NOT NULL,
  `persona` varchar(255) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `informaticos_cliente` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `razon_social_informatico` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `cif_informatico` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `domicilio_informatico` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `telefono_informatico` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `email_informatico` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `responsable_informatico` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `acceso_remoto_informatico` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `mantenimiento_sistema_informatico` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `mantenimiento_aplicaciones_informatico` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `mantenimiento_web_informatico` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `informes` (
  `codigo` int(255) NOT NULL,
  `fecha` date NOT NULL,
  `historico` text COLLATE latin1_spanish_ci NOT NULL,
  `codigoEvaluacion` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `instalaciones` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `eliminado` enum('NO','SI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `instalaciones_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoFormularioPRL` int(255) DEFAULT NULL,
  `codigoInstalacion` int(255) DEFAULT NULL,
  `codigoCentroTrabajo` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `locales_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoFormularioPRL` int(255) NOT NULL,
  `ubicacion` text NOT NULL,
  `senializado` enum('SI','NO') NOT NULL,
  `numero` int(50) NOT NULL,
  `revisado` enum('NO','SI') NOT NULL,
  `codigoCentroTrabajo` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `materias_primas_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `materia` varchar(255) NOT NULL,
  `lugar` varchar(255) NOT NULL,
  `cantidad` varchar(255) NOT NULL,
  `codigoFormularioPRL` int(255) DEFAULT NULL,
  `codigoCentroTrabajo` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `medidas_correctoras_accidentes` (
  `codigo` int(255) NOT NULL,
  `medidas` text COLLATE latin1_spanish_ci NOT NULL,
  `plazo` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `verificacion` text COLLATE latin1_spanish_ci NOT NULL,
  `codigoAccidente` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `medidas_riesgos_evaluacion_general` (
  `codigo` int(255) NOT NULL,
  `area` varchar(255) DEFAULT NULL,
  `responsable` varchar(255) NOT NULL,
  `recomendacion` text NOT NULL,
  `plazo` enum('0','3','6','12','13') NOT NULL,
  `otroPlazo` varchar(255) NOT NULL,
  `checkEjecutada` enum('SI','NO') NOT NULL DEFAULT 'NO',
  `codigoGestion` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `medios_humanos_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoFormularioPRL` int(255) NOT NULL,
  `ubicacion` text NOT NULL,
  `numero` int(50) NOT NULL,
  `revisado` enum('NO','SI') NOT NULL,
  `senializado` enum('NO','SI') NOT NULL,
  `codigoCentroTrabajo` int(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ofertas` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) DEFAULT NULL,
  `fecha` date NOT NULL,
  `rechazada` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `fechaRechazada` date NOT NULL,
  `motivosRechazo` tinytext COLLATE latin1_spanish_ci NOT NULL,
  `subtotal` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `iva` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `importe_iva` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `total` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `codigoInterno` int(11) NOT NULL,
  `aceptado` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO',
  `opcion` enum('1','2','3','4') COLLATE latin1_spanish_ci NOT NULL DEFAULT '1',
  `otraOpcion` text COLLATE latin1_spanish_ci NOT NULL,
  `codigoTarifa` int(255) DEFAULT NULL,
  `otraTarifa` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFin` date NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `importeRM` double(20,2) NOT NULL,
  `numRM` int(10) NOT NULL,
  `subtotalRM` double(20,2) NOT NULL,
  `importeRMExtra` double(20,2) NOT NULL,
  `mostrar` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'SI',
  `eliminado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `especialidadTecnica` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fechaInicioOferta` date NOT NULL,
  `fechaFinOferta` date NOT NULL,
  `codigoFormaPagoOferta` int(255) DEFAULT NULL,
  `codigoCuentaPropiaOferta` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `ofertas_ficheros` (
  `codigo` int(255) NOT NULL,
  `codigoOferta` int(255) DEFAULT NULL,
  `nombre` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fichero` varchar(255) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `parte_diario` (
  `codigo` int(255) NOT NULL,
  `fecha` date NOT NULL,
  `gastos` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `codigoUsuario` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `prl_cliente` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `razon_social_prl` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `cif_prl` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `domicilio_prl` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `telefono_prl` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `email_prl` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `responsable_prl` varchar(200) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `profesionales_centros` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `observaciones` text NOT NULL,
  `codigoCentroMedico` int(255) DEFAULT NULL,
  `eliminado` enum('NO','SI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `puestos_trabajo` (
  `codigo` int(255) NOT NULL,
  `nombre` tinytext NOT NULL,
  `descripcion` text NOT NULL,
  `tareas` text NOT NULL,
  `zonaTrabajo` tinytext NOT NULL,
  `checkClasificacionPuesto0` enum('NO','SI') NOT NULL,
  `checkClasificacionPuesto1` enum('NO','SI') NOT NULL,
  `checkClasificacionPuesto2` enum('NO','SI') NOT NULL,
  `checkClasificacionPuesto3` enum('NO','SI') NOT NULL,
  `checkClasificacionPuesto4` enum('NO','SI') NOT NULL,
  `checkClasificacionPuesto5` enum('NO','SI') NOT NULL,
  `checkClasificacionPuesto6` enum('NO','SI') NOT NULL,
  `checkClasificacionPuesto7` enum('NO','SI') NOT NULL,
  `checkClasificacionPuesto8` enum('NO','SI') NOT NULL,
  `checkClasificacionPuesto9` enum('NO','SI') NOT NULL,
  `checkClasificacionPuesto10` enum('NO','SI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `puestos_trabajo_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoPuestoTrabajo` int(255) DEFAULT NULL,
  `codigoFormularioPRL` int(255) DEFAULT NULL,
  `sensible` enum('SI','NO') NOT NULL,
  `codigoCentroTrabajo` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `reconocimientos_medicos` (
  `codigo` int(255) NOT NULL,
  `codigoEmpleado` int(255) DEFAULT NULL,
  `codigoContrato` int(255) DEFAULT NULL,
  `bloqueado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion0` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion1` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion2` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion3` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion4` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion5` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion6` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion7` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion8` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion9` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion10` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tipoReconocimiento` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `periodicidad` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `puestoAnterior` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tiempoPuestoAnterior` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `riesgosExtralaborales` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `numero` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `riesgoPuesto` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `sexo` varchar(1) COLLATE latin1_spanish_ci NOT NULL,
  `talla` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `peso` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `imc` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `sistolica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `diastolica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `frecuenciaCardiaca` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `alcohol` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tipoFumador` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `aniosFumando` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tabacoDia` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `cafeDia` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tratamientoHabitual` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkVacunaciones0` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkVacunaciones1` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkVacunaciones2` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkVacunaciones3` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `ejercicio` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `alimentacion` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `calidadSuenio` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `historiaActual` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `medicacionHabitual` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `trastornosCongenitos` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fracturas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `riesgosLaborales` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `antecedentesPersonales` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `alergias` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `toleraEpis` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `antecedentesFamiliares` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `minusvaliaReconocida` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkEpis` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `bajasProlongadas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `inspeccionGeneral` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `orl` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `oftalmologia` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `exploracionNeurologica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `pielMucosas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `auscultacionPulmonar` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `aparatoDigestivo` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `extremidades` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `auscultacionCardiaca` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `sistemaUrogenital` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `aparatoLocomotor` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `otros` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkAgudezaVisual` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `agudeza` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `observacionesVision` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `agudezaVisualCorregida` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `traeGafasLentillas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tipoGafasLentillas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `anioUltimaGraduacion` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `frecuenciaUso` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `percepcionColores` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision1` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision2` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision3` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision4` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision5` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision6` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision7` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision8` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision9` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision10` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision11` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision12` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkEspirometria` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `valoracionEspirometrica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `cv` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fvc` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fev1` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `itiff` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkAudiometria` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `audiometria` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `observacionesAudiometria` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria1` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria2` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria3` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria4` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria5` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria6` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria7` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria8` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria9` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria10` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria11` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria12` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria13` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria14` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkElectrocardiograma` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `electrocardiograma` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkHemograma` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `hemograma` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkBioquimica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `bioquimica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkOtrasPruebas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `otrasPruebas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkOtrasPruebasFuncionales` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `otrasPruebasFuncionales` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkOrina` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `orina` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `analitica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `ficheroAnalitica` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `checkRadiologia` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `radiologia` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkDinamometria` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `dinamometria` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `rxTorax` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `apto` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkObjetivan` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `debeAcudirMedico` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `profesionalExterno` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `observacionesNoImprimibles` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `recomendaciones` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `eliminado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `cerrado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `editable` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `codigoUsuario` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `reconocimientos_medicos_en_facturas` (
  `codigo` int(255) NOT NULL,
  `codigoEmpleado` int(255) DEFAULT NULL,
  `incluidoContrato` enum('SI','NO') NOT NULL,
  `precio` double(20,2) NOT NULL,
  `codigoFacturaReconocimientoMedico` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `remesas` (
  `codigo` int(255) NOT NULL,
  `formato` enum('CORE','COR1','B2B') COLLATE latin1_spanish_ci NOT NULL,
  `financiado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `mismoVencimiento` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `codigoCuentaPropia` int(255) DEFAULT NULL,
  `fecha` date NOT NULL,
  `total` double(20,2) NOT NULL,
  `fechaVencimiento` date NOT NULL,
  `estado` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `eliminado` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `representantes_cliente` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `nombre` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `dni` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `riesgos` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tipo` int(255) DEFAULT NULL,
  `codigoInterno` varchar(255) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `riesgos_evaluacion_general` (
  `codigo` int(255) NOT NULL,
  `codigoRiesgo` int(255) DEFAULT NULL,
  `descripcionRiesgo` text COLLATE latin1_spanish_ci NOT NULL,
  `probabilidad` enum('1','2','3','4') COLLATE latin1_spanish_ci DEFAULT NULL,
  `consecuencias` enum('1','2','3','4') COLLATE latin1_spanish_ci DEFAULT NULL,
  `prioridad` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `total` int(2) NOT NULL,
  `graficoBarra` text COLLATE latin1_spanish_ci NOT NULL,
  `codigoEvaluacion` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `riesgos_tipos` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `salas_centros_medicos` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `codigoCentroMedico` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `satisfaccion` (
  `codigo` int(255) NOT NULL,
  `cliente` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `pregunta1` enum('1','2','3','4','5') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta2` enum('1','2','3','4','5') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta3` enum('1','2','3','4','5') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta4` enum('1','2','3','4','5') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta5` enum('1','2','3','4','5') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta6` enum('1','2','3','4','5') COLLATE latin1_spanish_ci NOT NULL,
  `comentarios` varchar(500) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `sentencias_sql` (
  `codigo` int(255) NOT NULL,
  `codigoUsuario` int(255) DEFAULT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `sentencia` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `series_facturas` (
  `codigo` int(255) NOT NULL,
  `serie` varchar(2) COLLATE latin1_spanish_ci NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `activo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `codigoEmisorSerie` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `tareas` (
  `codigo` int(255) NOT NULL,
  `tarea` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFin` date NOT NULL,
  `horaInicio` time NOT NULL,
  `horaFin` time NOT NULL,
  `todoDia` enum('true','false') COLLATE latin1_spanish_ci NOT NULL,
  `estado` enum('PENDIENTE','REALIZADA') COLLATE latin1_spanish_ci NOT NULL,
  `prioridad` enum('ALTA','NORMAL','BAJA') COLLATE latin1_spanish_ci NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `codigoUsuario` int(255) DEFAULT NULL,
  `codigoCliente` int(255) DEFAULT NULL,
  `tiempoEmpleado` int(255) NOT NULL,
  `codigoContrato` int(255) DEFAULT NULL,
  `gastos` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `tareas_parte_diario` (
  `codigo` int(255) NOT NULL,
  `codigoContrato` int(255) DEFAULT NULL,
  `codigoTarea` int(255) DEFAULT NULL,
  `codigoParteDiario` int(255) DEFAULT NULL,
  `tiempo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tarifas` (
  `codigo` int(255) NOT NULL,
  `codigoInterno` int(255) NOT NULL,
  `nombre` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `trabajador1` double NOT NULL,
  `trabajador2` double NOT NULL,
  `trabajador3` double NOT NULL,
  `trabajador4` double NOT NULL,
  `trabajador5` double NOT NULL,
  `trabajador6` double NOT NULL,
  `trabajador7` double NOT NULL,
  `trabajador8` double NOT NULL,
  `trabajador9` double NOT NULL,
  `trabajador10` double NOT NULL,
  `trabajador11` double NOT NULL,
  `trabajador12` double NOT NULL,
  `trabajador13` double NOT NULL,
  `trabajador14` double NOT NULL,
  `trabajador15` double NOT NULL,
  `trabajador16` double NOT NULL,
  `trabajador17` double NOT NULL,
  `trabajador18` double NOT NULL,
  `trabajador19` double NOT NULL,
  `trabajador20` double NOT NULL,
  `sola` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `tipos_tareas` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `eliminado` enum('NO','SI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `usuarios` (
  `codigo` int(255) NOT NULL,
  `nombre` text COLLATE latin1_spanish_ci NOT NULL,
  `apellidos` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `dni` varchar(12) COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `telefono` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `usuario` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `clave` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `tipo` enum('ADMIN','COMERCIAL','ADMINISTRACION','CONSULTORIA','FORMACION','ATENCION','TELECONCERTADOR','TECNICO','COLABORADOR','ASESOR','CLIENTE','EMPLEADO','FACTURACION','MEDICO','ENFERMERIA') COLLATE latin1_spanish_ci NOT NULL,
  `firmaCorreo` text COLLATE latin1_spanish_ci,
  `director` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `habilitado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `colorTareas` varchar(7) COLLATE latin1_spanish_ci NOT NULL,
  `directorAsociado` int(255) DEFAULT NULL,
  `teleconcertador` int(255) DEFAULT NULL,
  `tipoEspecialista` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `numeroColegiado` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `ficheroFirma` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `sesion` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `usuarios_clientes` (
  `codigo` int(255) NOT NULL,
  `codigoUsuario` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `usuarios_empleados` (
  `codigo` int(255) NOT NULL,
  `codigoUsuario` int(255) NOT NULL,
  `codigoEmpleado` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `vencimientos_en_remesas` (
  `codigo` int(255) NOT NULL,
  `codigoRemesa` int(255) DEFAULT NULL,
  `codigoVencimiento` int(255) DEFAULT NULL,
  `importe` double(20,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `vencimientos_facturas` (
  `codigo` int(255) NOT NULL,
  `codigoFactura` int(255) DEFAULT NULL,
  `codigoFormaPago` int(255) DEFAULT NULL,
  `fechaVencimiento` date NOT NULL,
  `fechaDevolucion` date NOT NULL,
  `gastosDevolucion` double(20,2) NOT NULL,
  `importe` double(20,2) NOT NULL,
  `concepto` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `llamadaAsesoria` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `llamadaEmpresa` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `motivoDevolucion` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `eliminado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `codigoEstadoRecibo` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `vigilancia` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `codigoEmpleado` int(255) DEFAULT NULL,
  `codigoInterno` int(255) NOT NULL,
  `fecha` date NOT NULL,
  `aptitud` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `ficheroAptitud` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `exploracion` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `constitucion` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `frecuenciaRespiratoria` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `frecuenciaCardiaca` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `peso` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `talla` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `imc` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `PA` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `exploracionCabeza` text COLLATE latin1_spanish_ci NOT NULL,
  `alteracionesMotoras` text COLLATE latin1_spanish_ci NOT NULL,
  `alteracionesSensitivas` text COLLATE latin1_spanish_ci NOT NULL,
  `alteracionesMarcha` text COLLATE latin1_spanish_ci NOT NULL,
  `alteracionesEquilibrio` text COLLATE latin1_spanish_ci NOT NULL,
  `dismetrias` text COLLATE latin1_spanish_ci NOT NULL,
  `alteracionesReflejos` text COLLATE latin1_spanish_ci NOT NULL,
  `orientacionTemporespacial` text COLLATE latin1_spanish_ci NOT NULL,
  `hemograma` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `leucocitos` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `plaquetas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `velocidad` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `bioquimica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `perfiles` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `densidad` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `ph` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `anormales` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `sedimento` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `lejosDerecho` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `lejosIzquierdo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `lejosBinocular` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `lejosCorreccion` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `lejosColores` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `cercaDerecho` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `cercaIzquierdo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `cercaBinocular` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `cercaCorreccion` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `cercaColores` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `cvf` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fev1` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tiffenau` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `restrictivo` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `obstructivo` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `mixto` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `normal` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `holterIA` text COLLATE latin1_spanish_ci NOT NULL,
  `holterECG` text COLLATE latin1_spanish_ci NOT NULL,
  `ECGEsfuerzo` text COLLATE latin1_spanish_ci NOT NULL,
  `revision` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `fechaRevision` date NOT NULL,
  `nombreMedico` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `firmaDoctor` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `visitas` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL,
  `centro` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `pregunta1` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta2` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta3` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta4` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta5` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta6` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta7` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta8` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta9` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta10` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta11` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta12` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta13` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta14` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta15` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta16` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta17` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta18` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta19` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta20` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta21` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `pregunta22` enum('SI','NO','NP') COLLATE latin1_spanish_ci NOT NULL,
  `observacion1` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion2` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion3` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion4` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion5` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion6` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion7` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion8` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion9` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion10` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion11` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion12` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion13` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion14` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion15` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion16` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion17` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion18` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion19` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion20` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion21` text COLLATE latin1_spanish_ci NOT NULL,
  `observacion22` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `firmaTecnico` text COLLATE latin1_spanish_ci NOT NULL,
  `firmaResponsable` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


ALTER TABLE `accesos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoUsuario` (`codigoUsuario`);

ALTER TABLE `accidentes`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_accidentes` (`codigoEmpleado`),
  ADD KEY `fk_accidentes2` (`codigoUsuarioTecnico`);

ALTER TABLE `actividades`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `actividades_ofertas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoOferta` (`codigoOferta`),
  ADD KEY `codigoActividad` (`codigoActividad`);

ALTER TABLE `actividades_procesos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoActividad` (`codigoActividad`);

ALTER TABLE `actividad_parte_diario`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_actividad_parte_diario` (`codigoParte`);

ALTER TABLE `actuaciones_cliente`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `actuaciones_clientes_fk` (`codigoCliente`);

ALTER TABLE `aplicaciones_cliente`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `aplicaciones_clientes_fk` (`codigoCliente`);

ALTER TABLE `asistencia_a_inspecciones`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoCliente` (`codigoCliente`),
  ADD KEY `codigoUsuario` (`codigoUsuario`);

ALTER TABLE `auditorias_internas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `auditorias_clientes_fk` (`codigoCliente`);

ALTER TABLE `bie_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFormularioPRL` (`codigoFormularioPRL`),
  ADD KEY `fk_bie_formulario_prl2` (`codigoCentroTrabajo`);

ALTER TABLE `botiquines_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFormularioPRL` (`codigoFormularioPRL`),
  ADD KEY `fk_botiquines_formulario_prl2` (`codigoCentroTrabajo`);

ALTER TABLE `centros_medicos`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `centros_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoTrabajo` (`codigoTrabajo`);

ALTER TABLE `citas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_citas` (`codigoEmpleado`),
  ADD KEY `fk_citas2` (`codigoSala`);

ALTER TABLE `clientes`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `comercial` (`comercial`),
  ADD KEY `EMPACTIVIDAD` (`EMPACTIVIDAD`);

ALTER TABLE `clientes_centros`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoCliente` (`codigoCliente`);

ALTER TABLE `cobros_facturas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFactura` (`codigoFactura`),
  ADD KEY `codigoCuentaPropia` (`codigoCuentaPropia`),
  ADD KEY `fk_cobros_facturas3` (`codigoRemesa`),
  ADD KEY `fk_cobros_facturas4` (`codigoFormaPago`);

ALTER TABLE `colaboradores`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `contratos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoOferta` (`codigoOferta`),
  ADD KEY `tecnico` (`tecnico`),
  ADD KEY `fk_contratos3` (`codigoFormaPago`),
  ADD KEY `fk_contratos2` (`codigoCuentaPropiaContrato`),
  ADD KEY `fk_contratos4` (`codigoColaborador`);

ALTER TABLE `contratos_en_facturas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_contratos_en_facturas` (`codigoContrato`),
  ADD KEY `fk_contratos_en_facturas2` (`codigoFactura`);

ALTER TABLE `contratos_renovaciones`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoContrato` (`codigoContrato`);

ALTER TABLE `copias_seguridad`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `copias_clientes_fk` (`codigoCliente`);

ALTER TABLE `correos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoUsuario` (`codigoUsuario`);

ALTER TABLE `cuentas_propias`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `declaraciones`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `detalles_cliente`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `detalles_clientes_fk` (`codigoCliente`);

ALTER TABLE `documentosvs`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoContrato` (`codigoContrato`);

ALTER TABLE `documentos_clientes`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoCliente` (`codigoCliente`);

ALTER TABLE `documentos_info`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFormulario` (`codigoFormulario`);

ALTER TABLE `documentos_plan_prev`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFormulario` (`codigoFormulario`);

ALTER TABLE `emisores`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `empleados`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoCliente` (`codigoCliente`),
  ADD KEY `codigoPuestoTrabajo` (`codigoPuestoTrabajo`);

ALTER TABLE `empleados_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoTrabajo` (`codigoTrabajo`);

ALTER TABLE `empleados_sensibles_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_empleados_sensibles_formulario_prl` (`codigoFormularioPRL`),
  ADD KEY `fk_empleados_sensibles_formulario_prl2` (`codigoEmpleado`),
  ADD KEY `fk_empleados_sensibles_formulario_prl3` (`codigoCentroTrabajo`);

ALTER TABLE `epis`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `epis_de_puesto_trabajo`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_epis_de_puesto_trabajo` (`codigoPuestoTrabajo`),
  ADD KEY `fk_epis_de_puesto_trabajo2` (`codigoEPI`);

ALTER TABLE `epis_en_reconocimiento_medico`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_epis_en_reconocimiento_medico` (`codigoReconocimientoMedico`),
  ADD KEY `fk_epis_en_reconocimiento_medico2` (`codigoEPI`);

ALTER TABLE `estados_recibos`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `evaluacion_general`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoCliente` (`codigoCliente`),
  ADD KEY `fk_evaluacion_general3` (`codigoPuestoTrabajo`);

ALTER TABLE `extintores_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFormularioPRL` (`codigoFormularioPRL`),
  ADD KEY `fk_extintores_formulario_prl3` (`codigoCentroTrabajo`);

ALTER TABLE `facturas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoCliente` (`codigoCliente`),
  ADD KEY `codigoSerieFactura` (`codigoSerieFactura`),
  ADD KEY `fk_facturas6` (`codigoEmisor`),
  ADD KEY `codigoFacturaAsociada` (`codigoFacturaAsociada`);

ALTER TABLE `facturas_reconocimientos_medicos`
  ADD PRIMARY KEY (`codigo`),
  ADD UNIQUE KEY `fk_facturas_reconocimientos_medicos` (`codigoEmisor`),
  ADD KEY `fk_facturas_reconocimientos_medicos2` (`codigoSerieFactura`),
  ADD KEY `fk_facturas_reconocimientos_medicos3` (`codigoContrato`),
  ADD KEY `fk_facturas_reconocimientos_medicos4` (`codigoClienteParaConceptoManual`);

ALTER TABLE `ficheros_cliente`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `ficheros_clientes_fk` (`codigoCliente`);

ALTER TABLE `formas_pago`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `formularioPRL`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoContrato` (`codigoContrato`),
  ADD KEY `fk_formularioPRL` (`codigoUsuarioTecnico`);

ALTER TABLE `fotos_accidentes`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_fotos_accidentes` (`codigoAccidente`);

ALTER TABLE `funciones_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFormularioPRL` (`codigoFormularioPRL`),
  ADD KEY `codigoCentroTrabajo` (`codigoCentroTrabajo`);

ALTER TABLE `gastos_parte_diario`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_gastos_parte_diario` (`codigoParte`);

ALTER TABLE `gastos_tarea`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_gastos_tarea` (`codigoTarea`);

ALTER TABLE `gestion_evaluacion_general`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoEvaluacion` (`codigoEvaluacion`);

ALTER TABLE `gestorias_cliente`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `gestorias_clientes_fk` (`codigoCliente`);

ALTER TABLE `historicoVS`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoEmpleado` (`codigoEmpleado`);

ALTER TABLE `incidencias`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `informaticos_cliente`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `informaticos_clientes_fk` (`codigoCliente`);

ALTER TABLE `informes`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoGestion` (`codigoEvaluacion`),
  ADD KEY `codigoFormularioEvaluacion` (`codigoEvaluacion`);

ALTER TABLE `instalaciones`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `instalaciones_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_instalaciones_formulario_prl` (`codigoFormularioPRL`),
  ADD KEY `fk_instalaciones_formulario_prl2` (`codigoInstalacion`),
  ADD KEY `fk_instalaciones_formulario_prl3` (`codigoCentroTrabajo`);

ALTER TABLE `locales_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFormularioPRL` (`codigoFormularioPRL`),
  ADD KEY `fk_locales_formulario_prl` (`codigoCentroTrabajo`);

ALTER TABLE `materias_primas_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_materias_primas_formulario_prl` (`codigoFormularioPRL`),
  ADD KEY `fk_materias_primas_formulario_prl2` (`codigoCentroTrabajo`);

ALTER TABLE `medidas_correctoras_accidentes`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_medidas_correctoras_accidentes` (`codigoAccidente`);

ALTER TABLE `medidas_riesgos_evaluacion_general`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoEvaluacion` (`codigoGestion`),
  ADD KEY `area` (`area`);

ALTER TABLE `medios_humanos_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFormularioPRL` (`codigoFormularioPRL`),
  ADD KEY `fk_medios_humanos_formulario_prl` (`codigoCentroTrabajo`);

ALTER TABLE `ofertas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoCliente` (`codigoCliente`),
  ADD KEY `codigoTarifa` (`codigoTarifa`),
  ADD KEY `fk_ofertas3` (`codigoFormaPagoOferta`),
  ADD KEY `fk_ofertas_6` (`codigoCuentaPropiaOferta`);

ALTER TABLE `ofertas_ficheros`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `parte_diario`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_parte_diario` (`codigoUsuario`);

ALTER TABLE `prl_cliente`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `prl_clientes_fk` (`codigoCliente`);

ALTER TABLE `profesionales_centros`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_profesionales_centros` (`codigoCentroMedico`);

ALTER TABLE `puestos_trabajo`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `puestos_trabajo_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_puestos_trabajo_formulario_prl` (`codigoPuestoTrabajo`),
  ADD KEY `fk_puestos_trabajo_formulario_prl2` (`codigoFormularioPRL`),
  ADD KEY `fk_puestos_trabajo_formulario_prl3` (`codigoCentroTrabajo`);

ALTER TABLE `reconocimientos_medicos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoEmpleado` (`codigoEmpleado`),
  ADD KEY `codigoContrato` (`codigoContrato`),
  ADD KEY `fk_reconocimientos_medicos3` (`codigoUsuario`);

ALTER TABLE `reconocimientos_medicos_en_facturas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_reconocimientos_medicos_en_facturas` (`codigoEmpleado`),
  ADD KEY `fk_reconocimientos_medicos_en_facturas2` (`codigoFacturaReconocimientoMedico`);

ALTER TABLE `remesas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoCuentaPropia` (`codigoCuentaPropia`);

ALTER TABLE `representantes_cliente`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `representantes_clientes_fk` (`codigoCliente`);

ALTER TABLE `riesgos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoProceso` (`tipo`);

ALTER TABLE `riesgos_evaluacion_general`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoRiesgo` (`codigoRiesgo`),
  ADD KEY `codigoRiesgo_2` (`codigoRiesgo`),
  ADD KEY `codigoEvaluacion` (`codigoEvaluacion`);

ALTER TABLE `riesgos_tipos`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `salas_centros_medicos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_salas_centros_medicos` (`codigoCentroMedico`);

ALTER TABLE `satisfaccion`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `sentencias_sql`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `series_facturas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_series_facturas` (`codigoEmisorSerie`);

ALTER TABLE `tareas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoUsuario` (`codigoUsuario`),
  ADD KEY `codigoCliente` (`codigoCliente`),
  ADD KEY `fk_tareas` (`codigoContrato`);

ALTER TABLE `tareas_parte_diario`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_tareas_parte_diario` (`codigoTarea`),
  ADD KEY `fk_tareas_parte_diario2` (`codigoParteDiario`),
  ADD KEY `fk_tareas_parte_diario3` (`codigoContrato`);

ALTER TABLE `tarifas`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `tipos_tareas`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `usuarios_ibfk_1` (`directorAsociado`),
  ADD KEY `usuarios_ibfk_2` (`teleconcertador`);

ALTER TABLE `usuarios_clientes`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoUsuario` (`codigoUsuario`),
  ADD KEY `codigoCliente` (`codigoCliente`);

ALTER TABLE `usuarios_empleados`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoUsuario` (`codigoUsuario`),
  ADD KEY `codigoEmpleado` (`codigoEmpleado`);

ALTER TABLE `vencimientos_en_remesas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoRemesa` (`codigoRemesa`),
  ADD KEY `codigoVencimiento` (`codigoVencimiento`);

ALTER TABLE `vencimientos_facturas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoConcepto` (`concepto`),
  ADD KEY `codigoFactura` (`codigoFactura`),
  ADD KEY `fk_vencimientos_facturas` (`codigoFormaPago`),
  ADD KEY `fk_vencimientos_facturas3` (`codigoEstadoRecibo`);

ALTER TABLE `vigilancia`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoCliente` (`codigoCliente`),
  ADD KEY `codigoEmpleado` (`codigoEmpleado`);

ALTER TABLE `visitas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoCliente` (`codigoCliente`);


ALTER TABLE `accesos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1383;
ALTER TABLE `accidentes`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
ALTER TABLE `actividades`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
ALTER TABLE `actividades_ofertas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `actividades_procesos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=254;
ALTER TABLE `actividad_parte_diario`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
ALTER TABLE `actuaciones_cliente`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;
ALTER TABLE `aplicaciones_cliente`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `asistencia_a_inspecciones`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
ALTER TABLE `auditorias_internas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `bie_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
ALTER TABLE `botiquines_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
ALTER TABLE `centros_medicos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
ALTER TABLE `centros_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `citas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
ALTER TABLE `clientes`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1514;
ALTER TABLE `clientes_centros`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=890;
ALTER TABLE `cobros_facturas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
ALTER TABLE `colaboradores`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
ALTER TABLE `contratos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=903;
ALTER TABLE `contratos_en_facturas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=517;
ALTER TABLE `contratos_renovaciones`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=403;
ALTER TABLE `copias_seguridad`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `correos`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
ALTER TABLE `cuentas_propias`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
ALTER TABLE `declaraciones`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `detalles_cliente`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `documentosvs`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
ALTER TABLE `documentos_clientes`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
ALTER TABLE `documentos_info`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
ALTER TABLE `documentos_plan_prev`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
ALTER TABLE `emisores`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
ALTER TABLE `empleados`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=213;
ALTER TABLE `empleados_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `empleados_sensibles_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
ALTER TABLE `epis`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
ALTER TABLE `epis_de_puesto_trabajo`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
ALTER TABLE `epis_en_reconocimiento_medico`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
ALTER TABLE `estados_recibos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
ALTER TABLE `evaluacion_general`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
ALTER TABLE `extintores_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
ALTER TABLE `facturas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=639;
ALTER TABLE `facturas_reconocimientos_medicos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
ALTER TABLE `ficheros_cliente`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `formas_pago`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
ALTER TABLE `formularioPRL`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=599;
ALTER TABLE `fotos_accidentes`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
ALTER TABLE `funciones_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
ALTER TABLE `gastos_parte_diario`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;
ALTER TABLE `gastos_tarea`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;
ALTER TABLE `gestion_evaluacion_general`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
ALTER TABLE `gestorias_cliente`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `historicoVS`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;
ALTER TABLE `incidencias`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
ALTER TABLE `informaticos_cliente`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `informes`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
ALTER TABLE `instalaciones`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
ALTER TABLE `instalaciones_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
ALTER TABLE `locales_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
ALTER TABLE `materias_primas_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
ALTER TABLE `medidas_correctoras_accidentes`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
ALTER TABLE `medidas_riesgos_evaluacion_general`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;
ALTER TABLE `medios_humanos_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
ALTER TABLE `ofertas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1017;
ALTER TABLE `ofertas_ficheros`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
ALTER TABLE `parte_diario`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;
ALTER TABLE `prl_cliente`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `profesionales_centros`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
ALTER TABLE `puestos_trabajo`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
ALTER TABLE `puestos_trabajo_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
ALTER TABLE `reconocimientos_medicos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
ALTER TABLE `reconocimientos_medicos_en_facturas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
ALTER TABLE `remesas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `representantes_cliente`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `riesgos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
ALTER TABLE `riesgos_evaluacion_general`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
ALTER TABLE `riesgos_tipos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
ALTER TABLE `salas_centros_medicos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
ALTER TABLE `satisfaccion`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
ALTER TABLE `sentencias_sql`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12104;
ALTER TABLE `series_facturas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
ALTER TABLE `tareas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=878;
ALTER TABLE `tareas_parte_diario`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
ALTER TABLE `tarifas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
ALTER TABLE `tipos_tareas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
ALTER TABLE `usuarios`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1173;
ALTER TABLE `usuarios_clientes`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=804;
ALTER TABLE `usuarios_empleados`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=213;
ALTER TABLE `vencimientos_en_remesas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `vencimientos_facturas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
ALTER TABLE `vigilancia`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
ALTER TABLE `visitas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE `accesos`
  ADD CONSTRAINT `res_accesos_usuarios` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `accidentes`
  ADD CONSTRAINT `fk_accidentes` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_accidentes2` FOREIGN KEY (`codigoUsuarioTecnico`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `actividades_ofertas`
  ADD CONSTRAINT `actividades_ofertas_ibfk1` FOREIGN KEY (`codigoOferta`) REFERENCES `ofertas` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `actividades_ofertas_ibfk2` FOREIGN KEY (`codigoActividad`) REFERENCES `tarifas` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `actividades_procesos`
  ADD CONSTRAINT `actividades_procesos_ibfk` FOREIGN KEY (`codigoActividad`) REFERENCES `actividades` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `actividad_parte_diario`
  ADD CONSTRAINT `fk_actividad_parte_diario` FOREIGN KEY (`codigoParte`) REFERENCES `parte_diario` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `actuaciones_cliente`
  ADD CONSTRAINT `actuaciones_clientes_fk` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `aplicaciones_cliente`
  ADD CONSTRAINT `aplicaciones_clientes_fk` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `asistencia_a_inspecciones`
  ADD CONSTRAINT `fk_asistencia1` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_asistencia2` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `auditorias_internas`
  ADD CONSTRAINT `auditorias_clientes_fk` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `bie_formulario_prl`
  ADD CONSTRAINT `bie_formulario_prl2` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `bie_formulario_prl_ibfk_1` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `botiquines_formulario_prl`
  ADD CONSTRAINT `botiquines_formulario_prl_ibfk_1` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `botiquines_formulario_prl_ibfk_2` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `centros_prl`
  ADD CONSTRAINT `trabajos_centros` FOREIGN KEY (`codigoTrabajo`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `citas`
  ADD CONSTRAINT `fk_citas` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_citas2` FOREIGN KEY (`codigoSala`) REFERENCES `salas_centros_medicos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `clientes`
  ADD CONSTRAINT `clientes_actividad` FOREIGN KEY (`EMPACTIVIDAD`) REFERENCES `actividades` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `clientes_comercial` FOREIGN KEY (`comercial`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE `clientes_centros`
  ADD CONSTRAINT `clientes_centros` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cobros_facturas`
  ADD CONSTRAINT `fk_cobros_facturas` FOREIGN KEY (`codigoFactura`) REFERENCES `facturas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cobros_facturas2` FOREIGN KEY (`codigoCuentaPropia`) REFERENCES `cuentas_propias` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cobros_facturas3` FOREIGN KEY (`codigoRemesa`) REFERENCES `remesas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cobros_facturas4` FOREIGN KEY (`codigoFormaPago`) REFERENCES `formas_pago` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `contratos`
  ADD CONSTRAINT `contratos_ofertas_ibfk_1` FOREIGN KEY (`codigoOferta`) REFERENCES `ofertas` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `contratos_tecnico_ibfk_1` FOREIGN KEY (`tecnico`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_contratos2` FOREIGN KEY (`codigoCuentaPropiaContrato`) REFERENCES `cuentas_propias` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_contratos3` FOREIGN KEY (`codigoFormaPago`) REFERENCES `formas_pago` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_contratos4` FOREIGN KEY (`codigoColaborador`) REFERENCES `colaboradores` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `contratos_en_facturas`
  ADD CONSTRAINT `fk_contratos_en_facturas` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_contratos_en_facturas2` FOREIGN KEY (`codigoFactura`) REFERENCES `facturas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `contratos_renovaciones`
  ADD CONSTRAINT `contratos_renovaciones_ibfk` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `copias_seguridad`
  ADD CONSTRAINT `copias_clientes_fk` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `correos`
  ADD CONSTRAINT `correos_ibfk_1` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `detalles_cliente`
  ADD CONSTRAINT `detalles_clientes_fk` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `documentosvs`
  ADD CONSTRAINT `documentosvs_contratos_ibfk` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `documentos_clientes`
  ADD CONSTRAINT `clientes_documentos_ibfk1` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `documentos_info`
  ADD CONSTRAINT `documentos_formulario_ibfk2` FOREIGN KEY (`codigoFormulario`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `documentos_plan_prev`
  ADD CONSTRAINT `formulario_documentos_ibfk1` FOREIGN KEY (`codigoFormulario`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `empleados`
  ADD CONSTRAINT `empleados_clientes_ibfk` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_empleados2` FOREIGN KEY (`codigoPuestoTrabajo`) REFERENCES `puestos_trabajo` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `empleados_prl`
  ADD CONSTRAINT `empleados_prl_trabajos` FOREIGN KEY (`codigoTrabajo`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `empleados_sensibles_formulario_prl`
  ADD CONSTRAINT `empleados_sensibles_formulario_prl_ibfk_1` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `empleados_sensibles_formulario_prl_ibfk_2` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `empleados_sensibles_formulario_prl_ibfk_3` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `epis_de_puesto_trabajo`
  ADD CONSTRAINT `fk_epis_de_puesto_trabajo` FOREIGN KEY (`codigoPuestoTrabajo`) REFERENCES `puestos_trabajo` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_epis_de_puesto_trabajo2` FOREIGN KEY (`codigoEPI`) REFERENCES `epis` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `epis_en_reconocimiento_medico`
  ADD CONSTRAINT `fk_epis_en_reconocimiento_medico` FOREIGN KEY (`codigoReconocimientoMedico`) REFERENCES `reconocimientos_medicos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_epis_en_reconocimiento_medico2` FOREIGN KEY (`codigoEPI`) REFERENCES `epis` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `evaluacion_general`
  ADD CONSTRAINT `evaluacion_cliente_ibfk` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `evaluacion_general_ibfk_1` FOREIGN KEY (`codigoPuestoTrabajo`) REFERENCES `puestos_trabajo` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `extintores_formulario_prl`
  ADD CONSTRAINT `extintores_formulario_prl` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_extintores_formulario_prl2` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `facturas`
  ADD CONSTRAINT `fk_facturas` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturas6` FOREIGN KEY (`codigoSerieFactura`) REFERENCES `series_facturas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturas7` FOREIGN KEY (`codigoEmisor`) REFERENCES `emisores` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `facturas_reconocimientos_medicos`
  ADD CONSTRAINT `fk_facturas_reconocimientos_medicos` FOREIGN KEY (`codigoEmisor`) REFERENCES `emisores` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturas_reconocimientos_medicos2` FOREIGN KEY (`codigoSerieFactura`) REFERENCES `series_facturas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturas_reconocimientos_medicos3` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturas_reconocimientos_medicos4` FOREIGN KEY (`codigoClienteParaConceptoManual`) REFERENCES `clientes` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `ficheros_cliente`
  ADD CONSTRAINT `ficheros_clientes_fk` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `formularioPRL`
  ADD CONSTRAINT `fk_formularioPRL` FOREIGN KEY (`codigoUsuarioTecnico`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `fotos_accidentes`
  ADD CONSTRAINT `fk_fotos_accidentes` FOREIGN KEY (`codigoAccidente`) REFERENCES `accidentes` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `funciones_formulario_prl`
  ADD CONSTRAINT `funciones_formulario_prl` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `funciones_formulario_prl_1` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `gastos_parte_diario`
  ADD CONSTRAINT `fk_gastos_parte_diario` FOREIGN KEY (`codigoParte`) REFERENCES `parte_diario` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `gastos_tarea`
  ADD CONSTRAINT `fk_gastos_tarea` FOREIGN KEY (`codigoTarea`) REFERENCES `tareas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `gestion_evaluacion_general`
  ADD CONSTRAINT `res_gestion_evaluacion` FOREIGN KEY (`codigoEvaluacion`) REFERENCES `riesgos_evaluacion_general` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `gestorias_cliente`
  ADD CONSTRAINT `gestorias_clientes_fk` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `historicoVS`
  ADD CONSTRAINT `empleados_historico` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `informaticos_cliente`
  ADD CONSTRAINT `informaticos_clientes_fk` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `informes`
  ADD CONSTRAINT `informes_ibfk_2` FOREIGN KEY (`codigoEvaluacion`) REFERENCES `evaluacion_general` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `instalaciones_formulario_prl`
  ADD CONSTRAINT `fk_instalaciones_formulario_prl` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_instalaciones_formulario_prl2` FOREIGN KEY (`codigoInstalacion`) REFERENCES `instalaciones` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_instalaciones_formulario_prl3` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `locales_formulario_prl`
  ADD CONSTRAINT `locales_formulario_prl_ibfk_1` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `locales_formulario_prl_ibfk_2` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `materias_primas_formulario_prl`
  ADD CONSTRAINT `fk_materias_primas_formulario_prl2` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `materias_primas_formulario_prl` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `medidas_correctoras_accidentes`
  ADD CONSTRAINT `fk_medidas_correctoras_accidentes` FOREIGN KEY (`codigoAccidente`) REFERENCES `accidentes` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `medidas_riesgos_evaluacion_general`
  ADD CONSTRAINT `res_medidas_gestion` FOREIGN KEY (`codigoGestion`) REFERENCES `gestion_evaluacion_general` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `medios_humanos_formulario_prl`
  ADD CONSTRAINT `medios_humanos_formulario_prl_ibfk_1` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `medios_humanos_formulario_prl_ibfk_2` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `ofertas`
  ADD CONSTRAINT `fk_ofertas3` FOREIGN KEY (`codigoFormaPagoOferta`) REFERENCES `formas_pago` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ofertas_6` FOREIGN KEY (`codigoCuentaPropiaOferta`) REFERENCES `cuentas_propias` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `ofertas_clientes_ibfk_1` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `ofertas_tarifas_ibfk_1` FOREIGN KEY (`codigoTarifa`) REFERENCES `tarifas` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE `prl_cliente`
  ADD CONSTRAINT `prl_clientes_fk` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `profesionales_centros`
  ADD CONSTRAINT `fk_profesionales_centros` FOREIGN KEY (`codigoCentroMedico`) REFERENCES `centros_medicos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `puestos_trabajo_formulario_prl`
  ADD CONSTRAINT `fk_puestos_trabajo_formulario_prl` FOREIGN KEY (`codigoPuestoTrabajo`) REFERENCES `puestos_trabajo` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_puestos_trabajo_formulario_prl2` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_puestos_trabajo_formulario_prl3` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `reconocimientos_medicos`
  ADD CONSTRAINT `fk_reconocimientos_medicos` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_reconocimientos_medicos2` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_reconocimientos_medicos3` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `reconocimientos_medicos_en_facturas`
  ADD CONSTRAINT `fk_reconocimientos_medicos_en_facturas` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_reconocimientos_medicos_en_facturas2` FOREIGN KEY (`codigoFacturaReconocimientoMedico`) REFERENCES `facturas_reconocimientos_medicos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `remesas`
  ADD CONSTRAINT `fk_remesas` FOREIGN KEY (`codigoCuentaPropia`) REFERENCES `cuentas_propias` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `representantes_cliente`
  ADD CONSTRAINT `representantes_clientes_fk` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `riesgos`
  ADD CONSTRAINT `riesgos_tipo` FOREIGN KEY (`tipo`) REFERENCES `riesgos_tipos` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE `riesgos_evaluacion_general`
  ADD CONSTRAINT `riesgos_evaluacion_ibfk1` FOREIGN KEY (`codigoRiesgo`) REFERENCES `riesgos` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `riesgos_evaluacion_ibfk2` FOREIGN KEY (`codigoEvaluacion`) REFERENCES `evaluacion_general` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `salas_centros_medicos`
  ADD CONSTRAINT `fk_salas_centros_medicos` FOREIGN KEY (`codigoCentroMedico`) REFERENCES `centros_medicos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `series_facturas`
  ADD CONSTRAINT `fk_series_facturas` FOREIGN KEY (`codigoEmisorSerie`) REFERENCES `emisores` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `tareas`
  ADD CONSTRAINT `fk_tareas` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `tareas_clientes` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tareas_usuarios` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE `tareas_parte_diario`
  ADD CONSTRAINT `fk_tareas_parte_diario` FOREIGN KEY (`codigoParteDiario`) REFERENCES `parte_diario` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tareas_parte_diario2` FOREIGN KEY (`codigoTarea`) REFERENCES `tareas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tareas_parte_diario3` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`directorAsociado`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `usuarios_ibfk_2` FOREIGN KEY (`teleconcertador`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE `usuarios_clientes`
  ADD CONSTRAINT `usuarios_clientes_ibfk_1` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuarios_clientes_ibfk_2` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `usuarios_empleados`
  ADD CONSTRAINT `usuarios_empleados_ibfk_1` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuarios_empleados_ibfk_2` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `vencimientos_en_remesas`
  ADD CONSTRAINT `fk_facturas_en_remesas2` FOREIGN KEY (`codigoVencimiento`) REFERENCES `vencimientos_facturas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturas_en_remesas3` FOREIGN KEY (`codigoRemesa`) REFERENCES `remesas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `vencimientos_facturas`
  ADD CONSTRAINT `fk_vencimientos_facturas` FOREIGN KEY (`codigoFactura`) REFERENCES `facturas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vencimientos_facturas2` FOREIGN KEY (`codigoFormaPago`) REFERENCES `formas_pago` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vencimientos_facturas3` FOREIGN KEY (`codigoEstadoRecibo`) REFERENCES `estados_recibos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `vigilancia`
  ADD CONSTRAINT `vigilancia_cliente` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vigilancia_empleado` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `visitas`
  ADD CONSTRAINT `visitas_clientes_ibfk` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
