La factura de abril del cliente SJ Puerto fue creada para un contrato distinto del que figura actualmente.
La factura, y sus reconocimientos asociados, estaban emitidos para el contrato 21-022-18-004, pero posteriormente fue modificada la factura para que el contrato asociado fuese el 21-022-18-005.
El contrato original tenía los reconocimientos médicos asociados, pero como se cambió por otro contrato sin reconocimientos, la factura quedó en blanco.

Como ya he mencionado arriba, ya no deberíais tener problemas similares por cambiar conceptos de facturas porque he bloqueado esa posibilidad.

Adicionalmente, comentar que la modificación del contrato para la factura indicada la realizó el usuario MARTA ESPARRAGA PONCE el 22/10/2019 a las 11:11:58.