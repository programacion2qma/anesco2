-- Creción de la tabla accidentes:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `accidentes` (
  `codigo` int(255) NOT NULL,
  `fechaComunicacion` date NOT NULL,
  `comunicacion` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `textoComunicacion` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `codigoEmpleado` int(255) DEFAULT NULL,
  `fechaInvestigacion` date NOT NULL,
  `codigoUsuarioTecnico` int(255) DEFAULT NULL,
  `fechaAccidente` date NOT NULL,
  `horaAccidente` time NOT NULL,
  `puesto` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` text COLLATE latin1_spanish_ci NOT NULL,
  `causas` text COLLATE latin1_spanish_ci NOT NULL,
  `gradoLesion` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `fechaRecepcionInforme` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


ALTER TABLE `accidentes`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_accidentes` (`codigoEmpleado`),
  ADD KEY `fk_accidentes2` (`codigoUsuarioTecnico`);


ALTER TABLE `accidentes`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `accidentes`
  ADD CONSTRAINT `fk_accidentes2` FOREIGN KEY (`codigoUsuarioTecnico`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_accidentes` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Creción de la tabla medidas_correctoras_accidentes:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `medidas_correctoras_accidentes` (
  `codigo` int(255) NOT NULL,
  `medidas` text COLLATE latin1_spanish_ci NOT NULL,
  `plazo` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `verificacion` text COLLATE latin1_spanish_ci NOT NULL,
  `codigoAccidente` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


ALTER TABLE `medidas_correctoras_accidentes`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_medidas_correctoras_accidentes` (`codigoAccidente`);


ALTER TABLE `medidas_correctoras_accidentes`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `medidas_correctoras_accidentes`
  ADD CONSTRAINT `fk_medidas_correctoras_accidentes` FOREIGN KEY (`codigoAccidente`) REFERENCES `accidentes` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


ALTER TABLE `formularioPRL` ADD `horasVisita` INT NOT NULL AFTER `horasPrevistas`, ADD `horasPlanPrev` INT NOT NULL AFTER `horasVisita`, ADD `horasPap` INT NOT NULL AFTER `horasPlanPrev`, ADD `horasInfo` INT NOT NULL AFTER `horasPap`, ADD `horasMemoria` INT NOT NULL AFTER `horasInfo`, ADD `horasVs` INT NOT NULL AFTER `horasMemoria`;
ALTER TABLE `formularioPRL` ADD `totalHoras` VARCHAR(255) NOT NULL AFTER `horasVs`;
ALTER TABLE `formularioPRL` CHANGE `horasVisita` `horasVisita` VARCHAR(255) NOT NULL, CHANGE `horasPlanPrev` `horasPlanPrev` VARCHAR(255) NOT NULL, CHANGE `horasPap` `horasPap` VARCHAR(255) NOT NULL, CHANGE `horasInfo` `horasInfo` VARCHAR(255) NOT NULL, CHANGE `horasMemoria` `horasMemoria` VARCHAR(255) NOT NULL, CHANGE `horasVs` `horasVs` VARCHAR(255) NOT NULL;
ALTER TABLE `formularioPRL` CHANGE `horasPrevistas` `horasPrevistas` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL DEFAULT '0';

-- Creación de la tabla fotos_accidentes:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `fotos_accidentes` (
  `codigo` int(255) NOT NULL,
  `fichero` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `codigoAccidente` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


ALTER TABLE `fotos_accidentes`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_fotos_accidentes` (`codigoAccidente`);


ALTER TABLE `fotos_accidentes`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `fotos_accidentes`
  ADD CONSTRAINT `fk_fotos_accidentes` FOREIGN KEY (`codigoAccidente`) REFERENCES `accidentes` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Creación de la tabla asistencia_a_inspecciones:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `asistencia_a_inspecciones` (
`codigo` int(255) NOT NULL,
  `fecha` date NOT NULL,
  `codigoCliente` int(255) DEFAULT NULL,
  `codigoUsuario` int(255) DEFAULT NULL,
  `tipoAsistencia` varchar(50) NOT NULL,
  `observaciones` longtext NOT NULL,
  `horasDedicadas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `asistencia_a_inspecciones`
 ADD PRIMARY KEY (`codigo`), ADD KEY `codigoCliente` (`codigoCliente`), ADD KEY `codigoUsuario` (`codigoUsuario`);


ALTER TABLE `asistencia_a_inspecciones`
MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `asistencia_a_inspecciones`
ADD CONSTRAINT `fk_asistencia1` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
ADD CONSTRAINT `fk_asistencia2` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Creación de la tabla puestos_trabajo:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `puestos_trabajo` (
`codigo` int(255) NOT NULL,
  `nombre` tinytext NOT NULL,
  `descripcion` text NOT NULL,
  `codigoCliente` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `puestos_trabajo`
 ADD PRIMARY KEY (`codigo`), ADD KEY `codigoCliente` (`codigoCliente`);


ALTER TABLE `puestos_trabajo`
MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `puestos_trabajo`
ADD CONSTRAINT `fk_puestos_trabajo` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo codigoPuestoTrabajo a la tabla empleados:
ALTER TABLE `empleados` ADD `codigoPuestoTrabajo` INT(255) NULL , ADD INDEX (`codigoPuestoTrabajo`);
ALTER TABLE `empleados` ADD CONSTRAINT `fk_empleados2` FOREIGN KEY (`codigoPuestoTrabajo`) REFERENCES `puestos_trabajo`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `ofertas` ADD `otraTarifa` VARCHAR(255) NOT NULL AFTER `codigoTarifa`;

ALTER TABLE `clientes` ADD `EMPRL` VARCHAR(255) NOT NULL AFTER `baja`, ADD `EMPRLDNI` VARCHAR(20) NOT NULL AFTER `EMPRL`;
ALTER TABLE `clientes` ADD `EMPTELPRINC` VARCHAR(20) NOT NULL AFTER `EMPRLDNI`, ADD `EMPEMAILPRINC` VARCHAR(255) NOT NULL AFTER `EMPTELPRINC`;
UPDATE clientes SET EMPTELPRINC=EMPTEL1, EMPEMAILPRINC=EMPEMAIL;
ALTER TABLE `clientes` CHANGE `EMPRIESGO` `EMPRIESGO` ENUM('BAJO','MEDIO','ALTO') CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL DEFAULT ‘BAJO’, CHANGE `EMPANEXO` `EMPANEXO` ENUM('SI','NO') CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL DEFAULT 'NO';
ALTER TABLE `ofertas` ADD `fechaInicio` DATE NOT NULL AFTER `otraTarifa`, ADD `fechaFin` DATE NOT NULL AFTER `fechaInicio`;


-- Creación de la tabla puestos_trabajo_formulario_prl:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `puestos_trabajo_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoPuestoTrabajo` int(255) DEFAULT NULL,
  `codigoFormularioPRL` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `puestos_trabajo_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_puestos_trabajo_formulario_prl` (`codigoPuestoTrabajo`),
  ADD KEY `fk_puestos_trabajo_formulario_prl2` (`codigoFormularioPRL`);


ALTER TABLE `puestos_trabajo_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `puestos_trabajo_formulario_prl`
  ADD CONSTRAINT `fk_puestos_trabajo_formulario_prl2` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_puestos_trabajo_formulario_prl` FOREIGN KEY (`codigoPuestoTrabajo`) REFERENCES `puestos_trabajo` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Creación de la tabla materias_primas_formulario_prl:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `materias_primas_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `materia` varchar(255) NOT NULL,
  `lugar` varchar(255) NOT NULL,
  `cantidad` varchar(255) NOT NULL,
  `codigoFormularioPRL` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `materias_primas_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_materias_primas_formulario_prl` (`codigoFormularioPRL`);


ALTER TABLE `materias_primas_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `materias_primas_formulario_prl`
  ADD CONSTRAINT `materias_primas_formulario_prl` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Campos para adaptar la herramienta al PDF
ALTER TABLE `ofertas` ADD `observaciones` TEXT NOT NULL AFTER `fechaFin`;
ALTER TABLE `clientes` ADD `EMPRLCARGO` VARCHAR(255) NOT NULL AFTER `EMPRLDNI`;
ALTER TABLE `clientes` ADD `EMPFIRMA` LONGTEXT NOT NULL AFTER `EMPEMAILPRINC`;
ALTER TABLE `clientes` ADD `EMPGRUPO` VARCHAR(255) NOT NULL AFTER `EMPFIRMA`, ADD `EMPSECTOR` VARCHAR(255) NOT NULL AFTER `EMPGRUPO`;

--
-- Estructura de tabla para la tabla `extintores_formulario_prl`
--

CREATE TABLE `extintores_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoFormularioPRL` int(255) NOT NULL,
  `numero` varchar(255) NOT NULL,
  `kg` double NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `eficacia` varchar(255) NOT NULL,
  `senializado` enum('SI','NO') NOT NULL,
  `revisado` enum('SI','NO') NOT NULL,
  `ubicacion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `extintores_formulario_prl`
--
ALTER TABLE `extintores_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFormularioPRL` (`codigoFormularioPRL`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `extintores_formulario_prl`
--
ALTER TABLE `extintores_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `extintores_formulario_prl`
--
ALTER TABLE `extintores_formulario_prl`
  ADD CONSTRAINT `extintores_formulario_prl` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Estructura de tabla para la tabla `bie_formulario_prl`
--

CREATE TABLE `bie_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoFormularioPRL` int(255) NOT NULL,
  `numero` varchar(255) NOT NULL,
  `medida` enum('24','45') NOT NULL,
  `presion` varchar(255) NOT NULL,
  `senializado` enum('SI','NO') NOT NULL,
  `revisado` enum('SI','NO') NOT NULL,
  `ubicacion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bie_formulario_prl`
--
ALTER TABLE `bie_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFormularioPRL` (`codigoFormularioPRL`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bie_formulario_prl`
--
ALTER TABLE `bie_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `bie_formulario_prl`
--
ALTER TABLE `bie_formulario_prl`
  ADD CONSTRAINT `bie_formulario_prl_ibfk_1` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Estructura de tabla para la tabla `locales_formulario_prl`
--

CREATE TABLE `locales_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoFormularioPRL` int(255) NOT NULL,
  `ubicacion` varchar(255) NOT NULL,
  `senializado` enum('SI','NO') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `locales_formulario_prl`
--
ALTER TABLE `locales_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFormularioPRL` (`codigoFormularioPRL`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `locales_formulario_prl`
--
ALTER TABLE `locales_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `locales_formulario_prl`
--
ALTER TABLE `locales_formulario_prl`
  ADD CONSTRAINT `locales_formulario_prl_ibfk_1` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Estructura de tabla para la tabla `botiquines_formulario_prl`
--

CREATE TABLE `botiquines_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoFormularioPRL` int(255) NOT NULL,
  `ubicacion` varchar(255) NOT NULL,
  `senializado` enum('SI','NO') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `botiquines_formulario_prl`
--
ALTER TABLE `botiquines_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFormularioPRL` (`codigoFormularioPRL`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `botiquines_formulario_prl`
--
ALTER TABLE `botiquines_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `botiquines_formulario_prl`
--
ALTER TABLE `botiquines_formulario_prl`
  ADD CONSTRAINT `botiquines_formulario_prl_ibfk_1` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Estructura de tabla para la tabla `medios_humanos_formulario_prl`
--

CREATE TABLE `medios_humanos_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoFormularioPRL` int(255) NOT NULL,
  `ubicacion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `medios_humanos_formulario_prl`
--
ALTER TABLE `medios_humanos_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFormularioPRL` (`codigoFormularioPRL`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `medios_humanos_formulario_prl`
--
ALTER TABLE `medios_humanos_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `medios_humanos_formulario_prl`
--
ALTER TABLE `medios_humanos_formulario_prl`
  ADD CONSTRAINT `medios_humanos_formulario_prl_ibfk_1` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Añadido campo sensible
ALTER TABLE `puestos_trabajo_formulario_prl` ADD `sensible` ENUM('SI','NO') NOT NULL AFTER `codigoFormularioPRL`;


-- Añadido el campo firmaTecnico a la tabla formularioPRL:
ALTER TABLE `formularioPRL` ADD `firmaTecnico` TEXT NOT NULL AFTER `totalHoras`;

-- Añadidos los campos tipoEvaluacion, codigoPuestoTrabajo y otros a la tabla evaluacion_general:
ALTER TABLE `evaluacion_general` ADD `tipoEvaluacion` VARCHAR(10) NOT NULL AFTER `ficheroEvaluacion`, ADD `codigoPuestoTrabajo` INT(255) NULL AFTER `tipoEvaluacion`, ADD `otros` TINYTEXT NOT NULL AFTER `codigoPuestoTrabajo`, ADD INDEX `fk_evaluacion_general3` (`codigoPuestoTrabajo`);
ALTER TABLE  `evaluacion_general` ADD FOREIGN KEY (  `codigoPuestoTrabajo` ) REFERENCES  `puestos_trabajo` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE ;

-- Eliminado el 4º centro d los clientes
ALTER TABLE `clientes` DROP `EMPCENTRO4`;

-- Añadido el campo ficheroPlano a la tabla formularioPRL:
ALTER TABLE `formularioPRL` ADD `ficheroPlano` VARCHAR(100) NOT NULL AFTER `firmaTecnico`;

-- Añadidos los campos tareas y zonaTrabajo a la tabla puestos_trabajo:
ALTER TABLE `puestos_trabajo` ADD `tareas` TEXT NOT NULL AFTER `descripcion`, ADD `zonaTrabajo` TINYTEXT NOT NULL AFTER `tareas`;

-- Creación de la tabla epis:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `epis` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `codigoCliente` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `epis`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_epis` (`codigoCliente`);


ALTER TABLE `epis`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `epis`
  ADD CONSTRAINT `fk_epis` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Creación de la tabla epis_de_puesto_trabajo:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `epis_de_puesto_trabajo` (
  `codigo` int(255) NOT NULL,
  `codigoPuestoTrabajo` int(255) DEFAULT NULL,
  `codigoEPI` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `epis_de_puesto_trabajo`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_epis_de_puesto_trabajo` (`codigoPuestoTrabajo`),
  ADD KEY `fk_epis_de_puesto_trabajo2` (`codigoEPI`);


ALTER TABLE `epis_de_puesto_trabajo`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `epis_de_puesto_trabajo`
  ADD CONSTRAINT `fk_epis_de_puesto_trabajo2` FOREIGN KEY (`codigoEPI`) REFERENCES `epis` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_epis_de_puesto_trabajo` FOREIGN KEY (`codigoPuestoTrabajo`) REFERENCES `puestos_trabajo` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Añadidos los campos importeRM, numRM, subtotalRM e importeRMExtra a la tabla ofertas:
ALTER TABLE `ofertas` ADD `importeRM` DOUBLE(20,2) NOT NULL AFTER `observaciones`, ADD `numRM` DOUBLE(20,2) NOT NULL AFTER `importeRM`, ADD `subtotalRM` DOUBLE(20,2) NOT NULL AFTER `numRM`, ADD `importeRMExtra` DOUBLE(20,2) NOT NULL AFTER `subtotalRM`;

-- Modificado el nombre del campo checkVigor por enVigor en la tabla contratos:
ALTER TABLE `contratos` CHANGE `checkVigor` `enVigor` ENUM('SI','NO') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;



-- Creación de las tablas asociadas al módulo de Facturación de LAE (de donde se está clonando):
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `cobros_facturas` (
  `codigo` int(255) NOT NULL,
  `codigoFactura` int(255) DEFAULT NULL,
  `medioPago` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fechaCobro` date NOT NULL,
  `importe` double(20,2) NOT NULL,
  `codigoCuentaPropia` int(255) DEFAULT NULL,
  `codigoRemesa` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `cuentas_propias` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `razonSocial` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `cif` varchar(9) COLLATE latin1_spanish_ci NOT NULL,
  `iban` varchar(24) COLLATE latin1_spanish_ci NOT NULL,
  `bic` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `sufijo` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `referenciaAcreedor` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `descontado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `activo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `emisores` (
  `codigo` int(255) NOT NULL,
  `razonSocial` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `cif` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `domicilio` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `cp` varchar(7) COLLATE latin1_spanish_ci NOT NULL,
  `localidad` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `provincia` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `iban` varchar(25) COLLATE latin1_spanish_ci NOT NULL,
  `prefijo` varchar(10) COLLATE latin1_spanish_ci NOT NULL,
  `ficheroLogo` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `telefono` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `fax` varchar(15) COLLATE latin1_spanish_ci NOT NULL,
  `web` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `email` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `registro` text COLLATE latin1_spanish_ci NOT NULL,
  `referenciaAcreedor` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `activo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `facturas` (
  `codigo` int(255) NOT NULL,
  `codigoEmisor` int(255) DEFAULT NULL,
  `fecha` date NOT NULL,
  `codigoSerieFactura` int(255) DEFAULT NULL,
  `numero` int(255) NOT NULL,
  `codigoCliente` int(255) DEFAULT NULL,
  `tipoFactura` enum('NORMAL','ABONO') COLLATE latin1_spanish_ci NOT NULL,
  `baseImponible` double(20,2) NOT NULL,
  `total` double(20,2) NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `anulada` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `comentariosLlamada` text COLLATE latin1_spanish_ci NOT NULL,
  `activo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `codigoFacturaAsociada` int(255) DEFAULT NULL,
  `facturaEnviada` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `informeEnviado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `remesas` (
  `codigo` int(255) NOT NULL,
  `formato` enum('CORE','COR1','B2B') COLLATE latin1_spanish_ci NOT NULL,
  `financiado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `mismoVencimiento` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `codigoCuentaPropia` int(255) DEFAULT NULL,
  `fecha` date NOT NULL,
  `total` double(20,2) NOT NULL,
  `fechaVencimiento` date NOT NULL,
  `estado` varchar(20) COLLATE latin1_spanish_ci NOT NULL,
  `activo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `series_facturas` (
  `codigo` int(255) NOT NULL,
  `serie` varchar(2) COLLATE latin1_spanish_ci NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `activo` enum('SI','NO') COLLATE latin1_spanish_ci NOT NULL,
  `codigoEmisorSerie` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `vencimientos_en_remesas` (
  `codigo` int(255) NOT NULL,
  `codigoRemesa` int(255) DEFAULT NULL,
  `codigoVencimiento` int(255) DEFAULT NULL,
  `importe` double(20,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `vencimientos_facturas` (
  `codigo` int(255) NOT NULL,
  `codigoFactura` int(255) DEFAULT NULL,
  `medioPago` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fechaVencimiento` date NOT NULL,
  `fechaDevolucion` date NOT NULL,
  `gastosDevolucion` double(20,2) NOT NULL,
  `importe` double(20,2) NOT NULL,
  `estado` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `concepto` varchar(255) COLLATE latin1_spanish_ci DEFAULT NULL,
  `llamadaAsesoria` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `llamadaEmpresa` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `motivoDevolucion` varchar(100) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


ALTER TABLE `cobros_facturas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFactura` (`codigoFactura`),
  ADD KEY `codigoCuentaPropia` (`codigoCuentaPropia`),
  ADD KEY `fk_cobros_facturas3` (`codigoRemesa`);

ALTER TABLE `cuentas_propias`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `emisores`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `facturas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoCliente` (`codigoCliente`),
  ADD KEY `codigoSerieFactura` (`codigoSerieFactura`),
  ADD KEY `fk_facturas6` (`codigoEmisor`),
  ADD KEY `codigoFacturaAsociada` (`codigoFacturaAsociada`);

ALTER TABLE `remesas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoCuentaPropia` (`codigoCuentaPropia`);

ALTER TABLE `series_facturas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_series_facturas` (`codigoEmisorSerie`);

ALTER TABLE `vencimientos_en_remesas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoRemesa` (`codigoRemesa`),
  ADD KEY `codigoVencimiento` (`codigoVencimiento`);

ALTER TABLE `vencimientos_facturas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoConcepto` (`concepto`),
  ADD KEY `codigoFactura` (`codigoFactura`);


ALTER TABLE `cobros_facturas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `cuentas_propias`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `emisores`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `facturas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `remesas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `series_facturas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `vencimientos_en_remesas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `vencimientos_facturas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `cobros_facturas`
  ADD CONSTRAINT `fk_cobros_facturas` FOREIGN KEY (`codigoFactura`) REFERENCES `facturas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cobros_facturas2` FOREIGN KEY (`codigoCuentaPropia`) REFERENCES `cuentas_propias` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cobros_facturas3` FOREIGN KEY (`codigoRemesa`) REFERENCES `remesas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `facturas`
  ADD CONSTRAINT `fk_facturas` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturas6` FOREIGN KEY (`codigoSerieFactura`) REFERENCES `series_facturas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturas7` FOREIGN KEY (`codigoEmisor`) REFERENCES `emisores` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `remesas`
  ADD CONSTRAINT `fk_remesas` FOREIGN KEY (`codigoCuentaPropia`) REFERENCES `cuentas_propias` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `series_facturas`
  ADD CONSTRAINT `fk_series_facturas` FOREIGN KEY (`codigoEmisorSerie`) REFERENCES `emisores` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `vencimientos_en_remesas`
  ADD CONSTRAINT `fk_facturas_en_remesas2` FOREIGN KEY (`codigoVencimiento`) REFERENCES `vencimientos_facturas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturas_en_remesas3` FOREIGN KEY (`codigoRemesa`) REFERENCES `remesas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `vencimientos_facturas`
  ADD CONSTRAINT `fk_vencimientos_facturas` FOREIGN KEY (`codigoFactura`) REFERENCES `facturas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;



-- Creación de la tabla contratos_en_facturas:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `contratos_en_facturas` (
  `codigo` int(255) NOT NULL,
  `codigoContrato` int(255) DEFAULT NULL,
  `codigoFactura` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `contratos_en_facturas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_contratos_en_facturas` (`codigoContrato`),
  ADD KEY `fk_contratos_en_facturas2` (`codigoFactura`);


ALTER TABLE `contratos_en_facturas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `contratos_en_facturas`
  ADD CONSTRAINT `fk_contratos_en_facturas2` FOREIGN KEY (`codigoFactura`) REFERENCES `facturas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_contratos_en_facturas` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Modificado el campo numRM de la tabla ofertas para pasarlo de DOUBLE a INT:
ALTER TABLE `ofertas` CHANGE `numRM` `numRM` INT(10) NOT NULL;

-- Añadido campo aceptado a la tabla contratos
ALTER TABLE `contratos` ADD `aceptado` ENUM('SI','NO') NOT NULL DEFAULT 'NO' AFTER `horasPrevistas`;

-- Eliminados de campos en la tabla clientes
ALTER TABLE `clientes`
  DROP `EMPSPA`,
  DROP `EMPRESPCONCONTRATO`,
  DROP `EMPPDTERESP`,
  DROP `EMPPPTADO`,
  DROP `EMPUSUIDMOD`,
  DROP `EMPFHORAMOD`;
ALTER TABLE `clientes`
  DROP `EMPCONTRATADO`,
  DROP `EMPCLAVE`;
ALTER TABLE clientes DROP FOREIGN KEY clientes_comercial;
ALTER TABLE `clientes` DROP `comercial`;
ALTER TABLE `clientes`
  DROP `EMPSADMIN`,
  DROP `EMPSTEC`,
  DROP `EMPSPRL`;

--
-- Estructura de tabla para la tabla `riesgos_tipos`
--

CREATE TABLE `riesgos_tipos` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `riesgos_tipos`
--

INSERT INTO `riesgos_tipos` (`codigo`, `nombre`) VALUES
(1, 'Accidentes'),
(2, 'Enfermedad profesional'),
(3, 'Fatiga'),
(4, 'InsastifacciÃ³n');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `riesgos_tipos`
--
ALTER TABLE `riesgos_tipos`
  ADD PRIMARY KEY (`codigo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `riesgos_tipos`
--
ALTER TABLE `riesgos_tipos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;

-- Relacionar la tabla riesgos y a la tabla riesgos_tipos
ALTER TABLE `riesgos` CHANGE `tipo` `tipo` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_spanish_ci NULL;
UPDATE riesgos SET tipo='1' WHERE tipo='ACCIDENTES';
UPDATE riesgos SET tipo='2' WHERE tipo='ENFERMEDAD';
UPDATE riesgos SET tipo='3' WHERE tipo='FATIGA';
UPDATE riesgos SET tipo='4' WHERE tipo='INSATISFACCION';
ALTER TABLE `riesgos` CHANGE `tipo` `tipo` INT(255) NULL DEFAULT NULL;
ALTER TABLE `riesgos` ADD CONSTRAINT `riesgos_tipo` FOREIGN KEY (`tipo`) REFERENCES `anesco2`.`riesgos_tipos`(`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;

-- Creación del campo mostrar, para saber si la oferta se muestra o no (porque es un contrato sin oferta)
ALTER TABLE `ofertas` ADD `mostrar` ENUM('SI','NO') NOT NULL DEFAULT 'SI' AFTER `importeRMExtra`;

-- Campo para saber si la evaluación se genera en el informe o no
ALTER TABLE `evaluacion_general` ADD `checkActivo` ENUM('SI','NO') NOT NULL DEFAULT 'SI' AFTER `otros`;


-- Añadido el campo eliminado a la tabla ofertas:
ALTER TABLE `ofertas` ADD `eliminado` ENUM('NO','SI') NOT NULL AFTER `mostrar`;

-- Añadido el campo eliminado a la tabla contratos:
ALTER TABLE `contratos` ADD `eliminado` ENUM('NO','SI') NOT NULL AFTER `tecnico`;

-- 11/10/2017 Emilio

ALTER TABLE `clientes` ADD `eliminado` ENUM('NO','SI') CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL AFTER `EMPIBAN`;

ALTER TABLE `formularioprl` ADD `eliminado` ENUM('NO','SI') CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL AFTER `ficheroPlano`;

ALTER TABLE `facturas` ADD `eliminado` ENUM('NO','SI') CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL AFTER `facturaEnviada`;

ALTER TABLE `usuarios` CHANGE `tipo` `tipo` ENUM('ADMIN','COMERCIAL','ADMINISTRACION','CONSULTORIA','FORMACION','ATENCION','TELECONCERTADOR','TECNICO','COLABORADOR','ASESOR','CLIENTE','EMPLEADO','FACTURACION') CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `anesco2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sentencias_sql`
--

CREATE TABLE IF NOT EXISTS `sentencias_sql` (
`codigo` int(255) NOT NULL,
  `codigoUsuario` int(255) DEFAULT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `sentencia` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `sentencias_sql`
--
ALTER TABLE `sentencias_sql`
 ADD PRIMARY KEY (`codigo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `sentencias_sql`
--
ALTER TABLE `sentencias_sql`
MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- 20/10/2017 Emilio

ALTER TABLE `ofertas` CHANGE `fase` `rechazada` ENUM('NO','SI') CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;

ALTER TABLE `ofertas` ADD `fechaRechazada` DATE NOT NULL AFTER `rechazada`;

ALTER TABLE `ofertas` ADD `motivosRechazo` TINYTEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL AFTER `fechaRechazada`;

-- Añadido el campo especialidadTecnica a la tabla ofertas:
ALTER TABLE `ofertas` ADD `especialidadTecnica` VARCHAR(100) NOT NULL AFTER `eliminado`;


-- Creación de la tabla formas_pago:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `formas_pago` (
  `codigo` int(255) NOT NULL,
  `forma` varchar(255) NOT NULL,
  `plazos` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `formas_pago`
  ADD PRIMARY KEY (`codigo`);


ALTER TABLE `formas_pago`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

-- Añadido el campo eliminado a la tabla formas_pago:
ALTER TABLE `formas_pago` ADD `eliminado` ENUM('NO','SI') NOT NULL AFTER `plazos`;


-- Añadido el campo codigoFormaPago a la tabla contratos:
ALTER TABLE `contratos` ADD `codigoFormaPago` INT(255) NULL AFTER `checkNoRenovar`, ADD INDEX `fk_contratos3` (`codigoFormaPago`);
ALTER TABLE `contratos` DROP FOREIGN KEY `contratos_tecnico_ibfk_1`; 
ALTER TABLE `contratos` ADD CONSTRAINT `contratos_tecnico_ibfk_1` FOREIGN KEY (`tecnico`) REFERENCES `usuarios`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE; 
ALTER TABLE `contratos` ADD CONSTRAINT `fk_contratos3` FOREIGN KEY (`codigoFormaPago`) REFERENCES `formas_pago`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Eliminados el campo formaPago de la tabla ofertas:
ALTER TABLE `ofertas` DROP `formaPago`;

-- Modificado el campo codigoCliente de la tabla ofertas para que pueda ser NULL:
ALTER TABLE `ofertas` CHANGE `codigoCliente` `codigoCliente` INT(255) NULL;
ALTER TABLE `ofertas` DROP FOREIGN KEY `ofertas_clientes_ibfk_1`; 
ALTER TABLE `ofertas` ADD CONSTRAINT `ofertas_clientes_ibfk_1` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Eliminado el campo medioPago de la tabla vencimientos_facturas y sustituido por codigoFormaPago:
ALTER TABLE `vencimientos_facturas` DROP `medioPago`;
ALTER TABLE `vencimientos_facturas` ADD `codigoFormaPago` INT(255) NULL AFTER `codigoFactura`, ADD INDEX `fk_vencimientos_facturas` (`codigoFormaPago`);
ALTER TABLE `vencimientos_facturas` ADD CONSTRAINT `fk_vencimientos_facturas2` FOREIGN KEY (`codigoFormaPago`) REFERENCES `formas_pago`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Eliminado el campo medioPago de la tabla cobros_facturas y sustituido por codigoFormaPago:
ALTER TABLE `cobros_facturas` DROP `medioPago`;
ALTER TABLE `cobros_facturas` ADD `codigoFormaPago` INT(255) NULL AFTER `codigoFactura`, ADD INDEX `fk_cobros_facturas4` (`codigoFormaPago`);
ALTER TABLE `cobros_facturas` ADD CONSTRAINT `fk_cobros_facturas4` FOREIGN KEY (`codigoFormaPago`) REFERENCES `formas_pago`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Eliminado el campo facturaEnviada de la tabla facturas:
ALTER TABLE `facturas` DROP `facturaEnviada`;

-- Eliminado el campo anulada de la tabla facturas:
ALTER TABLE `facturas` DROP `anulada`;

-- Eliminado el campo aceptado de la tabla contratos:
ALTER TABLE `contratos` DROP `aceptado`;

-- Creación de la tabla facturas_reconocimientos_medicos
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `facturas_reconocimientos_medicos` (
  `codigo` int(255) NOT NULL,
  `codigoEmisor` int(255) DEFAULT NULL,
  `fecha` date NOT NULL,
  `codigoSerieFactura` int(255) DEFAULT NULL,
  `numero` int(255) NOT NULL,
  `codigoContrato` int(255) DEFAULT NULL,
  `total` double(20,2) NOT NULL,
  `observaciones` text NOT NULL,
  `facturaCobrada` enum('NO','SI') NOT NULL,
  `eliminado` enum('NO','SI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `facturas_reconocimientos_medicos`
  ADD PRIMARY KEY (`codigo`),
  ADD UNIQUE KEY `fk_facturas_reconocimientos_medicos` (`codigoEmisor`),
  ADD KEY `fk_facturas_reconocimientos_medicos2` (`codigoSerieFactura`),
  ADD KEY `fk_facturas_reconocimientos_medicos3` (`codigoContrato`);


ALTER TABLE `facturas_reconocimientos_medicos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `facturas_reconocimientos_medicos`
  ADD CONSTRAINT `fk_facturas_reconocimientos_medicos3` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturas_reconocimientos_medicos` FOREIGN KEY (`codigoEmisor`) REFERENCES `emisores` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturas_reconocimientos_medicos2` FOREIGN KEY (`codigoSerieFactura`) REFERENCES `facturas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Creación de la tabla reconocimientos_medicos_en_facturas:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `reconocimientos_medicos_en_facturas` (
  `codigo` int(255) NOT NULL,
  `codigoEmpleado` int(255) DEFAULT NULL,
  `incluidoContrato` enum('SI','NO') NOT NULL,
  `precio` double(20,2) NOT NULL,
  `codigoFacturaReconocimientoMedico` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `reconocimientos_medicos_en_facturas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_reconocimientos_medicos_en_facturas` (`codigoEmpleado`),
  ADD KEY `fk_reconocimientos_medicos_en_facturas2` (`codigoFacturaReconocimientoMedico`);


ALTER TABLE `reconocimientos_medicos_en_facturas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `reconocimientos_medicos_en_facturas`
  ADD CONSTRAINT `fk_reconocimientos_medicos_en_facturas2` FOREIGN KEY (`codigoFacturaReconocimientoMedico`) REFERENCES `facturas_reconocimientos_medicos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_reconocimientos_medicos_en_facturas` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Corregida la relación del campo codigoSerieFactura de la tabla facturas_reconocimientos_medicos:
ALTER TABLE `facturas_reconocimientos_medicos` DROP FOREIGN KEY `fk_facturas_reconocimientos_medicos2`; ALTER TABLE `facturas_reconocimientos_medicos` ADD CONSTRAINT `fk_facturas_reconocimientos_medicos2` FOREIGN KEY (`codigoSerieFactura`) REFERENCES `series_facturas`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo fechaRRMM a la tabla facturas_reconocimientos_medicos:
ALTER TABLE `facturas_reconocimientos_medicos` ADD `fechaRRMM` DATE NOT NULL AFTER `fecha`;

-- Añadido el campo facturaContratos a la tabla facturas_reconocimientos_medicos:
ALTER TABLE `facturas_reconocimientos_medicos` ADD `facturaContratos` ENUM('SI','NO') NOT NULL AFTER `numero`;

-- Añadido el campo conceptoManualFactura a la tabla facturas_reconocimientos_medicos:
ALTER TABLE `facturas_reconocimientos_medicos` ADD `conceptoManualFactura` TEXT NOT NULL AFTER `codigoContrato`;

-- Modificado el índice fk_facturas_reconocimientos_medicos2 de la tabla facturas_reconocimientos_medicos para que sea INDEX y no UNIQUE (HACER ONLINE!!)

-- Añadido el campo codigoClienteParaConceptoManual a la tabla facturas_reconocimientos_medicos:
ALTER TABLE `facturas_reconocimientos_medicos` ADD `codigoClienteParaConceptoManual` INT(255) NULL AFTER `conceptoManualFactura`, ADD INDEX `fk_facturas_reconocimientos_medicos4` (`codigoClienteParaConceptoManual`);
ALTER TABLE `facturas_reconocimientos_medicos` ADD CONSTRAINT `fk_facturas_reconocimientos_medicos4` FOREIGN KEY (`codigoClienteParaConceptoManual`) REFERENCES `clientes`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo eliminado a la tabla vencimientos_facturas:
ALTER TABLE `vencimientos_facturas` ADD `eliminado` ENUM('NO','SI') NOT NULL AFTER `motivoDevolucion`;

-- Modificado el campo plazos de la tabla formas_pago por el campo numPagos:
ALTER TABLE `formas_pago` CHANGE `plazos` `numPagos` INT(100) NOT NULL;

-- Añadido el campo bic a la tabla clientes:
ALTER TABLE `clientes` ADD `bic` VARCHAR(11) NOT NULL AFTER `EMPIBAN`;

-- Añadido el campo remesable a la tabla formas_pago:
ALTER TABLE `formas_pago` ADD `remesable` ENUM('NO','SI') NOT NULL AFTER `numPagos`;

-- Modificado el campo activo de la tabla remesas por eliminado:
ALTER TABLE `remesas` CHANGE `activo` `eliminado` ENUM('SI','NO') CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;



-- Creación de la tabla reconocimientos_medicos:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `reconocimientos_medicos` (
  `codigo` int(255) NOT NULL,
  `codigoEmpleado` int(255) DEFAULT NULL,
  `codigoContrato` int(255) DEFAULT NULL,
  `checkClasificacion0` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion1` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion2` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion3` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion4` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion5` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion6` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion7` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion8` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion9` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkClasificacion10` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tipoReconocimiento` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `periodicidad` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `puestoAnterior` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tiempoPuestoAnterior` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `riesgosExtralaborales` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `numero` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `riesgoPuesto` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `talla` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `peso` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `imc` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `sistolica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `diastolica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `frecuenciaCardiaca` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `alchohol` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tipoFumador` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `aniosFumando` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tabacoDia` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `cafeDia` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tratamientoHabitual` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkVacunaciones0` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkVacunaciones1` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkVacunaciones2` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkVacunaciones3` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `ejercicio` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `alimentacion` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `calidadSuenio` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `historiaActual` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `medicacionHabitual` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `trastornosCongenitos` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fracturas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `riesgosLaborales` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `antecedentesPersonales` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `alergias` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `toleraEpis` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `antecedentesFamiliares` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `minusvaliaReconocida` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkEpis` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `bajasProlongadas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `inspeccionGeneral` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `orl` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `oftalmologia` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `exploracionNeurologica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `pielMucosas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `auscultacionPulmonar` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `aparatoDigestivo` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `extremidades` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `auscultacionCardiaca` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `sistemaUrogenital` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `aparatoLocomotor` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `otros` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkAgudezaVisual` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `agudeza` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `observacionesVision` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `agudezaVisualCorregida` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `traeGafasLentillas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `tipoGafasLentillas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `anioUltimaGraduacion` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `frecuenciaUso` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `percepcionColores` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision1` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision2` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision3` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision4` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision5` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision6` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision7` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision8` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision9` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision10` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision11` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioVision12` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkEspirometria` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `valoracionEspirometrica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `cv` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fvc` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fev1` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `itiff` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkAudiometria` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `audiometria` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `observacionesAudiometria` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria1` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria2` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria3` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria4` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria5` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria6` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria7` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria8` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria9` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria10` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria11` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria12` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria13` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `estudioAudiometria14` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkElectrocardiograma` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `electrocardiograma` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkHemograma` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `hemograma` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkBioquimica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `bioquimica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkOtrasPruebas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `otrasPruebas` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkOtrasPruebasFuncionales` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `otrasPruebasFuncionales` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkOrina` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `orina` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `analitica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkRadiologia` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `radiologia` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkDinamometria` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `dinamometria` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `rxTorax` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `apto` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `checkObjetivan` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `debeAcudirMedico` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `profesionalExterno` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `observacionesNoImprimibles` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `recomendaciones` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `eliminado` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


ALTER TABLE `reconocimientos_medicos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoEmpleado` (`codigoEmpleado`),
  ADD KEY `codigoContrato` (`codigoContrato`);


ALTER TABLE `reconocimientos_medicos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `reconocimientos_medicos`
  ADD CONSTRAINT `fk_reconocimientos_medicos` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_reconocimientos_medicos2` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Creación de la tabla epis_en_reconocimiento_medico:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `epis_en_reconocimiento_medico` (
  `codigo` int(255) NOT NULL,
  `codigoReconocimientoMedico` int(255) DEFAULT NULL,
  `codigoEPI` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `epis_en_reconocimiento_medico`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_epis_en_reconocimiento_medico` (`codigoReconocimientoMedico`),
  ADD KEY `fk_epis_en_reconocimiento_medico2` (`codigoEPI`);


ALTER TABLE `epis_en_reconocimiento_medico`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `epis_en_reconocimiento_medico`
  ADD CONSTRAINT `fk_epis_en_reconocimiento_medico2` FOREIGN KEY (`codigoEPI`) REFERENCES `epis` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_epis_en_reconocimiento_medico` FOREIGN KEY (`codigoReconocimientoMedico`) REFERENCES `reconocimientos_medicos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo cerrado a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `cerrado` ENUM('NO','SI') NOT NULL AFTER `eliminado`;

-- Añadido el campo sexo a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `sexo` VARCHAR(1) NOT NULL AFTER `riesgoPuesto`;

-- Añadido el campo ficheroAnalitica a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `ficheroAnalitica` VARCHAR(50) NOT NULL AFTER `analitica`;

-- Añadido el campo bloqueado a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `bloqueado` ENUM('NO','SI') NOT NULL AFTER `codigoContrato`;


-- Modificado el campo tipo de la tabla usuarios para añadir los perfiles MEDICO y ENFERMERIA:
ALTER TABLE `usuarios` CHANGE `tipo` `tipo` ENUM('ADMIN','COMERCIAL','ADMINISTRACION','CONSULTORIA','FORMACION','ATENCION','TELECONCERTADOR','TECNICO','COLABORADOR','ASESOR','CLIENTE','EMPLEADO','FACTURACION','MEDICO','ENFERMERIA') CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;

-- Añadidos los campos tipoEspecialista, numeroColegiado y ficheroFirma a la tabla usuarios:
ALTER TABLE `usuarios` ADD `tipoEspecialista` VARCHAR(255) NOT NULL AFTER `teleconcertador`, ADD `numeroColegiado` VARCHAR(50) NOT NULL AFTER `tipoEspecialista`, ADD `ficheroFirma` VARCHAR(50) NOT NULL AFTER `numeroColegiado`;

-- Añadido el campo codigoUsuario a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `codigoUsuario` INT(255) NULL AFTER `cerrado`, ADD INDEX `fk_reconocimientos_medicos3` (`codigoUsuario`);
ALTER TABLE `reconocimientos_medicos` ADD CONSTRAINT `fk_reconocimientos_medicos3` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Corregido el nombre del campo alchohol de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` CHANGE `alchohol` `alcohol` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;

-- Añadidos los campos checkClasificacionPuesto a la tabla puestos_trabajo:
ALTER TABLE `puestos_trabajo` ADD `checkClasificacionPuesto0` ENUM('NO','SI') NOT NULL AFTER `zonaTrabajo`, ADD `checkClasificacionPuesto1` ENUM('NO','SI') NOT NULL AFTER `checkClasificacionPuesto0`, ADD `checkClasificacionPuesto2` ENUM('NO','SI') NOT NULL AFTER `checkClasificacionPuesto1`, ADD `checkClasificacionPuesto3` ENUM('NO','SI') NOT NULL AFTER `checkClasificacionPuesto2`, ADD `checkClasificacionPuesto4` ENUM('NO','SI') NOT NULL AFTER `checkClasificacionPuesto3`, ADD `checkClasificacionPuesto5` ENUM('NO','SI') NOT NULL AFTER `checkClasificacionPuesto4`, ADD `checkClasificacionPuesto6` ENUM('NO','SI') NOT NULL AFTER `checkClasificacionPuesto5`, ADD `checkClasificacionPuesto7` ENUM('NO','SI') NOT NULL AFTER `checkClasificacionPuesto6`, ADD `checkClasificacionPuesto8` ENUM('NO','SI') NOT NULL AFTER `checkClasificacionPuesto7`, ADD `checkClasificacionPuesto9` ENUM('NO','SI') NOT NULL AFTER `checkClasificacionPuesto8`;
ALTER TABLE `puestos_trabajo` ADD `checkClasificacionPuesto10` ENUM('NO','SI') NOT NULL AFTER `checkClasificacionPuesto9`;

-- Añadido el campo editable a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `editable` ENUM('SI','NO') NOT NULL AFTER `cerrado`;

-- Creación de la tabla instalaciones:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `instalaciones` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `instalaciones`
  ADD PRIMARY KEY (`codigo`);


ALTER TABLE `instalaciones`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

-- Añadido el campo eliminado a la tabla instalaciones:
ALTER TABLE `instalaciones` ADD `eliminado` ENUM('NO','SI') NOT NULL AFTER `nombre`;

-- Creación de la tabla instalaciones_formulario_prl:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `instalaciones_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoFormularioPRL` int(255) DEFAULT NULL,
  `codigoInstalacion` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `instalaciones_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_instalaciones_formulario_prl` (`codigoFormularioPRL`),
  ADD KEY `fk_instalaciones_formulario_prl2` (`codigoInstalacion`);


ALTER TABLE `instalaciones_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `instalaciones_formulario_prl`
  ADD CONSTRAINT `fk_instalaciones_formulario_prl2` FOREIGN KEY (`codigoInstalacion`) REFERENCES `instalaciones` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_instalaciones_formulario_prl` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioPRL` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Eliminado el campo corrientePago de la tabla formularioPRL:
ALTER TABLE `formularioPRL` DROP `corrientePago`;


-- Añadidos los campos fechaInicioOferta, fechaFinOferta y codigoFormaPagoOferta a la tabla ofertas:
ALTER TABLE `ofertas` ADD `fechaInicioOferta` DATE NOT NULL AFTER `especialidadTecnica`, ADD `fechaFinOferta` DATE NOT NULL AFTER `fechaInicioOferta`, ADD `codigoFormaPagoOferta` INT(255) NULL AFTER `fechaFinOferta`, ADD INDEX `fk_ofertas3` (`codigoFormaPagoOferta`);
ALTER TABLE `ofertas` ADD CONSTRAINT `fk_ofertas3` FOREIGN KEY (`codigoFormaPagoOferta`) REFERENCES `formas_pago`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;



-- Añadido el campo codigoUsuarioTecnico a la tabla formularioPRL:
ALTER TABLE `formularioPRL` ADD `codigoUsuarioTecnico` INT(255) NULL AFTER `eliminado`, ADD INDEX `fk_formularioPRL` (`codigoUsuarioTecnico`);
ALTER TABLE `formularioPRL` ADD CONSTRAINT `fk_formularioPRL` FOREIGN KEY (`codigoUsuarioTecnico`) REFERENCES `usuarios`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo codigoCentroTrabajo a la tabla puestos_trabajo_formulario_prl:
ALTER TABLE `puestos_trabajo_formulario_prl` ADD `codigoCentroTrabajo` INT(255) NULL AFTER `sensible`, ADD INDEX `fk_puestos_trabajo_formulario_prl3` (`codigoCentroTrabajo`);
ALTER TABLE `puestos_trabajo_formulario_prl` ADD CONSTRAINT `fk_puestos_trabajo_formulario_prl3` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo codigoCentroTrabajo a la tabla materias_primas_formulario_prl:
ALTER TABLE `materias_primas_formulario_prl` ADD `codigoCentroTrabajo` INT(255) NULL AFTER `codigoFormularioPRL`, ADD INDEX `fk_materias_primas_formulario_prl2` (`codigoCentroTrabajo`);
ALTER TABLE `materias_primas_formulario_prl` ADD CONSTRAINT `fk_materias_primas_formulario_prl2` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo codigoCentroTrabajo a la tabla instalaciones_formulario_prl:
ALTER TABLE `instalaciones_formulario_prl` ADD `codigoCentroTrabajo` INT(255) NULL AFTER `codigoInstalacion`, ADD INDEX `fk_instalaciones_formulario_prl3` (`codigoCentroTrabajo`);
ALTER TABLE `instalaciones_formulario_prl` ADD CONSTRAINT `fk_instalaciones_formulario_prl3` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo codigoCentroTrabajo a la tabla extintores_formulario_prl:
ALTER TABLE `extintores_formulario_prl` ADD `codigoCentroTrabajo` INT(255) NULL AFTER `ubicacion`, ADD INDEX `fk_extintores_formulario_prl3` (`codigoCentroTrabajo`);
ALTER TABLE `extintores_formulario_prl` ADD CONSTRAINT `fk_extintores_formulario_prl2` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo codigoCentroTrabajo a la tabla bie_formulario_prl:
ALTER TABLE `bie_formulario_prl` ADD `codigoCentroTrabajo` INT(255) NULL AFTER `ubicacion`, ADD INDEX `fk_bie_formulario_prl2` (`codigoCentroTrabajo`);
ALTER TABLE `bie_formulario_prl` ADD CONSTRAINT `bie_formulario_prl2` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Añadido el campo codigoCentroTrabajo a la tabla botiquines_formulario_prl:
ALTER TABLE `botiquines_formulario_prl` ADD `codigoCentroTrabajo` INT(255) NULL AFTER `senializado`, ADD INDEX `fk_botiquines_formulario_prl2` (`codigoCentroTrabajo`);
ALTER TABLE `botiquines_formulario_prl` ADD CONSTRAINT `botiquines_formulario_prl_ibfk_2` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Añadido el campo codigoCentroTrabajo a la tabla locales_formulario_prl:
ALTER TABLE `locales_formulario_prl` ADD `codigoCentroTrabajo` INT(255) NULL AFTER `senializado`, ADD INDEX `fk_locales_formulario_prl` (`codigoCentroTrabajo`);
ALTER TABLE `locales_formulario_prl` ADD CONSTRAINT `locales_formulario_prl_ibfk_2` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Añadido el campo codigoCentroTrabajo a la tabla medios_humanos_formulario_prl:
ALTER TABLE `medios_humanos_formulario_prl` ADD `codigoCentroTrabajo` INT(25) NULL AFTER `ubicacion`, ADD INDEX `fk_medios_humanos_formulario_prl` (`codigoCentroTrabajo`);
ALTER TABLE `medios_humanos_formulario_prl` ADD CONSTRAINT `medios_humanos_formulario_prl_ibfk_2` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Eliminado el campo firmaTecnico de la tabla formularioPRL:
ALTER TABLE `formularioPRL` DROP `firmaTecnico`;

-- Eliminado el campo ficheroPlano de la tabla formularioPRL:
ALTER TABLE `formularioPRL` DROP `ficheroPlano`;

-- Añadido el campo trabajadorSensible a la tabla empleados:
ALTER TABLE `empleados` ADD `trabajadorSensible` ENUM('NO','SI') NOT NULL AFTER `ficheroFoto`;

-- Creación de la tabla empleados_sensibles_formulario_prl:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `empleados_sensibles_formulario_prl` (
  `codigo` int(255) NOT NULL,
  `codigoFormularioPRL` int(255) DEFAULT NULL,
  `codigoEmpleado` int(255) DEFAULT NULL,
  `codigoCentroTrabajo` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `empleados_sensibles_formulario_prl`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_empleados_sensibles_formulario_prl` (`codigoFormularioPRL`),
  ADD KEY `fk_empleados_sensibles_formulario_prl2` (`codigoEmpleado`),
  ADD KEY `fk_empleados_sensibles_formulario_prl3` (`codigoCentroTrabajo`);


ALTER TABLE `empleados_sensibles_formulario_prl`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `empleados_sensibles_formulario_prl`
  ADD CONSTRAINT `fk_empleados_sensibles_formulario_prl` FOREIGN KEY (`codigoCentroTrabajo`) REFERENCES `clientes_centros` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_empleados_sensibles_formulario_prl2` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_empleados_sensibles_formulario_prl3` FOREIGN KEY (`codigoFormularioPRL`) REFERENCES `formularioprl` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

-- Modificación de la tabla botiquines_formulario_prl para adaptarla al nuevo formato del formulario:
ALTER TABLE `botiquines_formulario_prl` CHANGE `ubicacion` `ubicacion` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `botiquines_formulario_prl` ADD `numero` INT(50) NOT NULL AFTER `senializado`, ADD `revisado` ENUM('NO','SI') NOT NULL AFTER `numero`;

-- Modificación de la tabla locales_formulario_prl para adaptarla al nuevo formato del formulario:
ALTER TABLE `locales_formulario_prl` CHANGE `ubicacion` `ubicacion` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `locales_formulario_prl` ADD `numero` INT(50) NOT NULL AFTER `senializado`, ADD `revisado` ENUM('NO','SI') NOT NULL AFTER `numero`;

-- Modificación de la tabla medios_humanos_formulario_prl para adaptarla al nuevo formato del formulario:
ALTER TABLE `medios_humanos_formulario_prl` CHANGE `ubicacion` `ubicacion` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `medios_humanos_formulario_prl` ADD `numero` INT(50) NOT NULL AFTER `ubicacion`, ADD `revisado` ENUM('NO','SI') NOT NULL AFTER `numero`, ADD `senializado` ENUM('NO','SI') NOT NULL AFTER `revisado`;

-- Creación de la tabla tipos_tareas:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `tipos_tareas` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `tipos_tareas`
  ADD PRIMARY KEY (`codigo`);


ALTER TABLE `tipos_tareas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;COMMIT;


-- Añadido el campo eliminado a la tabla tipos_tareas:
ALTER TABLE `tipos_tareas` ADD `eliminado` ENUM('NO','SI') NOT NULL AFTER `nombre`;

-- Añadidos los campos tiempoEmpleado y codigoContrato a la tabla tareas:
ALTER TABLE `tareas` ADD `tiempoEmpleado` INT(255) NOT NULL AFTER `codigoCliente`, ADD `codigoContrato` INT(255) NULL AFTER `tiempoEmpleado`, ADD INDEX `fk_tareas` (`codigoContrato`);
ALTER TABLE `tareas` ADD CONSTRAINT `fk_tareas` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo codigoCuentaPropiaOferta a la tabla ofertas:
ALTER TABLE `ofertas` ADD `codigoCuentaPropiaOferta` INT(255) NULL AFTER `codigoFormaPagoOferta`, ADD INDEX `fk_ofertas_6` (`codigoCuentaPropiaOferta`);
ALTER TABLE `ofertas` ADD CONSTRAINT `fk_ofertas_6` FOREIGN KEY (`codigoCuentaPropiaOferta`) REFERENCES `cuentas_propias`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo codigoCuentaPropiaContrato a la tabla contratos:
ALTER TABLE `contratos` ADD `codigoCuentaPropiaContrato` INT(255) NULL AFTER `codigoFormaPago`, ADD INDEX `fk_contratos2` (`codigoCuentaPropiaContrato`);
ALTER TABLE `contratos` ADD CONSTRAINT `fk_contratos2` FOREIGN KEY (`codigoCuentaPropiaContrato`) REFERENCES `cuentas_propias`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Eliminación de la tabla colaboradores vieja:
DROP TABLE `colaboradores`;

-- Creación de la nueva tabla colaboradores:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `colaboradores` (
  `codigo` int(255) NOT NULL,
  `razonSocial` varchar(255) NOT NULL,
  `cif` varchar(9) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `cp` varchar(5) NOT NULL,
  `localidad` varchar(255) NOT NULL,
  `provincia` varchar(100) NOT NULL,
  `contacto` varchar(255) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `comision` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `colaboradores`
  ADD PRIMARY KEY (`codigo`);


ALTER TABLE `colaboradores`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;COMMIT;

-- Añadido el campo eliminado a la tabla colaboradores:
ALTER TABLE `colaboradores` ADD `eliminado` ENUM('NO','SI') NOT NULL AFTER `comision`;

-- Añadido el campo mostrarCuentas a la tabla formas_pago:
ALTER TABLE `formas_pago` ADD `mostrarCuentas` ENUM('NO','SI') NOT NULL AFTER `remesable`;

-- Añadido el campo codigoColaborador a la tabla contratos:
ALTER TABLE `contratos` ADD `codigoColaborador` INT(255) NULL AFTER `codigoCuentaPropiaContrato`, ADD INDEX `fk_contratos4` (`codigoColaborador`);
ALTER TABLE `contratos` ADD CONSTRAINT `fk_contratos4` FOREIGN KFY (`codigoColaborador`) REFERENCES `colaboradores`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo renovado a la tabla contratos:
ALTER TABLE `contratos` ADD `renovado` ENUM('NO','SI') NOT NULL AFTER `eliminado`;


-- Añadido el campo contratoRevisado a la tabla contratos:
ALTER TABLE `contratos` ADD `contratoRevisado` ENUM('NO','SI') NOT NULL AFTER `checkNoRenovar`;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades_procesos`
--

CREATE TABLE `actividades_procesos` (
  `codigo` int(255) NOT NULL,
  `codigoActividad` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividades_procesos`
--
ALTER TABLE `actividades_procesos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoActividad` (`codigoActividad`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actividades_procesos`
--
ALTER TABLE `actividades_procesos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividades_procesos`
--
ALTER TABLE `actividades_procesos`
  ADD CONSTRAINT `actividades_procesos_ibfk` FOREIGN KEY (`codigoActividad`) REFERENCES `actividades` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;


-- Creación de la tabla tareas_parte_diario:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `tareas_parte_diario` (
  `codigo` int(255) NOT NULL,
  `codigoTarea` int(255) DEFAULT NULL,
  `codigoParteDiario` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `tareas_parte_diario`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_tareas_parte_diario` (`codigoTarea`),
  ADD KEY `fk_tareas_parte_diario2` (`codigoParteDiario`);


ALTER TABLE `tareas_parte_diario`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `tareas_parte_diario`
  ADD CONSTRAINT `fk_tareas_parte_diario` FOREIGN KEY (`codigoParteDiario`) REFERENCES `parte_diario` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tareas_parte_diario2` FOREIGN KEY (`codigoTarea`) REFERENCES `tareas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo codigoContrato a la tabla tareas_parte_diario:
ALTER TABLE `tareas_parte_diario` ADD `codigoContrato` INT(255) NULL AFTER `codigo`, ADD INDEX `fk_tareas_parte_diario3` (`codigoContrato`);
ALTER TABLE `tareas_parte_diario` ADD CONSTRAINT `fk_tareas_parte_diario3` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Creación de la tabla estados_recibos:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `estados_recibos` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `observaciones` text NOT NULL,
  `eliminado` enum('NO','SI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `estados_recibos`
  ADD PRIMARY KEY (`codigo`);


ALTER TABLE `estados_recibos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;


-- Sustituido el campo estado de la tabla vencimientos_facturas por codigoEstadoRecibo:
UPDATE vencimientos_facturas SET concepto=CONCAT(concepto,' - Estado original: ',estado);
ALTER TABLE `vencimientos_facturas` DROP `estado`;
ALTER TABLE `vencimientos_facturas` ADD `codigoEstadoRecibo` INT(255) NULL AFTER `eliminado`, ADD INDEX `fk_vencimientos_facturas3` (`codigoEstadoRecibo`);
ALTER TABLE `vencimientos_facturas` ADD CONSTRAINT `fk_vencimientos_facturas3` FOREIGN KEY (`codigoEstadoRecibo`) REFERENCES `estados_recibos`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


ALTER TABLE `clientes` ADD `ficheroLogo` VARCHAR(255) NOT NULL DEFAULT 'NO' AFTER `eliminado`;
ALTER TABLE `formularioPRL` ADD `ficheroOrganigrama` VARCHAR(255) NOT NULL DEFAULT 'NO' AFTER `codigoUsuarioTecnico`;


-- Creación de la tabla historicoVS:

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historicoVS`
--

CREATE TABLE `historicoVS` (
  `codigo` int(255) NOT NULL,
  `codigoEmpleado` int(255) NOT NULL,
  `estado` int(255) NOT NULL,
  `sexo` enum('V','M') COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `nlab` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `eliminado` enum('SI','NO') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO',
  `observaciones` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `historicoVS`
--
ALTER TABLE `historicoVS`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoEmpleado` (`codigoEmpleado`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `historicoVS`
--
ALTER TABLE `historicoVS`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `historicoVS`
--
ALTER TABLE `historicoVS`
  ADD CONSTRAINT `empleados_historico` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;


-- Habilitación de los RM bloqueados:
UPDATE reconocimientos_medicos SET editable='SI' WHERE eliminado='NO' AND cerrado='NO';


-- Creación de la tabla centros_medicos:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `centros_medicos` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `observaciones` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `centros_medicos`
  ADD PRIMARY KEY (`codigo`);


ALTER TABLE `centros_medicos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

-- Creación de la tabla salas_centros_medicos:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `salas_centros_medicos` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `codigoCentroMedico` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `salas_centros_medicos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_salas_centros_medicos` (`codigoCentroMedico`);


ALTER TABLE `salas_centros_medicos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `salas_centros_medicos`
  ADD CONSTRAINT `fk_salas_centros_medicos` FOREIGN KEY (`codigoCentroMedico`) REFERENCES `centros_medicos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Creación de la tabla profesionales_centros:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `profesionales_centros` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `observaciones` text NOT NULL,
  `codigoCentroMedico` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `profesionales_centros`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_profesionales_centros` (`codigoCentroMedico`);


ALTER TABLE `profesionales_centros`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `profesionales_centros`
  ADD CONSTRAINT `fk_profesionales_centros` FOREIGN KEY (`codigoCentroMedico`) REFERENCES `centros_medicos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo eliminado a las tablas centros_medicos y profesionales_centros:
ALTER TABLE `centros_medicos` ADD `eliminado` ENUM('NO','SI') NOT NULL AFTER `observaciones`;
ALTER TABLE `profesionales_centros` ADD `eliminado` ENUM('NO','SI') NOT NULL AFTER `codigoCentroMedico`;

-- Creación de la tabla citas:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `citas` (
  `codigo` int(255) NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFin` date NOT NULL,
  `horaInicio` time NOT NULL,
  `horaFin` time NOT NULL,
  `estado` enum('SINCONFIRMAR','CONFIRMADA') NOT NULL,
  `codigoEmpleado` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `citas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_citas` (`codigoEmpleado`);


ALTER TABLE `citas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `citas`
  ADD CONSTRAINT `fk_citas` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo codigoSala a la tabla citas:
ALTER TABLE `citas` ADD `codigoSala` INT(255) NULL AFTER `codigoEmpleado`, ADD INDEX `fk_citas2` (`codigoSala`);
ALTER TABLE `citas` ADD CONSTRAINT `fk_citas2` FOREIGN KEY (`codigoSala`) REFERENCES `salas_centros_medicos`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentosVS`
--

CREATE TABLE `documentosVS` (
  `codigo` int(255) NOT NULL,
  `codigoContrato` int(255) NOT NULL,
  `tipoDocumento` int(255) NOT NULL,
  `formulario` text COLLATE utf8_unicode_ci NOT NULL,
  `enviado` enum('SI','NO') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO',
  `fechaCreacion` date NOT NULL,
  `fechaEnvio` date NOT NULL,
  `visible` enum('SI','NO') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `documentosVS`
--
ALTER TABLE `documentosVS`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoContrato` (`codigoContrato`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `documentosVS`
--
ALTER TABLE `documentosVS`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `documentosVS`
--
ALTER TABLE `documentosVS`
  ADD CONSTRAINT `documentosvs_contratos_ibfk` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

UPDATE formularioPRL SET vs="NO";

ALTER TABLE `empleados` ADD `aprobado` ENUM('SI','NO') NOT NULL DEFAULT 'SI' AFTER `eliminado`;
ALTER TABLE `empleados` CHANGE `aprobado` `aprobado` ENUM('SI','NO','PENDIENTE') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'PENDIENTE';


-- Modificados los campos hemograma, bioquimica, otrasPruebas, otrasPruebasFuncionales y orina de la tabla reconocimientos_medicos para que sean de tio TEXT:
ALTER TABLE `reconocimientos_medicos` CHANGE `hemograma` `hemograma` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL, CHANGE `bioquimica` `bioquimica` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL, CHANGE `otrasPruebas` `otrasPruebas` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL, CHANGE `otrasPruebasFuncionales` `otrasPruebasFuncionales` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL, CHANGE `orina` `orina` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;

-- Añadido el campo advertencia a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `advertencia` ENUM('NO','SI') NOT NULL AFTER `recomendaciones`;




--
-- Estructura de tabla para la tabla `evaluacion_descripciones`
--

CREATE TABLE `evaluacion_descripciones` (
  `codigo` int(255) NOT NULL,
  `codigoRiesgo` int(255) NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `evaluacion_descripciones`
--
ALTER TABLE `evaluacion_descripciones`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoRiesgo` (`codigoRiesgo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `evaluacion_descripciones`
--
ALTER TABLE `evaluacion_descripciones`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `evaluacion_descripciones`
--
ALTER TABLE `evaluacion_descripciones`
  ADD CONSTRAINT `evluacion_descripciones` FOREIGN KEY (`codigoRiesgo`) REFERENCES `riesgos_evaluacion_general` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Estructura de tabla para la tabla `evaluacion_imagenes`
--

CREATE TABLE `evaluacion_imagenes` (
  `codigo` int(255) NOT NULL,
  `codigoDescripcion` int(255) NOT NULL,
  `ficheroImagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `evaluacion_imagenes`
--
ALTER TABLE `evaluacion_imagenes`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoDescripcion` (`codigoDescripcion`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `evaluacion_imagenes`
--
ALTER TABLE `evaluacion_imagenes`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `evaluacion_imagenes`
--
ALTER TABLE `evaluacion_imagenes`
  ADD CONSTRAINT `evaluacion_imagenes` FOREIGN KEY (`codigoDescripcion`) REFERENCES `evaluacion_descripciones` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;



ALTER TABLE `puestos_trabajo_formulario_prl` ADD `seccionPuestoTrabajo` VARCHAR(255) NOT NULL AFTER `sensible`;


-- Creación de la tabla riesgos_reconocimiento_medico:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `riesgos_reconocimiento_medico` (
  `codigo` int(255) NOT NULL,
  `codigoReconocimiento` int(255) DEFAULT NULL,
  `codigoRiesgo` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `riesgos_reconocimiento_medico`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_riesgos_reconocimiento_medico` (`codigoReconocimiento`),
  ADD KEY `fk_riesgos_reconocimiento_medico2` (`codigoRiesgo`);


ALTER TABLE `riesgos_reconocimiento_medico`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `riesgos_reconocimiento_medico`
  ADD CONSTRAINT `fk_riesgos_reconocimiento_medico` FOREIGN KEY (`codigoReconocimiento`) REFERENCES `reconocimientos_medicos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_riesgos_reconocimiento_medico2` FOREIGN KEY (`codigoRiesgo`) REFERENCES `riesgos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo codigoEmpleado a la tabla documentosVS:
ALTER TABLE `documentosVS` ADD `codigoEmpleado` INT(255) NULL AFTER `visible`, ADD INDEX `fk_documentosVS2` (`codigoEmpleado`);
ALTER TABLE `documentosVS` ADD CONSTRAINT `fk_documentosVS2` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo fechaSiguienteReconocimiento a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `fechaSiguienteReconocimiento` DATE NOT NULL AFTER `fecha`;

-- Añadido el campo codigoContrato a la tabla citas:
ALTER TABLE `citas` ADD `codigoContrato` INT(255) NULL AFTER `estado`, ADD INDEX `fk_citas3` (`codigoContrato`);
ALTER TABLE `citas` ADD CONSTRAINT `fk_citas3` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo ficheroER a la tabla contratos:
ALTER TABLE `contratos` ADD `ficheroER` VARCHAR(255) NOT NULL AFTER `fichero`;


-- Añadido el campo codugoUsuarioFactura a la tabla facturas:
ALTER TABLE `facturas` ADD `codigoUsuarioFactura` INT(255) NULL AFTER `eliminado`, ADD INDEX `fk_facturas_5` (`codigoUsuarioFactura`);
ALTER TABLE `facturas` ADD CONSTRAINT `fk_facturas_5` FOREIGN KEY (`codigoUsuarioFactura`) REFERENCES `usuarios`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo codigoUsuarioSubeDocumento a la tabla documentos_clientes:
ALTER TABLE `documentos_clientes` ADD `codigoUsuarioSubeDocumento` INT(255) NULL AFTER `visible`, ADD INDEX `fk_documentos_clientes2` (`codigoUsuarioSubeDocumento`);
ALTER TABLE `documentos_clientes` ADD CONSTRAINT `fk_documentos_clientes2` FOREIGN KEY (`codigoUsuarioSubeDocumento`) REFERENCES `usuarios`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo codigoUsuarioInforme a la tabla informes:
ALTER TABLE `informes` ADD `codigoUsuarioInforme` INT(255) NULL AFTER `codigoEvaluacion`, ADD INDEX `fk_informes2` (`codigoUsuarioInforme`);
ALTER TABLE `informes` ADD CONSTRAINT `fk_informes2` FOREIGN KEY (`codigoUsuarioInforme`) REFERENCES `usuarios`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo vigencia a la tabla contratos:
ALTER TABLE `contratos` ADD `vigencia` VARCHAR(50) NOT NULL AFTER `ficheroER`;

-- Creación de la tabla protocolos:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `protocolos` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `eliminado` enum('NO','SI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `protocolos`
  ADD PRIMARY KEY (`codigo`);


ALTER TABLE `protocolos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

-- Creación de las tablas necesarias para el gestor documental:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE `documentos` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `fichero` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `fechaModificacion` date NOT NULL,
  `descripcion` text COLLATE latin1_spanish_ci NOT NULL,
  `codigoUsuario` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `documentos_enviados` (
  `codigo` int(255) NOT NULL,
  `codigoDocumento` int(255) DEFAULT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `correo` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `asunto` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `documentos_pertenecen_espacios_trabajo` (
  `codigoDocumento` int(255) NOT NULL DEFAULT '0',
  `codigoEspacioTrabajo` int(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `espacios_trabajo` (
  `codigo` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `descripcion` text COLLATE latin1_spanish_ci NOT NULL,
  `codigoUsuario` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `etiquetas` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `color` varchar(7) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

CREATE TABLE `etiquetas_documentos` (
  `codigoDocumento` int(255) NOT NULL DEFAULT '0',
  `codigoEtiqueta` int(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


ALTER TABLE `documentos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoUsuario` (`codigoUsuario`);

ALTER TABLE `documentos_enviados`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `documentos_enviados_clientes_fk1` (`codigoDocumento`);

ALTER TABLE `documentos_pertenecen_espacios_trabajo`
  ADD PRIMARY KEY (`codigoDocumento`,`codigoEspacioTrabajo`),
  ADD KEY `documentos_pertenecen_ibfk_2` (`codigoEspacioTrabajo`);

ALTER TABLE `espacios_trabajo`
  ADD PRIMARY KEY (`codigo`),
  ADD UNIQUE KEY `codigoUsuario` (`codigoUsuario`);

ALTER TABLE `etiquetas`
  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `etiquetas_documentos`
  ADD PRIMARY KEY (`codigoDocumento`,`codigoEtiqueta`),
  ADD KEY `documentos_etiquetas_ibfk_2` (`codigoEtiqueta`);


ALTER TABLE `documentos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `documentos_enviados`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;
ALTER TABLE `espacios_trabajo`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
ALTER TABLE `etiquetas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE `documentos`
  ADD CONSTRAINT `documentos_ibfk_1` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `documentos_enviados`
  ADD CONSTRAINT `documentos_enviados_clientes_res1` FOREIGN KEY (`codigoDocumento`) REFERENCES `documentos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `documentos_pertenecen_espacios_trabajo`
  ADD CONSTRAINT `documentos_pertenecen_ibfk_1` FOREIGN KEY (`codigoDocumento`) REFERENCES `documentos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `documentos_pertenecen_ibfk_2` FOREIGN KEY (`codigoEspacioTrabajo`) REFERENCES `espacios_trabajo` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `espacios_trabajo`
  ADD CONSTRAINT `espacios_trabajo_ibfk_1` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE `etiquetas_documentos`
  ADD CONSTRAINT `documentos_etiquetas_ibfk_1` FOREIGN KEY (`codigoDocumento`) REFERENCES `documentos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `documentos_etiquetas_ibfk_2` FOREIGN KEY (`codigoEtiqueta`) REFERENCES `etiquetas` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- Añadido el campo secuelas a la tabla accidentes:
ALTER TABLE `accidentes` ADD `secuelas` TEXT NOT NULL AFTER `checkVisible`;


ALTER TABLE `funciones_formulario_prl` DROP `fecha`;
ALTER TABLE `funciones_formulario_prl` CHANGE `nombre` `codigoEmpleado` INT(255) NULL;
ALTER TABLE `funciones_formulario_prl` CHANGE `dni` `area` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE `funciones_formulario_prl` ADD INDEX (`codigoEmpleado`); 
UPDATE `funciones_formulario_prl` SET codigoEmpleado=NULL;
ALTER TABLE `funciones_formulario_prl` ADD CONSTRAINT `funciones_formulario_prl_2` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados`(`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;
ALTER TABLE evaluacion_general DROP FOREIGN KEY evaluacion_general_ibfk_1;

-- Añadido el campo codigoContrato a la tabla correos:
ALTER TABLE `correos` ADD `codigoContrato` INT(255) NULL AFTER `tipo`, ADD INDEX `fk_correos2` (`codigoContrato`);
ALTER TABLE `correos` ADD CONSTRAINT `fk_correos2` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo codigoPuestoTrabajo a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `codigoPuestoTrabajo` INT(255) NULL AFTER `codigoEmpleado`, ADD INDEX `fk_reconocimientos_medicos` (`codigoPuestoTrabajo`);

-- Añadidos los campos pestaniaIdentificacion, pestaniaRiesgos, pestaniaHabitos, pestaniaAntecedentes, pestaniaEPIs, pestaniaExploracion1, pestaniaExploracion2, pestaniaVision, pestaniaEspiro, pestaniaAudio, pestaniaHemo, pestaniaOtras, pestaniaRadio, pestaniaJuicio y pestaniaRecomendaciones a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos`  ADD `pestaniaIdentificacion` VARCHAR(11) NOT NULL  AFTER `codigoUsuario`,  ADD `pestaniaRiesgos` VARCHAR(11) NOT NULL  AFTER `pestaniaIdentificacion`,  ADD `pestaniaHabitos` VARCHAR(11) NOT NULL  AFTER `pestaniaRiesgos`,  ADD `pestaniaAntecedentes` VARCHAR(11) NOT NULL  AFTER `pestaniaHabitos`,  ADD `pestaniaEPIs` VARCHAR(11) NOT NULL  AFTER `pestaniaAntecedentes`,  ADD `pestaniaExploracion1` VARCHAR(11) NOT NULL  AFTER `pestaniaEPIs`,  ADD `pestaniaExploracion2` VARCHAR(11) NOT NULL  AFTER `pestaniaExploracion1`,  ADD `pestaniaVision` VARCHAR(11) NOT NULL  AFTER `pestaniaExploracion2`,  ADD `pestaniaEspiro` VARCHAR(11) NOT NULL  AFTER `pestaniaVision`,  ADD `pestaniaAudio` VARCHAR(11) NOT NULL  AFTER `pestaniaEspiro`,  ADD `pestaniaHemo` VARCHAR(11) NOT NULL  AFTER `pestaniaAudio`,  ADD `pestaniaOtras` VARCHAR(11) NOT NULL  AFTER `pestaniaHemo`,  ADD `pestaniaRadio` VARCHAR(11) NOT NULL  AFTER `pestaniaOtras`,  ADD `pestaniaJuicio` VARCHAR(11) NOT NULL  AFTER `pestaniaRadio`,  ADD `pestaniaRecomendaciones` VARCHAR(11) NOT NULL  AFTER `pestaniaJuicio`;

-- Añadidos los campos puestoAnterior2, tiempoPuestoAnterior2, puestoAnterior3 y tiempoPuestoAnterior3 a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `puestoAnterior2` VARCHAR(255) NOT NULL AFTER `tiempoPuestoAnterior`, ADD `tiempoPuestoAnterior2` VARCHAR(255) NOT NULL AFTER `puestoAnterior2`, ADD `puestoAnterior3` VARCHAR(255) NOT NULL AFTER `tiempoPuestoAnterior2`, ADD `tiempoPuestoAnterior3` VARCHAR(255) NOT NULL AFTER `puestoAnterior3`;

-- Añadido el campo otrasVacunaciones a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `otrasVacunaciones` VARCHAR(255) NOT NULL AFTER `pestaniaRecomendaciones`;


-- Creación de la tabla protocolizacion:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `protocolizacion` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) DEFAULT NULL,
  `codigoPuestoTrabajo` int(255) DEFAULT NULL,
  `riesgosAsociados` text NOT NULL,
  `analiticaPruebas` text NOT NULL,
  `periodicidad` varchar(20) NOT NULL,
  `otraPeriodicidad` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `protocolizacion`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_protocolizacion` (`codigoCliente`),
  ADD KEY `fk_protocolizacion2` (`codigoPuestoTrabajo`);


ALTER TABLE `protocolizacion`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `protocolizacion`
  ADD CONSTRAINT `fk_protocolizacion` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_protocolizacion2` FOREIGN KEY (`codigoPuestoTrabajo`) REFERENCES `puestos_trabajo` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Creación de la tabla protocolos_asociados_protocolizacion:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `protocolos_asociados_protocolizacion` (
  `codigo` int(255) NOT NULL,
  `codigoProtocolizacion` int(255) DEFAULT NULL,
  `codigoProtocolo` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `protocolos_asociados_protocolizacion`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_protocolos_asociados_protocolizacion` (`codigoProtocolizacion`),
  ADD KEY `fk_protocolos_asociados_protocolizacion2` (`codigoProtocolo`);


ALTER TABLE `protocolos_asociados_protocolizacion`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `protocolos_asociados_protocolizacion`
  ADD CONSTRAINT `fk_protocolos_asociados_protocolizacion` FOREIGN KEY (`codigoProtocolizacion`) REFERENCES `protocolizacion` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_protocolos_asociados_protocolizacion2` FOREIGN KEY (`codigoProtocolo`) REFERENCES `protocolos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Añadidos los campos nombrePuesto y eliminado a la tabla protocolizacion:
ALTER TABLE `protocolizacion` ADD `nombrePuesto` VARCHAR(255) NOT NULL AFTER `otraPeriodicidad`, ADD `eliminado` ENUM('NO','SI') NOT NULL AFTER `nombrePuesto`;

-- Eliminados los campos de clasificación de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` DROP `checkClasificacion0`, DROP `checkClasificacion1`, DROP `checkClasificacion2`, DROP `checkClasificacion3`, DROP `checkClasificacion4`, DROP `checkClasificacion5`, DROP `checkClasificacion6`, DROP `checkClasificacion7`, DROP `checkClasificacion8`, DROP `checkClasificacion9`, DROP `checkClasificacion10`;

-- Añadido el campo protocoloClasificacion a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `protocoloClasificacion` TEXT NOT NULL AFTER `riesgoPuesto`;

-- Eliminados los campos checkHipercolesteremia y checkHipertension de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` DROP `checkHipercolesteremia`, DROP `checkHipertension`;

-- Creación de la tabla enfermedades_antecedentes_reconocimientos_medicos:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `enfermedades_antecedentes_reconocimientos_medicos` (
  `codigo` int(255) NOT NULL,
  `codigoReconocimiento` int(255) DEFAULT NULL,
  `codigoEnfermedad` int(11) DEFAULT NULL,
  `checkAntecedentesPersonales` enum('NO','SI') NOT NULL,
  `checkAntecedentesFamiliares` enum('NO','SI') NOT NULL,
  `checkAdjuntarRecomendaciones` enum('NO','SI') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `enfermedades_antecedentes_reconocimientos_medicos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_enfermedades_antecedentes_reconocimientos_medicos` (`codigoReconocimiento`),
  ADD KEY `fk_enfermedades_antecedentes_reconocimientos_medicos2` (`codigoEnfermedad`);


ALTER TABLE `enfermedades_antecedentes_reconocimientos_medicos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `enfermedades_antecedentes_reconocimientos_medicos`
  ADD CONSTRAINT `fk_enfermedades_antecedentes_reconocimientos_medicos` FOREIGN KEY (`codigoEnfermedad`) REFERENCES `enfermedades` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_enfermedades_antecedentes_reconocimientos_medicos2` FOREIGN KEY (`codigoReconocimiento`) REFERENCES `reconocimientos_medicos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Eliminación de la tabla riesgos_reconocimiento_medico:
DROP TABLE `riesgos_reconocimiento_medico`;

-- Eliminación del campo riesgosLaborales de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` DROP `riesgosLaborales`;

-- Modificado el campo toleraEpis de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` CHANGE `toleraEpis` `intoleranciasEpis` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;

-- Añadido el campo toleraEpis a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `toleraEpis` ENUM('SI','NO') NOT NULL AFTER `alergias`;

-- Importación de nuevos valores para la tabla desplegables:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

INSERT INTO `desplegables` (`codigo`, `texto`, `campo`) VALUES
(NULL, 'Buen estado general. Consciente, orientado y colaborador.', 'inspeccionGeneral'),
(NULL, 'ExploraciÃ³n farÃ­ngea normal.', 'orl'),
(NULL, 'InspecciÃ³n ocular sin hallazgos especÃ­ficos.', 'oftalmologia'),
(NULL, 'Dentro de los lÃ­mites de la normalidad; no problemas de equilibrio, coordinaciÃ³n, fuerza ni sensibilidad.', 'exploracionNeurologica'),
(NULL, 'Normal coloraciÃ³n e hidrataciÃ³n.', 'pielMucosas'),
(NULL, 'Buen murmullo vesicular en ambos campos pulmonares.', 'auscultacionPulmonar'),
(NULL, 'ABDOMEN: Blando y depresible. No dolor a la palpaciÃ³n profunda. No se evidencia masas ni megalias.', 'aparatoDigestivo'),
(NULL, 'Sin edemas ni signos de trombosis venosa profunda.', 'extremidades'),
(NULL, 'CorazÃ³n rÃ­tmico sin soplos ni extratonos.', 'auscultacionCardiaca'),
(NULL, 'PuÃ±o percusiÃ³n renal negativa.', 'sistemaUrogenital'),
(NULL, 'Sin hallazgos especÃ­ficos. Columna vertebral sin manifestaciones de efectos limitativos en la actualidad.', 'aparatoLocomotor');


-- Eliminado el campo agudeza de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` DROP `agudeza`;

-- Añadido el campo observacionesEspirometria a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `observacionesEspirometria` TEXT NOT NULL AFTER `valoracionEspirometrica`;

-- Eliminado el campo audiometria de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` DROP `audiometria`;

-- Añadido el campo checkDislipidemia a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `checkDislipidemia` ENUM('NO','SI') NOT NULL AFTER `checkHiperglucemia`;

-- Añadidos los campos valoracionEconomica y resultado a la tabla reconocimientos_medicos_otras_pruebas:
ALTER TABLE `reconocimientos_medicos_otras_pruebas` ADD `valoracionEconomica` DOUBLE(20,2) NOT NULL AFTER `texto`, ADD `resultado` TEXT NOT NULL AFTER `valoracionEconomica`;

-- Añadido el campo resultadoDinamometria a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `resultadoDinamometria` VARCHAR(10) NOT NULL AFTER `manoDominante`;

-- Añadido el campo indicePaquetesAnio a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `indicePaquetesAnio` DOUBLE(6,2) NOT NULL AFTER `pef`;

-- Modificado el campo cafeDia de la tabla reconocimientosMedicos por bebidasEstimulantes:
ALTER TABLE `reconocimientos_medicos` CHANGE `cafeDia` `bebidasEstimulantes` ENUM('NO','SI') CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;
UPDATE reconocimientos_medicos SET bebidasEstimulantes='NO';

-- Creación de la tabla bebidas_estimulantes_reconocimiento_medico:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `bebidas_estimulantes_reconocimiento_medico` (
  `codigo` int(255) NOT NULL,
  `unidadesDia` varchar(15) NOT NULL,
  `bebida` varchar(255) NOT NULL,
  `codigoReconocimientoMedico` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `bebidas_estimulantes_reconocimiento_medico`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_bebidas_estimulantes_reconocimiento_medico` (`codigoReconocimientoMedico`);


ALTER TABLE `bebidas_estimulantes_reconocimiento_medico`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bebidas_estimulantes_reconocimiento_medico`
  ADD CONSTRAINT `fk_bebidas_estimulantes_reconocimiento_medico` FOREIGN KEY (`codigoReconocimientoMedico`) REFERENCES `reconocimientos_medicos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Creación de la tabla ejercicios_reconocimiento_medico:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `ejercicios_reconocimiento_medico` (
  `codigo` int(255) NOT NULL,
  `ejercicio` varchar(255) NOT NULL,
  `frecuencia` varchar(255) NOT NULL,
  `codigoReconocimiento` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `ejercicios_reconocimiento_medico`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_ejercicios_reconocimiento_medico` (`codigoReconocimiento`);


ALTER TABLE `ejercicios_reconocimiento_medico`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `ejercicios_reconocimiento_medico`
  ADD CONSTRAINT `fk_ejercicios_reconocimiento_medico` FOREIGN KEY (`codigoReconocimiento`) REFERENCES `reconocimientos_medicos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Añadidos los campos reconocimientoVisible y aptoVisible a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `reconocimientoVisible` ENUM('NO','SI') NOT NULL AFTER `indicePaquetesAnio`, ADD `aptoVisible` ENUM('NO','SI') NOT NULL AFTER `reconocimientoVisible`;

-- Cambios en clientes y contratos:
ALTER TABLE `clientes` DROP `suspendido`;
ALTER TABLE `contratos` ADD `suspendido` ENUM('SI','NO') NOT NULL DEFAULT 'NO' AFTER `vigencia`;
ALTER TABLE `clientes` DROP `baja`;

-- Modificada la relación del campo codigoEmpleado de la tabla reconocimientos_medicos_en_facturas:
ALTER TABLE `reconocimientos_medicos_en_facturas` DROP FOREIGN KEY `fk_reconocimientos_medicos_en_facturas1`; ALTER TABLE `reconocimientos_medicos_en_facturas` ADD CONSTRAINT `fk_reconocimientos_medicos_en_facturas1` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados`(`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE `cursos` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`codigo`, `nombre`, `tipo`) VALUES
(1, 'FormaciÃ³n del Art. 19 de la Ley de PrevenciÃ³n de Riesgos Laborales', 'Presencial');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`codigo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos_modulos`
--

CREATE TABLE `cursos_modulos` (
  `codigo` int(255) NOT NULL,
  `codigoCurso` int(255) NOT NULL,
  `numero` int(20) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cursos_modulos`
--

INSERT INTO `cursos_modulos` (`codigo`, `codigoCurso`, `numero`, `nombre`) VALUES
(4, 1, 1, 'Conceptos bÃ¡sicos sobre seguridad y salud en el trabajo'),
(5, 1, 2, 'Riesgos generales y su prevenciÃ³n'),
(6, 1, 3, 'Elementos bÃ¡sicos de gestiÃ³n de la prevenciÃ³n de riesgos.'),
(7, 1, 4, 'Medidas de emergencia y Primeros auxilios');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cursos_modulos`
--
ALTER TABLE `cursos_modulos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoCurso` (`codigoCurso`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cursos_modulos`
--
ALTER TABLE `cursos_modulos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cursos_modulos`
--
ALTER TABLE `cursos_modulos`
  ADD CONSTRAINT `cursos_modulos` FOREIGN KEY (`codigoCurso`) REFERENCES `cursos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `certificados` ADD `codigoCurso` INT(255) NULL DEFAULT NULL AFTER `riesgos`, ADD INDEX (`codigoCurso`);
ALTER TABLE `certificados` ADD CONSTRAINT `certificados_ifk_5` FOREIGN KEY (`codigoCurso`) REFERENCES `cursos`(`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;


-- Creación de la tabla incrementos_ipc:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `incrementos_ipc` (
  `codigo` int(255) NOT NULL,
  `ejercicio` year(4) NOT NULL,
  `incremento` double(4,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


ALTER TABLE `incrementos_ipc`
  ADD PRIMARY KEY (`codigo`);


ALTER TABLE `incrementos_ipc`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;


-- Añadidos los campos incrementoIPC e incrementoIpcAplicado a la tabla contratos:
ALTER TABLE `contratos` ADD `incrementoIPC` ENUM('NO','SI') NOT NULL AFTER `suspendido`, ADD `incrementoIpcAplicado` DOUBLE(4,2) NOT NULL AFTER `incrementoIPC`;

ALTER TABLE `formularioPRL` ADD `ficheroEstructura` VARCHAR(255) NOT NULL DEFAULT 'NO' AFTER `anioVS`;

ALTER TABLE `formularioPRL` ADD `visible` ENUM('SI','NO') NOT NULL DEFAULT 'SI' AFTER `ficheroEstructura`;

-- Añadido el campo vencimientosOfacturas a la tabla contratos:
ALTER TABLE `contratos`  ADD `vencimientosOfacturas` ENUM('VENCIMIENTOS','FACTURAS') NOT NULL  AFTER `codigoFormaPago`;
UPDATE contratos SET vencimientosOfacturas='VENCIMIENTOS';

-- Modificación del campo estado de la tabla citas:
ALTER TABLE `citas` CHANGE `estado` `estado` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

-- Añadido el campo textoAlcohol a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `textoAlcohol` VARCHAR(255) NOT NULL AFTER `alcohol`;

-- Eliminación de la restricción de clave única para el campo codigoUsuario de la tabla espacios_trabajo

-- Añadido el campo textoMedicacionHabitual a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `textoMedicacionHabitual` TEXT NOT NULL AFTER `medicacionHabitual`;

-- Añadidos los campos checkAlergias4, textoAlergias0, textoAlergias1, textoAlergias2, textoAlergias3 a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `checkAlergias4` ENUM('NO','SI') NOT NULL AFTER `checkAlergias3`, ADD `textoAlergias1` TEXT NOT NULL AFTER `checkAlergias4`, ADD `textoAlergias2` TEXT NOT NULL AFTER `textoAlergias1`, ADD `textoAlergias3` TEXT NOT NULL AFTER `textoAlergias2`, ADD `textoAlergias4` TEXT NOT NULL AFTER `textoAlergias3`;

-- Añadido el campo textoMinusvalia a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `textoMinusvalia` TEXT NOT NULL AFTER `minusvaliaReconocida`;

-- Modificación del campo fracturas por textoFracturas de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` CHANGE `fracturas` `textoFracturas` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;

-- Añadido el campo fracturas a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `fracturas` ENUM('NO','SI') NOT NULL AFTER `trastornosCongenitos`;

-- Modificado el campo bajasProlongadas por textoBajasProlongadas de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` CHANGE `bajasProlongadas` `textoBajasProlongadas` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;

-- Añadido el campo bajasProlongadas a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `bajasProlongadas` ENUM('NO','SI') NOT NULL AFTER `checkEpis`;

-- Añadido el campo textoApto a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `textoApto` TEXT NOT NULL AFTER `apto`;

-- Creación de la tabla profesionales_externos:

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `profesionales_externos` (
  `codigo` int(255) NOT NULL,
  `nombre` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `apellidos` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `clinica` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `precio` double(20,2) NOT NULL,
  `observaciones` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


ALTER TABLE `profesionales_externos`
  ADD PRIMARY KEY (`codigo`);


ALTER TABLE `profesionales_externos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;


-- Modificado el campo profesionaExterno de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` CHANGE `profesionalExterno` `codigoProfesionalExterno` INT(255) NULL;
ALTER TABLE `reconocimientos_medicos` ADD INDEX(`codigoProfesionalExterno`);
UPDATE reconocimientos_medicos SET codigoProfesionalExterno=NULL;
ALTER TABLE `reconocimientos_medicos` ADD CONSTRAINT `fk_reconocimientos_medicos4` FOREIGN KEY (`codigoProfesionalExterno`) REFERENCES `profesionales_externos`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Eliminado el campo codigoSala de la tabla citas:
ALTER TABLE citas DROP FOREIGN KEY fk_citas2;
ALTER TABLE `citas` DROP INDEX `fk_citas2`;
ALTER TABLE `citas` DROP `codigoSala`;

-- Eliminados los campos medicacionHabitual y textoMedicacionHabitual de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` DROP `medicacionHabitual`, DROP `textoMedicacionHabitual`;

-- Modificado el campo transtornosCongenitos para que sea SI/NO de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` CHANGE `trastornosCongenitos` `trastornosCongenitos` ENUM('NO','SI') CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;

-- Añadido el campo textoTranstornosCongenitos a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `textoTranstornosCongenitos` TEXT NOT NULL AFTER `trastornosCongenitos`;

-- Eliminado el campo checkObjetivan de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` DROP `checkObjetivan`;

-- Añadidos los campos acudirMedicoEspecialista y textoAcurdirMedicoEspecialista a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `acudirMedicoEspecialista` ENUM('NO','SI') NOT NULL AFTER `checkTipoGafasLentillas4`, ADD `textoAcurdirMedicoEspecialista` TEXT NOT NULL AFTER `acudirMedicoEspecialista`;

-- Añadido el campo lopd y ficheroLOPD a la tabla empleados:
ALTER TABLE `empleados` ADD `lopd` ENUM('NO','SI') NOT NULL AFTER `puestoManual`, ADD `ficheroLOPD` VARCHAR(20) NOT NULL AFTER `lopd`;

-- Creación de la tabla pruebas_protocolizacion:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `pruebas_protocolizacion` (
  `codigo` int(255) NOT NULL,
  `codigoProtocolizacion` int(255) DEFAULT NULL,
  `texto` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


ALTER TABLE `pruebas_protocolizacion`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_pruebas_protocolizacion` (`codigoProtocolizacion`);


ALTER TABLE `pruebas_protocolizacion`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `pruebas_protocolizacion`
  ADD CONSTRAINT `fk_pruebas_protocolizacion` FOREIGN KEY (`codigoProtocolizacion`) REFERENCES `protocolizacion` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Eliminación del campo analiticaPruebas de la tabla protocolización:
ALTER TABLE `protocolizacion` DROP `analiticaPruebas`;

-- Corregido error ortografico en nombre de campo textosTrastornosCongenitos de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` CHANGE `textoTranstornosCongenitos` `textoTrastornosCongenitos` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;

UPDATE `contratos` SET `codigoInterno` = '2' WHERE `contratos`.`codigo` = 1211; 
UPDATE `contratos` SET `codigoInterno` = '2' WHERE `contratos`.`codigo` = 1111; 
ALTER TABLE `usuarios` CHANGE `tipo` `tipo` ENUM('ADMIN','COMERCIAL','ADMINISTRACION','CONSULTORIA','FORMACION','ATENCION','TELECONCERTADOR','TECNICO','COLABORADOR','ASESOR','CLIENTE','EMPLEADO','FACTURACION','MEDICO','ENFERMERIA','ADMINISTRATIVO') CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL; 
ALTER TABLE `usuarios` ADD `superadmin` ENUM('SI','NO') NOT NULL DEFAULT 'NO' AFTER `sesion`;
UPDATE `usuarios` SET `superadmin` = 'SI' WHERE `usuarios`.`codigo` = 152; 
UPDATE `usuarios` SET `superadmin` = 'SI' WHERE `usuarios`.`codigo` = 147; 

-- Creación de la tabla comentarios_analiticas:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `comentarios_analiticas` (
  `codigo` int(255) NOT NULL,
  `comentario` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `recomendaciones` text COLLATE latin1_spanish_ci NOT NULL,
  `ficheroAdjunto` varchar(20) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


ALTER TABLE `comentarios_analiticas`
  ADD PRIMARY KEY (`codigo`);


ALTER TABLE `comentarios_analiticas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

-- Creación de la tabla comentarios_analiticas_reconocimientos_medicos:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `comentarios_analiticas_reconocimientos_medicos` (
  `codigo` int(255) NOT NULL,
  `codigoReconocimiento` int(255) DEFAULT NULL,
  `codigoComentarioAnalitica` int(255) DEFAULT NULL,
  `checkComentario` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `checkRecomendaciones` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL,
  `checkAdjunto` enum('NO','SI') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


ALTER TABLE `comentarios_analiticas_reconocimientos_medicos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_comentarios_analiticas` (`codigoReconocimiento`),
  ADD KEY `fk_comentarios_analiticas2` (`codigoComentarioAnalitica`);


ALTER TABLE `comentarios_analiticas_reconocimientos_medicos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `comentarios_analiticas_reconocimientos_medicos`
  ADD CONSTRAINT `fk_comentarios_analiticas` FOREIGN KEY (`codigoComentarioAnalitica`) REFERENCES `comentarios_analiticas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_comentarios_analiticas2` FOREIGN KEY (`codigoReconocimiento`) REFERENCES `reconocimientos_medicos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Eliminación de los campos checkAnemia, checkColesterol, checkTrigliceridos, checkTransaminasa, checkUrico, checkHiperglucemia, checkDislipidemia y checkHematuria de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` DROP `checkAnemia`, DROP `checkColesterol`, DROP `checkTrigliceridos`, DROP `checkTransaminasa`, DROP `checkUrico`, DROP `checkHiperglucemia`, DROP `checkDislipidemia`, DROP `checkHematuria`;

-- Eliminación del comentario checkComentarioAnalitica de la tabla comentarios_analiticas_reconocimientos_medicos:
ALTER TABLE `comentarios_analiticas_reconocimientos_medicos` DROP `checkComentario`;

-- Añadido el campo eliminado a la tabla comentarios_analiticas:
ALTER TABLE `comentarios_analiticas` ADD `eliminado` ENUM('NO','SI') NOT NULL AFTER `ficheroAdjunto`;

-- Aumentado el tamaño del campo riesgosExtralaborales y riesgosPuesto de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` CHANGE `riesgosExtralaborales` `riesgosExtralaborales` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL, CHANGE `riesgoPuesto` `riesgoPuesto` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;

-- Añadido el campo checkAnalitica a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `checkAnalitica` ENUM('NO','SI') NOT NULL AFTER `electrocardiograma`;
UPDATE reconocimientos_medicos SET checkAnalitica='SI' WHERE checkHemograma='SI';

-- Eliminados los campos checkHemograma, checkBioquimica y checkOrina de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` DROP `checkHemograma`, DROP `checkBioquimica`, DROP `checkOrina`;

-- Añadido el campo observacionesEstudioVision a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `observacionesEstudioVision` TEXT NOT NULL AFTER `estudioVision12`;

-- Añadido el campo checkTratamientoHabitual a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `checkTratamientoHabitual` ENUM('SI','NO') NOT NULL AFTER `bebidasEstimulantes`;

-- Aumentado el tamaño del campo tratamientoHabitual de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` CHANGE `tratamientoHabitual` `tratamientoHabitual` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;

-- Añadido el campo checkVacunaciones a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `checkVacunaciones` ENUM('SI','NO') NOT NULL AFTER `tratamientoHabitual`;

-- Aumentado el tamaño del campo analitica de la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` CHANGE `analitica` `analitica` TEXT CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturacion_gastos`
--

CREATE TABLE `facturacion_gastos` (
  `codigo` int(255) NOT NULL,
  `codigoUsuario` int(255) DEFAULT NULL,
  `codigoContrato` int(255) DEFAULT NULL,
  `tipo` enum('Estructural','Técnico','Vigilancia de la Salud') COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `importe` double NOT NULL,
  `iva` int(20) NOT NULL,
  `total` double NOT NULL,
  `observaciones` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `facturacion_gastos`
--
ALTER TABLE `facturacion_gastos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoUsuario` (`codigoUsuario`),
  ADD KEY `codigoContrato` (`codigoContrato`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `facturacion_gastos`
--
ALTER TABLE `facturacion_gastos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `facturacion_gastos`
--
ALTER TABLE `facturacion_gastos`
  ADD CONSTRAINT `facturacion_gastos_ibfk_1` FOREIGN KEY (`codigoUsuario`) REFERENCES `usuarios` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `facturacion_gastos_ibfk_2` FOREIGN KEY (`codigoContrato`) REFERENCES `contratos` (`codigo`) ON DELETE SET NULL ON UPDATE SET NULL;


-- Añadido el campo concepto a la tabla facturacion_gastos:
ALTER TABLE `facturacion_gastos` ADD `concepto` TINYTEXT NOT NULL AFTER `fecha`;


-- Modificado el campo tipo de la tabla facturacion_gastos:
ALTER TABLE `facturacion_gastos` CHANGE `tipo` `tipo` ENUM('Estructural','Tecnico','Vigilancia de la Salud') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

-- Modificación de la asociación de la factura del cliente AURELIO, que tenía un contrato eliminado y una factura asociada a él, y un contrato creado asociado a una proforma eliminada:
DELETE FROM contratos_en_facturas WHERE codigoFactura=1279;
DELETE FROM contratos_en_facturas WHERE codigoContrato=1329;
INSERT INTO contratos_en_facturas VALUES(NULL,1329,1279);


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planificacion_visitas`
--

CREATE TABLE `planificacion_visitas` (
  `codigo` int(255) NOT NULL,
  `codigoFormulario` int(255) NOT NULL,
  `fechaPrevista` date NOT NULL,
  `fechaReal` date NOT NULL,
  `horaInicio` time NOT NULL,
  `horaFin` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `planificacion_visitas`
--
ALTER TABLE `planificacion_visitas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoFormulario` (`codigoFormulario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `planificacion_visitas`
--
ALTER TABLE `planificacion_visitas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `planificacion_visitas`
--
ALTER TABLE `planificacion_visitas`
  ADD CONSTRAINT `planificacion_visitas_ibfk_1` FOREIGN KEY (`codigoFormulario`) REFERENCES `formularioPRL` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

  -- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planificacion_visitas_tareas`
--

CREATE TABLE `planificacion_visitas_tareas` (
  `codigo` int(255) NOT NULL,
  `codigoVisita` int(255) NOT NULL,
  `codigoTarea` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `planificacion_visitas_tareas`
--
ALTER TABLE `planificacion_visitas_tareas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoVisita` (`codigoVisita`),
  ADD KEY `codigoTarea` (`codigoTarea`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `planificacion_visitas_tareas`
--
ALTER TABLE `planificacion_visitas_tareas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `planificacion_visitas_tareas`
--
ALTER TABLE `planificacion_visitas_tareas`
  ADD CONSTRAINT `planificacion_visitas_tareas_ibfk_1` FOREIGN KEY (`codigoVisita`) REFERENCES `planificacion_visitas` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `planificacion_visitas_tareas_ibfk_2` FOREIGN KEY (`codigoTarea`) REFERENCES `tareas` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;


-- Modificación del cmapo tipoFactura de la tabla facturas para que admita el valor LIBRE:
ALTER TABLE `facturas` CHANGE `tipoFactura` `tipoFactura` ENUM('NORMAL','ABONO','PROFORMA','LIBRE') CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL;

-- Añadido el campo conceptoManualFactura a la tabla facturas:
ALTER TABLE `facturas` ADD `conceptoManualFactura` TEXT NOT NULL AFTER `codigoUsuarioFactura`;


ALTER TABLE `riesgos_evaluacion_general` ADD `descripcion2` TEXT NOT NULL AFTER `codigoEvaluacion`, ADD `medida` TEXT NOT NULL AFTER `descripcion2`; 
ALTER TABLE evaluacion_imagenes DROP FOREIGN KEY evaluacion_imagenes;
ALTER TABLE evaluacion_medidas_imagenes DROP FOREIGN KEY evaluacion_medidas_imagenes;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `riesgos_evaluacion_general_bloques`
--

CREATE TABLE `riesgos_evaluacion_general_bloques` (
  `codigo` int(255) NOT NULL,
  `codigoRiesgo` int(255) NOT NULL,
  `descripcionRiesgo` text COLLATE utf8_unicode_ci NOT NULL,
  `probabilidad` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL,
  `consecuencias` enum('1','2','3','4') COLLATE utf8_unicode_ci NOT NULL,
  `prioridad` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `total` int(2) NOT NULL,
  `graficoBarra` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcion2` text COLLATE utf8_unicode_ci NOT NULL,
  `medida` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `riesgos_evaluacion_general_bloques`
--
ALTER TABLE `riesgos_evaluacion_general_bloques`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoRiesgo` (`codigoRiesgo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `riesgos_evaluacion_general_bloques`
--
ALTER TABLE `riesgos_evaluacion_general_bloques`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `riesgos_evaluacion_general_bloques`
--
ALTER TABLE `riesgos_evaluacion_general_bloques`
  ADD CONSTRAINT `riesgos_evaluacion_general_bloques_ibfk_1` FOREIGN KEY (`codigoRiesgo`) REFERENCES `riesgos_evaluacion_general` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
--
--
--
--
--
-- IMPORTANTE.....DESPUÉS DEL REAJUSTE
--
--
--
--
--
--

ALTER TABLE `riesgos_evaluacion_general`
  DROP `descripcionRiesgo`,
  DROP `probabilidad`,
  DROP `consecuencias`,
  DROP `prioridad`,
  DROP `total`,
  DROP `graficoBarra`,
  DROP `descripcion2`,
  DROP `medida`;
ALTER TABLE `evaluacion_imagenes` ADD FOREIGN KEY (`codigoDescripcion`) REFERENCES `riesgos_evaluacion_general_bloques`(`codigo`) ON DELETE CASCADE ON UPDATE CASCADE; 
ALTER TABLE `evaluacion_medidas_imagenes` ADD FOREIGN KEY (`codigoMedida`) REFERENCES `riesgos_evaluacion_general_bloques`(`codigo`) ON DELETE CASCADE ON UPDATE CASCADE; 


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `memorias`
--

CREATE TABLE `memorias` (
  `codigo` int(255) NOT NULL,
  `codigoCliente` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `memorias`
--
ALTER TABLE `memorias`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoCliente` (`codigoCliente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `memorias`
--
ALTER TABLE `memorias`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `memorias`
--
ALTER TABLE `memorias`
  ADD CONSTRAINT `memorias_ibfk_1` FOREIGN KEY (`codigoCliente`) REFERENCES `clientes` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

  -- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `memorias_evaluaciones`
--

CREATE TABLE `memorias_evaluaciones` (
  `codigo` int(255) NOT NULL,
  `codigoMemoria` int(255) NOT NULL,
  `anio` int(255) NOT NULL,
  `mes` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `memorias_evaluaciones`
--
ALTER TABLE `memorias_evaluaciones`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoMemoria` (`codigoMemoria`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `memorias_evaluaciones`
--
ALTER TABLE `memorias_evaluaciones`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `memorias_evaluaciones`
--
ALTER TABLE `memorias_evaluaciones`
  ADD CONSTRAINT `memorias_evaluaciones_ibfk_1` FOREIGN KEY (`codigoMemoria`) REFERENCES `memorias` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `memorias_maquinarias`
--

CREATE TABLE `memorias_maquinarias` (
  `codigo` int(255) NOT NULL,
  `codigoMemoria` int(255) NOT NULL,
  `codigoMaquinaria` int(255) NOT NULL,
  `mes` int(20) NOT NULL,
  `anio` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `memorias_maquinarias`
--
ALTER TABLE `memorias_maquinarias`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoMemoria` (`codigoMemoria`),
  ADD KEY `codigoMaquinaria` (`codigoMaquinaria`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `memorias_maquinarias`
--
ALTER TABLE `memorias_maquinarias`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `memorias_maquinarias`
--
ALTER TABLE `memorias_maquinarias`
  ADD CONSTRAINT `memorias_maquinarias_ibfk_1` FOREIGN KEY (`codigoMemoria`) REFERENCES `memorias` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `memorias_maquinarias_ibfk_2` FOREIGN KEY (`codigoMaquinaria`) REFERENCES `maquinaria_formulario_prl` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `memorias_evaluaciones` CHANGE `mes` `mes` INT(255) NOT NULL; 

-- Añadido el campo edadReconocimiento a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `edadReconocimiento` INT(11) NOT NULL AFTER `textoAcurdirMedicoEspecialista`;

-- Añadido el campo sexoEmpleado a la tabla empleados:
ALTER TABLE `empleados` ADD `sexoEmpleado` VARCHAR(2) NOT NULL AFTER `ficheroLOPD`;


ALTER TABLE `accidentes` ADD `codigoINSST` VARCHAR(255) NOT NULL DEFAULT '' AFTER `secuelas`; 

-- Añadidos los campos botonDatosAnterioresPulsado y botonExploracioNormalPulsado a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `botonDatosAnterioresPulsado` ENUM('NO','SI') NOT NULL AFTER `edadReconocimiento`, ADD `botonExploracioNormalPulsado` ENUM('NO','SI') NOT NULL AFTER `botonDatosAnterioresPulsado`;

-- Añadido el campo codigoReconocimientoMedico a la tabla reconocimientos_medicos_en_facturas:
ALTER TABLE `reconocimientos_medicos_en_facturas` ADD `codigoReconocimientoMedico` INT(255) NULL AFTER `codigo`, ADD INDEX `fk_reconocimientos_medicos_en_facturas3` (`codigoReconocimientoMedico`);
ALTER TABLE `reconocimientos_medicos_en_facturas` ADD CONSTRAINT `fk_reconocimientos_medicos_en_facturas3` FOREIGN KEY (`codigoReconocimientoMedico`) REFERENCES `reconocimientos_medicos`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadido el campo numEmpleados a la tabla ofertas:
ALTER TABLE `ofertas` ADD `numEmpleados` VARCHAR(20) NOT NULL AFTER `tipoEmpleados`;
UPDATE ofertas SET numEmpleados=(SELECT SUM(trabajadores) FROM clientes_centros WHERE activo="SI" AND ofertas.codigoCliente=clientes_centros.codigoCliente) WHERE codigoCliente IS NOT NULL AND tipoEmpleados='ACTIVOS';
UPDATE ofertas, clientes SET numEmpleados=EMPNTRAB WHERE tipoEmpleados='TODOS' AND ofertas.codigoCliente=clientes.codigo;

-- Creación de la tabla centros_trabajo_en_ofertas:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `centros_trabajo_en_ofertas` (
  `codigo` int(255) NOT NULL,
  `codigoOferta` int(255) DEFAULT NULL,
  `codigoClienteCentro` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


ALTER TABLE `centros_trabajo_en_ofertas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_centros_trabajo_en_ofertas` (`codigoOferta`),
  ADD KEY `fk_centros_trabajo_en_ofertas2` (`codigoClienteCentro`);


ALTER TABLE `centros_trabajo_en_ofertas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `centros_trabajo_en_ofertas`
  ADD CONSTRAINT `fk_centros_trabajo_en_ofertas` FOREIGN KEY (`codigoClienteCentro`) REFERENCES `clientes` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_centros_trabajo_en_ofertas2` FOREIGN KEY (`codigoOferta`) REFERENCES `ofertas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Corrección de la relación del campo codigoClienteCentro de la tabla centros_trabajo_en_ofertas:
ALTER TABLE `centros_trabajo_en_ofertas` DROP FOREIGN KEY `fk_centros_trabajo_en_ofertas`; ALTER TABLE `centros_trabajo_en_ofertas` ADD CONSTRAINT `fk_centros_trabajo_en_ofertas` FOREIGN KEY (`codigoClienteCentro`) REFERENCES `clientes_centros`(`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;

-- Añadidos los campos activoEmpleado y fechaBaja a la tabla empleados:
ALTER TABLE `empleados` ADD `activoEmpleado` ENUM('SI','NO') NOT NULL AFTER `sexoEmpleado`, ADD `fechaBaja` DATE NOT NULL AFTER `activoEmpleado`;

-- Añadido el campo valoracionEconomica a la tabla pruebas_protocolizacion:
ALTER TABLE `pruebas_protocolizacion` ADD `valoracionEconomica` DOUBLE(20,2) NOT NULL AFTER `texto`;


-- Creación de la tabla reconocimientos_medicos_ya_facturados:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `reconocimientos_medicos_ya_facturados` (
  `codigo` int(255) NOT NULL,
  `fecha` date NOT NULL,
  `codigoReconocimientoMedico` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


ALTER TABLE `reconocimientos_medicos_ya_facturados`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_reconocimientos_medicos_ya_facturados` (`codigoReconocimientoMedico`);


ALTER TABLE `reconocimientos_medicos_ya_facturados`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `reconocimientos_medicos_ya_facturados`
  ADD CONSTRAINT `fk_reconocimientos_medicos_ya_facturados` FOREIGN KEY (`codigoReconocimientoMedico`) REFERENCES `reconocimientos_medicos` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;


-- Añadido el campo emitirPlanificacionSinPagar a la tabla contratos:
ALTER TABLE `contratos` ADD `emitirPlanificacionSinPagar` ENUM('NO','SI') NOT NULL AFTER `incrementoIpcAplicado`;

-- Añadido el campo vencimientosOfacturas2 a la tabla contratos:
ALTER TABLE `contratos` ADD `vencimientosOfacturas2` ENUM('VENCIMIENTOS', 'FACTURAS') NOT NULL AFTER `emitirPlanificacionSinPagar`;

-- Añadido el campo omitirFacturacion a la tabla reconocimientos_medicos:
ALTER TABLE `reconocimientos_medicos` ADD `omitirFacturacion` ENUM('NO','SI') NOT NULL AFTER `botonExploracioNormalPulsado`;

-- Añadido el campo copiaOculta a la tabla correos:
ALTER TABLE `correos` ADD `copiaOculta` TEXT NOT NULL AFTER `codigoContrato`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_sincronizados`
--

CREATE TABLE `usuarios_sincronizados` (
  `codigo` int(255) NOT NULL,
  `usuarioPRL` int(255) NOT NULL,
  `usuarioFormacion` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios_sincronizados`
--
ALTER TABLE `usuarios_sincronizados`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `usuarioPRL` (`usuarioPRL`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios_sincronizados`
--
ALTER TABLE `usuarios_sincronizados`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `usuarios_sincronizados`
--
ALTER TABLE `usuarios_sincronizados`
  ADD CONSTRAINT `usuarios_sincronizados_ibfk_1` FOREIGN KEY (`usuarioPRL`) REFERENCES `usuarios` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;



DELETE FROM memorias;
ALTER TABLE `memorias` DROP `codigoCliente`;
ALTER TABLE `memorias` ADD `fechaCreacion` DATE NULL DEFAULT NULL AFTER `codigo`, ADD `fechaUltimaActualizacion` DATE NULL DEFAULT NULL AFTER `fechaCreacion`; 
ALTER TABLE `memorias` ADD `horaUltimaActualizacion` TIME NOT NULL AFTER `fechaUltimaActualizacion`; 

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `memorias_actividades_preventivas`
--

CREATE TABLE `memorias_actividades_preventivas` (
  `codigo` int(255) NOT NULL,
  `codigoMemoria` int(255) NOT NULL,
  `numeroInterno` int(20) NOT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `memorias_actividades_preventivas`
--
ALTER TABLE `memorias_actividades_preventivas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoMemoria` (`codigoMemoria`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `memorias_actividades_preventivas`
--
ALTER TABLE `memorias_actividades_preventivas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `memorias_actividades_preventivas`
--
ALTER TABLE `memorias_actividades_preventivas`
  ADD CONSTRAINT `memorias_actividades_preventivas_ibfk_1` FOREIGN KEY (`codigoMemoria`) REFERENCES `memorias` (`codigo`);

  -- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `memorias_evaluaciones_riesgos`
--

CREATE TABLE `memorias_evaluaciones_riesgos` (
  `codigo` int(255) NOT NULL,
  `codigoMemoria` int(255) NOT NULL,
  `numeroInterno` int(20) NOT NULL,
  `valorUno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorDos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorTres` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `memorias_evaluaciones_riesgos`
--
ALTER TABLE `memorias_evaluaciones_riesgos`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoMemo` (`codigoMemoria`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `memorias_evaluaciones_riesgos`
--
ALTER TABLE `memorias_evaluaciones_riesgos`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `memorias_evaluaciones_riesgos`
--
ALTER TABLE `memorias_evaluaciones_riesgos`
  ADD CONSTRAINT `memorias_evaluaciones_riesgos_ibfk_1` FOREIGN KEY (`codigoMemoria`) REFERENCES `memorias` (`codigo`);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `memorias_formacion_trabajadores`
--

CREATE TABLE `memorias_formacion_trabajadores` (
  `codigo` int(255) NOT NULL,
  `codigoMemoria` int(255) NOT NULL,
  `numeroInterno` int(20) NOT NULL,
  `valorUno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorDos` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `memorias_formacion_trabajadores`
--
ALTER TABLE `memorias_formacion_trabajadores`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoMemoria` (`codigoMemoria`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `memorias_formacion_trabajadores`
--
ALTER TABLE `memorias_formacion_trabajadores`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `memorias_formacion_trabajadores`
--
ALTER TABLE `memorias_formacion_trabajadores`
  ADD CONSTRAINT `memorias_formacion_trabajadores_ibfk_1` FOREIGN KEY (`codigoMemoria`) REFERENCES `memorias` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `memorias_formacion_trabajadores2`
--

CREATE TABLE `memorias_formacion_trabajadores2` (
  `codigo` int(255) NOT NULL,
  `codigoMemoria` int(255) NOT NULL,
  `numeroInterno` int(20) NOT NULL,
  `valorUno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorDos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorTres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorCuatro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorCinco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorSeis` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `memorias_formacion_trabajadores2`
--
ALTER TABLE `memorias_formacion_trabajadores2`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoMemoria` (`codigoMemoria`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `memorias_formacion_trabajadores2`
--
ALTER TABLE `memorias_formacion_trabajadores2`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `memorias_formacion_trabajadores2`
--
ALTER TABLE `memorias_formacion_trabajadores2`
  ADD CONSTRAINT `memorias_formacion_trabajadores2_ibfk_1` FOREIGN KEY (`codigoMemoria`) REFERENCES `memorias` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `memorias_siniestralidad`
--

CREATE TABLE `memorias_siniestralidad` (
  `codigo` int(255) NOT NULL,
  `codigoMemoria` int(255) NOT NULL,
  `numeroInterno` int(20) NOT NULL,
  `valorUno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorDos` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `memorias_siniestralidad`
--
ALTER TABLE `memorias_siniestralidad`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `codigoMemoria` (`codigoMemoria`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `memorias_siniestralidad`
--
ALTER TABLE `memorias_siniestralidad`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `memorias_siniestralidad`
--
ALTER TABLE `memorias_siniestralidad`
  ADD CONSTRAINT `memorias_siniestralidad_ibfk_1` FOREIGN KEY (`codigoMemoria`) REFERENCES `memorias` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `formularioPRL` ADD `horasPlanPrev` INT(20) NOT NULL AFTER `visible`, ADD `horasPap` INT(20) NOT NULL AFTER `horasPlanPrev`, ADD `formacionPresencial` INT(20) NOT NULL AFTER `horasPap`, ADD `formacionDistancia` INT(20) NOT NULL AFTER `formacionPresencial`, ADD `formacionOnline` INT(20) NOT NULL AFTER `formacionDistancia`; 
ALTER TABLE `formularioPRL` ADD `accidentes` ENUM('SI','NO') NOT NULL DEFAULT 'NO' AFTER `formacionOnline`, ADD `fechaAccidentes` DATE NULL DEFAULT NULL AFTER `accidentes`, ADD `fechaAccidentesReal` DATE NULL DEFAULT NULL AFTER `fechaAccidentes`, ADD `accidentesLeves` INT(20) NOT NULL DEFAULT '0' AFTER `fechaAccidentesReal`, ADD `accidentesGraves` INT(20) NOT NULL DEFAULT '0' AFTER `accidentesLeves`, ADD `accidentesMortales` INT(20) NOT NULL DEFAULT '0' AFTER `accidentesGraves`; 


-- Creación de la tabla memorias_xml:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `memorias_xml` (
  `codigo` INT(255) NOT NULL,
  `ejercicio` year(4) NOT NULL,
  `conciertosActividadA1` VARCHAR(20) NOT NULL,
  `actuacionesActividadA1` VARCHAR(20) NOT NULL,
  `trabajadoresActividadA1` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadA1` VARCHAR(20) NOT NULL,
  `conciertosActividadB1` VARCHAR(20) NOT NULL,
  `actuacionesActividadB1` VARCHAR(20) NOT NULL,
  `trabajadoresActividadB1` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadB1` VARCHAR(20) NOT NULL,
  `conciertosActividadC1` VARCHAR(20) NOT NULL,
  `actuacionesActividadC1` VARCHAR(20) NOT NULL,
  `trabajadoresActividadC1` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadC1` VARCHAR(20) NOT NULL,
  `conciertosActividadA2` VARCHAR(20) NOT NULL,
  `actuacionesActividadA2` VARCHAR(20) NOT NULL,
  `trabajadoresActividadA2` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadA2` VARCHAR(20) NOT NULL,
  `conciertosActividadB2` VARCHAR(20) NOT NULL,
  `actuacionesActividadB2` VARCHAR(20) NOT NULL,
  `trabajadoresActividadB2` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadB2` VARCHAR(20) NOT NULL,
  `conciertosActividadC2` VARCHAR(20) NOT NULL,
  `actuacionesActividadC2` VARCHAR(20) NOT NULL,
  `trabajadoresActividadC2` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadC2` VARCHAR(20) NOT NULL,
  `conciertosActividadA3` VARCHAR(20) NOT NULL,
  `actuacionesActividadA3` VARCHAR(20) NOT NULL,
  `trabajadoresActividadA3` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadA3` VARCHAR(20) NOT NULL,
  `conciertosActividadB3` VARCHAR(20) NOT NULL,
  `actuacionesActividadB3` VARCHAR(20) NOT NULL,
  `trabajadoresActividadB3` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadB3` VARCHAR(20) NOT NULL,
  `conciertosActividadC3` VARCHAR(20) NOT NULL,
  `actuacionesActividadC3` VARCHAR(20) NOT NULL,
  `trabajadoresActividadC3` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadC3` VARCHAR(20) NOT NULL,
  `conciertosActividadA4` VARCHAR(20) NOT NULL,
  `actuacionesActividadA4` VARCHAR(20) NOT NULL,
  `trabajadoresActividadA4` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadA4` VARCHAR(20) NOT NULL,
  `conciertosActividadB4` VARCHAR(20) NOT NULL,
  `actuacionesActividadB4` VARCHAR(20) NOT NULL,
  `trabajadoresActividadB4` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadB4` VARCHAR(20) NOT NULL,
  `conciertosActividadC4` VARCHAR(20) NOT NULL,
  `actuacionesActividadC4` VARCHAR(20) NOT NULL,
  `trabajadoresActividadC4` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadC4` VARCHAR(20) NOT NULL,
  `conciertosActividadA5` VARCHAR(20) NOT NULL,
  `actuacionesActividadA5` VARCHAR(20) NOT NULL,
  `trabajadoresActividadA5` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadA5` VARCHAR(20) NOT NULL,
  `conciertosActividadB5` VARCHAR(20) NOT NULL,
  `actuacionesActividadB5` VARCHAR(20) NOT NULL,
  `trabajadoresActividadB5` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadB5` VARCHAR(20) NOT NULL,
  `conciertosActividadC5` VARCHAR(20) NOT NULL,
  `actuacionesActividadC5` VARCHAR(20) NOT NULL,
  `trabajadoresActividadC5` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadC5` VARCHAR(20) NOT NULL,
  `conciertosActividadA6` VARCHAR(20) NOT NULL,
  `actuacionesActividadA6` VARCHAR(20) NOT NULL,
  `trabajadoresActividadA6` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadA6` VARCHAR(20) NOT NULL,
  `conciertosActividadB6` VARCHAR(20) NOT NULL,
  `actuacionesActividadB6` VARCHAR(20) NOT NULL,
  `trabajadoresActividadB6` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadB6` VARCHAR(20) NOT NULL,
  `conciertosActividadC6` VARCHAR(20) NOT NULL,
  `actuacionesActividadC6` VARCHAR(20) NOT NULL,
  `trabajadoresActividadC6` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadC6` VARCHAR(20) NOT NULL,
  `conciertosActividadA7` VARCHAR(20) NOT NULL,
  `actuacionesActividadA7` VARCHAR(20) NOT NULL,
  `trabajadoresActividadA7` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadA7` VARCHAR(20) NOT NULL,
  `conciertosActividadB7` VARCHAR(20) NOT NULL,
  `actuacionesActividadB7` VARCHAR(20) NOT NULL,
  `trabajadoresActividadB7` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadB7` VARCHAR(20) NOT NULL,
  `conciertosActividadC7` VARCHAR(20) NOT NULL,
  `actuacionesActividadC7` VARCHAR(20) NOT NULL,
  `trabajadoresActividadC7` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadC7` VARCHAR(20) NOT NULL,
  `conciertosActividadA8` VARCHAR(20) NOT NULL,
  `actuacionesActividadA8` VARCHAR(20) NOT NULL,
  `trabajadoresActividadA8` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadA8` VARCHAR(20) NOT NULL,
  `conciertosActividadB8` VARCHAR(20) NOT NULL,
  `actuacionesActividadB8` VARCHAR(20) NOT NULL,
  `trabajadoresActividadB8` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadB8` VARCHAR(20) NOT NULL,
  `conciertosActividadC8` VARCHAR(20) NOT NULL,
  `actuacionesActividadC8` VARCHAR(20) NOT NULL,
  `trabajadoresActividadC8` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadC8` VARCHAR(20) NOT NULL,
  `conciertosActividadA9` VARCHAR(20) NOT NULL,
  `actuacionesActividadA9` VARCHAR(20) NOT NULL,
  `trabajadoresActividadA9` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadA9` VARCHAR(20) NOT NULL,
  `conciertosActividadB9` VARCHAR(20) NOT NULL,
  `actuacionesActividadB9` VARCHAR(20) NOT NULL,
  `trabajadoresActividadB9` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadB9` VARCHAR(20) NOT NULL,
  `conciertosActividadC9` VARCHAR(20) NOT NULL,
  `actuacionesActividadC9` VARCHAR(20) NOT NULL,
  `trabajadoresActividadC9` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadC9` VARCHAR(20) NOT NULL,
  `conciertosActividadA10` VARCHAR(20) NOT NULL,
  `actuacionesActividadA10` VARCHAR(20) NOT NULL,
  `trabajadoresActividadA10` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadA10` VARCHAR(20) NOT NULL,
  `conciertosActividadB10` VARCHAR(20) NOT NULL,
  `actuacionesActividadB10` VARCHAR(20) NOT NULL,
  `trabajadoresActividadB10` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadB10` VARCHAR(20) NOT NULL,
  `conciertosActividadC10` VARCHAR(20) NOT NULL,
  `actuacionesActividadC10` VARCHAR(20) NOT NULL,
  `trabajadoresActividadC10` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadC10` VARCHAR(20) NOT NULL,
  `conciertosActividadA11` VARCHAR(20) NOT NULL,
  `actuacionesActividadA11` VARCHAR(20) NOT NULL,
  `trabajadoresActividadA11` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadA11` VARCHAR(20) NOT NULL,
  `conciertosActividadB11` VARCHAR(20) NOT NULL,
  `actuacionesActividadB11` VARCHAR(20) NOT NULL,
  `trabajadoresActividadB11` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadB11` VARCHAR(20) NOT NULL,
  `conciertosActividadC11` VARCHAR(20) NOT NULL,
  `actuacionesActividadC11` VARCHAR(20) NOT NULL,
  `trabajadoresActividadC11` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadC11` VARCHAR(20) NOT NULL,
  `conciertosActividadA12` VARCHAR(20) NOT NULL,
  `actuacionesActividadA12` VARCHAR(20) NOT NULL,
  `trabajadoresActividadA12` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadA12` VARCHAR(20) NOT NULL,
  `conciertosActividadB12` VARCHAR(20) NOT NULL,
  `actuacionesActividadB12` VARCHAR(20) NOT NULL,
  `trabajadoresActividadB12` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadB12` VARCHAR(20) NOT NULL,
  `conciertosActividadC12` VARCHAR(20) NOT NULL,
  `actuacionesActividadC12` VARCHAR(20) NOT NULL,
  `trabajadoresActividadC12` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadC12` VARCHAR(20) NOT NULL,
  `conciertosActividadA13` VARCHAR(20) NOT NULL,
  `actuacionesActividadA13` VARCHAR(20) NOT NULL,
  `trabajadoresActividadA13` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadA13` VARCHAR(20) NOT NULL,
  `conciertosActividadB13` VARCHAR(20) NOT NULL,
  `actuacionesActividadB13` VARCHAR(20) NOT NULL,
  `trabajadoresActividadB13` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadB13` VARCHAR(20) NOT NULL,
  `conciertosActividadC13` VARCHAR(20) NOT NULL,
  `actuacionesActividadC13` VARCHAR(20) NOT NULL,
  `trabajadoresActividadC13` VARCHAR(20) NOT NULL,
  `actuacionesTrabajadoresActividadC13` VARCHAR(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `memorias_xml`
  ADD PRIMARY KEY (`codigo`);


ALTER TABLE `memorias_xml`
  MODIFY `codigo` INT(255) NOT NULL AUTO_INCREMENT;
COMMIT;


-- Añadidos 226 campos para evaluaciones a la tabla memorias_xml:
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionA1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionA1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionA1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionB1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionB1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionB1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionC1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionC1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionC1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionA2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionA2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionA2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionB2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionB2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionB2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionC2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionC2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionC2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionA3` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionA3` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionA3` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionB3` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionB3` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionB3` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionC3` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionC3` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionC3` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionA4` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionA4` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionA4` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionB4` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionB4` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionB4` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionC4` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionC4` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionC4` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionA5` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionA5` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionA5` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionB5` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionB5` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionB5` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionC5` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionC5` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionC5` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionA6` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionA6` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionA6` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionB6` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionB6` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionB6` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionC6` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionC6` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionC6` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionA7` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionA7` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionA7` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionB7` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionB7` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionB7` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionC7` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionC7` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionC7` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionA8` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionA8` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionA8` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionB8` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionB8` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionB8` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionC8` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionC8` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionC8` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionA9` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionA9` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionA9` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionB9` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionB9` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionB9` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionC9` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionC9` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionC9` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionA10` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionA10` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionA10` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionB10` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionB10` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionB10` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionC10` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionC10` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionC10` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionA11` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionA11` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionA11` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionB11` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionB11` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionB11` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionC11` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionC11` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionC11` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionA12` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionA12` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionA12` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionB12` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionB12` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionB12` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `trabajadoresEvaluacionC12` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `empresasCentrosEvaluacionC12` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `horasEvaluacionC12` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `formacionTrabajadores` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `formacionEmpresasCentros` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `basicoOtras` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `basicoOnline` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `basicoDistancia` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `basicoPresencial` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `basicoTeoricoPractico` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `basicoTrabajadores` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `basicoEmpresasCentros` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `emergenciasOtras` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `emergenciasOnline` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `emergenciasDistancia` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `emergenciasPresencial` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `emergenciasTeoricoPractico` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `emergenciasTrabajadores` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `emergenciasEmpresasCentros` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaTrabajadores1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaEmpresasCentros1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaHoras1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaMujeres1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaVarones1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaTrabajadores2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaEmpresasCentros2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaHoras2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaMujeres2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaVarones2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaTrabajadores3` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaEmpresasCentros3` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaHoras3` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaMujeres3` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaVarones3` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaTrabajadores4` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaEmpresasCentros4` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaHoras4` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaMujeres4` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaVarones4` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaTrabajadores5` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaEmpresasCentros5` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaHoras5` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaMujeres5` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaVarones5` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaTrabajadores6` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaEmpresasCentros6` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaHoras6` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaMujeres6` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaVarones6` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaTrabajadores7` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaEmpresasCentros7` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaHoras7` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaMujeres7` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaVarones7` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaTrabajadores8` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaEmpresasCentros8` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaHoras8` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaMujeres8` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaVarones8` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaTrabajadores9` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaEmpresasCentros9` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaHoras9` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaMujeres9` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaVarones9` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaTrabajadores10` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaEmpresasCentros10` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaHoras10` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaMujeres10` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaVarones10` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaTrabajadores11` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaEmpresasCentros11` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaHoras11` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaMujeres11` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `medicinaVarones11` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesAccBajaA` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesMortalesA` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesGravesA` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesLevesA` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesIncidentesA` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesInvestigacionesA` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesAccBajaEttA` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesEmpresasA` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesAccBajaB` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesMortalesB` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesGravesB` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesLevesB` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesIncidentesB` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesInvestigacionesB` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesAccBajaEttB` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesEmpresasB` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesAccBajaC` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesMortalesC` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesGravesC` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesLevesC` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesIncidentesC` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesInvestigacionesC` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesAccBajaEttC` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `accidentesEmpresasC` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `casoSBajaA` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `casoCBajaA` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `sospechaSBajaA` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `sospechaCBajaA` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `casoSBajaB` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `casoCBajaB` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `sospechaSBajaB` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `sospechaCBajaB` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `casoSBajaC` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `casoCBajaC` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `sospechaSBajaC` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `sospechaCBajaC` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `otrosA` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `otrosB` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `otrosC` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `otraHoras1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `otraTrabajadores1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `otraEmpresasAct1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `otraEmpresas1` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `otraHoras2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `otraTrabajadores2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `otraEmpresasAct2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;
ALTER TABLE `memorias_xml` ADD `otraEmpresas2` VARCHAR(20) NOT NULL AFTER `actuacionesTrabajadoresActividadC13`;




ALTER TABLE certificados ADD duracion varchar(255) NOT NULL;
ALTER TABLE certificados ADD modalidad varchar(255) NOT NULL;


-- HASTA AQUÍ APLICADO EN LA VERSIÓN DE PRUEBAS -----------------------------------------------------------------------------------------------------------------------------------------------

-- Creación de la tabla empleados_en_ofertas:
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `empleados_en_ofertas` (
  `codigo` int(255) NOT NULL,
  `codigoEmpleado` int(255) DEFAULT NULL,
  `codigoOferta` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;


ALTER TABLE `empleados_en_ofertas`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `fk_empleados_en_ofertas` (`codigoEmpleado`),
  ADD KEY `fk_empleados_en_ofertas2` (`codigoOferta`);


ALTER TABLE `empleados_en_ofertas`
  MODIFY `codigo` int(255) NOT NULL AUTO_INCREMENT;


ALTER TABLE `empleados_en_ofertas`
  ADD CONSTRAINT `fk_empleados_en_ofertas` FOREIGN KEY (`codigoEmpleado`) REFERENCES `empleados` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_empleados_en_ofertas2` FOREIGN KEY (`codigoOferta`) REFERENCES `ofertas` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

-- Eliminados los campos tipoEmpleados y numEmpleados de la tabla ofertas:
ALTER TABLE `ofertas` DROP `tipoEmpleados`;
ALTER TABLE `ofertas` DROP `numEmpleados`;


ALTER TABLE cursos_modulos ADD numeroMostrar varchar(255) NOT NULL;
ALTER TABLE cursos_modulos ADD titulo enum('SI', 'NO') DEFAULT 'NO' NOT NULL;

-- HASTA AQUÍ APLICADO EN LA VERSIÓN ONLINE -----------------------------------------------------------------------------------------------------------------------------------------------