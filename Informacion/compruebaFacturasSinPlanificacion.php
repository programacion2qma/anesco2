<?php
include_once('config.php');//Carga del archivo de configuración
include_once('../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('funciones.php');//Carga de las funciones globales


conexionBD();

$consulta=consultaBD("SELECT codigo FROM facturas WHERE tipoFactura!='ABONO' AND YEAR(fecha)>2019");

while($datos=mysql_fetch_assoc($consulta)){

	if(!compruebaPlanificacionAsociadaFactura($datos['codigo'])){
		echo "<a href='facturas/gestion.php?codigo=".$datos['codigo']."' target='_blank'>".$datos['codigo']."</a><br />";
	}

}

cierraBD();




function compruebaPlanificacionAsociadaFactura($codigoFactura){
    $res=true;
    $pagada=false;
  

    $factura=consultaBD('SELECT facturas.codigo, facturaCobrada, contratos_en_facturas.codigoContrato, formularioPRL.codigo AS codigoFormulario, ofertas.opcion 
                         FROM facturas 
                         INNER JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura 
                         LEFT JOIN formularioPRL ON contratos_en_facturas.codigoContrato=formularioPRL.codigoContrato
                         INNER JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
                         INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
                         WHERE facturas.codigo='.$codigoFactura,false,true);
    
    $vencimientos=consultaBD('SELECT vencimientos_facturas.codigo, estados_recibos.nombre 
                              FROM vencimientos_facturas INNER JOIN estados_recibos ON vencimientos_facturas.codigoEstadoRecibo=estados_recibos.codigo 
                              WHERE codigoFactura='.$factura['codigo']);

    while($item=mysql_fetch_assoc($vencimientos)){
        if($item['nombre']=='PAGADO'){
            $pagada=true;
        }
    }
    
    if($pagada && $factura['codigoFormulario']==NULL && $factura['opcion']!=4){

        $res=false;

    }

    return $res;
}