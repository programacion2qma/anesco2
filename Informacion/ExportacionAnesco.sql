SELECT
	MAX(reconocimientos_medicos.fecha) AS Fecha,
	CONVERT(CAST(CONVERT(empleados.nombre USING latin1) AS binary) USING utf8) AS Nombre,
	CONVERT(CAST(CONVERT(empleados.apellidos USING latin1) AS binary) USING utf8) AS Apellidos,
	FLOOR(DATEDIFF(CURDATE(),fechaNacimiento)/365) AS Edad,
	CONVERT(CAST(CONVERT(clientes.EMPNOMBRE USING latin1) AS binary) USING utf8) AS Empresa,
	CONVERT(CAST(CONVERT(tratamientoHabitual USING latin1) AS binary) USING utf8) AS 'Tratamiento habitual',
	CONVERT(CAST(CONVERT(antecedentesPersonales1 USING latin1) AS binary) USING utf8) AS 'Antecedentes riesgo cardiovascular',
	CONVERT(CAST(CONVERT(antecedentesPersonales2 USING latin1) AS binary) USING utf8) AS 'Antecedentes enf. cardiorespiratorias',
	CONVERT(CAST(CONVERT(antecedentesPersonales3 USING latin1) AS binary) USING utf8) AS 'Antecedentes enf. digestivas',
	CONVERT(CAST(CONVERT(antecedentesPersonales4 USING latin1) AS binary) USING utf8) AS 'Antecedentes enf. genitourinarias',
	CONVERT(CAST(CONVERT(antecedentesPersonales5 USING latin1) AS binary) USING utf8) AS 'Antecedentes enf. ap. locomotor y neur.',
	CONVERT(CAST(CONVERT(antecedentesPersonales6 USING latin1) AS binary) USING utf8) AS 'Antecedentes enf. ORL',
	CONVERT(CAST(CONVERT(antecedentesPersonales7 USING latin1) AS binary) USING utf8) AS 'Antecedentes enf. oftalmológicas',
	CONVERT(CAST(CONVERT(antecedentesPersonales8 USING latin1) AS binary) USING utf8) AS 'Antecedentes cirugía',
	CONVERT(CAST(CONVERT(antecedentesPersonales9 USING latin1) AS binary) USING utf8) AS 'Otros antecedentes'

FROM reconocimientos_medicos INNER JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
INNER JOIN clientes ON empleados.codigoCliente=clientes.codigo

GROUP BY empleados.codigo;