<?php
$seccionActiva=4;
include_once("../cabecera.php");
gestionAgenda();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js"></script>
<script src="../js/filasTabla.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/funciones-gastos.js"></script>
<script type="text/javascript" src="../js/controlHorario.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

    oyenteTodoDia();
    $('input[name=todoDia]').change(function(){
        oyenteTodoDia(); 
    });

    var estadoTareas=$('input[name=estado]:checked').val();
    if(estadoTareas=='PENDIENTE'){
        $('#estado').css('display','none');
    }
    else{
        $('#estado').css('display','block'); 
    }

    $('input[name=estado]').change(function(){
        if($(this).val()=='PENDIENTE'){
            $('#estado').css('display','none');
        }
        else{
            $('#estado').css('display','block'); 
        }
    })

    $('.kilometraje').change(function(){
        oyenteKm($(this));
    });

    oyenteGastos();
    $('input[name=gastos]').change(function(){
        oyenteGastos();
    });

    $('#codigoCliente').change(function(){
        obtenerContratosClientes();
    });

    /*$('.campoTiempo').blur(function(){
    var flag=compruebaCampoTiempo($(this));
    if(flag){
    alert('El formato del campo \'Tiempo empleado\', debe ser HH:MM');
    $(this).val('00:00');
    }
    });*/

    $('.form-horizontal button[type=submit]').unbind();
    $('.form-horizontal button[type=submit]').click(function(e){
        e.preventDefault();
        if($('input:radio[name=estado]:checked').val()=='PENDIENTE'){
            flag=false;
        } else {
            var flag=compruebaCampoTiempo($('#tiempoEmpleado'));
        }
        if(flag){
            alert('El formato del campo \'Tiempo empleado\', debe ser HH:MM');
            $('#tiempoEmpleado').val('00:00');
        } 
        else if(compruebaHorariosTecnicosGestion()) {
            $('.form-horizontal').submit();
        }
    });


});

function oyenteGastos(){
    if($('input[name=gastos]:checked').val()=='SI') {
        $('#gastosActividad').show();
    }
    else{
        $('#gastosActividad').hide();       
    } 
}

function obtenerContratosClientes(){
    var campos={
        'codigoCliente':$('#codigoCliente').val()
    }
    //Fin obtención campos


    var consulta=$.post('../listadoAjax.php?include=tareas&funcion=recuperaContratosXclientes();',campos);//Para no tener 200 ficheros distintos para las múltiples gestiones AJAX, tengo un único archivo en la raíz

    consulta.done(function(respuesta){//La variable respuesta tiene el echo de la función creaAsesoriaCliente() de PHP
        if(respuesta=='fallo'){
            alert('Ha habido un problema al recuperar los datos.Si el problema persiste, contacte con programacion@qmaconsultores.com');
        }
        else{
            var res=respuesta;
            $('#codigoContrato').html(res);
            $('#codigoContrato').selectpicker('refresh');
        }

    });

}

function insertaGasto(tabla){
    insertaFila(tabla);
    $('.kilometraje').change(function(){
        oyenteKm($(this));
    });
}

function oyenteKm(campo){
    var fila=obtieneFilaCampo(campo);
    var km=campo.val().replace(",", ".");
    var importe=km * 0.19;
    $('#importe'+fila).val(importe);
}


function oyenteTodoDia(){
    if($('input[name=todoDia]:checked').val()=='true'){
        $('#todoDia').css('display','none');
    }
    else{
        $('#todoDia').css('display','block'); 
    }
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>