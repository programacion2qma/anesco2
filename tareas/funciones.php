<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de Agenda

function operacionesAgenda(){
    $res=true;
    if(isset($_POST['repetir'])){
        $res=repiteEventoSemanalmente();
    }
    elseif(isset($_GET['realizada'])){
        $res=realizarTarea();
    }
    elseif(isset($_POST['codigo'])){
        $res=actualizaTarea($_POST['codigo']);
    }
    elseif(isset($_POST['tarea'])){
        $res=insertaTarea();
    }
    elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
        $res=eliminaDatos('tareas');
    }
    elseif(isset($_POST['destinatarios'])){
   		$res=enviaCorreos();
  	}

    mensajeResultado('tarea',$res,'sesión');
    mensajeResultado('repetir',$res,'sesión');
    mensajeResultado('elimina',$res,'sesión', true);
    //mensajeResultadoCorreo('destinatarios',$res,'Correo');
}

function insertaTarea(){
	$res = true;
	$datos=arrayFormulario();
	
	if($datos['todoDia'] == 'true'){
		$_POST['todoDia'] = 'true';
		$_POST['horaInicio']='';
		$_POST['horaFin']='';
	} else {
		$_POST['todoDia'] = 'false';
	}
	$res = insertaDatos('tareas');
	$codigoTarea=$res;

	$res = $res && insertaGastos($codigoTarea,false);

	return $res;
}

function actualizaTarea($codigoTarea){
	$res = true;
	$datos=arrayFormulario();
	
	if($datos['todoDia'] == 'true'){
		$_POST['todoDia'] = 'true';
		$_POST['horaInicio']='';
		$_POST['horaFin']='';
	} else {
		$_POST['todoDia'] = 'false';
	}
	$res = actualizaDatos('tareas');

	$res = $res && insertaGastos($codigoTarea,true);

	return $res;
}

function realizarTarea(){
	$res=true;
	$res=cambiaValorCampo('estado','REALIZADA',$_GET['codigo'],'tareas');
	$res=cambiaValorCampo('fechaFin',date('Y-m-d'),$_GET['codigo'],'tareas');
	$datos=datosRegistro('tareas',$_GET['codigo']);
	$res=insertaParteDiarioAsociado($datos,$datos['codigo']);
	return $res;
	//http://localhost:8888/anesco2/tareas/gestion.php?codigo=724
}

function insertaGastos($codigoTarea,$actualizacion=false){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM gastos_tarea WHERE codigoTarea='$codigoTarea';");
	}

	$datos=arrayFormulario();

	if($datos['gastos']=='SI'){
		for($i=0;isset($datos['itinerario'.$i]);$i++){
			$codigoContrato=$datos['codigoContrato'];
			$itinerario=$datos['itinerario'.$i];
			$kilometraje=$datos['kilometraje'.$i];
			$importe=$datos['importe'.$i];
			$peaje=$datos['peaje'.$i];
			$taxi=$datos['taxi'.$i];
			$parking=$datos['parking'.$i];
			$dietas=$datos['dietas'.$i];
			$otros=$datos['otros'.$i];
			$total=$datos['total'.$i];
			$observaciones=$datos['observacionesGastos'.$i];
			$estado=$datos['estado'.$i];		

			$res=$res && consultaBD("INSERT INTO gastos_tarea VALUES(NULL,'$codigoContrato','$itinerario','$kilometraje','$importe','$peaje','$taxi','$parking','$dietas','$otros','$total','$observaciones','$estado','$codigoTarea');");
		}

	}

	if($datos['estado']=='REALIZADA'){
		insertaParteDiarioAsociado($datos,$codigoTarea);
	}

	return $res;
}

function insertaParteDiarioAsociado($datos,$codigoTarea){
	$res=false;

	$res=insertaDatosParteDiario($datos,$codigoTarea);
	if($res['inserta']){
		$res= $res && insertaActividad($res['codigoParte'],$datos,$codigoTarea);
	}
	//$res= $res && insertaGastosParteDiario($codigoParte,false,$datos);
	return $res;
}

function insertaDatosParteDiario($datos,$codigoTarea){
	$res=array('inserta'=>true,'codigoParte'=>'');
	$parte=datosRegistro('tareas_parte_diario',$codigoTarea,'codigoTarea');
	if($parte){
		$res['inserta']=false;
		$res['codigoParte']=$parte['codigoParteDiario'];
		$consulta=consultaBD('UPDATE tareas_parte_diario SET tiempo="'.$datos['tiempoEmpleado'].'" WHERE codigo='.$parte['codigo'],$conexion=true);
	} else {
		$parte=consultaBD('SELECT * FROM parte_diario WHERE fecha="'.$datos['fechaFin'].'" AND codigoUsuario='.$datos['codigoUsuario'],true,true);
		if($parte){
			$res['codigoParte']=$parte['codigo'];
			if($parte['gastos']=='NO' && $datos['gastos']=='SI'){
				$consulta=consultaBD('UPDATE parte_diario SET gastos="SI" WHERE codigo='.$parte['codigo'],true);
			}
		} else {
			$consulta=consultaBD("INSERT INTO parte_diario VALUES(NULL,'".$datos['fechaFin']."','".$datos['gastos']."','".$datos['codigoUsuario']."');");
			if($consulta){
				$res['codigoParte']=mysql_insert_id();
			}
		}
	}
	
	return $res;

}

function insertaActividad($codigoParte,$datos,$codigoTarea){
	$res=true;

	$codigoContrato=$datos['codigoContrato'];

	$res=$res && consultaBD("INSERT INTO tareas_parte_diario VALUES(NULL,$codigoContrato,$codigoTarea,'$codigoParte','".$datos['tiempoEmpleado']."');");

	return $res;
}

function insertaGastosParteDiario($codigoParte,$actualizacion=false,$datos){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM gastos_parte_diario WHERE codigoParte='$codigoParte';");
	}

	for($i=0;isset($datos['codigoContratoGastos'.$i]);$i++){
		$codigoContrato=$datos['codigoContratoGastos'.$i];
		$itinerario=$datos['itinerario'.$i];
		$kilometraje=$datos['kilometraje'.$i];
		$importe=$datos['importe'.$i];
		$peaje=$datos['peaje'.$i];
		$taxi=$datos['taxi'.$i];
		$parking=$datos['parking'.$i];
		$dietas=$datos['dietas'.$i];
		$otros=$datos['otros'.$i];
		$total=$datos['total'.$i];
		$observaciones=$datos['observacionesGastos'.$i];
		$estado=$datos['estado'.$i];		

		$res=$res && consultaBD("INSERT INTO gastos_parte_diario VALUES(NULL,'$codigoContrato','$itinerario','$kilometraje','$importe','$peaje','$taxi','$parking','$dietas','$otros','$total','$observaciones','$estado','$codigoParte');");
	}

	return $res;
}

function generaEventosCalendarioTareas($colores){
	//$codigoU=$_SESSION['codigoS'];
	conexionBD();

	$where=defineWhereEmpleadoTarea(false);
	$consulta=consultaBD("SELECT tareas.codigo, clientes.EMPNOMBRE, clientes.EMPMARCA, clientes.EMPTELPRINC, clientes.EMPLOC, tareas.tarea, tareas.fechaInicio, tareas.fechaFin, tareas.todoDia, tareas.horaInicio, 
						  tareas.horaFin, tareas.estado, tareas.prioridad, usuarios.colorTareas 
        				  FROM clientes RIGHT JOIN tareas ON clientes.codigo=tareas.codigoCliente 
        				  LEFT JOIN usuarios ON tareas.codigoUsuario=usuarios.codigo 
        				  $where 
        				  ORDER BY tareas.fechaInicio ASC;");

	$tareas="";
	$clases=array('ALTA'=>"backgroundColor: '#ec5b58', borderColor: '#b94a48'",'NORMAL'=>"backgroundColor: '#5e96ea', borderColor: '#3f85f5'",'BAJA'=>"backgroundColor: '#b0b0b0', borderColor: '#979797'");

	while($datos=mysql_fetch_assoc($consulta)){
		if($colores=='SI'){
			if($datos['colorTareas']!=''){
				$color="backgroundColor: '".$datos['colorTareas']."', borderColor: '".$datos['colorTareas']."'";
			} else {
				$color="backgroundColor: 'red', borderColor: '#FFF'";
			}
		} else {
			$color=$clases[$datos['prioridad']];
		}
		$fechaInicio=explode('-',$datos['fechaInicio']);
		$fechaFin=explode('-',$datos['fechaFin']);
		$fechaInicio[1]--;
		$fechaFin[1]--;

		$nombreTarea=obtieneNombreTarea($datos['tarea']);

		$tareas.="
			{
				id: ".$datos['codigo'].",
		    	title: '-".$datos['EMPNOMBRE']. '('.$datos['EMPMARCA']."): ".$nombreTarea."',";
		if($datos['todoDia']=='true'){
			$tareas.="
			    start: new Date(".$fechaInicio[0].", ".$fechaInicio[1].", ".$fechaInicio[2]."),
		    	end: new Date(".$fechaFin[0].", ".$fechaFin[1].", ".$fechaFin[2]."),
		    	allDay: true,
		    	url: 'gestion.php?codigo=".$datos['codigo']."',
		    	".$color."
		    	
	    	},";
		}
		else{
			$horaInicio=explode(':',$datos['horaInicio']);
			$horaFin=explode(':',$datos['horaFin']);

		    $tareas.="
			    start: new Date(".$fechaInicio[0].", ".$fechaInicio[1].", ".$fechaInicio[2].", ".$horaInicio[0].", ".$horaInicio[1]."),
		    	end: new Date(".$fechaFin[0].", ".$fechaFin[1].", ".$fechaFin[2].", ".$horaFin[0].", ".$horaFin[1]."),
		    	allDay: false,
		    	url: 'gestion.php?codigo=".$datos['codigo']."',
		    	".$color."
	    	},";
		}

  	}

  	cierraBD();

  	$tareas=substr_replace($tareas, '', strlen($tareas)-1, strlen($tareas));//Para quitar última coma
  	echo $tareas;
}


function generaDatosGraficoTareas(){
	$datos=array();
	//$codigoS=$_SESSION['codigoS'];
	$where=compruebaPerfilParaWhere();

	conexionBD();

	$consulta=consultaBD("SELECT COUNT(tareas.codigo) AS codigo FROM tareas $where AND tareas.estado='pendiente';",false,true);
	$datos['pendientes']=$consulta['codigo'];

	$consulta=consultaBD("SELECT COUNT(tareas.codigo) AS codigo FROM tareas $where AND tareas.estado='realizada';",false,true);
	$datos['realizadas']=$consulta['codigo'];

	cierraBD();

	return $datos;
}


function repiteEventoSemanalmente(){
	$res=true;
	$codigoEvento=$_POST['codigo'];
	
	conexionBD();
	$datos=consultaBD("SELECT * FROM tareas WHERE codigo='$codigoEvento';",false,true);

	date_default_timezone_set('Europe/Madrid');//Necesario para que el servidor no dé error.
	$fechaInicio=sumaDiasFecha($datos['fechaInicio'],7);
	$fechaFin=sumaDiasFecha($datos['fechaFin'],7);

	$datos['codigoCliente']=compruebaCampoNULL($datos['codigoCliente']);
	$datos['codigoIncidencia']=compruebaCampoNULL($datos['codigoIncidencia']);

	$res=$res && consultaBD("INSERT INTO tareas VALUES(NULL, '".$datos['tarea']."', '$fechaInicio', '$fechaFin', '".$datos['horaInicio']."','".$datos['horaFin']."','".$datos['todoDia']."','".$datos['fechaRecordatorio']."','PENDIENTE','".$datos['prioridad']."','".$datos['observaciones']."','".$datos['codigoUsuario']."',".$datos['codigoCliente'].",".$datos['codigoIncidencia'].");");
	cierraBD();

	return $res;
}

function compruebaCampoNULL($campo){
	if($campo==NULL){
		$campo='NULL';
	}

	return $campo;
}

function gestionEventos(){
	$res=false;

	$res = insertaDatos('tareas');

	echo $res;
}

function actualizaTiempo(){
	$datos=arrayFormulario();
	$res=consultaBD("UPDATE tareas SET fechaInicio='".$datos['fechaI']."', fechaFin='".$datos['fechaF']."', horaInicio='".$datos['horaI']."', horaFin='".$datos['horaF']."', todoDia='".$datos['todoDia']."' WHERE codigo=".$datos['codigo'],true);
}

function gestionAgenda(){
	operacionesAgenda();

	abreVentanaGestionConBotones('Gestión de agenda','index.php','span3','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('tareas');
	
	campoTarea($datos);

	cierraColumnaCampos(true);
	abreColumnaCampos();

		campoSelectConsulta('codigoCliente','Cliente','SELECT codigo, CONCAT(EMPNOMBRE," (",EMPMARCA,")") AS texto FROM clientes WHERE activo="SI" ORDER BY EMPNOMBRE',$datos);
		campoSelect('prioridad','Prioridad',array('Normal','Baja','Alta'),array('NORMAL','BAJA','ALTA'),$datos,'selectpicker span2 show-tick',"");
		campoFecha('fechaInicio','Fecha de inicio',$datos);
		$diaCompleto = $datos['todoDia'] == 'true' ? 'true':'false';
		campoRadio('todoDia','Día completo',$diaCompleto,'false',array('Si','No'),array('true','false'));
		echo "<div id='todoDia'>";
			campoHora('horaInicio','Hora de inicio',$datos);
			campoHora('horaFin','Hora de fin',$datos);
		echo "</div>";

	cierraColumnaCampos();
	abreColumnaCampos();

		campoSelectContrato($datos);
		
		$codigoUsuario=$_SESSION['codigoU'];
		if($datos){
			$codigoUsuario=$datos['codigoUsuario'];
		} 

		if($_SESSION['tipoUsuario'] == 'ADMIN'){
			campoSelectConsulta('codigoUsuario','Usuario asignado',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto  FROM usuarios WHERE tipo='TECNICO' OR tipo='MEDICO'",$codigoUsuario);
		} else {
			$usuario=datosRegistro('usuarios',$codigoUsuario);
			campoOculto($codigoUsuario,'codigoUsuario');
			campoDato('Usuario asignado',$usuario['nombre'].' '.$usuario['apellidos']);

		}
		campoFecha('fechaFin','Fecha de fin',$datos);
		campoRadio('estado','Estado',$datos,'PENDIENTE',array('Pendiente','Realizada'),array('PENDIENTE','REALIZADA'));

	cierraColumnaCampos(true);
	abreColumnaCampos();

		echo "<div id='estado'>";
		echo "<h3 class='apartadoFormulario'>Realizada</h3>";
			campoTextoSimbolo('tiempoEmpleado','Tiempo empleado','HH:MM',$datos,'input-mini paginationRight campoTiempo');
			campoRadio('gastos','¿Incluir Gastos?',$datos,'NO');

			echo "<div id='gastosActividad' style='display: none;'>";
			creaTablaGastos($datos);
			echo "</div>";	
		echo "</div>";

		echo "<h3 class='apartadoFormulario'>Observaciones</h3>";
		areaTexto('observaciones','Observaciones',$datos,'areaInforme');


	cierraVentanaGestion('index.php',true);
}

/*function gestionAgenda(){
	operacionesAgenda();

	abreVentanaGestionConBotones('Gestión de Agenda','index.php','span3');
	$datos=compruebaDatos('tareas');
	
	campoTarea($datos);

	campoSelectConsulta('codigoCliente','Cliente','SELECT codigo, EMPNOMBRE AS texto FROM clientes WHERE activo="SI" ORDER BY EMPNOMBRE',$datos);
	campoSelectContrato($datos);
	campoFecha('fechaInicio','Fecha de inicio',$datos);
	campoFecha('fechaFin','Fecha de fin',$datos);

	$diaCompleto = $datos['todoDia'] == 'true' ? 'true':'false';
	campoRadio('todoDia','Día completo',$diaCompleto,'false',array('Si','No'),array('true','false'));

	cierraColumnaCampos();
	abreColumnaCampos();

	echo "<div id='todoDia'>";
		campoHora('horaInicio','Hora de inicio',$datos);
		campoHora('horaFin','Hora de fin',$datos);
	echo "</div>";
	campoTextoSimbolo('tiempoEmpleado','Tiempo empleado','horas',$datos);
	campoRadio('estado','Estado',$datos,'PENDIENTE',array('Pendiente','Realizada'),array('PENDIENTE','REALIZADA'));

	campoSelect('prioridad','Prioridad',array('Normal','Baja','Alta'),array('NORMAL','BAJA','ALTA'),$datos,'selectpicker span2 show-tick',"");
	$tecnico='';
	if($datos){
		$tecnico=$datos['codigoUsuario'];
	} elseif ($_SESSION['tipoUsuario'] == 'TECNICO'){
		$tecnico=$_SESSION['codigoU'];
	} 
	campoSelectConsulta('codigoUsuario','Técnico asignado',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE tipo='TECNICO'",$tecnico);

	cierraColumnaCampos(true);
	abreColumnaCampos();

	areaTexto('observaciones','Observaciones',$datos,'areaInforme');


	cierraVentanaGestion('index.php',true);
}*/


function campoTarea($datos){
	if(is_numeric($datos['tarea'])){
		campoSelectConsulta('tarea','Tarea',"SELECT codigo, nombre AS texto FROM tipos_tareas ORDER BY nombre;",$datos);
	}
	else{
		campoTexto('tarea','Tarea',$datos,'span6');
	}
}

function campoSelectConsultaBlanco($nombreCampo,$texto,$consulta,$valor=false,$clase='selectpicker span3 show-tick busquedaSelect',$busqueda="data-live-search='true'",$disabled='',$tipo=0,$blanco=false){
	$valor=compruebaValorCampo($valor,$nombreCampo);
	if($disabled){
		$disabled="disabled='disabled'";
	}

	if($tipo==0){
		echo "
		<div class='control-group'>
			<label class='control-label' for='$nombreCampo'>$texto:</label>
			<div class='controls'>";
	}
	elseif($tipo==1){
		echo "<td>";
	}

	echo "<select name='$nombreCampo' id='$nombreCampo' class='$clase' $busqueda $disabled>";

		$consulta=consultaBD($consulta,true);
		$datos=mysql_fetch_assoc($consulta);
		if($blanco){
			echo "<option value='NULL'></option>";
		}
		while($datos!=false){
			echo "<option value='".$datos['codigo']."'";

			if($valor!=false && $valor==$datos['codigo']){
				echo " selected='selected'";
			}

			echo ">".$datos['texto']."</option>";
			$datos=mysql_fetch_assoc($consulta);
		}

	echo "</select>";

	if($tipo==0){
		echo "
			</div> <!-- /controls -->
		</div> <!-- /control-group -->";
	}
	elseif($tipo==1){
		echo "</td>";
	}
}

function imprimeTareas($inicio=false){
    //$codigoS=$_SESSION['codigoS'];

    conexionBD();
    
    $where=defineWhereEmpleadoTarea(false);

    if($inicio){
        $where.=" AND estado!='REALIZADA'";
    }
    else{
        $where.=" AND estado='REALIZADA'";
    }
    $consulta=consultaBD("SELECT tareas.codigo, clientes.EMPNOMBRE, clientes.EMPMARCA, clientes.EMPTELPRINC, clientes.EMPLOC, tareas.tarea, 
    					  tareas.fechaInicio, tareas.estado, tareas.prioridad, tareas.codigoUsuario 
        				  FROM clientes RIGHT JOIN tareas ON clientes.codigo=tareas.codigoCliente 
        				  $where 
        				  ORDER BY tareas.fechaInicio ASC;");

    $prioridad=array('NORMAL'=>"<span class='label label-info'>Normal</span>",'ALTA'=>"<span class='label label-danger'>Alta</span>",'BAJA'=>"<span class='label'>Baja</span>");
    $estado=array('PENDIENTE'=>"<span class='label label-warning'>Pendiente</span>",'REALIZADA'=>"<span class='label label-success'>Realizada</span>");

    while($datos=mysql_fetch_assoc($consulta)){
    	$tarea=obtieneNombreTarea($datos['tarea']);
        $fecha=formateaFechaWeb($datos['fechaInicio']);
        $tecnico=consultaBD("SELECT * FROM usuarios WHERE codigo='".$datos['codigoUsuario']."';",false,true);

        if($datos['EMPMARCA']!=NULL && $datos['EMPMARCA']!=''){
        	$datos['EMPNOMBRE'].=' ('.$datos['EMPMARCA'].')';
        }

        echo "
        <tr>
            <td> ".$datos['EMPNOMBRE']."</td>
            <td> ".$datos['EMPLOC']." </td>
            <td class='nowrap'> ".formateaTelefono($datos['EMPTELPRINC'])." </td>
            <td> ".$tarea." </td>
            <td> $fecha </td>
            <td> ".$tecnico['nombre']." ".$tecnico['apellidos']." </td>
            <td> ".$prioridad[$datos['prioridad']]." </td>";
        
        if(!$inicio){
            echo "
            <td> ".$estado[$datos['estado']]." </td>
            <td class='centro'>
                <a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio noAjax'><i class='icon-search-plus'></i> Ver datos</i></a>
            </td>";
        }
        else{
            echo "
            <td class='centro'>
                <a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio noAjax'><i class='icon-search-plus'></i> Ver datos</i></a>
                <a href='index.php?codigo=".$datos['codigo']."&realizada' class='btn btn-success'><i class='icon-check-circle'></i> Realizada</i></a>
            </td>";
        }

        echo "
            <td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
        </tr>";
    }
    cierraBD();
}


function filtroTecnico($colores,$codigoUsuarioFiltro=false){

	echo '<form action="?" method="post" id="filtroUsuario">';
	campoSelectConsulta('codigoUsuarioFiltro','Técnico','SELECT codigo, CONCAT(nombre," ",apellidos) AS texto FROM usuarios WHERE tipo <> "CLIENTE" AND tipo <> "EMPLEADO" ORDER BY nombre',$codigoUsuarioFiltro);
	campoOculto($colores,'colores');
	echo '</form>';
}


function campoSelectContrato($datos=false){
	$contratos=consultaBD('SELECT * FROM contratos WHERE eliminado="NO"',true);	
	$nombres=array('');
	$valores=array('NULL');
	$opciones=array('','SPA 4 Especialidades','SPA Vigilancia de la salud','SPA Especialides técnicas','Otras actuaciones',''=>'');
	while($contrato=mysql_fetch_assoc($contratos)){
		$cliente=consultaBD('SELECT clientes.*, ofertas.opcion FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo='.$contrato['codigoOferta']." AND ofertas.eliminado='NO'",true,true);
		$referencia=formateaReferenciaContrato($cliente,$contrato);
		array_push($nombres, 'Nº: '.$referencia.' - '.$cliente['EMPNOMBRE'].' - '.$opciones[$cliente['opcion']]);
		array_push($valores, $contrato['codigo']);
	}
	campoSelect('codigoContrato','Contrato',$nombres,$valores,$datos);
}

function obtieneContratosCliente(){
	$datos=arrayFormulario();
	extract($datos);

	if($_SESSION['tipoUsuario'] == 'COLABORADOR'){
		$consulta=consultaBD("SELECT contratos.codigo, EMPCP, EMPID, contratos.fechaInicio, contratos.codigoInterno
							  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
							  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
							  LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
							  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
							  LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
							  WHERE contratos.eliminado='NO' AND clientes.codigo='$codigoCliente'
							  codigoCliente IN (SELECT codigo FROM clientes WHERE EMPASESORIA=".$_SESSION['codigoU'].") 
							  GROUP BY contratos.codigo 
							  ORDER BY contratos.fechaInicio;",true);
	}
	elseif($_SESSION['tipoUsuario'] == 'TECNICO'){
		$consulta=consultaBD("SELECT contratos.codigo, EMPCP, EMPID, contratos.fechaInicio, contratos.codigoInterno
							  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
							  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
							  LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
							  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
							  LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
							  WHERE contratos.eliminado='NO' AND clientes.codigo='$codigoCliente'
							  AND (contratos.tecnico=".$_SESSION['codigoU']." || formularioPRL.codigoUsuarioTecnico=".$_SESSION['codigoU'].") 
							  GROUP BY contratos.codigo 
							  ORDER BY contratos.fechaInicio;",true);

	}
	else {
		$consulta=consultaBD("SELECT contratos.codigo, EMPCP, EMPID, contratos.fechaInicio, contratos.codigoInterno
							  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
							  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
							  LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
							  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura='PROFORMA'
							  LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
							  WHERE contratos.eliminado='NO' AND clientes.codigo='$codigoCliente'
							  GROUP BY contratos.codigo 
							  ORDER BY contratos.fechaInicio;",true);
	}

	$res="<option value='NULL'></option>";
	while($datos=mysql_fetch_assoc($consulta)){
		$res.="<option value='".$datos['codigo']."'>".formateaReferenciaContrato($datos,$datos)."</option>";
	}

	echo $res;
}

function creaTablaGastos($datos){
    echo "
    <h3 class='apartadoFormulario'>Gastos</h3>
    <div class='centro'>
        <table class='table table-striped tabla-simple mitadAncho' id='tablaGastosTareas'>
          <thead>
            <tr>
              <th> Itinerario </th>
              <th> Kilometraje </th>
              <th> Importe Km </th>
              <th> Peaje </th>
              <th> Táxi/Autobús </th>
              <th> Parking </th>
              <th> Dietas </th>
              <th> Otros </th>
              <th> Total </th>
              <th> Observaciones </th>
              <th> Estado </th>
              <th> </th>                            
            </tr>
          </thead>
          <tbody>";
        
            $i=0;

            if($datos){
                $consulta=consultaBD("SELECT * FROM gastos_tarea WHERE codigoTarea='".$datos['codigo']."'");

                while($gasto=mysql_fetch_assoc($consulta)){
                    imprimeLineaTablaGasto($i,$gasto);
                    $i++;
                }
            }
            
            if($i==0){
                imprimeLineaTablaGasto(0,false);
            }
      
    echo "</tbody>
        </table>
        <br />
        <button type='button' class='btn btn-small btn-success' onclick='insertaGasto(\"tablaGastosTareas\");'><i class='icon-plus'></i> Añadir Gasto</button> 
        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaGastosTareas\");'><i class='icon-trash'></i> Eliminar Gasto</button>
    </div>
    <br />";
}

function imprimeLineaTablaGasto($i,$datos){
    $j=$i+1;

    echo "<tr>";
			campoTextoTabla('itinerario'.$i,$datos['itinerario'],'input-mini');
			campoTextoTabla('kilometraje'.$i,$datos['kilometraje'],'input-mini kilometraje');
			campoTextoTabla('importe'.$i,$datos['importe'],'input-mini');
			campoTextoTabla('peaje'.$i,$datos['peaje'],'input-mini');
			campoTextoTabla('taxi'.$i,$datos['taxi'],'input-mini');
			campoTextoTabla('parking'.$i,$datos['parking'],'input-mini');
			campoTextoTabla('dietas'.$i,$datos['dietas'],'input-mini');
			campoTextoTabla('otros'.$i,$datos['otros'],'input-mini');
			campoTextoTabla('total'.$i,$datos['total'],'input-mini');
			campoTextoTabla('observacionesGastos'.$i,$datos['observacionesGastos'],'input-mini');
			campoSelect('estado'.$i,'',array('PENDIENTE','PAGADO'),array('Pendiente','Pagado'),$datos['estado'],'selectpicker span2 show-tick',"data-live-search='true'",1);
    echo "	<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    	  </tr>";
}

function campoSelectContratoGastos($i,$datos=false,$codigoTecnico){
	if($_SESSION['tipoUsuario']=='TECNICO'){
		$contratos=consultaBD("SELECT * FROM contratos WHERE eliminado='NO' AND tecnico='".$codigoTecnico."'",true);
	}else{
		$contratos=consultaBD('SELECT * FROM contratos WHERE eliminado="NO"',true);		
	}
	$nombres=array('');
	$valores=array('NULL');
	while($contrato=mysql_fetch_assoc($contratos)){
		$cliente=consultaBD('SELECT clientes.* FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo='.$contrato['codigoOferta']." AND ofertas.eliminado='NO'",true,true);
		$referencia=formateaReferenciaContrato($cliente,$contrato);
		array_push($nombres, 'Nº: '.$referencia.' - '.$cliente['EMPNOMBRE']);
		array_push($valores, $contrato['codigo']);
	}
	campoSelect('codigoContratoGastos'.$i,'',$nombres,$valores,$datos['codigoContratoGastos'],'selectpicker span3 show-tick',"data-live-search='true'",1);
}

function recuperaContratosXclientes(){

	$datos=arrayFormulario();
	$respuesta='fallo';

	conexionBD();

	$contratos=consultaBD('SELECT * FROM contratos WHERE eliminado="NO"',true);	
	//$nombres=array('');
	//$valores=array('NULL');
	$nombres='';
	$valores='';	
	$opciones=array('','SPA 4 Especialidades','SPA Vigilancia de la salud','SPA Especialides técnicas','Otras actuaciones',''=>'');
	if($contratos!=false){	
		$respuesta='';	
		while($contrato=mysql_fetch_assoc($contratos)){
			if($datos['codigoCliente']!=NULL){
				$cliente=consultaBD('SELECT clientes.*, ofertas.opcion AS opcionContrato FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo='.$contrato['codigoOferta']." AND ofertas.eliminado='NO' AND ofertas.codigoCliente='".$datos['codigoCliente']."'",true,true);
				echo 'SELECT clientes.*, ofertas.opcion AS opcionContrato FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo='.$contrato['codigoOferta']." AND ofertas.eliminado='NO' AND ofertas.codigoCliente='".$datos['codigoCliente']."'";
			}else{
				$cliente=consultaBD('SELECT clientes.*, ofertas.opcion AS opcionContrato FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo='.$contrato['codigoOferta']." AND ofertas.eliminado='NO'",true,true);				
			}
			$referencia=formateaReferenciaContrato($cliente,$contrato);
			if($cliente['EMPNOMBRE']!=''){
				$nombres='Nº: '.$referencia.' - '.$cliente['EMPNOMBRE'].' - '.$opciones[$cliente['opcionContrato']];
				$valores=$contrato['codigo'];
				$respuesta.="<option value='".$valores."'>".$nombres."</option>";
			}
		}
	}

	cierraBD();

	echo $respuesta;
}

//Fin parte de Agenda