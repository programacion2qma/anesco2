<?php
  $seccionActiva=4;
  include_once('../cabecera.php');

  if((isset($_GET['colores']) && $_GET['colores']=='SI') || ((isset($_POST['colores']) && $_POST['colores']=='SI'))){
    $colores='SI';
  } else {
    $colores='NO';
  }
  $codigoFiltroUsuario=false;
  if(isset($_POST['codigoUsuarioFiltro'])){
    $codigoFiltroUsuario=$_POST['codigoUsuarioFiltro'];
  }
  operacionesAgenda();

  /*$tareas=consultaBD('SELECT * FROM tareas',true);
  while($tarea=mysql_fetch_assoc($tareas)){
    $tiempo='00:00';
    if($tarea['tiempoEmpleado']>0){
      $horas=0;
      $tiempoEmpleado=$tarea['tiempoEmpleado'];
      while($tiempoEmpleado>=60){
        $horas++;
        $tiempoEmpleado=$tiempoEmpleado=$tiempoEmpleado-60;
      }
      $min=$tiempoEmpleado;
      $horas=$horas<10?'0'.$horas:$horas;
      $min=$min<10?'0'.$min:$min;
      $tiempo=$horas.':'.$min;
    }
    echo $tarea['codigo'].' - '.$tarea['tiempoEmpleado'].' = '.$tiempo.'<br/>';
    $res=consultaBD('UPDATE tareas SET tiempoEmpleado="'.$tiempo.'" WHERE codigo='.$tarea['codigo'],true);
  }*/
?>

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      
      <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container tareas">
                <div class="widget-content">
                  <h6 class="bigstats tareas">Estadísticas del sistema para el área de agenda:</h6>

                   <canvas id="grafico" class="chart-holder" height="165" width="600"></canvas>
                   <div class="leyenda" id="leyenda"></div>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
        </div>


        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de agenda</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="gestion.php" class="shortcut noAjax"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Registrar tarea</span></a>
                <?php 
                if($_SESSION['tipoUsuario']=='ADMIN'){
                    if($colores=='SI'){
                        echo '
                        <a href="index.php" class="shortcut"><i class="shortcut-icon icon-exclamation"></i><span class="shortcut-label">Ver agenda por colores de prioridad</span></a>';
                    } 
                    else {
                        echo '
                        <a href="index.php?colores=SI" class="shortcut"><i class="shortcut-icon icon-user"></i><span class="shortcut-label">Ver agenda por colores de usuario</span></a>';
                    } 
                    
                    echo '
                    <a href="'.$_CONFIG['raiz'].'tipos-tareas/" class="shortcut"><i class="shortcut-icon icon-list-ol"></i><span class="shortcut-label">Tipos de tareas</span></a>';
                    filtroTecnico($colores,$codigoFiltroUsuario);
                }
                ?>
                <a href="javascript:void" id="eliminar" class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      </div>

      <div class="row">
        <div class="span12">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-calendar"></i>
               <h3>Agenda de tareas</h3>
            </div>
            <div class="widget-content">
              <div id='calendar'></div>
            </div>
          </div>
        </div>
      </div>

      <div class="widget widget-table action-table">
        <div class="widget-header" id="controladorTareasPendientes"> <i class="icon-flag"></i>
          <h3>Tareas pendientes</h3>
        </div>
        <div id="tareasPendientesOculto" class="widget-content hide" oculto="SI">
        <!-- /widget-header -->
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th> Empresa </th>
                <th> Localidad </th>
                <th> Teléfono </th>
                <th> Tarea </th>
                <th> Fecha de inicio </th>
                <th> Técnico </th>
                <th> Proridad </th>
                <th class="centro"> </th>
                <th><input type='checkbox' id="todo"></th>
              </tr>
            </thead>
            <tbody>

              <?php
                imprimeTareas(true);
              ?>
            
            </tbody>
          </table>
        </div>
        <!-- /widget-content-->
      </div>      

      <br />

      <div class="widget widget-table action-table">
        <div class="widget-header" id="controladorTareasRealizadas"> <i class="icon-check-circle"></i>
          <h3>Tareas realizadas</h3>
        </div>
        <div id="tareasRealizadasOculto" class="widget-content hide" oculto="SI">
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th> Empresa </th>
                <th> Localidad </th>
                <th> Teléfono </th>
                <th> Tarea </th>
                <th> Fecha de inicio </th>
                <th> Técnico </th>
                <th> Proridad </th>
                <th> Estado </th>
                <th class="centro"> </th>
                <th><input type='checkbox' id="todo"></th>
              </tr>
            </thead>
            <tbody>

              <?php
                imprimeTareas(false);
              ?>
            
            </tbody>
          </table>
        </div>
        <!-- /widget-content-->
        </div>
      </div>      

      </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 


  <!-- Caja de tarea -->
  <div id='cajaTarea' class='modal hide fade cajaSelect'> 
    <div class='modal-header'> 
      <a class='close noAjax' id='cierra'>&times;</a>
      <h3> <i class="icon-calendar"></i><i class="icon-chevron-right"></i><i class="icon-plus-circle"></i> &nbsp; Nueva tarea</h3> 
    </div> 
    <div class='modal-body cajaSelect'>
      
      <form id="edit-profile" class="form-horizontal" method="post">
        <fieldset>
          <?php
            //campoTexto('nombreTarea','Tarea');
            campoSelectConsulta('nombreTarea','Tarea',"SELECT codigo, nombre AS texto FROM tipos_tareas WHERE eliminado='NO' ORDER BY nombre;");

            campoSelectConsulta('codigoCliente','Cliente','SELECT codigo, CONCAT(EMPNOMBRE," (",EMPMARCA,")") AS texto FROM clientes WHERE activo="SI" ORDER BY EMPNOMBRE');
            //campoSelectContrato();
            campoSelect('codigoContrato','Contrato',array(''),array('NULL'));

            campoSelect('prioridad','Prioridad',array('Normal','Baja','Alta'),array('NORMAL','BAJA','ALTA'),'','selectpicker span2 show-tick',"");
            areaTexto('observacionesTarea','Observaciones');
            campoOculto('PENDIENTE','estadoTarea');
            campoOculto('','codigoUsuario',$_SESSION['codigoU']);
            campoOculto('0','tiempoEmpleado');
            campoOculto('NO','gastos');
  
          ?>
        </fieldset>
      </form>

    </div> 
    <div class='modal-footer'> 
      <button type="button" class="btn btn-propio" id="registrar"><i class="icon-check"></i> Registrar tarea</button> 
      <button type="button" class="btn btn-default" id='cancelar'><i class="icon-remove"></i> Cancelar</button> 
    </div> 
  </div>
  <!-- Fin caja de tarea -->

  <!-- Caja de opciones -->
  <div id='cajaOpciones' class='modal hide fade'> 
    <div class='modal-header'> 
      <a class='close' data-dismiss='modal'>&times;</a>
      <h3> <i class="icon-calendar"></i><i class="icon-chevron-right"></i><i class="icon-cogs"></i> &nbsp; Opciones de tarea </h3> 
    </div> 
    <div class='modal-body centro'>
      <a href='#' class="btn btn-primary" id='enlace'><i class="icon-search-plus"></i> Detalles de tarea</a> 
      <button type="button" class="btn btn-success" id='repetirCita'><i class="icon-calendar"></i> Repetir semanalmente</button>
      <button type="button" class="btn btn-danger" id='eliminarTarea'><i class="icon-trash"></i> Eliminar tarea</button>
    </div> 
  </div>
  <!-- Fin caja de opciones -->
</div>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>

<script src="../../api/js/full-calendar/jquery-ui.custom.min.js" type="text/javascript" ></script><!-- Habilita el drag y el resize -->
<script src="../../api/js/full-calendar/fullcalendar.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/chart.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>js/moment.js" type="text/javascript"></script><!-- Para formatear fecha calendario -->

<script src="../js/controlHorario.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
  $('.selectpicker').selectpicker();
  $('#codigoUsuarioFiltro').change(function(){
    $('#filtroUsuario').submit();
  });


  var colorFondo={'NORMAL':"#7ba9ee",'ALTA':"#ea807e",'BAJA':"#7acc76"};
  var colorBorde={'NORMAL':"#3f85f5",'ALTA':"#b94a48",'BAJA':"#00a100"};

  var calendario = $('#calendar').fullCalendar({
    header: {
      left: 'prev today next',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    defaultView: 'month',
    selectable:true,
    selectHelper: true,
    editable:true,
    eventLimit:true,
    select: function(start, end, allDay, event) {
      var fechaI = moment(start).format('DD/MM/YYYY');
      var fechaF = moment(end).format('DD/MM/YYYY');
      var horaI = moment(start).format('HH:mm');
      var horaF = moment(end).format('HH:mm');

      if(compruebaHorariosTecnicosAgenda(fechaI,fechaF,horaI,horaF,allDay)){
        abreVentana();
      
        $('#registrar').click(function(){

            cierraVentana();

            var codigoTarea=$('#nombreTarea').val();
            var nombreTarea=$('#nombreTarea option:selected').text();
            var codigoCliente=$('#codigoCliente').val();
            var estadoTarea=$('#estadoTarea').val();
            var prioridad=$('#prioridad').val();
            var observacionesTarea=$('#observacionesTarea').val();
            var codigoUsuario=$('#codigoUsuario').val();
        
            var todoDia='SI';
            if(!allDay){
              todoDia='NO';
            }
            var tiempoEmpleado=$('#tiempoEmpleado').val();
            var codigoContrato=$('#codigoContrato').val();
            var gastos=$('#gastos').val();
            

            //Creación-renderización del evento
            var creacion=$.post("gestionEventos.php", {tarea: codigoTarea, fechaInicio:fechaI, fechaFin:fechaF, horaInicio: horaI, horaFin: horaF, todoDia: todoDia, estado: estadoTarea, prioridad: prioridad, observaciones: observacionesTarea, codigoUsuario: codigoUsuario, codigoCliente: codigoCliente,'tiempoEmpleado':tiempoEmpleado,'codigoContrato':codigoContrato,'gastos':gastos});

            creacion.done(function(datos){
              calendario.fullCalendar('renderEvent',
              {
                  id: datos,
                  title: nombreTarea,
                  start: start,
                  end: end,
                  allDay: allDay,
                  backgroundColor: colorFondo[prioridad],
                  borderColor: colorBorde[prioridad]
                },
                true
              );
              calendario.fullCalendar('rerenderEvents');//Re-renderización de los eventos (necesario para que coja los cambios).
            });
            //Fin creación-renderización
            
            

          });
      }
      calendario.fullCalendar('unselect');
    },

    eventResize: function( event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view ) {//Actualización del evento cuando se modifica tu tamaño (duración)
      //var fechaI = $.fullCalendar.formatDate(event.start, 'dd/MM/yyyy');
      var fechaI = moment(event.start).format('DD/MM/YYYY');
      var fechaF = moment(event.end).format('DD/MM/YYYY');
      var horaI = moment(event.start).format('HH:mm');
      var horaF = moment(event.end).format('HH:mm');
      
      if(compruebaHorariosTecnicosAgenda(fechaI,fechaF,horaI,horaF,event.allDay)){
        var todoDia='SI';
        if(!event.allDay){
          todoDia='NO';
        }

        accion = 'actualiza';
        var creacion=$.post("gestionEventos.php", {codigo: event.id, fechaI:fechaI, fechaF:fechaF, horaI: horaI, horaF: horaF, todoDia: todoDia, tipoAccion: accion});
        creacion.done(function(datos){
          calendario.fullCalendar('rerenderEvents');//Re-renderización de los eventos (necesario para que coja los cambios).
        });
      }
      else{
        revertFunc();//Vuelve el evento a su estado original
      }

    },
    eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view ) {//Actualización del evento cuando se mueve de hora
      var fechaI = moment(event.start).format('DD/MM/YYYY');
      var fechaF = moment(event.end).format('DD/MM/YYYY');
      var horaI = moment(event.start).format('HH:mm');
      var horaF = moment(event.end).format('HH:mm');
      
      if(compruebaHorariosTecnicosAgenda(fechaI,fechaF,horaI,horaF,event.allDay)){
        var todoDia='SI';
        if(!event.allDay){
          todoDia='NO';
        }
        accion = 'actualiza';
        var creacion=$.post("gestionEventos.php", {codigo: event.id, fechaI:fechaI, fechaF:fechaF, horaI: horaI, horaF: horaF, todoDia: todoDia, tipoAccion: accion});
        creacion.done(function(datos){
          calendario.fullCalendar('rerenderEvents');//Re-renderización de los eventos (necesario para que coja los cambios).
        });
      }
      else{
        revertFunc();//Vuelve el evento a su estado original
      }

    },

    firstDay:1,//Añadido por mi
    axisFormat: 'H:mm',
    minTime:7,
    maxTime:23,
    firstHour:7,
    slotMinutes:30,
    timeFormat: 'H:mm',//Fin añadido por mi*/
    events:[
      <?php
        generaEventosCalendarioTareas($colores);
      ?>]
  });

});

<?php
    $datos=generaDatosGraficoTareas();
  ?>
  var pieData = [
      {
          value: <?php echo $datos['pendientes']; ?>,
          color: "#b94a48",
          label: 'Pendiente'
      },
      {
          value: <?php echo $datos['realizadas']; ?>,
          color: "#428bca",
          label: 'Realizado'
      }
    ];

    var grafico = new Chart(document.getElementById("grafico").getContext("2d")).Pie(pieData);


$('#cancelar, #cierra').click(function(){
  cierraVentana();
});

$('#cajaOpciones a,#cajaOpciones button').mouseup(function(e){//Esta función oculta el modal de la caja de opciones cuando se pulsa alguno de sus botones. El evento es mousedown porque el enlace se envia antes de que se ejecute el click()
  $('#cajaOpciones').modal('hide');
});

    $('#controladorTareasPendientes').click(function(){
      if($('#tareasPendientesOculto').attr('oculto')=='SI'){
        $('#tareasPendientesOculto').show(300);
        $('#tareasPendientesOculto').attr('oculto','NO');
      }else{
        $('#tareasPendientesOculto').hide(300);
        $('#tareasPendientesOculto').attr('oculto','SI');
      }
    });

    $('#controladorTareasRealizadas').click(function(){
      if($('#tareasRealizadasOculto').attr('oculto')=='SI'){
        $('#tareasRealizadasOculto').show(300);
        $('#tareasRealizadasOculto').attr('oculto','NO');
      }else{
        $('#tareasRealizadasOculto').hide(300);
        $('#tareasRealizadasOculto').attr('oculto','SI');
      }
    });    

  //$('#calendar').load(location.href);

function abreVentana(){
  $('#cajaTarea').find('.selectpicker').selectpicker('refresh');
  $('#cajaTarea').modal({'show':true,'backdrop':'static','keyboard':false});
}

function cierraVentana(){
  $('#tarea').val('');
  $('#observaciones').val('');

  $('#registrar').unbind();//Necesario para que no se acumulen clicks en el botón de envío al AJAX.
  $('#cajaTarea').modal('hide');
}


function abreVentanaOpciones(evento){
  $('#enlace').attr('href','gestion.php?codigo='+evento.id);
  $('#repetirCita').attr('onclick','repetirCita('+evento.id+')');
  $('#eliminarTarea').attr('onclick','eliminarTarea('+evento.id+')');
  $('#cajaOpciones').modal({'show':true,'backdrop':'static','keyboard':false});
}

function repetirCita(codigoEvento){
  var valoresChecks=[];

  valoresChecks['codigo']=codigoEvento;
  valoresChecks['repetir']='SI';
  creaFormulario('index.php',valoresChecks,'post');
}

function eliminarTarea(codigoEvento){
  var valoresChecks=[];

  valoresChecks['codigo0']=codigoEvento;
  valoresChecks['elimina']='SI';
  creaFormulario('index.php',valoresChecks,'post');
}

$('#email').click(function(){
    var valoresChecks=recorreChecks();
    creaFormulario('../enviarEmail.php?seccion=12',valoresChecks,'post');
});

$('#codigoCliente').change(function(){
    oyenteCliente($(this).val());
});


function oyenteCliente(codigoCliente){
    var consulta=$.post('../listadoAjax.php?include=tareas&funcion=obtieneContratosCliente();',{'codigoCliente':codigoCliente});
    consulta.done(function(respuesta){
        $('#codigoContrato').html(respuesta).selectpicker('refresh');
    });
}

</script><!-- /Calendar -->
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/eventoGrafico.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>