<?php
  $seccionActiva=5;
  include_once('../cabecera.php');
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <form action='generaListado.php' method="post" class='noAjax'>
      <div class="span12">		    
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
                <h3>Formaciones registradas</h3>
                <div class="pull-right">
                    <button type="submit" class="btn btn-success btn-small"><i class="icon-file-excel-o"></i> Descargar en Excel</button>
                    <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
                </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
                <?php
                    filtroFormados();
                    campoOculto('','global');
                    campoOculto('','columnaOrdenada');
                    campoOculto('','sentidoOrden');
                ?>
            <table class="table table-striped table-bordered datatable" id="tablaFormados">
              <thead>
                <tr>
                  <th> Alumno </th>
                  <th> Empresa</th>
                  <th> Curso </th>
                  <th> Fecha </th>
                </tr>
              </thead>
              <tbody>              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		    </form>


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    listadoTabla('#tablaFormados','../listadoAjax.php?include=formados-online&funcion=listadoFormados();');
    
    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaFormados');
    $('#botonFiltro').trigger('click');
    realizaBusquedaFiltrada('#cajaFiltros','#tablaFormados');


    $('input[type=search]').change(function(){
      $('#global').val($(this).val());
    });
  });
</script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>