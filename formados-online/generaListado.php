<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('../../api/phpexcel/PHPExcel.php');
	require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("../documentos/plantillaFormados.xlsx");

	$objPHPExcel->setActiveSheetIndex(0);//Selección hoja
    $query=construyeConsultaExcelFormados();
	conexionBDFormacion();
	$listado=consultaBD($query);
	$i=2;
	while($item=mysql_fetch_assoc($listado)){
		$objPHPExcel->getActiveSheet()->getCell('A'.$i)->setValue($item['alumno']);
		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($item['empresa']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($item['curso']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue(formateaFechaWeb($item['fechaInicio']));
		$i++;
	}
	cierraBD();

	foreach(range('A','D') as $columnID) {
     	$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
   	}

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('../documentos/Alumnos_formados_onlne.xlsx');


	// Definir headers
	header("Content-Type: application/ms-xlsx");
	header("Content-Disposition: attachment; filename=Alumnos_formados_onlne.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('../documentos/Alumnos_formados_onlne.xlsx');

function construyeConsultaExcelFormados(){

	$columnas=array(
		'alumno',
		'empresa',
		'codigoCurso',
		'anio',
	);
	$having='HAVING 1=1';

	$having=obtieneWhereListadoExcel($having,$columnas,false);
	$having.= " AND MONTH(cursos_empleados.fechaInicio)=".$_GET['sSearch_4'];
	$orden=obtieneOrdenListadoExcel($columnas);
	
	return "SELECT cursos_empleados.codigo, CONCAT(usuarios.apellidos,', ',usuarios.nombre) AS alumno, empresas.nombre AS empresa, 
			cursos.nombre AS curso, cursos_empleados.fechaInicio, MONTH(cursos_empleados.fechaInicio) as mes, 
			YEAR(cursos_empleados.fechaInicio) as anio, cursos.codigo AS codigoCurso
			FROM cursos_empleados LEFT JOIN puestos_empleados ON cursos_empleados.codigoEmpleado=puestos_empleados.codigo
			LEFT JOIN usuarios ON puestos_empleados.codigoEmpleado=usuarios.codigo
			LEFT JOIN empresas ON puestos_empleados.codigoEmpresa=empresas.codigo
			LEFT JOIN cursos_puestos ON cursos_empleados.codigoCurso=cursos_puestos.codigo
			LEFT JOIN cursos ON cursos_puestos.codigoCurso=cursos.codigo
		    $having
			
			$orden";
}