<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de satisfacción de clientes

function operacionesCuestionario(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('satisfaccion');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('satisfaccion');
	}

	mensajeResultado('pregunta1',$res,'Cuestionario');
    mensajeResultado('elimina',$res,'Cuestionario', true);
}

function imprimeCuestionariosSatisfaccion(){
	$where=defineWhereEjercicio('WHERE 1=1','fecha');
	$consulta=consultaBD("SELECT * FROM satisfaccion ".$where,true);
	while($datos=mysql_fetch_assoc($consulta)){
		$valoracion=obtieneValoracionMediaCuestionario($datos);

		echo "<tr>
				<td> ".$datos['cliente']." </td>
				<td> ".formateaFechaWeb($datos['fecha'])." </td>
				<td> $valoracion </td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>
			 </tr>";
	}
}

function obtieneValoracionMediaCuestionario($datos){
	$total=0;
	for($i=1;isset($datos['pregunta'.$i]);$i++){//Aunque a priori conocemos el número de preguntas del cuestionario, lo hago con isset para permitir modularidad con el número de preguntas.
		$total+=$datos['pregunta'.$i];
	}

	$total=round($total*100/30,2);//Obtengo el porcentaje de satisfacción. En este caso, la puntuación máxima de un cuestionario (100%) sería 30.
	$total=str_replace('.',',',$total);

	return $total.' %';
}

function creaDatosEstadisticasSatisfaccion(){
	$datos=array();

	conexionBD();

	$where=defineWhereEjercicio('WHERE 1=1','fecha');
	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM satisfaccion ".$where.";",false,true);
	$datos['total']=$consulta['total'];

	$consulta=consultaBD("SELECT AVG(pregunta1) AS pregunta1, AVG(pregunta2) AS pregunta2, AVG(pregunta3) AS pregunta3, AVG(pregunta4) AS pregunta4, AVG(pregunta5) AS pregunta5, AVG(pregunta6) AS pregunta6 FROM satisfaccion",false,true);
	$datos['valoracion']=obtieneValoracionMediaCuestionario($consulta);

	cierraBD();

	return $datos;
}

function obtieneURLCuestionario(){
	global $_CONFIG;
	echo "<a href='".$_CONFIG['enlaceCuestionario']."' class='noAjax' target='_blank'>".$_CONFIG['enlaceCuestionario']."</a>";
}


function generaDatosGraficoCuestionariosSatisfaccion(){
	$datos=array(0,0,0,0,0,0);

	$consulta=consultaBD("SELECT * FROM satisfaccion;",true);	
	while($datosConsulta=mysql_fetch_assoc($consulta)){
		for($i=1;isset($datosConsulta['pregunta'.$i]);$i++){
			$datos=clasificaValorPreguntaCuestionario($datosConsulta['pregunta'.$i],$datos);
		}
	}

	return $datos;
}

function clasificaValorPreguntaCuestionario($valor,$datos){
	if(isset($datos[$valor])){
		$datos[$valor]++;
	}
	else{
		$datos[$valor]=1;	
	}

	return $datos;
}

function gestionCuestionario(){
	operacionesCuestionario();
	
	abreVentanaGestionConBotones('Gestión de Cuestionario de Satisfacción de Clientes','?');
	$datos=compruebaDatos('satisfaccion');

	campoDato('Cliente',$datos['cliente']);
    campoDato('Fecha',formateaFechaWeb($datos['fecha']));
    campoDato('Valoración global',obtieneValoracionMediaCuestionario($datos));

    textoCuestionario();

    creaTablaSatisfaccion(); 
	creaPreguntaTablaEncuesta("La atención comercial prestada:",1,$datos);
	creaPreguntaTablaEncuesta("La atención del Técnico:",2,$datos);
	creaPreguntaTablaEncuesta("Profesionalidad del Técnico:",3,$datos);
	creaPreguntaTablaEncuesta("Comportamiento del Técnico:",4,$datos);
	creaPreguntaTablaEncuesta("Información sobre plazos y cobros:",5,$datos);
	creaPreguntaTablaEncuesta("Los plazos de respuesta:",6,$datos);
	creaAreaTextoTablaEncuesta($datos);
	cierraTablaSatisfaccion();

	cierraVentanaGestion('index.php',false,false);
}




//Fin parte de satisfacción de clientes