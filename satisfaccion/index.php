<?php
  $seccionActiva=6;
  include_once('../cabecera.php');
    
  operacionesCuestionario();
  $estadisticas=creaDatosEstadisticasSatisfaccion();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Resultados de las encuestas de satisfacción realizadas:</h6>
                  <div id="big_stats" class="cf">
                  
                    <canvas id="graficoCircular" class="chart-holder" height="250" width="538"></canvas>
                  
                    <div class="leyenda" id="leyenda"></div>
                    
                    <!-- .stat --> 
                  </div>
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de satisfacción de clientes:</h6>
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-edit"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Cuestionarios registrados</div>
                    <div class="stat"> <i class="icon-star-half-o"></i> <span class="value"><?php echo $estadisticas['valoracion']?></span> <br>Valoración media</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
         
        </div>


        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-external-link"></i>
              <h3>Enlace a Cuestionario</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content cajaEnlace">
              <div class="widget big-stats-container">
                Proporcione el siguiente enlace a sus clientes para que accedan al cuestionario:<br>
                <?php obtieneURLCuestionario(); ?>
              </div>
            </div>
          </div>
         
        </div>


      <div class="span12">        
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Cuestionarios de Satisfacción registrados</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Cliente </th>
                  <th> Fecha </th>
                  <th> Valoración </th>
                  <th></th>
                </tr>
              </thead>
              <tbody>

                <?php
                  imprimeCuestionariosSatisfaccion();
                ?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content --> 
        </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/chart.js" type="text/javascript"></script>

<script type="text/javascript">
    <?php
      $datos=generaDatosGraficoCuestionariosSatisfaccion();
    ?>
    var datosGrafico=[
        {
            value: <?php echo $datos[1]; ?>,
            color: "#B02B2C",
            highlight:"#C13C3D",
            label: 'Muy mal'
        },
        {
            value: <?php echo $datos[2]; ?>,
            color: "#f89406",
            highlight: "#f9A517",
            label: 'Mal'
        },
        {
            value: <?php echo $datos[3]; ?>,
            color: "#ACACAC",
            highlight: "#BDBDBD",
            label: 'Regular'
        },
        {
            value: <?php echo $datos[4]; ?>,
            color: "#6BBA70",
            highlight: "#7CCB81",
            label: 'Bien'
        },
		    {
            value: <?php echo $datos[5]; ?>,
            color: "#428bca",
            highlight: "#539CDB",
            label: 'Excelente'
        }
      ];

    var grafico=new Chart(document.getElementById("graficoCircular").getContext("2d")).Pie(datosGrafico);    
</script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/eventoGrafico.js"></script>


<!-- /contenido --></div>

<?php include_once('../pie.php'); ?>