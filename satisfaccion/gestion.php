<?php
  $seccionActiva=6;
  include_once("../cabecera.php");
  gestionCuestionario();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-rating-input.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $('input,textarea').attr('readonly',true);
  });
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>