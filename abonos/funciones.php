<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de abonos

function creaEstadisticasAbonos($anio=''){
	if($anio==''){
		$whereEjercicio=obtieneWhereEjercicioEstadisticas('fecha');
		$res=estadisticasGenericas('facturas','','tipoFactura="ABONO" '.$whereEjercicio." AND eliminado='NO'");
	}
	else{
		$res=estadisticasGenericas('facturas',false,"tipoFactura='ABONO' AND YEAR(fecha)='$anio' AND eliminado='NO'");
	}
	
	return $res;
}

function operacionesAbonos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaFactura();
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=creaFactura();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('facturas');
	}

	mensajeResultado('codigoCliente',$res,'Factura');
    mensajeResultado('elimina',$res,'Factura', true);
}

function creaFactura(){
	formateaImportesFacturas();
	$res=insertaDatos('facturas');
	
	if($res){
		$res=insertaCamposAdicionalesFactura($res);
	}

	return $res;
}

function actualizaFactura(){
	formateaImportesFacturas();
	$res=actualizaDatos('facturas');
	
	if($res){
		$res=insertaCamposAdicionalesFactura($_POST['codigo'],true);
	}

	return $res;
}

function insertaCamposAdicionalesFactura($codigoFactura,$actualizacion=false){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	if($actualizacion){
		$res=$res && consultaBD("DELETE FROM vencimientos_facturas WHERE codigoFactura=$codigoFactura");
	}

	$res=$res && insertaVencimientosFactura($datos,$codigoFactura);

	cierraBD();

	return $res;
}


function imprimeAbonos($anio=''){
	$whereAnio=obtieneWhereEjercicioEstadisticas('facturas.fecha');
	if($anio!=''){
		$whereAnio="AND facturas.fecha LIKE '$anio-%'";
	}

	
	$consulta=consultaBD("SELECT facturas.codigo, facturas.fecha, serie, facturas.numero, clientes.EMPNOMBRE AS cliente, clientes.EMPCIF AS cif, facturas.baseImponible,
						  facturas.total, facturas.activo, clientes.codigo AS codigoCliente
						  FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
						  LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
						  WHERE tipoFactura='ABONO' $whereAnio;",true);

	while($datos=mysql_fetch_assoc($consulta)){
		echo "<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['serie']."</td>
				<td>".$datos['numero']."</td>
				<td><a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."' class='noAjax' target='_blank'>".$datos['cliente']."</a></td>
				<td>".$datos['cif']."</td>
				<td class='nowrap pagination-right'>".formateaNumeroWeb($datos['baseImponible'])." €</td>
				<td class='nowrap pagination-right'>".formateaNumeroWeb($datos['total'])." €</td>
        		".botonAcciones(array('Detalles','Descargar abono'),array('abonos/gestion.php?codigo='.$datos['codigo'],'abonos/generaAbono.php?codigo='.$datos['codigo']),array("icon-search-plus",'icon-cloud-download'),array(0,1),true,'')."
        		<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
	        </tr>";
	}
}

function gestionAbono(){
	if((isset($_REQUEST['codigoFactura']) && $_REQUEST['codigoFactura']!='NULL') || isset($_GET['codigo'])){
		formularioDetallesAbono();
	}
	else{
		formularioSeleccionFactura();
	}
}

function formularioSeleccionFactura(){
	abreVentanaGestionConBotones('Gestión de Abonos','?','','icon-edit','margenAb',false,'','index.php',true,'Continuar','icon-chevron-right');

	echo "<p>Por favor, seleccione a continuación la factura para la que desea emitir el abono:</p><br />";
	campoSelectConsulta('codigoFactura','Factura',"SELECT facturas.codigo, CONCAT(serie,'-',numero,' del ',DATE_FORMAT(facturas.fecha,'%d/%m/%Y'),' para el cliente ',EMPNOMBRE) AS texto FROM facturas LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo WHERE facturas.activo='SI' AND tipoFactura!='ABONO' AND tipoFactura!='PROFORMA' ORDER BY facturas.fecha DESC;",false,'selectpicker span8 show-tick');

	cierraVentanaGestion('index.php',false,true,'Continuar','icon-chevron-right');
}

function formularioDetallesAbono(){
	operacionesAbonos();

	abreVentanaGestionConBotones('Gestión de Abonos','index.php','span3');
	$datos=obtieneDatosAbono();

	campoSelectConsulta('codigoCliente','Cliente',"SELECT codigo, EMPNOMBRE AS texto FROM clientes ORDER BY EMPNOMBRE",$datos);
	campoTexto('fecha','Fecha',formateaFechaWeb($datos['fecha']),'input-small datepicker hasDatepicker obligatorio');
	
	campoFacturaAsociada($datos);

	campoTextoSimbolo('baseImponible','Base imponible','€',formateaNumeroWeb($datos['baseImponible']));
	campoTextoSimbolo('total','Total','€',formateaNumeroWeb($datos['total']),'input-mini pagination-right obligatorio');
	areaTexto('observaciones','Observaciones',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectConsulta('codigoSerieFactura','Serie',"SELECT codigo, serie AS texto FROM series_facturas WHERE activo='SI' ORDER BY serie",$datos,'span1 selectpicker show-tick');
	campoTexto('numero','Número',$datos,'input-mini pagination-right obligatorio',true);
	/*campoSelectConsulta('codigoEmisor','Emisor','SELECT codigo, razonSocial AS texto FROM emisores WHERE activo="SI" ORDER BY razonSocial;',$datos,'selectpicker show-tick span3 obligatorio');*/

	areaTexto('comentariosLlamada','Comentarios de llamada');
	campoOculto($datos,'activo','SI');
	
	campoOculto($datos,'facturaCobrada','NO');
	campoOculto($datos,'informeEnviado','NO');
	campoOculto($datos,'eliminado','NO');
	campoOculto($datos,'codigoUsuarioFactura',$_SESSION['codigoU']);
	campoOculto('','conceptoManualFactura');

	cierraColumnaCampos();
	abreColumnaCampos('span11');

	creaTablaVencimientos($datos);

	cierraVentanaGestion('index.php',true);
}

function obtieneDatosAbono(){
	if(isset($_REQUEST['codigoFactura'])){
		$res=datosRegistro('facturas',$_REQUEST['codigoFactura']);
		$res['codigo']=false;//Importante! Es una creación de abono!
		$res['codigoSerieFactura']=false;
		$res['numero']=false;
		$res['fecha']=false;
		$res['creacion']=true;//Índice personalizado para indicar que el formulario está en la creación de un nuevo abono
		$res['baseImponible']*=-1;
		$res['total']*=-1;
		$res['codigoFacturaAsociada']=$_REQUEST['codigoFactura'];
	}
	else{
		$res=compruebaDatos('facturas');	
		$res['creacion']=false;
	}

	return $res;
}


function campoFacturaAsociada($datos){
	$codigoFactura=$datos['codigoFacturaAsociada'];

	campoSelectConsulta('codigoFacturaAsociada','Factura asociada',"SELECT facturas.codigo, CONCAT(serie,'-',numero,' del ',DATE_FORMAT(facturas.fecha,'%d/%m/%Y'),' para el cliente ',EMPNOMBRE) AS texto FROM facturas LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo WHERE facturas.activo='SI' AND tipoFactura!='ABONO' ORDER BY facturas.fecha DESC;",$codigoFactura,'selectpicker span3 show-tick','',true);
	campoOculto($codigoFactura,'codigoFacturaAsociada');
	campoOculto('ABONO','tipoFactura');
}

function campoCuentaBancariaCliente($datos){
	echo "<div id='cajaCuenta'>";

	if(!$datos){
		campoSelect('codigoCuentaCliente','Cuenta cliente',array(),array());
	}
	else{
		campoSelectConsulta('codigoCuentaCliente','Cuenta cliente',"SELECT codigo, ccc AS texto FROM cuentas_cliente WHERE codigoCliente=".$datos['codigoCliente']." ORDER BY ccc",$datos);
	}

	echo "</div>";
}

function campoTipoFactura($datos){
	if(!$datos){
		campoSelect('tipoFactura','Tipo',array('','Formación','Servicios','Otros conceptos'),array('','FORMACION','SERVICIOS','OTROS'),$datos,'selectpicker span2 show-tick obligatorio','');
	}
	else{
		$tipos=array('FORMACION'=>'Formación','SERVICIOS'=>'Servicios','OTROS'=>'Otros conceptos');
		campoDato('Tipo',$tipos[$datos['tipoFactura']]);
		campoOculto($datos,'tipoFactura');
	}
}



/*
	Reglas para obtener el vencimiento:
	+ A Bonificación: Ej: Si un curso finaliza el mes de Mayo, independientemente del día, tendría que aparecer la fecha 01 de Julio. 
	+ Quincenas: Si la factura tiene fecha del 01 al 15 el vencimiento es el 24 del mismo mes. 
	             Si la factura tiene fecha del 16 al 31, el vencimiento es el 09 del mes siguiente.
	+ Día 1 pasados 2 meses: Si la factura tiene fecha 28/06 sería el 1 de Septiembre.
*/
function obtieneVencimientoConcepto($tipoVencimiento,$fecha){
	$arrayFecha=explode('-',$fecha);

	if($tipoVencimiento=='A BONIFICACIÓN'){
		$arrayFecha=sumaMesFecha($arrayFecha,2);
		$arrayFecha[2]='01';
	}
	elseif($tipoVencimiento=='QUINCENAS' && $arrayFecha[2]<16){
		$arrayFecha[2]='24';
	}
	elseif($tipoVencimiento=='QUINCENAS' && $arrayFecha[2]>15){
		$arrayFecha=sumaMesFecha($arrayFecha,1);
		$arrayFecha[2]='09';
	}
	else{
		$arrayFecha=sumaMesFecha($arrayFecha,3);
		$arrayFecha[2]='01';
	}

	$res=$arrayFecha[2].'/'.$arrayFecha[1].'/'.$arrayFecha[0];

	return $res;
}

function sumaMesFecha($arrayFecha,$suma){
	$arrayFecha[1]+=$suma;

	if($arrayFecha[1]>12){
		$arrayFecha[1]-=12;
		$arrayFecha[0]+=1;
	}

	if($arrayFecha[1]<10){
		$arrayFecha[1]='0'.$arrayFecha[1];
	}

	return $arrayFecha;
}


function creaTablaVencimientos($datos){
	echo "<br />
	<div class='control-group'>                     
		<label class='control-label'>Vencimientos:</label>
	    <div class='controls'>
	    </div>
	</div>
	<div class='centro'>
		<div class='table-responsive'>
			<table class='table table-striped tabla-simple' id='tablaVencimientos'>
			  	<thead>
			    	<tr>
			            <th> Medio pago </th>
						<th> F. Vencimiento </th>
						<th> Importe </th>
						<th> Estado </th>
						<th> Concepto </th>
						<th> F. Devolución </th>
						<th> Gastos </th>
						<th> </th>
			    	</tr>
			  	</thead>
			  	<tbody>";

			  		$i=0;

			  		if(isset($datos['codigo']) && $datos['codigo']!=false){
			  			$codigo=$datos['codigo'];
			  		}
			  		else{
			  			$codigo=$datos['codigoFacturaAsociada'];
			  		}

			  		conexionBD();

		  			$consulta=consultaBD("SELECT * FROM vencimientos_facturas WHERE codigoFactura=".$codigo);
		  			while($datosVencimiento=mysql_fetch_assoc($consulta)){
		  				imprimeLineaTablaVencimientos($datosVencimiento,$i,$datos);

		  				if(!$datos['creacion']){//Si no se está en la creación, se mete el código del vencimiento (si se está y mete el código actualiza el vencimiento de la factura).
		  					campoOculto($datosVencimiento['codigo'],'codigoVencimiento'.$i);
		  				}

		  				$i++;
		  			}
			  		

			  		if($i==0){
			  			imprimeLineaTablaVencimientos(false,0,false);
			  		}
			  		cierraBD();

	echo "		</tbody>
			</table>
			<div class='centro'>
				<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaVencimientos\");'><i class='icon-plus'></i> Añadir vencimiento</button> 
				<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaVencimientos\");'><i class='icon-trash'></i> Eliminar vencimiento</button>
			</div>
		</div>
	</div><br /><br />";
}

function imprimeLineaTablaVencimientos($datosVencimiento,$i,$datosFactura){
	$j=$i+1;

	if(!$datosFactura['creacion'] || ($datosFactura['creacion'] && $i==0)){//Si se está en los detalles o en la creación y es la primera fila, se imprime (en la creación solo hay 1 vencimiento)
		$importeVencimiento=formateaNumeroWeb($datosVencimiento['importe']);

		if($datosFactura['creacion']){
			$datosVencimiento['codigoFormaPago']=false;
			$datosVencimiento['estado']='PENDIENTE PAGO';
			$datosVencimiento['concepto']='ABONO DE FACTURA';
			$importeVencimiento=formateaNumeroWeb($datosFactura['total']);
			$datosVencimiento['fechaDevolucion']='0000-00-00';
			$datosVencimiento['gastosDevolucion']=false;
		}

		echo "
		<tr>";
			campoFormaPago($datosVencimiento['codigoFormaPago'],'codigoFormaPago'.$i,1,false);
			campoFechaTabla('fechaVencimiento'.$i,$datosVencimiento['fechaVencimiento']);
			campoTextoSimbolo('importeVencimiento'.$i,'','€',$importeVencimiento,'input-mini pagination-right',1);
			campoSelectEstadoVencimiento($datosVencimiento,$i);
			campoSelectConceptoVencimiento($datosVencimiento,$i,$datosFactura);
			campoFechaTabla('fechaDevolucion'.$i,$datosVencimiento['fechaDevolucion']);
			campoTextoSimbolo('gastosDevolucion'.$i,'','€',formateaNumeroWeb($datosVencimiento['gastosDevolucion']),'input-mini pagination-right',1);
			campoOculto($datosVencimiento['llamadaAsesoria'],'llamadaAsesoria'.$i,'NO');
			campoOculto($datosVencimiento['llamadaEmpresa'],'llamadaEmpresa'.$i,'NO');
		echo "<td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td>
		</tr>";
	}
}

function campoSelectConceptoVencimiento($datos,$i,$datosFactura){
	campoTextoTabla('conceptoVencimiento'.$i,$datos['concepto']);

	//Comentado temporalmente, a espera de ver si prefieren el campo de texto
	/*if(!$datos){
		campoSelect('codigoConcepto'.$i,'',array(),array(),false,'selectpicker span3 show-tick','data-live-search="true"',1);
	}
	else{
		$query=obtieneConsultaConceptosVencimiento($datosFactura['codigo'],$datosFactura['tipo']);
		campoSelectConsulta('codigoConcepto'.$i,'',$query,$datos['codigoConcepto'],'selectpicker span3 show-tick','data-live-search="true"','',1);
	}*/
}


function obtieneConsultaConceptosVencimiento($codigoFactura,$tipo){
	$res='';

	if($tipo=='FORMACION'){
		$res="SELECT grupos.codigo, CONCAT(accionFormativa,' - ',accion,' - ',grupos.grupo) AS texto FROM acciones_formativas INNER JOIN grupos ON acciones_formativas.codigo=grupos.codigoAccionFormativa INNER JOIN grupos_en_facturas ON grupos.codigo=grupos_en_facturas.codigoGrupo WHERE codigoFactura=$codigoFactura ORDER BY accionFormativa;";
	}
	elseif($tipo=='SERVICIOS'){
		$res="SELECT codigo, DATE_FORMAT(ventas_servicios.fecha,'%d/%m/%Y') AS texto FROM ventas_servicios INNER JOIN ventas_en_facturas ON ventas_servicios.codigo=ventas_en_facturas.codigoVentaServicio WHERE codigoFactura=$codigoFactura ORDER BY fecha;";
	}
	elseif($tipo=='OTROS'){
		$res="SELECT codigo, concepto AS texto FROM conceptos_libres_facturas WHERE codigoFactura=$codigoFactura ORDER BY concepto;";
	}

	return $res;
}


function creaTablaCobros($datos){
	echo "	
	<div class='control-group'>                     
		<label class='control-label'>Cobros:</label>
	    <div class='controls'>
			<div class='table-responsive'>
				<table class='table table-striped tabla-simple' id='tablaCobros'>
				  	<thead>
				    	<tr>
				            <th> Medio pago </th>
							<th> F. Cobro </th>
							<th> Importe </th>
							<th> Banco </th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		conexionBD();
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM cobros_facturas WHERE codigoFactura=".$datos['codigo']);
				  			while($datosVencimiento=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaCobros($datosVencimiento,$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaCobros(false,0);
				  		}
				  		cierraBD();

	echo "			</tbody>
				</table>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaCobros\");'><i class='icon-plus'></i> Añadir cobro</button> 
					<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaCobros\");'><i class='icon-trash'></i> Eliminar cobro</button>
				</div>
			</div>
		</div>
	</div>";
}

function imprimeLineaTablaCobros($datos,$i){
	$j=$i+1;

	echo "
	<tr>";
	
		campoFormaPago($datos['codigoFormaPago'],'codigoFormaPagoCobro'.$i,1,false);
		campoFechaTabla('fechaCobro'.$i,$datos['fechaCobro']);
		campoTextoSimbolo('importeCobro'.$i,'','€',formateaNumeroWeb($datos['importe']),'input-mini pagination-right',1);
		campoSelectConsulta('codigoCuentaPropia'.$i,'',"SELECT codigo, nombre AS texto FROM cuentas_propias WHERE activo='SI' ORDER BY nombre;",$datos['codigoCuentaPropia'],'selectpicker span3 show-tick','data-live-search="true"','',1);
	
	echo "<td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td>
	</tr>";
}


function obtieneConceptosFormacion(){
	$codigoCliente=$_POST['codigoCliente'];
	$codigoEmisor=$_POST['codigoEmisor'];

	echo rellenaOptionsConcepto("SELECT grupos.codigo, CONCAT(accionFormativa,' - ',accion,' - ',grupos.grupo) AS texto FROM acciones_formativas INNER JOIN grupos ON acciones_formativas.codigo=grupos.codigoAccionFormativa INNER JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo WHERE grupos.activo='SI' AND codigoCliente=$codigoCliente AND codigoEmisor=$codigoEmisor AND grupos.codigo NOT IN(SELECT codigoGrupo FROM grupos_en_facturas WHERE codigoFactura IS NOT NULL) ORDER BY accionFormativa;");
}

function obtieneConceptosServicios(){
	$codigoCliente=$_POST['codigoCliente'];
	$codigoEmisor=$_POST['codigoEmisor'];

	echo rellenaOptionsConcepto("SELECT ventas_servicios.codigo, DATE_FORMAT(ventas_servicios.fecha,'%d/%m/%Y') AS texto FROM ventas_servicios LEFT JOIN conceptos_venta_servicios ON ventas_servicios.codigo=conceptos_venta_servicios.codigoVentaServicio LEFT JOIN servicios ON conceptos_venta_servicios.codigoServicio=servicios.codigo WHERE codigoCliente=$codigoCliente AND codigoEmisor=$codigoEmisor GROUP BY ventas_servicios.codigo ORDER BY fecha ASC;");
}

function rellenaOptionsConcepto($query){
	$res='<option value="NULL"></option>';

	$consulta=consultaBD($query,true);
	while($datos=mysql_fetch_assoc($consulta)){
		$res.="<option value='".$datos['codigo']."'>".$datos['texto']."</option>";
	}

	return $res;
}

function obtieneDatosCliente(){
	$codigoCliente=$_POST['codigoCliente'];
	$res=array();

	$res['asesoria']=obtieneAsesoriaCliente($codigoCliente);

	$res['cuentas']=rellenaOptionsConcepto("SELECT codigo, ccc AS texto FROM cuentas_cliente WHERE codigoCliente=$codigoCliente ORDER BY ccc");

	echo json_encode($res);
}

function obtieneAsesoriaCliente($codigoCliente){
	$asesoria=consultaBD("SELECT asesorias.codigo FROM asesorias LEFT JOIN clientes ON asesorias.codigo=clientes.codigoAsesoria WHERE clientes.codigo=$codigoCliente",true,true);
	
	return $asesoria['codigo'];
}

function obtieneIVAEmisor(){
	$codigoEmisor=$_POST['codigoEmisor'];

	$datos=consultaBD("SELECT tipoIva FROM emisores WHERE codigo=$codigoEmisor",true,true);
	echo $datos['tipoIva'];
}

function filtroAbonos(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoSelectConsulta(1,'Serie',"SELECT serie AS codigo, serie AS texto FROM series_facturas WHERE activo='SI' ORDER BY serie",false,'span1 selectpicker show-tick');
	campoTexto(2,'Número',false,'input-mini pagination-right');
	campoTexto(3,'Emisor');
	campoTexto(5,'CIF cliente',false,'input-small');
	campoTexto(4,'Nombre cliente');
	campoTextoSimbolo(6,'Base imponible','€');
	campoSelect(7,'Tipo IVA',array('','21%','0%'),array('','21','0'),'','selectpicker span1 show-tick','');
	campoTextoSimbolo(8,'Base imponible','€');
	campoFormaPago(false,10);
	campoSelectTieneFiltro(18,'Cuenta bancaria');
	campoSelectConsulta(22,'Telemarketing',"SELECT codigo, nombre AS texto FROM telemarketing WHERE activo='SI' ORDER BY nombre;");

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto(23,'Comentarios llamada');
	campoTexto(24,'Observaciones factura');
	campoFecha(0,'Fecha desde');
	campoFecha(15,'Hasta');
	campoSelectConsulta(14,'Centro formación',"SELECT codigo, razonSocial AS texto FROM centros_formacion WHERE activo='SI' ORDER BY razonSocial");
	campoSelectConsultaAjax(20,'Asesoría',"SELECT codigo, asesoria AS texto FROM asesorias WHERE activo='SI'");
	campoSelectComercial(false,false,16);
	campoSelectConsultaAjax(17,'Colaborador',"SELECT codigo, nombre AS texto FROM colaboradores WHERE activo='SI' ORDER BY nombre;");
	campoSelectConsulta(19,'Administrativo/a',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE activo='SI' AND (tipo='ADMINISTRACION1' OR tipo='ADMINISTRACION2') ORDER BY nombre");
	//campoSelectSiNoFiltro(19,'G. devolución pagados');
	campoSelectSiNoFiltro(9,'Anulado');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Parte de generación de documentos

function generaPDFAbono($codigoFactura){
	conexionBD();
	$nombreFichero=array();
	
	$datos=consultaBD("SELECT facturas.*, serie, clientes.EMPNOMBRE AS cliente, clientes.EMPCIF AS cifCliente, clientes.EMPDIR AS domicilioCliente, 
	clientes.EMPCP AS cpCliente, clientes.EMPLOC AS localidadCliente, clientes.EMPPROV AS provinciaCliente, facturas.numero, codigoFacturaAsociada
	FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
	LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
	WHERE facturas.codigo=$codigoFactura",false,true);

	$emisor=consultaBD("SELECT emisores.razonSocial AS emisor, emisores.cif AS cifEmisor, emisores.domicilio AS domicilioEmisor,
					    emisores.cp AS cpEmisor, emisores.localidad AS localidadEmisor, emisores.provincia AS provinciaEmisor,
					    emisores.telefono, emisores.fax, emisores.web, emisores.email, emisores.registro, emisores.ficheroLogo, emisores.iban, emisores.registro AS registroPie
					    FROM emisores 
					    WHERE codigo=1",false,true);


	$nombreFichero[0]=$datos['serie'].'-'.$datos['numero'].'-'.trim(limpiaCadena($datos['cliente']));
	$conceptos=obtieneConceptoAbono($datos['codigoFacturaAsociada']);

	$vencimientos='';//obtieneVencimientosFactura($datos['codigo']);

	cierraBD();

	$observaciones=formateaObservacionesFactura($datos['observaciones']);

	$logo='../documentos/emisores/'.trim($emisor['ficheroLogo']);
	if(!file_exists($logo)){//Para evitar que la factura no se genere por no encontrar la imagen
		$logo='../img/logoOferta.png';
	}

	$contenido = "
	<style type='text/css'>
	<!--
		.tablaEmisorCliente{
			margin-top:40px;
			font-size:12px;
			width:100%;
		}

		.celdaEmisor{
			width:50%;
			padding-right:20px;
		}

		.celdaCliente{
			border:1px solid #000;
			width:50%;
			padding:10px 30px 1px 30px;
			line-height:15px;
		}

		.cajaNumero{
			margin-top:20px;
			font-size:12px;
		}

		.cajaFecha{
			text-align:right;
			margin-right:240px;
		}

		.cabeceraConceptos, .tablaConceptos{
			margin-top:20px;
			border:1px solid #000;
			width:100%;
		}
		.celdaConcepto{
			width:85%;
			padding:10px;
		}
		.celdaImporte{
			width:15%;
			text-align:right;
			padding:10px;
		}

		.tablaConceptos{
			border-collapse:collapse;
		}

		.tablaConceptos td, .tablaConceptos th{
			font-size:12px;
			border-top:1px solid #000;
			border-bottom:1px solid #000;
			line-height:18px;
		}

		.tablaConceptos .celdaConcepto{
			border-left:1px solid #000;
		}

		.tablaConceptos .celdaImporte{
			border-right:1px solid #000;
		}

		.tablaTotales{
			width:40%;
			position:relative;
			left:408px;
		}

		.tablaTotales .celdaConcepto{
			width:60%;
		}
		.tablaTotales .celdaImporte{
			width:40%;
		}

		.cajaMedioPago{
			max-width:100%;
			font-size:12px;
			line-height:18px;
		}

		.cajaMedioPago ol{
			margin-top:-20px;
		}

		.pie{
			font-size:10px;
			text-align:center;
			line-height:18px;
		}

		.textoAdvertencia{
			font-size:11px;
			text-align:justify;
			font-weight:bold;
		}
		.centrado{
			text-align:center;
		}

		.cajaObservaciones{
			width:100%;
		}

		.logo{
			width:20%;
		}
	-->
	</style>
	<page footer='page'>
	    <img src='".$logo."' class='logo' />
	    <br />

	    <table class='tablaEmisorCliente'>
		    <tr>
		    	<td class='celdaEmisor'>
		    		".$emisor['emisor']."<br />
				    ".$emisor['domicilioEmisor']."<br />
				    ".$emisor['cpEmisor']." ".$emisor['localidadEmisor']." - ".$emisor['provinciaEmisor']."<br />
				    CIF: ".$emisor['cifEmisor']."
		    	</td>
		    	<td class='celdaCliente'>
		    		".$datos['cliente']."<br />
				    ".$datos['domicilioCliente']."<br />
				    ".$datos['cpCliente']." ".$datos['localidadCliente']."<br />
				    ".$datos['provinciaCliente']."<br />
				    CIF: ".$datos['cifCliente']."
		    	</td>
		    </tr>
	    </table>

	    <div class='cajaNumero'>
	    	Referencia abono: ".$datos['serie']."".$datos['numero']."
	    	<div class='cajaFecha'>
	    		Fecha: ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    </div>

	    <table class='cabeceraConceptos'>
		    <tr>
		    	<th class='celdaConcepto'>Concepto</th>
		    	<th class='celdaImporte'></th>
		    </tr>
	    </table>

	    <table class='tablaConceptos'>
		    ".$conceptos."		    
	    </table>

	    <table class='tablaConceptos tablaTotales'>
	    	<tr>
	    		<td class='celdaConcepto'>Base imponible:</td>
	    		<td class='celdaImporte'>".$datos['baseImponible']." €</td>
	    	</tr>
	    	<tr>
	    		<th class='celdaConcepto'>Total factura:</th>
	    		<th class='celdaImporte'>".$datos['total']." €</th>
	    	</tr>
	    </table>

	    <div class='cajaMedioPago'>
		    ".$vencimientos."
		</div>

		<br /><br /><br />
		Observaciones:<br />
		<div class='cajaObservaciones'>
			".$observaciones."
		</div>

	    <page_footer>
	    	<div class='pie'>
	    		Teléfono: ".formateaTelefono($emisor['telefono'])." &nbsp; 
	    		Fax: ".formateaTelefono($emisor['fax'])." &nbsp;
	    		Web: <a href='".$emisor['web']."'>".$emisor['web']."</a> &nbsp;
	    		eMail: <a href='mailto:".$emisor['email']."'>".$emisor['email']."</a><br />
	    		".$emisor['registro']." 
	    	</div>
	    </page_footer>
	</page>";


	return $contenido;
}

function obtieneMedioPagoFactura($medioPago,$codigoCuentaCliente,$cuentaEmisor){
	$res='Medio pago: '.$medioPago.'.';
	
	if($medioPago=='TRANSFERENCIA' || $medioPago=='RECIBO SEPA'){
		$datos=consultaBD("SELECT iban FROM cuentas_cliente WHERE codigo='$codigoCuentaCliente';",false,true);
		$res.='<br />Cuenta de origen: '.$datos['iban'];
		$res.='<br />Cuenta de destino: '.$cuentaEmisor;
	}

	return $res;
}

function obtieneConceptoAbono($codigoFacturaAsociada){
	$res=consultaBD("SELECT CONCAT(serie,'-',numero,' del ',DATE_FORMAT(facturas.fecha,'%d/%m/%Y'),' para el cliente ',EMPNOMBRE) AS texto, total FROM facturas LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo WHERE facturas.codigo='$codigoFacturaAsociada'",false,true);

	$res="<tr><td class='celdaConcepto'>Abono de la factura ".$res['texto'].".</td><td class='celdaImporte'></td></tr>";

	return $res;
}