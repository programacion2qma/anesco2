<?php
  $seccionActiva=45;
  include_once("../cabecera.php");
  gestionAbono();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
		$('.selectpicker').selectpicker();

		//Oyentes
		$('#codigoSerieFactura').change(function(){
			oyenteSerieFactura();
		});

		$('#codigoCliente').change(function(){
			oyenteCliente($(this).val());
		});

		$('#codigoEmisor').change(function(){
			oyenteIVAEmisor($(this).val());
		});

		$('#tipoFactura,#codigoCliente,#codigoEmisor').change(function(){
			oyenteTipo();
		});

		$('#tablaFormacion').find('select').change(function(){
			oyenteConceptoFormacion($(this));
		});

		$('#tablaServicios').find('select').change(function(){
			oyenteConceptoServicio($(this));
		});

		$('#tablaConceptos').find('.input-mini').change(function(){
			oyenteBaseImponible();
		});

		$('#iva,#baseImponible').change(function(){
			calculaTotal();
		});
		//Fin oyentes
	});

	//Parte general

	function oyenteSerieFactura(){
		var codigoSerieFactura=$('#codigoSerieFactura').val();
		if(codigoSerieFactura!='NULL'){
			var consulta=$.post('../listadoAjax.php?include=facturas&funcion=consultaNumeroSerieFactura();',{'codigoSerieFactura':codigoSerieFactura});
			consulta.done(function(respuesta){
				$('#numero').val(respuesta);
			});
		}
	}

	function oyenteTipo(){
		var tipo=$('#tipoFactura').val();
		if(tipo=='FORMACION'){
			$('#cajaFormacion').removeClass('hide');
			$('#cajaServicios').addClass('hide');
			$('#cajaConceptos').addClass('hide');

			obtieneConceptosFormacion();
		}
		else if(tipo=='SERVICIOS'){
			$('#cajaFormacion').addClass('hide');
			$('#cajaServicios').removeClass('hide');
			$('#cajaConceptos').addClass('hide');

			obtieneConceptosServicios();
		}
		else if(tipo=='OTROS'){
			$('#cajaFormacion').addClass('hide');
			$('#cajaServicios').addClass('hide');
			$('#cajaConceptos').removeClass('hide');
		}
		else{
			$('#cajaFormacion').addClass('hide');
			$('#cajaServicios').addClass('hide');
			$('#cajaConceptos').addClass('hide');
		}
	}

	function oyenteCliente(codigoCliente){
		var consulta=$.post('../listadoAjax.php?include=facturas&funcion=obtieneDatosCliente();',{'codigoCliente':codigoCliente},
		function(respuesta){
			$('#codigoCuentaCliente').html(respuesta.cuentas).selectpicker('refresh');
			$('#codigoAsesoria').selectpicker('val',respuesta.asesoria);
		},'json');
	}

	//Parte de formación

	function obtieneConceptosFormacion(){
		var codigoCliente=$('#codigoCliente').val();
		var codigoEmisor=$('#codigoEmisor').val();
		var consulta=$.post('../listadoAjax.php?include=facturas&funcion=obtieneConceptosFormacion();',{'codigoCliente':codigoCliente,'codigoEmisor':codigoEmisor});
		
		consulta.done(function(respuesta){
			$('#tablaFormacion').find('select').html(respuesta).selectpicker('refresh');
		});
	}

	function oyenteConceptoFormacion(campo){
		var codigoGrupo=campo.val();
		var codigoCliente=$('#codigoCliente').val();
		var celda=campo.parent();

		if(codigoGrupo!='NULL' && codigoCliente!='NULL'){
			var consulta=$.post('../listadoAjax.php?include=facturas&funcion=obtieneDatosGrupo('+codigoGrupo+','+codigoCliente+',true);',{},
			function(respuesta){
				celda.next().text(respuesta.fechaInicio);
				celda.next().next().text(respuesta.fechaFin);
				celda.next().next().next().text(respuesta.horas);
				celda.next().next().next().next().html(respuesta.datosAccion);
				celda.next().next().next().next().next().html(respuesta.alumnos);
				celda.next().next().next().next().next().next().text(respuesta.importe);

				compruebaVencimiento(respuesta.importe,respuesta.vencimiento);
				actualizaColaboradores(respuesta);
				oyenteBaseImponible();
			},'json');
		}
	}

	function insertaFilaFormacion(){
		insertaFila('tablaFormacion');
		$('#tablaFormacion tr:last').find('select').each(function(){
			var campo=$(this);
			campo.change(function(){
				oyenteConceptoFormacion($(this));
			});

			var celda=campo.parent();
			celda.next().text('');
			celda.next().next().text('');
			celda.next().next().next().text('');
			celda.next().next().next().next().text('');
			celda.next().next().next().next().next().text('');
			celda.next().next().next().next().next().next().text('');
		});
	}

	//Parte de servicios

	function obtieneConceptosServicios(){
		var codigoCliente=$('#codigoCliente').val();
		var codigoEmisor=$('#codigoEmisor').val();
		var consulta=$.post('../listadoAjax.php?include=facturas&funcion=obtieneConceptosServicios();',{'codigoCliente':codigoCliente,'codigoEmisor':codigoEmisor});
		
		consulta.done(function(respuesta){
			$('#tablaServicios').find('select').html(respuesta).selectpicker('refresh');
		});
	}

	function oyenteConceptoServicio(campo){
		var codigoVenta=campo.val();
		var celda=campo.parent();

		if(codigoVenta!='NULL'){
			var consulta=$.post('../listadoAjax.php?include=facturas&funcion=obtieneDatosVenta('+codigoVenta+',true);',{},
			function(respuesta){
				celda.next().text(respuesta.servicios);
				celda.next().next().text(formateaNumeroWeb(respuesta.total));

				actualizaColaboradores(respuesta);
				oyenteBaseImponible();
				compruebaVencimiento(respuesta.total,respuesta.vencimiento);
			},'json');
		}
	}

	function insertaFilaServicios(){
		insertaFila("tablaServicios");
		$('#tablaServicios tr:last').find('select').each(function(){
			var campo=$(this);
			campo.change(function(){
				oyenteConceptoServicio($(this));
			});

			var celda=campo.parent();
			celda.next().text('');
			celda.next().next().text('');
		});
	}


	//Parte de conceptos

	function insertaFilaConceptos(){
		insertaFila("tablaConceptos");
		$('#tablaConceptos tr:last').find('.input-mini').change(function(){
			oyenteBaseImponible();
		});
	}

	//Parte de cálculos

	function eliminaFilaFacturacion(id){
		eliminaFila(id);
		oyenteBaseImponible();
	}

	function oyenteBaseImponible(){
		var tipo=$('#tipoFactura').val();
		var base=0;

		if(tipo=='FORMACION'){
			$('#tablaFormacion tbody').find('td:nth-last-child(2)').each(function(){
				var importe=formateaNumeroCalculo($(this).text());
				base+=importe;
			});
		}
		else if(tipo=='SERVICIOS'){
			$('#tablaServicios tbody').find('td:nth-last-child(2)').each(function(){
				var importe=formateaNumeroCalculo($(this).text());
				base+=importe;
			});
		}
		else if(tipo=='OTROS'){
			$('#tablaConceptos').find('.input-mini').each(function(){
				var importe=formateaNumeroCalculo($(this).val());
				base+=importe;
			});
		}

		$('#baseImponible').val(formateaNumeroWeb(base));
		calculaTotal();
	}

	function oyenteIVAEmisor(codigoEmisor){
		var consulta=$.post('../listadoAjax.php?include=facturas&funcion=obtieneIVAEmisor();',{'codigoEmisor':codigoEmisor});
		
		consulta.done(function(respuesta){
			$('#iva').val(respuesta);
		});

		calculaTotal();
	}

	function calculaTotal(){
		var baseImponible=formateaNumeroCalculo($('#baseImponible').val());
		var iva=formateaNumeroCalculo($('#iva').val());
		var total=baseImponible+(baseImponible*iva/100);

		$('#total').val(formateaNumeroWeb(total));

		//Para los casos de conceptos libres, para que se cree un vencimiento con el importe total siempre:
		if($('#importeVencimiento0').val().trim()=='' || $('#importeVencimiento0').val()=='0,00'){
			$('#importeVencimiento0').val(formateaNumeroWeb(total));
		}
	}

	function actualizaColaboradores(respuestaAjax){
		$('#codigoComercial').val(respuestaAjax.codigoComercial);
		$('#codigoColaborador').val(respuestaAjax.codigoColaborador);
		$('#codigoTelemarketing').val(respuestaAjax.codigoTelemarketing);

		$('#codigoComercial').selectpicker('refresh');
		$('#codigoColaborador').selectpicker('refresh');
		$('#codigoTelemarketing').selectpicker('refresh');
	}

	function compruebaVencimiento(importe,fechaVencimiento){
		var iva=$('#iva').val();
		importe=parseFloat(importe)+parseFloat(importe*iva/100);

		if($('#importeVencimiento0').val()=='' || $('#fechaVencimiento0').val()==''){//Aún no se han insertado vencimientos...
			$('#fechaVencimiento0').val(fechaVencimiento);
			$('#importeVencimiento0').val(formateaNumeroWeb(importe));
			$('#fechaDevolucion0').val('');//En los casos en los que se rellene el vencimiento de una factura creada desde el listado de pendientes, el campo Fecha Devolución aparecerá relleno con la fecha actual, lo cual no es correcto
		}
		else{
			var flag=true;

			$('#tablaVencimientos').find('.datepicker').each(function(){
				if($(this).val()==fechaVencimiento){//Ya hay otro vencimiento con la misma fecha, por tanto se suma los importes
					var fila=obtieneFilaCampo($(this));
					var importeAnterior=formateaNumeroCalculo($('#importeVencimiento'+fila).val());
					var importeVencimiento=parseFloat(importe)+parseFloat(importeAnterior);

					$('#importeVencimiento'+fila).val(formateaNumeroWeb(importeVencimiento));
					flag=false;//Para que no entre en la creación de una nueva fila
				}
			});

			if(flag){//Si flag es true, es porque no hay ningún vencimiento registrado con la misma fecha, y por tanto hay que crearlo
				insertaFila("tablaVencimientos");
				var campoFecha=$('#tablaVencimientos tr:last .datepicker');
				var fila=obtieneFilaCampo(campoFecha);

				$('#importeVencimiento'+fila).val(formateaNumeroWeb(importe));
				$('#fechaVencimiento'+fila).val(fechaVencimiento);
			}
		}
	}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>