<?php
include_once('funciones.php');

$res = true;
$i = 0;

conexionBD();

$consulta = consultaBD("SELECT * FROM contratos WHERE incrementoIPC='SI' AND eliminado='NO' ORDER BY fechaInicio");

while($contrato=mysql_fetch_assoc($consulta)){
	
	$codigo		=	$contrato['codigo'];
	$cambios	=	array();

	calculaIncrementoIPC( array('incrementoIPC'=>'SI'), $contrato, $cambios, true);

	if( isset($cambios['incrementoIpcAplicado']) ){
		$incrementoIpcAplicado = $cambios['incrementoIpcAplicado'];

		echo "Contrato: ".$codigo." - Incremento IPC original: ".$contrato['incrementoIpcAplicado']." - Incremento IPC actualizado: ".$incrementoIpcAplicado."<br />";
		$res = $res && consultaBD("UPDATE contratos SET incrementoIpcAplicado='$incrementoIpcAplicado' WHERE codigo='$codigo'");
		$i++;
	}
}

cierraBD();

if($res){
	echo "<h1>Actualizacion de IPC realizada con éxito :D. Contratos actualizados $i</h1>";
}
else{
	echo "Error al actualizar IPC :(";
	echo mysql_error();
}