<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	require_once('../../api/phpexcel/PHPExcel.php');
	require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');

	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("../documentos/plantillaContratosAntiguo.xlsx");
	

	listadoContratosAntiguos($objPHPExcel);//Llamada a la función que rellena el Excel

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('../documentos/Listado_Contratos.xlsx');


	header("Content-Type: application/ms-xlsx");
	header("Content-Disposition: attachment; filename=Listado_Contratos.xlsx");
	header("Content-Transfer-Encoding: binary");

	readfile('../documentos/Listado_Contratos.xlsx');

function listadoContratosAntiguos($objPHPExcel){
	$listado=consultaBD('SELECT DISTINCT contratos.codigo, clientes.EMPNOMBRE, clientes.EMPCIF, clientes.EMPDIR, clientes.EMPLOC, ofertas.opcion, clientes.EMPNTRAB, contratos.fechaInicio, clientes.EMPCP, clientes.EMPID, contratos.codigoInterno
  	FROM contratos 
  	LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
  	LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo
  	LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
	LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!="ABONO"
  	WHERE contratos.enVigor="SI" AND contratos.eliminado="NO" AND facturaCobrada="SI";',true);
  	$servicios=array(1=>'SPA 4 Especialidades',2=>'SPA Vigilancia de la salud',3=>'SPA Especialidades técnicas',4=>'Otras actuaciones',''=>'-');
  	$i=4;
  	while($item=mysql_fetch_assoc($listado)){
  		$referencia=formateaReferenciaContrato($item,$item);
  		/*echo $item['codigo'].'- '.$referencia.' - '.$item['EMPNOMBRE'].' - '.$item['EMPCIF'].' - '.$item['EMPDIR'].' - '.$item['EMPLOC'].' - '.$servicios[$item['opcion']].' - '.$item['EMPNTRAB'].'<br/>';*/
		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($referencia);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($item['EMPNOMBRE']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($item['EMPCIF']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue($item['EMPDIR']);
		$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue($item['EMPLOC']);
		$objPHPExcel->getActiveSheet()->getCell('G'.$i)->setValue($servicios[$item['opcion']]);		
		$objPHPExcel->getActiveSheet()->getCell('H'.$i)->setValue($item['EMPNTRAB']);
		$i++;
	}
	foreach(range('B','H') as $columnID) {
    	$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
	}
}
?>