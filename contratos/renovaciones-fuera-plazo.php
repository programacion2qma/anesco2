<?php
  $seccionActiva=1;
  include_once('../cabecera.php');

  //$estadisticas=generaEstadisticasRenovaciones();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de renovaciones fuera de plazo</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href='renovaciones.php' class='shortcut'><i class='shortcut-icon icon-chevron-left'></i><span class='shortcut-label'>Volver</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Contratos a renovar fuera de plazo</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Nº Cliente </th>
                  <th> Nº Contrato </th>
                  <th> Inicio </th>
                  <th> Fin </th>
                  <th> Razón Social </th>
                  <th> Nombre Comercial </th>     
                  <th class='nowrap'> Servicio contratado </th>             
                  <th> Técnico/Enfermera </th>
                  <th> Importe </th>
                  <th> Revisado </th>
                  <th class="centro"></th>
                  <!--th><input type='checkbox' id="todo"></th-->
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeContratosRenovar(true);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


<!-- Caja de tarea -->
<div id='cajaFechas' class='modal hide fade'> 
    <div class='modal-header'> 
        <a class='close noAjax' data-dismiss='modal'>&times;</a>
        <h3> <i class="icon-file-text-o"></i><i class="icon-chevron-right"></i><i class="icon-cloud-download"></i> &nbsp; Generar contrato</h3> 
    </div> 
    <div class='modal-body'>
        <form id="edit-profile" class="form-horizontal" method="post">
            <fieldset>
                <?php
                    campoOculto('','codigoContrato');
                    campoSelect('vigenciaContrato','Vigencia del contrato',array('Anual','Semestral','Trimestral','Mensual','Otros'),array('ANUAL','SEMESTRAL','TRIMESTRAL','MENSUAL','OTROS'),'','selectpicker span2 show-tick');
                    campoRadio('renovar','Renovar');
                    //campoFecha('fechaVigencia','Vigencia del contrato');
                    //campoFecha('fechaEmision','Fecha emisión');
                ?>
            </fieldset>
        </form>
    </div> 
    <div class='modal-footer'> 
        <button type="button" class="btn btn-propio" id="descargarContrato"><i class="icon-cloud-download"></i> Descargar contrato</button> 
        <button type="button" class="btn btn-default" data-dismiss='modal'><i class="icon-remove"></i> Cancelar</button> 
    </div> 
</div>
<!-- Fin caja de tarea -->


<!-- Caja de renovación -->
<div id='cajaRenovacion' class='modal hide fade'> 
    <div class='modal-header'> 
        <a class='close noAjax' data-dismiss='modal'>&times;</a>
        <h3> <i class="icon-file-text-o"></i><i class="icon-chevron-right"></i><i class="icon-check-circle"></i> &nbsp; Renovar contrato</h3> 
    </div> 
    <div class='modal-body'>
        <form id="formularioRenovacion" action="index.php" class="form-horizontal" method="post">
            <fieldset>
                <?php
                    campoFecha('fechaInicio','Fecha de inicio');
                    campoFecha('fechaFin','Fecha de finalización');
                    campoRadio('enviarMail','Enviar factura por email');
                    campoRadio('incrementoIPC','Aplicar incremento IPC','SI');
                ?>
            </fieldset>
        </form>
    </div> 
    <div class='modal-footer'> 
        <button type="button" class="btn btn-propio" id="renovarContrato"><i class="icon-check-circle"></i> Renovar</button> 
        <button type="button" class="btn btn-default" data-dismiss='modal'><i class="icon-remove"></i> Cancelar</button> 
    </div> 
</div>
<!-- Fin caja de renovación -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/funciones-renovaciones.js"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>