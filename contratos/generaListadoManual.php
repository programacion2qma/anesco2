<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('../../api/phpexcel/PHPExcel.php');
	require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("plantilla.xlsx");
	

	listadoContratosManual($objPHPExcel);//Llamada a la función que rellena el Excel

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('Contratos.xlsx');


	// Definir headers
	header("Content-Type: application/ms-xlsx");
	header("Content-Disposition: attachment; filename=Contratos.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('Contratos.xlsx');


function listadoContratosManual($objPHPExcel){
	conexionBD();

	$consulta=consultaBD("SELECT contratos.*, ofertas.opcion, ofertas.subtotal, ofertas.total, ofertas.subtotalRM
						  FROM contratos INNER JOIN ofertas ON ofertas.codigo=contratos.codigoOferta
						  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
						  WHERE contratos.eliminado='NO'
						  GROUP BY contratos.codigo 
						  ORDER BY contratos.fechaInicio;");

	$i=3;
	while($datos=mysql_fetch_assoc($consulta)){
		$oferta=$datos['codigoOferta'];
		$cliente=consultaBD('SELECT clientes.* FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo='.$oferta,false,true);
		$referencia=formateaReferenciaContrato($cliente,$datos);
		$servicio=obtieneNombreServicioContratado($datos['opcion']);		

		$objPHPExcel->getActiveSheet()->getCell('A'.$i)->setValue($cliente['EMPID']);
		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($cliente['EMPNOMBRE']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($cliente['EMPCIF']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($cliente['EMPLOC']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue($cliente['EMPNTRAB']);
		$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue($referencia);		
		$objPHPExcel->getActiveSheet()->getCell('G'.$i)->setValue(formateaFechaWeb($datos['fechaInicio']));
		$objPHPExcel->getActiveSheet()->getCell('H'.$i)->setValue(formateaFechaWeb($datos['fechaFin']));
		$objPHPExcel->getActiveSheet()->getCell('I'.$i)->setValue($servicio);
		$objPHPExcel->getActiveSheet()->getCell('J'.$i)->setValue($datos['subtotal'].' €');	
		$objPHPExcel->getActiveSheet()->getCell('K'.$i)->setValue($datos['subtotalRM'].' €');	
		$objPHPExcel->getActiveSheet()->getCell('L'.$i)->setValue($datos['total'].' €');		
		$i++;
	}
	foreach(range('A','I') as $columnID) {
    	$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
	}

	cierraBD();
}

?>