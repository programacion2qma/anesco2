<?php
  $seccionActiva=1;
  include_once('../cabecera.php');
  
  $res=operacionesContratos();
  $estadisticas=estadisticasContratosRestrict('SI');
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de gestión Contratos:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-trash"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Contratos eliminados</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Contratos de oferta</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="<?php echo $_CONFIG['raiz']; ?>contratos/" class="shortcut"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label">Volver</span> </a>
                <a href="javascript:void" id='reactivar' class="shortcut noAjax"><i class="shortcut-icon icon-check"></i><span class="shortcut-label">Restaurar contrato/s</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Contratos eliminados</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable tablaTextoPeque">
              <thead>
                <tr>
                  <th> Nº Cliente </th>
                  <th> Nº Contrato </th>
                  <th> Inicio </th>
                  <th> Fin </th>
                  <th> Razón Social </th>
                  <th> Nombre Comercial </th>     
                  <th class='nowrap'> Servicio contratado </th>             
                  <th> Técnico/Enfermera </th>
                  <th> Importe </th>
                  <th> Revisado </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeContratos('SI');
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


<!-- Caja de tarea -->
<div id='cajaFechas' class='modal hide fade'> 
    <div class='modal-header'> 
        <a class='close noAjax' data-dismiss='modal'>&times;</a>
        <h3> <i class="icon-file-text-o"></i><i class="icon-chevron-right"></i><i class="icon-cloud-download"></i> &nbsp; Generar contrato</h3> 
    </div> 
    <div class='modal-body'>
        <form id="edit-profile" class="form-horizontal" method="post">
            <fieldset>
                <?php
                    campoOculto('','codigoContrato');
                    campoFecha('fechaVigencia','Vigencia del contrato');
                    campoFecha('fechaEmision','Fecha emisión');
                ?>
            </fieldset>
        </form>
    </div> 
    <div class='modal-footer'> 
        <button type="button" class="btn btn-propio" id="descargarContrato"><i class="icon-cloud-download"></i> Descargar contrato</button> 
        <button type="button" class="btn btn-default" data-dismiss='modal'><i class="icon-remove"></i> Cancelar</button> 
    </div> 
</div>
<!-- Fin caja de tarea -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('#reactivar').click(function(){//Eliminación de registros
        var valoresChecks=recorreChecks();
        if(valoresChecks['codigo0']==undefined){
            alert('Por favor, seleccione antes un registro del listado.');
        }
        else if(confirm('¿Está seguro/a de que desea restaurar los registros seleccionados?')){
            valoresChecks['elimina']='NO';
            creaFormulario(document.URL,valoresChecks,'post');//Cambiado el nombre del documento por la URL.
        }
    });



    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    
    $(document).on('click','.confirmaContrato',function(e){
        alert('Para confirmar un contrato debe restaurarlo primero.')

        return false;
    });


    $(document).on('click','.generaContrato',function(e){
        if(isValidCifNif($(this).attr('cif'))){
            
            $('#codigoContrato').val($(this).attr('codigoContrato'));

            $('#fechaVigencia').val('');
            $('#fechaEmision').val('');
            $('#cajaFechas').modal({'show':true,'backdrop':'static','keyboard':false});
        }
    });

    $('#descargarContrato').click(function(){
        var codigoContrato=$('#codigoContrato').val();
        var fechaVigencia=$('#fechaVigencia').val();
        var fechaEmision=$('#fechaEmision').val();

        creaFormulario('generaDocumento.php',{'codigoContrato':codigoContrato,'fechaVigencia':fechaVigencia,'fechaEmision':fechaEmision},'post','_blank');
        $('#cajaFechas').modal('hide');
    });
});
</script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>