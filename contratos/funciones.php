<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión contratos


function operacionesContratos(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=insertaContrato();
	}
	elseif(isset($_POST['codigoInterno'])){
		$res=insertaContrato();
	}
	elseif(isset($_POST['codigoOfertaAceptar'])){
		$res=creaContrato();
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoContrato($_POST['elimina']);
	}
	elseif(isset($_GET['renovar'])){
		$res=renuevaContrato($_GET['renovar']);
	}

	mensajeResultado('codigoInterno',$res,'Contrato');
	mensajeResultado('codigo',$res,'Contrato');
    mensajeResultado('elimina',$res,'Contrato', true);
}

function creaContrato(){
	$oferta=datosRegistro('ofertas',$_POST['codigoOfertaAceptar']);
	$_POST['codigoOferta']=$_POST['codigoOfertaAceptar'];
	
	$res=aceptarOferta();
	
	conexionBD();
	
	$numeroInterno=generaNumero($oferta['codigo'],'',false);
	$res=$res && consultaBD("INSERT INTO contratos(codigo,codigoOferta,codigoInterno,fechaInicio,fechaFin,enVigor,horasPrevistas,tecnico,eliminado,renovado,suspendido) VALUES(NULL,".$oferta['codigo'].",'".$numeroInterno."','".date('Y-m-d')."','','SI','',NULL,'NO','NO','NO');");
	$codigoContrato=mysql_insert_id();
	
	cierraBD();

	
	$res=$res && creaFacturaProforma($codigoContrato);
	$res=$res && creaDocumentos($codigoContrato);

	generaFicheroPdfContrato($codigoContrato);

	return $res;
}

function insertaContrato(){
	if(isset($_POST['codigo'])){

		if( compruebaCambioFechaInicio() ){
			$_POST['codigoInterno'] = generaNumero($_POST['codigoOferta'], ''); // Sobreescritura del número interno, para cuando le dé a F5 o trabaje en pestañas paralelas
		}

		$res=aceptarOferta();
		$res=$res && actualizaDatos('contratos',time(),'../documentos/contratos');
		$res=$res && creaDocumentos($_POST['codigo']);
		
		$res=$res && creaPlanificacionExcepcionContrato($_POST['codigo']);

		if($_POST['emitirFacturas']=='SI'){
			$res=$res && creaFacturaProforma($_POST['codigo']);
		}

		generaFicheroPdfContrato($_POST['codigo']);
	}
	elseif(isset($_POST['codigoInterno'])){
		$_POST['codigoInterno'] = generaNumero($_POST['codigoOferta'], ''); // Sobreescritura del número interno, para cuando le dé a F5 o trabaje en pestañas paralelas
		$res=insertaDatos('contratos',time(),'../documentos/contratos');
		$_POST['codigo']=$res;
		$res=aceptarOferta();
		
		$res=$res && creaFacturaProforma($_POST['codigo']);
		$res=$res && creaDocumentos($_POST['codigo']);
		$res=$res && creaPlanificacionExcepcionContrato($_POST['codigo']);

		generaFicheroPdfContrato($_POST['codigo']);
	}
	
	return $res;
}

/**
 * Verifica que, al enviar un formulario de actualización de un contrato,
 * los campos de fechaInicio y fechaInicioAnterior hayan cambiado, para
 * en su caso comprobar los años de ambos y devolver true si son distintos,
 * para generar un nuevo número interno.
 *
 * @return void
 */
function compruebaCambioFechaInicio(){
	$res = false;
	$datos = arrayFormulario();
	
	if( isset($datos['fechaInicio']) && isset($datos['fechaInicioAnterior']) && $datos['fechaInicio'] != $datos['fechaInicioAnterior'] ){
		$fechaInicio = explode('-', $datos['fechaInicio']);
		$fechaInicioAnterior = explode('-', $datos['fechaInicioAnterior']);

		if( $fechaInicio[0] != $fechaInicioAnterior[0] ){
			$res = true;
		}
	}

	return $res;
}

function creaDocumentos($codigo){
	$res=true;
	$documento=consultaBD('SELECT COUNT(codigo) AS total FROM documentosVS WHERE codigoContrato='.$codigo,true,true);
	$oferta=datosRegistro('ofertas',$_POST['codigoOferta']);
	$tiposDocumentos=array(1,4,5,12);
	if($documento['total']==0){
		if(in_array($oferta['opcion'], array(1,2))){
			foreach ($tiposDocumentos as $value) {
				$res=consultaBD('INSERT INTO documentosVS(codigo,codigoContrato,tipoDocumento,formulario,enviado,fechaCreacion,fechaEnvio,visible) VALUES(NULL,'.$codigo.','.$value.',"","NO","'.date('Y-m-d').'","00000-00-00","NO");',true);
			}
		}
	} else {
		if(!in_array($oferta['opcion'], array(1,2))){
			$res=consultaBD('DELETE FROM documentosVS WHERE codigoContrato='.$codigo,true,true);
		}
	}
	return $res;
}


//Parte de eliminación/reactivación de contratos (llevan asociada emisión de abonos/facturas)

function cambiaEstadoEliminadoContrato($estado){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos['codigo'.$i]);$i++){
		$res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],'contratos',false);
		$codigoContrato=$datos['codigo'.$i];

		$res=$res && cambiaValorCampo('eliminado',$estado,$codigoContrato,'contratos',false);
		$res=$res && compruebaFacturaNormalContrato($codigoContrato,$estado);
		$res=$res && compruebaAbonoFacturaContrato($codigoContrato,$estado);
 	}

	cierraBD();

	return $res;
}


function compruebaFacturaNormalContrato($codigoContrato,$eliminado){
	$res=true;

	if($eliminado=='SI'){

		//Si el contrato a eliminar tenía ya una factura definitiva emitida, se procede a generar automáticamente un abono para la misma
		$consulta=consultaBD("SELECT facturas.* 
							  FROM facturas INNER JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
							  WHERE codigoContrato='$codigoContrato' AND facturas.tipoFactura='NORMAL' AND eliminado='NO';");

		if(mysql_num_rows($consulta)>0){
			$datos=mysql_fetch_assoc($consulta);
			$res=generaAbonoFacturaContratoEliminado($datos);
		}
	}

	return $res;
}


function generaAbonoFacturaContratoEliminado($datosFactura){
	extract($datosFactura);

	$codigoSerieAbono=4;//Importante, se corresponde con la serie "A"
	$numero=consultaNumeroSerieFactura2($codigoSerieAbono,false);
	$fecha=date('Y-m-d');

	$res=consultaBD("INSERT INTO facturas(codigo,fecha,codigoSerieFactura,numero,codigoCliente,tipoFactura,baseImponible,total,activo,eliminado,codigoFacturaAsociada)
				     VALUES(NULL,'$fecha','$codigoSerieAbono','$numero','$codigoCliente','ABONO','-$baseImponible','-$total','SI','NO','$codigo')");
	if($res){
		mensajeAdvertencia("Se ha emitido un abono con número $numero.");
	}

	return $res;
}


function compruebaAbonoFacturaContrato($codigoContrato,$eliminado){
	$res=true;

	if($eliminado=='NO'){

		//Si el contrato a reactivar tenía un abono asociado (principalmente creado desde la función compruebaFacturaNormalContrato()), se vuelve a activar la factura original creándose una nueva con los mismos datos (excepto la fecha y el número, que se incrementa)
		$consulta=consultaBD("SELECT abonos.* 
							  FROM facturas abonos INNER JOIN facturas ON abonos.codigoFacturaAsociada=facturas.codigo
							  INNER JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
							  WHERE codigoContrato='$codigoContrato' AND abonos.tipoFactura='ABONO';");

		if(mysql_num_rows($consulta)>0){
			$datos=mysql_fetch_assoc($consulta);
			$res=generaFacturaContratoReactivado($datos['codigoFacturaAsociada']);
		}
	}

	return $res;
}

function generaFacturaContratoReactivado($codigoFacturaAnulada){
	$facturaAnulada=consultaBD("SELECT * FROM facturas WHERE codigo='$codigoFacturaAnulada';",false,true);
	$numero=consultaNumeroSerieFactura2($facturaAnulada['codigoSerieFactura'],false);
	$fecha=date('Y-m-d');

	$res=copiaDatos('facturas',$facturaAnulada,array('numero'=>$numero,'fecha'=>$fecha),array('contratos_en_facturas'=>'codigoFactura'),false);

	if($res){
		mensajeAdvertencia("Se ha creado una factura nueva con número $numero");
	}

	return $res;
}

//Fin parte de eliminación/reactivación de contratos (llevan asociada emisión de abonos/facturas)



function aceptarOferta(){
	$res=true;
	$res=consultaBD('UPDATE ofertas SET aceptado="SI" WHERE codigo='.$_POST['codigoOferta'],true);
	$oferta=datosRegistro('ofertas',$_POST['codigoOferta']);
	if(isset($_POST['enVigor']) && $_POST['enVigor']=='SI'){
		$res=consultaBD('UPDATE clientes SET activo="SI" WHERE codigo='.$oferta['codigoCliente'],true);
	}
	return $res;
}

function modificarTecnico(){
	$res=true;
	$oferta=datosRegistro('ofertas',$_POST['codigoOferta']);
	$res=consultaBD('UPDATE clientes SET tecnico="'.$_POST['tecnico'].'" WHERE codigo='.$oferta['codigoCliente'],true);
	return $res;
}

function insertaRenovaciones($contrato){
	$res=true;//Siempre que vayamos a usarlo con un AND, inicializarlo a true
	$datos=arrayFormulario();
	conexionBD();
	$res=consultaBD('DELETE FROM contratos_renovaciones WHERE codigoContrato='.$contrato);
	$i=0;
	while(isset($datos['fecha'.$i])){
		if($datos['fecha'.$i] != ''){
			$res=$res && consultaBD("INSERT INTO contratos_renovaciones VALUES(NULL, ".$contrato.",'".$datos['fecha'.$i]."')");
		}
		$i++;
	}


	cierraBD();

	return $res;
}


function gestionContratos(){
	$validacion=false;

	operacionesContratos();
	
	abreVentanaGestionContrato();

	campoOculto('NO','enviarMail');
	
	if(isset($_GET['codigoContrato'])){
		$datos=datosRegistro('contratos',$_GET['codigoContrato']);
		$numeroInterno = generaNumero($datos['codigoOferta'],date('Y'));
		$datos['codigoInterno']=$numeroInterno;
		campoOculto($numeroInterno,'codigoInterno');

		$validacion=true;
	} 
	else {
		$datos=compruebaDatos('contratos');
	}
	
	if(!$datos){
		$oferta=$_GET['codigoOferta'];
		$datosOferta=datosRegistro('ofertas',$oferta);
		$numeroInterno = generaNumero($oferta,date('Y'));
		campoTexto('codigoInterno','Nº de contrato',$numeroInterno,'input-mini pagination-right');

		$cliente=consultaBD('SELECT clientes.*, total,subtotalRM,numRM,usuario, clave, fechaInicioOferta, fechaFinOferta, codigoFormaPagoOferta, codigoCuentaPropiaOferta, ofertas.observaciones FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN usuarios_clientes ON clientes.codigo=usuarios_clientes.codigoCliente INNER JOIN usuarios ON usuarios_clientes.codigoUsuario=usuarios.codigo WHERE ofertas.codigo='.$oferta,true,true);
		$observaciones=$cliente['observaciones'];
		divOculto($cliente['codigo'],'codigoCliente');

		campoDato('Cliente',"<a href='../clientes/gestion.php?codigo=".$cliente['codigo']."' class='noAjax' target='_blank'>".$cliente['EMPNOMBRE']."</a>",'EMPNOMBRE');
		campoDato('CIF/DNI',$cliente['EMPCIF'],'EMPCIF');
		campoDato('Importe contrato',formateaNumeroWeb($cliente['total']).' €','importeTotal');
		if($cliente['numRM']!='' && $cliente['numRM']>0){
			campoDato('Importe RM',formateaNumeroWeb($cliente['subtotalRM']).' €','importeTotalRM');
		}

		$validacion=true;

		$fechaInicio=$cliente['fechaInicioOferta'];
		$fechaFin=$cliente['fechaFinOferta'];
		$codigoFormaPago=$cliente['codigoFormaPagoOferta'];
		$codigoCuentaPropia=$cliente['codigoCuentaPropiaOferta'];
	} 
	else {
		if($datos && !esSuperAdmin()){
			campoOculto('SI','bloqueado');
		}
		$oferta=$datos['codigoOferta'];
		$observaciones=$datos['observaciones'];
		$cliente=consultaBD('SELECT clientes.*, total,subtotalRM,numRM, usuario, clave FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN usuarios_clientes ON clientes.codigo=usuarios_clientes.codigoCliente INNER JOIN usuarios ON usuarios_clientes.codigoUsuario=usuarios.codigo WHERE ofertas.codigo='.$oferta,true,true);

		$numeroInterno=formateaReferenciaContrato($cliente,$datos);
		campoDato('Nº de contrato',$numeroInterno);

		divOculto($cliente['codigo'],'codigoCliente');
		campoDato('Cliente',"<a href='../clientes/gestion.php?codigo=".$cliente['codigo']."' class='noAjax' target='_blank'>".$cliente['EMPNOMBRE']."</a>",'EMPNOMBRE');

		campoDato('CIF/DNI',$cliente['EMPCIF'],'EMPCIF');

		campoDato('Importe contrato',compruebaImporteContrato($cliente['total'],$datos).' €','importeTotal');

		if($cliente['numRM']!='' && $cliente['numRM']>0){
			campoDato('Importe RM',compruebaImporteContrato($cliente['subtotalRM'],$datos).' €','importeTotalRM');
		}

		$datosOferta=datosRegistro('ofertas',$oferta);
		$opciones=array(1=>'SPA 4 Especialidades',2=>'SPA Vigilancia de la salud',3=>'SPA Especialides técnicas',4=>'Otras actuaciones');
		campoDato('Contratación',$opciones[$datosOferta['opcion']]);

		if($datosOferta['opcion']==3){
			campoDato('Especialidades técnicas',str_replace('&{}&', ', ', $datosOferta['especialidadTecnica']));
		}
		if($datosOferta['opcion']==4){
			campoDato('Definir',nl2br($datosOferta['otraOpcion']));
		}
		if($datosOferta['observaciones']!=''){
			areaTexto('observaciones','Observaciones',$datosOferta,'areaInforme');		
		}

		$fechaInicio=$datos['fechaInicio'];
		$fechaFin=$datos['fechaFin'];
		$codigoFormaPago=$datos['codigoFormaPago'];
		$codigoCuentaPropia=$datos['codigoCuentaPropiaContrato'];
		$validacion=true;
	}
	
	campoOculto($datos,'suspendido','NO');
	campoOculto('NO','emitirFacturas');

	cierraColumnaCampos();
	abreColumnaCampos('span6');

	campoSelectConsulta('codigoColaborador','Colaborador',"SELECT codigo, razonSocial AS texto FROM colaboradores ORDER BY razonSocial",$datos);

	campoOculto($oferta,'codigoOferta');
	//campoRadio('enVigor','Contrato en vigor',$datos,'SI');
	campoOculto($datos,'enVigor','SI');
	campoOculto($datos,'eliminado','NO');

	campoSelect('vigencia','Vigencia del contrato',array('','Anual','Semestral','Trimestral','Mensual','Otros'),array('','ANUAL','SEMESTRAL','TRIMESTRAL','MENSUAL','OTROS'),$datos,'selectpicker span2 show-tick');
	campoFecha('fechaInicio','Fecha de inicio',$fechaInicio);
	if($fechaInicio){
		campoOculto(formateaFechaWeb($fechaInicio), 'fechaInicioAnterior');
	}
	campoFecha('fechaFin','Fecha de fin',$fechaFin);
	
	camposAntiguosContrato($datos);
		
	campoFormaPago($codigoFormaPago);
	campoSelectCuentaPropia($codigoCuentaPropia);
	
	if($datosOferta['opcion']==1){//Si la opción elegida es las 4 especialidades, debe emitirse 2 facturas, una de PT y otra de RM
		echo "<h3 class='apartadoFormulario'>Facturación PT</h3>";
		campoVencimientosOfacturas($datos);
		creaTablaFechasVencimiento($datos,$codigoFormaPago,$datosOferta);

		echo "<h3 class='apartadoFormulario'>Facturación RM</h3>";
		campoVencimientosOfacturas($datos,'vencimientosOfacturas2');
		creaTablaFechasVencimiento($datos,$codigoFormaPago,$datosOferta,true);
		echo "<h3 class='apartadoFormulario'></h3>";
	}
	else{
		campoVencimientosOfacturas($datos);
		creaTablaFechasVencimiento($datos,$codigoFormaPago,$datosOferta);

		campoOculto('','vencimientosOfacturas2');
	}

	campoRadio('contratoRevisado','Revisado',$datos);
	campoEmitirPlanificacion($datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	echo '<div style="margin-left:40px;margin-bottom:10px">';
	campoCheckIndividual('checkNoRenovar','No renovar',$datos);
	echo '</div>';
	tablaRenovaciones($datos);
	cierraColumnaCampos(true);
	
	campoOculto($datos,'renovado','NO');
	areaTexto('observaciones','Observaciones',$observaciones,'areaInforme');

	if($datosOferta['opcion']==4){
		campoFichero('fichero','Fichero',0,$datos,'../documentos/contratos/','Descargar');
	} else {
		campoOculto($datos,'fichero','NO');
	} 

	camposInformacionOferta($datos);

	campoOculto($datos,'ficheroER','NO');
	campoOculto($datos,'incrementoIPC');
	campoOculto($datos,'incrementoIpcAplicado');

	cierraVentanaGestionContratos();
	
	if($validacion){
		//Campos para validación de cliente
		divOculto($cliente['EMPID'],'EMPID');
		divOculto($cliente['EMPNOMBRE'],'EMPNOMBRE');
		divOculto($cliente['EMPACTIVIDAD'],'EMPACTIVIDAD');
		divOculto($cliente['EMPDIR'],'EMPDIR');
		divOculto($cliente['EMPLOC'],'EMPLOC');
		divOculto($cliente['EMPTELPRINC'],'EMPTELPRINC');
		divOculto($cliente['EMPMARCA'],'EMPMARCA');
		divOculto($cliente['EMPCNAE'],'EMPCNAE');
		divOculto($cliente['EMPCP'],'EMPCP');
		divOculto($cliente['EMPPROV'],'EMPPROV');
		divOculto($cliente['EMPEMAILPRINC'],'EMPEMAILPRINC');
		divOculto($cliente['EMPRL'],'EMPRL');
		divOculto($cliente['EMPRLDNI'],'EMPRLDNI');
		divOculto($cliente['EMPRLCARGO'],'EMPRLCARGO');
		divOculto($cliente['EMPNTRAB'],'EMPNTRAB');
		divOculto($cliente['EMPPC'],'EMPPC');
		divOculto($cliente['EMPCENTROS'],'EMPCENTROS');
		divOculto($cliente['comercial'],'comercial');
		divOculto($cliente['usuario'],'usuario');
		divOculto($cliente['clave'],'clave');
		divOculto($cliente['EMPIBAN'],'EMPIBAN');

		formasDePagoRemesablesValidacion();

		centrosDeTrabajoParaValidacion($cliente['codigo']);
		//Fin campos para validaciónd e cliente
	}

}

function campoEmitirPlanificacion($datos){

	if($_SESSION['tipoUsuario']=='ADMIN'){
		campoRadio('emitirPlanificacionSinPagar','Emitir planificación sin cobrar',$datos);
	}
	else{
		campoOculto($datos,'emitirPlanificacionSinPagar','NO');
	}

}

function formasDePagoRemesablesValidacion(){
	$consulta=consultaBD("SELECT codigo FROM formas_pago WHERE remesable='SI' AND eliminado='NO';",true);

	$i=0;
	while($datos=mysql_fetch_assoc($consulta)){
		divOculto($datos['codigo'],'formaPago'.$i);
		$i++;
	}
}

function campoVencimientosOfacturas($datos,$nombreCampo='vencimientosOfacturas'){
	if($datos){
		campoDato('Tipo emisión',ucfirst(strtolower($datos[$nombreCampo])));
		campoOculto($datos,$nombreCampo);
	}
	else{
		campoRadio($nombreCampo,'Tipo emisión',$datos,'VENCIMIENTOS',array('Vencimientos','Facturas'),array('VENCIMIENTOS','FACTURAS'));
	}
}

function camposAntiguosContrato($datos){
	if($datos['tecnico']!=NULL){
		campoTexto('horasPrevistas','Horas previstas',$datos,'input-small');

		// if($_SESSION['tipoUsuario']=='ADMIN'){
		// 	campoSelectConsulta('tecnico','Técnico/Enfermera','SELECT codigo, CONCAT(nombre, " ", apellidos,IF(habilitado="NO"," (Inactivo)","")) AS texto FROM usuarios WHERE (tipo="TECNICO" OR tipo="ENFERMERIA") AND (habilitado="SI" OR codigo="'.$datos['tecnico'].'");',$datos);
		// } else {
			campoOculto($datos,'tecnico','NULL');
		//}
	}
	else{
		campoOculto(0,'horasPrevistas');
		campoOculto('NULL','tecnico');
	}
}

function centrosDeTrabajoParaValidacion($codigoCliente){
	$res='OK';

	conexionBD();

	$datos=consultaBD("SELECT COUNT(codigo) AS total FROM clientes_centros WHERE codigoCliente='$codigoCliente';",false,true);
	$consultaVacios=consultaBD("SELECT codigo FROM clientes_centros WHERE codigoCliente='$codigoCliente' AND direccion!='' AND cp!='' AND localidad!='' AND provincia!='';");

	cierraBD();


	if($datos['total']==0 || mysql_num_rows($consultaVacios)==0){
		$res='';
	}

	divOculto($res,'direccionCentroTrabajo');
}


function abreVentanaGestionContrato(){
	if(isset($_GET['editable']) && $_GET['editable']=='NO'){
		$ejercicio=obtieneEjercicioParaWhere();
		abreVentanaGestionConBotones('Gestión de Contratos','index.php','span12','icon-edit','',true,'noAjax','index.php?anio='.$ejercicio.'&aprobados',false);
	}
	elseif(isset($_GET['editable']) && $_GET['editable']=='ELIMINADO'){
		abreVentanaGestionConBotones('Gestión de Contratos','index.php','span12','icon-edit','',true,'noAjax','eliminados.php',false);
	}
	elseif((isset($_GET['editable']) && $_GET['editable']=='CREADO')){//Modificación de uno existente
		abreVentanaGestionConBotones('Gestión de Contratos','index.php','span12','icon-edit','',true,'noAjax','index.php',false);
	}
	else{
		abreVentanaGestionConBotones('Gestión de Contratos','index.php','span12','icon-edit','',true,'noAjax');
	}
}

function cierraVentanaGestionContratos(){
	if(isset($_GET['editable']) && $_GET['editable']=='NO'){
		cierraVentanaGestionAdicional('Guardar y re-emitir facturas','index.php?enVigor',true,true);
	}
	elseif(isset($_GET['editable']) && $_GET['editable']=='HISTORICO'){
		cierraVentanaGestionAdicional('Guardar y re-emitir facturas','index.php?historico',true,true);
	}
	elseif(isset($_GET['editable']) && $_GET['editable']=='ELIMINADO'){
		cierraVentanaGestion('eliminados.php',true,true);
	}
	elseif((isset($_GET['editable']) && $_GET['editable']=='CREADO')){//Modificación de uno existente
		cierraVentanaGestionAdicional('Guardar y re-emitir facturas','index.php',true,true);
	}
	else{
		cierraVentanaGestion('index.php',true);
	}
}

//Parte de campos personalizados


//Fin parte de campos personalizados

function imprimeContratos($eliminado){
	global $_CONFIG;
	$iconoRevisado=array('SI'=>'<i class="icon-check-circle iconoFactura icon-success" title="Revisado"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger" title="Pendiente"></i>');

	$celdaEstado=false;

	if($eliminado=='SI'){
		$where='WHERE contratos.eliminado="SI"';
	}
	elseif (isset($_GET['baja'])) {
		$where='WHERE clientes.codigo NOT IN (SELECT clientes.codigo FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE activo="SI" AND contratos.enVigor="SI" AND clientes.eliminado="NO")';
	}
	elseif(isset($_GET['historico'])){
		$where='WHERE contratos.eliminado="NO" AND enVigor="NO"';
	}
	elseif(isset($_GET['enVigor'])){
		$where='WHERE contratos.eliminado="NO" AND enVigor="SI" AND facturas.facturaCobrada="SI"';
	}
	elseif(isset($_GET['pendiente'])){
		$where='WHERE contratos.eliminado="NO" AND enVigor="SI" AND (facturas.facturaCobrada IS NULL OR facturas.facturaCobrada="NO")';
	} 
	else{
		$where='WHERE contratos.eliminado="NO"';
		$celdaEstado=true;
	}

	// Añadido seccion para comprobar contratos con facturas eliminadas, para su revision y acceso a historico.
	$whereFactura = " AND facturas.eliminado='NO'";
	if (isset($_GET['facturadoEliminado'])) {
		$whereFactura = "";
	}

	conexionBD();

	if($_SESSION['tipoUsuario'] == 'COLABORADOR'){
		$consulta=consultaBD("SELECT (ofertas.total-ofertas.importe_iva) AS importe, clientes.EMPNOMBRE,clientes.EMPMARCA,clientes.codigo AS codigoCliente, contratos.*,MAX(contratos_renovaciones.fecha), ofertas.opcion,
							  IFNULL(facturas.facturaCobrada,'NO') AS facturaCobrada, clientes.EMPID, formularioPRL.codigoUsuarioTecnico, contratoRevisado, contratos.fichero, suspendido, incrementoIPC, incrementoIpcAplicado,
							  emitirPlanificacionSinPagar
							  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
							  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
							  LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
							  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo
							  LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
							  ".$where." AND (facturas.codigo IS NULL OR (facturas.tipoFactura!='ABONO' ".$whereFactura."))
							  codigoCliente IN (SELECT codigo FROM clientes WHERE EMPASESORIA=".$_SESSION['codigoU'].") 
							  GROUP BY contratos.codigo 
							  ORDER BY contratos.fechaInicio;");
	}
	elseif($_SESSION['tipoUsuario'] == 'TECNICO'){
		$consulta=consultaBD("SELECT (ofertas.total-ofertas.importe_iva) AS importe, clientes.EMPNOMBRE,clientes.EMPMARCA,clientes.codigo AS codigoCliente,contratos.*, MAX(contratos_renovaciones.fecha) AS renovacion, 
							  contratos.tecnico, ofertas.opcion, IFNULL(facturas.facturaCobrada,'NO') AS facturaCobrada, clientes.EMPID, formularioPRL.codigoUsuarioTecnico, contratoRevisado, contratos.fichero, suspendido, incrementoIPC, incrementoIpcAplicado,
							  emitirPlanificacionSinPagar
							  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
							  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
							  LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
							  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo
							  LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
							  ".$where." AND (facturas.codigo IS NULL OR (facturas.tipoFactura!='ABONO' ".$whereFactura."))
							  AND (contratos.tecnico=".$_SESSION['codigoU']." || formularioPRL.codigoUsuarioTecnico=".$_SESSION['codigoU'].") 
							  GROUP BY contratos.codigo 
							  ORDER BY contratos.fechaInicio;");

	}
	else {
		$consulta=consultaBD("SELECT (ofertas.total-ofertas.importe_iva) AS importe, clientes.EMPNOMBRE,clientes.EMPMARCA,clientes.codigo AS codigoCliente,contratos.*, MAX(contratos_renovaciones.fecha) AS renovacion, 
							  contratos.tecnico, ofertas.opcion, IFNULL(facturas.facturaCobrada,'NO') AS facturaCobrada, clientes.EMPID, formularioPRL.codigoUsuarioTecnico, contratoRevisado, contratos.fichero,facturas.codigo AS codigoFactura, suspendido, incrementoIPC, incrementoIpcAplicado,
							  emitirPlanificacionSinPagar
							  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
							  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
							  LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
							  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo
							  LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
							  ".$where." AND (facturas.codigo IS NULL OR (facturas.tipoFactura!='ABONO' ".$whereFactura."))
							  GROUP BY contratos.codigo 
							  ORDER BY contratos.fechaInicio;");
	}

	cierraBD();

	while($datos=mysql_fetch_assoc($consulta)){
		
		$cliente=datosRegistro('clientes',$datos['codigoCliente']);
		
		if($datos['tecnico']!=NULL){
			$tecnico=datosRegistro('usuarios',$datos['tecnico']);
		} 
		else {
			$tecnico=datosRegistro('usuarios',$datos['codigoUsuarioTecnico']);
		}
		
		$servicio=obtieneNombreServicioContratado($datos['opcion']);

		if($celdaEstado){
			$importe=compruebaImporteContrato($datos['importe'],$datos);
			$estado=obtieneEstadoContratoListado($datos,true,$importe);

			echo "
				<tr>
					<td class='nowrap'>".$datos['EMPID']."</td>
					<td class='nowrap referenciaContrato'>".formateaReferenciaContrato($cliente,$datos)."</td>
					<td>".formateaFechaWeb($datos['fechaInicio'])."</td>
					<td>".formateaFechaWeb($datos['fechaFin'])."</td>
					<td>".$cliente['EMPNOMBRE']."</td>
					<td>".$cliente['EMPMARCA']."</td>
					<td>".$servicio."</td>
					<td>".$tecnico['nombre']." ".$tecnico['apellidos']."</td>
					<td class='nowrap'>".$importe." €</td>
					<td class='centro'>".$iconoRevisado[$datos['contratoRevisado']]."</td>
					<td>".$estado."</td>
					<td>
						".crearBotonContratos($datos,$cliente['EMPCIF'],$eliminado)."
					</td>
					<td>
						<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
		        	</td>
				</tr>";
		}
		else{
			echo "
			<tr>
				<td class='nowrap'>".$datos['EMPID']."</td>
				<td class='nowrap referenciaContrato'>".formateaReferenciaContrato($cliente,$datos)."</td>
				<td>".formateaFechaWeb($datos['fechaInicio'])."</td>
				<td>".formateaFechaWeb($datos['fechaFin'])."</td>
				<td>".$cliente['EMPNOMBRE']."</td>
				<td>".$cliente['EMPMARCA']."</td>
				<td>".$servicio."</td>
				<td>".$tecnico['nombre']." ".$tecnico['apellidos']."</td>
				<td class='nowrap'>".compruebaImporteContrato($datos['importe'],$datos)." €</td>
				<td class='centro'>".$iconoRevisado[$datos['contratoRevisado']]."</td>
				<td>
					".crearBotonContratos($datos,$cliente['EMPCIF'],$eliminado)."
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
		}
	}

}




function generaNumeroDesdeJS(){
	$anio=explode('/', $_POST['fechaInicio']);
	echo generaNumero($_POST['codigoOferta'],$anio[2]);
}

function generaNumero($oferta,$anio,$conexion=true){

	if($anio=='' && isset($_POST['fechaInicio'])){
		$fecha=explode('/',$_POST['fechaInicio']);
		$anio=$fecha[2];
	}
	elseif($anio==''){
		$anio=date('Y');
	}

	$consulta=consultaBD("SELECT MAX(contratos.codigoInterno) AS numero 
						  FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo 
						  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
						  WHERE clientes.codigo = (SELECT codigoCliente FROM ofertas WHERE codigo=".$oferta.") 
						  AND YEAR(contratos.fechaInicio)='".$anio."'", $conexion, true);
	
	return $consulta['numero']+1;
}

function estadisticasContratosRestrict($eliminado){
	$res=array();

	conexionBD();
	if($eliminado=='SI'){
		$where='WHERE contratos.eliminado="SI"';
	}
	elseif(isset($_GET['historico'])){
		$where='WHERE contratos.eliminado="NO" AND enVigor="NO"';
	} 
	elseif(isset($_GET['baja'])){
		$where='WHERE clientes.codigo NOT IN (SELECT clientes.codigo FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE activo="SI" AND contratos.enVigor="SI" AND clientes.eliminado="NO")';
	} 
	elseif(isset($_GET['enVigor'])){
		$where='WHERE contratos.eliminado="NO" AND enVigor="SI" AND facturas.facturaCobrada="SI" AND (contratos_en_facturas.codigo IS NULL OR facturas.eliminado="NO")';
	} 
	elseif(isset($_GET['pendiente'])){
		$where='WHERE contratos.eliminado="NO" AND enVigor="SI" AND (facturas.facturaCobrada IS NULL OR facturas.facturaCobrada="NO")';
	}


	if($_SESSION['tipoUsuario'] == 'COLABORADOR'){
		$consulta=consultaBD("SELECT COUNT(DISTINCT contratos.codigo) AS total,SUM(ofertas.total) AS importe 
							  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
							  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
							  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
							  ".$where."
							  AND codigoCliente IN (SELECT codigo FROM clientes WHERE EMPASESORIA=".$_SESSION['codigoU'].") 
							  GROUP BY contratos.codigo",false,true);
	}
	else if($_SESSION['tipoUsuario'] == 'TECNICO'){
		$consulta=consultaBD("SELECT COUNT(DISTINCT contratos.codigo) AS total,SUM(ofertas.total) AS importe 
							  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
							  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
							  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
							  LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
							  ".$where."
							  AND (contratos.tecnico=".$_SESSION['codigoU']." || formularioPRL.codigoUsuarioTecnico=".$_SESSION['codigoU'].");",false, true);
	} 
	else {
		$consulta=consultaBD("SELECT COUNT(DISTINCT contratos.codigo) AS total,SUM(ofertas.total) AS importe 
							  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
							  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
							  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
							  ".$where.";",false, true);
	}

	$res['total'] = $consulta['total'];
	$res['importe'] = $consulta['importe'];

	cierraBD();

	return $res;
}



function tablaRenovaciones($datos){
	echo"	<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaRenovaciones' style='width:200px;'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Fechas de renovaciones </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    $i=0;
	if($datos){
		$consulta=consultaBD("SELECT * FROM contratos_renovaciones WHERE codigoContrato=".$datos['codigo'],true);				  
		while($renovacion=mysql_fetch_assoc($consulta)){
		 	echo "<tr class='centro'><td>".formateaFechaWeb($renovacion['fecha'])."</td></tr>";;
		}
	}
	echo "
					</tbody>
	                </table>
	            </div>
		 	";
}

function generaPDFContrato($codigo){
	$datos=datosRegistro('contratos',$codigo);
	$oferta=datosRegistro('ofertas',$datos['codigoOferta']);
	$cliente=obtieneDatosClientePDF($oferta['codigoCliente']);
	$formaPago=datosRegistro('formas_pago',$datos['codigoFormaPago']);
	$datosCuentas=datosRegistro('cuentas_propias',$datos['codigoCuentaPropiaContrato']);

	$vigenciaContrato=$datos['vigencia'];
	$noRenovar=$datos['checkNoRenovar'];

	$referencia=formateaReferenciaContrato($cliente,$datos);
	$tiposContratos=array(1=>'SPA 4 Especialidades',2=>'SPA Vigilancia de la salud',3=>'SPA Especialidades técnicas',4=>'');
	$imagenFirma='';
	
	$consultaCentros=consultaBD("SELECT clientes_centros.* 
								FROM clientes_centros INNER JOIN centros_trabajo_en_ofertas ON clientes_centros.codigo=centros_trabajo_en_ofertas.codigoClienteCentro
								WHERE codigoOferta='".$oferta['codigo']."' AND codigoClienteCentro IS NOT NULL AND clientes_centros.eliminado='NO';",true);
	$centros=mysql_num_rows($consultaCentros);  

	/*
	$numPago1='';
	$porcentaje1='';
	$numPago2='';
	$porcentaje2='';
	$numPago3='';
	$porcentaje3='';
	$numPago4='';
	$porcentaje4='';
	$numPago5='';
	$porcentaje5='';

	$transferencia='';
	if(substr_count(strtolower($formaPago['forma']),'transferencia')>0){
		$transferencia='X';
		$clase1='trMarcado';
		$numPago1='1';
		$porcentaje1='100';
	}
	$cheque='';
	if(substr_count(strtolower($formaPago['forma']),'cheque')>0){
		$cheque='X';
		$clase2='trMarcado';
		$numPago2='1';
		$porcentaje2='100';
	}
	$domiciliacion='';
	$iban=array('','','','','','');
	if(substr_count(strtolower($formaPago['forma']),'domiciliaci')>0){
		$domiciliacion='X';
		$clase3='trMarcado';
		$iban = str_split($cliente['EMPIBAN'], 4);
		$tamanio=count($iban);
		for($i=$tamanio;$i<6;$i++){
			$iban[$i]='';
		}

		$numPago3='1';
		$porcentaje3='100';
	}
	$efectivo='';
	if(substr_count(strtolower($formaPago['forma']),'efectivo')>0){
		$efectivo='X';
		$clase4='trMarcado';
		$numPago4='1';
		$porcentaje4='100';
	}
	$face='';
	if(substr_count(strtolower($formaPago['forma']),'face')>0){
		$face='X';
		$clase5='trMarcado';
		$numPago5='1';
		$porcentaje5='100';
	}
	*/

	$tablas1=array('Plan de Prevención','Evaluación de riesgos','Planificación  actividades preventivas','Coordinación Actividades empresariales','Información a los trabajadores','Equipos de trabajo y medios de protección','Formación a los trabajadores','Investigar los daños a la salud','Determinar actividades con riesgos especiales','Protección de trabajadores sensibles, maternidad y  menores','Programación','Integración','Asesoramiento','Inspección de Trabajo','Memoria','Evaluación de riesgos','Planificación de actividades preventivas.','Equipos de trabajo y medios de protección','Equipos de trabajo y medios de protección','Información-Formación a los trabajadores','Medidas de emergencia','Minas: Elaboración de documentos de seguridad minera','Evaluación de riesgos ','Planificación de actividades preventivas','Información-Formación a los trabajadores','Determinar actividades con riesgos especiales','Evaluación de riesgos','Planificación de actividades preventivas','Evaluación de riesgos','Evaluación de riesgos','Evaluación de riesgos','Información-Formación a los trabajadores','Vigilancia de la Salud','Información-Formación a los trabajadores','Unidad móvil','Medidas de seguridad y salud en obras de construcción');
	$tablas2=array('G- 1 Asesoramiento en el diseño, implantación y aplicación de un Plan de Prevención.','G- 2 Evaluación inicial y/o  revisión de evaluación de riesgos en los casos exigidos por el ordenamiento jurídico, en particular, con ocasión de los daños para la salud de los trabajadores que se hayan producido.','G- 3 Dar asesoramiento y apoyo en la planificación de la actividad preventiva y la determinación de las prioridades en la adopción de las medidas preventivas y la vigilancia de su eficacia','G- 4 Elaboración de un procedimiento de coordinación de actividades empresariales y asesoramiento para su implantación.','G.4.1 Implantación del Procedimiento','G.4.2.Seguimiento y control del cumplimiento del procedimiento.','G- 5 Asesoramiento y apoyo a la información de los trabajadores','G- 5.1. Elaboración de un Plan de información','G- 5.2. Desarrollo de material informativo','G- 6 Elaboración de instrucciones generales para la adquisición de equipos de trabajo, productos químicos y otros bienes','G- 7.1. Diseño y asesoramiento para la programación de un Plan de formación sobre Riesgos Generales','G- 7.2. Desarrollo de materiales e impartición de formación del art. 19 de la Ley 31/1995 de Prevención de Riesgos Laborales','G-7.3. Otra formación no incluida en el artículo 19 de la Ley 31/1995 de Prevención de Riesgos Laborales','G-8.1. Elaboración de un procedimiento de investigación de accidentes y enfermedades profesionales','G-8.2. Elaboración de informes de investigación de accidentes y enfermedades profesionales','G-9 Identificación de actividades o procesos que reglamentariamente sean considerados como peligrosos o con riesgos especiales.','G-9.1  Estudios específicos de actividades o procesos peligrosos.','G9.2 Evaluación de riesgos especiales.','G-10 Asesoramiento respecto a riesgos especiales para trabajadores especialmente sensibles, riesgo para el embarazo o lactancia.','G-11 Elaboración de una Programación anual de actividades del SPA','G-12  Valoración de la efectividad de la integración de la PRL en las especialidades concertadas','G-13.Asesoramiento a empresarios, trabajadores, a sus representantes y órganos de representación especializados','G-13.1 Realización de evaluaciones específicas para trabajadores especialmente sensibles, embarazadas, o en periodo de lactancia.','G-14.Asistencia a requerimientos de la Inspección de Trabajo sobre temas preventivos','G-15 Elaboración de la Memoria anual del SPA en las especialidades concertadas','S-1 Identificar, evaluar y proponer las medidas correctoras que procedan considerando todos los riesgos de esta naturaleza existentes en la empresa (incluidos los originados por las condiciones de las máquinas, equipos e instalaciones y la verificación de su mantenimiento adecuado, condiciones generales de los lugares, locales, instalaciones de servicio y protección).','S-2  Asesoramiento y apoyo en la planificación de la actividad preventiva y la determinación de las prioridades en la adopción de las medidas preventivas y la vigilancia de su eficacia','S-3 Seguimiento y valoración de la implantación de las medidas preventivas derivadas de la evaluación con la periodicidad que requieran los riesgos existentes','S-4 Evaluación del riesgo de explosión derivado de la presencia de atmósferas explosivas  de acuerdo al RD 681/2003','S-5 Estudios específicos y/o adecuación de los equipos de trabajo al RD 1215/1997.','S-6 Diseño y asesoramiento para la programación de un Plan de información-formación sobre Riesgos especifico de seguridad','S-6.1 Desarrollo de materiales e impartición de información- formación específica de seguridad (art. 18 y art 19 de la Ley 31/1995 de Prevención de Riesgos Laborales).','S-6.2. Otra formación no incluida en el artículo 19 de la Ley 31/1995 de Prevención de Riesgos Laborales ','S-7. Elaboración de las medidas de actuación en caso de emergencia y análisis de la necesidad de elaborar un Plan de autoprotección','S-8 Elaboración de Planes de Autoprotección','S-9 Elaboración de documentos de seguridad minera del RD 1389/1997','H-1 Identificar, evaluar y proponer las medidas correctoras que procedan considerando para ello todos los riesgos de esta naturaleza existentes en la empresa valorando la necesidad o no necesidad de realizar mediciones ambientales.','H-2 Diseño y aplicación de un programa de mediciones ambientales y análisis de resultados. ','H-3  Asesoramiento y apoyo en la planificación de la actividad preventiva y la determinación de las prioridades en la adopción de las medidas preventivas y la vigilancia de su eficacia','H-4 Seguimiento y valoración de la implantación de las medidas preventivas derivadas de la evaluación con la periodicidad que requieran los riesgos existentes','H5.Diseño y asesoramiento para la programación de un Plan de formación sobre Riesgos específicos de Higiene','H5.1 Desarrollo de materiales e Impartición de la información- formación específica de Higiene (art. 18 y art 19 de la Ley 31/1995 de Prevención de Riesgos Laborales).','H-5.2. Otra formación no incluida en el artículo 19 de la Ley 31/1995 de Prevención de Riesgos Laborales ','H-6  Valoración de la necesidad de realización de mediciones higiénicas especificas','H-6.1  Medición de contaminantes físicos','H6.2. Medición de contaminantes químicos','H6.3 Medición de contaminantes biológicos','E-1 Identificar, evaluar y proponer las medidas correctoras que procedan considerando para ello todos los riesgos de esta naturaleza existentes en la empresa.','E-2  Asesoramiento y apoyo en la planificación de la actividad preventiva y la determinación de las prioridades en la adopción de las medidas preventivas y la vigilancia de su eficacia','E-3 Seguimiento y valoración de la implantación de las medidas preventivas derivadas de la evaluación con la periodicidad que requieran los riesgos existentes','E-4 Elaboración de informes de factores psicosociales y organizativos. ','E-5 Elaboración de informes de carga física de trabajo. ','E-6 Elaboración de informes de factores ambientales relacionados con el confort/disconfort. ','E- 7 Diseño y asesoramiento para la elaboración  de un  programa formativo sobre Ergonomía y Psicosociología aplicada','E-7 .1  Desarrollo de materiales e Impartición deinformación- formación específica en  Ergonomía y Psicosociología aplicada( art. 18 y art 19 de la Ley 31/1995 de Prevención de Riesgos Laborales).','E-7.2. Otra formación no incluida en el artículo 19 de la Ley 31/1995 de Prevención de Riesgos Laborales','V-1 Diseño y aporte de un método de gestión y modelos tipo de documentación de la vigilancia y el control de la salud de los trabajadores.','V-2 Protocolización. Estudio y Valoración de la Evaluación de Riesgos. Realización Cuestionario de Salud Específico por puesto de trabajo','V-3 Planificación de los Reconocimientos médicos. ','V-4 Elaboración de Historial Clínico-Laboral. Realización de reconocimientos médico-específicos a los trabajadores, según anexo III del presente contrato.','V-5 Entrega Examen de Salud y criterio de aptitud.','V-6 Memoria Anual de las actividades en materia de Vigilancia de la Salud cuando proceda legalmente.','V-7 Estudios Epidemiológico de los resultados de la vigilancia de la salud','V-8  Información sanitaria y colaboración  con el Sistema Público de Salud.','V-9 Declaración de sospecha de Enfermedades Profesionales e investigación de las mismas.','V 10 Valoración de los riesgos que puedan afectar a trabajadoras embarazadas, menores y  trabajadores especialmente sensibles a determinados riesgos','V11.1 Formación primeros auxilios','V-11.2 Promoción de salud en el lugar del trabajo.','V-12  Desplazamiento de unidades móviles para la realización de los reconocimientos médicos más de 25 trabajadores','V-13  Desplazamiento de unidades móviles para la realización de los reconocimientos médicos menos de 25 trabajadores','O-1 Elaboración del Estudio de seguridad y salud o estudio básico de seguridad y salud','O-2  Elaboración del Plan de Seguridad y Salud','O-3 Elaboración de Anexos al Plan de Seguridad y Salud','O-4 Coordinación de Seguridad y salud en fase de proyecto','O-5 Coordinación de Seguridad y salud en fase de ejecución','O-6 Gestión del Libro subcontratación','O-7 Visitas de inspección de seguridad a obras');
	$tablas3=array('SI','SI','SI','SI','NO','NO','SI','SI','NO','NO','SI','SI','NO','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','NO','SI','SI','NO','SI','SI','SI','SI','SI','SI','NO','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','NO','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','NO','SI','SI','SI','SI','SI','SI','SI');
	$tablas4=array('Actividad incluida en el concierto y en el importe del mismo.','Actividad incluida en el concierto y en el importe del mismo.','Actividad incluida en el concierto y en el importe del mismo.','Actividad incluida en el concierto y en el importe del mismo','Realizado por el empresario con recursos propios u otros recursos  ajenos a ANESCO.','Realizado por el empresario con recursos propios u otros recursos  ajenos a ANESCO.','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Realizado por el empresario con recursos propios u otros recursos  ajenos a ANESCO','Realizado por el empresario con recursos propios u otros recursos  ajenos a ANESCO','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Importe no incluido en las condiciones económicas del contrato','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Importe no incluido en las condiciones económicas del contrato','Importe no incluido en las condiciones económicas del contrato','Actividad incluida en el concierto y en el importe del mismo ','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Importe no incluido en las condiciones económicas del contrato','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Importe no incluido en las condiciones económicas del contrato.','Importe no incluido en las condiciones económicas del contrato.','Actividad incluida en el concierto y en el importe del mismo ','Actividad incluida en el concierto y en el importe del mismo','Importe no incluido en las condiciones económicas del contrato','Importe no incluido en las condiciones económicas del contrato','Importe no incluido en las condiciones económicas del contrato','Realizado por el empresario con recursos propios u otros recursos  ajenos a ANESCO','Actividad incluida en el concierto y en el importe del mismo','Importe no incluido en las condiciones económicas del contrato','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Importe no incluido en las condiciones económicas del contrato','Actividad incluida en el concierto y en el importe del mismo','Importe no incluido en las condiciones económicas del contrato','Importe no incluido en las condiciones económicas del contrato','Importe no incluido en las condiciones económicas del contrato','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Importe no incluido en las condiciones económicas del contrato','Importe no incluido en las condiciones económicas del contrato','Importe no incluido en las condiciones económicas del contrato','Actividad incluida en el concierto y en el importe del mismo','Actividad incluida en el concierto y en el importe del mismo','Importe no incluido en las condiciones económicas del contrato','Actividad incluida en el concierto y en el importe ','Actividad incluida en el concierto y en el importe ','Actividad incluida en el concierto y en el importe ','Actividad incluida en el concierto y en el importe ','Actividad incluida en el concierto y en el importe ','Incluido obligatoriamente en concierto','Actividad incluida en el concierto y en el importe ','Actividad incluida en el concierto y en el importe ','Actividad incluida en el concierto y en el importe ','Actividad incluida en el concierto y en el importe ','Importe no incluido en las condiciones económicas del contrato','Actividad incluida en el concierto y en el importe ','Actividad incluida en el concierto y en el importe ','Importe no incluido en las condiciones e del contrato','Importe no incluido en las condiciones económicas del contrato','Importe no incluido en las condiciones económicas del contrato','Importe no incluido en las condiciones económicas del contrato','Importe no incluido en las condiciones económicas del contrato','Importe no incluido en las condiciones económicas del contrato','Importe no incluido en las condiciones económicas del contrato','Importe no incluido en las condiciones económicas del contrato');

	$inicioContrato=formateaFechaInicioContratoPDF($datos['fechaInicio']);

	$especialidadesContrato=obtieneEspecialidadesParaPDF($oferta,$datos);

    $contenido = "
	<style type='text/css'>
	<!--
	        body{
	            font-size:12px;
	            font-family: helvetica;
	            font-weight: lighter;
	            line-height: 24px;
	        }

	        #container{
	            background: url(../img/fondoOferta3.jpeg) center center no-repeat;
	            height:100%;
	        }

	        .logo{
	        	text-align:center;
	        	margin-top:0px;
	        }
	        .logo img{
	        	width:100px;
	        }

	        .tituloDoc{
	        	margin-top:355px;
	        }

	        .tituloDoc h1{
	        	font-size:28px;
	        	text-align:center;
	        	margin-top:-10px;
	        	color:#075581;
	        	line-height:36px;
	        }

	        .tituloTipo{
	        	margin-top:430px;
	        	text-align:right;
	        	margin-right:100px;
	        }

	        .sinBorde{
	        	margin-left:230px;
	        	width:70%;
	        }

	        .sinBorde .td1{
	        	width:15%;
	        }

	        .sinBorde .td2{
	        	width:35%;
	        }

	        .contratante{
	        	width:100%;
	        	border:1px solid #000;
	        	border-collapse:collapse;
	        	font-size:9px;
	        }

	        .contratadas{
	        	text-align:center;
	        }


	        .contratante td{
	        	border:1px solid #000;
	        	padding:5px 2px 3px 2px;
	        	width:12%;
	        	text-align:left;
	        }

	        .contratante .titulo{
	        	background:#919EB1;
	        	font-weigh:bold;
	        }

	        th{
	        	text-align:center;
	        	color:#FFF;
	        	background:#4E6998;
	        	border:1px solid #000;
	        	padding:3px 2px;
	        	width:100%;
	        }

	        .contratante th{
	        	
	        }

	        table .titulo{
	        	background:#ADC0CE;
	        	text-align:center;
	        }

	        .a3{
	        	width:2%;
	        }
	        .a5{
	        	width:5%;
	        }
	        .a61{
	        	width:6.5%;
	        }
	        .a6{
	        	width:10.5%;
	        }
	        .a9{
	        	width:9%;	
	        }
	        .a10{
	        	width:10%;	
	        }
	        .a12{
	        	width:12%;
	        }
	        .a13{
	        	width:12.5%;
	        }
	        .a14{
	        	width:14%;
	        }
	        .a15{
	        	width:15%;
	        }
	        .a17{
	        	width:17%;
	        }
	        .a20{
	        	width:20%;
	        }
	        .a23{
	        	width:23.5%;
	        }
	        .a24{
	        	width:24.5%;
	        }
	        .a26{
	        	width:26%;
	        }
	        .a30{
	        	width:30%;
	        }
	        .a34{
	        	width:34%;
	        }
	        .a35{
	        	width:35.2%;
	        }
	        .a44{
	        	width:34%;
	        }
	        .a50{
	        	width:50%;
	        }
	        .a70{
	        	width:70%;
	        }
	        .a76{
	        	width:76%;
	        }
	        .a70{
	        	width:70%;
	        }
	        .a80{
	        	width:80%;
	        }
	        .a58{
	        	width:58%;
	        }
	        .a28{
	        	width:28%;
	        }

	        .centro{
	        	text-align:center;
	        }

	        .sinBorderLeft{
	        	border-left:0px;
	        }

			.sinBorderLeftBottom{
	        	border-left:0px;
	        	border-bottom:0px;
	        }	

	        .fecha{
	        	margin-top:15px;
	        	text-align:right;
	        }        
	        
	        .texto{
	        	margin-top:5px;
	        	text-align:justify;
	        	font-size:11px
	        }

	        h2{
	        	font-size:12px;
	        	text-align:center;
	        }

	        li{
	        	padding-bottom:10px;
	        }

	        #firmas{
	        	width:70%;
	        	margin-left:90px;
	        	margin-top:100px;
	        }

	        #firmas td{
	        	width:50%;
	        	text-align:center;
	        }

	        .pempresa{
	        	text-align:center;
	        	padding:3px;
	        	border:1px solid #000;
	        	margin-bottom:10px;
	        }

	        #centros{
	        	margin-top:20px;
	        	border:0px;
	        	width:100%;
	        	border-collapse:collapse;
	        	text-align:left;
	        	border-bottom:1px solid #000;
	    	}	

	    	#centros th{
	    		font-weight:bold;
	    		border:0px;
	    		border-bottom:1px solid #000;
	    		background:transparent;
	    		color:#000;
	    		text-align:left;
	    	}

	    	#centros td{
	        	border-bottom:1px solid #000;
	        	padding-bottom:10px;
	        	padding-top:10px;
	    	}	

	    	#centros .a50{
	    		width:50%;
	    	}

	    	#centros .a30{
	    		width:30%;
	    	}

	    	#centros .a20{
	    		width:20%;
	    	}

	    	.pie{
	    		font-size:10px;
	    		margin-left:100px;
	    	}

	    	.datos{
	    		font-size:13px;
	    	}

	    	.a56{
	    		width:56%;
	    	}

	    	.trMarcado td{
	    		background:#BBBBBB;
	    		color:#FFF;
	    	}

	    	.a25{
	    		width:25%
	    	}

	    	.celdaFirma{
	    		height:200px;
	    	}

	    	.observaciones{
	    		width:90%;
	    		border-top:1px solid #000;
	    	}

	    	#centros td.numTrabajadoresCentros{
	    		text-align:center;
	    		vertical-align: middle;
	    	}

	    	#centros .bordeDerecha{
	    		border-right:1px solid #000;
	    	}
		}
	-->
	</style>
	<page>
		<div id='container'>
			<div class='tituloDoc'>
				<h1>CONTRATO DE PRESTACION DE SERVICIOS<br/>
				EXTERNOS<br/>
				DE PREVENCION DE RIESGOS LABORALES<br/><br/><br/>
				".$cliente['EMPNOMBRE']."</h1>
			</div>
			<div class='tituloTipo'>
				CONTRATO: ".$tiposContratos[$oferta['opcion']]."<br/>
				ID CLIENTE: ".$cliente['EMPID']." / Nº DE CONTRATO: ".$referencia."
			</div>
		</div>
	</page>
	<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='fecha'>
		En Sevilla, ".$inicioContrato.".
		</div>
		<div class='texto'>
		De una parte ".$cliente['EMPRL'].", mayor de edad, con D.N.I. ".$cliente['EMPRLDNI'].", y en representación como representante legal en calidad de ".$cliente['EMPRLCARGO']." de ".$cliente['EMPNOMBRE'].", con domicilio social en ".$cliente['EMPDIR'].", y CIF / NIF ".$cliente['EMPCIF'].".<br/><br/>

		Y de otra parte D. Patricio Peñas de Bustillo, mayor de edad, con D.N.I. nº 28759305-J y en representación como administrador de la mercantil ANESCO SALUD Y PREVENCION SL, con domicilio social en C/ Murillo 1, 2º Planta y C.I.F. nº B90072240.<br/><br/>

		Se reconocen mutuamente capacidad legal suficiente para suscribir el presente contrato y a dicho fin y por vía de antecedente.<br/><br/>
		<h2>MANIFIESTAN</h2>
		<ul style='list-style:none;'>
			<li><b>A)</b> Que la Ley 31/95, de 8 de Noviembre de Prevención de Riesgos Laborales, establece los principios generales relativos a la prevención de riesgos profesionales para la protección de la seguridad y la salud de los trabajadores, como obligación expresa que recae en el empresario, desarrollada en el Reglamento de los Servicios de Prevención, aprobado por el Real Decreto 39/1997, de 17 de enero, modificado por  Real Decreto 899/2015, de 9 de octubre y Real Decreto 091/2015 de 9 de octubre, y Orden ESS/2259/2015, de 22 de octubre.</li>

			<li><b>B)</b> ".$cliente['EMPNOMBRE'].", tras analizar las diversas alternativas, y previa consulta con los trabajadores o sus representantes ha optado, entre las modalidades organizativas, establecidas en la mencionada Ley por la contratación externa de actividades preventivas con un servicio de prevención ajeno.</li>

			<li><b>C)</b> Que ANESCO SALUD Y PREVENCION, S.L. cuenta con la acreditación administrativa definitiva para actuar como Servicio de Prevención Ajeno, mediante resolución de la Autoridad Laboral, recaída en expediente nº SP304/13.</li>

			<li><b>D)</b> Que, estando interesado ".$cliente['EMPNOMBRE'].", en concertar el Servicio de Prevención Ajeno con ANESCO SALUD Y PREVENCION, S.L. (en adelante ANESCO) suscriben ambas partes el presente concierto, previamente examinado y discutido en su contenido por ambas partes y en base a las siguientes:</li>

		</ul>
		<h2>ESTIPULACIONES</h2>
		<b>PRIMERA:</b><br/><br/>
		".$cliente['EMPNOMBRE'].", concierta con ANESCO la prestación del Servicio de Prevención Ajeno, para las actividades que se indican en los centros de trabajo incluidos en las condiciones particulares del Anexo I ( condiciones particulares) de este concierto, y en aquellos centros que sean comunicados con posterioridad, para el personal con contrato laboral de ".$cliente['EMPNOMBRE'].", adscrito a dichos centros a la fecha de este contrato, así como todo aquel personal que ".$cliente['EMPNOMBRE'].", contrate laboralmente con posterioridad, siempre y cuando notifique su contratación por escrito a ANESCO, en tiempo y forma. La regla a aplicar para el cómputo del número de trabajadores será la establecida en el artículo 35.3 de la Ley 31/95, de 8 de noviembre.<br/><br/>
Con carácter específico están excluidos las materias o servicios indicados en presente contrato, que serán objeto de una contratación aparte y especial, una vez efectuada la evaluación de riesgos.<br/><br/><br />
		<b>SEGUNDA</b><br/><br/>
		ANESCO proporcionará a ".$cliente['EMPNOMBRE'].",y a sus trabajadores las especificaciones incluidas en las condiciones particulares del <b>Anexo II (especialidades y actividades concertadas)</b>, el asesoramiento y apoyo en lo referente a:
		<ul>
			<li>Diseño, aplicación y coordinación de los planes y programas de actuación preventiva.</li>
			<li>Evaluación de todos los factores de riesgo que puedan afectar a la seguridad y la salud de los trabajadores.</li>
			<li>La determinación técnica de las prioridades en la adopción de las medidas preventivas adecuadas y la vigilancia de su eficacia.</li>
			<li>La planificación de la información y formación de los trabajadores.</li>
			<li>Las obligaciones en materia de coordinación de actividades empresariales según dispone el RD 171/2004.</li>
			<li>La identificación de actividades o procesos que reglamentariamente sean considerados como peligrosos o con riesgos especiales, según lo dispuesto en el artículo 32 bis 1.b de la Ley 31/95.</li>
			<li>Valorar la efectividad de la integración de la prevención de riesgos laborales en el sistema general de la empresa, indicando esta valoración en la memoria anual.</li>
		</ul>
		</div>
		<page_footer>
			<div class='pie'>
			Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
			Ref. ".$referencia."<br/>
			".$cliente['EMPNOMBRE']."<br/>
			</div>
		</page_footer>
	</page>
	<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='texto'>
		Estas serán desarrolladas de acuerdo con los planes y programas definidos por ANESCO y aprobadas por ".$cliente['EMPNOMBRE'].", ajustándose a las condiciones particulares incluidas en el Anexo II (especialidades y actividades concertadas).<br/><br/>

			Quedarán excluidas, salvo concertación expresa:
			<ul>
				<li>las actividades incluidas en el ámbito de aplicación del RD 1627/97, tales como estudios o estudios básicos de seguridad y salud, planes de seguridad y salud y coordinación de seguridad en obra, en cualquier fase.</li>
				<li>La presencia física de personal de ANESCO para realizar la coordinación de actividades empresariales según lo previsto en el RD 171/2004.</li>
				<li>Aquellas actividades realizadas por la empresa con recursos propios o por otros servicios de prevención o ajenos a ANESCO, además de los no incluidos en los anexos.</li>
			</ul>
			<b>TERCERA</b><br/><br/>
			Las condiciones económicas que convienen las partes, por los servicios concertados, son las especificadas en el Anexo III (condiciones económicas y vigencia del contrato) del presente contrato.
Cualquier modificación que supere el 5% del número de trabajadores en plantilla podrá dar lugar a una revisión de las mismas.
Las cantidades pactadas serán revisadas anualmente, de común acuerdo, en función del número de trabajadores, centro de trabajo, actividades y/o cualquier otro nuevo requisito que pudiera devenir de la evaluación de riesgo o del cumplimiento de la normativa de prevención de riesgos laborales.<br/><br/>

ANESCO se reserva en caso de renovación del presente contrato el derecho a incrementar el precio en función de la variación del Índice de Precio al Consumo (IPC).<br/><br/>

En caso de duración plurianual o prórroga del contrato, las condiciones económicas serán revisadas anualmente de común acuerdo entre ".$cliente['EMPNOMBRE'].", y ANESCO.<br/><br/>

La validez del presente contrato y de sus prórrogas queda condicionada a la acreditación del correspondiente justificante de pago.
<br/><br/>
		<b>CUARTA</b><br/><br/>";
if($noRenovar=='NO'){		
	$contenido.="El plazo de vigencia del presente contrato es ".$vigenciaContrato.", siendo prorrogado tácitamente de no existir denuncia expresa por cualquiera de las partes con una antelación de 60 días a la fecha de su vencimiento inicial o cualquiera de sus prórrogas. Para el caso de que se produjera la prórroga del contrato, quedarían excluidas la planificación y evaluación salvo que se hubiera producido una modificación en los procesos productivos de ".$cliente['EMPNOMBRE'].", o en la legislación, siempre que exigiera un nuevo estudio a criterio técnico. <br/><br/>";
}else{
	$contenido.="El plazo de vigencia del presente contrato es ".$vigenciaContrato.".<br/><br/>";	
}

$contenido.="La prestación de servicios estará sujeta a la elaboración de un calendario, de tal modo que hasta tanto no esté implantado o cumplidos los plazos establecidos, ANESCO declinará toda responsabilidad por los hechos acaecidos durante dicho período, atendiendo al estudio previo que debe desarrollar ANESCO y la dedicación del personal de ".$cliente['EMPNOMBRE'].", destinado a ello según el documento de planificación preventiva.<br/><br/>
		<b>QUINTA</b><br/><br/>
		Para llevar a cabo las actuaciones del Servicio de Prevención por parte de ANESCO, ".$cliente['EMPNOMBRE'].", vendrá obligado a:<br/><br/>

Permitir el acceso al centro de trabajo de las personas designadas por ANESCO para la realización de los servicios contratados. ".$cliente['EMPNOMBRE'].", se compromete a facilitar el acceso del personal de ANESCO a sus instalaciones<br/><br/>

Facilitar a ANESCO, con carácter previo al inicio de las actividades contratadas, toda la información relativa a la organización, características y complejidad de los puestos de trabajo, procesos de producción, instalaciones, relación de materias primas, productos químicos utilizados, maquinaria y equipos de trabajo existentes, adjuntando la certificación (marcado CE u homologación al RD 1215/1997), así como la relación de trabajadores y de los puestos de trabajo que ocupen y de las tareas que realicen en dichos puestos.<br/><br/>

".$cliente['EMPNOMBRE'].", informará a ANESCO de los daños a la salud de los trabajadores, los accidentes de trabajo, ya sea muy grave, graves, leves y sin baja incluyendo los de personal de ETT, y las enfermedades profesionales, así como las situaciones conocidas de maternidad o parto reciente, discapacidad física o psíquica, menores y trabajadores especialmente sensibles a determinados riesgos.<br/><br/>

".$cliente['EMPNOMBRE'].", informará a ANESCO sobre la gestión de la coordinación empresarial que realice en sus instalaciones y centros de trabajo. Asimismo, informará sobre los centros de trabajo de terceros donde su personal trabaje habitualmente.<br/><br/>


		</div>
		<page_footer>
			<div class='pie'>
			Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
			Ref. ".$referencia."<br/>
			".$cliente['EMPNOMBRE']."<br/>
			</div>
		</page_footer>
	</page>
	<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='texto'>
		".$cliente['EMPNOMBRE'].", se compromete a comunicar a ANESCO las actividades o funciones realizadas con otros recursos preventivos y /u otras entidades para facilitar la colaboración y coordinación de todos ellos.<br/><br/>

".$cliente['EMPNOMBRE']." se compromete a comunicar a ANESCO cualquier cambio que se produzca en los datos facilitados. Para las anteriores declaraciones se provee junto con el presente contrato un anexo que deberá ir firmado debidamente por representantes de ambos contratantes sin cuyo requisito no surtirá efecto. ANESCO no se hace responsable de las consecuencias.<br/><br/>

Derivadas en el supuesto de inexactitud de los datos suministrados por ".$cliente['EMPNOMBRE'].", , quedando pues exonerada de cualquier responsabilidad y sanción que se imponga a ".$cliente['EMPNOMBRE']."<br/><br/>

".$cliente['EMPNOMBRE'].", reconoce y acepta expresamente que la integración de la prevención, ejecución y puesta en práctica de las recomendaciones efectuadas por el Servicio de Prevención ANESCO es responsabilidad exclusivamente de ".$cliente['EMPNOMBRE'].", y que las fechas reflejadas en la planificación preventiva que ".$cliente['EMPNOMBRE'].", acuerda con ANESCO se entienden como plazos máximos aconsejables, asumiendo ".$cliente['EMPNOMBRE'].", la responsabilidad de que dichas mejoras o recomendaciones deben llevase a cabo lo antes posible.<br/><br/>

".$cliente['EMPNOMBRE'].", firmará la recepción de informes y recomendaciones emitidas por el Servicio de Prevención, ANESCO.<br/><br/>

".$cliente['EMPNOMBRE'].", Llevar a cabo las medidas de vigilancia y control de la salud de los trabajadores a tenor del artículo 22 de la Ley PRL, articulo 196 de la LGSS y siguiendo las recomendaciones de ANESCO. <br/><br/>

".$cliente['EMPNOMBRE'].": declara que la información reflejada en el AnexoI, referida a datos de la empresa, centro de trabajo, actividad y número de trabajadores es fiel reflejo de la situación de la empresa a la fecha de formalización del contrato.<br/><br/>

Cualquier dato, información o documentación de los que tenga conocimiento ANESCO, como consecuencia del desarrollo del presente concierto, será tratado de forma confidencial.<br/><br/>
<b>SEXTA</b><br/><br/>
ANESCO se obliga mediante al presente contrato a asesorar al empresario, a los trabajadores y sus representantes y a los órganos de representación especializadas, en los términos establecidos en la normativa aplicable.<br/><br/>
<b>SEPTIMA</b><br/><br/>
ANESCO se compromete a dedicar anualmente los recursos humanos y materiales necesarios para la realización de las actividades concertadas en el presente contrato.<br/><br/>

ANESCO declara disponer de los medios adecuados y precisos para realizar los servicios de prevención concertados, así como del personal con formación, capacitación y dedicación suficiente para analizar, supervisar y realizar los servicios contratadas con la debida capacitación en función de las actividades contratadas. <br/><br/>

<b>OCTAVA</b><br/><br/>
El presente contrato entrará en vigor en la fecha reflejada en el Anexo III<br/>
Ambas partes acuerdan expresamente que la vigencia del presente contrato queda condicionada al pago por parte de".$cliente['EMPNOMBRE']." de la cantidad acordada en los plazos pactados.<br/><br/>

<b>NOVENA</b><br/><br/>
Serán causas de rescisión de contrato:<br/><br/>

La falta de pago de las condiciones económicas. En concreto el presente contrato quedará resuelto de pleno derecho, sin necesidad de resolución expresa por falta de pago de todo o parte de lo estipulado desde la misma fecha del incumplimiento bastando la comunicación a ".$cliente['EMPNOMBRE']." por cualquier medio (fax, correo electrónico o similar) sin perjuicio de los derechos y acciones que pudieran corresponder por los servicios prestados con anterioridad.<br/><br/>

El incumplimiento por parte de ".$cliente['EMPNOMBRE']." de su deber de colaboración con el Servicio de Prevención, ANESCO y en especial la ocultación y falsedad de datos, por omisión simple o intencionada.<br/><br/>

El incumplimiento grave o reiterado por parte de ".$cliente['EMPNOMBRE']." de los planes y programas de actuación preventiva recomendados por ANESCO.<br/><br/>

La no adopción en plazos razonables de las medidas preventivas requeridas como consecuencia de la evaluación de riesgos o cualquier otro documento emitido por ANESCO


		</div>
		<page_footer>
			<div class='pie'>
				Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
				Ref. ".$referencia."<br/>
				".$cliente['EMPNOMBRE']."<br/>
			</div>
		</page_footer>
	</page>
	<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='texto'>
		".$cliente['EMPNOMBRE']." podrá cancelar el concierto si ANESCO no lleva a cabo las actividades concertadas de conformidad con la legislación vigente.<br/><br/>
		<b>DÉCIMA</b><br/><br/>
		A los efectos de lo establecido en la Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y garantía de los derechos digitales, ".$cliente['EMPNOMBRE']." presta su consentimiento expreso para el tratamiento de los datos de carácter personal de sus empleados que sean suministrados para la prestación de los servicios incluidos en el presente contrato, de conformidad con el artículo 6 de la citada ley, teniendo no obstante el presente documento carácter de contrato y siendo los datos necesarios para el fin a que viene destinado a los efectos del artículo 8, 11 y demás concordantes y confidenciales. A estos efectos, ANESCO dispone de todas las medidas de seguridad recogidas en la legislación vigente para el adecuado tratamiento de dicha información y dispone de la oportuna política de privacidad recogida en <a href='http://anescoprl.es/politica-de-privacidad-y-aviso-legal/'>http://anescoprl.es/politica-de-privacidad-y-aviso-legal/</a> donde se indica, además, de forma clara y sencilla como ejercitar los derechos correspondientes.<br /><br />
		<b>UNDÉCIMA</b><br/><br/>
		Los contratantes se consideran empresarios independientes, por lo que el incumplimiento por parte de ".$cliente['EMPNOMBRE'].",  de las normativas y recomendaciones dadas por ANESCO implicarán la exoneración de responsabilidad de ANESCO en cualquiera de los órdenes jurisdiccionales y administrativos, siendo por tanto de su exclusiva cuenta de ".$cliente['EMPNOMBRE'].",  a la puesta en marcha  y ejecución del Plan de Prevención así como la integración de la prevención y del resto de normativas y pautas de actuación que ANESCO recomiende a ".$cliente['EMPNOMBRE']." en su operativa.<br /><br />
		<b>DUODECIMA</b><br/><br/>
		ANESCO no realizará acciones formativasfinanciadas a través de bonificaciones en las cuotas de la seguridad social (Formación tripartita). Ni gestiona créditos de la misma.<br /><br />
		<b>DECIMOTERCERA</b><br/><br/>
		Ambas partes se comprometen a desarrollar sus obligaciones fielmente y para resolver cualquier diferencia que pudiera surgir de la aplicación del presente contrato, se someten a la jurisdicción de los juzgados y tribunales de Sevilla renunciando al fuero propio que pudiera corresponderle.<br/><br/>

Y en prueba de conformidad con cuanto antecede, firman el presente en duplicado ejemplar y a un solo efecto.
		<table id='firmas'>
			<tr>
				<td>".$cliente['EMPNOMBRE']."</td>
				<td>ANESCO SALUD Y PREVENCIÓN, S.L</td>
			</tr>
			<tr>
				<td>".$imagenFirma."</td>
				<td><img src='../img/firmaAnescoBlanco.png'></td>
			</tr>
			<tr>
				<td>D. ".$cliente['EMPRL']."</td>
				<td>D. PATRICIO PEÑAS DE BUSTILLO</td>
			</tr>
		</table>
		</div>
		<page_footer>
			<div class='pie'>
			Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
			Ref. ".$referencia."<br/>
			".$cliente['EMPNOMBRE']."<br/>
			</div>
		</page_footer>
	</page>
	<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='texto'>
		<h2>ANEXO I<br/>CONDICIONES PARTICULARES AL CONCIERTO DE SERVICIO AJENO DE PREVENCIÓN DE<br/> RIESGOS LABORALES</h2>
		<div class='pempresa'>DATOS DE LA EMPRESA</div>
		EMPRESA: ".$cliente['EMPNOMBRE'].", <br/>
CIF: ".$cliente['EMPCIF']."<br/>
C.N.A.E: ".$cliente['EMPCNAE']."<br/>
Dirección: ".$cliente['EMPDIR']."<br/>
CP: ".$cliente['EMPCP']."<br/>
Localidad: ".$cliente['EMPLOC']."<br/>
Provincia: ".$cliente['EMPPROV']."<br/>
Actividad de la empresa: ".defineActividad($cliente['EMPACTIVIDAD'])."<br/>";
/*Grupo de actividad al que pertenece: ".$cliente['EMPGRUPO']."<br/>
Sector empresarial que engloba: ".$cliente['EMPSECTOR']."<br/>*/
$contenido.="<br/><br/>
<b>CENTROS DE TRABAJO</b>
<table id='centros'>
	<tr>
		<th class='a50'>Centro de trabajo</th>
		<th class='a30'>Actividad</th>
		<th class='a20'>Nº Trabajadores</th>
	</tr>";


	if($centros>0){
		//Hago una lectura adelantada para poner el rowspan en la última celda
		$centro=mysql_fetch_assoc($consultaCentros);
		$contenido.="
		<tr>
			<td class='a50 bordeDerecha'>".formateaDireccionCentro($centro)."</td>
			<td class='a30 numTrabajadoresCentros bordeDerecha' rowspan='".$centros."'>".defineActividad($cliente['EMPACTIVIDAD'])."</td>
			<td class='a20 numTrabajadoresCentros' rowspan='".$centros."'>".$cliente['EMPNTRAB']."</td>
		</tr>";

		while($centro=mysql_fetch_assoc($consultaCentros)){
			$contenido.="
			<tr>
				<td class='a50 bordeDerecha'>".formateaDireccionCentro($centro)."</td>
			</tr>";
		}
	}



$contenido.="</table>
<br/><br/><br/>";

if($cliente['codigo']!=1726){
	$contenido.="
	<b>El cliente declara que la anterior declaración concuerda y es fiel reflejo de la situación de su empresa.<br/><br/>
	TRABAJADORES INCLUIDOS EN EL CONCIERTO<br/></b>
	<table id='centros'>
		<tr>
			<th class='a50'>Nombre</th>
			<th class='a20'>DNI</th>
			<th class='a30'>Puesto</th>
		</tr>";

        $empleados=consultaBD('SELECT * FROM empleados WHERE codigoCliente='.$cliente['codigo'].' AND eliminado="NO";', true);
		
		while($empleado=mysql_fetch_assoc($empleados)){
			$puesto=datosRegistro('puestos_trabajo',$empleado['codigoPuestoTrabajo']);
			$contenido.="<tr>
			<td class='a50'>".$empleado['nombre']." ".$empleado['apellidos']."</td>
			<td class='a20'>".$empleado['dni']."</td>
			<td class='a30'>".$puesto['nombre']."</td>
			</tr>";
		}

	$contenido.="
	</table>";
}

$contenido.="
		</div>
		<page_footer>
			<div class='pie'>
			Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
			Ref. ".$referencia."<br/>
			".$cliente['EMPNOMBRE']."<br/>
			</div>
		</page_footer>
	</page>";
	if($oferta['opcion']==4){
		$contenido.="<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='texto'>
		<h2>ANEXO II<br/>ESPECIALIDADES Y ACTIVIDADES CONCERTADAS</h2>
		".nl2br($oferta['otraOpcion'])."
		</div>
		<page_footer>
			<div class='pie'>
			Contrato de prestación de servicios<br/>
			Ref. ".$referencia."<br/>
			".$cliente['EMPNOMBRE']."<br/>
			</div>
		</page_footer>
		</page>";		
	}elseif($oferta['opcion']==1){
		$contenido.="<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='texto'>
		<h2>ANEXO II<br/>ESPECIALIDADES Y ACTIVIDADES CONCERTADAS</h2>
		<table class='contratante'>
			<tr>
				<th colspan='4'>Actividades de carácter GENERAL a realizar por el Servicio de Prevención Ajeno</th>
			</tr>
			<tr>
				<td class='titulo a20'>Obligación de la empresa</td>
				<td class='titulo a55'>Actividades a desarrollar por el SPA</td>
				<td class='titulo a5'>Incluido<br/>concierto</td>
				<td class='titulo a24'>Observaciones</td>
			</tr>";
			for($i=0,$j=0;$i<15;$i++,$j++){
			$span='';
			if($i==3 || $i==4 || $i==6 || $i==8){
				$span="rowspan='3'";
			}
			if($i==7 || $i==12){
				$span="rowspan='2'";
			}
			$contenido.="<tr>
				<td class='a20' ".$span.">".$tablas1[$i]."</td>	
				<td class='a50'>".$tablas2[$j]."</td>	
				<td class='a61 centro'>".$tablas3[$j]."</td>	
				<td class='a23 centro'>".$tablas4[$j]."</td>
				</tr>";
				if($i==3 || $i==4 || $i==6 || $i==8){
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
				}
				if($i==7 || $i==12){
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
				}
			}
			$contenido.="</table>
		</div>
		<page_footer>
			<div class='pie'>
			Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
			Ref. ".$referencia."<br/>
			".$cliente['EMPNOMBRE']."<br/>
			</div>
		</page_footer>
	</page>
	<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='texto'>
		<table class='contratante'>
			<tr>
				<th colspan='4'>Actividades en la especialidad de SEGURIDAD a realizar por el Servicio de Prevención Ajeno</th>
			</tr>
			<tr>
				<td class='titulo a20'>Obligación de la empresa</td>
				<td class='titulo a55'>Actividades a desarrollar por el SPA</td>
				<td class='titulo a5'>Incluido<br/>concierto</td>
				<td class='titulo a24'>Observaciones</td>
			</tr>";
			for($i,$j;$i<22;$i++,$j++){
			$span='';
			if($i==19){
				$span="rowspan='3'";
			}
			if($i==16 || $i==20){
				$span="rowspan='2'";
			}
			$contenido.="<tr>
				<td class='a20' ".$span.">".$tablas1[$i]."</td>	
				<td class='a50'>".$tablas2[$j]."</td>	
				<td class='a61 centro'>".$tablas3[$j]."</td>	
				<td class='a23 centro'>".$tablas4[$j]."</td>
				</tr>";
				if($i==19){
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
				}
				if($i==16 || $i==20){
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
				}
			}
			$contenido.="</table><br/>
			<table class='contratante'>
			<tr>
				<th colspan='4'>Actividades en la especialidad de HIGIENE INDUSTRIAL a realizar por el Servicio de Prevención Ajeno</th>
			</tr>
			<tr>
				<td class='titulo a20'>Obligación de la empresa</td>
				<td class='titulo a55'>Actividades a desarrollar por el SPA</td>
				<td class='titulo a5'>Incluido<br/>concierto</td>
				<td class='titulo a24'>Observaciones</td>
			</tr>";
			for($i,$j;$i<26;$i++,$j++){
			$span='';
			if($i==24){
				$span="rowspan='3'";
			}
			if($i==22 || $i==23){
				$span="rowspan='2'";
			}
			if($i==25){
				$span="rowspan='4'";
			}
			$contenido.="<tr>
				<td class='a20' ".$span.">".$tablas1[$i]."</td>	
				<td class='a50'>".$tablas2[$j]."</td>	
				<td class='a61 centro'>".$tablas3[$j]."</td>	
				<td class='a23 centro'>".$tablas4[$j]."</td>
				</tr>";
				if($i==24){
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
				}
				if($i==22 || $i==23){
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
				}
				if($i==25){
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
				}
			}
			$contenido.="</table>
		</div>
		<page_footer>
			<div class='pie'>
			Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
			Ref. ".$referencia."<br/>
			".$cliente['EMPNOMBRE']."<br/>
			</div>
		</page_footer>
	</page>
	<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='texto'>
		<table class='contratante'>
			<tr>
				<th colspan='4'>Actividades en la especialidad de ERGONOMÍA Y PSICOSOCIOLOGÍA APLICADA a realizar por el Servicio de Prevención Ajeno</th>
			</tr>
			<tr>
				<td class='titulo a20'>Obligación de la empresa</td>
				<td class='titulo a55'>Actividades a desarrollar por el SPA</td>
				<td class='titulo a5'>Incluido<br/>concierto</td>
				<td class='titulo a24'>Observaciones</td>
			</tr>";
			for($i,$j;$i<32;$i++,$j++){
			$span='';
			if($i==31){
				$span="rowspan='3'";
			}
			if($i==27){
				$span="rowspan='2'";
			}
			$contenido.="<tr>
				<td class='a20' ".$span.">".$tablas1[$i]."</td>	
				<td class='a50'>".$tablas2[$j]."</td>	
				<td class='a61 centro'>".$tablas3[$j]."</td>	
				<td class='a23 centro'>".$tablas4[$j]."</td>
				</tr>";
				if($i==31){
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
				}
				if($i==27){
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
				}
			}
			$contenido.="</table><br/>
			<table class='contratante'>
			<tr>
				<th colspan='4'>Actividades en la especialidad de VIGILANCIA DE LA SALUD a realizar por el Servicio de Prevención Ajeno</th>
			</tr>
			<tr>
				<td class='titulo a20'>Obligación de la empresa</td>
				<td class='titulo a55'>Actividades a desarrollar por el SPA</td>
				<td class='titulo a5'>Incluido<br/>concierto</td>
				<td class='titulo a24'>Observaciones</td>
			</tr>";
			for($i,$j;$i<35;$i++,$j++){
			$span='';
			if($i==33 || $i==34){
				$span="rowspan='2'";
			}
			if($i==32){
				$span="rowspan='10'";
			}
			$contenido.="<tr>
				<td class='a20' ".$span.">".$tablas1[$i]."</td>	
				<td class='a50'>".$tablas2[$j]."</td>	
				<td class='a61 centro'>".$tablas3[$j]."</td>	
				<td class='a23 centro'>".$tablas4[$j]."</td>
				</tr>";
				if($i==33 || $i==34){
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
				}
				if($i==32){
					for($k=0;$k<9;$k++){
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					}
				}
			}
			$contenido.="</table>
		</div>
		<page_footer>
			<div class='pie'>
			Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
			Ref. ".$referencia."<br/>
			".$cliente['EMPNOMBRE']."<br/>
			</div>
		</page_footer>
	</page>
	<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='texto'>
		<table class='contratante'>
			<tr>
				<th colspan='4'>Actividades en centros de trabajo sometidos a la normativa de Seguridad y Salud en Obras de Construcción</th>
			</tr>
			<tr>
				<td class='titulo a20'>Obligación de la empresa</td>
				<td class='titulo a55'>Actividades a desarrollar por el SPA</td>
				<td class='titulo a5'>Incluido<br/>concierto</td>
				<td class='titulo a24'>Observaciones</td>
			</tr>";
			for($i,$j;$i<36;$i++,$j++){
			$span="rowspan='7'";
			$contenido.="<tr>
				<td class='a20' ".$span.">".$tablas1[$i]."</td>	
				<td class='a50'>".$tablas2[$j]."</td>	
				<td class='a61 centro'>".$tablas3[$j]."</td>	
				<td class='a23 centro'>".$tablas4[$j]."</td>
				</tr>";
				for($k=0;$k<6;$k++){
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
				}
			}
			$contenido.="</table>
		</div>
		<page_footer>
			<div class='pie'>
			Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
			Ref. ".$referencia."<br/>
			".$cliente['EMPNOMBRE']."<br/>
			</div>
		</page_footer>
	</page>";
	}elseif($oferta['opcion']==2){
		$contenido.="<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='texto'>
		<h2>ANEXO II<br/>ESPECIALIDADES Y ACTIVIDADES CONCERTADAS</h2>
			<table class='contratante'>
				<tr>
					<th colspan='4'>Actividades en la especialidad de VIGILANCIA DE LA SALUD a realizar por el Servicio de Prevención Ajeno</th>
				</tr>
				<tr>
					<td class='titulo a20'>Obligación de la empresa</td>
					<td class='titulo a55'>Actividades a desarrollar por el SPA</td>
					<td class='titulo a5'>Incluido<br/>concierto</td>
					<td class='titulo a24'>Observaciones</td>
				</tr>";
				for($i=32,$j=56;$i<35;$i++,$j++){
				$span='';
				if($i==33 || $i==34){
					$span="rowspan='2'";
				}
				if($i==32){
					$span="rowspan='10'";
				}
				$contenido.="<tr>
					<td class='a20' ".$span.">".$tablas1[$i]."</td>	
					<td class='a50'>".$tablas2[$j]."</td>	
					<td class='a61 centro'>".$tablas3[$j]."</td>	
					<td class='a23 centro'>".$tablas4[$j]."</td>
					</tr>";
					if($i==33 || $i==34){
						$j++;
						$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
								<td class='a61 centro'>".$tablas3[$j]."</td>	
								<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					}
					if($i==32){
						for($k=0;$k<9;$k++){
						$j++;
						$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
								<td class='a61 centro'>".$tablas3[$j]."</td>	
								<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
						}
					}
				}
		$contenido.="
			</table>
		</div>
		<page_footer>
			<div class='pie'>
			Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
			Ref. ".$referencia."<br/>
			".$cliente['EMPNOMBRE']."<br/>
			</div>
		</page_footer>
		</page>";
	}elseif($oferta['opcion']==3){
		$contenido.="<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='texto'>
		<h2>ANEXO II<br/>ESPECIALIDADES Y ACTIVIDADES CONCERTADAS</h2>
		<table class='contratante'>
			<tr>
				<th colspan='4'>Actividades de carácter GENERAL a realizar por el Servicio de Prevención Ajeno</th>
			</tr>
			<tr>
				<td class='titulo a20'>Obligación de la empresa</td>
				<td class='titulo a55'>Actividades a desarrollar por el SPA</td>
				<td class='titulo a5'>Incluido<br/>concierto</td>
				<td class='titulo a24'>Observaciones</td>
			</tr>";
			for($i=0,$j=0;$i<15;$i++,$j++){
			$span='';
			if($i==3 || $i==4 || $i==6 || $i==8){
				$span="rowspan='3'";
			}
			if($i==7 || $i==12){
				$span="rowspan='2'";
			}
			$contenido.="<tr>
				<td class='a20' ".$span.">".$tablas1[$i]."</td>	
				<td class='a50'>".$tablas2[$j]."</td>	
				<td class='a61 centro'>".$tablas3[$j]."</td>	
				<td class='a23 centro'>".$tablas4[$j]."</td>
				</tr>";
				if($i==3 || $i==4 || $i==6 || $i==8){
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
				}
				if($i==7 || $i==12){
					$j++;
					$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
							<td class='a61 centro'>".$tablas3[$j]."</td>	
							<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
				}
			}
			$contenido.="</table>
		</div>
		<page_footer>
			<div class='pie'>
			Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
			Ref. ".$referencia."<br/>
			".$cliente['EMPNOMBRE']."<br/>
			</div>
		</page_footer>
	</page>
	<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='texto'>";

		$especialidadesTecnicas=explode('&{}&',$oferta['especialidadTecnica']);


		if(in_array('Seguridad en el Trabajo',$especialidadesTecnicas)){
			$contenido.="
			<table class='contratante'>
				<tr>
					<th colspan='4'>Actividades en la especialidad de SEGURIDAD a realizar por el Servicio de Prevención Ajeno</th>
				</tr>
				<tr>
					<td class='titulo a20'>Obligación de la empresa</td>
					<td class='titulo a55'>Actividades a desarrollar por el SPA</td>
					<td class='titulo a5'>Incluido<br/>concierto</td>
					<td class='titulo a24'>Observaciones</td>
				</tr>";

				for($i,$j;$i<22;$i++,$j++){
					$span='';
					if($i==19){
						$span="rowspan='3'";
					}
					if($i==16 || $i==20){
						$span="rowspan='2'";
					}
					
					$contenido.="
					<tr>
						<td class='a20' ".$span.">".$tablas1[$i]."</td>	
						<td class='a50'>".$tablas2[$j]."</td>	
						<td class='a61 centro'>".$tablas3[$j]."</td>	
						<td class='a23 centro'>".$tablas4[$j]."</td>
					</tr>";

					if($i==19){
						$j++;
						$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
								<td class='a61 centro'>".$tablas3[$j]."</td>	
								<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
						$j++;
						$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
								<td class='a61 centro'>".$tablas3[$j]."</td>	
								<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					}
					if($i==16 || $i==20){
						$j++;
						$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
								<td class='a61 centro'>".$tablas3[$j]."</td>	
								<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					}
				}
				$contenido.="
			</table>
			<br/><br/>";
		}
		
		if(in_array('Higiene Industrial',$especialidadesTecnicas)){
			$contenido.="
			<table class='contratante'>
				<tr>
					<th colspan='4'>Actividades en la especialidad de HIGIENE INDUSTRIAL a realizar por el Servicio de Prevención Ajeno</th>
				</tr>
				<tr>
					<td class='titulo a20'>Obligación de la empresa</td>
					<td class='titulo a55'>Actividades a desarrollar por el SPA</td>
					<td class='titulo a5'>Incluido<br/>concierto</td>
					<td class='titulo a24'>Observaciones</td>
				</tr>";
				for($i,$j;$i<26;$i++,$j++){
				$span='';
				if($i==24){
					$span="rowspan='3'";
				}
				if($i==22 || $i==23){
					$span="rowspan='2'";
				}
				if($i==25){
					$span="rowspan='4'";
				}
				$contenido.="<tr>
					<td class='a20' ".$span.">".$tablas1[$i]."</td>	
					<td class='a50'>".$tablas2[$j]."</td>	
					<td class='a61 centro'>".$tablas3[$j]."</td>	
					<td class='a23 centro'>".$tablas4[$j]."</td>
					</tr>";
					if($i==24){
						$j++;
						$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
								<td class='a61 centro'>".$tablas3[$j]."</td>	
								<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
						$j++;
						$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
								<td class='a61 centro'>".$tablas3[$j]."</td>	
								<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					}
					if($i==22 || $i==23){
						$j++;
						$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
								<td class='a61 centro'>".$tablas3[$j]."</td>	
								<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					}
					if($i==25){
						$j++;
						$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
								<td class='a61 centro'>".$tablas3[$j]."</td>	
								<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
						$j++;
						$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
								<td class='a61 centro'>".$tablas3[$j]."</td>	
								<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
						$j++;
						$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
								<td class='a61 centro'>".$tablas3[$j]."</td>	
								<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					}
				}
			$contenido.="
			</table>
			<br/><br/>";
		}

		if(in_array('Ergonomía y Psicosociología',$especialidadesTecnicas)){
			$contenido.="
			<table class='contratante'>
				<tr>
					<th colspan='4'>Actividades en la especialidad de ERGONOMÍA Y PSICOSOCIOLOGÍA APLICADA a realizar por el Servicio de Prevención Ajeno</th>
				</tr>
				<tr>
					<td class='titulo a20'>Obligación de la empresa</td>
					<td class='titulo a55'>Actividades a desarrollar por el SPA</td>
					<td class='titulo a5'>Incluido<br/>concierto</td>
					<td class='titulo a24'>Observaciones</td>
				</tr>";
				for($i,$j;$i<32;$i++,$j++){
				$span='';
				if($i==31){
					$span="rowspan='3'";
				}
				if($i==27){
					$span="rowspan='2'";
				}
				$contenido.="<tr>
					<td class='a20' ".$span.">".$tablas1[$i]."</td>	
					<td class='a50'>".$tablas2[$j]."</td>	
					<td class='a61 centro'>".$tablas3[$j]."</td>	
					<td class='a23 centro'>".$tablas4[$j]."</td>
					</tr>";
					if($i==31){
						$j++;
						$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
								<td class='a61 centro'>".$tablas3[$j]."</td>	
								<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
						$j++;
						$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
								<td class='a61 centro'>".$tablas3[$j]."</td>	
								<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					}
					if($i==27){
						$j++;
						$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
								<td class='a61 centro'>".$tablas3[$j]."</td>	
								<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					}
				}
			$contenido.="
			</table><br/>";
		}

		$contenido.="
			</div>
			<page_footer>
				<div class='pie'>
				Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
				Ref. ".$referencia."<br/>
				".$cliente['EMPNOMBRE']."<br/>
				</div>
			</page_footer>
		</page>";
	
		/*<page footer='page' backleft='10mm' backright='10mm'>
			<div class='logo'>
					<img src='../img/logoOferta.png' />
				</div>
			<div class='texto'>
			<table class='contratante'>
				<tr>
					<th colspan='4'>Actividades en centros de trabajo sometidos a la normativa de Seguridad y Salud en Obras de Construcción</th>
				</tr>
				<tr>
					<td class='titulo a20'>Obligación de la empresa</td>
					<td class='titulo a55'>Actividades a desarrollar por el SPA</td>
					<td class='titulo a5'>Incluido<br/>concierto</td>
					<td class='titulo a24'>Observaciones</td>
				</tr>";
				for($i=35,$j;$i<36;$i++,$j++){
				$span="rowspan='7'";
				$contenido.="<tr>
					<td class='a20' ".$span.">".$tablas1[$i]."</td>	
					<td class='a50'>".$tablas2[$j]."</td>	
					<td class='a61 centro'>".$tablas3[$j]."</td>	
					<td class='a23 centro'>".$tablas4[$j]."</td>
					</tr>";
					for($k=0;$k<6;$k++){
						$j++;
						$contenido.="<tr><td class='a50 sinBorderLeft'>".$tablas2[$j]."</td>	
								<td class='a61 centro'>".$tablas3[$j]."</td>	
								<td class='a23 centro'>".$tablas4[$j]."</td></tr>";
					}
				}
				$contenido.="</table>
			</div>
			<page_footer>
				<div class='pie'>
				Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
				Ref. ".$referencia."<br/>
				".$cliente['EMPNOMBRE']."<br/>
				</div>
			</page_footer>
		</page>";*/
	}

	$contenido.="<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='texto'>
		<h2>ANEXO III<br/>CONDICIONES ECONOMICAS Y VIGENCIA DEL CONTRATO</h2>
		<br/><br/><b>CONDICIONES ECONÓMICAS: Especialidades concertadas. Precio.</b><br/>
		<table class='contratante tablaDatos'>
			<tr>
				<th colspan='6' style='text-align:left;'>Datos de la empresa</th>
			</tr>
			<tr>
				<td class='a30'><b>Empresa:</b></td>
				<td class='a70' colspan='5'>".$cliente['EMPNOMBRE']."</td>
			</tr>
			<tr>
				<td class='a30'><b>CIF:</b></td>
				<td class='a14'>".$cliente['EMPCIF']."</td>
				<td class='a14'><b>CNAE</b></td>
				<td class='a14'>".$cliente['EMPCNAE']."</td>
				<td class='a14'><b>Nº trabjadores</b></td>
				<td class='a14'>".$oferta['numEmpleados']."</td>
			</tr>
			<tr>
				<td class='a30'><b>Periodo de vigencia del contrato:</b></td>
				<td class='a14 colspan='5'>".$vigenciaContrato."</td>
				<td class='a14'>Fecha de inicio:</td>
				<td class='a14'>".formateaFechaWeb($datos['fechaInicio'])."</td>
				<td class='a14'>Fecha de fin:</td>
				<td class='a14'>".formateaFechaWeb($datos['fechaFin'])."</td>
			</tr>
			<tr>
				<td class='a30'><b>Actividad:</b></td>
				<td class='a28' colspan='2'>".defineActividad($cliente['EMPACTIVIDAD'])."</td>
				<td class='a28' colspan='2'><b>Nº Centro de trabajos concertados:</b></td>
				<td class='a14'>".$centros."</td>
			</tr>
		</table>
		<br/><br/>
		<table class='contratante tablaDatos'>
			<tr>
				<th class='a20'>Especialidades</th>
				<th class='a20'>Ofertadas</th>
				<th class='a12'>Concertadas</th>
				<th class='a12'>Importe / €</th>
				<th class='a12'>IVA</th>
				<th class='a12'>TOTAL</th>
				<th class='a12'>FACTURAR</th>
			</tr>
			".$especialidadesContrato['contenido']."
			<tr>
				<td class='a76' colspan='5' style='border-left:0px;border-bottom:0px;'></td>
				<td class='a12'>TOTAL</td>
				<td class='a12'>".formateaNumeroWeb($especialidadesContrato['total'])." €</td>
			</tr>";

			if($oferta['opcion']!=3 && $oferta['opcion']!=4){
				$contenido.="
				</table>
				<table class='contratante tablaDatos' style='margin-top:10px;width:100%;'>
				<tr>
					<td style='width:100%;'>El coste de cada reconocimiento médico adicional  no incluido en contrato será de ".formateaNumeroWeb($oferta['importeRMExtra'])." € (Exento de Impuestos Indirectos).
						El examen médico de salud, incluye analítica básica consistente en hemograma, VSG, bioquímica  y orina completa.
					</td>
				</tr>";
			}
			elseif(trim($datos['observaciones'])!=''){
				$contenido.="
				</table>
				<table class='contratante tablaDatos' style='margin-top:10px;width:100%;'>
				<tr>
					<td style='width:100%;'><div><b>Observaciones: </b><br/>".nl2br($datos['observaciones'])."</div></td>
				</tr>";
			}

		$contenido.="
		</table>
		<br/><br/>";

		if($oferta['opcion']!=3 && $oferta['opcion']!=4){
			$contenido.="
			La Vigilancia de la Salud Individual (Exento de Impuestos Indirectos), se facturará  a mes vencido tras la realización de los reconocimientos médicos al precio referenciado. <br/><br/>";
		}

		$contenido.="
El coste abarca, sólo y exclusivamente las actividades indicadas en el presente concierto. Las actividades requeridas por la empresa, distintas a las indicadas en el presente concierto será motivo de facturación aparte, previa emisión y aprobación por la empresa contratante del presupuesto correspondiente.<br/><br/>

Si como consecuencia de la evaluación de riesgo fuese necesaria, en algún puesto de trabajo, la realización de alguna determinación analítica, ampliación de la existente, consulta con médicos de otras especialidades o exploraciones radiológicas específica para el  riesgo detectado le será comunicada, presupuestada y  objeto de facturación aparte.



		</div>
		<page_footer>
			<div class='pie'>
			Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
			Ref. ".$referencia."<br/>
			".$cliente['EMPNOMBRE']."<br/>
			</div>
		</page_footer>
	</page>
	<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='texto'>
		<br/><br/><b>FORMA DE PAGO</b><br/>
		<br/>
		La forma de pago será mediante: ".$formaPago['forma']."
		";
		if($formaPago['mostrarCuentas']=='SI'){
			$cuenta=defineFormaPago($cliente['EMPIBAN'],$datosCuentas['iban'],$formaPago['forma']);
			$contenido.="en la cuenta:<br/><br/>".$cuenta."<br/><br/>
			El pago se realizará distribuido de la siguiente forma:";
		}

		if($oferta['opcion']==1 || $oferta['opcion']==3){
			$contenido.="
			<br/><br/><b>Especialidades</b><br/><br/>
			<table class='contratante tablaDatos'>
				<tr>
					<th class='a20'>FORMA DE PAGO</th>
					<th class='a25'>PAGO</th>
					<th class='a20'>%</th>
					<th class='a20'>FECHA VENCIMIENTO</th>
					<th class='a10'>IMPORTE</th>
				</tr>";
        	$consulta=consultaBD("SELECT fechaVencimiento, importe 
					  			  FROM vencimientos_facturas INNER JOIN contratos_en_facturas ON vencimientos_facturas.codigoFactura=contratos_en_facturas.codigoFactura
					  			  INNER JOIN facturas ON vencimientos_facturas.codigoFactura=facturas.codigo
					  			  WHERE facturas.eliminado='NO' AND contratos_en_facturas.codigoContrato='".$datos['codigo']."';",true);	
			//$renovaciones=consultaBD('SELECT * FROM contratos_renovaciones WHERE codigoContrato='.$datos['codigo'],true);
        	$total=mysql_num_rows($consulta);
        	if($total!=0){
        		$porcentaje=100/$total;
        	}
			//$total=consultaBD('SELECT COUNT(codigo) AS total FROM contratos_renovaciones WHERE codigoContrato='.$datos['codigo'],true,true);
			$i=1;
			while($renovacion=mysql_fetch_assoc($consulta)){
				$anio=explode('-', $renovacion['fechaVencimiento']);
				$contenido.="<tr>
					<td class='a20' style='text-align:center;'>".$formaPago['forma']."</td>
					<td class='a25' style='text-align:center;'>".$i." de ".$total."</td>
					<td class='a20' style='text-align:center;'>".$porcentaje."</td>
					<td class='a10' style='text-align:center;'>".formateaFechaWeb($renovacion['fechaVencimiento'])." </td>
					<td class='a10' style='text-align:center;'>".$renovacion['importe']." €</td>
				</tr>";
				$i++;
			}
			$contenido.="
			</table>";
		}

		if($oferta['opcion']==1 || $oferta['opcion']==2){
			$contenido.="
			<br/><b>•	Reconocimientos Médicos</b><br/><br/>
			Los reconocimientos  médicos se facturaran y girarán a mes vencido de su realización.
			Para renovaciones sucesivas el precio se revisará según el incremento del IPC del año en curso.";
		}

	$contenido.="
		<br/><br/><br/><br/>
		<table id='firmas'>
			<tr>
				<td>".$imagenFirma."</td>
				<td><img src='../img/firmaAnescoBlanco.png'></td>
			</tr>
			<tr>
				<td>D. ".$cliente['EMPRL']."</td>
				<td>D. PATRICIO PEÑAS DE BUSTILLO</td>
			</tr>
			<tr>
				<td></td>
				<td><br/><br/><br/><br/><br/><br/>En Sevilla, a ".formateaFechaWeb($datos['fechaInicio'])."</td>
			</tr>
		</table>
		
		</div>
		<page_footer>
			<div class='pie'>
			Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
			Ref. ".$referencia."<br/>
			".$cliente['EMPNOMBRE']."<br/>
			</div>
		</page_footer>
	</page>";
	if(in_array($formaPago['codigo'],array(2,8))){
	$contenido.="<page footer='page' backleft='10mm' backright='10mm'>
		<div class='logo'>
				<img src='../img/logoOferta.png' />
			</div>
		<div class='texto'>
		<h2><u>ORDEN DE DOMICILIACION DE ADEUDO DIRECTO SEPA DE EMPRESA A EMPRESA</u></h2>
		<br/><br/>Mediante la firma de este documento de Orden de Domiciliación, usted autoriza a:<br/>
<b>(A)</b> Empresa emisora de recibos a enviar órdenes a su entidad financiera para adeudar en su cuenta.<br/>
<b>(B)</b> A su entidad financiera para poder adeudar los importes correspondientes en su cuenta de adeudo con las órdenes de Empresa emisora de recibos.<br/>
Esta orden de domiciliación está prevista exclusivamente para operaciones de empresa. Usted no tiene derecho a que su entidad le reembolse una vez haya adeudado su cuenta, pero tiene derecho a solicitar a su entidad financiera que no adeude su cuenta hasta la fecha de vencimiento para el cobro.<br/><br/><br/>

		<table class='contratante tablaDatos'>
			<tr>
				<td class='a10'>1.</td>
				<td class='a20'><b>Empresa</b></td>
				<td class='a70'><b>".$cliente['EMPNOMBRE']."</b></td>
			</tr>
			<tr>
				<td class='a10'>2.</td>
				<td class='a20'><b>Dirección</b></td>
				<td class='a70'>".$cliente['EMPDIR']."</td>
			</tr>
			<tr>
				<td class='a10'>3.</td>
				<td class='a20'><b>Código Postal y Ciudad</b></td>
				<td class='a70'>".$cliente['EMPCP']." - ".$cliente['EMPPROV']."</td>
			</tr>
			<tr>
				<td class='a10'>4.</td>
				<td class='a20'><b>País</b></td>
				<td class='a70'>España</td>
			</tr>
			<tr>
				<td class='a10'>5.</td>
				<td class='a20'><b>IBAN</b></td>
				<td class='a70'>".$cliente['EMPIBAN']."</td>
			</tr>
			<tr>
				<td class='a10'>6.</td>
				<td class='a20'><b>Acreedor</b></td>
				<td class='a70'>ANESCO SALUD Y PREVENCION, S.L.</td>
			</tr>
			<tr>
				<td class='a10'>7.</td>
				<td class='a20'><b>Identificador del acreedor</b></td>
				<td class='a70'>B91879692</td>
			</tr>
			<tr>
				<td class='a10'>8.</td>
				<td class='a20'><b>Domicilio</b></td>
				<td class='a70'>Calle Murillo, 1 2º planta</td>
			</tr>
			<tr>
				<td class='a10'>9.</td>
				<td class='a20'><b>Código Postal y Ciudad</b></td>
				<td class='a70'>41001 Sevilla</td>
			</tr>
			<tr>
				<td class='a10'>10.</td>
				<td class='a20'><b>País</b></td>
				<td class='a70'>España</td>
			</tr>
			<tr>
				<td class='a10'></td>
				<td class='a20'><b>FIRMA Y SELLO</b></td>
				<td class='a70'></td>
			</tr>
			<tr>
				<td class='a10'></td>
				<td class='a20'></td>
				<td class='a70 celdaFirma'>".$imagenFirma."</td>
			</tr>

		</table>
		</div>
		<page_footer>
			<div class='pie'>
			Contrato de prestación de servicios: ".$tiposContratos[$oferta['opcion']]."<br/>
			Ref. ".$referencia."<br/>
			".$cliente['EMPNOMBRE']."<br/>
			</div>
		</page_footer>
	</page>";
	}

	return $contenido;
}

function formateaDireccionCentro($centro){
	$res=$centro['direccion'].'. '.$centro['cp'].' '.$centro['localidad'];
	
	if(strtolower($centro['localidad'])!=strtolower($centro['provincia'])){
		$res.=', '.$centro['provincia'];
	}

	$res.='.';

	return $res;
}


function obtieneImagenFirmaConsentimiento($firma){
	$res='';

	if(trim($firma)!=''){
		$imagenFirma=sigJsonToImage($firma, array('imageSize'=>array(1000, 500),'drawMultiplier'=>5));
		imagepng($imagenFirma,'../documentos/firmaContrato.png');

		$res="<img style='width:250px;height:125px;' src='../documentos/firmaContrato.png' />";
	}

	return $res;
}

function formateaFechaInicioContratoPDF($fechaInicio){
	$fecha=explode('-',$fechaInicio);
	$meses=array(''=>'','01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');

	return $fecha[2].' de '.$meses[$fecha[1]].' de '.$fecha[0];
}


function obtieneDatosClientePDF($codigoCliente){
	$res=datosRegistro('clientes',$codigoCliente);

	foreach($res as $campo => $valor) {
		$res[$campo]=convierteMayusculas($valor);
	}

	return $res;
}


function convierteMayusculas($cadena){
	$res=strtoupper($cadena);

	$res=str_replace('á','Á',$res);
	$res=str_replace('é','É',$res);
	$res=str_replace('í','Í',$res);
	$res=str_replace('ó','Ó',$res);
	$res=str_replace('ú','Ú',$res);
	$res=str_replace('ñ','Ñ',$res);

	return $res;
}


function obtieneEspecialidadesParaPDF($oferta,$datos){
	$res=array('contenido'=>'','total'=>'');

	if($oferta['opcion']==1){
		$res['contenido']="
			<tr>
				<td class='a20' rowspan='5'>Especialidades<br/>Técnicas</td>
				<td class='a20'>Seguridad (ST)</td>
				<td class='a12'>Incluido</td>
				<td class='a12' rowspan='5'>".formateaNumeroWeb(compruebaImporteContrato($oferta['subtotal'],$datos,false))." €</td>
				<td class='a12' rowspan='5'>".formateaNumeroWeb(compruebaImporteContrato($oferta['importe_iva'],$datos,false))." €</td>
				<td class='a12' rowspan='5'>".formateaNumeroWeb(compruebaImporteContrato($oferta['subtotal'],$datos,false)+compruebaImporteContrato($oferta['importe_iva'],$datos,false))." €</td>
				<td class='a12' rowspan='5'>".formateaNumeroWeb(compruebaImporteContrato($oferta['subtotal'],$datos,false)+compruebaImporteContrato($oferta['importe_iva'],$datos,false))." €</td>
			</tr>
			<tr>
				<td class='a20 sinBorderLeft'>Higiene Industrial (H)</td>
				<td class='a12'>Incluido</td>
			</tr>
			<tr>
				<td class='a20 sinBorderLeft'>Ergonomía (E)</td>
				<td class='a12'>Incluido</td>
			</tr>
			<tr>
				<td class='a20 sinBorderLeft'>Psicología aplicada (PA)</td>
				<td class='a12'>Incluido</td>
			</tr>
			<tr>
				<td class='a20 sinBorderLeft'>Vigilancia Salud Colectiva</td>
				<td class='a12'>Incluido</td>
			</tr>
			<tr>
				<td class='a20'>Vig. Salud<br/>Individual</td>
				<td class='a20'>Reconocimiento médico</td>
				<td class='a12'>".$oferta['numRM']."</td>
				<td class='a12'>".formateaNumeroWeb(compruebaImporteContrato($oferta['importeRM'],$datos,false))." €/RM</td>
				<td class='a12'>n/a</td>
				<td class='a12'>".formateaNumeroWeb(compruebaImporteContrato($oferta['subtotalRM'],$datos,false))."</td>
				<td class='a12'>".formateaNumeroWeb(compruebaImporteContrato($oferta['subtotalRM'],$datos,false))."</td>
			</tr>";
		$res['total']=compruebaImporteContrato($oferta['subtotal'],$datos,false)+compruebaImporteContrato($oferta['importe_iva'],$datos,false)+compruebaImporteContrato($oferta['subtotalRM'],$datos,false);
	}
	elseif($oferta['opcion']==2 && $oferta['numRM']!=0){
		$subtotal2=compruebaImporteContrato($oferta['subtotal'],$datos,false);
		$total2=compruebaImporteContrato($oferta['total'],$datos,false);
		$iva2=$total2-$subtotal2;
		
		//Nueva parte de extracción del IVA de la Vigilancia de la Salud
		$baseImponibleVS=$subtotal2/1.21;
		$ivaVS=$baseImponibleVS*0.21;
		//Fin nueva parte de extracción del IVA de la Vigilancia de la Salud

		$res['contenido']="
			<tr>
				<td class='a20' rowspan='2'>Vig. Salud<br/>Individual</td>
				<td class='a20 sinBorderLeft'>Vigilancia Salud Colectiva</td>
				<td class='a12'>Incluido</td>
				<td class='a12'>".formateaNumeroWeb($baseImponibleVS)." €</td>
				<td class='a12'>".formateaNumeroWeb($ivaVS)." €</td>
				<td class='a12' rowspan='2'>".formateaNumeroWeb($total2)."</td>
				<td class='a12' rowspan='2'>".formateaNumeroWeb($total2)."</td>
			</tr>
			<tr>
				<td class='a20'>Reconocimiento médico</td>
				<td class='a12'>".$oferta['numRM']."</td>
				<td class='a12'>".formateaNumeroWeb($iva2)."</td>
				<td class='a12'> N/A</td>
			</tr>";
		$res['total']=$total2;
	}
	elseif($oferta['opcion']==2){//Cuando los contratos de VS no tienen recos incluidos en contrato, se procesan como un caso especial (ver contrato 2097)
		$subtotal2=compruebaImporteContrato($oferta['subtotal'],$datos,false);
		$total2=compruebaImporteContrato($oferta['total'],$datos,false);
		$iva2=$total2-$subtotal2;

		$res['contenido']="
			<tr>
				<td class='a20' rowspan='2'>Vig. Salud<br/>Individual</td>
				<td class='a20 sinBorderLeft'>Vigilancia Salud Colectiva</td>
				<td class='a12'>Incluido</td>
				<td class='a12'>".formateaNumeroWeb($subtotal2)." €</td>
				<td class='a12'>".formateaNumeroWeb($iva2)." €</td>
				<td class='a12' rowspan='2'>".formateaNumeroWeb($total2)."</td>
				<td class='a12' rowspan='2'>".formateaNumeroWeb($total2)."</td>
			</tr>
			<tr>
				<td class='a20'>Reconocimiento médico</td>
				<td class='a12'>".$oferta['numRM']."</td>
				<td class='a12'>".formateaNumeroWeb(compruebaImporteContrato($oferta['subtotalRM'],$datos,false))."</td>
				<td class='a12'> N/A</td>
			</tr>";
		$res['total']=$total2;
	}
	elseif($oferta['opcion']==3){
		$subtotal1=compruebaImporteContrato($oferta['subtotal'],$datos,false);
		$total1=compruebaImporteContrato($oferta['total'],$datos,false);
		$iva1=$total1-$subtotal1;
		
		$especialidadesTecnicas=explode('&{}&',$oferta['especialidadTecnica']);

		if(
			in_array('Seguridad en el Trabajo',$especialidadesTecnicas) &&
			in_array('Higiene Industrial',$especialidadesTecnicas) &&
			in_array('Ergonomía y Psicosociología',$especialidadesTecnicas)
		){
			$res['contenido'].="
				<tr>
					<td class='a20' rowspan='4'>Especialidades<br/>Técnicas</td>
					<td class='a20'>Seguridad (ST)</td>
					<td class='a12'>Incluido</td>
					<td class='a12' rowspan='4'>".formateaNumeroWeb($subtotal1)." €</td>
					<td class='a12' rowspan='4'>".formateaNumeroWeb($iva1)." €</td>
					<td class='a12' rowspan='4'>".formateaNumeroWeb($total1)." €</td>
					<td class='a12' rowspan='4'>".formateaNumeroWeb($total1)." €</td>
				</tr>
				<tr>
					<td class='a20 sinBorderLeft'>Higiene Industrial (H)</td>
					<td class='a12'>Incluido</td>
				</tr>
				<tr>
					<td class='a20 sinBorderLeft'>Ergonomía (E)</td>
					<td class='a12'>Incluido</td>
				</tr>
				<tr>
					<td class='a20 sinBorderLeft'>Psicología aplicada (PA)</td>
					<td class='a12'>Incluido</td>
				</tr>";
				
			$res['total']=$res['total']+$total1;
		}
		elseif(
			in_array('Seguridad en el Trabajo',$especialidadesTecnicas) &&
			in_array('Higiene Industrial',$especialidadesTecnicas)
		){
			$res['contenido'].="
				<tr>
					<td class='a20' rowspan='2'>Especialidades<br/>Técnicas</td>
					<td class='a20'>Seguridad (ST)</td>
					<td class='a12'>Incluido</td>
					<td class='a12' rowspan='2'>".formateaNumeroWeb($subtotal1)." €</td>
					<td class='a12' rowspan='2'>".formateaNumeroWeb($iva1)." €</td>
					<td class='a12' rowspan='2'>".formateaNumeroWeb($total1)." €</td>
					<td class='a12' rowspan='2'>".formateaNumeroWeb($total1)." €</td>
				</tr>
				<tr>
					<td class='a20 sinBorderLeft'>Higiene Industrial (H)</td>
					<td class='a12'>Incluido</td>
				</tr>";
				
			$res['total']=$res['total']+$total1;
		}
		elseif(
			in_array('Seguridad en el Trabajo',$especialidadesTecnicas) &&
			in_array('Ergonomía y Psicosociología',$especialidadesTecnicas)
		){
			$res['contenido'].="
				<tr>
					<td class='a20' rowspan='3'>Especialidades<br/>Técnicas</td>
					<td class='a20'>Seguridad (ST)</td>
					<td class='a12'>Incluido</td>
					<td class='a12' rowspan='3'>".formateaNumeroWeb($subtotal1)." €</td>
					<td class='a12' rowspan='3'>".formateaNumeroWeb($iva1)." €</td>
					<td class='a12' rowspan='3'>".formateaNumeroWeb($total1)." €</td>
					<td class='a12' rowspan='3'>".formateaNumeroWeb($total1)." €</td>
				</tr>
				<tr>
					<td class='a20 sinBorderLeft'>Ergonomía (E)</td>
					<td class='a12'>Incluido</td>
				</tr>
				<tr>
					<td class='a20 sinBorderLeft'>Psicología aplicada (PA)</td>
					<td class='a12'>Incluido</td>
				</tr>";
				
			$res['total']=$res['total']+$total1;
		}
		elseif(
			in_array('Higiene Industrial',$especialidadesTecnicas) &&
			in_array('Ergonomía y Psicosociología',$especialidadesTecnicas)
		){
			$res['contenido'].="
				<tr>
					<td class='a20' rowspan='3'>Especialidades<br/>Técnicas</td>
					<td class='a20 sinBorderLeft'>Higiene Industrial (H)</td>
					<td class='a12'>Incluido</td>
					<td class='a12' rowspan='3'>".formateaNumeroWeb($subtotal1)." €</td>
					<td class='a12' rowspan='3'>".formateaNumeroWeb($iva1)." €</td>
					<td class='a12' rowspan='3'>".formateaNumeroWeb($total1)." €</td>
					<td class='a12' rowspan='3'>".formateaNumeroWeb($total1)." €</td>
				</tr>
				<tr>
					<td class='a20 sinBorderLeft'>Ergonomía (E)</td>
					<td class='a12'>Incluido</td>
				</tr>
				<tr>
					<td class='a20 sinBorderLeft'>Psicología aplicada (PA)</td>
					<td class='a12'>Incluido</td>
				</tr>";
				
			$res['total']=$res['total']+$total1;
		}
		elseif(in_array('Seguridad en el Trabajo',$especialidadesTecnicas)){
			$res['contenido'].="
				<tr>
					<td class='a20'>Especialidades<br/>Técnicas</td>
					<td class='a20'>Seguridad (ST)</td>
					<td class='a12'>Incluido</td>
					<td class='a12' row='4'>".formateaNumeroWeb($subtotal1)." €</td>
					<td class='a12'>".formateaNumeroWeb($iva1)." €</td>
					<td class='a12'>".formateaNumeroWeb($total1)." €</td>
					<td class='a12'>".formateaNumeroWeb($total1)." €</td>
				</tr>";
			$res['total']=$res['total']+$total1;
		}
		elseif(in_array('Higiene Industrial',$especialidadesTecnicas)){
			$res['contenido'].="
			<tr>
				<td class='a20'>Especialidades<br/>Técnicas</td>
				<td class='a20 sinBorderLeft'>Higiene Industrial (H)</td>
				<td class='a12'>Incluido</td>
				<td class='a12'>".formateaNumeroWeb($subtotal1)." €</td>
				<td class='a12'>".formateaNumeroWeb($iva1)." €</td>
				<td class='a12'>".formateaNumeroWeb($total1)." €</td>
				<td class='a12'>".formateaNumeroWeb($total1)." €</td>
			</tr>";
			$res['total']=$res['total']+$total1;
		}
		elseif(in_array('Ergonomía y Psicosociología',$especialidadesTecnicas)){
			$res['contenido'].="
			<tr>
				<td class='a20' rowspan='2'>Especialidades<br/>Técnicas</td>
				<td class='a20 sinBorderLeft'>Ergonomía (E)</td>
				<td class='a12'>Incluido</td>
				<td class='a12' rowspan='2'>".formateaNumeroWeb($subtotal1)." €</td>
				<td class='a12' rowspan='2'>".formateaNumeroWeb($iva1)." €</td>
				<td class='a12' rowspan='2'>".formateaNumeroWeb($total1)." €</td>
				<td class='a12' rowspan='2'>".formateaNumeroWeb($total1)." €</td>
			</tr>
			<tr>
				<td class='a20 sinBorderLeft'>Psicología aplicada (PA)</td>
				<td class='a12'>Incluido</td>
			</tr>";
			$res['total']=$res['total']+$total1;
		}
		
	}


	return $res;
}

function listadoContratos($objPHPExcel,$aprobados){
	if($aprobados=='SI'){
		$ejercicio=obtieneEjercicioParaWhere();
		$where='WHERE (YEAR(contratos.fechaInicio)="'.$ejercicio.'" OR YEAR(contratos_renovaciones.fecha)="'.$ejercicio.'") AND facturaCobrada="SI"';
	}
	else{
		$where=defineWhereEjercicio('WHERE (facturaCobrada IS NULL OR facturaCobrada="NO")','contratos.fechaInicio');
	}

	conexionBD();

	$consulta=consultaBD("SELECT contratos.*
						  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
						  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
						  LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
						  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
						  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura='PROFORMA'
						  ".$where." AND contratos.eliminado='NO'
						  GROUP BY contratos.codigo 
						  ORDER BY contratos.codigo;");

	$i=4;
	while($datos=mysql_fetch_assoc($consulta)){
		$oferta=$datos['codigoOferta'];
		$cliente=consultaBD('SELECT clientes.*, total FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo='.$oferta,false,true);		

		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($datos['codigo']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($cliente['EMPNOMBRE']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($datos['codigoOferta']);		
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue($datos['fechaInicio']);
		$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue($datos['fechaFin']);
		$i++;
	}
	foreach(range('B','F') as $columnID) {
    	$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
	}

	cierraBD();
}


function creaFacturaProforma($codigoContrato){
	$datos=arrayFormulario();
	
	conexionBD();

	if(isset($datos['vencimientosOfacturas2'])){//2 facturas por contrato, PT+RM
		
		$res=procesaEmisionFacturaProforma($codigoContrato,$datos,'PT');
		$res=$res && procesaEmisionFacturaProforma($codigoContrato,$datos,'RM');
	}
	else{//Proceso normal con 1 factura
		
		$res=procesaEmisionFacturaProforma($codigoContrato,$datos,'CONJUNTO');
	}

	cierraBD();

	return $res;
}


function procesaEmisionFacturaProforma($codigoContrato,$datos,$tipo){
	$res=true;



	$contrato=consultaBD("SELECT ofertas.importe_iva,contratos.codigo, contratos.codigoInterno, ofertas.total, contratos.fechaInicio, clientes.EMPCP, clientes.EMPID, ofertas.subtotal, 
						  ofertas.subtotalRM, incrementoIPC, incrementoIpcAplicado, ofertas.codigoCliente, contratos.codigoFormaPago, ofertas.opcion, ofertas.iva
					  	  FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
					  	  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
					  	  WHERE contratos.codigo='$codigoContrato';",false,true);

	$numeroContrato=formateaReferenciaContrato($contrato,$contrato);

	/// Siempre se le aplica el nuevo metodo de IPC
	$contrato['subtotal'] 		= compruebaImporteContrato($contrato['subtotal'],$datos,false);
	$contrato['importe_iva'] 	= compruebaImporteContrato($contrato['importe_iva'],$datos,false);
	$contrato['subtotalRM'] 	= compruebaImporteContrato($contrato['subtotalRM'], $datos, false);
	$contrato['total']          = $contrato['subtotal'] + $contrato['importe_iva'];

	if($tipo=='CONJUNTO'){
		$concepto='Nº: '.$numeroContrato.'. &nbsp; RRMM:  '.formateaNumeroWeb($contrato['subtotalRM']).' €. &nbsp; Actividades: '.formateaNumeroWeb($contrato['subtotal']).' €. &nbsp; Total: '.formateaNumeroWeb($contrato['total']).' €';
		$indiceTipoEmision='vencimientosOfacturas';
		$sufijoCampos='';
	}
	elseif($tipo=='PT'){
		$concepto='Nº: '.$numeroContrato.'. &nbsp; Total: '.formateaNumeroWeb($contrato['subtotal']).' €';
		$indiceTipoEmision='vencimientosOfacturas';
		$sufijoCampos='';
	}
	else{
		$concepto='Nº: '.$numeroContrato.'. &nbsp; Total:  '.formateaNumeroWeb($contrato['subtotalRM']).' €';
		$indiceTipoEmision='vencimientosOfacturas2';
		$sufijoCampos='RM';
	}
	
	$tipoEmision=$datos[$indiceTipoEmision];

	if($tipoEmision=='VENCIMIENTOS'){
		
		$res=$res && insertaFacturaProforma($contrato,$datos,$concepto,$sufijoCampos);

	}
	elseif($tipoEmision=='FACTURAS'){
		
		for($i=0;isset($datos['fechaVencimiento'.$i]);$i++){
			$conceptoFacturaActual=$concepto.'. Factura '.($i+1).'.';
			$res=$res && insertaFacturaProforma($contrato,$datos,$conceptoFacturaActual,$sufijoCampos,true,$i);
		}
		
	}

	return $res;
}


function insertaFacturaProforma($contrato,$datos,$concepto,$sufijoCampos,$variasFacturas=false,$facturaActual=false){
	$res=true;

	$codigoContrato=$contrato['codigo'];
	$codigoCliente=$contrato['codigoCliente'];
	
	if($variasFacturas && $sufijoCampos==''){//Varias facturas con tratamiento normal o PT
		$total=tofloat($datos['importeVencimiento'.$sufijoCampos.$facturaActual]);
		$baseImponible=$total/1.21;
		$fecha=$datos['fechaVencimiento'.$sufijoCampos.$facturaActual];
	}
	else if($variasFacturas && $sufijoCampos=='RM'){//Varias facturas con tratamiento de RM
		$total=tofloat($datos['importeVencimiento'.$sufijoCampos.$facturaActual]);
		$baseImponible=$total;
		$fecha=$datos['fechaVencimiento'.$sufijoCampos.$facturaActual];
	}
	elseif(!$variasFacturas && $sufijoCampos=='' && $contrato['subtotal']>0){//Una sola factura con tratamiento normal o PT
		$baseImponible=$contrato['subtotal'];
		$total=$contrato['total'];
		$fecha=fechaBD();
	}
	else{//Una sola factura con tratamiento de RM
		$baseImponible=$contrato['subtotalRM'];
		$total=$baseImponible;//Las facturas de RM no llevan IVA!
		$fecha=fechaBD();
	}

	$codigoFormaPago=$contrato['codigoFormaPago'];
	
	if($codigoFormaPago==NULL){
		$codigoFormaPago='NULL';
	}

	// if($contrato['incrementoIPC']=='SI'){
	// 	$ipc=$contrato['incrementoIpcAplicado'];
	// 	$baseImponible=$baseImponible*(1+$ipc/100);
	// 	$total=$total*(1+$ipc/100);
	// }
	
	if($sufijoCampos=='RM' || ($contrato['subtotal']==0 && $contrato['subtotalRM']>0) || ($contrato['opcion']==4 && $contrato['iva']=='NO')){
		$serie=consultaBD('SELECT * FROM series_facturas WHERE serie="RM"',false,true);
	} 
	else {
		$serie=consultaBD('SELECT * FROM series_facturas WHERE serie="PT"',false,true);
	}


	if($total>0){//NUEVO: si la factura es de 0 €, no debe emitirse factura, si no directamente la planificación técnica, en caso de que lleve asociada
		
		$numero=consultaNumeroSerieFactura2($serie['codigo'],false);

		$res=consultaBD("INSERT INTO facturas(codigo,fecha,codigoCliente,tipoFactura,baseImponible,total,activo,eliminado,codigoUsuarioFactura,codigoSerieFactura,numero)
					 	 VALUES(NULL,'$fecha','$codigoCliente','PROFORMA','$baseImponible','$total','SI','NO',".$_SESSION['codigoU'].",".$serie['codigo'].",$numero);");

		$listado=array();
		if($res){
			$codigoProforma=mysql_insert_id();
			array_push($listado,$codigoProforma);

			$res=$res && consultaBD("INSERT INTO contratos_en_facturas VALUES(NULL,'$codigoContrato','$codigoProforma');");

			if($variasFacturas){

				$res=$res && consultaBD("INSERT INTO vencimientos_facturas(codigo,codigoFactura,codigoFormaPago,fechaVencimiento,importe,concepto,codigoEstadoRecibo) VALUES(NULL,'$codigoProforma',$codigoFormaPago,'$fecha','$total','$concepto',4);");

			}
			else{

				for($i=0;isset($datos['fechaVencimiento'.$sufijoCampos.$i]);$i++){
					$fechaVencimiento=$datos['fechaVencimiento'.$sufijoCampos.$i];
					$importe=$datos['importeVencimiento'.$sufijoCampos.$i];
					$importe=tofloat($importe);
					$res=$res && consultaBD("INSERT INTO vencimientos_facturas(codigo,codigoFactura,codigoFormaPago,fechaVencimiento,importe,concepto,codigoEstadoRecibo) VALUES(NULL,'$codigoProforma',$codigoFormaPago,'$fechaVencimiento','$importe','$concepto',4);");
				}

			}
			
		}
		
		if($datos['enviarMail']=='SI'){
			$res=$res && enviaEmailFacturaProforma($listado);
		}

	}
	else{//Inserción de la planificación que NO llevará asociada ninguna factura
		$res=creaPlanificacionExcepcionContrato($codigoContrato,false);//Importante! Usar esta función en lugar de directamente insertaPlanificacionContrato() para no crear planificaciones repetidas

	}

	return $res;
}

function creaTablaFechasVencimiento($datos,$codigoForma,$datosOferta,$rm=false){
	$formaPago=datosRegistro('formas_pago',$codigoForma);
	$i=0;

	$selectorCaja='cajaVencimientos';
	$selectorTabla='tablaVencimientos';
	$sufijoVencimientos='';
	$selectorTextoImporte='textoImporte';

	$serie='PT';
	if(($datosOferta['opcion']==4 && $datosOferta['iva']=='NO') || $rm){//Si la factura es de Otras actuaciones o de RM en el caso de las 4 especialidadess, debe consultarse la serie RM
		$serie='RM';
	}

	if($rm){//Lo pongo aparte del if de arriba porque en los casos de la primera condición el selector debe ser sin el 2
		$selectorCaja.='2';
		$selectorTabla.='2';
		$sufijoVencimientos='RM';
		$selectorTextoImporte.='2';
	}

    echo "
    <div class='control-group hide' id='$selectorCaja'>
	    <label class='control-label'>Vencimientos:</label>
	    <div class='controls'>
    		<table class='table table-striped table-bordered mitadAncho' id='$selectorTabla'>
		        <thead>
		            <tr>
		            	<th> Fecha </th>
		            	<th> Pago </th>
		            	<th id='$selectorTextoImporte'> Importe </th>
		            </tr>
		        </thead>
		        <tbody>";

		        if($datos){
		        	$consulta=consultaBD("SELECT fechaVencimiento, vencimientos_facturas.importe 
		        						  FROM vencimientos_facturas INNER JOIN contratos_en_facturas ON vencimientos_facturas.codigoFactura=contratos_en_facturas.codigoFactura
		        						  INNER JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo
		        						  LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
		        						  WHERE facturas.eliminado='NO' AND serie='$serie' AND contratos_en_facturas.codigoContrato='".$datos['codigo']."';",true);
		        	
		        	if(mysql_num_rows($consulta)>$formaPago['numPagos']){
		        		$formaPago['numPagos']=mysql_num_rows($consulta);
		        	}

		        	while($vencimiento=mysql_fetch_assoc($consulta)){
		        		echo obtieneFilaTablaFechasVencimiento($vencimiento,$i,false,0,$formaPago['numPagos'],$sufijoVencimientos);
		        		$i++;
		        	}
		        }

		        for($i;$i<$formaPago['numPagos'];$i++){
		        	echo obtieneFilaTablaFechasVencimiento(false,$i,false,0,$formaPago['numPagos'],$sufijoVencimientos);
		        }

    echo "			<tr>
    					<td>Total:</td>
    					<td></td>
    					<td><div class='pagination-right' id='totalVencimiento$sufijoVencimientos'></div></td>
    				</tr>
    			</tbody>
		    </table>
		</div>
	</div>";
}

function obtieneFilaTablaFechasVencimiento($datos,$i,$importe,$importeRM,$numeroPagos,$sufijoVencimientos){
	$j=$i+1;
	
	if(!$datos){
		
		if($importeRM>0){
			$importe=$importe + $importeRM;
		}

		$res="			<tr>
							<td><input type='text' name='fechaVencimiento$sufijoVencimientos$i' id='fechaVencimiento$sufijoVencimientos$i' value='".formateaFechaWeb($datos['fechaVencimiento'])."' class='input-small datepicker hasDatepicker' /></td>
							<td><input type='text' name='pago$sufijoVencimientos$i' id='pago$sufijoVencimientos$i' value='$j de $numeroPagos' class='input-mini' readonly /></td>						
							<td>
								<div class='input-prepend input-append nowrap'>
						          <input type='text' class='input-mini pagination-right importeVencimiento$sufijoVencimientos' id='importeVencimiento$sufijoVencimientos$i' name='importeVencimiento$sufijoVencimientos$i' value='".formateaNumeroWeb($importe)."' />
						          <span class='add-on'> €</span>
						        </div>
							</td>
						</tr>";
	}
	else{
		
		if($importeRM>0){
			$importe=$datos['importe'] + $importeRM;
		} 
		else {
			$importe=$datos['importe'];
		}

		$res="			<tr>
							<td><input type='text' name='fechaVencimiento$sufijoVencimientos$i' id='fechaVencimiento$sufijoVencimientos$i' value='".formateaFechaWeb($datos['fechaVencimiento'])."' class='input-small datepicker hasDatepicker' /></td>
							<td><input type='text' name='pago$sufijoVencimientos$i' id='pago$sufijoVencimientos$i' value='$j de $numeroPagos' class='input-mini' readonly /></td>						
							<td>
								<div class='input-prepend input-append nowrap'>
						          <input type='text' class='input-mini pagination-right importeVencimiento$sufijoVencimientos' id='importeVencimiento$sufijoVencimientos$i' name='importeVencimiento$sufijoVencimientos$i' value='".formateaNumeroWeb($importe)."' />
						          <span class='add-on'> €</span>
						        </div>
							</td>
						</tr>";
	}

	return $res;
}

function obtieneVencimientosFormaPago(){
	$res='';
	$datos=arrayFormulario();

	$formaPago=consultaBD("SELECT numPagos FROM formas_pago WHERE codigo=".$datos['codigoFormaPago'],true,true);
	
	$importeTotal=$datos['importe'];
	$importe=tofloat($importeTotal)/$formaPago['numPagos'];
	
	$importeTotalRM=0;
	$importeRM=0;
	
	if($datos['importeRM']!=''){
		$importeTotalRM=$datos['importeRM'];
		$importeRM=tofloat($importeTotalRM)/$formaPago['numPagos'];
	}


	for($i=0;$i<$formaPago['numPagos'];$i++){
		//echo $i;
		$res.=obtieneFilaTablaFechasVencimiento(false,$i,$importe,$importeRM,$formaPago['numPagos'],$datos['sufijoCampos']);
	}

	echo $res;
}

//Parte de renovaciones
function generaEstadisticasRenovaciones($ejercicioAnterior){
	if($_SESSION['tipoUsuario'] == 'COLABORADOR'){
		$res=consultaBD("SELECT COUNT(contratos.codigo) AS total
							  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
							  LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
							  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
							  LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
							  WHERE (YEAR(contratos.fechaInicio)='$ejercicioAnterior' OR YEAR(contratos.fechaFin)='$ejercicioAnterior') AND contratos.renovado='NO' AND facturaCobrada='SI' AND
							  codigoCliente IN (SELECT codigo FROM clientes WHERE EMPASESORIA=".$_SESSION['codigoU'].") 
							  GROUP BY contratos.codigo 
							  ORDER BY contratos.fechaInicio;",true,true);
	}
	elseif($_SESSION['tipoUsuario'] == 'TECNICO'){
		$res=consultaBD("SELECT COUNT(contratos.codigo) AS total
							  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
							  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
							  LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
							  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
							  LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
							  WHERE (YEAR(contratos.fechaInicio)='$ejercicioAnterior' OR YEAR(contratos.fechaFin)='$ejercicioAnterior') AND contratos.renovado='NO' AND facturaCobrada='SI' AND
							  (contratos.tecnico=".$_SESSION['codigoU']." OR formularioPRL.codigoUsuarioTecnico=".$_SESSION['codigoU'].") 
							  GROUP BY contratos.codigo 
							  ORDER BY contratos.fechaInicio;",true,true);

	}
	else {
		$res=consultaBD("SELECT COUNT(contratos.codigo) AS total
							  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
							  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
							  LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
							  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura='PROFORMA'
							  LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
							  WHERE (YEAR(contratos.fechaInicio)='$ejercicioAnterior' OR YEAR(contratos.fechaFin)='$ejercicioAnterior') AND contratos.renovado='NO' AND facturaCobrada='SI'
							  GROUP BY contratos.codigo 
							  ORDER BY contratos.fechaInicio;",true,true);
	}

	return $res;
}


function imprimeContratosRenovar($fueraPlazo=false){
	global $_CONFIG;
	$iconoRevisado=array('SI'=>'<i class="icon-check-circle iconoFactura icon-success" title="Revisado"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger" title="Pendiente"></i>');

	$wherePlazo="enVigor='SI' AND contratos.fechaFin<=DATE_ADD(CURDATE(),INTERVAL 90 DAY) AND contratos.fechaFin>=CURDATE()";
	if($fueraPlazo){
		$wherePlazo="contratos.fechaFin<CURDATE()";
	}


	conexionBD();

	if($_SESSION['tipoUsuario'] == 'COLABORADOR'){
		$consulta=consultaBD("SELECT (ofertas.total-ofertas.importe_iva) AS importe, clientes.EMPNOMBRE,clientes.EMPMARCA,clientes.codigo AS codigoCliente, contratos.*,MAX(contratos_renovaciones.fecha), ofertas.opcion,
							  IFNULL(facturas.facturaCobrada,'NO') AS facturaCobrada, clientes.EMPID, formularioPRL.codigoUsuarioTecnico, contratoRevisado
							  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
							  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
							  LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
							  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
							  LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
							  WHERE $wherePlazo AND contratos.renovado='NO' AND facturaCobrada='SI' AND
							  codigoCliente IN (SELECT codigo FROM clientes WHERE EMPASESORIA=".$_SESSION['codigoU'].") AND checkNoRenovar='NO'
							  GROUP BY contratos.codigo 
							  ORDER BY contratos.fechaInicio;");
	}
	elseif($_SESSION['tipoUsuario'] == 'TECNICO'){
		$consulta=consultaBD("SELECT (ofertas.total-ofertas.importe_iva) AS importe, clientes.EMPNOMBRE,clientes.EMPMARCA,clientes.codigo AS codigoCliente,contratos.*, MAX(contratos_renovaciones.fecha) AS renovacion, 
							  contratos.tecnico, ofertas.opcion, IFNULL(facturas.facturaCobrada,'NO') AS facturaCobrada, clientes.EMPID, formularioPRL.codigoUsuarioTecnico, contratoRevisado
							  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
							  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
							  LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
							  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
							  LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
							  WHERE $wherePlazo AND contratos.renovado='NO' AND facturaCobrada='SI' AND
							  (contratos.tecnico=".$_SESSION['codigoU']." OR formularioPRL.codigoUsuarioTecnico=".$_SESSION['codigoU'].") 
							  AND checkNoRenovar='NO'
							  GROUP BY contratos.codigo 
							  ORDER BY contratos.fechaInicio;");

	}
	else {
		$consulta=consultaBD("SELECT (ofertas.total-ofertas.importe_iva) AS importe, clientes.EMPNOMBRE,clientes.EMPMARCA,clientes.codigo AS codigoCliente,contratos.*, MAX(contratos_renovaciones.fecha) AS renovacion, 
							  contratos.tecnico, ofertas.opcion, IFNULL(facturas.facturaCobrada,'NO') AS facturaCobrada, clientes.EMPID, formularioPRL.codigoUsuarioTecnico, contratoRevisado
							  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
							  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
							  LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
							  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
							  LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
							  WHERE ($wherePlazo AND contratos.renovado='NO' AND facturaCobrada='SI' AND checkNoRenovar='NO') OR (contratos.codigo IN(920,1028,1689) AND enVigor='SI')
							  OR ($wherePlazo AND (ofertas.total-ofertas.importe_iva)=0 AND enVigor='SI')
							  GROUP BY contratos.codigo 
							  ORDER BY contratos.fechaInicio;");
	}

	cierraBD();

	while($datos=mysql_fetch_assoc($consulta)){
		$cliente=datosRegistro('clientes',$datos['codigoCliente']);
		if($datos['tecnico']!=NULL){
			$tecnico=datosRegistro('usuarios',$datos['tecnico']);
		} else {
			$tecnico=datosRegistro('usuarios',$datos['codigoUsuarioTecnico']);
		}
		$servicio=obtieneNombreServicioContratado($datos['opcion']);

		echo "
			<tr>
				<td class='nowrap'>".$datos['EMPID']."</td>
				<td class='nowrap'>".formateaReferenciaContrato($cliente,$datos)."</td>
				<td>".formateaFechaWeb($datos['fechaInicio'])."</td>
				<td>".formateaFechaWeb($datos['fechaFin'])."</td>
				<td>".$cliente['EMPNOMBRE']."</td>
				<td>".$cliente['EMPMARCA']."</td>
				<td>".$servicio."</td>
				<td>".$tecnico['nombre']." ".$tecnico['apellidos']."</td>
				<td class='nowrap'>".compruebaImporteContrato($datos['importe'], $datos)." €</td>
				<td class='centro'>".$iconoRevisado[$datos['contratoRevisado']]."</td>
				<td>
					<div class='btn-group centro'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
						<ul class='dropdown-menu' role='menu'>
							<li><a href='".$_CONFIG['raiz']."contratos/gestion.php?codigo=".$datos['codigo']."&editable=CREADO'><i class='icon-search-plus'></i> Detalles</i></a></li>
							<li class='divider'></li>
							<li><a href='#' class='noAjax renovar' codigoContrato='".$datos['codigo']."' fechaInicio='".formateaFechaWeb($datos['fechaInicio'])."' fechaFin='".formateaFechaWeb($datos['fechaFin'])."'><i class='icon-check-circle'></i> Renovar contrato</i></a></li>
							<li class='divider'></li>
							<li><a href='#' class='noAjax generaContrato' cif='".$cliente['EMPCIF']."' codigoContrato='".$datos['codigo']."'><i class='icon-cloud-download'></i> Descargar contrato</i></a></li>
						</ul>
					</div>
				</td>
				<!--td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td-->
			</tr>";
	}
}

function renuevaContrato($codigoContrato){
	$res=true;
	$datos=arrayFormulario();
	$_POST['codigoInterno']='';//Para que entre en el mensajeResultado

	if(isset($datos['fechaInicio'])){
		
		$contrato=datosRegistro('contratos', $codigoContrato);

		$cambios=array();
		$cambios['fechaInicio']=$datos['fechaInicio'];
		$cambios['fechaFin']=$datos['fechaFin'];

		conexionBD();

		calculaIncrementoIPC($datos, $contrato, $cambios);

		$res=copiaDatos('contratos',$contrato,$cambios);

		if($res && comparaFechas($contrato['fechaFin'],fechaBD())>=0){//Se renueva el contrato viejo solo si ya ha llegado su fecha de fin

			$res=$res && consultaBD("UPDATE contratos SET renovado='SI', enVigor='NO' WHERE codigo='$codigoContrato';");//Se marca el contrato como renovado y se anula su vigor
		}
		elseif($res){//Si aún no ha llegado su fecha de fin, simplemente se marca como renovado y se le quita el vigor al nuevo
			$codigoNuevoContrato=$res;

			$res=$res && consultaBD("UPDATE contratos SET renovado='SI' WHERE codigo='$codigoContrato';");
			$res=$res && consultaBD("UPDATE contratos SET enVigor='NO' WHERE codigo='$codigoNuevoContrato';");
		}

		cierraBD();

	}


	return $res;
}

/**
 * Función que calcula el incremento del IPC
 * correspondiente a un contrato, obteniendo
 * el importe de la oferta original y sumando
 * los IPCs aplicados a cada contrato que se
 * le haya hecho.
 * 
 * Debe hacerse así porque, desafortunadamente,
 * en Anesco cada contrato NO tiene un campo de importe,
 * si no que se tira de los importes de la oferta original.
 * 
 * Para solventar una incidencia detectada en Enero de 2024,
 * en la cual se aplicaba el incremento de IPC sobre el importe
 * original de la oferta sin tener en cuenta las subidas anteriores,
 * esta función sigue el siguiente algoritmo:
 * 
 * 1. Obtiene el importe de la oferta original. Por ejemplo: 100 €. Además, extrae también los incrementos (en porcentaje) aplicados en contratos anteriores. Por ejemplo, 4%.
 * 2. Va calculando el IMPORTE de los incrementos aplicados en cada contrato anterior. Por ejemplo, 4% de incremento sobre 100€ = 104€.
 * 3. Obtiene el incremento del IPC correspondiente al ejercicio actual. Por ejemplo, 10%.
 * 4. Calcula el incremento total que se le aplicará al nuevo contrato. Por ejemplo, 104€ + 10% = 114,4€.
 *
 * @param  array $datos: datos del formulario de renovación.
 * @param  array $contrato: datos del contrato a renovar.
 * @param  array $cambios: array de cambios a realizar en el contrato para la renovación. Pasado por referencia.
 * @param  boolean $actualizacionMasiva: indica si se está realizando una actualización masiva de contratos, para obtener el IPC que corresponda al contrato (en lugar del último).
 * @return void
 */
function calculaIncrementoIPC($datos, $contrato, &$cambios, $actualizacionMasiva=false){
	
	if($datos['incrementoIPC']=='SI'){

		// Se establece que el nuevo contrato tendrá incremento del IPC (puede ser que el anterior no lo tuviera)
		$cambios['incrementoIPC'] = 'SI';

		/*
			Cálculo de incrementos aplicados anteriormente:
			Como el cálculo del porcentaje aplicado será acumulativo,
			se extrae solo el último contrato registrado, que ya traerá el
			porcentaje de incremento acumulado.
		*/
		$codigoOferta = $contrato['codigoOferta'];
		$importeOriginal = 0; // Para almacenar el importe original de la oferta, para luego calcular el porcentaje real de incremento
		$importeIncremento = 0; // Para ir sumando el IMPORTE de los incrementos realizados en cada contrato anterior, para calcular luego el porcentaje final de incremento
		
		if($actualizacionMasiva){ // Se llama desde un script de actualización masiva, por lo que se obtiene la fecha de inicio del propio contrato
			$fechaInicioContrato = $contrato['fechaInicio'];
		}
		else{ // Se obtiene la fecha de inicio del nuevo contrato
			$fechaInicioContrato = $cambios['fechaInicio'];
		}

		$contratoAnterior = consultaBD("SELECT subtotal, incrementoIpcAplicado
										FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
										WHERE ofertas.codigo='$codigoOferta' AND contratos.eliminado='NO'
										AND contratos.fechaInicio<'$fechaInicioContrato'
										ORDER BY contratos.fechaInicio DESC;", false, true);
		
		if($contratoAnterior){
			// Se almacena el importe original de la oferta para luego calcular el porcentaje real de incremento
			$importeOriginal = $contratoAnterior['subtotal'];

			// Cálculo del IMPORTE del incremento que se le aplicó al contrato anterior
			if($contratoAnterior['incrementoIpcAplicado']>0){
				$importeContratoConIPC = $importeOriginal * (1 + $contratoAnterior['incrementoIpcAplicado'] / 100);
				$importeIncremento = $importeContratoConIPC - $importeOriginal; // Se resta el importe original para obtener el incremento total
			}
		}

		if($actualizacionMasiva){ // Se llama a la función desde un script de actualización masiva
			// Obtención del nuevo incremento que le corresponde al ejercicio del contrato
			$ipc = consultaBD("SELECT incremento FROM incrementos_ipc WHERE ejercicio=YEAR('$fechaInicioContrato')", false, true);
		}
		else{
			// Obtención del nuevo incremento que le corresponde al ejercicio actual
			$ipc = consultaBD("SELECT incremento FROM incrementos_ipc ORDER BY ejercicio DESC", false, true);
		}

		if($importeOriginal!=0 && $ipc){
			// Cálculo del incremento total que se le aplicará al nuevo contrato
			$importeIncremento = ($importeOriginal + $importeIncremento) * (1 + $ipc['incremento'] / 100);
			$importeIncremento = $importeIncremento - $importeOriginal; // Se resta el importe original para obtener el incremento total
			$porcentajeIncremento = round($importeIncremento / $importeOriginal * 100, 2);
			
			// Asignación del incremento total al nuevo contrato
			$cambios['incrementoIpcAplicado'] = $porcentajeIncremento;
		}
	}

}

//Fin parte de renovaciones


function obtieneBotonesGestionContratos(){
	global $_CONFIG;

    if(isset($_GET['enVigor'])){
    	$res="<a href='".$_CONFIG['raiz']."contratos/' class='shortcut'><i class='shortcut-icon icon-chevron-left'></i><span class='shortcut-label'>Volver</span> </a>
              <a href='".$_CONFIG['raiz']."contratos/generaListadoVigor.php?tipo=excel' class='shortcut noAjax'><i class='shortcut-icon icon-cloud-download'></i><span class='shortcut-label'>Descargar en Excel</span> </a>
              <!--a href='".$_CONFIG['raiz']."contratos/generaListadoVigor.php?tipo=csv' class='shortcut noAjax'><i class='shortcut-icon icon-code'></i><span class='shortcut-label'>Descargar en CSV</span> </a-->";
     }
	elseif(isset($_GET['baja'])){
    	$res="<a href='".$_CONFIG['raiz']."contratos/' class='shortcut'><i class='shortcut-icon icon-chevron-left'></i><span class='shortcut-label'>Volver</span> </a>
              <!--a href='".$_CONFIG['raiz']."contratos/generaListado.php?baja' class='shortcut noAjax'><i class='shortcut-icon icon-download'></i><span class='shortcut-label'>Listado Contratos</span> </a-->";
     }
	elseif(isset($_GET['pendiente'])){
	    $res="
	    <a href='".$_CONFIG['raiz']."contratos/' class='shortcut'><i class='shortcut-icon icon-chevron-left'></i><span class='shortcut-label'>Volver</span> </a>
		<!--a href='".$_CONFIG['raiz']."contratos/generaListado.php?aprobados=NO' class='shortcut noAjax'><i class='shortcut-icon icon-download'></i><span class='shortcut-label'>Listado Contratos</span> </a-->
		<a href='javascript:void' id='eliminar' class='shortcut noAjax'><i class='shortcut-icon icon-times-circle'></i><span class='shortcut-label'>Eliminar contrato/s</span> </a>
		<a href='eliminados.php' class='shortcut noAjax'><i class='shortcut-icon icon-trash-o'></i><span class='shortcut-label'>Eliminados</span> </a>";
	}
	else{
		$res="
	    <a href='".$_CONFIG['raiz']."ofertas/gestion.php?desdeContrato' class='shortcut'><i class='shortcut-icon icon-plus-circle'></i><span class='shortcut-label'>Nuevo contrato</span> </a>
		<!--a href='".$_CONFIG['raiz']."contratos/generaListado.php?aprobados=NO' class='shortcut noAjax'><i class='shortcut-icon icon-download'></i><span class='shortcut-label'>Listado Contratos</span> </a-->
		<a href='javascript:void' id='eliminar' class='shortcut noAjax'><i class='shortcut-icon icon-times-circle'></i><span class='shortcut-label'>Eliminar contrato/s</span> </a>
		<a href='eliminados.php' class='shortcut noAjax'><i class='shortcut-icon icon-trash-o'></i><span class='shortcut-label'>Eliminados</span> </a>
		<a href='".$_CONFIG['raiz']."contratos/index.php?baja' class='shortcut'><i class='shortcut-icon icon-arrow-down'></i><span class='shortcut-label'>De clientes de baja</span> </a><br />
		<a href='".$_CONFIG['raiz']."contratos/index.php?pendiente' class='shortcut'><i class='shortcut-icon icon-flag'></i><span class='shortcut-label'>Pendientes</span> </a>
		<a href='".$_CONFIG['raiz']."contratos/index.php?enVigor' class='shortcut'><i class='shortcut-icon icon-check-circle'></i><span class='shortcut-label'>En vigor</span> </a>
		<a href='".$_CONFIG['raiz']."contratos/index.php?historico' class='shortcut'><i class='shortcut-icon icon-calendar-o'></i><span class='shortcut-label'>Histórico</span> </a>
		<a href='".$_CONFIG['raiz']."contratos/renovaciones.php' class='shortcut'><i class='shortcut-icon icon-exclamation'></i><span class='shortcut-label'>Renovaciones</span> </a>
		<a href='".$_CONFIG['raiz']."contratos/index.php?facturadoEliminado' class='shortcut'><i class='shortcut-icon icon-money'></i><span class='shortcut-label'>Con facturas<br>eliminadas</span> </a>";

	}

    return $res;
}

function tofloat($num) {
    $dotPos = strrpos($num, '.');
    $commaPos = strrpos($num, ',');
    $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos : 
        ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);
   
    if (!$sep) {
        return floatval(preg_replace("/[^0-9]/", "", $num));
    } 

    return floatval(
        preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
        preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
    );
}


function camposInformacionOferta($datos){
	if($datos){
		conexionBD();
		$codigoOferta=$datos['codigoOferta'];
		
		$oferta=consultaBD("SELECT opcion, subtotal, iva, importe_iva, total, importeRM, numRM, subtotalRM, importeRMExtra, codigoCliente FROM ofertas WHERE codigo='$codigoOferta';",false,true);
		$centros=obtieneCentrosClienteOferta($codigoOferta);
		cierraBD();

		echo "<h3 class='apartadoFormulario'>Datos de la oferta <div class='pull-right'><a href='../ofertas/gestion.php?codigo=$codigoOferta' class='btn btn-success btn-small noAjax' target='_blank'><i class='icon-external-link'></i> Ver oferta</a></h3>";

		$opciones=array(1=>'SPA 4 Especialidades',2=>'SPA Vigilancia de la salud',3=>'SPA Especialides técnicas',4=>'Otras actuaciones');

		abreColumnaCampos('span4');

		campoDato('Servicio contratado',$opciones[$oferta['opcion']]);
		campoDato('Subtotal',compruebaImporteContrato($oferta['subtotal'],$datos).' €');
		
		if($oferta['iva']=='SI'){
			campoDato('IVA','21% ('.compruebaImporteContrato($oferta['importe_iva'],$datos).' €)');
		}

		campoDato('Total',compruebaImporteContrato($oferta['total'],$datos).' €');

		cierraColumnaCampos();
		abreColumnaCampos();

		campoDato('Precio RM incluido en contrato',compruebaImporteContrato($oferta['importeRM'],$datos).' €');
		campoDato('Nº de RRMM incluidos',$oferta['numRM']);

		if($oferta['importeRM']==0 || $oferta['numRM']==0){
			$oferta['subtotalRM']='0.00';
		}

		campoDato('Subtotal RM (exento IVA)',compruebaImporteContrato($oferta['subtotalRM'],$datos).' €');
		campoDato('Precio RM extra (fuera contrato)',compruebaImporteContrato($oferta['importeRMExtra'],$datos).' €');

		cierraColumnaCampos(true);

		abreColumnaCampos('span12');
		if($centros!=''){
			campoDato('Centros de trabajo',$centros);
		}
		cierraColumnaCampos();

	}
}

function obtieneCentrosClienteOferta($codigoOferta){
	$res='';

	$consulta=consultaBD("SELECT direccion, cp, localidad, provincia, nombre 
						  FROM clientes_centros INNER JOIN centros_trabajo_en_ofertas ON clientes_centros.codigo=centros_trabajo_en_ofertas.codigoClienteCentro
						  WHERE codigoOferta='$codigoOferta';");
	
	if(mysql_num_rows($consulta)>1){
		
		$i=1;
		while($centro=mysql_fetch_assoc($consulta)){
			$res.='<strong>Centro '.$i.'</strong>: '.$centro['direccion'].'. '.$centro['cp'].' '.$centro['localidad'].' ('.$centro['provincia'].').<br />';
			$i++;
		}

	}

	return $res;
}


//La siguiente función se encarga de generar, en PDF, el contrato para que las modificaciones posteriores a la empresa no altenen el documento original, que quedará archivado para futuras descargas
function generaFicheroPdfContrato($codigoContrato){
    include_once('../../api/js/firma/signature-to-image.php');
    require_once('../../api/html2pdf/html2pdf.class.php');

    $contenido=generaPDFContrato($codigoContrato);

    $html2pdf=new HTML2PDF('P','A4','es');
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->WriteHTML($contenido);
    $html2pdf->Output('../documentos/contratos/'.$codigoContrato.'.pdf','f');
}

//Fin parte de contratos


//Parte de certificado de corriente de pago

function generaCertificadoCorrientePago($PHPWord,$codigoContrato){
	
	$fechaEmision=fecha();
	$textoRenovacion='';

	conexionBD(); 
	$datos=consultaBD("SELECT EMPID, EMPCIF, EMPNOMBRE, EMPDIR, EMPLOC, EMPCP, EMPPROV, opcion, contratos.fechaInicio, contratos.fechaFin, enVigor, otraOpcion,
					   contratos.codigoInterno, EMPEMAILPRINC
					   FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
					   INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
					   WHERE contratos.codigo='$codigoContrato';",false,true);

	$factura = consultaBD("SELECT facturaCobrada
						   FROM contratos_en_facturas LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo
					   	   WHERE codigoContrato='$codigoContrato' AND tipoFactura='NORMAL' AND facturas.eliminado='NO';",false,true);
	
	 cierraBD();
	
	$numeroContrato=formateaReferenciaContrato($datos,$datos);

	$servicios=array(
		'1'	=>	'Seguridad en el Trabajo, Higiene Industrial, Ergonomía y Psicosociología Aplicada y Vigilancia de la Salud',
		'2'	=>	'Vigilancia de la salud',
		'3'	=>	'Seguridad en el Trabajo, Higiene Industrial y Ergonomía y Psicosociología Aplicada',
		'4'	=>	$datos['otraOpcion']
	);

	$vigenciaContrato='finalizado el '.formateaFechaWeb($datos['fechaFin']);
	if($datos['enVigor']=='SI'){
		$vigenciaContrato='en vigor hasta el '.formateaFechaWeb($datos['fechaFin']);
		$textoRenovacion='Dicho contrato se renovará automáticamente con una periodicidad de un año, salvo renuncia expresa de alguna de las partes.';
	}

	$estadoPago='con importes pendientes de pago';
	if($factura['facturaCobrada']=='SI'){
		$estadoPago='al corriente de pago';
	}

	$documento=$PHPWord->loadTemplate('../documentos/certificados-corriente-pago/plantillaCertificadoCorrientePago.docx');
	
	$documento->setValue("fechaEmision",utf8_decode($fechaEmision));
	$documento->setValue("razonSocial",utf8_decode($datos['EMPNOMBRE']));
	$documento->setValue("direccion",utf8_decode($datos['EMPDIR']));
	$documento->setValue("cp",utf8_decode($datos['EMPCP']));
	$documento->setValue("localidad",utf8_decode($datos['EMPLOC']));
	$documento->setValue("provincia",utf8_decode($datos['EMPPROV']));
	$documento->setValue("cif",utf8_decode($datos['EMPCIF']));
	$documento->setValue("serviciosContratados",utf8_decode($servicios[$datos['opcion']]));
	$documento->setValue("numContrato",utf8_decode($numeroContrato));
	$documento->setValue("fechaContrato",utf8_decode(formateaFechaWeb($datos['fechaInicio'])));
	$documento->setValue("vigenciaContrato",utf8_decode($vigenciaContrato));
	$documento->setValue("textoRenovacion",utf8_decode($textoRenovacion));
	$documento->setValue("estadoContrato",utf8_decode($estadoPago));

	
	$documento->save('../documentos/certificados-corriente-pago/Certificado-corriente-pago.docx');

	return $datos['EMPEMAILPRINC'];
}

//Fin parte de certificado de corriente de pago


//Parte de Excel y CSV de contratos en vigor

function generaListadoContratosEnVigor($documento,$anio=false){
	$servicios=array(
		1	=>	'SPA 4 Especialidades',
		2	=>	'SPA Vigilancia de la salud',
		3	=>	'SPA Especialides técnicas',
		4	=>	'Otras actuaciones'
	);

	$where="WHERE contratos.eliminado='NO' AND enVigor='SI' AND facturas.facturaCobrada='SI'";
	if($anio!=false){
		$where="WHERE YEAR(contratos.fechaInicio)='$anio'";
	}
	
	conexionBD();

	$consulta=consultaBD("SELECT clientes.EMPNOMBRE AS razonSocial, clientes.EMPCIF AS cif, clientes.EMPDIR AS domicilioSocial, clientes_centros.direccion AS centroTrabajo,
						  clientes.EMPCNAE AS actividad, ofertas.numEmpleados AS numTrabajadores, ofertas.opcion AS servicio, contratos.fechaInicio

						  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
						  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
						  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
						  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
						  LEFT JOIN clientes_centros ON clientes.codigo=clientes_centros.codigoCliente

						  $where

						  GROUP BY contratos.codigo 

						  ORDER BY clientes.EMPNOMBRE;");

	$fila=2;
	while($datos=mysql_fetch_assoc($consulta)){

		$documento->getActiveSheet()->getCell('A'.$fila)->setValue($datos['razonSocial']);
		$documento->getActiveSheet()->getCell('B'.$fila)->setValue($datos['cif']);
		$documento->getActiveSheet()->getCell('C'.$fila)->setValue($datos['domicilioSocial']);
		$documento->getActiveSheet()->getCell('D'.$fila)->setValue($datos['centroTrabajo']);
		$documento->getActiveSheet()->getCell('E'.$fila)->setValue($datos['actividad']);
		$documento->getActiveSheet()->getCell('F'.$fila)->setValue($datos['numTrabajadores']);
		$documento->getActiveSheet()->getCell('G'.$fila)->setValue($servicios[$datos['servicio']]);
		$documento->getActiveSheet()->getCell('H'.$fila)->setValue(formateaFechaWeb($datos['fechaInicio']));
		
		$fila++;
	}


	foreach(range('A','H') as $columna) {
    	$documento->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);
	}

	cierraBD();
}

//Fin parte de Excel y CSV de contratos en vigor