<?php
  $seccionActiva=1;
  include_once("../cabecera.php");
  $j=gestionContratos();
  global $_CONFIG;
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script src="../js/oyenteFormaPagoOfertasContratos.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.selectpicker').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$('#fechaInicio').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
      var fecha=$(this).val().split('/').reverse().join('-');
      fecha = new Date(fecha);
      fecha.setDate(fecha.getDate() - 1);
      fecha.setDate(fecha.getDate() + 365);
      var dia=fecha.getDate()<10?'0'+fecha.getDate():fecha.getDate();
      var mes=(fecha.getMonth()+1)<10?'0'+(fecha.getMonth()+1):(fecha.getMonth()+1);
      var anio = fecha.getFullYear();
      $('#fechaFin').val(dia+'/'+mes+'/'+anio);
      $('#fechaFin').datepicker("update");
      generaNumeroContrato();
    });

	oyenteFormaPago();
	$('#codigoFormaPago').change(function(){
		oyenteFormaPago(true);
		oyenteCuentasPropiasFormaPago();
	});

	var res=validacionContrato();


	$('.submit').unbind().click(function(e){
		e.preventDefault();
		compruebaFechasPago($(this));
	});

	//Parte de sumatorio de vencimientos
	oyenteSumatorioVencimientos();
	$('.importeVencimiento').change(function(){
		oyenteSumatorioVencimientos();
	});
	$('.importeVencimientoRM').change(function(){
		oyenteSumatorioVencimientos();
	});
	//Fin parte de sumatorio de vencimientos

	$('input[name=vencimientosOfacturas]').change(function(){
		oyenteVencimientosOfacturas($(this),'#textoImporte','#tablaVencimientos');
	});

	$('input[name=vencimientosOfacturas2]').change(function(){
		oyenteVencimientosOfacturas($(this),'#textoImporte2','#tablaVencimientos2');
	});
});

function oyenteFormaPago(cambio=false){

	var codigoFormaPago=$('#codigoFormaPago').val();
	var importe=$('#importeTotal').text();
	var importeRM='';
	
	if($('#importeTotalRM').length){
		importeRM=$('#importeTotalRM').text();
	}
	
	if(codigoFormaPago=='NULL'){
		$('#cajaVencimientos,#cajaVencimientos2').addClass('hide');
	}
	else if($('#codigo').val()!=undefined || codigoFormaPago!='NULL'){//En los detalles de un contrato, la función PHP es la que imprime las filas
		$('#cajaVencimientos,#cajaVencimientos2').removeClass('hide');	
	}
	
	if(cambio || ($('#codigo').val()==undefined && codigoFormaPago!='NULL')){

		if($('input[name=vencimientosOfacturas2]:checked').val()==undefined){//Solo 1 factura
			obtieneTablaVencimientosFactura(codigoFormaPago,importe,importeRM,'#tablaVencimientos','#cajaVencimientos','');
		}
		else{//Contratos con facturas PT + RM
			obtieneTablaVencimientosFactura(codigoFormaPago,importe,0,'#tablaVencimientos','#cajaVencimientos','');
			obtieneTablaVencimientosFactura(codigoFormaPago,0,importeRM,'#tablaVencimientos2','#cajaVencimientos2','RM');
		}

	}

}

function obtieneTablaVencimientosFactura(codigoFormaPago,importe,importeRM,selectorTabla,selectorCaja,sufijoCampos){
	var consulta=$.post('../listadoAjax.php?include=contratos&funcion=obtieneVencimientosFormaPago();',{'codigoFormaPago':codigoFormaPago,'importe':importe,'importeRM':importeRM,'sufijoCampos':sufijoCampos});
	consulta.done(function(respuesta){
		$(selectorTabla+' tbody').html(respuesta);
		$(selectorTabla+' .hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
		$(selectorCaja).removeClass('hide');	
	});
}

function redireccionaFichaCliente(){
	var codigoCliente=$('#codigoCliente').text();
	var ruta = <?php echo $_CONFIG['raiz'];?>;
	window.open(ruta+'clientes/gestion.php?codigo='+codigoCliente,'_blank');
}

function compruebaFechasPago(boton){
	var res=true;
	var codigoFormaPago=$('#codigoFormaPago').val();
	if(codigoFormaPago=='NULL'){
		alert('No se puede registrar un contrato sin definir la forma de pago');
		res=false;
	}
	else if($('#tablaVencimientos .hasDatepicker').length>0){
		$('#tablaVencimientos .hasDatepicker').each(function(){
			if($(this).val()==''){
				res=false;
			}
		});

		if(!res){
			alert('No se puede registrar un contrato sin definir todas las fechas de vencimiento');
		} else {
			res=validacionContrato();
		}
	}
	
	var importeVencimiento=0;
	$('.importeVencimiento').each(function(){
		importeVencimiento+=formateaNumeroCalculo($(this).val());
	});
	
	if(res){
		if($('#codigo').length==0 && importeVencimiento>0){
			res=confirm("¿Enviar al cliente la factura proforma relacionada con el contrato?");
			if(res==true){
				$('#enviarMail').val('SI');
			}
		}

		if(boton.val()=='Finalizar'){
			$('#emitirFacturas').val('SI');
		}

		$('form').submit();
	}
}

function oyenteSumatorioVencimientos(){
	var total=0;
	var totalRM=0;

	$('.importeVencimiento').each(function(){
		total+=formateaNumeroCalculo($(this).val());
	});

	$('.importeVencimientoRM').each(function(){
		totalRM+=formateaNumeroCalculo($(this).val());
	});


	var importeTotal = formateaNumeroCalculo($('#importeTotal').text());
	if(importeTotal!=total.toFixed(2)){
		$('#totalVencimiento').html("<span class='label label-danger'><i class='icon-exclamation-circle'></i></span> "+formateaNumeroWeb(total)+" €");
	}
	else{
		$('#totalVencimiento').text(formateaNumeroWeb(total)+' €');
	}

	var importeTotalRM = formateaNumeroCalculo($('#importeTotalRM').text());
    if (totalRM>0 && importeTotalRM!=totalRM.toFixed(2)) {
		$('#totalVencimientoRM').html("<span class='label label-danger'><i class='icon-exclamation-circle'></i></span> "+formateaNumeroWeb(totalRM)+" €");
	}
	else if(totalRM>0){
		$('#totalVencimientoRM').text(formateaNumeroWeb(totalRM)+' €');
	}
}

function validacionContrato(){
	var red=false;
	//var res=isValidNif($('#EMPCIF').text().trim());
	var res=0;

	if(res>0){
		//$('.widget .btn-propio').addClass('hide');
		alert('No se puede tramitar el contrato: el CIF del cliente no es válido.');
		red=true;
	}
	else{
		res=true;
	}

	if($('#codigo').val()==undefined){
		res='';
		var formaPago=$('#codigoFormaPago').val();
		var idCampos=['EMPID','EMPNOMBRE','EMPACTIVIDAD','EMPDIR','EMPLOC','EMPTELPRINC','EMPMARCA','EMPCNAE','EMPCP','EMPPROV','EMPEMAILPRINC','EMPRL','EMPRLDNI','EMPRLCARGO','EMPNTRAB','EMPPC','EMPCENTROS','comercial','usuario','clave','direccionCentroTrabajo'];
		var nombresCampos={'EMPID':'Nº de cliente','EMPNOMBRE':'Razón social','EMPACTIVIDAD':'Actividad','EMPDIR':'Domicilio social','EMPLOC':'Localidad','EMPTELPRINC':'Teléfono de empresa','EMPMARCA':'Nombre comercial','EMPCNAE':'CNAE','EMPCP':'Código Postal','EMPPROV':'Provincia','EMPEMAILPRINC':'eMail principal', 'EMPRL':'Nombre del RL','EMPRLDNI':'DNI del RL','EMPRLCARGO':'Cargo del RL','EMPNTRAB':'Nº de trabajadores','EMPPC':'Persona de contacto','EMPCENTROS':'Número de centros','comercial':'Comercial','usuario':'Usuario','clave':'Clave','direccionCentroTrabajo':'Al menos 1 centro con la dirección completa'};

		for(var i=0;$('#formaPago'+i).text()!='';i++){
			if(formaPago==$('#formaPago'+i).text()){
				idCampos.push('EMPIBAN');
				nombresCampos['EMPIBAN']='IBAN'
			}
		}

		for(var i=0;i<idCampos.length;i++){
			if($('#'+idCampos[i]).text().trim()=='' || $('#'+idCampos[i]).text().trim()=='NULL'){
				res+='-'+nombresCampos[idCampos[i]]+'\n';
			}
		}

		if(res!=''){
			//$('.widget .btn-propio').addClass('hide');
			alert('No se puede tramitar el contrato. Faltan los siguientes datos en la ficha del cliente:\n\n'+res);
			red=true;
			res=false;
		} else {
			res=true;
		}
	}

	if(red){
		redireccionaFichaCliente();
	}
	return res;
}

function oyenteVencimientosOfacturas(campo,selectorTexto,selectorTabla){
	var tipoEmision=campo.val();
	var texto=campo.parent().text().trim();

	$(selectorTabla).parent().parent().find('.control-label').text(texto+':');

	if(tipoEmision=='FACTURAS'){
		$(selectorTexto).text('Total factura');
	}
	else{
		$(selectorTexto).text('Importe');
	}
}

function generaNumeroContrato(){
	if($('#fechaInicio').val()!=''){
		var consulta=$.post('../listadoAjax.php?include=contratos&funcion=generaNumeroDesdeJS();',{'codigoOferta':$('#codigoOferta').val(),'fechaInicio':$('#fechaInicio').val()});
		consulta.done(function(respuesta){
			$('#codigoInterno').val(respuesta);
		});
	}
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>