<?php

  $seccionActiva=1;
  include_once('../cabecera.php');
  compruebaContratosVigor();
  $res=operacionesContratos(); 

  $celdaEstado='';

  if(isset($_GET['enVigor'])){
    $estadisticas=estadisticasContratosRestrict('NO');

    $estadistica1='<div class="stat"> <i class="icon-check"></i> <span class="value">'.$estadisticas['total'].'</span><br />Contratos en vigor</div>';
    $estadistica2='<div class="stat"> <i class="icon-euro"></i> <span class="value">'.formateaNumeroWeb($estadisticas['importe']).'</span><br />Total</div>';

    $botones=obtieneBotonesGestionContratos();

    $textoListado='en vigor';
  }
  elseif(isset($_GET['baja'])){
    $estadisticas=estadisticasContratosRestrict('NO');

    $estadistica1='<div class="stat"> <i class="icon-arrow-down"></i> <span class="value">'.$estadisticas['total'].'</span><br />Contratos de clientes de baja</div>';
    $estadistica2='';

    $botones=obtieneBotonesGestionContratos();

    $textoListado='de clientes de baja';
  }  
  elseif(isset($_GET['historico'])){
    $estadisticas=estadisticasContratosRestrict('NO');

    $estadistica1='<div class="stat"> <i class="icon-calendar-o"></i> <span class="value">'.$estadisticas['total'].'</span><br />Contratos sin vigor</div>';
    $estadistica2='';

    $botones=obtieneBotonesGestionContratos();

    $textoListado='sin vigor';
  } 
  elseif(isset($_GET['pendiente'])){
    $estadisticas=estadisticasContratosRestrict('NO');

    $estadistica1='<div class="stat"> <i class="icon-file-text-o"></i> <span class="value">'.$estadisticas['total'].'</span><br />Contratos pendientes de cobrar</div>';
    $estadistica2='';

    $botones=obtieneBotonesGestionContratos();

    $textoListado='en vigor pendientes de cobrar';
  }
  else {
    $estadisticas=estadisticasInicialesContratos();

    $estadistica1='<div class="stat"> <i class="icon-check-circle"></i> <span class="value">'.$estadisticas['enVigor'].'</span><br />Contratos en vigor</div>
                   <div class="stat"> <i class="icon-exclamation"></i> <span class="value">'.$estadisticas['aRenovar'].'</span><br />Pendientes renovar</div>';
    
    $estadistica2='<div class="stat"> <i class="icon-calendar-times-o"></i> <span class="value">'.$estadisticas['fueraPlazo'].'</span><br />Vencidos</div>
                   <div class="stat"> <i class="icon-trash-o"></i> <span class="value">'.$estadisticas['anulados'].'</span><br />Anulados</div>';

    $botones=obtieneBotonesGestionContratos();

    $textoListado=' registrados';
    $celdaEstado='<th>Estado</th>';
  }
?> 
<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de gestión de Contratos:</h6>
                   <div id="big_stats" class="cf">
                     <?php 
                        echo $estadistica1;
                        echo $estadistica2;
                      ?>
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de contratos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <?php echo $botones; ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Contratos <?=$textoListado?></h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Nº Cliente </th>
                  <th> Nº Contrato </th>
                  <th> Inicio </th>
                  <th> Fin </th>
                  <th> Razón Social </th>
                  <th> Nombre Comercial </th>     
                  <th> Servicio </th>             
                  <th> Técnico </th>
                  <th> Importe </th>
                  <th> Revisado </th>
                  <?=$celdaEstado?>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeContratos('NO');
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    
    $(document).on('click','.confirmaContrato',function(e){
        var res=false;

        if(isValidCifNif($(this).attr('cif'))){
            res=true;
        }

        return res;
    });


    $(document).on('click','.generaContrato',function(e){
        if(!isValidCifNif($(this).attr('cif'))){
            e.preventDefault();
            alert('CIF/NIF de cliente no válido');
        }
    });


    $(document).on('click','a.enviaCertificado',function(e){
        e.preventDefault();
        url=$(this).attr('href');
        var consulta=$.post(url);

        consulta.done(function(respuesta){
            $('#contenido').append(respuesta);

            setTimeout(function() {//Para hacer desaparecer la caja de resultado
                $(".errorLogin:not(:contains('Error'),:contains('Atención'))").fadeOut(3000);
            },3000);
            
        });
    });
});
</script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>