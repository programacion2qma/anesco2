<?php
	session_start();
	include_once('funciones.php');
	compruebaSesion();
	require_once '../../api/phpword/PHPWord.php';


	$emailCliente=generaCertificadoCorrientePago(new PHPWord(),$_GET['codigoContrato']);

	if(isset($_GET['envio'])){

        $mensaje="
        <i>Estimado cliente,</i><br /><br />
        Gracias por confiar en los servicios de <strong>ANESCO SERVICIO DE PREVENCIÓN</strong>.
        Atendiendo a su solicitud, adjunto le remitimos el certificado con los importes actualizados que tiene pendientes de regularizar por la prestación de nuestros servicios.
        <br /><br />
		Muchas gracias por su atención.<br />
		Reciba un cordial saludo.<br /><br />
		<img src='https://crmparapymes.com.es/anesco2/img/logo.png' /><br />
		<div style='color:#075581;font-weight:bold'>
			<i>ANESCO SERVICIO DE PREVENCION</i><br />
			Tlf .954.10.92.93<br />
			info@anescoprl.es · www.anescoprl.es<br />
			C/ Murillo 1, 2ª P. 41001 - Sevilla.
		</div>";

        $adjunto='../documentos/certificados-corriente-pago/Certificado-corriente-pago.docx';
        
        if(enviaEmail($emailCliente, 'Certificado de pagos - ANESCO SALUD PREVENCION',$mensaje,$adjunto)){
        	mensajeOk('Certificado enviado correctamente');//Uso echo porque se llamará por AJAX desde el listado
        }
        else{
        	echo mensajeError("hubo un error al enviar el certificado. Por favor, inténtelo de nuevo más tarde");
        }
    } 
    else{
		// Definir headers
		header("Content-Type: application/msword");
		header("Content-Disposition: attachment; filename=Certificado-corriente-pago.docx");
		header("Content-Transfer-Encoding: binary");

		// Descargar archivo
		readfile('../documentos/certificados-corriente-pago/Certificado-corriente-pago.docx');
	}