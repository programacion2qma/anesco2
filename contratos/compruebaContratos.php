<?php
session_start();
include_once("funciones.php");
compruebaSesion();

$repetidos=array();
$repetidos2=array();

//En Histórico
ob_start();

$_GET['historico']=true;

imprimeContratos('NO');

$salida = ob_get_clean();

preg_match_all("/\<td class='nowrap referenciaContrato'>.*/",$salida,$repetidos);

//En Vigor
ob_start();

unset($_GET['historico']);
$_GET['enVigor']=true;

imprimeContratos('NO');

$salida = ob_get_clean();

preg_match_all("/\<td class='nowrap referenciaContrato'>.*/",$salida,$repetidos2);

//Procesamiento de duplicados

$resultado=array_intersect($repetidos[0],$repetidos2[0]);

foreach($resultado as $repetido){
	$repetido=strip_tags($repetido);
	echo $repetido.'<br />';
}