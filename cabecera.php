<?php
  @session_start();
  include_once("funciones.php");
  compruebaPermisos();
  $nombre='';
  if($_SESSION['tipoUsuario']=='CLIENTE'){
    $cliente=consultaBD('SELECT EMPNOMBRE FROM usuarios_clientes INNER JOIN clientes ON usuarios_clientes.codigoCliente=clientes.codigo WHERE codigoUsuario='.$_SESSION['codigoU'],true,true);
    $nombre=' - '.$cliente['EMPNOMBRE'];
  }
  if(!isset($_GET['ajax'])){
?>
  <!DOCTYPE html>
  <html lang="es">
  <head>
  <meta charset="utf-8">
  <title><?php echo $_CONFIG['tituloGeneral'] ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=0.3, maximum-scale=1.0, user-scalable=1">
  <meta name="apple-mobile-web-app-capable" content="yes">

  <link href="<?php echo $_CONFIG['raiz']; ?>css/bootstrap.min.php" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/bootstrap-responsive.css" rel="stylesheet" />
  <link href="//fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/font-awesome.css?v=1.0" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>js/full-calendar/fullcalendar.css" rel="stylesheet" />
  <?php
    /*if(isset($seccionActiva) && $seccionActiva==4){
      echo '<link href="../js/full-calendar/fullcalendar.css" rel="stylesheet" />';
    }*/
  ?>

  <link href="<?php echo $_CONFIG['raiz']; ?>css/style.php" rel="stylesheet /">
  <link href="<?php echo $_CONFIG['raiz']; ?>css/paneles.php" rel="stylesheet" />

  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/datepicker.css" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/bootstrap-select.css" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/bootstrap-wysihtml5.css" rel="stylesheet" />

  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/animate.css" rel="stylesheet" type="text/css" />

  <link href="<?php echo $_CONFIG['raiz']; ?>../api/js/guidely/guidely.css" rel="stylesheet" />

  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/bootstrap-colorpicker.css" rel="stylesheet" />

  <!-- Script HTML5, para el soporte del mismo en IE6-8 -->
  <!--[if lt IE 9]>
        <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->


  <!-- Común -->  
  <script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery-1.7.2.min.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-datepicker.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>../api/js/controladorAJAX.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>../api/js/funciones.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>/js/function.js" type="text/javascript"></script>
  <script src="https://maps.google.com/maps/api/js?key=AIzaSyBx_nRiwvkg8-ZnX5gCd66ygwYO4WdIKZY"></script>
  <!-- Fin común -->


  </head>
  <body>
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container">

        <span class="logo2">
          <img src="<?php echo $_CONFIG['raiz']; ?>img/logo2.png" alt="<?php echo $_CONFIG['altLogo']; ?>"> &nbsp; <span><?php echo $_CONFIG['tituloSoftware'].$nombre; ?></span>
        </span>

        <div class="nav-collapse">
          <ul class="nav pull-right cajaUsuario">
            <li class="dropdown"  id="target-5"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> <?php echo ucfirst($_SESSION['usuario']).perfilUsuario(); ?> <b class="caret"></b></a>
              <ul class="dropdown-menu menu">
                <li><a href="<?php echo $_CONFIG['raiz']; ?>inicio.php" class='noAjax'><i class="icon-home"></i> Inicio</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo $_CONFIG['raiz']; ?>cerrarSesion.php" class='noAjax'><i class="icon-power-off"></i> Cerrar sesión</a></li>
              </ul>
            </li>
          </ul>

        </div>
        <div class="nav-collapse">
          <?php cajaFiltroEjercicio(); ?>
        </div>
          <?php cajaHistoricoSQL(); ?>
        <!--/.nav-collapse -->
      </div>
      <!-- /container -->
    </div>
    <!-- /navbar-inner -->
  </div>
  <!-- /navbar -->
  <div class="subnavbar">
    <div class="subnavbar-inner">
      <div class="container">
        <ul class="mainnav menu" id="target-4">
        <?php
          creaOpcionMenu(1,'Ofertas','ofertas/','icon-check-square-o',$seccionActiva,array('COLABORADOR'));
          creaOpcionDesplegableMenu(1,'Comercial','icon-shopping-cart',array('Posibles clientes','Clientes','Ofertas','Contratos','Tarifas','Colaboradores','Planificación / Control email'),array('posibles-clientes/','clientes/','ofertas/','contratos/','tarifas/','colaboradores/','planificacion-vs/'),array('icon-question-circle','icon-check-circle','icon-tags','icon-file-text-o','icon-list-ol','icon-handshake-o','icon-check-square-o'),$seccionActiva,obtenerTiposUsuarios(array('COLABORADOR','CLIENTE','EMPLEADO','MEDICO','ENFERMERIA','ADMINISTRACION')));
          creaOpcionDesplegableMenu(1,'Comercial','icon-shopping-cart',array('Clientes','Ofertas','Contratos','Tarifas','Actividades','Instalaciones','Colaboradores'),array('clientes/','ofertas/','contratos/','tarifas/','actividades/','instalaciones/','colaboradores/'),array('icon-check-circle','icon-tags','icon-file-text-o','icon-list-ol','icon-industry','icon-building-o','icon-handshake-o'),$seccionActiva,obtenerTiposUsuarios(array('ADMIN','COLABORADOR','CLIENTE','EMPLEADO','FACTURACION','MEDICO','ENFERMERIA','COMERCIAL','TECNICO','ADMINISTRACION')));
          creaOpcionDesplegableMenu(1,'Comercial','icon-shopping-cart',array('Posibles clientes','Clientes','Ofertas','Contratos','Actividades','Instalaciones'),array('posibles-clientes/','clientes/','ofertas/','contratos/','actividades/','instalaciones/'),array('icon-question-circle','icon-check-circle','icon-tags','icon-file-text-o','icon-industry','icon-building-o'),$seccionActiva,array('ADMINISTRACION'));
          
          creaOpcionMenu(12,'Empleados','empleados/seleccionaCliente.php','icon-group',$seccionActiva,array('ADMIN','MEDICO','ENFERMERIA','TECNICO','ADMINISTRACION'));


          creaOpcionDesplegableMenu(9,'Planificación','icon-check-square-o',array('Planificación','Mapas de procesos'),array('planificacion/','mapas/'),array('icon-check-circle','icon-sitemap'),$seccionActiva,array('TECNICO'));    
          creaOpcionDesplegableMenu(9,'Planificación','icon-check-square-o',array('Pendientes','Confirmadas','Por técnicos','Mapas de procesos'),array('planificacion-pendiente/','planificacion/','planificacion-tecnicos/seleccionaTecnico.php','mapas/'),array('icon-flag','icon-check-circle','icon-user','icon-sitemap'),$seccionActiva,obtenerTiposUsuarios(array('TECNICO','COLABORADOR','CLIENTE','EMPLEADO','FACTURACION','MEDICO','ENFERMERIA','ADMINISTRACION')));          
          creaOpcionDesplegableMenu(9,'Planificación','icon-check-square-o',array('Pendientes','Confirmadas'),array('planificacion-pendiente/','planificacion/'),array('icon-flag','icon-check-circle'),$seccionActiva,array('FACTURACION'));          

          
          creaOpcionDesplegableMenu(5,'Área técnica','icon-wrench',array('Toma de datos','Evaluación de riesgos','Accidentes de trabajo','Inspeccion de trabajo','Formación / Información','Alumnos formados online','Otras visitas','Visitas de seguimiento','Visitas de revisión','Formación / Información','Visita a demanda de la empresa','Asistencia a comité de seguridad y salud','Elaboración de memoria','Elaboración de presupuesto de actuación','Otros motivos'),array('toma-datos/','evaluacion-de-riesgos/','accidentes/','asistencia/','certificados/','formados-online/','prueba','visitas_seguridad/','prueba','prueba','prueba','prueba','prueba','prueba','prueba'),array('icon-edit','icon-exclamation-triangle','icon-ambulance','icon-support','icon-certificate','icon-users','icon-arrow-down','icon-arrow-right','icon-arrow-right','icon-arrow-right','icon-arrow-right','icon-arrow-right','icon-arrow-right','icon-arrow-right','icon-arrow-right'),$seccionActiva,obtenerTiposUsuarios(array('FACTURACION','EMPLEADO','MEDICO','ENFERMERIA','CLIENTE','ADMINISTRACION')));
          
          creaOpcionDesplegableMenu(3,'VS','icon-stethoscope',array('Contratos','Generación documentos','Protocolización','Reconocimientos','Gestión de citas'),array('contratos-vs/','documentos-vs/','protocolizacion/','reconocimientos-medicos/','citas/'),array('icon-paste','icon-files-o','icon-table','icon-stethoscope','icon-calendar'),$seccionActiva,array('ADMIN','MEDICO'));
          creaOpcionDesplegableMenu(3,'VS','icon-stethoscope',array('Contratos','Reconocimientos','Gestión de citas'),array('contratos-vs/','reconocimientos-medicos/','citas/'),array('icon-paste','icon-stethoscope','icon-calendar'),$seccionActiva,array('ADMINISTRACION'));
          creaOpcionDesplegableMenu(3,'VS','icon-stethoscope',array('Reconocimientos','Gestión de citas'),array('reconocimientos-medicos/','citas/'),array('icon-stethoscope','icon-calendar'),$seccionActiva,array('ENFERMERIA'));

          
          creaOpcionDesplegableMenu(4,'Agenda','icon-calendar',array('Agenda','Memoria','Listado de gastos por técnicos'),array('tareas/','memoria/','gastos/'),array('icon-calendar','icon-history','icon-money'),$seccionActiva,array('ADMIN','ADMINISTRATIVO'));
          creaOpcionDesplegableMenu(4,'Agenda','icon-calendar',array('Agenda','Memoria'),array('tareas/','memoria/'),array('icon-calendar','icon-history'),$seccionActiva,obtenerTiposUsuarios(array('ADMIN','FACTURACION','EMPLEADO','MEDICO','ENFERMERIA','CLIENTE')));
          creaOpcionMenu(4,'Agenda','tareas/','icon-calendar',$seccionActiva,array('COLABORADOR','FACTURACION','MEDICO'));

          
          creaOpcionDesplegableMenu(10,'Configuración','icon-cogs',array('EPIs','Puestos','Actividades','Instalaciones','Emails','Protocolos','Enfermedades','Comentarios analíticas','Incrementos IPC','Profesionales externos'),array('epis/','puestos-trabajo/','actividades/','instalaciones/','correos/','protocolos/','enfermedades/','comentarios-analiticas/','incrementos-ipc/','profesionales-externos/'),array('icon-shield','icon-sitemap','icon-industry','icon-building-o','icon-envelope','icon-heartbeat','icon-medkit','icon-tint','icon-line-chart','icon-user-md'),$seccionActiva,array('ADMIN'));

          /*creaOpcionDesplegableMenu(10,'Configuración','icon-cogs',array('EPIs','Puestos','Instalaciones','Protocolos'),array('epis/','puestos-trabajo/','instalaciones/','protocolos/'),array('icon-shield','icon-sitemap','icon-building-o','icon-heartbeat'),$seccionActiva,array('TECNICO'));*/

          creaOpcionDesplegableMenu(10,'Configuración','icon-cogs',array('EPIs','Puestos','Actividades','Instalaciones','Protocolos','Enfermedades'),array('epis/','puestos-trabajo/','actividades/','instalaciones/','protocolos/','enfermedades'),array('icon-shield','icon-sitemap','icon-industry','icon-building-o','icon-heartbeat','icon-medkit'),$seccionActiva,array('COMERCIAL','TECNICO'));



          creaOpcionDesplegableMenu(14,'Documentación','icon-archive',array('Administración','Técnico','Vigilancia de la Salud','Memorias'),array('documentacion-cliente/?tipo=administracion','documentacion-cliente/?tipo=tecnico','documentacion-cliente/?tipo=vs','memorias-xml/'),array('icon-files-o','icon-edit','icon-stethoscope','icon-history'),$seccionActiva,obtenerTiposUsuarios(array('FACTURACION','EMPLEADO','MEDICO','ENFERMERIA','CLIENTE','ADMINISTRACION')));

          creaOpcionMenu(6,'Satisfacción','satisfaccion/','icon-star-half-o',$seccionActiva,obtenerTiposUsuarios(array('COLABORADOR','CLIENTE','EMPLEADO','FACTURACION','MEDICO','ENFERMERIA','ADMINISTRACION')));
          creaOpcionDesplegableMenu(7,'Incidencias','icon-exclamation-circle',array('Incidencias','Incidencias de software'),array('incidencias/','incidencias-software'),array('icon-exclamation-circle','icon-laptop'),$seccionActiva,obtenerTiposUsuarios(array('COLABORADOR','CLIENTE','EMPLEADO','MEDICO','ENFERMERIA','ADMINISTRACION')));
          
          creaOpcionMenu(12,'Empleados','empleados/','icon-group',$seccionActiva,array('CLIENTE'));
          //creaOpcionDesplegableMenu(array(28,29,30,31,32,33,45,13,46),'Facturación','icon-eur',array('Emisores','Estados recibos','Formas de pago','Cuentas bancarias propias','Facturas','Recibos','Remesas','Gastos'),array('emisores/','estados-recibos/','formas-pago/','cuentas-propias/','facturas/','recibos/','remesas/','facturacion-gastos/'),array('icon-list-ul','icon-cogs','icon-credit-card','icon-cc','icon-file-text','icon-ticket','icon-file-code-o','icon-money'),$seccionActiva,array('ADMIN','FACTURACION'));
          creaOpcionDesplegableMenu(array(28,29,30,31,32,33,45,46,47),'Facturación','icon-eur',array('Emisores','Estados recibos','Formas de pago','Cuentas bancarias propias','Facturas','Recibos','Gastos','Informe rentabilidad'),array('emisores/','estados-recibos/','formas-pago/','cuentas-propias/','facturas/','recibos/','facturacion-gastos/','informe-rentabilidad/'),array('icon-list-ul','icon-cogs','icon-credit-card','icon-cc','icon-file-text','icon-ticket','icon-money','icon-paste'),$seccionActiva,array('ADMIN','FACTURACION'));
          
          creaOpcionMenu(14,'Documentación','documentacion-empleado/','icon-files-o',$seccionActiva,array('EMPLEADO'));

          creaOpcionMenu(14,'Administración','documentacion-cliente/listado.php?tipo=administracion','icon-files-o',$seccionActiva,array('CLIENTE'));
          creaOpcionMenu(16,'Técnico','documentacion-cliente/listado.php?tipo=tecnico','icon-edit',$seccionActiva,array('CLIENTE'));
          creaOpcionMenu(17,'Vigilancia de la Salud','documentacion-cliente/listado.php?tipo=vs','icon-stethoscope',$seccionActiva,array('CLIENTE'));
          creaOpcionMenu(15,'Parte Diario','parte-diario/','icon-calendar',$seccionActiva,array('COLABORADOR','FACTURACION','MEDICO'));

          
          creaOpcionDesplegableMenu(array(8,16,17),'Interno','icon-lock',array('Usuarios','Correos','Documentos internos'),array('usuarios/','correos/','documentos-internos/'),array('icon-key','icon-envelope','icon-file-archive-o'),$seccionActiva,array('ADMIN'));

          creaOpcionMenu(16,'Correos','correos/','icon-envelope',$seccionActiva,array('TECNICO','MEDICO','FACTURACION'));
          creaOpcionMenu(17,'Documentos internos','documentos-internos/','icon-file-archive-o',$seccionActiva,array('TECNICO','MEDICO'));


          creaOpcionMenu(5,'Accidentes','accidentes-empleado/','icon-ambulance',$seccionActiva,array('EMPLEADO'));
          
          creaOpcionDesplegableMenu(10,'Configuración','icon-cogs',array('Actividades'),array('actividades/'),array('icon-industry'),$seccionActiva,array('FACTURACION'));

          creaOpcionDesplegableMenu(48,'Informes','icon-paste',array('Contratos sin planificación','Relación contrato-factura-planificación'),array('informe-contratos-sin-planificacion/','informe-contrato-factura-planificacion/'),array('icon-file-text','icon-list'),$seccionActiva,array('ADMIN'));

          //creaOpcionMenu(49, 'Memorias', 'memorias-xml/', 'icon-history', $seccionActiva, array('ADMIN'));

          //creaOpcionDesplegableMenu(15,'Citas','icon-clock-o',array('Centros médicos','Profesionales','Citas'),array('centros-citas/','profesionales-citas/','citas/'),array('icon-hospital-o','icon-user-md','icon-clock-o'),$seccionActiva,array('ADMIN'));

          //obtenerTiposUsuarios(array('COLABORADOR'));
      	?>

            

       
        </ul>
      </div>
      <!-- /container -->
    </div>
    <!-- /subnavbar-inner -->
  </div>
  <!-- /subnavbar -->
<?php
}
