<?php
    session_start();
    include_once("funciones.php");
    include_once('../../api/js/firma/signature-to-image.php');
    compruebaSesion();

    ob_start();
    $contenido=generaCertificado($_GET['codigo']);

    require_once('../../api/html2pdf/html2pdf.class.php');
    $html2pdf=new HTML2PDF('L','A4','es');
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->WriteHTML($contenido);
    ob_clean();
    $html2pdf->Output('Certificado.pdf');

function generaCertificado($codigo){
    
    $sql = "SELECT 
                c.codigo, 
                c.fecha, 
                c.modalidad,
                c.duracion,
                cl.EMPNOMBRE, 
                cl.EMPMARCA, 
                CONCAT(e.nombre,' ',e.apellidos) AS empleado, 
                p.nombre AS puesto, 
                CONCAT(u.nombre,' ',u.apellidos) AS tecnico, 
                e.dni, 
                c.riesgos, 
                u.codigo AS codigoTecnico, 
                cursos.nombre AS curso, 
                cursos.tipo AS tipoCurso, 
                cursos.codigo AS codigoCurso 
            FROM 
                certificados c 
            LEFT JOIN clientes cl ON 
                c.codigoCliente = cl.codigo 
            LEFT JOIN empleados e ON 
                c.codigoEmpleado = e.codigo 
            LEFT JOIN puestos_trabajo p ON 
                c.codigoPuesto = p.codigo 
            LEFT JOIN usuarios u ON 
                c.codigoTecnico = u.codigo 
            LEFT JOIN cursos ON 
                c.codigoCurso = cursos.codigo 
            WHERE 
                c.codigo = ".$codigo.";";

    $datos = consultaBD($sql, true, true);
    
    $modulos = consultaBD('SELECT * FROM cursos_modulos WHERE codigoCurso="'.$datos['codigoCurso'].'" ORDER BY numero',true);
    
    $firmasCodigo = array(
        145,
        146,
        150,
        1558,
        152,
        872
    );

    $firmas = array(
        145  => 'firmaAdan.png',
        146  => 'firmaAngeles.png',
        150  => 'firmaMarta.png',
        1558 => 'firmaChesco.png',
        152  => 'firmaPatricio.jpg',
        872  => 'firmaPatricio.jpg'
    );

    if(in_array($datos['codigoTecnico'], $firmasCodigo)){
        $firma='<img style="width:250px" src="../img/'.$firmas[$datos['codigoTecnico']].'" />';
    }  else {
        $firma='';
    }

    $duracion  = $datos['duracion']  == '' ? "2 h" : $datos['duracion'];
    $modalidad = $datos['modalidad'] == '' ? $datos['tipoCurso'] : $datos['modalidad'];

    //ESTILOS
    $contenido = "
        <style type='text/css'>
            <!--
                    body{
                        font-size:12px;
                        font-family: helvetica;
                        font-weight: lighter;
                        line-height: 24px;
                    }

                    #container{
                        background: url(../img/fondoOfertaCertificado.jpeg) center center no-repeat;
                        height:100%;
                    }

                    .content{
                        width:100%;
                        border:2px solid #075581;
                        margin:0px;
                        padding:0px;
                    }

                    h3{
                        margin-top:0px;
                        padding-top:0px;
                        color:#075581;
                        text-align:center;
                    }

                    p{
                        text-align:center;
                        font-size:22px;
                        line-height: 28px;
                    }

                    table{
                        width:100%;
                    }

                    ul{
                        padding-left:0px;
                        margin:0px;
                        list-style-type:none;
                        width:100%;
                        display:block;
                    }

                    ul li{
                        width:100%;
                        padding-left:0px;
                        margin:0px !important;
                    }

                    .listado-modulos {
                        font-size: 12px;
                        line-height: 20px;
                        text-align: left;
                        margin-left: 20px;
                    }
                }
            -->
        </style>
    ";

    // Página 1
    $contenido .= "
        <page backleft='30mm' backright='30mm'>
            <div id='container'>
                <div style='text-align:center'>
                    <img src='../img/logoCertificado.png' />
                </div>
                <h3>DIPLOMA DE FORMACIÓN</h3>
                <div class='content'>
                    <p>
                        ".$datos['empleado'].", con DNI ".$datos['dni']."<br/>
                        de la empresa ".$datos['EMPNOMBRE']."<br/>
                        ha realizado con éxito la acción formativa de<br/>
                        ".$datos['curso'].".<br/>Puesto: ".$datos['puesto'].".
                    </p>
                
                    <table>
                        <tr>
                            <td style='width:60%;'>Fecha de impartición: ".formateaFechaWeb($datos['fecha']).".</td>
                            <td style='width:20%;'>Duración: ".$duracion.".</td>
                            <td style='width:20%;'>Modalidad: ".$modalidad."</td>
                        </tr>
                    </table>
                </div>
                <br/>
                <div class='content'>
                    <table>
                        <tr>
                            <td style='width:70%;'>Firmado:</td>
                            <td style='width:30%;'>Firmado:<br/>".$datos['tecnico'].".</td>
                        </tr>
                        <tr>
                            <td style='width:70%;height:120px;'></td>
                            <td style='width:30%;height:120px;text-align:right;'>".$firma."</td>
                        </tr>
                        <tr>
                            <td style='width:70%;'>El alumno.</td>
                            <td style='width:30%;'>Tutor de la formación.<br/>TSPRL ANESCO</td>
                        </tr>
                    </table>
                </div>
            </div>
        </page>
    ";

    // Página 2
    $contenido .= "
        <page backleft='30mm' backright='30mm'>
            <div id='container'>
                <div class='content' style='height:650px;width:98%;'>
                    <p style='font-size:18px;'><b>CONTENIDO DEL CURSO</b></p>
                    <p class='listado-modulos'>
    ";
    
    $i = 1;    
    while ($item = mysql_fetch_assoc($modulos)) {
        if ($item['titulo'] == 'NO') {
            $contenido .= "&nbsp;&nbsp;&nbsp;".$item['numeroMostrar'].". ".$item['nombre']."<br>";
        }
        else {
            if ($i > 1) {
                $contenido .= '<br>';
            }
            
            $contenido .= "<span>MODULO ".$item['numeroMostrar']." ".$item['nombre']."</span><br>";
        }
        
        $i++;
    }

    if($datos['riesgos']!=''){
        $contenido .= "
            <br>MODULO Riesgos específicos del puesto de trabajo<br>
            ".nl2br($datos['riesgos'])
        ;
    }
    
    $contenido .= "
                    </p>
                </div>
                <p style='font-size:11px;'>Formación según lo establecido en el Art. 19 de la LEY 31/1995, de 8 de noviembre, de Prevención de Riesgos Laborales<br/>
                <span style='font-size:20px;font-weight:bold;color:#075581;'>www.anescoprl.es</span></p>
            </div>
        </page>
    ";

    return $contenido;
}

function a_romano($integer, $upcase = true) {
        $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 
                       'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9,   
                       'V'=>5, 'IV'=>4, 'I'=>1);
        $return = '';
        while($integer > 0) 
        {
            foreach($table as $rom=>$arb) 
            {
                if($integer >= $arb)
                {
                    $integer -= $arb;
                    $return .= $rom;
                    break;
                }
            }
        }
        return $return;
    }
?>