<?php
  $seccionActiva=5;
  include_once("../cabecera.php");
  gestionCertificados();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $('.selectpicker').selectpicker();

    obtieneEmpleados($('#codigoCliente option:selected').val());
    $('#codigoCliente').change(function(){
      obtieneEmpleados($(this).val());
    });

    $('#codigoEmpleado').change(function(){
      obtienePuesto($(this).val());
    });

    $(document).on('change', '#codigoCurso', function(e) {
      var valor = $(this).val();

      if (valor == 'NULL') {
        $('#modalidad').val('');
      }
      else {
        var consulta = $.post('../listadoAjax.php?include=certificados&funcion=obtieneModalidaAJAX();',
          {'valor':valor}).always(function(respuesta) 
        {
          $('#modalidad').val(respuesta);  
        });
      }

    });
  });

function obtieneEmpleados(codigoCliente){
  if(codigoCliente=='NULL'){

  } else {
    var consulta=$.post('../listadoAjax.php?include=certificados&funcion=obtieneEmpleados();',{'codigoCliente':codigoCliente});
    consulta.done(function(respuesta){
      $('#codigoEmpleado').html(respuesta);
      $('#codigoEmpleado').val($('#codigoEmpleadoOculto').val());
      $('#codigoEmpleado').selectpicker('refresh');
    });
  }
}

function obtienePuesto(codigoEmpleado){
  if(codigoEmpleado!='NULL'){
    var consulta=$.post('../listadoAjax.php?include=certificados&funcion=obtienePuesto();',{'codigoEmpleado':codigoEmpleado});
    consulta.done(function(respuesta){
      $('#codigoPuesto').val(respuesta);
      $('#codigoPuesto').selectpicker('refresh');
    });
  }
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>