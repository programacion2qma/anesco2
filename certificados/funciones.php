<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de incidencias

function operacionesCertificados(){
	$res=true;

	if (isset($_POST['codigo'])) {
		$res=actualizaDatos('certificados');
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=insertaDatos('certificados');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('certificados');
	}

	mensajeResultado('codigoCliente',$res,'Certificado');
    mensajeResultado('elimina',$res,'Certificado', true);
}


function gestionCertificados(){
	operacionesCertificados();
	
	abreVentanaGestionConBotones('Gestión de Certificados','index.php');
	$datos=compruebaDatos('certificados');
	abreColumnaCampos();
		campoFecha('fecha','Fecha',$datos);
		campoSelectClientesCertificado('codigoCliente','Cliente',$datos);
		campoSelect('codigoEmpleado','Empleado',array(''),array('NULL'));
		$codigoEmpleado=$datos?$datos['codigoEmpleado']:0;
		campoOculto($codigoEmpleado,'codigoEmpleadoOculto');
		campoSelectConsulta('codigoPuesto','Puesto de trabajo','SELECT codigo, nombre AS texto FROM puestos_trabajo',$datos);
		campoSelectConsulta('codigoTecnico','Técnico','SELECT codigo,CONCAT(nombre," ",apellidos) AS texto FROM usuarios WHERE tipo="TECNICO"',$datos);
		campoSelectConsulta('codigoCurso','Curso','SELECT codigo, nombre AS texto from cursos',$datos);
		campoTexto('duracion', 'Duracion', $datos, 'input-small');
		campoTexto('modalidad', 'Modalidad', $datos);
		areaTexto('riesgos','Riesgos específicos del puesto incluidos en la formación',$datos,'areaInforme'); 
	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}


function campoSelectClientesCertificado($nombreCampo,$texto,$valor,$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'",$tipo=0){
	$nombres=array('');
	$valores=array('NULL');
	$where='eliminado="NO"';
	if($valor && $valor['codigoCliente']!=NULL){
		$where.=' OR codigo='.$valor['codigoCliente'];
	}
	$listado=consultaBD('SELECT * FROM clientes WHERE '.$where,true);
	while($item=mysql_fetch_assoc($listado)){
		array_push($valores, $item['codigo']);
		$nombre='';
		if($item['EMPNOMBRE']!=''){
			$nombre=$item['EMPNOMBRE'];
		}
		if($item['EMPMARCA']!=''){
			if($nombre==''){
				$nombre=$item['EMPMARCA'];
			} else {
				if($nombre!=$item['EMPMARCA']){
					$nombre.=' / '.$item['EMPMARCA'];
				}
			}
		}
		array_push($nombres, $nombre);
	}
	campoSelect($nombreCampo,$texto,$nombres,$valores,$valor,$clase,$busqueda,$tipo);
}

function imprimeCertificados(){
	global $_CONFIG;
	$consulta=consultaBD("SELECT c.codigo, c.fecha, cl.EMPNOMBRE, cl.EMPMARCA, CONCAT(e.nombre,' ',e.apellidos) AS empleado, p.nombre AS puesto, CONCAT(u.nombre,' ',u.apellidos) AS tecnico, cursos.nombre AS curso FROM certificados c LEFT JOIN clientes cl ON c.codigoCliente=cl.codigo LEFT JOIN empleados e ON c.codigoEmpleado=e.codigo LEFT JOIN puestos_trabajo p ON c.codigoPuesto=p.codigo LEFT JOIN usuarios u ON c.codigoTecnico=u.codigo LEFT JOIN cursos ON c.codigoCurso=cursos.codigo",true);
	while($datos=mysql_fetch_assoc($consulta)){
		$nombre='';
		if($datos['EMPNOMBRE']!=''){
			$nombre=$datos['EMPNOMBRE'];
		}
		if($datos['EMPMARCA']!=''){
			if($nombre==''){
				$nombre=$datos['EMPMARCA'];
			} else {
				if($nombre!=$datos['EMPMARCA']){
					$nombre.=' / '.$datos['EMPMARCA'];
				}
			}
		}
		$boton="<li><a target='_blank' class='noAjax' href='".$_CONFIG['raiz']."certificados/generaCertificado.php?codigo=".$datos['codigo']."'><i class='icon-cloud-download'></i> Descargar</i></a></li>";
		if($datos['curso']==''){
			$boton="<li><a class='noAjax' onClick='alert(\"Este certificado no tiene seleccionado ningún curso\");' href='javascript:void(0);'><i class='icon-cloud-download'></i> Descargar</i></a></li>";
		}
		echo "<tr>
				<td> ".formateaFechaWeb($datos['fecha'])." </td>
				<td> ".$nombre." </td>
				<td> ".$datos['empleado']." </td>
				<td> ".$datos['puesto']." </td>
				<td> ".$datos['tecnico']." </td>
				<td> ".$datos['curso']." </td>
				<td>
					<div class='btn-group centro'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
						<ul class='dropdown-menu' role='menu'>
							<li><a href='".$_CONFIG['raiz']."certificados/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li>
							<li class='divider'></li>
							".$boton."
						</ul>
					</div>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	  	</td>
			 </tr>";
	}
}

function obtieneEmpleados(){
	$res='<option value="NULL"></option>';
	$listado=consultaBD('SELECT * FROM empleados WHERE eliminado="NO" AND codigoCliente='.$_POST['codigoCliente'],true);
	while($item=mysql_fetch_assoc($listado)){
		$res.='<option value="'.$item['codigo'].'">'.$item['nombre'].' '.$item['apellidos'].'</option>';
	}
	echo $res;
}

function obtienePuesto(){
	$empleado=datosRegistro('empleados',$_POST['codigoEmpleado']);
	echo $empleado['codigoPuestoTrabajo'];
}

function obtieneModalidaAJAX() {

	$res = '';

	$codigo = $_POST['valor'];

	$consulta = consultaBD("SELECT tipo FROM cursos WHERE codigo = ".$codigo.";", true, true);

	$res = $consulta['tipo'];

	echo $res;

}