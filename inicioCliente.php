<?php
  $seccionActiva=0;
  include_once('cabecera.php');

  $meses=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
  compruebaVencimientosRemesas();
?>

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

         <div class="span6">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-home"></i>
              <h3>Inicio</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Bienvenido/a <?php echo $_SESSION['usuario']; ?>. A continuación se muestra el estado del sistema:</h6>
                      <?php

                      if($_SESSION['tipoUsuario']=='CLIENTE'){

                        $datos=consultaBD("SELECT codigoCliente FROM usuarios_clientes WHERE codigoCliente IS NOT NULL AND codigoUsuario='".$_SESSION['codigoU']."';",true,true);
                        $codigoCliente=$datos['codigoCliente'];

                        echo '<div id="big_stats" class="cf">';

                            creaEstadisticaInicio('icon-group','Empleado/s','empleados','codigo',"eliminado='NO' AND codigoCliente='$codigoCliente'",'empleados/');
                            creaEstadisticaInicio('icon-paste','Contrato/s activos','contratos','codigo',"eliminado='NO' AND enVigor='SI' AND codigoOferta IN(SELECT codigo FROM ofertas WHERE codigoCliente='$codigoCliente' AND eliminado='NO')",'documentacion-cliente/listado.php?tipo=administracion');
                            creaEstadisticaInicio('icon-eur','Factura/s','facturas','codigo',"eliminado='NO' AND codigoCliente='$codigoCliente' AND tipoFactura!='ABONO'",'documentacion-cliente/listado.php?tipo=administracion');

                        echo '</div>';
                      }
                      ?>
                   
                </div> <!-- /widget-content -->
                <!-- /widget-content -->
              </div>
            </div>
          </div>
        </div>


        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-th"></i>
              <h3>Opciones</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
                <div class="shortcuts">
                    <a href="javascript:void(0);" id='guia' class="shortcut"><i class="shortcut-icon icon-book"></i><span class="shortcut-label">Guía rápida</span> </a>
                    <a href="cerrarSesion.php" class="shortcut"><i class="shortcut-icon icon-power-off"></i><span class="shortcut-label">Cerrar sesión</span> </a>
                <?php
                    if($_SESSION['tipoUsuario']=='CLIENTE'){
                        echo "
                        <br />
                        <a href='informacion-covid-19/' class='shortcut botonCovid'> <span class='badge'><i class='icon-exclamation'></i></span> <i class='shortcut-icon icon-home'></i> &nbsp; <i class='shortcut-icon icon-laptop'></i><span class='shortcut-label'>INFORMACIÓN IMPORTANTE: TELETRABAJO - COVID 19 </span> </a>";
                    }
                ?>

                </div>
              <!-- /shortcuts -->
            </div>
            <!-- /widget-content -->
          </div>
        </div>

      </div><!-- /row -->
      <?php if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='TECNICO'){?>
      <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-shopping-cart"></i>
          <h3>Clientes</h3>
        </div>
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable" id='target-3'>
            <thead>
              <tr>
                  <th> Nº Cliente </th>
                  <th> Razón Social </th>
                  <th> Nombre comercial </th>
                  <th> Ofertas </th>
                  <th> Contratos </th>
                  <th> Planificaciones </th>
                  <th> Facturas </th>
                  <th> Es cliente </th>
              </tr>
            </thead>
            <tbody>

              <?php
                imprimeClientesInicio();
              ?>

            </tbody>
          </table>
        </div>
      </div>

      <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-calendar"></i>
          <h3>Tareas para hoy y fuera de plazo</h3>
        </div>
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                  <th> Fecha </th>
                  <th> Empresa </th>
                  <th> Localidad </th>
                  <th> Teléfono </th>
                  <th> Tarea </th>
                  <th> Técnico </th>
                  <th> Prioridad </th>
                  <th> </th>
              </tr>
            </thead>
            <tbody>

              <?php
                imprimeTareasInicio();
              ?>

            </tbody>
          </table>
        </div>
      </div>
      <? 
      }
      elseif($_SESSION['tipoUsuario']=='CLIENTE'){
        $cliente=obtieneCodigoCliente();
      ?>

      <div class="row">
        <div class="span12">        
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Empleados registrados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable" id='target-3'>
                <thead>
                  <tr>
                    <th> Nº Empleado</th>
                    <th> Nombre </th>
                    <th> Apellidos </th>
                    <th id="target-3"> DNI </th>
                    <th> Sexo </th>
                    <th> Teléfono </th>
                    <th> eMail </th>
                    <th class="centro"></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    imprimeEmpleados($cliente,'NO',true);
                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

        </div>
      </div>

      <?php
      }
      ?>

    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/guidely/guidely.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){

  guidely.add ({
    attachTo: '#target-1'
    , anchor: 'top-left'
    , title: 'Panel de Estadísticas'
    , text: 'En el lado izquierdo de su pantalla encontrará el Panel de Estadísticas, que le servirá para ver de forma rápida y visual el estado del sistema.'
  });

  guidely.add ({
    attachTo: '#target-2'
    , anchor: 'top-right'
    , title: 'Panel de Acciones'
    , text: 'En este panel se agrupan las distintas opciones disponibles en cada sección (crear un nuevo elemento, acceder a una subsección, generar un fichero, ...).<br>Para acceder a una opción solo tiene que pulsar el botón correspondiente.'
  });

  guidely.add ({
    attachTo: '#target-3'
    , anchor: 'middle-middle'
    , title: 'Listado de Elementos'
    , text: 'Esta tabla muestra un listado en el que podrá consultar los elementos creados en cada sección. Utilice la caja de <b>Búsqueda</b> para filtrar los registros a visualizar.'
  });

  guidely.add ({
    attachTo: '#target-4'
    , anchor: 'middle-middle'
    , title: 'Menú principal'
    , text: 'Una vez que accede al sistema, el Menú Principal está siempre disponible para permitirle navegar entre los distintos módulos de la herramienta. Solo tiene que pulsar en la sección correspondiente para acceder a ella.'
  });

    guidely.add ({
    attachTo: '#target-5'
    , anchor: 'bottom-right'
    , title: 'Menú de Usuario'
    , text: 'Por último, el Menú de Usuario le recuerda en cada momento con que cuenta está trabajando. Además, haciendo click en él se le proporciona un acceso directo para cerrar su sesión.'
  });

  $('#guia').click(function(){
    guidely.init({welcome: true, startTrigger: false, welcomeTitle: 'Guía de uso básico', welcomeText: 'Bienvenido a la Guía de uso del sistema. Pulsando en el botón Iniciar, la plataforma le mostrará unas sencillas explicaciones sobre los distintos elementos que encontrará en la mayoría de las secciones.' });
  });

});

</script>

<!-- /contenido -->
</div>

<?php include_once('pie.php'); ?>
