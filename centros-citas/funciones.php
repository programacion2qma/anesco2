<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de centros médicos

function operacionesCentrosMedicos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaCentroMedico();
	}
	elseif(isset($_POST['nombre'])){
		$res=creaCentroMedico();
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminado('centros_medicos',$_POST['elimina']);
	}

	mensajeResultado('nombre',$res,'centro médico');
    mensajeResultado('elimina',$res,'centro médico', true);
}

function creaCentroMedico(){
	$res=true;
	
	$res=insertaDatos('centros_medicos');

	if($res){
		$res=insertaSalasCentroMedico($res);
	}

	return $res;
}

function actualizaCentroMedico(){
	$res=true;

	$res=actualizaDatos('centros_medicos');

	if($res){
		$res=insertaSalasCentroMedico($_POST['codigo']);
	}

	return $res;
}

function insertaSalasCentroMedico($codigoCentroMedico){
	$res=true;
	$datos=arrayFormulario();

	for($i=0;isset($datos["codigoSala$i"]) || isset($datos["nombreSala$i"]);$i++){

		if(isset($datos['codigoSala'.$i]) && isset($datos['nombreSala'.$i])){//Actualización
            $res=$res && consultaBD("UPDATE salas_centros_medicos SET nombre='".$datos["nombreSala".$i]."' WHERE codigo=".$datos['codigoSala'.$i].";");
        }
        elseif(!isset($datos['codigoSala'.$i]) && isset($datos['nombreSala'.$i])){//Inserción
            $res=$res && consultaBD("INSERT INTO salas_centros_medicos VALUES(NULL,'".$datos['nombreSala'.$i]."','$codigoCentroMedico');");
        }
        else{//Eliminación
            $res=$res && consultaBD("DELETE FROM salas_centros_medicos WHERE codigo='".$datos['codigoSala'.$i]."';");
        }
    }

	return $res;
}


function gestionCentroMedico(){
	operacionesCentrosMedicos();
	
	abreVentanaGestionConBotones('Gestión de centros médicos','index.php','span3');
	$datos=compruebaDatos('centros_medicos');

	campoTexto('nombre','Nombre',$datos);
	tablaSalas($datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	areaTexto('observaciones','Observaciones',$datos);	
	campoOculto($datos,'eliminado','NO');

	cierraVentanaGestion('index.php',true);
}

function tablaSalas($datos){
	echo "
	    <div class='control-group'>                     
	        <label class='control-label'>Salas disponibles en el centro:</label>
	        <div class='controls'>
	            <table class='tabla-simple mitadAncho' id='tablaSalas'>
	                <thead>
	                    <tr>
	                        <th> Nombre sala </th>
	                        <th> </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos){
	                $consulta=consultaBD("SELECT * FROM salas_centros_medicos WHERE codigoCentroMedico=".$datos['codigo']);
	                while($sala=mysql_fetch_assoc($consulta)){
	                	imprimeLineaTablaSalas($sala,$i);
	                    
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	            	imprimeLineaTablaSalas(false,$i);
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaSalas\");'><i class='icon-plus'></i> Añadir sala</button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaSalas\");'><i class='icon-trash'></i> Eliminar sala</button>
	        </div>
	    </div>";
}

function imprimeLineaTablaSalas($datos,$i){
	$j=$i+1;

	if($datos){
		campoOculto($datos['codigo'],'codigoSala'.$i);
	}

	echo "<tr>";
			campoTextoTabla('nombreSala'.$i,$datos['nombre'],'span3');
	echo "	<td><input type='checkbox' name='filasTabla[]' value='$j' /></td>
		  </tr>";
}


function imprimeCentrosMedicos($eliminado){
	$consulta=consultaBD("SELECT * FROM centros_medicos WHERE eliminado='$eliminado';",true);
	while($datos=mysql_fetch_assoc($consulta)){

		echo "<tr>
				<td> ".$datos['nombre']." </td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	  	</td>
			 </tr>";
	}
}

//Fin parte de centros médicos