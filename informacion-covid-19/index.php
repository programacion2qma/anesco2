<?php
$seccionActiva=10;
include_once('../cabecera.php');
?> 

<div class="main" id="contenido">
    <div class="main-inner">
        <div class="container">
            <div class="row">


                <div class="span12 margenAb">
                    <div class="widget">
                        <div class="widget-header"> 
                            <i class='shortcut-icon icon-home'></i> <i class='shortcut-icon icon-laptop'></i>
                            <h3>INFORMACIÓN IMPORTANTE: TELETRABAJO - COVID 19</h3>
                            <div class='pull-right'>
                                <a href='../inicio.php' class='btn btn-small btn-default'><i class='icon-chevron-left'></i> Volver</a>
                            </div>
                        </div>
                        <!-- /widget-header -->
                        <div class="widget-content documentosCovid">
                            <div class="shortcuts">
                                <a href="<?=$_CONFIG['raiz']?>documentacion/covid19/ULTIMA-HORA-13-DE-ABRIL-SOBRE-REANUDACION-ACTIVIDADED-NO-ESENCIALES.pdf" class="shortcut noAjax" target='_blank'><i class="shortcut-icon icon-exclamation-circle"></i><span class="shortcut-label">ÚLTIMA HORA SOBRE<br />REANUDACIÓN DE ACTIVIDADES NO ESENCIALES<br />(13 DE ABRIL)</span></a>
                                <a href="<?=$_CONFIG['raiz']?>documentacion/covid19/Modelo1-Comunicacion-TRABAJADOR-A-EMPRESA-ESP-SENS.doc" class="shortcut noAjax" target='_blank'><i class="shortcut-icon icon-edit"></i><span class="shortcut-label">COMUNICACIÓN DEL TRABAJADOR A LA EMPRESA DE LA CONDICIÓN DE ESPECIALMENTE SENSIBLE  EN RELACION A LA INFECCION DEL CORONAVIRUS SARS-CoV-2</span></a>
                                <a href="<?=$_CONFIG['raiz']?>documentacion/covid19/Modelo2-COMUNICACION-EMPRESA-A-ANESCO-ESP-SENS.docx" class="shortcut noAjax" target='_blank'><i class="shortcut-icon icon-edit"></i><span class="shortcut-label">COMUNICACIÓN DE LA EMPRESA AL SERVICIO DE PREVENCIÓN DE LA EXISTENCIA DE TRABAJADOR ESPECIALMENTE SENSIBLE  EN RELACION A LA INFECCION DEL CORONAVIRUS SARS-CoV-2</span></a>
                                <a href="<?=$_CONFIG['raiz']?>documentacion/covid19/Modelo3-COMUNICACION-EMPRESA-A-ANESCO-DE-CASOS.doc" class="shortcut noAjax" target='_blank'><i class="shortcut-icon icon-edit"></i><span class="shortcut-label">MODELO DE COMUNICACIÓN DE CONTACTOS ESTRECHOS Y CASOS  POSIBLES,  PROBABLES O CONFIRMADOS</span></a>

                                <a href="<?=$_CONFIG['raiz']?>documentacion/covid19/CHECK-LIST-TELETRABAJO.pdf" class="shortcut noAjax" target='_blank'><i class="shortcut-icon icon-edit"></i><span class="shortcut-label">CUESTIONARIO DE AUTOEVALUACIÓN DE LAS CONDICIONES DEL TELETRABAJO</span> </a>
                                <a href="<?=$_CONFIG['raiz']?>documentacion/covid19/FICHA-PRL-ATENCION-TELEFONICA.pdf" class="shortcut noAjax" target='_blank'><i class="shortcut-icon icon-info-circle"></i><span class="shortcut-label">FICHA INFORMATIVA DE PREVENCIÓN DE RIESGOS LABORALES</span> </a>
                                <a href="<?=$_CONFIG['raiz']?>documentacion/covid19/FICHA-PRL-USO-DE-PORTATIL.pdf" class="shortcut noAjax" target='_blank'><i class="shortcut-icon icon-info-circle"></i><span class="shortcut-label">FICHA INFORMATIVA DE PREVENCIÓN DE RIESGOS LABORALES</span> </a>
                                <a href="<?=$_CONFIG['raiz']?>documentacion/covid19/teletrabajo-y-prevencion-de-riesgos-laborales.pdf" class="shortcut noAjax" target='_blank'><i class="shortcut-icon icon-file-text-o"></i><span class="shortcut-label">TELETRABAJO Y PREVENCIÓN DE RIESGOS LABORALES (CEOE)</span> </a>
                                <a href="<?=$_CONFIG['raiz']?>documentacion/covid19/FICHA-PRL-TELETRABAJO.pdf" class="shortcut noAjax" target='_blank'><i class="shortcut-icon icon-info-circle"></i><span class="shortcut-label">FICHA INFORMATIVA DE PREVENCIÓN DE RIESGOS LABORALES PARA PUESTOS EN TELETRABAJO</span> </a>
                            </div>
                            <!-- /shortcuts --> 
                        </div>
                        <!-- /widget-content --> 
                    </div>
                </div>


            </div>
        </div>
        <!-- /container --> 
    </div>
    <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>