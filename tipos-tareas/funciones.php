<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales
include_once('../../api/js/firma/signature-to-image.php');
include_once('../Mobile_Detect.php');
//Inicio de funciones específicas


//Parte de gestión tipos de tareas


function operacionesTiposTareas(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('tipos_tareas');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('tipos_tareas');
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoItem($_POST['elimina'],'tipos_tareas');
	}

	mensajeResultado('nombre',$res,'tipo de tarea');
    mensajeResultado('elimina',$res,'tipo de tarea', true);
}



function gestionFormaPago(){
	operacionesTiposTareas();
	
	abreVentanaGestionConBotones('Gestión de tipos de tareas','index.php');
	$datos=compruebaDatos('tipos_tareas');

	campoTexto('nombre','Nombre tipo',$datos,'span3');
	campoOculto('NO','eliminado','SI');

	cierraVentanaGestion('index.php');
}



function imprimeTiposTareas($eliminado){
	global $_CONFIG;

	$consulta=consultaBD("SELECT * FROM tipos_tareas WHERE eliminado='$eliminado';",true);

	while($datos=mysql_fetch_assoc($consulta)){

		echo "
		<tr>
			<td>".$datos['nombre']."</td>
			<td class='centro'><a class='btn btn-propio' href='".$_CONFIG['raiz']."tipos-tareas/gestion.php?codigo=".$datos['codigo']."'><i class='icon-edit'></i> Editar</i></a></td>
			<td class='centro'>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' />
        	</td>
		</tr>";
	}
}

//Fin parte de gestión de tipos de tareas