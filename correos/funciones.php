<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales
include_once('../documentos-vs/funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de correos

function operacionesCorreos(){
	$res=true;
	
	if(isset($_POST['asunto'])){
    	$res=enviaCorreos();
  	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('correos');
	}

	mensajeResultado('asunto',$res,'Correo');
    mensajeResultado('elimina',$res,'Correo', true);
}


function gestionCorreo(){
	abreVentanaGestionConBotones('Envio de correos','index.php',' ','icon-edit','',true,'noAjax');
	$datos=obtieneDatosCorreos();

	if($datos){
		$usuario=datosRegistro('usuarios',$datos['codigoUsuario']);
		campoTexto('remitente','Remitente',$usuario['email']);
		campoTexto('usuario','Usuario',$usuario['usuario']);
		campoOculto($datos['tipo'],'tipo');
	} 
	else {
		campoOculto($_SESSION['codigoU'],'codigoUsuario');
		campoOculto('USUARIO','tipo');
	}

	campoFecha('fecha','Fecha',$datos);
	campoHora('hora','Hora',$datos);
	campoDestinatariosAsuntoCorreo($datos);
	areaTexto('mensaje','Mensaje',$datos,'areaMensajeCorreo');
	campoOculto($datos,'codigoContrato','NULL');
	
	campoAdjuntoCorreo($datos);

	cierraVentanaGestion('index.php',false,true,'Enviar correo','icon-send');
}

function obtieneDatosCorreos(){
	$res=compruebaDatos('correos');

	if(!$res && isset($_GET['tipo']) && isset($_GET['codigoContrato'])){

		$tipo=$_GET['tipo'];
		$codigoContrato=$_GET['codigoContrato'];
		
		$dia=date('d');
		$mes=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
		$mes=$mes[date('m')];
		$anio=date('Y');

		$contrato=consultaBD("SELECT contratos.codigo, EMPNOMBRE AS nombre, EMPDIR AS direccion, EMPCP AS cp, EMPLOC AS localidad, EMPPC AS contacto, EMPPCEMAIL AS email, 
							  CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS tecnico, usuarios.email AS emailTecnico, usuarios.telefono, usuarioCliente.usuario, 
							  usuarioCliente.clave

					 		  FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
					 		  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
					 		  LEFT JOIN formularioPRL on contratos.codigo=formularioPRL.codigoContrato
					 		  LEFT JOIN usuarios ON codigoUsuarioTecnico=usuarios.codigo
					 		  LEFT JOIN usuarios_clientes ON clientes.codigo=usuarios_clientes.codigoCliente
					 		  LEFT JOIN usuarios usuarioCliente ON usuarios_clientes.codigoUsuario=usuarioCliente.codigo

					 		  WHERE contratos.codigo='$codigoContrato';",true,true);


		$res=array(
			'codigoUsuario'		=>	$_SESSION['codigoU'],
			'tipo'				=>	$tipo,
			'fecha'				=>	fechaBD(),
			'hora'				=>	horaBD(),
			'destinatarios'		=>	$contrato['email'],
			'asunto'			=>	'',	//Se define abajo
			'mensaje'			=>	'',	//Se define abajo
			'ficheroAdjunto'	=>	'',	//Se define abajo
			'codigoContrato'	=>	$codigoContrato,
		);


		if($tipo=='VS0'){
			obtieneDatosTipoVS0($contrato,$dia,$mes,$anio,$res);
		}
		elseif($tipo=='VS1'){
			obtieneDatosTipoVS1($contrato,$dia,$mes,$anio,$res);	
		}
		elseif($tipo=='VS2'){
			obtieneDatosTipoVS2($contrato,$dia,$mes,$anio,$res);	
		}
		elseif($tipo=='VS3'){
			obtieneDatosTipoVS3($contrato,$dia,$mes,$anio,$res);	
		}
		elseif($tipo=='VS4'){
			obtieneDatosTipoVS4($contrato,$dia,$mes,$anio,$res);	
		}
		elseif($tipo=='VS5'){
			obtieneDatosTipoVS5($contrato,$dia,$mes,$anio,$res);	
		}
		elseif($tipo=='VS6'){
			obtieneDatosTipoVS6($contrato,$dia,$mes,$anio,$res);	
		}
		elseif($tipo=='VS7'){
			obtieneDatosTipoVS7($contrato,$dia,$mes,$anio,$res);	
		}


	}

	return $res;
}


function obtieneDatosTipoVS0($contrato,$dia,$mes,$anio,&$res){
	extract($contrato);

	$res['asunto']='PRESENTACIÓN - ANESCO';

	$res['mensaje']=
	"
	<div style='text-align:right'>
		$nombre<br />
		$direccion<br />
		$cp $localidad<br />
		<br />
		<br />
		Sevilla, a $dia de $mes $anio<br />
	</div>
	<div style='text-align:justify'>
	<br />
	Estimado/a Sr/Sra. $contacto:<br />
	<br />
	Tras la firma del contrato con nuestro Servicio de Prevención, queremos agradecerle la confianza depositada en nosotros y le anunciamos que, próximamente, el técnico asignado a su empresa  contactará con ustedes para concertar una visita a sus instalaciones,  conocerles personalmente e informarles de la programación de actividades relacionadas con el concierto preventivo.<br />
	<br />
	Igualmente, le adelantamos que para el desarrollo de nuestra actividad precisaremos de diversa documentación que podrá  ser remitida por correo electrónico a la dirección que le comunicará el técnico asignado o bien ser entregada en el momento de la visita.<br />
	<br />
	En principio, la documentación básica que sería necesaria es la siguiente:<br />
	<br />
	-	Listado de trabajadores con nombre y apellidos, DNI/NIF, fecha de nacimiento y categoría laboral, por cada uno de los puestos de trabajo de su empresa.<br />
	-	Listados de equipos de trabajo que utilizan.<br />
	-	Relación de productos químicos o sustancias que utilizan.<br />
	-	Relación de instalaciones (instalaciones contraincendios, ascensores, aparatos a presión, instalaciones de gas,...).<br />
	-	Relación de otras empresas que le presten servicio (limpieza, seguridad, mantenimiento,...). <br />
	-	Evaluación de riesgos realizada con anterioridad, en su caso.<br />
	-	Datos de siniestralidad laboral (accidentes de trabajo y enfermedades profesionales) de los últimos años.<br />
	<br />
	<br />
	Sin otro particular, le saluda atentamente:
	<br />
	<br />
	<br />
	</div>
	<div style='text-align:center'>Fdo.:</div> <br />
	<br />
	<br />
	<div style='text-align:right'>Dirección Gerencia  ANESCO SALUD Y PREVENCIÓN S.L.</div>";

	return $res;
}


function obtieneDatosTipoVS1($contrato,$dia,$mes,$anio,&$res){
	extract($contrato);

	$res['asunto']='PRESENTACIÓN TÉCNICO - ANESCO';

	$res['mensaje']=
	"
	<div style='text-align:justify'>
	Estimados Sres. <br />
	<br />
	Mi nombre es $tecnico, y soy el técnico de prevención asignado a su empresa  para la realización de la Evaluación de Riesgos y de todas aquellas actividades relacionadas con el concierto preventivo.<br />
	<br />
 	La intención de este correo es la de facilitarles mis datos de contacto por si tuviese cualquier duda o consulta y comunicarles que me pondré en contacto con ustedes para concertar una visita a sus instalaciones en la que pueda  conocerles personalmente, informarles de la programación de actividades relacionadas con el concierto preventivo y realizar la toma de datos necesaria para la elaboración del informe de evaluación de riesgos y la correspondiente Planificación Preventiva.<br />
 	<br />
	En este sentido,  y tal como le habrán comunicado en un correo anterior, necesitaremos disponer de cierta información y documentación que podrá  ser remitida a mi dirección de correo electrónico o bien serme entregada en el momento de la visita.<br />
	<br />
	En principio, la documentación básica que sería necesaria es la siguiente:<br />
	<br />
	-	Listado de trabajadores con nombre y apellidos, DNI/NIF, fecha de nacimiento y categoría laboral, por cada uno de los puestos de trabajo de su empresa.<br />
	-	Listados de equipos de trabajo que utilizan.<br />
	-	Relación de productos químicos o sustancias que utilizan.<br />
	-	Relación de instalaciones (instalaciones contraincendios, ascensores, aparatos a presión, instalaciones de gas,...).<br />
	-	Relación de otras empresas que le presten servisio (limpieza, seguridad, mantenimiento,...).<br /> 
	-	Evaluación de riesgos realizada con anterioridad, en su caso.<br />
	-	Datos de siniestralidad laboral (accidentes de trabajo y enfermedades profesionales) de los últimos años.<br />
	<br />
	Por último, indicarle que para acceder a los distintos documentos de su empresa que estarán disponibles en nuestra plataforma, , deberán entrar en www.anescoprl.es e introducir los siguientes campos:<br />
	<br />
	USUARIO: $usuario<br />
	CLAVE: $clave<br />
	<br />
	El día de la visita, le aclararé personalmente el sistema de acceso y cuantas dudas puedan planteársele al respecto.<br />
	<br />
	Si desea realizar alguna consulta, puede contactar conmigo mediante el correo y teléfono que les indico a continuación:<br />
	<br />
    <br />                
	Teléfono: $telefono<br />
	e-Mail: $emailTecnico<br />
	<br />
	Sin otro particular, le saluda atentamente:<br />
	<br />
	</div>
	<div style='text-align:right'>Fdo.: $tecnico</div>";

	return $res;
}

function obtieneDatosTipoVS2($contrato,$dia,$mes,$anio,&$res){
	extract($contrato);

	$primerMensaje=consultaBD("SELECT fecha FROM correos WHERE codigoContrato='$codigo' AND tipo='VS1';",true,true);

	$fecha=formateaFechaWeb($primerMensaje['fecha']);

	$res['asunto']='SEGUNDA COMUNICACIÓN - ANESCO';

	$res['mensaje']=
	"
	<div style='text-align:justify'>
	Estimados Sres. <br />
	<br />
	Tal como les comuniqué en correo de fecha $fecha, he intentado contactar con ustedes por teléfono para concertar una visita sin que hasta la fecha, me haya sido posible confirmarla.<br />
	<br />
 	Les ruego que, en la medida de lo posible, respondan a este e-mail indicando fecha de la semana próxima en la que podría personarme en sus instalaciones.<br />
 	<br />
 	Les recuerdo la necesidad de disponer de la documentación solicitada.<br />
 	<br />
 	<br />
	Sin otro particular, le saluda atentamente:<br />
	<br />
	<br />
	<br />
	</div>
	<div style='text-align:right'>Fdo.: $tecnico</div>";

	return $res;
}


function obtieneDatosTipoVS3($contrato,$dia,$mes,$anio,&$res){
	extract($contrato);

	$res['asunto']='ENVÍO INFORMES E.R. - ANESCO';

	$res['mensaje']=
	"
	<div style='text-align:justify'>
	Estimados Sres.  <br />
	<br />
	Les adjunto a este correo los informes de Evaluación de riesgos de su empresa, junto con la propuesta de Planificación Preventiva.<br />
	<br />
 	Tal como comentamos en nuestra primera reunión, a partir de ahora la empresa debe designar responsables y plazos de ejecución de las acciones  y, posteriormente, proceder a la aprobación de dicha Planificación previa consulta con los representantes de los trabajadores, en su caso.<br />
  	<br />
	También  les remito archivo con información preventiva general  que deben distribuir a sus trabajadores acompañado del recibí de la misma que deberán recogerle a cada uno y remitírnoslo debidamente firmado, así como Manuales de formación para los puestos de trabajo descritos en la Evaluación de riesgos, acompañados de unos cuestionarios que deberán ser cumplimentados por los trabajadores y remitidos a ANESCO SALUD Y PREVENCIÓN, S.L. por correo electrónico o por cualquier otro medio.<br />
	<br />
	Para la  programación de la impartición de esta formación  en su modalidad presencial, quedamos a la espera de que nos comuniquen las fechas en las que sus trabajadores pudieran desplazarse a nuestras  instalaciones. <br />
 	<br />
	Para cualquier duda o consulta, pueden ponerse en contacto conmigo por correo electrónico o por teléfono.<br />
	<br />
	Sin otro particular, le saluda atentamente:<br />
	<br />
	<br />
	</div>
	<div style='text-align:right'>Fdo.: $tecnico</div>";

	return $res;
}

function obtieneDatosTipoVS4($contrato,$dia,$mes,$anio,&$res){
	extract($contrato);

	$res['asunto']='RECORDATORIO OBLICACIÓN V.S. - ANESCO';

	$res['mensaje']=
	"
	<div style='text-align:justify'>
	Estimados Sres.  <br />
	<br />
	Le recordamos que, en  cumplimiento de la obligación recogida en el artículo 22 de la Ley 31/95 de Prevención de Riesgos Laborales, el empresario debe poner a disposición de todo su personal empleado los medios suficientes y necesarios para su vigilancia de la salud. <br />
	<br />
	El carácter de tales reconocimientos según se contempla en el citado artículo, tendrá el carácter voluntario para el personal empleado de la empresa, salvo en los casos en que la realización de estos sean imprescindibles para evaluar los efectos de las condiciones de trabajo sobre la salud del personal empleado o para verificar si el estado de salud del personal puede constituir un peligro para el mismo, para los demás, o para otras personas relacionadas con la empresa o así lo diga una disposición normativa.<br />
	<br />
	En virtud de lo expuesto,  le rogamos nos remita el listado del personal que se realizará el reconocimiento médico para dar traslado de dicha información al área de Vigilancia de la Salud del Servicio de Prevención de ANESCO para la concertación de la correspondiente cita.<br />
	<br />
	<br />
	</div>
	<div style='text-align:right'>Fdo.: $tecnico</div>";

	return $res;
}



function obtieneDatosTipoVS5($contrato,$dia,$mes,$anio,&$res){
	extract($contrato);

	$res['asunto']='RENOVACIÓN CONTRATO - ANESCO';

	$res['mensaje']=
	"
	<div style='text-align:right'>
		$nombre<br />
		$direccion<br />
		$cp $localidad<br />
		<br />
		<br />
		Sevilla, a $dia de $mes $anio<br />
	</div>
	<div style='text-align:justify'>
	<br />
	Estimado/a Sr/Sra. $contacto:<br />
	<br />
	Nuevamente queremos agradecerle la confianza depositada en nosotros al haber renovado recientemente el contrato con nuestro Servicio de Prevención.<br />
	<br />
 	Próximamente, el técnico asignado a su empresa  contactará con ustedes para recabar información sobre los cambios que pudieran haberse producido, ya sea por modificación del número de trabajadores o por cualquier otra circunstancia y remitirles la Memoria correspondiente a las actividades preventivas realizadas en el presente ejercicio.<br />
	<br />
	<br />
	Sin otro particular, le saluda atentamente:
	<br />
	<br />
	<br />
	</div>
	<div style='text-align:center'>Fdo.:</div> <br />
	<br />
	<br />
	<div style='text-align:right'>Dirección Gerencia  ANESCO SALUD Y PREVENCIÓN S.L.</div>";

	return $res;
}


function obtieneDatosTipoVS6($contrato,$dia,$mes,$anio,&$res){
	extract($contrato);

	$res['asunto']='MEMORIA ANUAL Y CUESTIONARIO DE SATISFACCIÓN - ANESCO';

	$res['mensaje']=
	"
	<div style='text-align:justify'>
	Estimados Sres.  <br />
	<br />
	Habiendo finalizado el año, les remito la Memoria anual de  actividades preventivas correspondiente al pasado año, así como un cuestionario de satisfacción para valorar nuestro servicio que le agradeceríamos nos remitiese cumplimentado a nuestra dirección de correo.<br />
	<br />
 	Esta información es de vital importancia para nosotros ya que es nuestro deseo mejorar todos los aspectos que sean posibles, siempre en beneficio de nuestros clientes. Le llevará muy poco tiempo, pero nos servirá de mucho.<br />
 	<br />
 	<br />
	Sin otro particular,  y agradeciéndole de antemano su colaboración, le saluda atentamente:<br />
	<br />
	<br />
	</div>
	<div style='text-align:right'>Fdo.: Dirección Gerencia ANESCO</div>";

	return $res;
}


function obtieneDatosTipoVS7($contrato,$dia,$mes,$anio,&$res){
	extract($contrato);

	$res['asunto']='RENOVACIÓN TÉCNICO - ANESCO';

	$res['mensaje']=
	"
	<div style='text-align:justify'>
	Estimados Sres.  <br />
	<br />
	<br />
	Desde nuestro Departamento de Administración, me comunican  la renovación por parte de su empresa del contrato  que tenía firmado con ANESCO.<br />
	<br />
 	En principio, y salvo que indiquen lo contrario, continuaré como técnico asignado a su empresa por lo que sería conveniente que me comunicaran cualquier cambio respecto a las condiciones de trabajo o número de trabajadores que se hubiera podido producir y que no nos haya sido comunicado con anterioridad, con objeto de contar con dicha información previamente a la visita de revisión de la evaluación inicial que realizaremos en el primer mes de vigencia del nuevo contrato.<br />
	Próximamente me pondré en contacto con ustedes para fijar la fecha que pueda serles más conveniente.<br />
	<br />
	Para cualquier duda o consulta, pueden ponerse en contacto conmigo por correo electrónico o por teléfono.<br />
	<br />
	Sin otro particular, le saluda atentamente:<br />
	<br />
	</div>
	<div style='text-align:right'>Fdo.: $tecnico</div>";

	return $res;
}



function campoAdjuntoCorreo($datos){
	if(isset($_GET['codigoContrato'])){
		campoFichero('ficheroAdjunto','Adjunto');
	}
	elseif($datos){
		compruebaAdjuntos($datos['ficheroAdjunto']);
	} 
	elseif(isset($_GET['codigoOferta'])){ 
		generaOfertaParaCorreo();
	}
	elseif(isset($_GET['codigoDoc'])){ 
		generaDocParaCorreo();
	}
	else{
		campoFichero('ficheroAdjunto','Adjunto');
	}
}

function generaOfertaParaCorreo(){
	require_once('../../api/html2pdf/html2pdf.class.php');

    $contenido=generaPDFOferta($_GET['codigoOferta']);

    $nombreFichero=time().'.pdf';
    
    $html2pdf=new HTML2PDF('P','A4','es');
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->WriteHTML($contenido);
    $html2pdf->Output('../documentos/adjuntosCorreos/'.$nombreFichero,'f');

    compruebaAdjuntos('../documentos/adjuntosCorreos/'.$nombreFichero);
    campoOculto('../documentos/adjuntosCorreos/'.$nombreFichero,'ofertaPDF');
}

function generaDocParaCorreo(){
	include_once('../../api/js/firma/signature-to-image.php');
	$doc=datosRegistro('documentosVS',$_GET['codigoDoc']);
    $nombreFichero=generaDocumentoVS($doc['tipoDocumento'],$doc['codigo'],true);
    $nuevoFichero=str_replace('../documentacion/vs/', '../documentos/adjuntosCorreos/', $nombreFichero);
    $nuevoFichero=str_replace('.docx', '_'.time().'.docx', $nuevoFichero);
    copy($nombreFichero, $nuevoFichero);
    compruebaAdjuntos($nuevoFichero);
    campoOculto($nuevoFichero,'nombreDoc');
}

function campoDestinatariosAsuntoCorreo($datos){
	if(isset($_GET['codigoOferta'])){
		$cliente=consultaBD("SELECT EMPID, EMPNOMBRE, EMPEMAILPRINC FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo=".$_GET['codigoOferta'],true,true);
		
		$datos['destinatarios']=$cliente['EMPEMAILPRINC'];
		$datos['asunto']='Envío oferta - '.$cliente['EMPID'].' '.$cliente['EMPNOMBRE'];
	} else if(isset($_GET['codigoDoc'])){
		$cliente=consultaBD("SELECT EMPID, EMPNOMBRE, EMPEMAILPRINC, tipoDocumento 
		FROM clientes 
		INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
		INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
		INNER JOIN documentosVS ON contratos.codigo=documentosVS.codigoContrato
		WHERE documentosVS.codigo=".$_GET['codigoDoc'],true,true);
		$tipos=array(1=>'Informe VS',4=>'Comunicación al trabajador para la realización de la VS',5=>'Programación reconocimientos médicos',8=>'Carta aptitud',12=>'Protección maternidad');
		campoOculto($_GET['codigoDoc'],'codigoDocumentoVS');

		$datos['destinatarios']=$cliente['EMPEMAILPRINC'];
		$datos['asunto']='Envío del documento '.$tipos[$cliente['tipoDocumento']].' para '.$cliente['EMPNOMBRE'];
	}

	campoTexto('destinatarios','Destinatario/s',$datos,'span5');
	campoTexto('copiaOculta','Copia oculta',$datos,'span5');
	campoTexto('asunto','Asunto',$datos,'span5');
}

//Fin parte de correos