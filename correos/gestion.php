<?php
  $seccionActiva=16;
  include_once('../cabecera.php');

  gestionCorreo();

?>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-wysihtml5.es-ES.js"></script>

<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-filestyle.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('#mensaje').wysihtml5({locale: "es-ES"});
    $(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
  });
</script>

</div><!-- contenido -->

<?php include_once('../pie.php'); ?>