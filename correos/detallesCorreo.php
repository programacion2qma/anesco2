<?php
  $seccionActiva=11;
  include_once('../cabecera.php');

                      abreVentanaGestionConBotones('Envio de correos','index.php',' ','icon-edit','',true,'noAjax');

                      $datos=compruebaDatos('correos');
                      if($datos){
                        $usuario=datosRegistro('usuarios',$datos['codigoUsuario']);
                        campoTexto('remitente','Remitente',$usuario['email']);
                        campoTexto('usuario','Usuario',$usuario['usuario']);
                        campoOculto($datos['tipo'],'tipo');
                      } else {
                        campoOculto($_SESSION['codigoU'],'codigoUsuario');
                        campoOculto('USUARIO','tipo');
                      }
                      campoFecha('fecha','Fecha',$datos);
                      campoHora('hora','Hora',$datos);
                      campoTexto('destinatarios','Destinatarios',$datos,'span5');
                      campoTexto('asunto','Asunto',$datos,'span5');
                      areaTexto('mensaje','Mensaje',$datos,'areaInforme');
                      if($datos){
                        compruebaAdjuntos($datos['ficheroAdjunto']);
                      } else { 
                        campoFichero('ficheroAdjunto','Adjunto');
                      }

                      cierraVentanaGestion('index.php',false,$botonGuardar=true,$texto='Enviar correo','icon-check',true,'Cancelar');
                    ?>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>
<script src="../js/funciones25.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-wysihtml5.es-ES.js"></script>

<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-filestyle.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('#mensaje').wysihtml5({locale: "es-ES"});
    $(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
  });
</script>
