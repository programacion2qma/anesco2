<?php
  $seccionActiva=2;
  include_once("../cabecera.php");
  gestionFormularioLOPD();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
		
		$('#eliminar').click(function(){//Generación de documentos de registros
  			var valoresChecks=recorreChecks();
  			if(valoresChecks['codigo0']==undefined){
    			alert('Por favor, seleccione antes un registro del listado.');
  			}
  			else if(confirm('¿Está seguro/a de que desea eliminar los registros seleccionados?')){
    			valoresChecks['elimina']='SI';
    			creaFormulario('generaDocumento.php',valoresChecks,'post');//Cambiado el nombre del documento por la URL.
  			}
		});

	});
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>