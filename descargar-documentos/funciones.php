<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión de Descargar documentación

function imprimeFormularioDocumentos($cliente){
	global $_CONFIG;
	conexionBD();
	/*if($empresa == "Todas"){
		$sql="SELECT f.codigo AS codigo,f.fecha AS fecha, c.razon_s AS cliente, f.n_razon_responsableFichero AS empresa FROM formularioLOPD f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE c.codigo = ".$cliente." ORDER BY c.razon_s;";
	} else {
		$sql="SELECT f.codigo AS codigo,f.fecha AS fecha, c.razon_s AS cliente, f.n_razon_responsableFichero AS empresa FROM formularioLOPD f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE c.codigo = ".$cliente." AND f.n_razon_responsableFichero LIKE '".$empresa."' ORDER BY c.razon_s;";	
	}*/
	$sql="SELECT f.codigo AS codigo,f.fecha AS fecha, c.razon_s AS cliente, f.n_razon_responsableFichero AS empresa FROM formularioLOPD f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE c.codigo = ".$cliente." ORDER BY c.razon_s;";
	$consulta=consultaBD($sql);
	
	$nombreDocumentos = array('Documento de seguridad','Anexo 5: Comunicado interno a los trabajadores', 'Anexo 6: Politica de privacidad de los trabajadores','Anexo 8: Derecho ARCO');
	$ficheroDocumentos = array('documentoseguridad','anexo5', 'anexo6', 'anexo8');
	while($datos=mysql_fetch_assoc($consulta)){	
		for($i=0;$i<4;$i++){
			echo "
				<tr>
					<td>".$datos['cliente']."</td>
					<td>".$datos['empresa']."</td>
					<td>".formateaFechaWeb($datos['fecha'])."</td>
					<td>".$nombreDocumentos[$i]."</td>
					<td class='centro'>
						<div class='btn-group'>
							<a class='noAjax btn btn-propio' href='".$_CONFIG['raiz']."descargar-documentos/generaDocumento.php?codigo=".$datos['codigo']."&documento=".$ficheroDocumentos[$i]."'><i class='icon-cloud-download'></i> Descargar</a></li>
						</div>
				 	</td>
				 
				 	<td>
						<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        		</td>
				</tr>";
		}
	}
	cierraBD();
	
}


//Parte de gestión de generar documentos
function seleccionaCliente(){
	abreVentanaGestion("Selecciona cliente y empresa",'index.php');
		if($_SESSION['tipoUsuario'] == 'ADMIN'){
				$sql="SELECT DISTINCT c.codigo, c.razon_s AS texto FROM clientes c INNER JOIN formularioLOPD f ON c.codigo=f.codigoCliente ORDER BY razon_s;";
			} else {
				$director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],true, true);
				if($director['director'] == 'SI'){
					$sql="SELECT DISTINCT c.codigo, c.razon_s AS texto FROM clientes c INNER JOIN formularioLOPD f ON c.codigo=f.codigoCliente WHERE c.empleado IN (SELECT codigo FROM usuarios WHERE tipo LIKE '".$director['tipo']."') ORDER BY razon_s;";
				} else {
					$sql="SELECT DISTINCT c.codigo, c.razon_s AS texto FROM clientes c INNER JOIN formularioLOPD f ON c.codigo=f.codigoCliente WHERE c.empleado LIKE ".$_SESSION['codigoU']." ORDER BY razon_s;";
				}
			}

		campoSelectConsulta('cliente','Cliente',$sql);
		
		/*$consulta=consultaBD($consulta,true);
		while($datos=mysql_fetch_assoc($consulta)){
			$consulta2 = consultaBD("SELECT DISTINCT n_razon_responsableFichero AS texto FROM formularioLOPD WHERE codigoCliente=".$datos['codigo'],true);
			$i=0;
			$texto='';
			$valores='';
			$texto[$i] = 'Todas';
			$valores[$i] = 'Todas';
			while($datos2=mysql_fetch_assoc($consulta2)){
				$i++;
				$texto[$i] = $datos2['texto'];
				$valores[$i] = $datos2['texto'];
			}
			echo "<div class='selectEmpresa cod".$datos['codigo']."'>";	
				campoSelect('none','Empresa responsable del fichero',$texto,$valores);
			echo "</div>";
		}
		echo "<div class='empresavacia'>";	
			campoSelect('empresa','Empresa responsable del fichero',array('Nada'),array('NULL'));
		echo "</div>";*/
	cierraVentanaGestion('../clientes/index.php',true,true,'Ver documentos','icon-check',false);
}

function generaDocumento($codigo, $tipoDocumento){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
	
	conexionBD();
	if($tipoDocumento=="anexo5"){
		generaAnexo5($codigo, $PHPWord);
	}
	if($tipoDocumento=="anexo6"){
		generaAnexo6($codigo, $PHPWord);
	}
	if($tipoDocumento=="anexo8"){
		generaAnexo8($codigo, $PHPWord);
	}
	if($tipoDocumento=="documentoseguridad"){
		generaDocumentoDeSeguridad($codigo, $PHPWord);
	}
	cierraBD();
}

function generaAnexo5($codigo, $PHPWord){
	$consulta=consultaBD("SELECT f.codigo AS codigo,c.razon_s AS cliente FROM formularioLOPD f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE f.codigo = ".$codigo);
	$datos=mysql_fetch_assoc($consulta);
	
	$documento=$PHPWord->loadTemplate('../documentacion/manualSeguridad/plantillaAnexo5.docx');
	$documento->setValue("nombreEmpresa",utf8_decode($datos['cliente']));
	$nombreFichero="Comunicado_interno_a_los_trabajadores.docx";
	$documento->save('../documentacion/manualSeguridad/'.$nombreFichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$nombreFichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentacion/manualSeguridad/'.$nombreFichero);
}

function generaAnexo6($codigo, $PHPWord){
	$consulta=consultaBD("SELECT f.codigo AS codigo,c.razon_s AS cliente,c.nombre AS nombreCliente,
	c.apellido1 AS ape1,c.apellido2 AS ape2,c.nif AS dni,c.dir_postal AS dir,c.postal AS cp, 
	c.localidad AS loc,c.provincia AS prov FROM formularioLOPD f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE f.codigo = ".$codigo);
	$datos=mysql_fetch_assoc($consulta);
	
	$provinciasAgencia=array('01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');
	$provincia = $provinciasAgencia[$datos['prov']];
	
	$documento=$PHPWord->loadTemplate('../documentacion/manualSeguridad/plantillaAnexo6.docx');
	$documento->setValue("nombreEmpresa",utf8_decode($datos['cliente']));
	$documento->setValue("nombreCliente",utf8_decode($datos['nombreCliente']));
	$documento->setValue("ape1",utf8_decode($datos['ape1']));
	$documento->setValue("ape2",utf8_decode($datos['ape2']));
	$documento->setValue("dni",utf8_decode($datos['dni']));
	$documento->setValue("dir",utf8_decode($datos['dir']));
	$documento->setValue("cp",utf8_decode($datos['cp']));
	$documento->setValue("loc",utf8_decode($datos['loc']));
	$documento->setValue("prov",utf8_decode($provincia));
	$documento->setValue("fecha",utf8_decode(date("d/m/Y")));
	$nombreFichero="Politica_de_privacidad_de_los_trabajadores.docx";
	$documento->save('../documentacion/manualSeguridad/'.$nombreFichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$nombreFichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentacion/manualSeguridad/'.$nombreFichero);
}

function generaAnexo8($codigo, $PHPWord){
	$consulta=consultaBD("SELECT f.codigo AS codigo,c.razon_s AS cliente,c.codigo AS codigoCliente, c.nombre AS nombreCliente,
	c.apellido1 AS ape1,c.apellido2 AS ape2,c.nif AS dni,c.dir_postal AS dir,c.postal AS cp, 
	c.localidad AS loc,c.provincia AS prov, c.cif_nif AS cif, f.n_razon_responsableFichero AS nombreResponsable,
	f.cif_responsableFichero AS cifResponsable,f.dir_postal_responsableFichero AS dirResponsable,
	f.postal_responsableFichero AS cpResponsable,f.localidad_responsableFichero AS localidadResponsable,
	f.provincia_responsableFichero AS provinciaResponsable FROM formularioLOPD f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE f.codigo = ".$codigo);
	$datos=mysql_fetch_assoc($consulta);
	
	$detalles=consultaBD("SELECT * FROM detalles_cliente WHERE codigoCliente =".$datos['codigoCliente'],true,true);
	$dir = $detalles['diferente_arco'] == 'SI'? $detalles['direccion_arco'] : $datos['dir'];
	$cp = $detalles['diferente_arco'] == 'SI'? $detalles['cp_arco'] : $datos['cp'];
	$loc = $detalles['diferente_arco'] == 'SI'? $detalles['poblacion_arco'] : $datos['loc'];
	$prov = $detalles['diferente_arco'] == 'SI'? $detalles['provincia_arco'] : $datos['prov'];
	
	$provinciasAgencia=array('01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');
	$provincia = $provinciasAgencia[$prov];
	$provinciaResponsable = $provinciasAgencia[$datos['provinciaResponsable']];
	
	$meses=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
	$mes=$meses[date("m")];
	
	$documento=$PHPWord->loadTemplate('../documentacion/manualSeguridad/plantillaAnexo8.docx');
	$documento->setValue("nombreEmpresa",utf8_decode($datos['cliente']));
	$documento->setValue("nombreCliente",utf8_decode($datos['nombreCliente']));
	$documento->setValue("ape1",utf8_decode($datos['ape1']));
	$documento->setValue("ape2",utf8_decode($datos['ape2']));
	$documento->setValue("dni",utf8_decode($datos['dni']));
	$documento->setValue("dir",utf8_decode($dir));
	$documento->setValue("cp",utf8_decode($cp));
	$documento->setValue("loc",utf8_decode($loc));
	$documento->setValue("prov",utf8_decode($provincia));
	$documento->setValue("cif",utf8_decode($datos['cif']));
	$documento->setValue("nombreResponsable",utf8_decode($datos['nombreResponsable']));
	$documento->setValue("cifResponsable",utf8_decode($datos['cifResponsable']));
	$documento->setValue("dirResponsable",utf8_decode($datos['dirResponsable']));
	$documento->setValue("cpResponsable",utf8_decode($datos['cpResponsable']));
	$documento->setValue("localidadResponsable",utf8_decode($datos['localidadResponsable']));
	$documento->setValue("provinciaResponsable",utf8_decode($provinciaResponsable));
	$documento->setValue("dia",utf8_decode(date("d")));
	$documento->setValue("mes",utf8_decode($mes));
	$documento->setValue("anio",utf8_decode(date("y")));
	$nombreFichero="Derechos_ARCO.docx";
	$documento->save('../documentacion/manualSeguridad/'.$nombreFichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$nombreFichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentacion/manualSeguridad/'.$nombreFichero);
}

function generaDocumentoDeSeguridad($codigo, $PHPWord){
	$consulta=consultaBD("SELECT f.codigo AS codigo,c.razon_s AS cliente,c.nombre AS nombreCliente,
	c.apellido1 AS ape1,c.apellido2 AS ape2,c.nif AS dni,c.dir_postal AS dir,c.postal AS cp, 
	c.localidad AS loc,c.provincia AS prov, c.cif_nif AS cif, f.n_razon_responsableFichero AS nombreResponsable,
	f.cif_responsableFichero AS cifResponsable,f.dir_postal_responsableFichero AS dirResponsable,
	f.postal_responsableFichero AS cpResponsable,f.localidad_responsableFichero AS localidadResponsable,
	f.provincia_responsableFichero AS provinciaResponsable FROM formularioLOPD f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE f.codigo = ".$codigo);
	$datos=mysql_fetch_assoc($consulta);
	
	$provinciasAgencia=array('01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');
	$provincia = $provinciasAgencia[$datos['prov']];
	$provinciaResponsable = $provinciasAgencia[$datos['provinciaResponsable']];
	
	$meses=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
	$mes=$meses[date("m")];
	
	$documento=$PHPWord->loadTemplate('../documentacion/manualSeguridad/plantillaDocumentoDeSeguridad.docx');
	$documento->setValue("nombreEmpresa",utf8_decode($datos['cliente']));
	$documento->setValue("nombreCliente",utf8_decode($datos['nombreCliente']));
	$documento->setValue("ape1",utf8_decode($datos['ape1']));
	$documento->setValue("ape2",utf8_decode($datos['ape2']));
	$documento->setValue("dni",utf8_decode($datos['dni']));
	$documento->setValue("dir",utf8_decode($datos['dir']));
	$documento->setValue("cp",utf8_decode($datos['cp']));
	$documento->setValue("loc",utf8_decode($datos['loc']));
	$documento->setValue("prov",utf8_decode($provincia));
	$documento->setValue("cif",utf8_decode($datos['cif']));
	$documento->setValue("nombreResponsable",utf8_decode($datos['nombreResponsable']));
	$documento->setValue("cifResponsable",utf8_decode($datos['cifResponsable']));
	$documento->setValue("dirResponsable",utf8_decode($datos['dirResponsable']));
	$documento->setValue("cpResponsable",utf8_decode($datos['cpResponsable']));
	$documento->setValue("localidadResponsable",utf8_decode($datos['localidadResponsable']));
	$documento->setValue("provinciaResponsable",utf8_decode($provinciaResponsable));
	$documento->setValue("dia",utf8_decode(date("d")));
	$documento->setValue("mes",utf8_decode($mes));
	$documento->setValue("anio",utf8_decode(date("y")));
	$nombreFichero="Documento_de_seguridad.docx";
	$documento->save('../documentacion/manualSeguridad/'.$nombreFichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$nombreFichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentacion/manualSeguridad/'.$nombreFichero);
}
?>