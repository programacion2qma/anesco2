<?php
  $seccionActiva=4;
  include_once("../cabecera.php");
?>
<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
         <div class="span12 margenAb">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-euro"></i><i class="icon-chevron-right"></i><i class="icon-filter"></i>
              <h3>Filtrado de Gastos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content centro">
              <h3>Seleccione las opciones de filtrado de gastos:</h3><br><br>
        				<form action='gastosFiltrado.php' method='post'>
        					<div class='control-group'>                     
                    <div class='controls'>
                      <?php
                        if($_SESSION['tipoUsuario']=='TECNICO'){
                          campoSelectConsulta('codigoUsuario','Técnico asignado',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE tipo='TECNICO' ORDER BY nombre,apellidos;",$_SESSION['codigoU'],'selectpicker span3 show-tick',"data-live-search='true'",true);    
                        }elseif($_SESSION['tipoUsuario']=='ADMIN'){
                          campoSelectConsulta('codigoUsuario','Técnico asignado',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE tipo='TECNICO' ORDER BY nombre,apellidos;");
                        }
                      ?>  
                    </div> <!-- /controls -->        					  
                    <div class='controls'>
        						Fecha de: &nbsp;<input type='text' class='input-small datepicker hasDatepicker' id='fechaUno' name='fechaUno' value='<?php echo imprimeFecha(); ?>'> 
        						Al: <input type='text' class='input-small datepicker hasDatepicker' id='fechaDos' name='fechaDos' value='<?php echo imprimeFecha(); ?>'>
        					  </div> <!-- /controls -->       
        					</div> <!-- /control-group -->
        					                   
        					
        					<br>
        					
        					<button type="submit" class="btn btn-primary">Seleccionar <i class="icon-arrow-right"></i></button>
        				</form>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		</div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>

<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1});
    $('.selectpicker').selectpicker();

  });
</script>