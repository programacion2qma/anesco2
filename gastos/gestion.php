<?php
  $seccionActiva=4;
  include_once("../cabecera.php");
  gestionParteDiario();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/filasTabla.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/funciones-gastos.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

		cargaOyentes();

		var gastos=$('input[name=gastos]').val();
		if(gastos=='SI') {
			$('#gastosActividad').show();
		}else{
			$('#gastosActividad').hide();				
		}

		$('.kilometraje').change(function(){
      		oyenteKm($(this));
    	});

		oyenteGastos();
		$('input[name=gastos]').change(function(){
			oyenteGastos();		
		});

		$(document).on('change','.codigoContrato',function(){
			oyenteContrato($(this));
		});

		/*$('input[type=checkbox]').change(function(){
			if($(this).prop('checked')) {
				alert();
    			$('#gastosActividad').show();
			}else{
    			$('#gastosActividad').hide();				
			}
		});

		$('#nuevoGasto').click(function(){
			insertaGasto();
		});*/

	});

/*function insertaGasto(){
	var clon=$( ".spanGasto:last" ).clone();
	clon.find('input[type=text]').val('');
	clon.find('input[type=radio]:[value=NO]').prop('checked',true);
	clon.find("select").val("NULL");
    clon.find('.bootstrap-select').remove();//Eliminación de los selectpicker
	clon.find('input:not(.input-block-level),select').each(function() {
  		name=$(this).attr("name").split('_');
  		name=name[0]+'_'+ ++name[1];
  		alert(name[1]);
  		$(this).attr("name",name);
  		$(this).attr("id",name);
	});

	$('.spanGasto:last').after(clon);
	$('select.selectpicker').selectpicker();
}*/

function oyenteGastos(){
	if($('input[name=gastos]:checked').val()=='SI') {
		$('#gastosActividad').show();
	}
	else{
		$('#gastosActividad').hide();				
	}	
}

function oyenteContrato(campo){
	var fila=obtieneFilaCampo(campo);
	var codigoContrato=campo.val();

	var consulta=$.post('../listadoAjax.php?include=parte-diario&funcion=obtieneTareasContratoAjax();',{'codigoContrato':codigoContrato});
	consulta.done(function(respuesta){
		$('#codigoTarea'+fila).html(respuesta).selectpicker('refresh');
	});
}

function insertaGasto(tabla){
  insertaFila(tabla);
  $('.kilometraje').change(function(){
    oyenteKm($(this));
  });
}

function oyenteKm(campo){
  var fila=obtieneFilaCampo(campo);
  var km=campo.val().replace(",", ".");
  var importe=km * 0.19;
  $('#importe'+fila).val(importe);
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>