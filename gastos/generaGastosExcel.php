<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('../../api/phpexcel/PHPExcel.php');
	require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');
	
	$datosFormulario=arrayFormulario();

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("../documentos/plantillaGastosExcel.xlsx");
		
	$objPHPExcel->setActiveSheetIndex(0);
	
	
	$i=9;
	$fecha1=formateaFechaBD($_GET['fechaUno']);
	$fecha2=formateaFechaBD($_GET['fechaDos']);
	$codigoUsuario=$_GET['codigoUsuario'];	
	
	$consulta=consultaBD("SELECT parte_diario.codigo, parte_diario.fecha, gastos_parte_diario.codigoContratoGastos, gastos_parte_diario.itinerario, gastos_parte_diario.kilometraje, gastos_parte_diario.importe, gastos_parte_diario.peaje, gastos_parte_diario.taxi, gastos_parte_diario.parking, gastos_parte_diario.dietas, gastos_parte_diario.otros, gastos_parte_diario.total, gastos_parte_diario.estado FROM parte_diario INNER JOIN gastos_parte_diario ON parte_diario.codigo=gastos_parte_diario.codigoParte WHERE parte_diario.fecha>='$fecha1' AND parte_diario.fecha<='$fecha2' AND parte_diario.codigoUsuario='$codigoUsuario'",true);

	$datosUsuario=datosRegistro('usuarios',$codigoUsuario);

	$objPHPExcel->getActiveSheet()->getCell('B5')->setValue($datosUsuario['nombre'].' '.$datosUsuario['apellidos']);
	$objPHPExcel->getActiveSheet()->getCell('G4')->setValue(formateaFechaWeb($fecha1));
	$objPHPExcel->getActiveSheet()->getCell('H4')->setValue(formateaFechaWeb($fecha2));				

	while($datos=mysql_fetch_assoc($consulta)){

		$datosContratos=datosRegistro('contratos',$datos['codigoContratoGastos']);
		$datosOfertas=datosRegistro('ofertas',$datosContratos['codigoOferta']);
		$datosClientes=datosRegistro('clientes',$datosOfertas['codigoCliente']);

		$objPHPExcel->getActiveSheet()->getCell('A'.$i)->setValue(formateaFechaWeb($datos['fecha']));
		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($datos['itinerario']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($datosClientes['EMPNOMBRE']);		
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($datos['kilometraje']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue($datos['importe']);
		$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue($datos['peaje']);
		$objPHPExcel->getActiveSheet()->getCell('G'.$i)->setValue($datos['taxi']);
		$objPHPExcel->getActiveSheet()->getCell('H'.$i)->setValue($datos['parking']);
		$objPHPExcel->getActiveSheet()->getCell('I'.$i)->setValue($datos['dietas']);
		$objPHPExcel->getActiveSheet()->getCell('J'.$i)->setValue($datos['otros']);
		$objPHPExcel->getActiveSheet()->getCell('K'.$i)->setValue($datos['total']);
		$objPHPExcel->getActiveSheet()->getCell('L'.$i)->setValue($datos['estado']);		
		$i++;
	}

	$nombre=$datosUsuario['nombre'];
	$tiempo=time();
	
	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('../documentos/Gastos'.$nombre.'.xlsx');


	// Definir headers
	header("Content-Type: application/vnd.ms-xlsx");
	header("Content-Disposition: attachment; filename=Gastos$nombre.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('../documentos/Gastos'.$nombre.'.xlsx');
?>