<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de partes diarios

function imprimeGastos(){
	global $_CONFIG;

	$where='';
	if(isset($_SESSION['ejercicio'])){
		$where=' WHERE YEAR(fechaInicio)="'.$_SESSION['ejercicio'].'" OR YEAR(fechaFin)="'.$_SESSION['ejercicio'].'"';
	}

	$consulta=consultaBD("SELECT tareas.fechaFin, tareas.codigoContrato, tareas.codigo AS codigoTarea, gastos_tarea.*,
						  CONCAT(nombre,' ',apellidos) AS usuario
						  FROM gastos_tarea INNER JOIN tareas ON gastos_tarea.codigoTarea=tareas.codigo 
						  INNER JOIN usuarios ON tareas.codigoUsuario=usuarios.codigo
						  $where",true);

	while($datos=mysql_fetch_assoc($consulta)){
		
		$contrato=datosRegistro('contratos',$datos['codigoContrato']);
		$oferta=datosRegistro('ofertas',$contrato['codigoOferta']);
		$cliente=datosRegistro('clientes',$oferta['codigoCliente']);
		$referencia=formateaReferenciaContrato($cliente,$contrato);

		echo "
			<tr>
				<td>".formateaFechaWeb($datos['fechaFin'])."</td>
				<td>".$referencia." - ".$cliente['EMPNOMBRE']."</td>
				<td>".$datos['itinerario']."</td>
				<td>".$datos['usuario']."</td>
				<td>".$datos['total']." €</td>
				<td><a href='../tareas/gestion.php?codigo=".$datos['codigoTarea']."' class='btn btn-propio btn-small noAjax' target='_blank'><i class='icon-external-link'></i> Ver tarea</a></td>
			</tr>";
	}

}


function estadisticasGastos(){
	$where='';
	if(isset($_SESSION['ejercicio'])){
		$where=' WHERE YEAR(fechaInicio)="'.$_SESSION['ejercicio'].'" OR YEAR(fechaFin)="'.$_SESSION['ejercicio'].'"';
	}
	$res=consultaBD('SELECT COUNT(gastos_tarea.codigo) AS total FROM gastos_tarea INNER JOIN tareas ON gastos_tarea.codigoTarea=tareas.codigo '.$where,true,true);

	return $res;
}

//Fin parte de partes diarios