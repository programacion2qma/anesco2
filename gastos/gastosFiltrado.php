<?php
  $seccionActiva=4;
  include_once('../cabecera.php');
    
  //operacionesFacturas();
  $fechaUno=$_POST['fechaUno'];
  $fechaDos=$_POST['fechaDos'];
  $codigoUsuario=$_POST['codigoUsuario'];
  $datosUsuario=datosRegistro('usuarios',$codigoUsuario);
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        
        
              <div class="shortcuts">
				<a href="index.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
        <a href="generaGastosExcel.php?fechaUno=<?php echo $fechaUno; ?>&fechaDos=<?php echo $fechaDos; ?>&codigoUsuario=<?php echo $codigoUsuario; ?>" class="shortcut noAjax"><i class="shortcut-icon icon-download"></i><span class="shortcut-label">Descargar Excel Imprimir</span> </a>
              </div>


      <div class="span12">        
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Gastos registrados desde el <?php echo $fechaUno;?> al <?php echo $fechaDos; ?> para <?php echo $datosUsuario['nombre']; ?></h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Fecha </th>
                  <th> Contrato </th>                  
                  <th> Itinerario </th>
                  <th> Kilometraje </th>
                  <th> Importe Km </th>
                  <th> Peaje </th>
                  <th> Táxi / Autobus </th>
                  <th> Parking </th>
                  <th> Dietas </th>
                  <th> Otros </th>
                  <th> Total </th>
                  <th> Estado </th>                                                                                                            
                </tr>
              </thead>
              <tbody>

                <?php
                  imprimeGastos("WHERE parte_diario.fecha>='".formateaFechaBD($fechaUno)."' AND parte_diario.fecha<='".formateaFechaBD($fechaDos)."' AND parte_diario.codigoUsuario='".$codigoUsuario."'");
                ?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content --> 
        </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/chart.js" type="text/javascript"></script>


<!-- /contenido --></div>

<?php include_once('../pie.php'); ?>