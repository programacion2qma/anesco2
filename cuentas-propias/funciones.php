<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de cuentas bancarias propias

function operacionesCuentasPropias(){
	$res=true;

	if(isset($_POST['codigo'])){
		$_POST['iban']=trim($_POST['iban']);
		$res=actualizaDatos('cuentas_propias');
	}
	elseif(isset($_POST['nombre'])){
		$_POST['iban']=trim($_POST['iban']);
		$res=insertaDatos('cuentas_propias');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('cuentas_propias');
	}

	mensajeResultado('nombre',$res,'Cuenta Bancaria Propia');
    mensajeResultado('elimina',$res,'Cuenta Bancaria Propia', true);
}


function imprimeCuentasPropias(){
	$iconoDescontado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success" title="Descontado"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger" title="Sin descontar"></i>');
	
	$consulta=consultaBD("SELECT * FROM cuentas_propias",true);
	while($datos=mysql_fetch_assoc($consulta)){
		echo "<tr>
				<td>".$datos['nombre']."</td>
				<td>".$datos['razonSocial']."</td>
				<td>".$datos['cif']."</td>
				<td>".$datos['iban']."</td>
				<td>".$datos['bic']."</td>
				<td>".$datos['sufijo']."</td>
				<td>".$datos['referenciaAcreedor']."</td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
	        </tr>";
	}
}


function gestionCuentaPropia(){
	operacionesCuentasPropias();

	abreVentanaGestionConBotones('Gestión de cuentas bancarias propias','index.php','span3');
	$datos=compruebaDatos('cuentas_propias');

	campoTexto('nombre','Nombre',$datos);
	campoTexto('razonSocial','Razón social',$datos,'span3');
	campoTexto('cif','CIF',$datos,'input-small validaCIF');
	campoTexto('iban','IBAN',$datos,'input-large');
	campoTexto('bic','BIC',$datos,'input-medium');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto('sufijo','Sufijo',$datos,'input-mini');
	campoTexto('referenciaAcreedor','Código acreedor',$datos,'input-medium');
	campoRadio('activo','Activa',$datos,'SI');

	cierraVentanaGestion('index.php',true);
}

//Fin parte de cuentas bancarias propias