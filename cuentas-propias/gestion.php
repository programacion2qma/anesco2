<?php
  $seccionActiva=29;
  include_once("../cabecera.php");
  gestionCuentaPropia();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="../js/cuentaBancaria.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('#referenciaAcreedor').focus(function(){
		if($(this).val()==''){
			var iban=$('#iban').val().substr(0,7)
			var cif=$('#cif').val();
			$(this).val(iban+cif);
		}
	});

	$('#bic').focus(function(){
		if($(this).val()==''){
			var cc=$('#iban').val().slice(4);
			var bic=obtieneBic(cc);
			$("#bic").val(bic);
		}
	});
});
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>