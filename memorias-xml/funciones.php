<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión de memorias XML

function operacionesMemorias(){
	$res = false;

	if(isset($_POST['codigo'])){
		$res = actualizaDatos('memorias_xml');
	}
	elseif(isset($_POST['ejercicio'])){
		$res = insertaDatos('memorias_xml');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res = eliminaEmpleado('memorias_xml');
	}

	mensajeResultado('ejercicio', $res, 'memorias');
    mensajeResultado('elimina', $res, 'memorias', true);
}



function gestionMemoria(){
	operacionesMemorias();
	
	abreVentanaGestionConBotones('Gestión de memoria', 'index.php');
	$datos = compruebaDatos('memorias_xml');
	
	campoSelectEjercicioMemoria($datos);

	creaTablaActividadesPreventivas($datos);

	creaTablaEvaluaciones($datos);

	creaTablaFormaciones($datos);

	creaTablaMedicina($datos);

	creaTablaInvestigaciones($datos);

	creaTablaOtros($datos);
	
	cierraVentanaGestion('index.php');
}

function campoSelectEjercicioMemoria($datos){
	$ejercicios = array();

	for($i=2017; $i<date('Y'); $i++){
		array_push($ejercicios, $i);
	}

	$valor = $datos;
	if(!$valor){
		$valor = date('Y')-1;
	}

	campoSelect('ejercicio', 'Ejercicio', $ejercicios, $ejercicios, $valor, 'selectpicker span1 show-tick', '');
}

function creaTablaActividadesPreventivas($datos){

	$actividades = array(
		"1. Diseño e implantación de planes de prevención según Ley 54/2003",
		"2. Evaluaciones iniciales de riesgos",
		"3. Revisión o actualización de evaluaciones de riesgos",
		"4. Planificación de la actividad preventiva",
		"5. Seguimiento de las actividades planificadas",
		"6. Información de los trabajadores",
		"7. Formación de los trabajadores ( Exclusivamente la formación referida al articulo 19 de la LPRL)",
		"8. Realización de planes de emergencia",
		"9. Investigación y análisis de Accidentes de trabajo",
		"10. Investigación y análisis de Enfermedades Profesionales",
		"11. Planificación de la Vigilancia individual de la salud",
		"12. Planificación de la Vigilancia colectiva de la salud",
		"13. Seguimiento de las actividades sanitarias planificadas"
	);

	$rangos = array('A', 'B', 'C');

	echo "
	<table class='table table-striped tabla-simple'>
		<thead>
			
			<tr>
				<th rowspan='3'> Actividad preventiva </th>
				<th colspan='4' class='centro'> Rango A </th>
				<th colspan='4' class='centro'> Rango B </th>
				<th colspan='4' class='centro'> Rango C </th>
			</tr>

			<tr>
				<th colspan='2' class='centro'> Empresas </th>
				<th colspan='2' class='centro'> Trabajadores </th>
				<th colspan='2' class='centro'> Empresas </th>
				<th colspan='2' class='centro'> Trabajadores </th>
				<th colspan='2' class='centro'> Empresas </th>
				<th colspan='2' class='centro'> Trabajadores </th>
			</tr>
			<tr>
				<th class='centro'> Afectadas </th>
				<th class='centro'> Actuaciones </th>
				<th class='centro'> Afectados </th>
				<th class='centro'> Actuaciones </th>
				<th class='centro'> Afectados </th>
				<th class='centro'> Actuaciones </th>
				<th class='centro'> Afectados </th>
				<th class='centro'> Actuaciones </th>
				<th class='centro'> Afectados </th>
				<th class='centro'> Actuaciones </th>
				<th class='centro'> Afectados </th>
				<th class='centro'> Actuaciones </th>
			</tr>
		</thead>
		<tbody>";

		foreach($actividades as $i => $actividad){
			$j = $i+1;
			
			echo 
			"<tr>
				<td>$actividad</td>";

			foreach($rangos as $rango){
				echo "
					<td class='centro'>";
						campoTextoSolo('conciertosActividad'.$rango.$j, $datos, 'input-iban pagination-right');
				echo "
					</td>
					<td class='centro'>";
						campoTextoSolo('actuacionesActividad'.$rango.$j, $datos, 'input-iban pagination-right');
				echo "
					</td>
					<td class='centro'>";
						campoTextoSolo('trabajadoresActividad'.$rango.$j, $datos, 'input-iban pagination-right');
				echo "
					</td>
					<td class='centro'>";
						campoTextoSolo('actuacionesTrabajadoresActividad'.$rango.$j, $datos, 'input-iban pagination-right');
				echo "
					</td>";
			}

			echo "
			</tr>";
		}

	echo "
		</tbody>
	</table>";
}

function creaTablaEvaluaciones($datos){

	$evaluaciones = array(
		"1. De seguridad: lugares de trabajo (excepto condiciones ambientales)",
		"2. De seguridad: máquinas, equipos e instalaciones",
		"3. Higiénicos: agentes químicos",
		"4. Higiénicos: agentes cancerígenos",
		"5. Higiénicos: agentes biológicos",
		"6. Higiénicos: ruido",
		"7. Higiénicos: vibraciones",
		"8. Higiénicos: iluminación",
		"9. Higiénicos: estrés térmico",
		"10. Higiénicos: otros",
		"11. Ergonómicos: carga física",
		"12. Psicosociales"
	);

	$rangos = array('A', 'B', 'C');

	echo "
	<br />
	<table class='table table-striped tabla-simple'>
		<thead>
			<tr>
				<th rowspan='2'> Evaluaciones </th>
				<th colspan='3' class='centro'> Rango A </th>
				<th colspan='3' class='centro'> Rango B </th>
				<th colspan='3' class='centro'> Rango C </th>
			</tr>
			<tr>
				<th class='centro'> Empresas </th>
				<th class='centro'> Trabajadores </th>
				<th class='centro'> Horas </th>
				<th class='centro'> Empresas </th>
				<th class='centro'> Trabajadores </th>
				<th class='centro'> Horas </th>
				<th class='centro'> Empresas </th>
				<th class='centro'> Trabajadores </th>
				<th class='centro'> Horas </th>
			</tr>
		</thead>
		<tbody>";

		
		foreach($evaluaciones as $i => $evaluacion){
			$j = $i+1;
			
			echo 
			"<tr>
				<td>$evaluacion</td>";

			foreach($rangos as $rango){
				echo "
					<td class='centro'>";
						campoTextoSolo('empresasCentrosEvaluacion'.$rango.$j, $datos, 'input-iban pagination-right');
				echo "
					</td>
					<td class='centro'>";
						campoTextoSolo('trabajadoresEvaluacion'.$rango.$j, $datos, 'input-iban pagination-right');
				echo "
					</td>
					<td class='centro'>";
						campoTextoSolo('horasEvaluacion'.$rango.$j, $datos, 'input-iban pagination-right');
				echo "
					</td>";
			}

			echo "
			</tr>";
		}

	echo "
		</tbody>
	</table>";
}

function creaTablaFormaciones($datos){

	$formaciones = array(
		'basico'		=>	"Formación de nivel básico (anexo IV R.D. 39/1997)",
		'emergencias'	=>	"Formación para emergencias"
	);

	echo "
	<br />
	<table class='table table-striped tabla-simple'>
		<thead>
			<tr>
				<th> Formaciones </th>
				<th class='centro'> Trabajadores </th>
				<th class='centro'> Centros </th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Formación de los trabajadores sobre los riesgos específicos de sus puestos de trabajo (art. 19 LPRL)</td>
				<td class='centro'>";
					campoTextoSolo('formacionEmpresasCentros', $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
				campoTextoSolo('formacionTrabajadores', $datos, 'input-iban pagination-right');
			echo "
				</td>
			</tr>
		</tbody>
		<thead>
			<tr>
				<th> Formaciones </th>
				<th class='centro'> Centros </th>
				<th class='centro'> Trabajadores </th>
				<th class='centro'> Teórico-práctico </th>
				<th class='centro'> Presencial </th>
				<th class='centro'> Distancia </th>
				<th class='centro'> Online </th>
				<th class='centro'> Otras </th>
			</tr>
		</thead>
		<tbody>";

		foreach($formaciones as $tipo => $formacion){

			echo 
			"<tr>
				<td>$formacion</td>
				<td class='centro'>";
					campoTextoSolo($tipo.'EmpresasCentros', $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo($tipo.'Trabajadores', $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo($tipo.'TeoricoPractico', $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo($tipo.'Presencial', $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo($tipo.'Distancia', $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
				campoTextoSolo($tipo.'Online', $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo($tipo.'Otras', $datos, 'input-iban pagination-right');
			echo "
				</td>
			</tr>";
		}

	echo "
		</tbody>
	</table>";
}

function creaTablaMedicina($datos){

	$medicinas = array(
		"1. Reconocimiento médico inicial",
		"2. Reconocimientos periódicos",
		"3. Reconocimiento tras ausencia prolongada",
		"4. Reconocimiento tras asignación de nuevas tareas",
		"5. Valoración de las trabajadoras embarazadas o en lactancia",
		"6. Valoración de los trabajadores especialmente sensibles",
		"7. No de protocolos específicos aplicados",
		"8. Estudio epidemiológico a partir de los resultados de vigilancia de la salud",
		"9. Investigación de daños para la salud en relación a estudio de riesgos laborales",
		"10. Estudios de vigilancia diseñados ad-hoc",
		"11. Programas de promoción de la salud"
	);

	echo "
	<br />
	<table class='table table-striped tabla-simple'>
		<thead>
			
			<tr>
				<th> Actividad sanitaria </th>
				<th class='centro'> Trabajadores </th>
				<th class='centro'> Empresas </th>
				<th class='centro'> Horas </th>
				<th class='centro'> Mujeres </th>
				<th class='centro'> Varones </th>
			</tr>
		</thead>
		<tbody>";

		foreach($medicinas as $i => $medicina){
			$j = $i+1;
			
			echo 
			"<tr>
				<td>$medicina</td>
				<td class='centro'>";
					campoTextoSolo('medicinaTrabajadores'.$j, $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo('medicinaEmpresasCentros'.$j, $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo('medicinaHoras'.$j, $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo('medicinaMujeres'.$j, $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo('medicinaVarones'.$j, $datos, 'input-iban pagination-right');
			echo "
				</td>					
			</tr>";
		}

	echo "
		</tbody>
	</table>";
}


function creaTablaInvestigaciones($datos){

	$rangos = array('A', 'B', 'C');

	echo "
	<br />
	<table class='table table-striped tabla-simple'>
		<thead>
			<tr>	
				<th colspan='9' class='centro'> Investigación de accidentes de trabajo </th>
			</tr>
			<tr>
				<th> </th>
				<th class='centro'> Investigaciones </th>
				<th class='centro'> Incidentes </th>
				<th class='centro'> Leves </th>
				<th class='centro'> Graves </th>
				<th class='centro'> Mortales </th>
				<th class='centro'> Accidentes con baja </th>
				<th class='centro'> Empresas </th>
				<th class='centro'> Bajas trabajadores ETT </th>
			</tr>
		</thead>
		<tbody>";

		foreach($rangos as $rango){
			echo "
			<tr>
				<th> Rango $rango </th>
				<td class='centro'>";
					campoTextoSolo('accidentesInvestigaciones'.$rango, $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo('accidentesIncidentes'.$rango, $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo('accidentesLeves'.$rango, $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo('accidentesGraves'.$rango, $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo('accidentesMortales'.$rango, $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo('accidentesAccBaja'.$rango, $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo('accidentesEmpresas'.$rango, $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo('accidentesAccBajaEtt'.$rango, $datos, 'input-iban pagination-right');
			echo "
				</td>
			</tr>";
		}

	echo "
		</tbody>
	</table>
	<table class='table table-striped tabla-simple'>
		<thead>
			<tr>	
				<th colspan='5' class='centro'> Investigación de Enfermedades de trabajo</th>
			</tr>
			<tr>
				<th> </th>
				<th class='centro'> Sospechas casos en baja </th>
				<th class='centro'> Sospechas casos sin baja </th>
				<th class='centro'> Casos comunicados con baja </th>
				<th class='centro'> Casos comunicados sin baja </th>
			</tr>
		</thead>
		<tbody>";

		foreach($rangos as $rango){
			echo "
			<tr>
				<th> Rango $rango </th>
				<td class='centro'>";
					campoTextoSolo('sospechaCBaja'.$rango, $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo('sospechaSBaja'.$rango, $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo('casoCBaja'.$rango, $datos, 'input-iban pagination-right');
			echo "
				</td>
				<td class='centro'>";
					campoTextoSolo('casoSBaja'.$rango, $datos, 'input-iban pagination-right');
			echo "
				</td>
			</tr>";
		}

	echo "
		</tbody>
	</table>
	<table class='table table-striped tabla-simple'>
		<thead>
			<tr>	
				<th colspan='2' class='centro'> Investigación de otros daños a la salud de posible origen laboral</th>
			</tr>
			<tr>
				<th> </th>
				<th class='centro'> Nº investigaciones </th>
			</tr>
		</thead>
		<tbody>";

		foreach($rangos as $rango){
			echo "
			<tr>
				<th> Rango $rango </th>
				<td class='centro'>";
					campoTextoSolo('otros'.$rango, $datos, 'input-iban pagination-right');
			echo "
				</td>
			</tr>";
		}

	echo "
		</tbody>
	</table>";
}


function imprimeMemorias(){

	$consulta = consultaBD("SELECT codigo, ejercicio FROM memorias_xml", true);

	while($datos=mysql_fetch_assoc($consulta)){

		echo "
		<tr>
			<td>".$datos['ejercicio']."</td>
			".botonAcciones(
				array('Detalles', 'Descargar XML SPA'), 
				array('memorias-xml/gestion.php?codigo='.$datos['codigo'], 'memorias-xml/genera-xml-spa.php?codigo='.$datos['codigo']),
				array('icon-search-plus', 'icon-cloud-download'),
				array(0, 1),
				true
			)."
			<td class='centro'>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' />
			</td>
		</tr>";
	}
}


function obtieneTotalesClientes($ejercicio){
	
	return consultaBD("SELECT COUNT(DISTINCT codigoCliente) AS total, SUM(ofertas.numEmpleados) AS empleados
					   FROM formularioPRL INNER JOIN contratos ON formularioPRL.codigoContrato=contratos.codigo
					   INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
					   WHERE memoria='SI' AND YEAR(fechaMemoria)='$ejercicio'
					   AND formularioPRL.eliminado='NO' AND contratos.eliminado='NO';", false, true);
}

function obtieneRangoCNAE($cnae){
	$codigosCNAE = array( // Formato CNAE => tarifa
		'01'	=> 2.60,
		'0113' 	=> 2.00,
		'0119'  => 2.00,
		'0129'  => 5.15,
		'0130'  => 2.25,
		'014'   => 3.30,
		'0147'  => 2.40,
		'015'   => 2.80,
		'016'   => 2.80,
		'0164'  => 2.25,
		'017'   => 3.30,
		'02'    => 5.15,
		'03'    => 6.40,
		'0322'	=> 6.25,
		'05'	=> 5.20,
		'06'	=> 5.20,
		'07'	=> 5.20,
		'08'	=> 5.20,
		'0811'	=> 7.15,
		'09'	=> 5.20,
		'10'	=> 3.20,
		'101'	=> 3.90,
		'102'	=> 3.30,
		'106'	=> 3.30,
		'107'	=> 1.95,
		'108'	=> 1.95,
		'11'	=> 3.20,
		'12'	=> 1.80,
		'13'	=> 1.85,
		'1391'	=> 1.50,
		'14'	=> 1.50,
		'1411'	=> 2.60,
		'1420'	=> 2.60,
		'143'	=> 1.50,
		'15'	=> 2.60,
		'16'	=> 5.15,
		'1624'	=> 4.10,
		'1629'	=> 4.10,
		'17'	=> 2.05,
		'171'	=> 3.50,
		'18'	=> 2.00,
		'19'	=> 3.35,
		'20'	=> 3.00,
		'204'	=> 2.70,
		'206'	=> 2.70,
		'21'	=> 2.40,
		'22'	=> 3.00,
		'23'	=> 4.10,
		'231'	=> 3.10,
		'232'	=> 3.10,
		'2331'	=> 3.10,
		'234'	=> 3.10,
		'237'	=> 6.10,
		'24'	=> 3.85,
		'25'	=> 3.85,
		'26'	=> 2.60,
		'27'	=> 2.80,
		'28'	=> 3.85,
		'29'	=> 2.80,
		'30'	=> 3.85,
		'3091'	=> 2.80,
		'3092'	=> 2.80,
		'31'	=> 3.85,
		'32'	=> 2.80,
		'321'	=> 1.85,
		'322'	=> 1.85,
		'33'	=> 3.85,
		'3313'	=> 2.60,
		'3314'	=> 2.80,
		'35'	=> 3.30,
		'36'	=> 3.70,
		'37'	=> 3.70,
		'38'	=> 3.70,
		'39'	=> 3.70,
		'41'	=> 6.70,
		'411'	=> 1.65,
		'42'	=> 6.70,
		'43'	=> 6.70,
		'45'	=> 2.05,
		'452'	=> 4.45,
		'454'	=> 2.90,
		'46'	=> 2.60,
		'4623'	=> 3.30,
		'4624'	=> 3.30,
		'4632'	=> 3.30,
		'4638'	=> 3.00,
		'4672'	=> 3.30,
		'4673'	=> 3.30,
		'4674'	=> 3.35,
		'4677'	=> 3.35,
		'4690'	=> 3.35,
		'47'	=> 1.65,
		'473'	=> 1.85,
		'49'	=> 3.30,
		'494'	=> 3.70,
		'50'	=> 3.85,
		'51'	=> 3.60,
		'52'	=> 3.30,
		'5221'	=> 2.10,
		'53'	=> 1.75,
		'55'	=> 1.50,
		'56'	=> 1.50,
		'58'	=> 1.65,
		'59'	=> 1.50,
		'60'	=> 1.50,
		'61'	=> 1.50,
		'62'	=> 1.50,
		'63'	=> 1.65,
		'6391'	=> 1.50,
		'64'	=> 1.50,
		'65'	=> 1.50,
		'66'	=> 1.50,
		'68'	=> 1.65,
		'69'	=> 1.50,
		'70'	=> 1.50,
		'71'	=> 1.65,
		'72'	=> 1.50,
		'73'	=> 1.70,
		'74'	=> 1.75,
		'742'	=> 1.50,
		'75'	=> 2.60,
		'77'	=> 2.00,
		'78'	=> 2.75,
		'781'	=> 1.95,
		'79'	=> 1.50,
		'80'	=> 3.60,
		'81'	=> 3.60,
		'811'	=> 1.85,
		'82'	=> 2.05,
		'8220'	=> 1.50,
		'8292'	=> 3.30,
		'84'	=> 1.65,
		'842'	=> 3.60,
		'85'	=> 1.50,
		'86'	=> 1.50,
		'869'	=> 1.75,
		'87'	=> 1.50,
		'88'	=> 1.50,
		'90'	=> 1.50,
		'91'	=> 1.50,
		'9104'	=> 2.95,
		'92'	=> 1.50,
		'93'	=> 3.00,
		'94'	=> 1.65,
		'95'	=> 2.60,
		'9524'	=> 3.85,
		'96'	=> 1.55,
		'9602'	=> 1.50,
		'9603'	=> 3.30,
		'9609'	=> 2.60,
		'97'	=> 1.50,
		'99'	=> 2.35
	);

	$tarifa = 0;

	if(isset($codigosCNAE[$cnae])){
		$tarifa = $codigosCNAE[$cnae];
	}
	elseif(strlen($cnae)>=2 && isset($codigosCNAE[substr($cnae, 0, 2)])){
		$tarifa = $codigosCNAE[substr($cnae, 0, 2)];
	}
	
	if($tarifa>6){
		$res = 'A';
	}
	elseif($tarifa>=1.5){
		$res = 'B';
	}
	else{
		$res = 'C';
	}

	return $res;
}

function obtieneTotalesEspecialidad($ejercicio, $especialidad){

	$res = array(
		'rangoA' => array(
			'empresasCentros'	=>	0,
			'trabajadores'		=>	0
		),
		'rangoB' => array(
			'empresasCentros'	=>	0,
			'trabajadores'		=>	0
		),
		'rangoC' => array(
			'empresasCentros'	=>	0,
			'trabajadores'		=>	0
		)
	);

	if($especialidad=='st'){
		$opcion = "'1', '3'";
	}
	elseif($especialidad=='hi'){
		$opcion = "'1', '3'";
	}
	elseif($especialidad=='epa'){
		$opcion = "'1', '3'";
	}
	elseif($especialidad=='mt'){
		$opcion = "'1', '2'";
	}

	$consulta = consultaBD("SELECT EMPCNAE, ofertas.numEmpleados
							FROM formularioPRL INNER JOIN contratos ON formularioPRL.codigoContrato=contratos.codigo
							INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
							INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
							WHERE memoria='SI' AND YEAR(fechaMemoria)='$ejercicio' AND opcion IN($opcion)
							AND formularioPRL.eliminado='NO' AND contratos.eliminado='NO';");

	while($datos=mysql_fetch_assoc($consulta)){
		
		$rango = obtieneRangoCNAE($datos['EMPCNAE']);

		if($datos['numEmpleados']==''){
			$datos['numEmpleados'] = 0;
		}

		$res['rango'.$rango]['empresasCentros']++;
		$res['rango'.$rango]['trabajadores'] += $datos['numEmpleados'];
	}

	
	return $res;
}

function obtieneProfesionales($perfil){
	
	$res = array();

	$consulta = consultaBD("SELECT codigo, nombre, apellidos, dni
							FROM usuarios
							WHERE tipo='$perfil' AND habilitado='SI'
							ORDER BY nombre, apellidos;");

	while($datos=mysql_fetch_assoc($consulta)){
		// Separar apellidos en 2
		$apellidos = explode(' ', $datos['apellidos']);
		$datos['apellido1'] = $apellidos[0];
		$datos['apellido2'] = $apellidos[1];

		array_push($res, $datos);
	}

	return $res;
}

function obtieneActividadProfesionalPorRango($ejercicio, $codigoUsuario){
	$res = array(
		'rangoA' => array(
			'empresasCentros'	=>	0,
			'trabajadores'		=>	0
		),
		'rangoB' => array(
			'empresasCentros'	=>	0,
			'trabajadores'		=>	0
		),
		'rangoC' => array(
			'empresasCentros'	=>	0,
			'trabajadores'		=>	0
		)
	);

	$consulta = consultaBD("SELECT EMPCNAE, ofertas.numEmpleados
							FROM formularioPRL INNER JOIN contratos ON formularioPRL.codigoContrato=contratos.codigo
							INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
							INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
							WHERE memoria='SI' AND YEAR(fechaMemoria)='$ejercicio' AND codigoUsuarioTecnico='$codigoUsuario'
							AND contratos.eliminado='NO' AND formularioPRL.eliminado='NO';");

	while($datos=mysql_fetch_assoc($consulta)){
		
		$rango = obtieneRangoCNAE($datos['EMPCNAE']);

		if($datos['numEmpleados']==''){
			$datos['numEmpleados'] = 0;
		}

		$res['rango'.$rango]['empresasCentros']++;
		$res['rango'.$rango]['trabajadores'] += $datos['numEmpleados'];
	}

	
	return $res;
}

function obtieneActividadRecosPorRango($ejercicio){
	$res = array(
		'rangoA' => array(
			'empresasCentros'	=>	0,
			'trabajadores'		=>	0
		),
		'rangoB' => array(
			'empresasCentros'	=>	0,
			'trabajadores'		=>	0
		),
		'rangoC' => array(
			'empresasCentros'	=>	0,
			'trabajadores'		=>	0
		)
	);

	$consulta = consultaBD("SELECT EMPCNAE, COUNT(DISTINCT codigoEmpleado) AS numEmpleados
							FROM reconocimientos_medicos INNER JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
							INNER JOIN clientes ON empleados.codigoCliente=clientes.codigo
							WHERE YEAR(reconocimientos_medicos.fecha)='$ejercicio' AND reconocimientos_medicos.eliminado='NO'
							GROUP BY clientes.codigo;");

	while($datos=mysql_fetch_assoc($consulta)){
		
		$rango = obtieneRangoCNAE($datos['EMPCNAE']);

		$res['rango'.$rango]['empresasCentros']++;
		$res['rango'.$rango]['trabajadores'] += $datos['numEmpleados'];
	}

	
	return $res;
}

function creaTablaOtros($datos){

	$otros = array(
		"1. Presencia de recursos preventivos",
		"2. Coordinación de actividades empresariales"
	);

	echo "
	<br />
	<table class='table table-striped tabla-simple'>
		<thead>
			<tr>
				<th colspan='5' class='centro'> Otras actividades preventivas </th>
			</tr>
			<tr>
				<th> Actividad </th>
				<th class='centro'> Empresas </th>
				<th class='centro'> Empresas donde se ha actuado </th>
				<th class='centro'> Trabajadores </th>
				<th class='centro'> Horas </th>
			</tr>
		</thead>
		<tbody>";

		
		foreach($otros as $i => $otro){
			$j = $i+1;
			
			echo "
			<tr>
				<td>$otro</td>
					<td class='centro'>";
						campoTextoSolo('otraEmpresas'.$j, $datos, 'input-iban pagination-right');
				echo "
					</td>
					<td class='centro'>";
						campoTextoSolo('otraEmpresasAct'.$j, $datos, 'input-iban pagination-right');
				echo "
					</td>
					<td class='centro'>";
						campoTextoSolo('otraTrabajadores'.$j, $datos, 'input-iban pagination-right');
				echo "
					</td>
					<td class='centro'>";
						campoTextoSolo('otraHoras'.$j, $datos, 'input-iban pagination-right');
				echo "
					</td>
			</tr>";
		}

	echo "
		</tbody>
	</table>";
}

//Fin parte de gestión de memorias XML