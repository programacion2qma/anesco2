<?php
include_once('funciones.php');

// Obtención de datos
$codigoMemoria = $_GET['codigo'];
$memoria = datosRegistro('memorias_xml', $codigoMemoria);
$ejercicio = $memoria['ejercicio'];

// Carga de plantilla
$memoriaEmpresa = simplexml_load_file('../documentos/memorias-xml/plantilla.xml');

conexionBD();

$clientes = obtieneTotalesClientes($ejercicio);

// GENERAL //
// Año
$memoriaEmpresa->general->addChild('anio', $ejercicio);

// Comunidad autónoma
$ca = $memoriaEmpresa->general->addChild('ca');
$ca->addAttribute('id', '01'); // Andalucía

// Empresa
$spa = $memoriaEmpresa->general->addChild('spa');
$spa->addAttribute('numero', 'B90072240');
$spa->addAttribute('tipo', '3');

// Actividad concertada
$actividad = $memoriaEmpresa->general->addChild('actividadConcertada');
$actividad->addAttribute('empresasCentros', $clientes['total']);
$actividad->addAttribute('trabajadores', $clientes['empleados']);

// ESPECIALIDADES //
$especialidades = array('st', 'hi', 'epa', 'mt');
$rangos = array('A', 'B', 'C'); // A => tarifa promedio >6, B => tarifa promedio <6 && >=1.5, C => tarifa promedio <1.5
foreach ($especialidades as $especialidad) {
	$nodoEspecialidad = $memoriaEmpresa->especialidades->addChild($especialidad);
    
	$datosEspecialidades = obtieneTotalesEspecialidad($ejercicio, $especialidad);

	foreach($rangos as $rango){
		$nodoRango = $nodoEspecialidad->addChild('rango'.$rango);

		$datosEspecialidad = $datosEspecialidades['rango'.$rango];

		$nodoRango->addAttribute('empresasCentros', $datosEspecialidad['empresasCentros']);
		$nodoRango->addAttribute('trabajadores', $datosEspecialidad['trabajadores']);
	}
}

// PROFESIONALES NO SANITARIOS//
$noSanitarios = $memoriaEmpresa->profesionales->addChild('noSanitarios');

$datosProfesionales = obtieneProfesionales('TECNICO');

foreach ($datosProfesionales as $datosProfesional) {
    $tecnico = $noSanitarios->addChild('tecnico');

    $idTecnico = $tecnico->addChild('id');
    $idTecnico->addAttribute('tipo', '1');
    $idTecnico->addAttribute('numero', $datosProfesional['dni']);

    $tecnico->addChild('nombre', $datosProfesional['nombre']);
    $tecnico->addChild('apellido1', $datosProfesional['apellido1']);
    $tecnico->addChild('apellido2', $datosProfesional['apellido2']);
    $tecnico->addChild('cualificacion', 'TS'); // Se define de forma fija, según el XML del 2020 puesto de ejemplo

	// Se define de forma fija, según el XML del 2020 puesto de ejemplo:
	$especialidades = array('ST', 'HI', 'EPA');
    $tecnico->addChild('especialidades');
    foreach($especialidades as $especialidad){
		$especialidadTecnico = $tecnico->especialidades->addChild('especialidad');
		$especialidadTecnico->addAttribute('codigo', $especialidad);
	}

	$datosActividades = obtieneActividadProfesionalPorRango($ejercicio, $datosProfesional['codigo']);
    $tecnico->addChild('actividad');
    foreach ($rangos as $rango) {
        $nodoRango = $tecnico->actividad->addChild('rango'.$rango);

		$datosActividad = $datosActividades['rango'.$rango];

        $nodoRango->addAttribute('empresasCentros', $datosActividad['empresasCentros']);
        $nodoRango->addAttribute('trabajadores', $datosActividad['trabajadores']);
    }
}

// PROFESIONALES SANITARIOS//
$sanitarios = $memoriaEmpresa->profesionales->addChild('sanitarios');
// Puesto de forma fija, solo Patricio es el médico (cambiar en futuro si se dan de alta otros)
$tecnico = $sanitarios->addChild('tecnico');

$idTecnico = $tecnico->addChild('id');
$idTecnico->addAttribute('tipo', '1');
$idTecnico->addAttribute('numero', '28626011G');
$tecnico->addChild('nombre', 'Patricio');
$tecnico->addChild('apellido1', 'Peñas');
$tecnico->addChild('apellido2', 'DE BUSTILLO');
$tecnico->addChild('cualificacion', 'MT');

$datosActividad = obtieneActividadRecosPorRango($ejercicio);

$tecnico->addChild('actividad');
foreach($rangos as $rango){
	$nodoRango = $tecnico->actividad->addChild('rango'.$rango);

	$datoActividad = $datosActividad['rango'.$rango];
	
	$nodoRango->addAttribute('empresasCentros', $datoActividad['empresasCentros']);
	$nodoRango->addAttribute('trabajadores', $datoActividad['trabajadores']);
}

// ACTIVIDADES PREVENTIVAS //
$actividades = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13');
$memoriaEmpresa->actividadesPreventivas->addChild('actividades');
$i = 1;
foreach ($actividades as $actividad) {
	$nodoActividad = $memoriaEmpresa->actividadesPreventivas->actividades->addChild('actividad');
	$nodoActividad->addAttribute('id', $actividad);
	
	foreach($rangos as $rango){
		$nodoRango = $nodoActividad->addChild('rango'.$rango);

		$nodoRango->addAttribute('trabajadoresAc', $memoria['actuacionesTrabajadoresActividad'.$rango.$i]);
		$nodoRango->addAttribute('trabajadoresCo', $memoria['trabajadoresActividad'.$rango.$i]);
		$nodoRango->addAttribute('empresasAc', $memoria['actuacionesActividad'.$rango.$i]);
		$nodoRango->addAttribute('empresasCo', $memoria['conciertosActividad'.$rango.$i]);
	}
	
	$i++;
}

$evaluaciones = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
$memoriaEmpresa->actividadesPreventivas->addChild('detalle');
$memoriaEmpresa->actividadesPreventivas->detalle->addChild('evaluaciones');
$i = 1;
foreach($evaluaciones as $evaluacion){
	$nodoEvaluacion = $memoriaEmpresa->actividadesPreventivas->detalle->evaluaciones->addChild('evaluacion');
	$nodoEvaluacion->addAttribute('id', $evaluacion);

	foreach($rangos as $rango){
		$nodoRango = $nodoEvaluacion->addChild('rango'.$rango);

		$nodoRango->addAttribute('trabajadores', $memoria['trabajadoresEvaluacion'.$rango.$i]);
		$nodoRango->addAttribute('empresasCentros', $memoria['empresasCentrosEvaluacion'.$rango.$i]);
		$nodoRango->addAttribute('horas', $memoria['horasEvaluacion'.$rango.$i]);
	}

	$i++;
}

$memoriaEmpresa->actividadesPreventivas->detalle->addChild('formacion');
$riesgos = $memoriaEmpresa->actividadesPreventivas->detalle->formacion->addChild('riesgos');
$riesgos->addAttribute('trabajadores', $memoria['formacionTrabajadores']);
$riesgos->addAttribute('empresasCentros', $memoria['formacionEmpresasCentros']);
$basico = $memoriaEmpresa->actividadesPreventivas->detalle->formacion->addChild('basico');
$basico->addAttribute('otras', $memoria['basicoOtras']);
$basico->addAttribute('online', $memoria['basicoOnline']);
$basico->addAttribute('distancia', $memoria['basicoDistancia']);
$basico->addAttribute('presencial', $memoria['basicoPresencial']);
$basico->addAttribute('teoricoPractico', $memoria['basicoTeoricoPractico']);
$basico->addAttribute('trabajadores', $memoria['basicoTrabajadores']);
$basico->addAttribute('empresasCentros', $memoria['basicoEmpresasCentros']);
$emergencias = $memoriaEmpresa->actividadesPreventivas->detalle->formacion->addChild('emergencias');
$emergencias->addAttribute('otras', $memoria['emergenciasOtras']);
$emergencias->addAttribute('online', $memoria['emergenciasOnline']);
$emergencias->addAttribute('distancia', $memoria['emergenciasDistancia']);
$emergencias->addAttribute('presencial', $memoria['emergenciasPresencial']);
$emergencias->addAttribute('teoricoPractico', $memoria['emergenciasTeoricoPractico']);
$emergencias->addAttribute('trabajadores', $memoria['emergenciasTrabajadores']);
$emergencias->addAttribute('empresasCentros', $memoria['emergenciasEmpresasCentros']);

$actividadesSanitarias = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11');
$memoriaEmpresa->actividadesPreventivas->detalle->addChild('medicina');
$i = 1;
foreach($actividadesSanitarias as $actividad){
	$nodoActividad = $memoriaEmpresa->actividadesPreventivas->detalle->medicina->addChild('actividadSanitaria');
	
	$nodoActividad->addAttribute('id', $actividad);
	$nodoActividad->addAttribute('trabajadores', $memoria['medicinaTrabajadores'.$i]);
	$nodoActividad->addAttribute('empresasCentros', $memoria['medicinaEmpresasCentros'.$i]);
	$nodoActividad->addAttribute('horas', $memoria['medicinaHoras'.$i]);
	$nodoActividad->addAttribute('mujeres', $memoria['medicinaMujeres'.$i]);
	$nodoActividad->addAttribute('varones', $memoria['medicinaVarones'.$i]);

	$i++;
}

$memoriaEmpresa->actividadesPreventivas->detalle->addChild('investigacion');
$memoriaEmpresa->actividadesPreventivas->detalle->investigacion->addChild('accidentes');
foreach($rangos as $rango){
	$nodoRango = $memoriaEmpresa->actividadesPreventivas->detalle->investigacion->accidentes->addChild('rango'.$rango);

	$nodoRango->addAttribute('accBaja', $memoria['accidentesAccBaja'.$rango]);
	$nodoRango->addAttribute('mortales', $memoria['accidentesMortales'.$rango]);
	$nodoRango->addAttribute('graves', $memoria['accidentesGraves'.$rango]);
	$nodoRango->addAttribute('leves', $memoria['accidentesLeves'.$rango]);
	$nodoRango->addAttribute('incidentes', $memoria['accidentesIncidentes'.$rango]);
	$nodoRango->addAttribute('investigaciones', $memoria['accidentesInvestigaciones'.$rango]);
	$nodoRango->addAttribute('accBajaEtt', $memoria['accidentesAccBajaEtt'.$rango]);
	$nodoRango->addAttribute('empresas', $memoria['accidentesEmpresas'.$rango]);
}
$memoriaEmpresa->actividadesPreventivas->detalle->investigacion->addChild('enfermedades');
foreach($rangos as $rango){
	$nodoRango = $memoriaEmpresa->actividadesPreventivas->detalle->investigacion->enfermedades->addChild('rango'.$rango);

	$nodoRango->addAttribute('casoSBaja', $memoria['casoSBaja'.$rango]);
	$nodoRango->addAttribute('casoCBaja', $memoria['casoCBaja'.$rango]);
	$nodoRango->addAttribute('sospechaSBaja', $memoria['sospechaSBaja'.$rango]);
	$nodoRango->addAttribute('sospechaCBaja', $memoria['sospechaCBaja'.$rango]);
}
$memoriaEmpresa->actividadesPreventivas->detalle->investigacion->addChild('otros');
foreach($rangos as $rango){
	$nodoRango = $memoriaEmpresa->actividadesPreventivas->detalle->investigacion->otros->addChild('rango'.$rango);

	$nodoRango->addAttribute('investigaciones', $memoria['otros'.$rango]);
}

$otras = array('01', '02');
$memoriaEmpresa->actividadesPreventivas->detalle->addChild('otras');
$i = 1;
foreach($otras as $otra){
	$nodoOtra = $memoriaEmpresa->actividadesPreventivas->detalle->otras->addChild('otra');

	$nodoOtra->addAttribute('id', $otra);
	$nodoOtra->addAttribute('horas', $memoria['otraHoras'.$i]);
	$nodoOtra->addAttribute('trabajadores', $memoria['otraTrabajadores'.$i]);
	$nodoOtra->addAttribute('empresasAct', $memoria['otraEmpresasAct'.$i]);
	$nodoOtra->addAttribute('empresas', $memoria['otraEmpresas'.$i]);

	$i++;
}

cierraBD();

// Guardo el fichero generado
$memoriaEmpresa->asXML('../documentos/memorias-xml/memoria.xml');

header('Content-Type: application/octet-stream');
header("Content-Transfer-Encoding: Binary"); 
header("Content-disposition: attachment; filename=\"S_B90072240_01_$ejercicio.xml\""); 
readfile('../documentos/memorias-xml/memoria.xml');