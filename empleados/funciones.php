<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión empleados


function operacionesEmpleados(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaEmpleado();
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaEmpleado();
	}
	elseif(isset($_POST['codigoClienteImportar'])){
		$res=importaEmpleados();
	}
	elseif(isset($_GET['codigoAprobar'])){
		$res=apruebaEmpleado();
	}
	elseif(isset($_POST['codigoPasar'])){
		$res=pasarAFormacion($_POST['codigoPasar'],$_POST['asignarCurso'],$_POST['codigoCurso']);
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoEmpleado($_POST['elimina'],'empleados');
	}
	elseif(isset($_GET['codigoCursoEliminar'])){
		$res=eliminaCurso($_GET['codigoCursoEliminar']);
	}
	elseif(isset($_GET['codigoDocumentoEliminar'])){
		$res=eliminaDocumento($_GET['codigoDocumentoEliminar']);
	}

	mensajeResultado('nombre',$res,'Empleado');
    mensajeResultado('elimina',$res,'Empleado', true);
}

function insertaEmpleado(){	
	$res=insertaDatos('empleados',array(time().'1',time().'2'),'../documentacion/fotos');
	
    if ($res) {
        $res=insertaUsuario($res);
    }

	return $res;
}

function actualizaEmpleado(){
	$res=actualizaDatos('empleados',array(time().'1',time().'2'),'../documentacion/fotos');
    
	if ($res) {
        $res=actualizaContrasenia($_POST['codigo']);
        $res=$res && insertarNuevoCurso($_POST['codigo']);
        $res=$res && gestionaDocumentos();
    }
	return $res;
}

function actualizaContrasenia($codigoEmpleado){
	$res=true;
	$datos=arrayFormulario();
	
	if($_SESSION['tipoUsuario']!='CLIENTE'){
		$usuario=consultaBD('SELECT usuarios.codigo 
							 FROM usuarios_empleados INNER JOIN usuarios ON usuarios_empleados.codigoUsuario=usuarios.codigo 
							 WHERE usuarios_empleados.codigoEmpleado='.$codigoEmpleado,true,true);
		
		if($usuario){
			$res=consultaBD('UPDATE usuarios SET nombre="'.$datos['nombre'].'",apellidos="'.$datos['apellidos'].'",dni="'.$datos['dni'].'",email="'.$datos['email'].'",
							 telefono="'.$datos['telefono'].'",usuario="'.$datos['usuario'].'",clave="'.$datos['clave'].'" WHERE codigo='.$usuario['codigo'],true);
		} 
		else {
			$res=insertaUsuario($codigoEmpleado);
		}
		
		if(isset($_POST['codigoUsuarioFormacion'])){
			conexionBDFormacion();
		
			$res=$res && consultaBD('UPDATE usuarios SET usuario="'.$datos['usuarioFormacion'].'",clave="'.$datos['claveFormacion'].'" WHERE codigo='.$datos['codigoUsuarioFormacion']);
		
			cierraBD();
		}
		/*else{
			conexionBDFormacion();
			$res=insertaUsuario($_POST['codigo']);
			cierraBD();
		}*/
	}

	return $res;
}

function insertaUsuario($codigoEmpleado){
	
	$datos=arrayFormulario();

	conexionBD();

	$res=consultaBD("INSERT INTO usuarios(codigo,nombre,apellidos,dni,email,telefono,usuario,clave,tipo) 
					 VALUES (NULL,'".$datos['nombre']."','".$datos['apellidos']."','".$datos['dni']."','".$datos['email']."',
					 '".$datos['telefono']."','".$datos['usuario']."','".$datos['clave']."','EMPLEADO');");
	
    if ($res) {
		$codigoUsuario=mysql_insert_id();
        $res=consultaBD('INSERT INTO usuarios_empleados VALUES (NULL,'.$codigoUsuario.','.$codigoEmpleado.');');
    }

	cierraBD();

    return $res;
}

function apruebaEmpleado(){
	return cambiaValorCampo('aprobado','SI',$_GET['codigoAprobar'],'empleados');
}

function insertarNuevoCurso($codigo){
	$res=true;

	if(isset($_POST['codigoNuevoCurso']) && $_POST['codigoNuevoCurso']!='NULL'){
		if($_POST['codigoCursoModificar']==''){
			$usuario=consultaBD('SELECT usuarios_sincronizados.usuarioFormacion 
							 FROM usuarios_sincronizados INNER JOIN usuarios_empleados ON usuarios_empleados.codigoUsuario=usuarios_sincronizados.usuarioPRL 
							 WHERE usuarios_empleados.codigoEmpleado='.$codigo,true,true);
	
			conexionBDFormacion();

			$codigoCurso=$_POST['codigoNuevoCurso'];
			$puesto_empleado=consultaBD('SELECT * FROM puestos_empleados WHERE actual="SI" AND codigoEmpleado='.$usuario['usuarioFormacion'],false,true);
		
			//$codigoEmpresa=$puesto_empleado['codigoEmpresa'];
		
			$codigoPuesto=$puesto_empleado['codigoPuesto'];
			$codigoEmpleado=$puesto_empleado['codigo'];
			$cursos_puestos=consultaBD('SELECT * FROM cursos_puestos WHERE codigoCurso='.$codigoCurso.' AND codigoPuesto='.$codigoPuesto,false,true);
		
			if($cursos_puestos){
				$codigoCurso=$cursos_puestos['codigo'];
			} 
			else {
				$orden=consultaBD('SELECT IFNULL(MAX(orden),0)+1 AS orden FROM cursos_puestos WHERE codigoPuesto='.$codigoPuesto,false,true);
				$res=consultaBD('INSERT INTO cursos_puestos VALUES(NULL,'.$codigoCurso.','.$codigoPuesto.','.$orden['orden'].',"SI");');
				$codigoCurso=mysql_insert_id();
			}
		
			$res=$res && consultaBD('INSERT INTO cursos_empleados(codigo,codigoEmpleado,codigoCurso,fechaMatriculacion,intentos,finalizado) VALUES(NULL,'.$codigoEmpleado.','.$codigoCurso.',"'.fechaBD().'",0,"NO");');
		
			cierraBD();
		} else {
			$codigoCurso=$_POST['codigoNuevoCurso'];
			conexionBDFormacion();
			$res=consultaBD('UPDATE cursos_puestos SET codigoCurso='.$codigoCurso.' WHERE codigo='.$_POST['codigoCursoModificar']);
			cierraBD();
		}
	}
	return $res;
}

function eliminaCurso($codigo){
	$res=true;
	conexionBDFormacion();
	$res=consultaBD('DELETE FROM cursos_puestos WHERE codigo='.$codigo);
	cierraBD();
	return $res;
}

function eliminaDocumento($codigo){
	$res=true;
	conexionBDFormacion();
	$res=consultaBD('DELETE FROM documentos_formacion WHERE codigo='.$codigo);
	cierraBD();
	return $res;
}

function gestionaDocumentos(){
	$res=true;
	$i=0;
	conexionBDFormacion();
	while(isset($_FILES['ficheroNuevoDocumento'.$i])){
		if($_FILES['ficheroNuevoDocumento'.$i]['name']!=''){
			$codigoEmpleado=$_POST['codigoUsuarioFormacion'];
			$codigoCurso=$_POST['codigoCursoEmpleado'.$i];
			$fichero=subeDocumento('ficheroNuevoDocumento'.$i,time().$i,'../../anesco-formacion/documentos/personal');
			$nombre=$_POST['nombreNuevoDocumento'.$i];
			$fecha=formateaFechaBD($_POST['fechaNuevoDocumento'.$i]);
			$res=$res && consultaBD('INSERT INTO documentos_formacion VALUES(NULL,'.$codigoEmpleado.','.$codigoCurso.',"'.$nombre.'","'.$fichero.'","SI","'.$fecha.'");');
		}
		$j=0;
		while(isset($_POST['nombreDocumento'.$i.'_'.$j])){
			if(isset($_FILES['ficheroDocumento'.$i.'_'.$j])){
				$fichero=subeDocumento('ficheroDocumento'.$i.'_'.$j,time().$i,'../../anesco-formacion/documentos/personal');
			} else {
				$fichero=$_POST['ficheroDocumento'.$i.'_'.$j];
			}
			$res=consultaBD('UPDATE documentos_formacion SET nombre="'.$_POST['nombreDocumento'.$i.'_'.$j].'",fichero="'.$fichero.'",fecha="'.formateaFechaBD($_POST['fechaDocumento'.$i.'_'.$j]).'"WHERE codigo='.$_POST['codigoDocumento'.$i.'_'.$j]);
			$j++;
		}
		$i++;
	}
	cierraBD();
	return $res;
}

function importaEmpleados(){
	$res=true;

	//Carga de PHP Excel
	require_once('../../api/phpexcel/PHPExcel.php');
	require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');

	$fichero=subeDocumento('importado','importacion','ficheros');

	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load('ficheros/'.$fichero);
	conexionBD();
	$i=2;
	$noImportados='';
	while($objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue()!=''){

		$codigoCliente 	=	$_POST['codigoClienteImportar'];
		$codigoInterno 	=	generaNumero($codigoCliente,false);
		$dni 			=	$objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue();
		$nombre 		=	$objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue();
		$apellidos 		=	$objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue();
		$apellidos 		.=	' '.$objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue();
		$telefono 		=	str_replace(' ','',$objPHPExcel->getActiveSheet()->getCell('M'.$i)->getValue());
		$email 			=	$objPHPExcel->getActiveSheet()->getCell('N'.$i)->getValue();
		

		$fechaNacimiento 	=$objPHPExcel->getActiveSheet()->getCell('L'.$i)->getValue();
		if(strpos($fechaNacimiento,'/')===false){
			$fechaNacimiento 			=	date("d/m/Y", PHPExcel_Shared_Date::ExcelToPHP($fechaNacimiento)); 
			//echo $fechaNacimiento . ' es igual a '.$objPHPExcel->getActiveSheet()->getCell('L'.$i)->getValue().' de L'.$i;die;
		}
		if($fechaNacimiento!=''){
			$fechaNacimiento=	formateaFechaBD($fechaNacimiento);
			$edad 			=	calculaEdad($fechaNacimiento);
		} else {
			$fechaNacimiento=	'0000-00-00';
			$edad 			=	'';
		}

		$direccion1 		=	$objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue();
		$direccion2 		=	$objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue();
		$cp 				=	$objPHPExcel->getActiveSheet()->getCell('G'.$i)->getValue();
		$ciudad 			=	$objPHPExcel->getActiveSheet()->getCell('H'.$i)->getValue();
		$provincia 			=	$objPHPExcel->getActiveSheet()->getCell('I'.$i)->getValue();
		$pais 				=	$objPHPExcel->getActiveSheet()->getCell('O'.$i)->getValue();
		$movil 				=	str_replace(' ', '',$objPHPExcel->getActiveSheet()->getCell('P'.$i)->getValue());
		

		$fechaAlta 			=	$objPHPExcel->getActiveSheet()->getCell('Q'.$i)->getValue();
		if(strpos($fechaAlta,'/')===false){
		$fechaAlta 			=	date("d/m/Y", PHPExcel_Shared_Date::ExcelToPHP($fechaAlta)); 
		}
		if($fechaAlta!=''){
			$fechaAlta 		=	formateaFechaBD($fechaAlta);
		} else {
			$fechaAlta 		=	'0000-00-00';
		}

		$objetivo 			=	'';
		$observaciones 		=	'';
		$ficheroFoto 		=	'NO';
		$puesto 			=	$objPHPExcel->getActiveSheet()->getCell('J'.$i)->getValue();
		$sensible 			=	strtoupper($objPHPExcel->getActiveSheet()->getCell('K'.$i)->getValue());
		$eliminado 			=	'NO';
		
		//$apto=estado($apto);
		//extract($apto);
		if(compruebaEmpleadoExcel($dni,$codigoCliente)){
			/*if($puesto!=''){
				$puesto=trim($puesto);
				$consulta=consultaBD('SELECT codigo FROM puestos_trabajo WHERE nombre LIKE "'.$puesto.'";',false,true);
				if($consulta){
					$codigoPuesto=$consulta['codigo'];
				} else {
					$codigoPuesto="INSERT INTO puestos_trabajo VALUES(NULL,'".$puesto."','','','','NO','NO','NO','NO','NO','NO','NO','NO','NO','NO','NO');";
					$codigoPuesto=consultaBD($codigoPuesto);
					$codigoPuesto=mysql_insert_id();
				}
			} else {
				$codigoPuesto='NULL';
			}*/
			$codigoPuesto 	=	'NULL';

			$sqlEmpleado='INSERT INTO empleados VALUES(NULL,'.$codigoCliente.','.$codigoInterno.',"'.$dni.'","'.$nombre.'","'.$apellidos.'","'.$telefono.'","'.$email.'","'.$fechaNacimiento.'","'.$edad.'","'.$direccion1.'","'.$direccion2.'","'.$cp.'","'.$ciudad.'","'.$provincia.'","'.$pais.'","'.$movil.'","'.$fechaAlta.'","'.$objetivo.'","'.$observaciones.'","'.$ficheroFoto.'","'.$sensible.'",'.$codigoPuesto.',"'.$eliminado.'","PENDIENTE","'.$puesto.'","NO","","","SI","");';
			$res=$res && consultaBD($sqlEmpleado);

			$codigoEmpleado=mysql_insert_id();
			$res=$res && consultaBD('INSERT INTO usuarios (codigo,nombre,apellidos,dni,usuario,clave,tipo) VALUES (NULL,"'.$nombre.'","'.$apellidos.'","'.$dni.'","'.$codigoCliente.$codigoInterno.'","'.$codigoCliente.$codigoInterno.'","EMPLEADO")');

			$codigoUsuario=mysql_insert_id();
			$res=$res && consultaBD("INSERT INTO usuarios_empleados VALUES(NULL,".$codigoUsuario.",".$codigoEmpleado.")");

		} 
		else{

			$datos=consultaBD("SELECT * FROM empleados WHERE dni='$dni' AND codigoCliente='$codigoCliente';",false,true);

			$camposActualizar 	=	compruebaCampoActualizarImportacionEmpleados($datos,'codigoInterno',$codigoInterno);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'dni',$dni);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'nombre',$nombre);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'apellidos',$apellidos);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'telefono',$telefono);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'email',$email);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'fechaNacimiento',$fechaNacimiento);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'direccion',$direccion1);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'direccion2',$direccion2);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'cp',$cp);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'ciudad',$ciudad);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'provincia',$provincia);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'pais',$pais);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'movil',$movil);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'fechaAlta',$fechaAlta);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'puestoManual',$puesto);
			$camposActualizar 	.=	compruebaCampoActualizarImportacionEmpleados($datos,'trabajadorSensible',$sensible);
			
			if($camposActualizar!=''){
				$camposActualizar=quitaUltimaComa($camposActualizar);

				$res=$res && consultaBD("UPDATE empleados SET $camposActualizar WHERE dni='$dni' AND codigoCliente='$codigoCliente';");
			}


			$noImportados.=$nombre.' '.$apellidos.'<br/>';
		}	
		
		$i++;
	}

	if($noImportados!=''){
		mensajeAdvertencia('Los siguientes empleados ya estaban registrados, por lo que se han actualizado sus datos:<br />'.$noImportados);
	}

	cierraBD();

	return $res;
}

function compruebaCampoActualizarImportacionEmpleados($datos,$indiceCampo,$campoExcel){
	$res='';

	if(($datos[$indiceCampo]=='' || $datos[$indiceCampo]==NULL || $datos[$indiceCampo] == "0000-00-00") && $campoExcel!=''){
		$res="$indiceCampo='$campoExcel', ";
	}

	return $res;
}


function gestionEmpleados(){
	operacionesEmpleados();
	
	$datos=compruebaDatos('empleados');
	$codigoCliente=obtieneCodigoClienteEmpleado($datos);
	$nombreCliente=obtieneNombreCliente($codigoCliente);

	abreVentanaGestionConBotones('Gestión de empleado de la empresa '.$nombreCliente,'?','','icon-edit','',true,'noAjax','index.php?codigo='.$codigoCliente);
	$datos=compruebaDatos('empleados');
	
	campoOculto($codigoCliente,'codigoCliente');
	campoOculto($datos,'eliminado','NO');
	campoOculto($_SESSION['tipoUsuario'],'tipoUsuarioConectado');

 	$datosFormacion=datosDeFormacion($datos);
	if($datosFormacion){
		creaPestaniasAPI(array('Datos','Formación'));
		abrePestaniaAPI(1,true);
	}
	abreColumnaCampos();
		if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='ENFERMERIA'){
			campoRadio('aprobado','Aprobado',$datos,'PENDIENTE',array('Si','No','Pendiente de aprobar'),array('SI','NO','PENDIENTE'));
		} else {
			campoOculto($datos,'aprobado','PENDIENTE');
		}
	cierraColumnaCampos(true);

	abreColumnaCampos();
		if(!$datos){
			$numeroEmpleado = generaNumero($_GET['codigoCliente']);
			$usuario=false;
			$edad=false;
		} else {
			$numeroEmpleado = $datos['codigoInterno'];
			$usuario=consultaBD('SELECT codigoUsuario,usuario,clave FROM usuarios_empleados INNER JOIN usuarios ON usuarios_empleados.codigoUsuario=usuarios.codigo WHERE usuarios_empleados.codigoEmpleado='.$datos['codigo'],true,true);
			campoOculto($usuario['codigoUsuario'],'codigoUsuario');
			$edad=calculaEdadEmpleado($datos['fechaNacimiento']);
		}
		campoTexto('codigoInterno','Nº de empleado',$numeroEmpleado,'input-mini pagination-right');
		campoTexto('nombre','Nombre',$datos,'input-large');
		campoTexto('apellidos','Apellidos',$datos,'input-large');
		campoTexto('dni','DNI',$datos,'input-small');
	cierraColumnaCampos();	

	abreColumnaCampos();
		campoRadio('sexoEmpleado','Sexo',$datos,'V',array('V','M'),array('V','M'));
		campoFecha('fechaNacimiento','Fecha de nacimiento',$datos);
		campoTexto('edad','Edad',$edad,'input-mini',true);
		campoTextoSimbolo('telefono','Teléfono','<i class="icon-phone"></i>',$datos,'input-small pagination-right');
		campoEmailEmpleado($datos);
	cierraColumnaCampos();
	
	
	abreColumnaCampos();
		campoTexto('direccion','Dirección 1',$datos,'input-large');
		campoTexto('direccion2','Dirección 2',$datos);
		campoTexto('cp','Código postal',$datos,'input-mini');
		campoTexto('ciudad','Ciudad',$datos,'input-large');
		campoTexto('provincia','Provincia',$datos);
		campoSelectPuestoTrabajoER($datos,$codigoCliente);
		campoTexto('puestoManual','Nombre dado por la empresa',$datos);
		campoRadio('trabajadorSensible','Trabajador sensible',$datos);

		$clase='';
		if($_SESSION['tipoUsuario']=='CLIENTE'){
			$clase='hide';
		}
		echo "<div class='$clase'>";
			campoUsuarioEmpleado($usuario);
			campoClaveEmpleado($usuario);
		echo "</div>";

	cierraColumnaCampos();

	abreColumnaCampos();
		campoTexto('pais','País',$datos,'input-large');
		campoTexto('movil','Teléfono móvil',$datos,'input-large');
		campoFecha('fechaAlta','Fecha de alta',$datos);
		areaTexto('objetivo','Objetivos',$datos);
		areaTexto('observaciones','Observaciones',$datos);
		campoFichero('ficheroFoto','Foto',0,$datos,'../documentacion/fotos/','Foto');
		
		campoRadio('lopd','LOPD firmada',$datos);
		echo "
		<div class='hide' id='cajaFicheroLOPD'>";
			campoFichero('ficheroLOPD','Documento LOPD firmado',0,$datos,'../documentacion/fotos/','Descargar');
		echo "
		</div>";

		campoRadio('activoEmpleado','Activo',$datos,'SI');
		echo "
		<div class='hide' id='cajaBaja'>";
			campoFecha('fechaBaja','Fecha de baja',$datos);
		echo "
		</div>";
		

	cierraColumnaCampos();

	if($datosFormacion){
		cierraPestaniaAPI();
		abrePestaniaAPI(2);
		conexionBDFormacion();
			campoOculto('','codigoCursoModificar');
			$usuarioFormacion=consultaBD('SELECT codigo AS codigoUsuarioFormacion, usuario AS usuarioFormacion, clave AS claveFormacion FROM usuarios WHERE codigo='.$datosFormacion['usuarioFormacion'],false,true);
			campoOculto($usuarioFormacion,'codigoUsuarioFormacion');
			campoUsuarioEmpleado($usuarioFormacion,'usuarioFormacion');
			campoClaveEmpleado($usuarioFormacion,'claveFormacion');
			campoSelectConsulta('codigoNuevoCurso','Nuevo curso','SELECT codigo, nombre AS texto FROM cursos ORDER BY nombre',false,'selectpicker span3 show-tick',"data-live-search='true'",'',0,false);
			$listado=consultaBD('SELECT cursos.nombre AS curso, cursos_empleados.fechaInicio, cursos_empleados.fechaFin, cursos_empleados.finalizado, cursos_puestos.codigo AS codigoCursoModificar, cursos_empleados.codigo AS cursoEmpleado
				FROM cursos_empleados
				INNER JOIN puestos_empleados ON cursos_empleados.codigoEmpleado=puestos_empleados.codigo
				INNER JOIN cursos_puestos ON cursos_empleados.codigoCurso=cursos_puestos.codigo
				INNER JOIN cursos ON cursos_puestos.codigoCurso=cursos.codigo
				WHERE puestos_empleados.codigoEmpleado='.$datosFormacion['usuarioFormacion']);
			$iconos=array('SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
			if(mysql_num_rows($listado)>0){
                        $i=0;
                        while($item=mysql_fetch_assoc($listado)){
                        	$j=0;
                        	echo "
							<table class='table table-striped tabla-simple' id='tablaFormacion'>
                        		<thead>
                            	<tr>
                               		<th> Curso </th>
                               		<th> Estado </th>
                               		<th> Fecha de inicio </th>
                               		<th> Fecha de fin </th>
                               		<th> </th>
                            	</tr>
                        	</thead>
                        	<tbody>";
                        	$btn="";
                        	if($item['finalizado']=='SI'){
                        		$estado='<label class="label label-success">Finalizado</label>';
                        	} else if($item['fechaInicio']!='0000-00-00'){
                        		$estado='<label class="label label-warning">En desarrollo</label>';
                        	} else {
                        		$estado='<label class="label label-info">Sin comenzar</label>';
                        		$btn='<a href="#" class="btnModificaCurso btn btn-propio noAjax" codigo="'.$item['codigoCursoModificar'].'"><i class="icon-edit"></i></a> <a href="gestion.php?codigo='.$datos['codigo'].'&codigoCursoEliminar='.$item['codigoCursoModificar'].'" class="btn btn-propio"><i class="icon-trash"></i></a>';
                        	}
                            echo 
                            '<tr>';
                            echo '<td>'.$item['curso'].'</td>';
                            echo '<td class="centro">'.$estado.'</td>';
                            echo '<td>'.formateaFechaWeb($item['fechaInicio']).'</td>';
                            echo '<td>'.formateaFechaWeb($item['fechaFin']).'</td>';
                            echo '<td class="centro">'.$btn.'</td>';
                            echo 
                            '</tr>';
                            $documentos=consultaBD('SELECT * FROM documentos_formacion WHERE codigoEmpleado='.$datosFormacion['usuarioFormacion'].' AND codigoCurso='.$item['cursoEmpleado']);
                            echo '
                            <tr>
                               	<th> Documentación </th>
                               	<th> Nombre </th>
                               	<th> Fecha </th>
                               	<th> Fichero </th>
                               	<th> </th>
                            </tr>';
                            campoOculto($item['cursoEmpleado'],'codigoCursoEmpleado'.$i);
                            while($d=mysql_fetch_assoc($documentos)){
                            	$d['ficheroDocumento']=$d['fichero'];
                            	$btn2="";
                            	campoOculto($d['codigo'],'codigoDocumento'.$i.'_'.$j);
                            	echo '
                            	<tr>
                               	<th> Documento </th>';
                               		campoTextoTabla('nombreDocumento'.$i.'_'.$j,$d['nombre']);
                               		campoFechaTabla('fechaDocumento'.$i.'_'.$j,$d['fecha']);
                            		campoFicheroEliminar('ficheroDocumento',$i.'_'.$j,'',1,$d,'../../anesco-formacion/documentos/personal/','Ver');
                            	echo '
                               		<td class="centro"><a href="gestion.php?codigo='.$datos['codigo'].'&codigoDocumentoEliminar='.$d['codigo'].'" class="btn btn-propio"><i class="icon-trash"></i></a></td>
                            	</tr>';
                            	$j++;
                            }
                            echo '
                            <tr>
                               	<th> Nuevo documento </th>';
                               	campoTextoTabla('nombreNuevoDocumento'.$i,'');
                               	campoFechaTabla('fechaNuevoDocumento'.$i,'');
                            	campoFichero('ficheroNuevoDocumento'.$i,'',1);
                            echo '
                               	<td class="centro"><button class="btn btn-propio" type="submit"><i class="icon-upload"></i> Subir</button></td>
                            </tr>

                         </tbody>
                    </table><br/><br/>';
                           	$i++;
                        }

        		$certificado=consultaBD('SELECT * FROM puestos_empleados WHERE codigoEmpleado='.$datosFormacion['usuarioFormacion'].' ORDER BY codigo DESC LIMIT 1',false,true);
        		if($certificado['fechaCertificado']!='0000-00-00'){
        			$fecha=explode('-',$certificado['fechaCertificado']);
        			$icono=certificadoCaducado($certificado['fechaCertificado']);
            	echo "	
            			<table class='table table-striped tabla-simple' id='tablaFormacion'>
            				<tr>
                               	<th> Fecha de certificación </th>
                               	<td> ".formateaFechaWeb($certificado['fechaCertificado'])." ".$icono."</td>
                               	<th> Nº certificado </th>
                               	<td> ".$fecha[0].'/'.$fecha[1].'/'.$certificado['numeroCertificado']."</td>
                               	<td></td>
                            </tr>
                        </table>";
                }
			} else {
				echo "En el software de formación, pero sin ninguna formación cursada";
			}
		cierraBD();
		cierraPestaniaAPI();
	}

	if($datosFormacion){
		cierraPestaniaAPI();
	}
		
		
	cierraVentanaGestion('index.php?codigo='.$codigoCliente,true);
}

function datosDeFormacion($datos){
	$res=false;
	if($datos){
		$res=estaEnFormacion($datos['codigo']);
	}
	return $res;
}

function campoSelectPuestoTrabajoER($datos,$codigoCliente){
	/*$query="SELECT puestos_trabajo.codigo, puestos_trabajo.nombre AS texto
			FROM puestos_trabajo INNER JOIN evaluacion_general ON puestos_trabajo.codigo=evaluacion_general.codigoPuestoTrabajo
			WHERE codigoCliente='$codigoCliente' AND tipoEvaluacion='PUESTOS'
			GROUP BY puestos_trabajo.codigo;";*/

	$query="SELECT codigo, nombre AS texto FROM puestos_trabajo ORDER BY nombre";

	campoSelectConsulta('codigoPuestoTrabajo','Puesto E.R.',$query,$datos,'selectpicker show-tick');
}

function campoEmailEmpleado($datos){
	if($_SESSION['tipoUsuario']!='CLIENTE'){
		campoTextoSimbolo('email','eMail','<i class="icon-envelope"></i>',$datos,'input-large');
	}
	else{
		campoTextoSimbolo('email','eMail','<i class="icon-envelope"></i>',$datos,'input-large',0,true);	
	}
}


function campoUsuarioEmpleado($datos,$campo='usuario',$texto='Usuario'){
	if($_SESSION['tipoUsuario']=='CLIENTE' || $_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='TECNICO' || $_SESSION['tipoUsuario']=='ENFERMERIA'){
		campoTexto($campo,$texto,$datos,'input-large');
	}
	else{
		campoDato($texto,$datos['usuario']);
		campoOculto($datos['usuario'],$campo);
	}
}

function campoClaveEmpleado($datos,$campo='clave',$texto='Contraseña'){
	if($_SESSION['tipoUsuario']=='CLIENTE' || $_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='TECNICO' || $_SESSION['tipoUsuario']=='MEDICO' || $_SESSION['tipoUsuario']=='ENFERMERIA'){
		campoClave($campo,$texto,$datos,'btn-propio','input-small');
	}
	else{
		campoDato($texto,'*********');
		campoOculto($datos['usuario'],$campo);
	}
}

function obtieneCodigoClienteEmpleado($datos){
	if(isset($_GET['codigoCliente'])){
		$res=$_GET['codigoCliente'];	
	}
	elseif($datos){
		$res=$datos['codigoCliente'];
	}
	else{
		$res='NULL';
	}

	return $res;
}

//Parte de campos personalizados

function campoSelectProvincia($nombreCampo,$datos){
	$valores=array();
	$nombres=array();
	$provinciasAgencia=array('01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');
	
	foreach ($provinciasAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}

	campoSelect($nombreCampo,'Provincia',$nombres,$valores,$datos);
}

//Fin parte de campos personalizados


function imprimeEmpleadosGeneral(){
	global $_CONFIG;

	conexionBD();

	$consulta=consultaBD("SELECT empleados.codigo, empleados.nombre, empleados.apellidos, sexoEmpleado, 
						  CONCAT('Empleado Nº',empleados.codigoInterno,' de ',clientes.EMPNOMBRE,' (',EMPMARCA,')') AS empresa, empleados.dni, 
						  empleados.telefono, empleados.email, puestos_trabajo.nombre AS nombrePuesto, empleados.eliminado, empleados.aprobado,
						  activoEmpleado, empleados.codigoCliente, usuarioFormacion

						  FROM empleados LEFT JOIN clientes ON empleados.codigoCliente=clientes.codigo 
						  LEFT JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo 
						  LEFT JOIN usuarios_empleados ON empleados.codigo=usuarios_empleados.codigoEmpleado
						  LEFT JOIN usuarios_sincronizados ON usuarios_empleados.codigoUsuario=usuarios_sincronizados.usuarioPRL
						  ORDER BY nombre;");
	cierraBD();

	$iconoRevisado=array('SI'=>'','NO'=>'<i class="icon-times-circle iconoFactura icon-danger" title="No aprobado"></i>','PENDIENTE'=>'<i class="icon-question-circle iconoFactura icon-warning-color" title="Pendiente de aprobar"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		$estado='Alta';
		if($datos['eliminado']=='SI'){
			$estado='Eliminado';
		}
		elseif($datos['activoEmpleado']=='NO'){
			$estado='Baja';
		}
		elseif($datos['aprobado']!='SI'){
			$estado='Sin aprobar';
		}
		$formacion=estaEnFormacion($datos['codigo']);
		$formado='';
        if($formacion){
            conexionBDFormacion();
                $certificado=consultaBD('SELECT * FROM puestos_empleados WHERE codigoEmpleado='.$formacion['usuarioFormacion'].' ORDER BY codigo DESC LIMIT 1',false,true);
                if($certificado['fechaCertificado']!='0000-00-00'){
                    $fecha=explode('-',$certificado['fechaCertificado']);
                    $formado=certificadoCaducado($certificado['fechaCertificado']);
                }
            cierraBD();
            if($formado==''){
                $formado='<span class="hide">Formado</span><i class="icon-check-circle iconoFactura icon-success" title="Formado"></i>';
            }
        } else {
            $formado='<span class="hide">No formado</span><i class="icon-times-circle iconoFactura icon-danger" title="No formado"></i>';
        }	
		echo "
			<tr>
				<td>".$datos['nombre']." ".$iconoRevisado[$datos['aprobado']]."</td>
				<td>".$datos['apellidos']."</td>
				<td>".$datos['empresa']."</td>
				<td>".$datos['nombrePuesto']."</td>
				<td>".$datos['dni']."</td>
				<td>".$datos['sexoEmpleado']."</td>
				<td><a href='tel:".$datos['telefono']."'>".formateaTelefono($datos['telefono'])."</a></td>
				<td><a href='mailto:".$datos['email']."'>".$datos['email']."</a></td>
				<td class='centro'>".$estado."</td>
				<td class='centro'>".$formado."</td>";
				if(($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='TECNICO') && $datos['aprobado']!='SI'){
		echo "<td class='centro'>
					<div class='btn-group centro'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span> Acciones</button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."empleados/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
						    <li class='divider'></li>
						    <li><a href='".$_CONFIG['raiz']."empleados/seleccionaCliente.php?codigoAprobar=".$datos['codigo']."'><i class='icon-check'></i> Aprobar</i></a></li> 
						</ul>
					</div>
				</td>";
				} else if(($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='TECNICO') && $datos['aprobado']=='SI' && !$formacion){
		echo "<td class='centro'>
					<div class='btn-group centro'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span> Acciones</button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."empleados/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
						    <li class='divider'></li>
						    <li><a class='btnFormacion noAjax' codigoCliente='".$datos['codigoCliente']."' codigoPasar='".$datos['codigo']."' href='#'><i class='icon-graduation-cap'></i> Pasar a<br/>formación</i></a></li> 
						</ul>
					</div>
				</td>";
			} else if(($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='TECNICO') && $datos['aprobado']=='SI' && $formacion){
		echo "<td class='centro'>
					<div class='btn-group centro'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span> Acciones</button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."empleados/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li>"; 
						conexionBDFormacion();
						$empleado=consultaBD('SELECT codigo FROM puestos_empleados WHERE (firma!="" OR firmaImagen!="NO") AND codigoEmpleado='.$datos['usuarioFormacion'],false,true);
						if($empleado){
							echo "<li class='divider'></li>";
							echo "<li><a class='noAjax' target='_blank' href='generaCertificado.php?empleado=".$empleado['codigo']."'><i class='icon-download'></i> Certificado A</i></a></li>";
						}
						cierraBD();
		echo "</ul>
					</div>
				</td>";
				} else {
		echo "	<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>";
				}
				
			echo "</tr>";
	}
}

function generaNumero($cliente,$conexion=true){
	$consulta=consultaBD("SELECT MAX(codigoInterno) AS numero FROM empleados WHERE codigoCliente=".$cliente, $conexion, true);
	
	return $consulta['numero']+1;
}

function estadisticasEmpleadosRestrict($cliente,$eliminado){
	$res=array();
	if($eliminado=='SI'){
		$eliminado="eliminado='SI' AND aprobado='SI'";
	} else {;
		$eliminado="(eliminado='NO' OR (eliminado='SI' AND aprobado!='SI'))";
	}
	conexionBD();
	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM empleados WHERE ".$eliminado." AND codigoCliente=".$cliente,false, true);

	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

function creaVentanaImportarFichero($cliente){
    abreVentanaModalConFichero('Fichero','cajaFichero');
    campoOculto($cliente,'codigoClienteImportar');
    campoFichero('importado','Fichero');
    echo '<br/><a href="ficheros/modelo_importacion_empleados.xlsx" class="btn btn-propio noAjax"><i class="icon-download"></i> Descargar modelo</a>';
    /*echo 'Listado de ficheros subidos';
    echo '<ul id="ulFicheros">';
    echo '<li>No hay fichero subidos para este cliente</li>';
    echo '</ul>';*/
    cierraVentanaModal('importaFichero','Importar','icon-upload');
}

function abreVentanaModalConFichero($titulo,$id='cajaGestion',$claseColumna=''){
    echo "
    <!-- Caja de gestión -->
    <div id='$id' class='modal hide fade'> 
      <form class='form-horizontal sinMargenAb noAjax' method='post' action='?' enctype='multipart/form-data'>
        <div class='modal-header'> 
          <h3> <i class='icon-list'></i><i class='icon-chevron-right'></i><i class='icon-edit'></i> &nbsp; Gestión de $titulo </h3> 
        </div> 
        <div class='modal-body'>
            <fieldset class='$claseColumna'>";
}

function creaVentanaPuestos(){
	abreVentanaModal('Puestos disponibles','cajaPuestos');
echo '	<div class="widget-content">
		<table class="table table-striped table-bordered datatable tablaPuestos">
            <thead>
            	<tr><th> Puesto</th></tr>
            </thead>
            </tbody>';
    $puestos=consultaBD('SELECT nombre FROM puestos_trabajo ORDER BY nombre',true);
    while($puesto=mysql_fetch_assoc($puestos)){
    	echo '<tr><td>'.$puesto['nombre'].'</td></tr>';
    }
    echo '	</tbody>
          </table>
          </div>';
	cierraVentanaModal('','','',false);
}

function calculaEdad($fechanacimiento){
	list($ano,$mes,$dia) = explode("-",$fechanacimiento);
	$ano_diferencia  = date("Y") - $ano;
	$mes_diferencia = date("m") - $mes;
	$dia_diferencia   = date("d") - $dia;
	if ($dia_diferencia < 0 || $mes_diferencia < 0)
		$ano_diferencia--;
	return $ano_diferencia;
}

function compruebaEmpleado(){
	$res='ok';

	$datos=arrayFormulario();
	extract($datos);
	if (strpos($dni, '-') === false) {
		$letra=substr($dni, -1);
		$dni2=substr($dni,0,-1).'-'.substr($dni,-1);
	} else {
		$dni2=$dni;
		$dni=explode('-', $dni);
		$dni=$dni[0].$dni[1];
	}

	$consulta=consultaBD("SELECT codigo, codigoCliente FROM empleados WHERE (dni='$dni' OR dni='$dni2') AND codigo!='$codigoEmpleado' AND codigoCliente='$codigoCliente';",true);
	if(mysql_num_rows($consulta)>0){
		$res='error';
	}
	echo $res;
}

function compruebaEmpleadoExcel($dni,$codigoCliente){
	$res=true;
	if (strpos($dni, '-') === false) {
		$letra=substr($dni, -1);
		$dni2=substr($dni,0,-1).'-'.substr($dni,-1);
	} else {
		$dni2=$dni;
		$dni=explode('-', $dni);
		$dni=$dni[0].$dni[1];
	}

	$consulta=consultaBD("SELECT codigo, codigoCliente FROM empleados WHERE (dni='$dni' OR dni='$dni2') AND codigoCliente='$codigoCliente';");
	if(mysql_num_rows($consulta)>0){
		$res=false;
	}
	return $res;
}

function obtenerCampoCliente($cliente,$campo){
	$contrato=consultaBD('SELECT '.$campo.' FROM clientes WHERE codigo='.$cliente,true,true);
	return $contrato[$campo];
}

function cambiaEstadoEliminadoEmpleado($estado,$tabla){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();
    for($i=0;isset($datos['codigo'.$i]);$i++){
        $res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],$tabla,false);
        if($_SESSION['tipoUsuario']=='CLIENTE'){
			$res=$res && cambiaValorCampo('aprobado','PENDIENTE',$datos['codigo'.$i],$tabla,false);
		} else {
			$res=$res && cambiaValorCampo('aprobado','SI',$datos['codigo'.$i],$tabla,false);
		}
    }
    cierraBD();

    return $res;
}

function campoSelectClienteEmpleado(){
	$query="SELECT clientes.codigo, CONCAT(EMPNOMBRE,' (',EMPMARCA,')') AS texto 
			FROM clientes 
			WHERE clientes.eliminado='NO'  
			GROUP BY clientes.codigo
			ORDER BY EMPNOMBRE";

	campoSelectConsulta('codigoCliente','Cliente',$query,false,'selectpicker span10 show-tick',"data-live-search='true'",'',2);
}


function obtieneDatosFormacion(){
	$res=array('puesto'=>'','empresa'=>'','form'=>'');
	$empleado=datosRegistro('empleados',$_POST['codigoPasar']);
	$empresa=datosRegistro('clientes',$_POST['codigoCliente']);
	$puesto=datosRegistro('puestos_trabajo',$empleado['codigoPuestoTrabajo'],$campo='codigo');
	$cursosAsignados='(0';
	conexionBDFormacion();
		$empresaF=consultaBD('SELECT codigo FROM empresas WHERE cif="'.$empresa['EMPCIF'].'";',false,true);
		if($empresaF){
			$puestoF=consultaBD('SELECT codigo FROM puestos_trabajos WHERE nombre="'.$puesto['nombre'].'";',false,true);
			if($puestoF){
				$puestoEmpresa=consultaBD('SELECT codigo FROM puestos_empresas WHERE codigoEmpresa='.$empresaF['codigo'].' AND codigoPuesto='.$puestoF['codigo'],false,true);
				if($puestoEmpresa){
					$res['puesto']='Este empresa tiene asignado los siguientes cursos para este puesto.';
					$cursos=consultaBD('SELECT checkActivo,cursos.nombre, cursos.codigo FROM cursos_puestos INNER JOIN cursos ON cursos_puestos.codigoCurso=cursos.codigo WHERE codigoPuesto='.$puestoEmpresa['codigo'].' ORDER BY orden ASC, checkActivo DESC');
					while($curso=mysql_fetch_assoc($cursos)){
						$res['puesto'].='<br/>- '.$curso['nombre'];
						if($curso['checkActivo']=='SI'){
							$res['puesto'].=' - Activo';
						} else {
							$res['puesto'].=' - Inactivo';
						}
						$cursosAsignados.=','.$curso['codigo'];
					}
					$res['puesto'].='<br/><br/>Puedes seleccionar otro curso si lo desea';
				} else {
					$res['puesto']='Esa empresa no tiene el puesto puesto asignado en el software de formación en formación. Por lo tanto se asignará el puesto y el curso seleccionado';
				}
			} else {
				$res['puesto']='Ese puesto no existe en el software de formación. Por lo tanto se creará el puesto y se asignara el curso seleccionado';
			}
		} else {
			$res['empresa']='Esta empresa no existe en  el software de formación. Por lo tanto se creará la empresa, se le asignará el puesto y el curso seleccionado';
		}
		$cursosAsignados.=')';
		$listado=consultaBD('SELECT codigo, nombre AS texto FROM cursos WHERE codigo NOT IN '.$cursosAsignados.' ORDER BY nombre');
		$res['form']="<div class='control-group'><label class='control-label' for='codigoCurso'>Asignar curso:</label><div class='controls'><select name='codigoCurso' class='selectpicker span3 show-tick' id='codigoCurso' data-live-search='true'><option value='NULL'></option>";
				while($curso=mysql_fetch_assoc($listado)){
					$res['form'].="<option value='".$curso['codigo']."'>".$curso['texto']."</option>";
				}
		$res['form'].='</select></div></div>';	
		$res['form'].="<div class='control-group'><label class='control-label'>Asignar curso a:</label><div class='controls nowrap'><label class='radio inline'><input type='radio' name='asignarCurso' value='PUESTO' checked='checked'>Puesto <span style='font-size:8px;'>(los próximos empleados con este puesto también tendrán asignado este curso)</span></label><br/><label class='radio inline'><input type='radio' name='asignarCurso' value='EMPLEADO'>Empleado <span style='font-size:8px;'>(los próximos empleados con este puesto no tendrán asignado)</span></label></div></div>";

	cierraBD();
	echo json_encode($res);
}

function creaTablaDocumentos(){
        echo "
        <div class='control-group'>                     
            <label class='control-label'>Documentación:</label>
            <div class='controls'>
                <table class='tabla-simple mitadAncho' id='tablaDocumentos'>
                    <thead>
                        <tr>
                            <th> Nombre </th>
                            <th> Fichero </th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                    	<tr>";
                    	campoTextoTabla('nombreDocumento0');
                    	campoFichero('fichero0','',1);
        echo "      	<td><input type='checkbox' name='filasTabla[]' value='1'></td>
        				</tr>
        			</tbody>
                </table>
                <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaDocumentos\");'><i class='icon-plus'></i></button> 
                <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaDocumentos\");'><i class='icon-trash'></i></button>
            </div>
        </div>";
}

//Fin parte de gestión empleados