<?php
  $seccionActiva=12;
  include_once('../cabecera.php');

  $eliminado='NO';
  if(isset($_GET['eliminado'])){
    $eliminado=$_GET['eliminado'];
  }
  
  $res=operacionesEmpleados();
  $cliente=obtieneCodigoCliente();
  $estadisticas=estadisticasEmpleadosRestrict($cliente,$eliminado);

?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de gestión Empleados eliminados:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-trash-o"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Empleados eliminados</div>
                     <!--div class="stat"> <i class="icon-send"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Enviados a la agencia</div-->
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Empleados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">

                <?php 
                  if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='CLIENTE' || $_SESSION['tipoUsuario']=='ADMINISTRACION' || $_SESSION['tipoUsuario']=='ENFERMERIA'  || $_SESSION['tipoUsuario']=='TECNICO'){
                    echo '
                          <a href="'.$_CONFIG['raiz'].'empleados/index.php?eliminado=NO&codigo='.$cliente.'" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
                          <a href="javascript:void" id="reactivar" class="shortcut noAjax"><i class="shortcut-icon icon-check-circle"></i><span class="shortcut-label">Restaurar empleado/s</span> </a>';

                
                  } 
                  elseif($_SESSION['tipoUsuario']!='CLIENTE'){
                    /*echo '<a href="seleccionaCliente.php" class="shortcut noAjax"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label"> Volver</span> </a>';*/
                  }
                  else{
                    echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="icon-info-circle"></i> <strong> Sin acciones disponibles </strong></div>';
                  }
                ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Empleados eliminados</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Nº Empleado</th>
                  <th> Nombre </th>
                  <th> Apellidos </th>
                  <th> DNI </th>
                  <th> Sexo </th>
                  <th> Teléfono </th>
                  <th> eMail </th>
                  <th> Formado </th>
                  <th class="centro"></th>
                  <?php if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='CLIENTE' || $_SESSION['tipoUsuario']=='ADMINISTRACION' || $_SESSION['tipoUsuario']=='ENFERMERIA'  || $_SESSION['tipoUsuario']=='TECNICO'){ ?>
                  <th><input type='checkbox' id="todo"></th>
                  <?php } ?>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeEmpleados($cliente,$eliminado);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>