<?php
  $seccionActiva=12;
  include_once("../cabecera.php");
  $res=operacionesEmpleados();
?>
<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

         <div class="span12">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-list"></i> <i class="icon-chevron-right"></i> <i class="icon-search"></i>
              <h3>Visualización por cliente</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content centro">
              <h3>Seleccione un cliente para gestionar sus empleados:</h3><br />
				      <form action='index.php' method='post'>
      					<div class='control-group'>                     
      					  <div class='controls'>
                    <?php
                    campoSelectClienteEmpleado();
                    ?>                              
      					  </div> <!-- /controls -->       
      					</div> <!-- /control-group -->
					
    					  <button type="submit" class="btn btn-propio">Seleccionar <i class="icon-chevron-right"></i></button>
    				  </form>
            </div>
            <!-- /widget-content --> 
           </div>
          </div>

          <div class="span12">        
            <div class="widget widget-table action-table">
              <div class="widget-header"> <i class="icon-list"></i>
                <h3>Empleados registrados</h3>
              </div>
              <!-- /widget-header -->
              <div class="widget-content">
                <table class="table table-striped table-bordered datatable tablaTextoPeque" id="tablaEmpleados">
                  <thead>
                    <tr>
                      <th> Nombre </th>
                      <th> Apellidos </th>
                      <th> Nº Empleado</th>
                      <th> Puesto </th>
                      <th> DNI </th>
                      <th> Sexo </th>
                      <th> Teléfono </th>
                      <th> eMail </th>
                      <th> Estado </th>
                      <th> Formado </th>
                      <th class="centro"></th>
                      <?php if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='CLIENTE'){?>
                      <!--<th><input type='checkbox' id="todo"></th>-->
                      <?php } ?>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                    imprimeEmpleadosGeneral();
                  ?>
                  </tbody>
                </table>
            </div>
          <!-- /widget-content-->
          </div>
        </div>
        
    </div>
		
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->
<?php
abreVentanaModalConFichero('formación');
  campoOculto('','codigoCliente');
  campoOculto('','codigoPasar');
  echo '<div class="vacio"></div>';
  creaTablaDocumentos();
cierraVentanaModal();
?>

<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filasTabla.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();

    $('#tablaEmpleados').on('click','.btnFormacion',function(e){
      e.preventDefault();
      var codigoCliente=$(this).attr('codigoCliente');
      var codigoPasar=$(this).attr('codigoPasar');
      $('#cajaGestion #codigoCliente').val(codigoCliente);
      $('#cajaGestion #codigoPasar').val(codigoPasar);

      var consulta=$.post('../listadoAjax.php?include=empleados&funcion=obtieneDatosFormacion();',{'codigoCliente':codigoCliente,'codigoPasar':codigoPasar},
      function(respuesta){
        $('#cajaGestion .modal-body div.vacio').html('');
        $('#cajaGestion .modal-body div.vacio').append(respuesta.empresa);
        $('#cajaGestion .modal-body div.vacio').append(respuesta.puesto);
        $('#cajaGestion .modal-body div.vacio').append(respuesta.form);
        $('.selectpicker').selectpicker();
      },'json');
      $('#cajaGestion').modal({'show':true,'backdrop':'static','keyboard':false});
    });

    $('#cajaGestion #enviar').unbind();
    $('#cajaGestion #enviar').click(function(e){
      e.preventDefault();
      $('#cajaGestion').modal('hide');
      $('#cajaGestion form').submit();
    })
  });
</script>

</div>

<?php include_once('../pie.php'); ?>