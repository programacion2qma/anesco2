<?php
    session_start();
    @include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
    include_once('../funciones.php');
    require_once("../../api/js/firma/signature-to-image.php");

    conexionBDFormacion();
    if(isset($_POST['empleado'])){
    	$empleado=$_POST['empleado'];
    	$numeroCertificado=generaNumeroReferencia('puestos_empleados','numeroCertificado');
    	$consulta=consultaBD("UPDATE puestos_empleados SET firma='".$_POST['firma']."', fechaCertificado='".date('Y-m-d')."', numeroCertificado='".$numeroCertificado."' WHERE codigo=".$empleado,true);
    } else {
    	$empleado=$_GET['empleado'];

    	$compruebaCertificado=consultaBD("SELECT numeroCertificado FROM puestos_empleados WHERE codigo=".$empleado,false,true);
    	if($compruebaCertificado['numeroCertificado']==0 || $compruebaCertificado['numeroCertificado']==''){
    		$numeroCertificado=generaNumeroReferencia('puestos_empleados','numeroCertificado');
    		$consulta=consultaBD("UPDATE puestos_empleados SET  fechaCertificado='".date('Y-m-d')."', numeroCertificado='".$numeroCertificado."' WHERE codigo=".$empleado);
    	}
    }

    if(isset($_POST['codigoCurso']) || isset($_GET['codigoCurso'])){
    	$codigoCurso=isset($_POST['codigoCurso'])?$_POST['codigoCurso']:$_GET['codigoCurso'];
    	$contenido=generaCertificado($empleado,$codigoCurso);
    }else{
    	$contenido=generaCertificado($empleado);
    }
    cierraBD();
    

    require_once('../../api/html2pdf/html2pdf.class.php');
    $html2pdf=new HTML2PDF('L','A4','es',true,'UTF-8',array(0,0,0,0));
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->setDefaultFont("adonide");
    $html2pdf->WriteHTML($contenido);
    $html2pdf->Output('Certificado.pdf');

function generaCertificado($empleado=false,$codigo=''){
	global $_CONFIG;
	$texto='ha superado con éxito la formación relacionada con el puesto de';
	$gestiones=obtenerGestionCertificado($empleado,'NULL','PUESTO');
	conexionBDFormacion();
	$datos=consultaBD('SELECT CONCAT(usuarios.nombre," ",usuarios.apellidos) AS nombreEmpleado, usuarios.dni, puestos_empleados.fechaInicio, puestos_empleados.fechaFin, empresas.nombre AS nombreEmpresa, puestos_trabajos.nombre AS nombrePuesto, empresas.cif, fechaCertificado, firma, numeroCertificado, firmaImagen FROM puestos_empleados INNER JOIN usuarios ON puestos_empleados.codigoEmpleado=usuarios.codigo INNER JOIN puestos_empresas ON puestos_empleados.codigoPuesto=puestos_empresas.codigo INNER JOIN empresas ON puestos_empresas.codigoEmpresa=empresas.codigo INNER JOIN puestos_trabajos ON puestos_empresas.codigoPuesto=puestos_trabajos.codigo WHERE puestos_empleados.codigo='.$empleado,false,true);
	
	$anio=explode('-', $datos['fechaCertificado']);
	$firma='';
	if($datos['firma'] != ''){
		$img = sigJsonToImage($datos['firma'], array('imageSize'=>array(300, 150),'bgColour'=>'transparent'));
		$f='../documentos/Firma.png';
		if(file_exists($f)){
			unlink($f);
		}
		imagepng($img, $f);
       	$firma = "<img class='firmaAlumno' src='".$f."'>";
	} else if($datos['firmaImagen']!='NO' && $datos['firmaImagen']!=''){
		$firma = "<img class='firmaAlumno' src='../../anesco-formacion/documentos/personal/".$datos['firmaImagen']."'>";
	}
	$fechaPartida=explode('-', $datos['fechaCertificado']);
	$meses=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
	$ultimoCurso=consultaBD('SELECT fechaFin FROM cursos_empleados WHERE codigoEmpleado='.$empleado.' ORDER BY fechaFin DESC LIMIT 1',false,true);
	$ultimoDocumento=consultaBD('SELECT documentos_formacion.fecha FROM documentos_formacion INNER JOIN cursos_empleados ON documentos_formacion.codigoCurso=cursos_empleados.codigo WHERE cursos_empleados.codigoEmpleado='.$empleado.' ORDER BY documentos_formacion.fecha DESC LIMIT 1',false,true);
	$fechaFirma=comparaFechas($ultimoCurso['fechaFin'],$ultimoDocumento['fecha']);
	if($fechaFirma==-1){
		$fechaFirma=explode('-',$ultimoCurso['fechaFin']);
	} else {
		$fechaFirma=explode('-',$ultimoDocumento['fecha']);
	}
    $contenido = "
	<style type='text/css'>
	<!--
	        body{
	            font-size:12px;
	            font-family: helvetica;
	            font-weight: lighter;
	            line-height: 24px;
	            text-align:justify;
	        }

	        #completo{
	        	width:94%;
	        	height:91%;
	        	margin:32px;
	        	position:relative;
	        	background-repeat: no-repeat;
	        	border:2px solid rgb(17,53,147);
	    	}

	    	#completo img.fondo{
	        	width:96%;
	        	position:absolute;
	        	top:0;
	        	left:0;
	        	z-index:0;
	    	}

	        .cabecera{
	        	width:100%;
	        	margin-top:0px;
	        	margin-bottom:30px;
	        	text-align:center;
	        	font-size:14px;
	        	color:#0067B6;
	        	z-index:1;
	        }

	        .cabecera img{
	        	width:100px;
	        }

	        h1{
	        	text-align:center;
	        	font-weight:none;
	        }

	        table{
	        	width:100%;
	        }

	        table td{
	        	text-align:left;
	        	padding-left:200px;
	        	width:50%;
	        	padding-bottom:10px;
	        }

	        table td .firmaAlumno{
	        	width:190px;
	        }

	        table td .firmaCertificado{
	        	width:200px;
	        }

	        li{
	        	padding-bottom:10px;
	        }

	        #pie{
	        	margin-bottom:40px;
	    	}

 			#pie p{
	        	text-align:center;
	        	font-size:10px;
	        	color:rgb(110,131,189)
	    	}

	    	#pie span.enlace{
	    		font-weight:bold;
	    		font-size:14px;
	    		color:#0067B6;
	    	}

	    	.tiraCompleta{
	    		background:rgb(17,53,147);
	    		padding:10px 0px;
	    		color:#FFF;
	    		font-size:36px;
	    		font-weight:bold;
	    		text-align:center;
	    		position:absolute;
	    		top:180px;
	    		right:0px;
	    		width:100%;
	    		border-top:2px solid rgb(148,171,243);
	    		border-bottom:2px solid rgb(148,171,243);
	    	}

	    	p.texto{
	    		font-size:16px;
	    		text-align:justify;
	    		padding:5%;
	    		margin:20px 100px;
	    	}
	-->
	</style>
	<page backbottom='0mm'>
		<div class='tiraCompleta'>
			Certificado de aprovechamiento
		</div>
		<div id='completo'>
	    	<div class='cabecera'>
	    		<img src='../img/logoCertificado2.png'>
	    	</div><br/><br/><br/><br/>
	    	<p class='texto'>D. / Dª ".$datos['nombreEmpleado'].", con NIF ".$datos['dni']." ha sido informado de los riesgos laborales y ".$texto."</p>";
	    	
	    	$erpt="";
	    	if($codigo!=''){
	    		$cursos=consultaBD('SELECT cursos.nombre, cursos.descripcion, cursos.fichero, cursos.temas,cursos.puestoCertificado, cursos.fechaInicio, cursos_empleados.fechaFin, cursos.duracion, cursos.modalidad, cursos.observaciones, cursos_empleados.codigo AS codigoCursoEmpleado FROM cursos_empleados INNER JOIN cursos_puestos ON cursos_empleados.codigoCurso=cursos_puestos.codigo INNER JOIN cursos ON cursos_puestos.codigoCurso=cursos.codigo WHERE cursos_empleados.codigoEmpleado='.$empleado.' AND cursos_puestos.codigoCurso='.$codigo,false);
	    		$curso=mysql_fetch_assoc($cursos);
	    		$fechaFin=explode('-', $curso['fechaFin']);
	    		$infoCurso=consultaBD('SELECT * FROM cursos WHERE codigo='.$codigo,false,true);
	    		$contenido.="<h1>".$infoCurso['nombre']."</h1>";
	    		//$erpt="<li>Información ERPT ".$infoCurso['nombre']."</li>";
	    		if($curso['modalidad']=='teleformación'){
	    			$modalidad='de teleformación';
	    		} else if($curso['modalidad']=='distancia'){
	    			$modalidad='a distancia';
	    		} else {
	    			$modalidad=$curso['modalidad'];
	    		}
	    		if($modalidad==''){
	    			$modalidad='de teleformación';
	    		}
	    		$listadoFichero=consultaBD('SELECT codigo,nombre, fecha FROM documentos_formacion WHERE codigoCurso='.$curso['codigoCursoEmpleado']);
	    		while($f=mysql_fetch_assoc($listadoFichero)){
	    			if(!$gestiones || in_array($f['codigo'],$gestiones)){
	    				$erpt.='<li>'.$f['nombre'].'. Finalizado el '.formateaFechaWeb($f['fecha']).'</li>';
	    			}
	    		}
	    	}else{
	    		$cursos=consultaBD('SELECT cursos.nombre, cursos.descripcion, cursos.fichero, cursos.temas,cursos.puestoCertificado, cursos.fechaInicio, cursos_empleados.fechaFin, cursos.duracion, cursos.modalidad, cursos.observaciones, cursos_empleados.codigo AS codigoCursoEmpleado, cursos.codigo AS codigoCurso FROM cursos_empleados INNER JOIN cursos_puestos ON cursos_empleados.codigoCurso=cursos_puestos.codigo INNER JOIN cursos ON cursos_puestos.codigoCurso=cursos.codigo WHERE cursos_empleados.codigoEmpleado='.$empleado);
	    		$puesto=consultaBD('SELECT puestos_trabajos.nombre FROM puestos_empleados INNER JOIN puestos_empresas ON puestos_empleados.codigoPuesto=puestos_empresas.codigo INNER JOIN puestos_trabajos ON puestos_empresas.codigoPuesto=puestos_trabajos.codigo WHERE puestos_empleados.codigo='.$empleado,false,true);
	    		$contenido.="<h1>".$puesto['nombre']."</h1>";
	    		//$erpt="<li>Información ERPT ".$puesto['nombre']."</li>";
	    		$modalidad="";
	    		while($curso=mysql_fetch_assoc($cursos)){
	    			if(!$gestiones || in_array($curso['codigoCursoEmpleado'],$gestiones)){
	    				$gestiones2=obtenerGestionCertificado($empleado,$curso['codigoCurso'],'CURSO');
	    				if($curso['modalidad']!=''){
	    					if($curso['modalidad']=='teleformación'){
	    						$modalidad='de teleformación';
	    					} else if($curso['modalidad']=='distancia'){
	    						$modalidad='a distancia';
	    					} else {
	    						$modalidad=$curso['modalidad'];
	    					}
	    				}
	    				$listadoFichero=consultaBD('SELECT codigo,nombre,fecha FROM documentos_formacion WHERE codigoCurso='.$curso['codigoCursoEmpleado']);
	    				while($f=mysql_fetch_assoc($listadoFichero)){
	    					if(!$gestiones2 || in_array($f['codigo'],$gestiones2)){
	    						$erpt.='<li>'.$f['nombre'].'. Finalizado el '.formateaFechaWeb($f['fecha']).'</li>';
	    					}
	    				}
	    			}
	    		}
	    		$cursos=consultaBD('SELECT cursos.nombre, cursos.descripcion, cursos.fichero, cursos.temas,cursos.puestoCertificado, cursos.fechaInicio, cursos_empleados.fechaFin, cursos.duracion, cursos.modalidad, cursos.observaciones, cursos_empleados.codigo AS codigoCursoEmpleado FROM cursos_empleados INNER JOIN cursos_puestos ON cursos_empleados.codigoCurso=cursos_puestos.codigo INNER JOIN cursos ON cursos_puestos.codigoCurso=cursos.codigo WHERE cursos_empleados.codigoEmpleado='.$empleado);
	    		$fechaFin=$fechaPartida;
	    	}
	    	 //cierraBD();
	    	$contenido.="<p class='texto'>Impartida en la modalidad ".$modalidad.", de acuerdo a los artículos 18 y 19 de la ley 31/1995 y tomando como base los riesgos detectados en la correspondiente evaluación de riesgos de la empresa <b>".$datos['nombreEmpresa']."</b>, con una carga lectiva de 2 horas.</p>";
	    	$contenido.="<p class='texto' style='margin-top:0px;'>Formación e información impartidas y certificadas por Anesco Salud y Prevención S.L., como entidad autorizada por la Consejería de Trabajo de Andalucía con el número SP-304/13. Certificado número ".$fechaPartida[0]."/".$fechaPartida[1]."/".$datos['numeroCertificado']."</p>";
	    	$contenido.="
	    	<br/><br/>
	    	<table>
	    		<tr>
	    			<td>El Alumno/a</td>
	    			<td>En Sevilla a ".$fechaFirma[2]." de ".$meses[$fechaFirma[1]]." de ".$fechaFirma[0]."</td>
	    		</tr>
	    		<tr>
	    			<td>".$firma."</td>
	    			<td><img class='firmaCertificado' src='../img/firmaCertificado.png'></td>
	    		</tr>
	    	</table>
	    </div>
	    <page_footer>
			<div id='pie'>
				<p>ANESCO SALUD Y PREVENCIÓN, S.L.<br/>
				C/ Murillo 1, 2ª planta, 41001. Sevilla<br/>
				www.anescoprl.es</p>
			</div>
		</page_footer>
	</page>
	<page>
		<div id='completo'>
	    <ul style='list-style:none'>
	    <li><b>Contenido formación</b></li>";

	    if($codigo==''){
	    	while($curso=mysql_fetch_assoc($cursos)){
	    		if(!$gestiones || in_array($curso['codigoCursoEmpleado'],$gestiones)){
	    			$contenido.="<li><b>".$curso['nombre']."</b>: ".$curso['descripcion'].' - Finalizado el '.formateaFechaWeb($curso['fechaFin']).'<br/><br/>';
	    			$indice=preparaArrayTemas($curso['fichero']);
	    			$temas=explode('&{}&', $curso['temas']);
	    			$i=1;
	    			foreach ($indice as $key => $value) {
	    				if(in_array($key+1, $temas)){
	    				$contenido.='<b>'.$i."</b>.- ".$value.', ';
	    					$i++;
	    				}
	    			}
	    			$contenido = substr($contenido, 0, -2);
	    			$contenido.='.</li>';
	    		}
	    	}
	    
	}else{
			$contenido.="<li><b>".$infoCurso['nombre']."</b>: ".$infoCurso['descripcion'].'<br/><br/>';
	    	$indice=preparaArrayTemas($infoCurso['fichero']);
	    	$temas=explode('&{}&', $infoCurso['temas']);
	    	$i=1;
	    	foreach ($indice as $key => $value) {
	    		if(in_array($key+1, $temas)){
	    		$contenido.='<b>'.$i."</b>.- ".$value.', ';
	    			$i++;
	    		}
	    	}
	    	$contenido = substr($contenido, 0, -2);
	    	$contenido.='.</li>';	
	}
$contenido.="
		<li><b>Contenido información</b></li>
		".$erpt."
		</ul>
		</div>
		<page_footer>
			<div id='pie'>
				<p>ANESCO SALUD Y PREVENCIÓN, S.L.<br/>
				C/ Murillo 1, 2ª planta, 41001. Sevilla<br/>
				www.anescoprl.es</p>
			</div>
		</page_footer>
	</page>";


	return $contenido;
}

?>