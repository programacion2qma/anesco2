<?php
  $seccionActiva=12;
  include_once('../cabecera.php');

  $eliminado='NO';
  if(isset($_GET['eliminado'])){
    $eliminado=$_GET['eliminado'];
  }
  
  if(isset($_POST['codigoClienteImportar'])){
    $_POST['codigoCliente']=$_POST['codigoClienteImportar'];
  }
  $res=operacionesEmpleados();
  $cliente=obtieneCodigoCliente();
  $estadisticas=estadisticasEmpleadosRestrict($cliente,$eliminado);
  $empleados=obtenerCampoCliente($cliente,'EMPNTRAB');
  $nombre=obtenerCampoCliente($cliente,"CONCAT(EMPNOMBRE,' (',EMPMARCA,')')");
  $icono='<i class="icon-users"></i>';
  if($empleados < $estadisticas['total']){
    $icono='<i class="icon-exclamation-circle plazoSuperado"></i>';
  }
  $filtroFormacion='';
  if(isset($_GET['formacion'])){
    $filtroFormacion=$_GET['formacion'];
  }
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de gestión Empleados de <?php echo $nombre; ?>:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <?php echo $icono; ?>  <span class="value"><?php echo $estadisticas['total'].' / '.$empleados; ?></span><br />Empleados registrados / Empleados en ficha de cliente</div>
                     <!--div class="stat"> <i class="icon-send"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Enviados a la agencia</div-->
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de empleados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">

                <?php 

                  if($_SESSION['tipoUsuario']!='CLIENTE'){
                    echo '
                    <a href="'.$_CONFIG['raiz'].'empleados/seleccionaCliente.php" class="shortcut"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label">Volver</span> </a>';
                  } 

                  if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='CLIENTE' || $_SESSION['tipoUsuario']=='ADMINISTRACION' || $_SESSION['tipoUsuario']=='ENFERMERIA'  || $_SESSION['tipoUsuario']=='TECNICO'){
                    echo '
                          <a href="'.$_CONFIG['raiz'].'empleados/gestion.php?codigoCliente='.$cliente.'" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nuevo empleado</span> </a>
                          <a href="#" id="importarDatos" class="shortcut noAjax"><i class="shortcut-icon icon-cloud-upload"></i><span class="shortcut-label">Importar Empleados</span> </a><br/>
                          <a href="javascript:void" id="eliminar" class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
                          <a href="'.$_CONFIG['raiz'].'empleados/eliminados.php?eliminado=SI&codigo='.$cliente.'" class="shortcut"><i class="shortcut-icon icon-trash-o"></i><span class="shortcut-label">Eliminados</span> </a>';
                          creaVentanaImportarFichero($cliente);

                    /*if($_SESSION['tipoUsuario']=='CLIENTE'){       
                      creaVentanaPuestos();               
                      echo '<a href="#" id="puestosEmpleados" class="shortcut noAjax"><i class="shortcut-icon icon-sitemap"></i><span class="shortcut-label">Ver<br/>puestos</span> </a>';
                    }*/
                  } 
                  else{
                    echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><i class="icon-info-circle"></i> <strong> Sin acciones disponibles </strong></div>';
                  }

                  echo '<br/>';
                  if(isset($_GET['formacion'])){
                  echo '<a href="'.$_CONFIG['raiz'].'empleados/index.php?eliminado=NO&codigo='.$cliente.'" class="shortcut"><i class="shortcut-icon icon-users"></i><span class="shortcut-label">Todos</span> </a>';
                  }
                  if(!isset($_GET['formacion']) || $_GET['formacion']!='SI'){
                  echo '<a href="'.$_CONFIG['raiz'].'empleados/index.php?eliminado=NO&codigo='.$cliente.'&formacion=SI" class="shortcut"><i class="shortcut-icon icon-check-circle"></i><span class="shortcut-label">Formados</span> </a>';
                  }
                  if(!isset($_GET['formacion']) || $_GET['formacion']!='NO'){
                  echo '<a href="'.$_CONFIG['raiz'].'empleados/index.php?eliminado=NO&codigo='.$cliente.'&formacion=NO" class="shortcut"><i class="shortcut-icon icon-times-circle"></i><span class="shortcut-label">No formados</span> </a>';
                  }
                  if(!isset($_GET['formacion']) || $_GET['formacion']!='CADUCADO'){
                  echo '<a href="'.$_CONFIG['raiz'].'empleados/index.php?eliminado=NO&codigo='.$cliente.'&formacion=CADUCADO" class="shortcut"><i class="shortcut-icon icon-exclamation-circle"></i><span class="shortcut-label">Certificados<br/>caducados</span> </a>';
                  }
                ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Empleados registrados</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable" id="tablaEmpleados">
              <thead>
                <tr>
                  <th> Nº Empleado</th>
                  <th> Nombre </th>
                  <th> Apellidos </th>
                  <th> DNI </th>
                  <th> Sexo </th>
                  <th> Teléfono </th>
                  <th> eMail </th>
                  <th> Formado </th>
                  <th class="centro"></th>
                  <?php if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='CLIENTE' || $_SESSION['tipoUsuario']=='TECNICO'){?>
                  <th><input type='checkbox' id="todo"></th>
                  <?php } ?>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeEmpleados($cliente,$eliminado,false,$filtroFormacion);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->
<?php
abreVentanaModalConFichero('formación');
  campoOculto('','codigoCliente');
  campoOculto('','codigoPasar');
  echo '<div class="vacio"></div>';
  creaTablaDocumentos();
cierraVentanaModal();
?>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filasTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
  $('#importarDatos').click(function(){
    abreVentana('#cajaFichero');
  });
  $('#puestosEmpleados').click(function(){
    abreVentana('#cajaPuestos');
  });

  $('#cajaPuestos select').val(10);
  $('#cajaPuestos select').trigger("change");

  $('#importaFichero').attr('data-dismiss','modal')
  $('#importaFichero').click(function(){
    $('#cajaFichero form').submit();
  });

  $('#tablaEmpleados').on('click','.btnFormacion',function(e){
      e.preventDefault();
      var codigoCliente=$(this).attr('codigoCliente');
      var codigoPasar=$(this).attr('codigoPasar');
      $('#cajaGestion #codigoCliente').val(codigoCliente);
      $('#cajaGestion #codigoPasar').val(codigoPasar);

      var consulta=$.post('../listadoAjax.php?include=empleados&funcion=obtieneDatosFormacion();',{'codigoCliente':codigoCliente,'codigoPasar':codigoPasar},
      function(respuesta){
        $('#cajaGestion .modal-body div.vacio').html('');
        $('#cajaGestion .modal-body div.vacio').append(respuesta.empresa);
        $('#cajaGestion .modal-body div.vacio').append(respuesta.puesto);
        $('#cajaGestion .modal-body div.vacio').append(respuesta.form);
        $('.selectpicker').selectpicker();
      },'json');
      $('#cajaGestion').modal({'show':true,'backdrop':'static','keyboard':false});
    });

    $('#cajaGestion #enviar').unbind();
    $('#cajaGestion #enviar').click(function(e){
      e.preventDefault();
      $('#cajaGestion').modal('hide');
      $('#cajaGestion form').submit();
    })
});

function abreVentana(id){
  $(id).modal({'show':true,'backdrop':'static','keyboard':false});
}
</script>
<!-- contenido --></div>

<?php include_once('../pie.php'); ?>