<?php
  $seccionActiva=12;
  include_once("../cabecera.php");
  gestionEmpleados();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.selectpicker').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$('#fechaNacimiento').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');calcular_edad('#fechaNacimiento');});
	$(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});

	$("#nif").focusout(function(){
		isValidNif($(this).val());
        if($('#usuario').val()==''){
            $('#usuario').val($(this).val());
        }
	});

  $('#tablaFormacion').on('click','.btnModificaCurso',function(e){
    e.preventDefault();
    alert('Indica en el campo Nuevo curso, el curso por el que lo quieres reemplazar');
    $('#codigoCursoModificar').val($(this).attr('codigo'))
  });
    $('#usuario,#clave').attr('autocomplete','off');

    //$('.datepicker').addClass('obligatorio'); 
    $('.form-horizontal button[type=submit]').unbind();
    $('.form-horizontal button[type=submit]').click(function(e){
      e.preventDefault();
      var obligatorio=true;
      /*$("input.obligatorio").each(function( index ) {
        if($(this).val()==''){
          obligatorio=false;
        }
      });
      if($('#codigoPuestoTrabajo').val()=='NULL'){
        obligatorio=false;
      }*/
      if(obligatorio){
        e.preventDefault();
        if(($('#usuario').val()=='' || $('#clave').val()=='') && $('#tipoUsuarioConectado').val().trim()!='CLIENTE'){
          alert('Por favor, introduzca unas credenciales para el usuario.');
        } else {
          var usuario=$('#usuario').val();
          var codigoUsuario=$('#codigoUsuario').val();
          if(codigoUsuario==undefined){
            codigoUsuario=0;
          }
          var consulta=$.post('../listadoAjax.php?funcion=compruebaUsuario();',{'codigoUsuario':codigoUsuario,'usuario':usuario});
          consulta.done(function(respuesta){
            if(respuesta=='error' && $('#tipoUsuarioConectado').val().trim()!='CLIENTE'){
              alert('Nombre de usuario repetido. Por favor, elija otro nombre.');
              $('#usuario').focus();
            } else {
              if($('#dni').val()==''){
                alert('El campo DNI es obligatorio');
              } else {
                var dni=$('#dni').val();
                var codigoCliente=$('#codigoCliente').val();
                var codigoEmpleado=$('#codigo').val();
                if(codigoEmpleado==undefined){
                  codigoEmpleado=0;
                }
                var consulta=$.post('../listadoAjax.php?include=empleados&funcion=compruebaEmpleado();',{'codigoEmpleado':codigoEmpleado,'dni':dni,'codigoCliente':codigoCliente});
                consulta.done(function(respuesta){ 
                  if(respuesta=='error'){
                    alert('Este empleado ya está registrado en esta empresa.');
                  } else {
                    $('.form-horizontal').submit();
                  }
                });
              }
            }
          });
        }
      } else {
        alert('Por favor, rellene todos los campos obligatorios');
      }
    });

	$('#telefono, #fax').attr('maxlength','9');
    $('#dni').change(function(){
      cambiarCampo($(this),$('#usuario'));
    });
    $('#apellidos').change(function(){
      cambiarCampo($(this),$('#clave'));
    });

    oyenteLOPD();
    $('input[name=lopd]').change(function(){
        oyenteLOPD();
    });

    oyenteActivo();
    $('input[name=activoEmpleado]').change(function(){
        oyenteActivo();
    });

    $('.eliminaFichero').click(function(){
      var codigo=$(this).attr('codigo');
      var id=$(this).attr('idAsociado');
      $('#'+id).remove();
      var elem=$(this).parent();
      elem.html("<input type='file' name='"+id+"' id='"+id+"' />");
      $(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar"});
       //eliminaFichero(codigo,id.split('_'));
    }); 
});

function eliminaFichero(codigo,campo){
  var consulta=$.post('../gestionesAjax.php?include=no-conformidades&funcion=eliminaCroquis();',{'codigo':codigo,'campo':campo[0]}
    ,function(respuesta){
    });
}

function calcular_edad(id){
    var hoy=new Date()
    var fecha=$(id).val();
    var array_fecha = fecha.split("/")
    
    if(fecha.trim()==''){
        alert('entra');
        return false;
    }

    //si el array no tiene tres partes, la fecha es incorrecta
    if (array_fecha.length!=3)
       return false

    //compruebo que los ano, mes, dia son correctos
    var ano
    ano = parseInt(array_fecha[2]);
    if (isNaN(ano))
       return false

    var mes
    mes = parseInt(array_fecha[1]);
    if (isNaN(mes))
       return false

    var dia
    dia = parseInt(array_fecha[0]);
    if (isNaN(dia))
       return false

    //si el año de la fecha que recibo solo tiene 2 cifras hay que cambiarlo a 4
    if (ano<=99)
       ano +=1900

    //resto los años de las dos fechas
    edad=hoy.getFullYear()- ano - 1; //-1 porque no se si ha cumplido años ya este año
    //si resto los meses y me da menor que 0 entonces no ha cumplido años. Si da mayor si ha cumplido
    if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0
       edad = edad
    if (hoy.getMonth() + 1 - mes > 0)
       edad = edad+1

    //entonces es que eran iguales. miro los dias
    //si resto los dias y me da menor que 0 entonces no ha cumplido años. Si da mayor o igual si ha cumplido
    if (hoy.getUTCDate() - dia >= 0)
       edad = edad + 1

    $('#edad').val(edad);
}

function cambiarCampo(campo,destino){
  var anterior=destino.val();
  if(anterior==''){
    destino.val(limpiarCadena(campo.val()));
  } else {
    texto="El campo Usuario está relleno ,¿Quiere sustituirlo por el nuevo dato?";
    if(campo.attr('id')=='apellidos'){
      texto="El campo Contraseña está relleno ,¿Quiere sustituirlo por el nuevo dato?";
    }
    if(confirm(texto)){
      destino.val(limpiarCadena(campo.val()));
    }
  }
} 

function limpiarCadena(cadena){
   // Definimos los caracteres que queremos eliminar
   var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";

   // Los eliminamos todos
   for (var i = 0; i < specialChars.length; i++) {
       cadena= cadena.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
   }   

   // Quitamos espacios y los sustituimos por _ porque nos gusta mas asi
   cadena = cadena.replace(/ /g,"_");

   // Quitamos acentos y "ñ". Fijate en que va sin comillas el primer parametro
   cadena = cadena.replace(/á/gi,"a");
   cadena = cadena.replace(/é/gi,"e");
   cadena = cadena.replace(/í/gi,"i");
   cadena = cadena.replace(/ó/gi,"o");
   cadena = cadena.replace(/ú/gi,"u");
   cadena = cadena.replace(/ñ/gi,"n");
   return cadena;
}

function oyenteLOPD(){
    if($('input[name=lopd]:checked').val()=='SI'){
        $('#cajaFicheroLOPD').removeClass('hide');
    }
    else{
        $('#cajaFicheroLOPD').addClass('hide');
    }
}

function oyenteActivo(){
    
    if($('input[name=activoEmpleado]:checked').val()=='NO'){
        $('#cajaBaja').removeClass('hide');
    }
    else{
        $('#cajaBaja').addClass('hide');
    }
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>