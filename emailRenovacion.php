<?php

	date_default_timezone_set('Europe/Madrid');//Necesario para que no dé los warnings de fechas.
	$fechaInicio=date('Y-m-d');
	
	$conexion=mysql_connect("localhost","anesco2","Writemaster7");//PARA CONFIG
	//$conexion=mysql_connect("localhost","root","root");
	if(!$conexion){ 
		echo "Error estableciendo la conexi&oacute;n a la BBDD.<br />";
	}
	else{
		if(!mysql_select_db("anesco2")){
			echo "Error seleccionando base de datos QMA.<br />";
		}
	}

	$consulta=consultaBD("SELECT * FROM contratos WHERE enVigor='SI' AND fechaFin>'".date('Y-m-d').";'");
	while($item=mysql_fetch_assoc($consulta)){
		$dias=dias_transcurridos(date('Y-m-d'),$item['fechaFin']);
		//echo $item['codigo'].' - Fecha fin: '.formateaFechaWeb($item['fechaFin']).' - '.$dias.'<br/>';
		if($dias == '58'){
			enviarMail($item);
		}
		if($dias == '10'){
			enviarMail2($item);
		}
	}

	cierraBD();

	function dias_transcurridos($fecha_i,$fecha_f){
		$dias = 0;
		if($fecha_f != ''){
			$dias	= (strtotime($fecha_i)-strtotime($fecha_f))/86400;
			$dias 	= abs($dias); $dias = floor($dias);		
		}
		return $dias;
	}


	function consultaBD($query,$assoc=false){
		$consulta=mysql_query($query);
		if($assoc){
			$consulta=mysql_fetch_assoc($consulta);
		}
		return $consulta;
	}
	
	function cierraBD(){
		mysql_close();
	}

	function enviarMail($item){
		$cliente=consultaBD('SELECT clientes.* FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo='.$item['codigoOferta'],true);
		if($cliente['EMPEMAILPRINC']!=''){
			

			$mail_destinatario=$cliente['EMPEMAILPRINC'];

			$asunto = "Renovación de contrato - ".$cliente['EMPNOMBRE'];

            $mensaje="Estimado Cliente<br/><br/>
				Nuevamente queremos agradecerle la confianza depositada en nosotros al haber renovado recientemente el contrato con nuestro Servicio de Prevención.<br/><br/>
				Próximamente, el técnico asignado a su empresa  contactará con ustedes para recabar información sobre los cambios que pudieran haberse producido, ya sea por modificación del número de trabajadores o por cualquier otra circunstancia y remitirles la Memoria correspondiente a las actividades preventivas realizadas en el presente ejercicio.<br/><br/><br/><br/>
				Sin otro particular,  le  saluda atentamente:<br/><br/>
 
				Patricio Peñas del Bustillo<br/>
				Dirección Gerencia  ANESCO SALUD Y PREVENCIÓN S.L.<br/>";

			enviaEmail($mail_destinatario, $asunto ,$mensaje);
		}
	}

	function enviarMail2($item){
		$cliente=consultaBD('SELECT clientes.* FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo='.$item['codigoOferta'],true);
		if($cliente['EMPEMAILPRINC']!=''){
			$contenido=generaPDFFactura($item['codigo']);
			require_once('../api/html2pdf/html2pdf.class.php');
        	$html2pdf=new HTML2PDF('P','A4','es',true,'UTF-8',array(20,10,10,15));
        	$html2pdf->pdf->SetDisplayMode('fullpage');
        	$html2pdf->WriteHTML($contenido[1]);
			$html2pdf->Output($contenido[0].'.pdf','f');

			$mail_destinatario=$cliente['EMPEMAILPRINC'];

			$asunto = "Renovación de contrato - ".$cliente['EMPNOMBRE'];

			$mensaje="Su contrato con ANESCO SALUD Y PREVENCIÓN finaliza el día ".formateaFechaWeb($item['fechaFin']).".<br/><br/>Aquí le envío su factura proforma.<br/><br/>Póngase en contacto con nosotros para la renovación<br><br>
				Agradeciendo de antemano su atención, reciba un cordial saludo.<br /><br />
             	<img src='https://crmparapymes.com.es/anesco2/img/logo.png' /><br />
             	<div style='color:#075581;font-weight:bold'>
                 <i>ANESCO SERVICIO DE PREVENCION</i><br />
                 Tlf .954.10.92.93<br />
                 info@anescoprl.es · www.anescoprl.es<br />
                 C/ Murillo 1, 2ª P. 41001 - Sevilla.
             	</div>";

        	$adjunto=$contenido[0].'.pdf';

        	enviaEmail($mail_destinatario, $asunto ,$mensaje, $adjunto);
        }
	}

	function formateaFechaWeb($fechaV){
		$res='';

		if($fechaV!='0000-00-00' && trim($fechaV)!=''){
			$fechaA=explode("-",$fechaV);
			$fecha=$fechaA[2]."/".$fechaA[1]."/".$fechaA[0];
			$res=$fecha;
		}
	
		return $res;
	}

	function generaPDFFactura($codigo){

		$nombreFichero=array();
	
		$datos=consultaBD("SELECT facturas.codigo AS codigoFactura, facturas.*, serie, clientes.EMPNOMBRE AS cliente, clientes.EMPCIF AS cifCliente, clientes.EMPDIR AS domicilioCliente, 
    	clientes.EMPCP AS cpCliente, clientes.EMPLOC AS localidadCliente, clientes.EMPPROV AS provinciaCliente,
    	cuentas_propias.iban AS cuentaEmisor,
    	facturas.numero, clientes.EMPIBAN,
    	clientes.EMPCP, clientes.EMPID, contratos.fechaInicio, contratos.fechaFin, contratos.codigoInterno, IFNULL(formas_pago.codigo,'Pago') AS formaPago,
    	contratos.incrementoIPC, contratos.incrementoIpcAplicado, contratos.fechaFin
    	FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
    	LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
    	LEFT JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
    	LEFT JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
    	LEFT JOIN formas_pago ON contratos.codigoFormaPago=formas_pago.codigo
    	LEFT JOIN cuentas_propias ON contratos.codigoCuentaPropiaContrato=cuentas_propias.codigo
    	WHERE contratos.codigo=$codigo
    	GROUP BY facturas.codigo",true);

    	$emisor=consultaBD("SELECT 
    	emisores.razonSocial AS emisor, emisores.cif AS cifEmisor, emisores.domicilio AS domicilioEmisor,
    	emisores.cp AS cpEmisor, emisores.localidad AS localidadEmisor, emisores.provincia AS provinciaEmisor,
    	emisores.telefono, emisores.fax, emisores.web, emisores.email, emisores.registro, emisores.ficheroLogo, emisores.iban, emisores.registro AS registroPie
    	FROM emisores 
    	WHERE codigo=1",false,true);

		$referenciaContrato=formateaReferenciaContrato($datos,$datos);
		$referenciaContrato=explode('-',$referenciaContrato);
		$referenciaContrato=$referenciaContrato[0].'-'.$referenciaContrato[1].'-'.($referenciaContrato[2]+1).'-'.$referenciaContrato[3];

		$nombreFichero[0]=$datos['serie'].'-'.$datos['numero'].'-'.trim(limpiaCadena($datos['cliente']));
		$conceptos=obtieneConceptosFactura($datos['codigoFactura'],$datos,$referenciaContrato);

		$vencimientos=obtieneVencimientosFactura($datos['codigo'],$datos['EMPIBAN'],$datos['cuentaEmisor'],$datos['formaPago'],$datos);

		cierraBD();

		$observaciones=formateaObservacionesFactura($datos['observaciones']);

		$logo='../documentos/emisores/'.trim($emisor['ficheroLogo']);
		if(!file_exists($logo)){//Para evitar que la factura no se genere por no encontrar la imagen
			$logo='img/logo.png';
		}

	$contenido = "
	<style type='text/css'>
	<!--
		.tablaCabecera{
			width:100%;
		}

		.tablaCabecera td{
			width:50%;
		}

		.tablaCabecera .emisor{
			text-align:right;
			font-weight:bold;
			line-height:15px;
		}

		.tablaEmisorCliente{
			margin-top:10px;
			font-size:12px;
			width:100%;
			border-collapse:collapse;
		}

		.tablaBottom{
			top:740px;
			position: absolute;
		}

		.tablaEmisorCliente td{
			border-bottom:1px solid #000;
		}

		.celdaEmisor{
			width:49%;
			padding:10px 30px 1px 30px;
			border:1px solid #000;
		}

		.celdaCliente{
			border:1px solid #000;
			width:49%;
			padding:10px 30px 1px 30px;
			line-height:15px;
			text-align:center;
		}

		.celdaTabla{
			padding:0px;
			width:49%;
		}
		.celdaBlanca{
			width:2%;
			border-top:0px;
			border-bottom:0px;
		}

		.cajaNumero{
			margin-top:20px;
			font-size:12px;
		}

		.cajaFecha{
			text-align:right;
			margin-right:240px;
		}

		.cabeceraConceptos, .tablaConceptos{
			margin-top:20px;
			border:1px solid #000;
			width:100%;
		}
		.celdaConcepto{
			width:85%;
			padding:10px;
		}
		.celdaImporte{
			width:15%;
			text-align:right;
			padding:10px;
		}

		.tablaConceptos{
			border-collapse:collapse;
			margin-top:0px;
		}

		.tablaConceptos td, .tablaConceptos th{
			font-size:12px;
			border:1px solid #000;
			line-height:18px;
		}

		.tablaConceptos th{
			background:#EEE;
		}

		.tablaConceptos .celdaConcepto{
			border-left:1px solid #000;
		}

		.tablaConceptos .celdaImporte{
			border-right:1px solid #000;
		}

		.tablaTotales{
			width:100%;
		}

		.tablaTotales .celdaConcepto{
			width:60%;
		}
		.tablaTotales .celdaImporte{
			width:40%;
		}

		.cajaMedioPago{
			max-width:100%;
			font-size:12px;
			line-height:18px;
		}

		.cajaMedioPago ol{
			margin-top:-20px;
		}

		.pie{
			font-size:10px;
			text-align:center;
			line-height:18px;
			color:#0062B3;
		}

		.textoAdvertencia{
			font-size:11px;
			text-align:justify;
			font-weight:bold;
		}
		.centrado{
			text-align:center;
		}

		.cajaObservaciones{
			width:100%;
		}

		.logo{
			width:30%;
		}

		.titulo{
			background:#CCC;
			width:100%;
			text-align:center;
			margin:0px;
			padding:0px;
			font-weight:bold;
			font-size:28px;
			font-style:italic;
		}

		.tablaDatos{
			width:100%;
			border-collapse: collapse;
		}

		.tablaDatos th{
			font-weight:normal;
			border:1px solid #000;
			background:#EEE;
			padding:5px;
		}

		.tablaDatos td{
			font-weight:normal;
			border:1px solid #000;
			padding:5px;
		}

		.tablaDatos .a5{
			width:5%;
			text-align:center;
		}

		.tablaDatos .a9{
			width:9%;
			text-align:center;
		}

		.tablaDatos .a17{
			width:17%;
			text-align:center;
		}

		.tablaDatos .a35{
			width:35%;
		}

		.tablaDatos .a100{
			width:100%;
			padding:10px;
		}

		.centro{
			text-align:center;
		}

		ul li{
			font-size:14px;
		}

		.h200{
			height:200px;
		}

		.textoLateralFactura{
			position:absolute;
			height:60%;
			left:-50px;
			top:300px;
		}

		.enlacePie{
            display:block;
            position:absolute;
            bottom:20px;
            left:33%;
            text-align:center;
            color:#0062B3;
            font-size:11px;
            line-height:14px;
        }

        .enlacePie a{
            text-decoration:none;
            color:#0062B3;
        }
	-->
	</style>
	<page footer='page'>
	    <table class='tablaCabecera'>
	    	<tr>
	    		<td><img src='".$logo."' class='logo' /><br /></td>
	    		<td class='emisor'>
		    		".$emisor['emisor']."<br />
		    		".$emisor['cifEmisor']."<br/>
				    ".$emisor['domicilioEmisor']."<br />
				    ".$emisor['cpEmisor']." ".$emisor['localidadEmisor']."<br/>".$emisor['provinciaEmisor']."<br />
				    
		    	</td>
		    </tr>
	    </table>
	    <br/>
	    <div class='titulo'>
	    	FACTURA
	    </div>
	    <table class='tablaEmisorCliente'>
	    	<tr>
	    		<td>Factura</td>
	    		<td class='celdaBlanca'></td>
	    		<td>Cliente</td>
	    	</tr>
		    <tr>
		    	<td class='celdaEmisor'>
		    		Factura: <strong>PT - PROFORMA</strong><br />
				    Fecha: <strong>".formateaFechaWeb($datos['fechaFin'])."</strong><br />
				    Contrato: <strong>".$referenciaContrato."</strong>
		    	</td>
		    	<td class='celdaBlanca' style='border-right:1px solid #000'></td>
		    	<td class='celdaCliente'>
		    		<strong>".$datos['cliente']."</strong><br />
				    ".$datos['domicilioCliente']."<br />
				    ".$datos['cpCliente']." ".$datos['localidadCliente']." (".$datos['provinciaCliente'].")<br />
				    CIF: ".$datos['cifCliente']."
		    	</td>
		    </tr>
	    </table>
	    <br/>
	    <table class='tablaDatos'>
	    	<tr>
	    		<th class='a5 centro'>Ud.</th>
	    		<th class='a35'>Concepto</th>
	    		<th class='a17 centro'>Sujeto I.V.A</th>
	    		<th class='a17 centro'>Exento I.V.A</th>
	    		<th class='a9 centro'>I.V.A</th>
	    		<th class='a17 centro'>Total</th>
	    	</tr>
	    	".$conceptos['html']."	
	    </table>

	    <table class='tablaEmisorCliente tablaBottom'>
	    <tr>
	    		<td>Observaciones</td>
	    		<td class='celdaBlanca'></td>
	    		<td style='border-bottom:0px'></td>
	    </tr>
	    <tr>
	    	<td class='celdaEmisor'>".$observaciones."".$conceptos['rm']."</td>
	    	<td class='celdaBlanca' style='border-right:0px solid #000'></td>
	    	<td class='celdaTabla'>
			<table class='tablaConceptos tablaTotales'>
					<tr>
						<td class='celdaConcepto'>Exento de IVA:</td>
						<td class='celdaImporte'>".formateaNumeroWeb($conceptos['baseExenta'])." €</td>
					</tr>
					<tr>
						<td class='celdaConcepto'>Sujeto a IVA:</td>
						<td class='celdaImporte'>".formateaNumeroWeb($conceptos['baseImponible'])." €</td>
					</tr>
					<tr>
						<td class='celdaConcepto'>I.V.A. (21%):</td>
						<td class='celdaImporte'>".formateaNumeroWeb($conceptos['importeIva'])." €</td>
					</tr>
					<tr>
						<td class='celdaConcepto'></td>
						<td class='celdaImporte'></td>
					</tr>
					<tr>
						<th class='celdaConcepto'><b>Total factura:</b></th>
						<th class='celdaImporte'>".formateaNumeroWeb($conceptos['total'])." €</th>
					</tr>
			</table>
	    	</td>
	    </tr>
	    </table>

	    <div class='cajaMedioPago'>
	    	<strong>
		    	".$vencimientos."
		    </strong>
		</div>
		<img src='img/textoLateralFactura.png' class='textoLateralFactura' />

	    <page_footer>
	    	<div class='enlacePie'>
                <strong><i>ANESCO SERVICIO DE PREVENCION</i></strong><br />
                Tlf .954.10.92.93 <a href='mailto:administracion@anescoprl.es'>administracion@anescoprl.es</a><br />
                <a href='http://www.anescoprl.es'>www.anescoprl.es</a>
            </div>
            
	    	<div class='pie'>
	    	".$emisor['registroPie']."
	    	</div>
	    </page_footer>
	</page>";

	$nombreFichero[1]=$contenido;

	return $nombreFichero;
}

	function formateaReferenciaContrato($cliente,$datos){
    	$cp=substr($cliente['EMPCP'], 0,2);
    	$idCliente=substr(str_pad($cliente['EMPID'], 3,0,STR_PAD_LEFT),-3);
    	$anio=explode('-', $datos['fechaInicio']);
    	$anio=substr($anio[0],-2);
    	$idContrato=str_pad($datos['codigoInterno'], 3,0,STR_PAD_LEFT);
    	return $cp.'-'.$idCliente.'-'.$anio.'-'.$idContrato;
	}

	function limpiaCadena($string){
    $string=trim($string);

    $string=str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );

    $string=str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );

    $string=str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );

    $string=str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );

    $string=str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );

    $string=str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );

    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string=str_replace(
        array("\\", "¨", "º", "-", "~",
             "#", "@", "|", "!", "\"",
             "·", "$", "%", "&", "/",
             "(", ")", "?", "'", "¡",
             "¿", "[", "^", "`", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":",
             "."),
        '',
        $string
    );

	return $string;
}


function obtieneConceptosFactura($codigoFactura,$factura,$referenciaContrato){
    $html='';
    $rm='<br/><br/>* No incluye los reconocimientos médicos que serán objeto de facturación al mes cumplido de su realización';
    $baseImponible=0;
    $importeIva=0;
    $baseExenta=0;
    $total=0;

    $consulta=consultaBD("SELECT ofertas.total, ofertas.subtotal, ofertas.subtotalRM, ofertas.opcion, ofertas.otraOpcion, ofertas.numRM, ofertas.importe_iva, ofertas.iva, ofertas.especialidadTecnica,
                          contratos.incrementoIPC, contratos.incrementoIpcAplicado
                          FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
                          INNER JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
                          WHERE codigoFactura=$codigoFactura");

    while($datos=mysql_fetch_assoc($consulta)){
    	$fechaInicio=explode('-',$factura['fechaInicio']);
    	$fechaInicio=$fechaInicio[2].'/'.$fechaInicio[1].'/'.($fechaInicio[0]+1);
    	$fechaFin=explode('-',$factura['fechaFin']);
    	$fechaFin=$fechaFin[2].'/'.$fechaFin[1].'/'.($fechaFin[0]+1);
        //Parte de actividades
        $html.="<tr>
                    <td class='a5'>1</td>
                    <td class='a35'>";


            if($factura['serie']=='RM'){
                $html.="Reconocimientos médicos sujetos al contrato:<br />
                        Nº de contrato: ".$referenciaContrato."<br />
                        Vigencia desde: ".$fechaInicio.' hasta '.$fechaFin."<br />";
                $rm='';
            }
            elseif($datos['opcion']==1){
                $html.="Cuota única de Servicio de Prevención Ajeno con:<br />
                        Nº de contrato: ".$referenciaContrato."<br />
                        Vigencia desde: ".$fechaInicio.' hasta '.$fechaFin."<br />
                        Concierto Servicio de Prevención Ajeno para las 4 especialidades:<br />
                        Seguridad en el Trabajo,<br />
                        Higiene Industrial,<br />
                        Ergonomía y Psicosociología,<br />
                        Vigilancia de la Salud*";
            }
            elseif($datos['opcion']==2){
                $html.="Cuota única de Servicio de Prevención Ajeno con:<br />
                        Nº de contrato: ".$referenciaContrato."<br />
                        Vigencia desde: ".$fechaInicio.' hasta '.$fechaFin."<br />
                        Concierto Servicio de Prevención Ajeno para Vigilancia de la Salud*";
            }
            elseif($datos['opcion']==3){
                $especialidades=explode('&{}&',$datos['especialidadTecnica']);
                $especialidades=implode(', ',$especialidades);

                $html.="Cuota única de Servicio de Prevención Ajeno con:<br />
                        Nº de contrato: ".$referenciaContrato."<br />
                        Vigencia desde: ".$fechaInicio.' hasta '.$fechaFin."<br />
                        Concierto Servicio de Prevención Ajeno para la especialidad de ".$especialidades;
            }
            elseif($datos['opcion']==4){
                $html.='Cuota única de otras actuaciones: '.nl2br($datos['otraOpcion']);
                $rm='';
            }

        $html.="    </td>";

        if($factura['serie']=='RM'){
            $html.="<td class='a17'></td><td class='a17'>".compruebaImporteContrato($datos['subtotalRM'],$datos)."</td><td class='a9'></td><td class='a17'>".compruebaImporteContrato($datos['subtotalRM'],$datos)."</td>";
            $baseExenta+=$datos['subtotalRM'];
            $total+=$datos['subtotalRM']+$importeIva;
        } elseif($datos['iva']=='SI'){
            $totalIva=floatval($datos['subtotal'])+floatval($datos['importe_iva']);
            $html.="<td class='a17'>".compruebaImporteContrato($datos['subtotal'],$datos)."</td><td class='a17'></td><td class='a9'>".compruebaImporteContrato($datos['importe_iva'],$datos)."</td><td class='a17'>".compruebaImporteContrato($totalIva,$datos)."</td>";
            $baseImponible+=$datos['subtotal'];
            $importeIva+=$datos['importe_iva'];
            $total+=$datos['subtotal']+$importeIva;
        } else {
            $html.="<td class='a17'></td><td class='a17'>".compruebaImporteContrato($datos['subtotal'],$datos)."</td><td class='a9'></td><td class='a17'>".compruebaImporteContrato($datos['subtotal'],$datos)."</td>";
            $baseImponible+=$datos['subtotal'];
            $importeIva+=$datos['importe_iva'];
            $total+=$datos['subtotal']+$importeIva;
        }

        $html.="</tr>";

        //Fin parte de actividades
        
        //$html.="<tr><td colspan='6' class='a100 h200'></td></tr>";
    }

    $res=array(
        'html'=>$html,
        'rm'=>$rm,
        'baseImponible'=>$baseImponible,
        'importeIva'=>$importeIva,
        'baseExenta'=>$baseExenta,
        'total'=>$total
    );

    return $res;
}

function obtieneVencimientosFactura($codigoFactura,$cuentaCliente,$cuentaEmisor,$formaPago,$contrato){
    $formaPago=datosRegistro('formas_pago',$formaPago);
    $res="Forma de pago: ".$formaPago['forma'].defineFormaPago($cuentaCliente,$cuentaEmisor,$formaPago['codigo']);
    $res="Forma de pago: ";

    $consulta=consultaBD("SELECT fechaVencimiento, SUM(importe) AS importe, formas_pago.forma AS forma, formas_pago.codigo AS codigoForma, concepto FROM vencimientos_facturas LEFT JOIN formas_pago ON vencimientos_facturas.codigoFormaPago=formas_pago.codigo WHERE codigoFactura=$codigoFactura GROUP BY fechaVencimiento, forma ORDER BY fechaVencimiento;");
    $i=1;
    
    if(mysql_num_rows($consulta)>0){
        $res.='<br/>';

        while($datos=mysql_fetch_assoc($consulta)){
            $res.="&nbsp;Vencimiento ".$i.": ".compruebaImporteContrato($datos['importe'],$contrato)." € el ".formateaFechaWeb($datos['fechaVencimiento']);
            if($datos['forma']!=$formaPago){
                $res.=' - '.$datos['forma'].defineFormaPago($cuentaCliente,$cuentaEmisor,$datos['codigoForma']);
            }

            $res.='<br />';

            $i++;
        }
    }
    if($i==1){
        $contratoEnFactura=datosRegistro('contratos_en_facturas',$codigoFactura,'codigoFactura');
        $factura=consultaBD('SELECT * FROM contratos_en_facturas WHERE codigoContrato='.$contratoEnFactura['codigoContrato'].' AND codigoFactura!='.$codigoFactura,true,true);
        if($factura){
            $consulta=consultaBD("SELECT fechaVencimiento, SUM(importe) AS importe, formas_pago.forma AS forma, formas_pago.codigo AS codigoForma, concepto FROM vencimientos_facturas LEFT JOIN formas_pago ON vencimientos_facturas.codigoFormaPago=formas_pago.codigo WHERE codigoFactura=".$factura['codigoFactura']." GROUP BY fechaVencimiento, forma ORDER BY fechaVencimiento;",true);
            if(mysql_num_rows($consulta)>0){
                $res.='<br/>';

                while($datos=mysql_fetch_assoc($consulta)){
                    $res.="&nbsp;Vencimiento ".$i.": ".compruebaImporteContrato($datos['importe'],$contrato)." € el ".formateaFechaWeb($datos['fechaVencimiento']);
                    if($datos['forma']!=$formaPago){
                        $res.=' - '.$datos['forma'].defineFormaPago($cuentaCliente,$cuentaEmisor,$datos['codigoForma']);
                    }

                    $res.='<br />';

                    $i++;
                }
            }
        }
    }

    return $res;
}

function datosRegistro($tabla,$codigo,$campo='codigo'){
	//echo "SELECT * FROM $tabla WHERE $campo='$codigo';<br/>";
	return consultaBD("SELECT * FROM $tabla WHERE $campo='$codigo';",true,true);
}

function defineFormaPago($cuentaCliente,$cuentaEmisor,$formaPago){
    $res='';
    $codigos=array(1,2,4,8);
    if(in_array($formaPago, $codigos)){
        $res=' IBAN: ';
        if(in_array($formaPago, array(2,8))){
             if($cuentaCliente==''){
                $res.='No registrado';
            } else {
                $dato=substr($cuentaCliente, 20);
                $res.='**** **** **** **** **** '.$dato;
            }
        } else {
            if($cuentaEmisor==''){
                $res.='No registrado';
            } else {
                $res.=$cuentaEmisor;
            }
        }
    }
    return $res;
}

function formateaObservacionesFactura($observaciones){
    $res=str_replace('<div>','',$observaciones);
    $res=str_replace('</div>','',$res);
    $res=nl2br($res);
    $res=preg_replace("/(^|\t|\s+)\<br \/\>/",'',$res);

    return $res;
}

function compruebaImporteContrato($importe,$datos){
    
    if($datos['incrementoIPC']=='SI'){
        $ipc=$datos['incrementoIpcAplicado'];

        $importe=$importe*(1+$ipc/100);
    }

    $res=formateaNumeroWeb($importe);

    return $res;
}

function formateaNumeroWeb($dato,$bd=false){//MODIFICACIÓN 17/02/2015: añadida comprobación de $dato y de parts[1] en e else.
	$res=$dato;
	if($dato!='' && $bd){
		$dato=str_replace('.','',$dato);//MODIFICACIÓN 27/06/2016: eliminados los puntos de millares, en caso de que existan

		$parts=explode(',', $dato);

		if(isset($parts[1])){
			$dato = $parts[0].".".$parts[1];
		}

		$res = round($dato,2);
	}
	elseif($dato=='' && $bd){//MODIFICACIÓN 27/06/2016: modificada la estructura condicional para que solo devuelva 0 en caso de que se pase una cadena vacía para la BDD
		$res=0;
	}
	elseif($dato!=''){
		$dato=round($dato,2);
		$parts=explode('.', $dato);
		$parts[0]=number_format($parts[0],0,',','.');//MODIFICACIÓN 27/06/2016: añadidos separadores de miles a los números destinados a mostrar por pantalla.

		if(isset($parts[1]) && strlen($parts[1])>1){//MODIFICACIÓN 18/02/2015: añadida comprobación de que los decimales no son 0, y si lo son no los concatena
			$dato=$parts[0].",".$parts[1];
		}
		elseif(isset($parts[1]) && strlen($parts[1])==1){
			$dato=$parts[0].",".$parts[1].'0';
		}
		else{
			$dato=$parts[0].',00';
		}
		$res=$dato;
	}

	return $res;
}


?>