<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de empleados

function operacionesTipos(){
	$res=true;

	if(isset($_POST['codigo'])){
    	$res=actualizaDatos('riesgos_tipos');
  	}
  	elseif(isset($_POST['nombre'])){
    	$res=insertaDatos('riesgos_tipos');
  	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('riesgos_tipos');
	}

	mensajeResultado('nombre',$res,'Tipo');
    mensajeResultado('elimina',$res,'Tipo', true);
}

function imprimeTipos(){
	global $_CONFIG;

	$codigoS=$_SESSION['codigoU'];
	$consulta=consultaBD("SELECT * FROM riesgos_tipos ORDER BY nombre;", true);

	while($datos=mysql_fetch_assoc($consulta)){
		echo "
		<tr>
			<td> ".$datos['nombre']." </td>
        	<td class='td-actions'>
        		<a href='".$_CONFIG['raiz']."riesgos_tipos/gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
        	</td>
        	<td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
    	</tr>";
	}
}

function gestionTipos(){
	operacionesTipos();

	abreVentanaGestionConBotones('Gestión de tipos de riesgos','index.php');
	$datos=compruebaDatos('riesgos_tipos');
	campoTexto('nombre','Nombre',$datos,'span4');

	cierraVentanaGestion('index.php');
}



//Fin parte de empleados


