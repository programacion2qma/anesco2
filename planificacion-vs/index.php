<?php
  $seccionActiva=1;
  include_once('../cabecera.php');
  
  operacionesPlanificacion();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    
      
		

      
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-paste"></i>
            <h3>Planificación / Control de email</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable tablaConFicheros tablaTextoPeque">
              <thead>
                <tr>
                  <th> Contrato </th>
                  <th> Nº Cliente </th>
                  <th> Empresa </th>
                  <th> E.R. </th>
                  <th> Inf. </th>
                  <th> Planif. </th>
                  <th> Cuestionarios salud </th>
                  <th> Protocolización </th>
                  <th> Progr. </th>
                  <th> Citas </th>
                  <th> Email presentación </th>
                  <th> Email presentación técnico</th>
                  <th> Email técnico 2 (sin respuesta el 1)</th>
                  <th> Email informe E.R.</th>
                  <th> Email recordatorio V.S.</th>
                  <th> Email renovación contrato </th>
                  <th> Email memoria y cuestionario </th>
                  <th> Email renovación técnico </th>
                </tr>
              </thead>
              <tbody>

    				<?php
    					imprimePlanificacionVS();
    				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


    
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
    $(":file").filestyle({input: false, iconName: "icon-cloud-upload", buttonText: "Subir", buttonName: "btn-success btn-small"});

    $(document).on('change','input',function(){
        $(this).closest('form').submit();
    });
});
</script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>