<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión Planificación VS

function imprimePlanificacionVS(){
	global $_CONFIG;

	conexionBD();
	$consulta=consultaBD("SELECT contratos.codigo, EMPCP, EMPID, contratos.fechaInicio, contratos.fechaFin, contratos.codigoInterno, EMPNOMBRE, EMPMARCA, opcion, ofertas.codigoCliente, ficheroER, clientes.codigo AS codigoCliente
						  FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
						  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
						  WHERE contratos.eliminado='NO' AND opcion IN(1,2) AND contratos.enVigor='SI';");

	while($datos=mysql_fetch_assoc($consulta)){
		$contrato=formateaReferenciaContrato($datos,$datos);
		$evaluacionRiesgos=obtieneEvaluacionRiesgosContrato($datos);
		$informeVS=obtieneEstadoEnvioDocumentoVSContrato($datos['codigo'],'1');
		$planificacion=obtienePlanificacionContrato($datos['codigo']);
		$cita=obtieneUltimaCitaContrato($datos['codigo']);
		$correos=obtieneCorreosEnviados($datos['codigo']);

		echo "
			<tr>
				<td>".$contrato."</td>
				<td><a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."' class='noAjax' target='_blank'>".$datos['EMPID']."</a></td>
				<td><a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."' class='noAjax' target='_blank'>".$datos['EMPNOMBRE']." (".$datos['EMPMARCA'].")</a></td>
				<td class='centro'>".$evaluacionRiesgos."</td>
				<td>".$informeVS."</td>
				<td>".$planificacion."</td>
				<td> - </td>
				<td> - </td>
				<td> - </td>
				<td> ".$cita." </td>
				<td class='centro'>".$correos['VS0']."</td>
				<td class='centro'>".$correos['VS1']."</td>
				<td class='centro'>".$correos['VS2']."</td>
				<td class='centro'>".$correos['VS3']."</td>
				<td class='centro'>".$correos['VS4']."</td>
				<td class='centro'>".$correos['VS5']."</td>
				<td class='centro'>".$correos['VS6']."</td>
				<td class='centro'>".$correos['VS7']."</td>
			</tr>";
	}
	cierraBD();
}


function obtieneCorreosEnviados($codigoContrato){
	$res=array();

	for($i=0;$i<8;$i++){
		$res['VS'.$i]="<a class='btn btn-propio btn-small' href='../correos/gestion.php?tipo=VS".$i."&codigoContrato=".$codigoContrato."'><i class='icon-send'></i> Enviar</a>";
	}

	$consulta=consultaBD("SELECT codigo, fecha, tipo FROM correos WHERE codigoContrato='$codigoContrato';");
	while($datos=mysql_fetch_assoc($consulta)){
		//Sobreescribo los botones ya creados con los mensajes enviados.
		$res[$datos['tipo']]="<a href='../correos/gestion.php?codigo=".$datos['codigo']."' class='label label-success'>".formateaFechaWeb($datos['fecha'])."</a>";
	}

	return $res;
}

function obtieneEvaluacionRiesgosContrato($datos){
	$codigoContrato=$datos['codigo'];
	$codigoCliente=$datos['codigoCliente'];
	$fechaInicio=$datos['fechaInicio'];
	$fechaFin=$datos['fechaFin'];
	$ficheroER=$datos['ficheroER'];//Si el contrato es solo de VS (opción 2), el Informe de Evaluación de Riesgos se sube al campo ficheroER de la tabla contratos (a través de este mismo listado)

	if($datos['opcion']==1){
		$consulta=consultaBD("SELECT codigo FROM evaluacion_general WHERE codigoCliente='$codigoCliente' AND fechaEvaluacion>='$fechaInicio' AND fechaEvaluacion<='$fechaFin';");
		if(mysql_num_rows($consulta)>0){
			$res="<a href='../evaluacion-de-riesgos/?codigoCliente=$codigoCliente' target='_blank' class='noAjax'>Si</a>";
		}
		else{
			$res='No';
		}
	}
	elseif($ficheroER!='' && $ficheroER!='NO'){
		$res="<a href='../documentos/contratos/$ficheroER' class='noAjax' target='_blank'>Si</a>";
	}
	else{
		$res="<a href='index.php?solicitarER=$codigoContrato' class='noAjax btn btn-small btn-propio'><i class='icon-envelope'></i> Solicitar</a><br />";
		$res.="<form action='?' method='post' enctype='multipart/form-data' class='noAjax'>";
		$res.="<input type='hidden' class='hide' name='codigoContrato' value='$codigoContrato' />";
		$res.="<input type='file' name='ficheroER' />";
		$res.="</form>";
	}

	return $res;
}

function obtieneEstadoEnvioDocumentoVSContrato($codigoContrato,$tipoDocumento){
	$res='No';

	$consulta=consultaBD("SELECT enviado FROM documentosVS WHERE tipoDocumento='$tipoDocumento' AND codigoContrato='$codigoContrato';");
	if(mysql_num_rows($consulta)>0){
		$datos=mysql_fetch_assoc($consulta);

		if($datos['enviado']=='NO'){
			$res='Sin enviar';
		}
		else{
			$res='Enviado';
		}
	}

	return $res;
}

function obtienePlanificacionContrato($codigoContrato){
	$res='No';

	$consulta=consultaBD("SELECT codigo FROM formularioPRL WHERE codigoContrato='$codigoContrato';");
	if(mysql_num_rows($consulta)>0){
		$res='Si';
	}

	return $res;
}

function obtieneUltimaCitaContrato($codigoContrato){
	$res='No';

	$consulta=consultaBD("SELECT fechaInicio FROM citas WHERE codigoContrato='$codigoContrato' AND fechaInicio>=CURDATE() ORDER BY fechaInicio");
	if(mysql_num_rows($consulta)>0){
		$datos=mysql_fetch_assoc($consulta);
		$res=formateaFechaWeb($datos['fechaInicio']);
	}
	else{
		$consulta=consultaBD("SELECT fechaInicio FROM citas WHERE codigoContrato='$codigoContrato' ORDER BY fechaInicio DESC");
		if(mysql_num_rows($consulta)>0){
			$datos=mysql_fetch_assoc($consulta);
			$res=formateaFechaWeb($datos['fechaInicio']);
		}
	}

	return $res;
}

function operacionesPlanificacion(){
	$res=true;

	if(isset($_GET['solicitarER'])){
		$res=enviaMensajeSolicitudER($_GET['solicitarER']);
	}
	elseif(isset($_POST['codigoContrato'])){
		$res=subeFicheroER($_POST['codigoContrato']);
	}

	mensajeResultado('resultado',$res,'E.R.');
}

function enviaMensajeSolicitudER($codigoContrato){
	$_POST['resultado']=true;//Para que entre en el mensajeResultado

	$datos=consultaBD("SELECT EMPCP, EMPID, contratos.fechaInicio, contratos.codigoInterno, EMPNOMBRE, EMPEMAILPRINC
					   FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
					   INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
					   WHERE contratos.codigo='$codigoContrato';",true,true);

	$referenciaContrato=formateaReferenciaContrato($datos,$datos);
	
	$mensaje="
	<i>Estimado ".$datos['EMPNOMBRE'].":</i><br /><br />
	Desde ANESCO SERVICIO DE PREVENCIÓN nos ponemos en contacto con usted para solicitar su informe de Evaluación de Riesgos correspondiente al contrato $referenciaContrato.
	<br />
	Puede hacérnoslo llegar respondiendo a este email con el documento adjunto.
	<br /><br />
	Agradeciendo de antemano su atención, reciba un cordial saludo.<br /><br />
    <img src='https://crmparapymes.com.es/anesco2/img/logo.png' /><br />
    <div style='color:#075581;font-weight:bold'>
         <i>ANESCO SERVICIO DE PREVENCION</i><br />
         Tlf .954.10.92.93<br />
         info@anescoprl.es · www.anescoprl.es<br />
         C/ Murillo 1, 2ª P. 41001 - Sevilla.
     </div>";

	return enviaEmail($datos['EMPEMAILPRINC'],'Solicitud de Informe - ANESCO',$mensaje);
}

function subeFicheroER($codigoContrato){
	$_POST['resultado']=true;//Para que entre en el mensajeResultado

	$fichero=subeDocumento('ficheroER',time(),'../documentos/contratos/');

	if($fichero=='NO'){
		$res=false;
	}
	else{
		$res=consultaBD("UPDATE contratos SET ficheroER='$fichero' WHERE codigo='$codigoContrato';",true);
	}

	return $res;
}

//Fin parte de gestión Planificación VS