<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de partes diarios


function operacionesParteDiario(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('parte_diario');
		$res= $res && insertaActividad($_POST['codigo'],true);
		//$res= $res && insertaGastos($_POST['codigo'],true);		
	}
	elseif(isset($_POST['fecha'])){
		$res=insertaDatos('parte_diario');
		$codigoParte=$res;
		$res= $res && insertaActividad($codigoParte);
		//$res= $res && insertaGastos($codigoParte);
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('parte_diario');
	}

	mensajeResultado('fecha',$res,'Parte Diario');
    mensajeResultado('elimina',$res,'Parte Diario', true);
}

function insertaActividad($codigoParte,$actualizacion=false){
	$res=true;

	/*if($actualizacion){
		$res=consultaBD("DELETE FROM actividad_parte_diario WHERE codigoParte='$codigoParte';");
	}

	$datos=arrayFormulario();

	for($i=0;isset($datos['codigoContrato'.$i]);$i++){
		$codigoContrato=$datos['codigoContrato'.$i];
		$actividad=$datos['actividad'.$i];
		$minutos=$datos['minutos'.$i];
		$observaciones=$datos['observaciones'.$i];		

		$res=$res && consultaBD("INSERT INTO actividad_parte_diario VALUES(NULL,'$codigoContrato','$actividad','$minutos','$observaciones','$codigoParte');");
	}*/

	if($actualizacion){
		$res=consultaBD("DELETE FROM tareas_parte_diario WHERE codigoParteDiario='$codigoParte';");
	}

	$datos=arrayFormulario();

	for($i=0;isset($datos['codigoContrato'.$i]);$i++){
		$codigoContrato=$datos['codigoContrato'.$i];
		$codigoTarea=$datos['codigoTarea'.$i];
		$tiempo=$datos['tiempo'.$i];

		$res=$res && consultaBD("INSERT INTO tareas_parte_diario VALUES(NULL,$codigoContrato,$codigoTarea,'$codigoParte','$tiempo');");
	}

	return $res;
}

function insertaGastos($codigoParte,$actualizacion=false){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM gastos_parte_diario WHERE codigoParte='$codigoParte';");
	}

	$datos=arrayFormulario();

	for($i=0;isset($datos['codigoContratoGastos'.$i]);$i++){
		$codigoContrato=$datos['codigoContratoGastos'.$i];
		$itinerario=$datos['itinerario'.$i];
		$kilometraje=$datos['kilometraje'.$i];
		$importe=$datos['importe'.$i];
		$peaje=$datos['peaje'.$i];
		$taxi=$datos['taxi'.$i];
		$parking=$datos['parking'.$i];
		$dietas=$datos['dietas'.$i];
		$otros=$datos['otros'.$i];
		$total=$datos['total'.$i];
		$observaciones=$datos['observacionesGastos'.$i];
		$estado=$datos['estado'.$i];		

		$res=$res && consultaBD("INSERT INTO gastos_parte_diario VALUES(NULL,'$codigoContrato','$itinerario','$kilometraje','$importe','$peaje','$taxi','$parking','$dietas','$otros','$total','$observaciones','$estado','$codigoParte');");
	}

	return $res;
}

function gestionParteDiario(){
	operacionesParteDiario();
	
	abreVentanaGestionConBotones('Gestión de Parte Diario','index.php','','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('parte_diario');
	
	campoFecha('fecha','Fecha',$datos);

	cierraColumnaCampos();
	abreColumnaCampos('span3');

	campoOculto($datos,'codigoUsuario',$_SESSION['codigoU']);
	cierraColumnaCampos();
	abreColumnaCampos('span10');

	//creaTablaActividad($datos);
	creaTablaTareasParte($datos);

	//abreColumnaCampos('span3 spanGasto');
	//echo "<div id='gastosActividad' style='display: none;'>";
	//creaTablaGastos($datos);
	//echo "</div>";
	//cierraColumnaCampos();
	campoRadio('gastos','¿Incluir Gastos?',$datos,'NO');

	echo "<div id='gastosActividad' style='display: none;'>";
	creaTablaGastos($datos);
	echo "</div>";

	cierraVentanaGestion('index.php',true);
}


function imprimeParteDiario(){
	global $_CONFIG;

	//$where=defineWhereEjercicio($where,array('asistencia_a_inspecciones.fecha'));
	$opciones=array('','SPA 4 Especialidades','SPA Vigilancia de la salud','SPA Especialides técnicas','Otras actuaciones',''=>'');
	$consulta=consultaBD("SELECT parte_diario.codigo, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS tecnico,
						  parte_diario.fecha
						  FROM parte_diario 
						  LEFT JOIN usuarios ON parte_diario.codigoUsuario=usuarios.codigo");

	while($datos=mysql_fetch_assoc($consulta)){
		$tareas=consultaBD('SELECT codigoTarea
		FROM tareas_parte_diario 
		INNER JOIN tareas ON tareas_parte_diario.codigo=tareas.codigo
		WHERE codigoParteDiario='.$datos['codigo'],true);
		$actividad='';
		$contratos=array();
		while($tarea=mysql_fetch_assoc($tareas)){
			if($tarea['codigoTarea']!=NULL){
				$contrato=consultaBD('SELECT contratos.* FROM tareas INNER JOIN contratos ON tareas.codigoContrato=contratos.codigo WHERE tareas.codigo='.$tarea['codigoTarea'],true,true);
				if($contrato){
					if(!in_array($contrato['codigo'], $contratos)){
						$cliente=consultaBD('SELECT clientes.*, ofertas.opcion FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='.$contrato['codigo'],true,true);
						$referencia=formateaReferenciaContrato($cliente,$contrato);
						$actividad.='Nº: '.$referencia.' - '.$cliente['EMPNOMBRE'].' - '.$opciones[$cliente['opcion']].'<br/>';
						array_push($contratos, $contrato['codigo']);
					}
				}
			}
		}

		echo "
			<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$actividad."</td>
				<td>".$datos['tecnico']."</td>
				<td class='centro'>
					<a href='".$_CONFIG['raiz']."parte-diario/gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a> 
				</td>
				<td><input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'></td>

			</tr>";
	}

}


function estadisticasParteDiario(){
	$res=consultaBD('SELECT COUNT(parte_diario.codigo) AS total FROM parte_diario LEFT JOIN usuarios ON parte_diario.codigoUsuario=usuarios.codigo',true,true);

	return $res;
}

function creaTablaActividad($datos){
    echo "
    <h3 class='apartadoFormulario'>Actividad</h3>
    <div class='centro'>
        <table class='table table-striped tabla-simple mitadAncho' id='tablaActividad'>
          <thead>
            <tr>
              <th> Contrato </th>
              <th> Actividad </th>
              <th> Minutos </th>
              <th> Observaciones </th>
              <th> </th>
            </tr>
          </thead>
          <tbody>";
        
            $i=0;

            if($datos){
                $consulta=consultaBD("SELECT * FROM actividad_parte_diario WHERE codigoParte='".$datos['codigo']."'");

                while($actividad=mysql_fetch_assoc($consulta)){
                    imprimeLineaTablaActividad($i,$actividad);
                    $i++;
                }
            }
            
            if($i==0){
                imprimeLineaTablaActividad(0,false);
            }
      
    echo "</tbody>
        </table>
        <br />
        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaActividad\");'><i class='icon-plus'></i> Añadir</button> 
        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaActividad\");'><i class='icon-trash'></i> Eliminar</button>
    </div>
    <br />";

}

function imprimeLineaTablaActividad($i,$datos){
    $j=$i+1;

    echo "<tr>";
    		campoSelectContrato($i,$datos,$_SESSION['codigoU']);
	        areaTextoTabla('actividad'.$i,$datos['actividad']);
			campoTextoTabla('minutos'.$i,$datos['minutos'],'input-mini');
	        areaTextoTabla('observaciones'.$i,$datos['observaciones']);
    echo "	<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    	  </tr>";
}

function campoSelectContrato($i,$datos=false,$codigoTecnico){
	$ejercicio=obtieneEjercicioParaWhere();
	$opciones=array('','SPA 4 Especialidades','SPA Vigilancia de la salud','SPA Especialides técnicas','Otras actuaciones',''=>'');

	conexionBD();

	if($_SESSION['tipoUsuario']=='TECNICO'){
		$contratos=consultaBD("SELECT * FROM contratos WHERE eliminado='NO' AND tecnico='".$codigoTecnico."' AND (YEAR(fechaInicio)='$ejercicio' OR YEAR(fechaFin)='$ejercicio');");
	}
	else{
		$contratos=consultaBD("SELECT * FROM contratos WHERE eliminado='NO' AND (YEAR(fechaInicio)='$ejercicio' OR YEAR(fechaFin)='$ejercicio');");		
	}
	$contratos=consultaBD("SELECT * FROM contratos WHERE eliminado='NO'");	
	
	$nombres=array('');
	$valores=array('NULL');
	while($contrato=mysql_fetch_assoc($contratos)){
		$cliente=consultaBD('SELECT clientes.*, ofertas.opcion AS opcion FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo='.$contrato['codigoOferta']." AND ofertas.eliminado='NO'",false,true);
		$referencia=formateaReferenciaContrato($cliente,$contrato);
		array_push($nombres, 'Nº: '.$referencia.' - '.$cliente['EMPNOMBRE'].' - '.$opciones[$cliente['opcion']]);
		array_push($valores, $contrato['codigo']);
	}

	cierraBD();

	campoSelect('codigoContrato'.$i,'',$nombres,$valores,$datos['codigoContrato'],'selectpicker span7 show-tick codigoContrato',"data-live-search='true'",1);
}

function campoSelectContratoGastos($i,$datos=false,$codigoTecnico){
	if($_SESSION['tipoUsuario']=='TECNICO'){
		$contratos=consultaBD("SELECT * FROM contratos WHERE eliminado='NO' AND tecnico='".$codigoTecnico."'",true);
	}else{
		$contratos=consultaBD('SELECT * FROM contratos WHERE eliminado="NO"',true);		
	}
	$nombres=array('');
	$valores=array('NULL');
	while($contrato=mysql_fetch_assoc($contratos)){
		$cliente=consultaBD('SELECT clientes.* FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo='.$contrato['codigoOferta']." AND ofertas.eliminado='NO'",true,true);
		$referencia=formateaReferenciaContrato($cliente,$contrato);
		array_push($nombres, 'Nº: '.$referencia.' - '.$cliente['EMPNOMBRE']);
		array_push($valores, $contrato['codigo']);
	}
	campoSelect('codigoContratoGastos'.$i,'',$nombres,$valores,$datos['codigoContratoGastos'],'selectpicker span3 show-tick',"data-live-search='true'",1);
}

function creaTablaGastos($datos){
    echo "
    <h3 class='apartadoFormulario'>Gastos</h3>
    Al modificar los contratos del parte, los gastos serán modificados una vez actualizado el parte.
    <div class='centro'>
        <table class='table table-striped tabla-simple mitadAncho' id='tablaGastos'>
          <thead>
            <tr>
           	  <th> Cliente </th>
              <th> Contrato </th>
              <th> Itinerario </th>
              <th> Kilometraje </th>
              <th> Importe Km </th>
              <th> Peaje </th>
              <th> Táxi/Autobús </th>
              <th> Parking </th>
              <th> Dietas </th>
              <th> Otros </th>
              <th> Total </th>
              <th> Observaciones </th>
              <th> Estado </th>                          
            </tr>
          </thead>
          <tbody>";
        
            $i=0;
            if($datos){
            	$consulta=consultaBD("SELECT * FROM tareas_parte_diario WHERE codigoParteDiario='".$datos['codigo']."'",true);
            	while($tarea=mysql_fetch_assoc($consulta)){
            		$gastos=consultaBD('SELECT * FROM gastos_tarea WHERE codigoTarea='.$tarea['codigoTarea'],true);
            		while($gasto=mysql_fetch_assoc($gastos)){
            			imprimeLineaTablaGasto2($i,$gasto);
            		}
            	}
            }
            if($i==0){

            }
            /*if($datos){
                $consulta=consultaBD("SELECT * FROM gastos_parte_diario WHERE codigoParte='".$datos['codigo']."'");

                while($gasto=mysql_fetch_assoc($consulta)){
                    imprimeLineaTablaGasto($i,$gasto);
                    $i++;
                }
            }
            
            if($i==0){
                imprimeLineaTablaGasto(0,false);
            }*/
      
    echo "</tbody>
        </table>
    </div>
    <br />";
}

function imprimeLineaTablaGasto($i,$datos){
    $j=$i+1;

    echo "<tr>";
			campoSelectContratoGastos($i,$datos,$_SESSION['codigoU']);
			campoTextoTabla('itinerario'.$i,$datos['itinerario'],'input-mini');
			campoTextoTabla('kilometraje'.$i,$datos['kilometraje'],'input-mini kilometraje');
			campoTextoTabla('importe'.$i,$datos['importe'],'input-mini');
			campoTextoTabla('peaje'.$i,$datos['peaje'],'input-mini');
			campoTextoTabla('taxi'.$i,$datos['taxi'],'input-mini');
			campoTextoTabla('parking'.$i,$datos['parking'],'input-mini');
			campoTextoTabla('dietas'.$i,$datos['dietas'],'input-mini');
			campoTextoTabla('otros'.$i,$datos['otros'],'input-mini');
			campoTextoTabla('total'.$i,$datos['total'],'input-mini');
			campoTextoTabla('observacionesGastos'.$i,$datos['observacionesGastos'],'input-mini');
			campoSelect('estado'.$i,'',array('PENDIENTE','PAGADO'),array('Pendiente','Pagado'),$datos['estado'],'selectpicker span2 show-tick',"data-live-search='true'",1);
    echo "	<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    	  </tr>";
}

function imprimeLineaTablaGasto2($i,$datos){
	$contrato=datosRegistro('contratos',$datos['codigoContratoGastos']);
	$oferta=datosRegistro('ofertas',$contrato['codigoOferta']);
	$cliente=datosRegistro('clientes',$oferta['codigoCliente']);
	echo "<tr>";
	echo "<td>".$cliente['EMPNOMBRE']."</td>";
	echo "<td>".formateaReferenciaContrato($cliente,$contrato)."</td>";
	echo "<td>".$datos['itinerario']."</td>";
	echo "<td>".$datos['kilometraje']."</td>";
	echo "<td>".$datos['importe']."</td>";
	echo "<td>".$datos['peaje']."</td>";
	echo "<td>".$datos['taxi']."</td>";
	echo "<td>".$datos['parking']."</td>";
	echo "<td>".$datos['dietas']."</td>";
	echo "<td>".$datos['otros']."</td>";
	echo "<td>".$datos['total']."</td>";
	echo "<td>".$datos['observacionesGastos']."</td>";
	echo "<td>".$datos['estado']."</td>";
	echo "</tr>";

}

function imprimeGastos($where=''){
	
	$consulta=consultaBD("SELECT tareas.codigo, tareas.fechaFin, gastos_tarea.codigoContratoGastos, gastos_tarea.itinerario, gastos_tarea.kilometraje, gastos_tarea.importe, gastos_tarea.peaje, gastos_tarea.taxi, gastos_tarea.parking, gastos_tarea.dietas, gastos_tarea.otros, gastos_tarea.total, gastos_tarea.estado
	FROM tareas INNER JOIN gastos_tarea ON tareas.codigo=gastos_tarea.codigoTarea $where;",true);	
	

	while($datos=mysql_fetch_assoc($consulta)){

		$datosContratos=datosRegistro('contratos',$datos['codigoContratoGastos']);
		$datosOfertas=datosRegistro('ofertas',$datosContratos['codigoOferta']);
		$datosClientes=datosRegistro('clientes',$datosOfertas['codigoCliente']);		

		echo "<tr>
			    <td> ".formateaFechaWeb($datos['fechaFin'])." </td>
			    <td> ".formateaReferenciaContrato($datosClientes,$datosContratos)." - ".$datosClientes['EMPNOMBRE']." </td>			    
				<td> ".$datos['itinerario']." </td>
				<td> ".$datos['kilometraje']." €</td>
				<td> ".$datos['importe']." €</td>
				<td> ".$datos['peaje']." €</td>
				<td> ".$datos['taxi']." €</td>
				<td> ".$datos['parking']." €</td>
				<td> ".$datos['dietas']." €</td>
				<td> ".$datos['otros']." €</td>
				<td> ".$datos['total']." €</td>
				<td> ".$datos['estado']." </td>				
			 </tr>";
	}
}


function creaTablaTareasParte($datos){
    echo "
    <h3 class='apartadoFormulario'>Actividad</h3>
    <div class='centro'>
        <table class='table table-striped tabla-simple' id='tablaActividad'>
          <thead>
            <tr>
              <th> Contrato </th>
              <th> Tarea </th>
              <th> Tiempo </th>
              <th> </th>
            </tr>
          </thead>
          <tbody>";
        
            $i=0;

            if($datos){
                $consulta=consultaBD("SELECT * FROM tareas_parte_diario WHERE codigoParteDiario='".$datos['codigo']."'");

                while($actividad=mysql_fetch_assoc($consulta)){
                    imprimeLineaTablaTareasParte($i,$actividad);
                    $i++;
                }
            }
            
            if($i==0){
                imprimeLineaTablaTareasParte(0,false);
            }
      
    echo "</tbody>
        </table>
        <br />
        <button type='button' class='btn btn-small btn-success' onclick='insertaActividad(\"tablaActividad\");'><i class='icon-plus'></i> Añadir</button> 
        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaActividad\");'><i class='icon-trash'></i> Eliminar</button>
    </div>
    <br />";

}

function imprimeLineaTablaTareasParte($i,$datos){
    $j=$i+1;

    echo "<tr>";
    		campoSelectContrato($i,$datos,$_SESSION['codigoU']);
	        campoSelectTareaContrato($i,$datos);
	        campoTextoTabla('tiempo'.$i,$datos['tiempo'],'input-mini campoTiempo');
    echo "	<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    	  </tr>";
}

function campoSelectTareaContrato($i,$datos){
	if($datos){
		$tareas=obtieneTareasContrato($datos['codigoContrato']);
		campoSelect('codigoTarea'.$i,'',$tareas['nombres'],$tareas['valores'],$datos['codigoTarea'],'selectpicker span6 show-tick',"data-live-search='true'",1);
	}
	else{
		campoSelect('codigoTarea'.$i,'',array(''),array('NULL'),false,'selectpicker span6 show-tick',"data-live-search='true'",1);	
	}
}


function obtieneTareasContrato($codigoContrato){
	$res=array('nombres'=>array(''),'valores'=>array('NULL'));

	$consulta=consultaBD("SELECT * FROM tareas WHERE codigoContrato='$codigoContrato';",true);
	while($datos=mysql_fetch_assoc($consulta)){
		array_push($res['nombres'],$datos['tarea'].' - '.formateaFechaWeb($datos['fechaInicio']));
		array_push($res['valores'],$datos['codigo']);
	}

	return $res;
}

function obtieneTareasContratoAjax(){
	$datos=arrayFormulario();
	extract($datos);

	$tareas=obtieneTareasContrato($codigoContrato);

	for($i=0;$i<count($tareas['nombres']);$i++){
		$res.="<option value='".$tareas['valores'][$i]."'>".$tareas['nombres'][$i]."</option>";
	}

	echo $res;
}

//Fin parte de partes diarios