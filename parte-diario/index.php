<?php
  $seccionActiva=4;
  include_once('../cabecera.php');
  
  $where='';
  $res=operacionesParteDiario();
  //$where=defineWhereEjercicio($where,array('asistencia_a_inspecciones.fecha'));
  $estadisticas=estadisticasParteDiario();

  $tareas=consultaBD('SELECT * FROM tareas_parte_diario',true);
  /*while($tarea=mysql_fetch_assoc($tareas)){
    $tiempo='00:00';
    if($tarea['tiempo']>0){
      $horas=0;
      $tiempoEmpleado=$tarea['tiempo'];
      while($tiempoEmpleado>=60){
        $horas++;
        $tiempoEmpleado=$tiempoEmpleado=$tiempoEmpleado-60;
      }
      $min=$tiempoEmpleado;
      $horas=$horas<10?'0'.$horas:$horas;
      $min=$min<10?'0'.$min:$min;
      $tiempo=$horas.':'.$min;
    }
    echo $tarea['codigo'].' - '.$tarea['tiempo'].' = '.$tiempo.'<br/>';
    $res=consultaBD('UPDATE tareas_parte_diario SET tiempo="'.$tiempo.'" WHERE codigo='.$tarea['codigo'],true);
  }*/
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

         <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de Partes Diario:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-calendar"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Partes Diario registrado/s</div>
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Partes Diario</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="<?php echo $_CONFIG['raiz']; ?>parte-diario/gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nuevo Parte Diario</span> </a>
                <a href="<?php echo $_CONFIG['raiz']; ?>parte-diario/filtroGastos.php" class="shortcut"><i class="shortcut-icon icon-filter"></i><span class="shortcut-label">Filtrar Gastos</span> </a>                
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Partes Diario registrados </h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table id='tabla1' class="table table-striped table-bordered datatable tablaNo">
              <thead>
                <tr>
                  <th> Fecha </th>
                  <th> Actividad </th>
                  <th> Técnico </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeParteDiario();
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
    </div>
        
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/chart.js" type="text/javascript"></script>
<script type="text/javascript">    

    
</script>

</div><!-- Contenido! -->
<?php include_once('../pie.php'); ?>