<?php
  $seccionActiva=30;
  include_once('../cabecera.php');
  
  operacionesFacturas();
  $estadisticas=estadisticasGenericas('facturas',false,"tipoFactura='PROFORMA' AND eliminado='SI'");
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de facturación:</h6>
                   <div class="big_stats cf">
                     <div class="stat"> <i class="icon-trash"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Proforma/s eliminada/s</div>
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Facturas proforma</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                  <a href="index.php" class="shortcut noAjax"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label"> Volver</span> </a>
                  <a href="javascript:void(0);" id="reactivar" class="shortcut noAjax"><i class="shortcut-icon icon-check-circle"></i><span class="shortcut-label"> Restaurar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

        <div class="span12">
          <div class="widget widget-table action-table">
              <div class="widget-header"> <i class="icon-list"></i>
                <h3>Facturas proforma eliminadas</h3>
              </div>
              <!-- /widget-header -->
              <div class="widget-content">
                <table class="table table-striped table-bordered datatable tablaTextoPeque" id="tablaFacturas">
                  <thead>
                    <tr>
                      <th> Fecha </th>
                      <th> Serie </th>
                      <th> Cliente </th>
                      <th> CIF </th>
                      <th> Base Imp. </th>
                      <th> Total </th>
                      <th> Concepto </th>
                      <th> Cobrada </th>
                      <th class="centro"></th>
                      <th class='centro'><input type='checkbox' id="todo"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      imprimeFacturas('SI');
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /widget-content-->
            </div>

        </div>
        
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/filtroTabla.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript">
$(document).ready(function(){
  $('a.enviaFactura').click(function(e){
        e.preventDefault();
        url=$(this).attr('href');
        var consulta=$.post(url);

        consulta.done(function(respuesta){
            $('#contenido').append(respuesta);

            //Parte de redibujo de tabla (para actualizar columna de envío)
            var datatable=$(tabla).dataTable();
            datatable.fnDraw();
            //Fin parte de redibujo de tabla (para actualizar columna de envío)

            setTimeout(function() {//Para hacer desaparecer la caja de resultado
                $(".errorLogin:not(:contains('Error'),:contains('Atención'))").fadeOut(3000);
            },3000);
        });
    });
});
</script>

<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>