<?php
  $seccionActiva=30;
  include_once('../cabecera.php');
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <form action='confirmacionVentasPendientes.php' method="post">
          <div class="span12">
            <div class="widget widget-table action-table">
                <div class="widget-header"> <i class="icon-list"></i>
                  <h3>Ventas pendientes de facturar</h3>
                  <div class="pull-right">
                    <a href='index.php' class='btn btn-small btn-default'><i class='icon-chevron-left'></i> Volver</a>
                    <button type='submit' class='btn btn-small btn-success'><i class='icon-chevron-right'></i> Continuar</a>
                  </div>
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                  <table class="table table-striped table-bordered datatable tablaTextoPeque" id="tablaVentas">
                    <thead>
                      <tr>
                        <th> Nº de contrato </th>
                        <th> Fecha de inicio </th>
                        <th> Fecha de fin </th>
                        <th> Última renovación </th>
                        <th> Cliente </th>
                        <th class='sumatorio'>Importe</th>
                        <th class='centro'><input type='checkbox' id='todo'></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                        imprimeContratosPendientes();
                      ?>
                    </tbody>
                  </table>
                </div>
                <!-- /widget-content-->
              </div>

          </div>
	  
      </form>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>
<script type="text/javascript" src="../../api/js/filtroTabla.js"></script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>