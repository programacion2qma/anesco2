<?php
    session_start();
    include_once("funciones.php");
    include_once('../../api/js/firma/signature-to-image.php');
    compruebaSesion();

    conexionBD();
    $contenido=generaPDFFacturaProforma($_GET['codigo']);
    cierraBD();
    
    require_once('../../api/html2pdf/html2pdf.class.php');
        $html2pdf=new HTML2PDF('P','A4','es',true,'UTF-8',array(20,10,10,15));
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->WriteHTML($contenido[1]);
    
    if(isset($_GET['envio'])){
        $html2pdf->Output($contenido[0].'.pdf','f');
        
        conexionBD();

        $factura=consultaBD("SELECT clientes.EMPEMAILPRINC AS emailCliente 
            FROM facturas
            INNER JOIN clientes ON facturas.codigoCliente=clientes.codigo 
            WHERE facturas.codigo=".$_GET['codigo'],false,true);
        
        $res=consultaBD("INSERT INTO facturas_envios VALUES(NULL,".$_GET['codigo'].",'".date('Y-m-d')."');");

        cierraBD();

        $mensaje="<i>Estimado cliente,</i><br /><br />
             Gracias por confiar en los servicios de <strong>ANESCO SERVICIO DE PREVENCIÓN</strong>. Adjunto remitimos la factura proforma correspondiente al inicio del contrato suscrito con nuestra entidad.<br /><br />
             Aprovechamos para recordarle que, una vez confirmemos el pago de la misma en virtud de la forma de pago indicada en el contrato, procederemos a confirmar el alta en nuestro sistema informático y enviarle sus claves de usuario, 
             desde las que podrá acceder a la información y documentos de su apreciada organización en tiempo real. <br /><br />
             Agradeciendo de antemano su atención, reciba un cordial saludo.<br /><br />
             <img src='https://crmparapymes.com.es/anesco2/img/logo.png' /><br />
             <div style='color:#075581;font-weight:bold'>
                 <i>ANESCO SERVICIO DE PREVENCION</i><br />
                 Tlf .954.10.92.93<br />
                 info@anescoprl.es · www.anescoprl.es<br />
                 C/ Murillo 1, 2ª P. 41001 - Sevilla.
             </div>";

        $adjunto=$contenido[0].'.pdf';
        
        enviaEmail($factura['emailCliente'],'Envío de factura proforma', $mensaje,$adjunto);
        
        unlink($adjunto);
    } 
    elseif(isset($_GET['descargaMasiva'])){
        $html2pdf->Output($contenido[0].'.pdf','D');
    }
    else {
        $html2pdf->Output($contenido[0].'.pdf');
    }
?>

 