<?php
  $seccionActiva=30;
  include_once("../cabecera.php");
  gestionFactura();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>

<script type="text/javascript" src="../../api/js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-wysihtml5.es-ES.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#observaciones').wysihtml5({locale: "es-ES"});

		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
		$('.selectpicker').selectpicker();

		compruebaFechaCobro();

		//Oyentes
		$('#codigoSerieFactura').change(function(){
			oyenteSerieFactura();
		});

		$('#codigoCliente').change(function(){
			oyenteCliente($(this).val());
		});


		$(document).on('change','#tablaConceptos select',function(){
			if(compruebaContratoRepetido($(this).val(),$(this))==1){
				oyenteBaseImponible();
			}
		});

		//Fin oyentes

		if($('#codigo').val()!=undefined){
			$('#tablaConceptos select').attr('readonly',true);
		}
	});

	//Parte general

	function oyenteSerieFactura(){
		var codigoSerieFactura=$('#codigoSerieFactura').val();
		if(codigoSerieFactura!='NULL'){
			var consulta=$.post('../listadoAjax.php?include=facturas&funcion=consultaNumeroSerieFactura();',{'codigoSerieFactura':codigoSerieFactura});
			consulta.done(function(respuesta){
				$('#numero').val(respuesta);
			});
		}
	}

	function oyenteCliente(codigoCliente){

		var esModeloAntiguo = $('#esModeloAntiguo').val();
		var consulta=$.post('../listadoAjax.php?include=facturas&funcion=obtieneContratosClienteAjax();',{'codigoCliente':codigoCliente,'esModeloAntiguo':esModeloAntiguo});	
		consulta.done(function(respuesta){
			$('#tablaConceptos select').html(respuesta).selectpicker('refresh');
		});
	}



	//Parte de cálculos

	function eliminaFilaFacturacion(id){
		eliminaFila(id);
		oyenteBaseImponible();
	}

	function oyenteBaseImponible(){
		var base=0;
		var total=0;

		$('#tablaConceptos').find('select option:selected').each(function(){
			var codigoContrato=$(this).val();
			if(codigoContrato!='NULL'){
				var importe=$(this).text();
				
				var subtotalRM=importe.substring(importe.indexOf("RRMM: ")+6,importe.indexOf(" €"));
				var subtotal=importe.substring(importe.indexOf("Actividades: ")+13,importe.indexOf(" €",importe.indexOf(" €")+2));
				var totalContrato=importe.substring(importe.indexOf("Total: ")+7).replace(" €","");
				
				base=parseFloat(base)+formateaNumeroCalculo(subtotalRM)+formateaNumeroCalculo(subtotal);
				total=parseFloat(total)+formateaNumeroCalculo(totalContrato);
			}
		});

		$('#baseImponible').val(formateaNumeroWeb(base));
		$('#total').val(formateaNumeroWeb(total));	
	}



	//Cuando se crea una factura desde Ventas Pendientes, el código existe en el formulario y por tanto el valor por defecto de la fecha de cobro
	function compruebaFechaCobro(){
		if($('#codigoFormaPagoCobro0').val()=='NULL'){
			$('#fechaCobro0').val('');
		}
	}


	function mostrarOcultos(tabla){
		$(tabla).find('.filaOculta').removeClass('hide');
	}


	function ocultarSeleccionados(tabla){
		$(tabla).find('input[name="filasTabla[]"]:checked').each(function(){
			var fila=$(this).parent().parent();
			fila.addClass('filaOculta');
			fila.addClass('hide');
			fila.find('.campoOculto').val('SI');
		});
	}

	function desOcultarSeleccionados(tabla){
		$(tabla).find('input[name="filasTabla[]"]:checked').each(function(){
			var fila=$(this).parent().parent();
			fila.removeClass('filaOculta');
			fila.removeClass('hide');
			fila.find('.campoOculto').val('NO');
		});
	}


	//La siguiente función sirve llama de forma normal al insertaFila, pero además se asegura que la nueva fila clonada e insertada no esté oculta (puede darse el caso que la fila clonada esté oculta)
	function insertaFilaTablaOcultos(tabla){
		insertaFila(tabla);
		$('#'+tabla).find('tr:last').removeClass('hide').removeClass('filaOculta');
	    $('#'+tabla).find('tr:last .campoOculto').val('NO');
	    $('#'+tabla).find('tr:last .tdEstado').val('3');
	    $('#'+tabla).find('tr:last .tdEstado').selectpicker('refresh');
	}


	function compruebaContratoRepetido(codigoContrato,select){
		var ocurrencias=0;

		$('#tablaConceptos select').each(function(){
			if($(this).val()==codigoContrato){
				ocurrencias++;
			}
		});

		if(ocurrencias>1){
			alert("Ha seleccionado el mismo contrato más de una vez. Por favor seleccione otro contrato.");
			select.selectpicker('val','NULL');
		}

		return ocurrencias;
	}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>