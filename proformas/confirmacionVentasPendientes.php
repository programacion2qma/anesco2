<?php
  $seccionActiva=30;
  include_once("../cabecera.php");
  ventasPendientesSeleccionadas();
?>

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>
<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../../api/js/funciones.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.selectpicker').selectpicker();

	//oyenteSeriePorDefecto();//Para seleccionar por defecto la primera serie de la facturación
	$('select.serie').change(function(){
		oyenteSerieFactura($(this));
	});

	//Eliminación de facturas creadas
    $('.form-actions .btn-default').unbind();
    $('.form-actions .btn-default').click(function(e){
        e.preventDefault();
        
        var facturas=[];
        for(i=0;$('#codigoFactura'+i).val()!=undefined;i++){
            facturas[i]=$('#codigoFactura'+i).val();
        }

        var eliminacion=$.post('../listadoAjax.php?include=facturas&funcion=eliminaFacturasVentasPendientes();',{'facturas':facturas});
        eliminacion.done(function(respuesta){
            window.location='index.php';
        });
    });
    //Fin eliminación facturas creadas
});

function oyenteSerieFactura(campo){
	var codigoSerieFactura=campo.val();
	if(codigoSerieFactura!='NULL'){
		var fila=obtieneFilaCampo(campo);

		var consulta=$.post('../listadoAjax.php?include=facturas&funcion=consultaNumeroSerieFactura();',{'codigoSerieFactura':codigoSerieFactura});
		consulta.done(function(respuesta){
			var anteriores=consultaNumerosSerieAnteriores(codigoSerieFactura,fila);
			var numero=parseInt(respuesta)+parseInt(anteriores);

			$('#numero'+fila).val(numero);
		});
	}
}

function consultaNumerosSerieAnteriores(codigoSerieFactura,fila){
	var res=0;

	$('#tablaVentasRegistradas').find('select.serie').each(function(){
		if(obtieneFilaCampo($(this))<fila && $(this).val()==codigoSerieFactura){
			res++;
		}
	});

	return res;
}


/*function oyenteSeriePorDefecto(){
	var fila=0;
	var codigosEmisores=[];
	$('select.emisor').each(function(){
		codigosEmisores[fila]=$(this).val();
		fila++;
	});



	var consulta=$.post('../listadoAjax.php?include=facturas&funcion=consultaSerieEmisor();',{'codigosEmisores':codigosEmisores},'json');
	consulta.done(function(respuesta){
		var datos=respuesta.split(',');

		for(i=0,j=0;i<datos.length;i+=2,j++){
			$('#codigoSerieFactura'+j).selectpicker('val',datos[i]);
			$('#numero'+j).val(datos[i+1]);
		}

	});
}*/
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>