<?php
  $seccionActiva=30;
  include_once('../cabecera.php');

  operacionesFacturas();
  $estadisticas=estadisticasFacturas($_POST['anio']);
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de facturas proforma:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-file-text"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Facturas proforma registradas</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Facturas proforma</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                  <a href="index.php" class="shortcut"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label">Volver</span> </a>
                  <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
                  <a href="filtrarHistorico.php" class="shortcut"><i class="shortcut-icon icon-calendar"></i><span class="shortcut-label">Cambiar año</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

        <div class="span12">
          <div class="widget widget-table action-table">
              <div class="widget-header"> <i class="icon-list"></i>
                <h3>Facturas proforma registradas</h3>
              </div>
              <!-- /widget-header -->
              <div class="widget-content">
                <table class="table table-striped table-bordered datatable tablaTextoPeque" id="tablaFacturas">
                  <thead>
                    <tr>
                      <th> Fecha </th>
                      <th> Serie </th>
                      <th> Emisor </th>
                      <th> Cliente </th>
                      <th> CIF </th>
                      <th> Base Imp. </th>
                      <th> Total </th>
                      <th> Cobrada </th>
                      <th class="centro"></th>
                      <th class='centro'><input type='checkbox' id="todo"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      imprimeFacturas($_POST['anio']);
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /widget-content-->
            </div>

        </div>
        
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/filtroTabla.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>