<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de facturas


function operacionesFacturas(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaFactura();
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=creaFactura();
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminado('facturas',$_POST['elimina']);
	}
	elseif(isset($_POST['codigoFactura0'])){
		$res=actualizaFacturacionPendientes();
	}
	elseif(isset($_GET['codigoContrato'])){
		$res=creaFacturaContrato();
	}

	mensajeResultado('codigoCliente',$res,'Factura');
    mensajeResultado('elimina',$res,'Factura', true);
}

function creaFacturaContrato(){
	$res=true;
	$contrato=consultaBD('SELECT contratos.codigo, ofertas.codigoCliente, ofertas.subtotal, ofertas.total FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo WHERE contratos.codigo='.$_GET['codigoContrato'],true,true);
	//$res=consultaBD('UPDATE contratos SET aceptado="SI" WHERE codigo='.$contrato['codigo'],true);
	$emisor=consultaBD('SELECT * FROM emisores');
	$emisor=mysql_fetch_assoc($emisor);
	$serie=consultaBD('SELECT * FROM series_facturas');
	$serie=mysql_fetch_assoc($serie);
	$numero=0;
	$res=consultaBD("INSERT INTO facturas(codigo,fecha,codigoCliente,tipoFactura,baseImponible,total,activo,codigoEmisor,codigoSerieFactura,numero,codigoUsuarioFactura) VALUES(NULL,'".date('Y-m-d')."','".$contrato['codigoCliente']."','PROFORMA','".$contrato['subtotal']."','".$contrato['total']."','SI','".$emisor['codigo']."','".$serie['codigo']."','".$numero."',".$_SESSION['codigoU'].");",true);
	$codigoFactura=mysql_insert_id();
	$res=consultaBD("INSERT INTO contratos_en_facturas VALUES(NULL,".$contrato['codigo'].",".$codigoFactura.")",true);
	if($_GET['mail']=='SI'){
		notificaProforma($contrato['codigo'],$contrato['codigoCliente']);
	}
	return $res;
}

function creaFactura(){
	formateaImportesFacturas();
	$res=insertaDatos('facturas');
	
	if($res){
		$res=insertaCamposAdicionalesFactura($res);
	}

	return $res;
}

function actualizaFactura(){
	formateaImportesFacturas();
	$res=actualizaDatos('facturas');
	
	if($res){
		$res=insertaCamposAdicionalesFactura($_POST['codigo'],true);
	}

	return $res;
}


function insertaCamposAdicionalesFactura($codigoFactura,$actualizacion=false){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	if($actualizacion){
		$res=$res && consultaBD("DELETE FROM contratos_en_facturas WHERE codigoFactura=$codigoFactura");
	}

	$res=$res && insertaConceptosFactura($datos,$codigoFactura);
	$res=$res && insertaVencimientosFactura($datos,$codigoFactura);
	//$res=$res && insertaCobrosFactura($datos,$codigoFactura);
	$res=$res && creaPlanificacion($codigoFactura);

	cierraBD();

	return $res;
}

function insertaConceptosFactura($datos,$codigoFactura){
	$res=true;

	for($i=0;isset($datos['codigoContrato'.$i]);$i++){
		$codigoContrato=$datos['codigoContrato'.$i];

		$res=$res && consultaBD("INSERT INTO contratos_en_facturas VALUES(NULL,$codigoContrato,$codigoFactura);");
	}

	return $res;
}


function insertaCobrosFactura($datos,$codigoFactura){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos["codigoCobro$i"]) || isset($datos["codigoFormaPagoCobro$i"]);$i++){
		$codigoFormaPagoCobro=$datos['codigoFormaPagoCobro'.$i];
		$fechaCobro=$datos['fechaCobro'.$i];
		$importeCobro=formateaNumeroWeb($datos['importeCobro'.$i],true);
		$codigoCuentaPropia=$datos['codigoCuentaPropia'.$i];

		if(isset($datos['codigoCobro'.$i]) && isset($datos['codigoFormaPagoCobro'.$i])){//Actualización
			$codigoCobro=$datos['codigoCobro'.$i];

			$res=$res && consultaBD("UPDATE cobros_facturas SET codigoFormaPago=$codigoFormaPagoCobro, fechaCobro='$fechaCobro', importe='$importeCobro', codigoCuentaPropia=$codigoCuentaPropia WHERE codigo=$codigoCobro");
		}
		elseif(!isset($datos['codigoCobro'.$i]) && isset($datos['codigoFormaPagoCobro'.$i])){//Inserción
			$res=$res && consultaBD("INSERT INTO cobros_facturas VALUES(NULL,$codigoFactura,$codigoFormaPagoCobro,'$fechaCobro',$importeCobro,$codigoCuentaPropia,NULL);");
		}
		else{//Eliminación
			$res=$res && consultaBD("DELETE FROM cobros_facturas WHERE codigo='".$datos['codigoCobro'.$i]."';");
		}
	}

	cierraBD();

	return $res;
}


/*function estadisticasFacturas($anio=''){
	$res=array();
	if($anio==''){
		$whereEjercicio=obtieneWhereEjercicioEstadisticas('facturas.fecha');
		$consulta=consultaBD('SELECT COUNT(codigo) AS cantidad FROM facturas WHERE tipoFactura="PROFORMA" '.$whereEjercicio.' AND eliminado="NO"',true,true);
		$res['cantidad']=$consulta['cantidad'];
		$consulta=consultaBD('SELECT SUM(total) AS total FROM facturas WHERE tipoFactura="PROFORMA" '.$whereEjercicio.' AND eliminado="NO"',true,true);
		$res['total']=$consulta['total'];
		$consulta=consultaBD('SELECT SUM(facturas.total) AS total FROM facturas INNER JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura INNER JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo WHERE tipoFactura="PROFORMA" '.$whereEjercicio.' AND facturas.eliminado="NO" AND ofertas.opcion=1',true,true);
		$res['total1']=$consulta['total'];
		$consulta=consultaBD('SELECT IFNULL(SUM(facturas.total),0) AS total FROM facturas INNER JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura INNER JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo WHERE tipoFactura="PROFORMA" '.$whereEjercicio.' AND facturas.eliminado="NO" AND ofertas.opcion=2',true,true);
		$res['total2']=$consulta['total'];
		$consulta=consultaBD('SELECT IFNULL(SUM(facturas.total),0) AS total FROM facturas INNER JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura INNER JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo WHERE tipoFactura="PROFORMA" '.$whereEjercicio.' AND facturas.eliminado="NO" AND ofertas.opcion=3',true,true);
		$res['total3']=$consulta['total'];
		$consulta=consultaBD('SELECT IFNULL(SUM(facturas.total),0) AS total FROM facturas WHERE tipoFactura="PROFORMA" '.$whereEjercicio.' AND eliminado="NO" AND facturaCobrada="SI";',true,true);
		$res['totalCobrado']=$consulta['total'];
		$consulta=consultaBD('SELECT IFNULL(SUM(facturas.total),0) AS total FROM facturas WHERE tipoFactura="PROFORMA" '.$whereEjercicio.' AND eliminado="NO" AND facturaCobrada="NO";',true,true);
		$res['totalNoCobrado']=$consulta['total'];
	}
	else{
		$consulta=consultaBD('SELECT COUNT(codigo) AS cantidad FROM facturas WHERE tipoFactura="PROFORMA" AND YEAR(fecha)="'.$anio.'" AND eliminado="NO"',true,true);
		$res['cantidad']=$consulta['cantidad'];
		$consulta=consultaBD('SELECT SUM(total) AS total FROM facturas WHERE tipoFactura="PROFORMA" AND YEAR(fecha)="'.$anio.'" AND eliminado="NO"',true,true);
		$res['total']=$consulta['total'];
	}


	return $res;
}*/


function imprimeFacturas($eliminado='NO'){
	$whereAnio=obtieneWhereEjercicioEstadisticas('facturas.fecha');

	
	$consulta=consultaBD("SELECT facturas.codigo, facturas.fecha, serie, facturas.numero, CONCAT(clientes.EMPNOMBRE,' (',clientes.EMPMARCA,')') AS cliente, clientes.EMPCIF AS cif, facturas.baseImponible, 
						  facturas.total, facturas.activo, facturas.facturaCobrada,
						  cobros_facturas.codigo AS codigoCobro, clientes.codigo AS codigoCliente, clientes.EMPCP, clientes.EMPID, contratos.fechaInicio, contratos.codigoInterno, contratos.enVigor

						  FROM facturas INNER JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
						  INNER JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
						  LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
						  LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
						  LEFT JOIN cobros_facturas ON facturas.codigo=cobros_facturas.codigoFactura AND cobros_facturas.importe>0

						  WHERE facturas.tipoFactura='PROFORMA' AND facturas.eliminado='$eliminado' AND contratos.eliminado='NO' $whereAnio

						  GROUP BY facturas.codigo",true);

	$iconosEstado=array(
					'SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>',
					'PARCIAL'=>'<i class="icon-adjust iconoFactura icon-warning-color"></i>',
					'NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		$cobrada = $iconosEstado[obtienePagada($datos)];
		$textoEnvia='Enviar proforma';
		$envio=consultaBD('SELECT COUNT(codigo) AS total FROM facturas_envios WHERE codigoFactura='.$datos['codigo'],true,true);
		if($envio['total']>0){
			$textoEnvia='Reenviar proforma';
		}

		$referenciaContrato = formateaReferenciaContrato($datos,$datos);
		conexionBD();
		$conceptos=obtieneConceptosFactura($datos['codigo'],$datos,$referenciaContrato,$datos['total'],$datos['baseImponible']);
		cierraBD();
		
		// Para la comprobación, los valores no son exactamente los mismo, por lo que aumentamos la precision de la base imponible de conceptos
		// porque al hacerle formateaNumeroWeb lleva implicito un round
		// Esto se realiza en la comprobacion ya que una diferencia en el tercer decimal es posible y puede dar falsas alertas
		// Aun asi, en las factura, saldra saliendo la BI que es mas alta y redondeado a unas decimas.
		$baseImponibleConcepto = floor($conceptos['baseImponible']*100)/100;
		$baseTotalConcepto     =floor($conceptos['total']*100)/100;

		// Por ello, creamos dos campos extras de visualizacion, para segun cada caso, muestre el valor mas alto y si hay que hacerle alguna comprobación adicional
		$baseImponibleVista = $datos['baseImponible'];
		$totalVista 		= $datos['total'];

		$icono = "";
		$bloquear = false; // A peticion expresa del cliente, si se ve algun fallo, no podra pasarse a definitiva hasta que este corregido.
		$acciones = botonAccionesConClase(array('Detalles','Descargar proforma',$textoEnvia,'Pasar a definitiva'),array('proformas/gestion.php?codigo='.$datos['codigo'],'proformas/generaFactura.php?codigo='.$datos['codigo'],'proformas/generaFactura.php?envio&codigo='.$datos['codigo'],'facturas/gestion.php?codigoFacturaProforma='.$datos['codigo']),array("icon-search-plus",'icon-cloud-download','icon-envelope','icon-file-text'),array(0,1,0,0),true,'',array('','','enviaFactura noAjax',''));
		if (($baseImponibleConcepto > $datos['baseImponible'] || $baseTotalConcepto > $datos['total']) && $_SESSION['ejercicio'] >= 2024) {
			$icono = '<i class="btn-danger btn btn-small icon-exclamation" title="Se ha detectado una anomalia entre totales y/o base imponible."></i>';
			$acciones = botonAccionesConClase(array('Detalles','Descargar proforma'),array('proformas/gestion.php?codigo='.$datos['codigo'],'proformas/generaFactura.php?codigo='.$datos['codigo']),array("icon-search-plus",'icon-cloud-download'),array(0,1),true,'',array('',''));
			$baseImponibleVista = $baseImponibleConcepto;
			$totalVista			= $baseTotalConcepto;
		} 


		$vigor=$datos['enVigor']=='SI'?'En vigor':'Sin vigor';
		$concepto='Contrato Nº '.$referenciaContrato.' ('.$vigor.')';
		echo "<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['serie']." ".$icono."</td>
				<td><a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."' class='noAjax' target='_blank'>".$datos['cliente']."</a></td>
				<td>".$datos['cif']."</td>
				<td class='nowrap pagination-right'>".formateaNumeroWeb($baseImponibleVista)."€</td>
				<td class='nowrap pagination-right'>".formateaNumeroWeb($totalVista)." €</td>
				<td class='centro'>".$concepto."</td>
				<td class='centro'>".$cobrada."</td>
				".$acciones."
				<td class='centro'>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
}

function obtieneIconoAbono($codigoAbono){
	$res='';

	if($codigoAbono!=NULL){
		$res='<i class="icon-danger icon-exclamation-circle" title="Abono generado"></i>';
	}

	return $res;
}


function gestionFactura(){
	operacionesFacturas();

	abreVentanaGestionConBotones('Gestión de Facturas','index.php','span3','icon-edit','',false,'noAjax');
	$datos=compruebaDatos('facturas');

	campoSelectConsulta('codigoCliente','Cliente',"SELECT codigo, CONCAT(EMPNOMBRE,' (',EMPMARCA,')') AS texto FROM clientes ORDER BY EMPNOMBRE",$datos);
	campoTexto('fecha','Fecha',formateaFechaWeb($datos['fecha']),'input-small datepicker hasDatepicker obligatorio');
	campoSelect('esModeloAntiguo','Modelo de facturacion',['Nuevo 2024','Antiguo 2023'],[true,false],true);
	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectConsulta('codigoSerieFactura','Serie',"SELECT codigo, serie AS texto FROM series_facturas WHERE activo='SI' ORDER BY serie",$datos,'span1 selectpicker show-tick');
	/*campoTexto('numeroMuestra','Número','PROFORMA','input-mini pagination-right obligatorio');
	campoDato('Serie','PROFORMA');*/
	/*campoSelectConsulta('codigoEmisor','Emisor','SELECT codigo, razonSocial AS texto FROM emisores WHERE activo="SI" ORDER BY razonSocial;',$datos,'selectpicker show-tick span3 obligatorio');*/

	cierraColumnaCampos();
	abreColumnaCampos('sinFlotar');
	cierraColumnaCampos();
	abreColumnaCampos('span11');

	tablaEnviosFacturas($datos);
	creaTablaConceptos($datos);

	cierraColumnaCampos();
	abreColumnaCampos('span5');

	campoTextoSimbolo('baseImponible','Base imponible','€',formateaNumeroWeb($datos['baseImponible']));
	campoTextoSimbolo('total','Total','€',formateaNumeroWeb($datos['total']),'input-mini pagination-right obligatorio');

	cierraColumnaCampos();
	abreColumnaCampos();

	areaTexto('comentariosLlamada','Comentarios de llamada',$datos);
	
	campoOculto($datos,'activo','SI');
	campoOculto('NULL','codigoFacturaAsociada');//Este campo es exclusivo para los abonos
	campoOculto($datos,'tipoFactura','PROFORMA');
	campoOculto($datos,'facturaCobrada','NO');
	campoOculto($datos,'informeEnviado','NO');
	campoOculto($datos,'codigoUsuarioFactura',$_SESSION['codigoU']);

	cierraColumnaCampos();
	abreColumnaCampos('span11');

	areaTexto('observaciones','Observaciones',$datos,'observacionesParaFactura');
	creaTablaVencimientos($datos);
	//creaTablaCobros($datos);

	cierraVentanaGestion('index.php',true);
}

function compruebaAbono($datos){
	if($datos){
		$abono=consultaBD("SELECT codigo FROM facturas WHERE codigoFacturaAsociada=".$datos['codigo']." AND tipoFactura='ABONO'",true,true);
		if($abono){
			campoDato('Abono generado',"<a href='../abonos/gestion.php?codigo=".$abono['codigo']."' class='btn btn-small btn-danger noAjax' target='_blank'><i class='icon-exclamation-circle'></i> Ver abono</a>");
		}
	}
}


/*
	Reglas para obtener el vencimiento:
	+ A Bonificación: Ej: Si un curso finaliza el mes de Mayo, independientemente del día, tendría que aparecer la fecha 01 de Julio. 
	+ Quincenas: Si la factura tiene fecha del 01 al 15 el vencimiento es el 24 del mismo mes. 
	             Si la factura tiene fecha del 16 al 31, el vencimiento es el 09 del mes siguiente.
	+ Día 1 pasados 2 meses: Si la factura tiene fecha 28/06 sería el 1 de Septiembre.
	+ En la fecha: se define directamente la fecha del vencimiento.
*/
function obtieneVencimientoConcepto($tipoVencimiento,$fecha,$fechaFacturacion=false,$bdd=false){

	if($tipoVencimiento=='EN LA FECHA'){
		$arrayFecha=explode('-',$fechaFacturacion);
	}
	else{
		$arrayFecha=explode('-',$fecha);

		if($tipoVencimiento=='A BONIFICACIÓN'){
			$arrayFecha=sumaMesFecha($arrayFecha,2);
			$arrayFecha[2]='01';
		}
		elseif($tipoVencimiento=='QUINCENAS' && $arrayFecha[2]<16){
			$arrayFecha[2]='24';
		}
		elseif($tipoVencimiento=='QUINCENAS' && $arrayFecha[2]>15){
			$arrayFecha=sumaMesFecha($arrayFecha,1);
			$arrayFecha[2]='09';
		}
		else{
			$arrayFecha=sumaMesFecha($arrayFecha,3);
			$arrayFecha[2]='01';
		}
	}

	if(!$bdd){
		$res=$arrayFecha[2].'/'.$arrayFecha[1].'/'.$arrayFecha[0];
	}
	else{
		$res=$arrayFecha[0].'-'.$arrayFecha[1].'-'.$arrayFecha[2];
	}

	return $res;
}

function sumaMesFecha($arrayFecha,$suma){
	$arrayFecha[1]+=$suma;

	if($arrayFecha[1]>12){
		$arrayFecha[1]-=12;
		$arrayFecha[0]+=1;
	}

	if($arrayFecha[1]<10){
		$arrayFecha[1]='0'.$arrayFecha[1];
	}

	return $arrayFecha;
}


function creaTablaConceptos($datos){
	echo "
	<div class='control-group'>                     
		<label class='control-label'>Conceptos:</label>
	    <div class='controls'>
			<table class='table table-striped tabla-simple mitadAncho' id='tablaConceptos'>
			  	<thead>
			    	<tr>
			            <th> Contrato </th>
						<th> </th>
			    	</tr>
			  	</thead>
			  	<tbody>";

			  		$i=0;
			  		conexionBD();
			  		if($datos){
			  			$consulta=consultaBD("SELECT * FROM contratos_en_facturas WHERE codigoFactura=".$datos['codigo']);
			  			while($datosConceptos=mysql_fetch_assoc($consulta)){
			  				imprimeLineaTablaConceptos($datosConceptos,$i,$datos['codigoCliente']);
			  				$i++;
			  			}
			  		}

			  		if($i==0){
			  			imprimeLineaTablaConceptos(false,0,false);
			  		}
			  		cierraBD();

	echo "		</tbody>
			</table>
			<!--button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaConceptos\");'><i class='icon-plus'></i> Añadir concepto</button> 
			<button type='button' class='btn btn-small btn-danger' onclick='eliminaFilaFacturacion(\"tablaConceptos\");'><i class='icon-trash'></i> Eliminar concepto</button-->
		</div>
	</div>
	<br /><br />";
}

function imprimeLineaTablaConceptos($datos,$i,$codigoCliente){
	$j=$i+1;

	echo "
	<tr>";
		campoSelectContratoFactura($datos,$i,$codigoCliente);	
	echo "<td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td>
	</tr>";
}

function campoSelectContratoFactura($datos,$i,$codigoCliente){
	$nombres=array('');
	$valores=array('NULL');

	if($datos){
		$datosCliente=obtieneContratosCliente($codigoCliente);
		$nombres=$datosCliente['nombres'];
		$valores=$datosCliente['valores'];
	}

	campoSelect('codigoContrato'.$i,'',$nombres,$valores,$datos['codigoContrato'],'selectpicker span8 show-tick','data-live-search="true"',1);
}


function obtieneContratosCliente($codigoCliente){
	$res=array('nombres'=>array(''),'valores'=>array('NULL'));

	$consulta=consultaBD("SELECT contratos.codigo, contratos.codigoInterno, ofertas.total, contratos.fechaInicio, clientes.EMPCP, clientes.EMPID, ofertas.subtotal, ofertas.subtotalRM,
						  contratos.incrementoIPC, contratos.incrementoIpcAplicado
						  FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
						  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
						  WHERE ofertas.codigoCliente='$codigoCliente';");

	while($datos=mysql_fetch_assoc($consulta)){
		$numeroContrato=formateaReferenciaContrato($datos,$datos);
		$nombre='Nº: '.$numeroContrato.'. &nbsp; RRMM:  '.compruebaImporteContrato($datos['subtotalRM'],$datos).' €. &nbsp; Actividades: '.compruebaImporteContrato($datos['subtotal'],$datos).' €. &nbsp; Total: '.compruebaImporteContrato($datos['total'],$datos).' €';

		array_push($res['nombres'],$nombre);
		array_push($res['valores'],$datos['codigo']);
	}

	return $res;
}

function obtieneContratosClienteAjax(){
	$datos=arrayFormulario();
	extract($datos);

	$res="<option value='NULL'></option>";

	$consulta=consultaBD("SELECT contratos.codigo, contratos.codigoInterno, ofertas.total, contratos.fechaInicio, clientes.EMPCP, clientes.EMPID, ofertas.subtotal, ofertas.subtotalRM,
						  contratos.incrementoIPC, contratos.incrementoIpcAplicado
						  FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
						  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
						  WHERE ofertas.codigoCliente='$codigoCliente' AND codigoContrato NOT IN(SELECT codigoContrato FROM contratos_en_facturas WHERE codigoContrato IS NOT NULL AND codigoFactura IS NOT NULL);",true);

	while($datos=mysql_fetch_assoc($consulta)){
		$numeroContrato=formateaReferenciaContrato($datos,$datos);
		$nombre='Nº: '.$numeroContrato.'. &nbsp; RRMM:  '.compruebaImporteContrato($datos['subtotalRM'],$datos).' €. &nbsp; Actividades: '.compruebaImporteContrato($datos['subtotal'],$datos).' €. &nbsp; Total: '.compruebaImporteContrato($datos['total'],$datos).' €';

		$res.="<option value='".$datos['codigo']."'>".$nombre."</option>";
	}

	echo $res;
}


function creaTablaVencimientos($datos){
	echo "<br />
	<div class='control-group'>                     
		<label class='control-label'>Vencimientos:</label>
	    <div class='controls'>
	    </div>
	</div>
	<div class='centro'>
		<div class='table-responsive'>
			<table class='table table-striped tabla-simple' id='tablaVencimientos'>
			  	<thead>
			    	<tr>
			            <th> Medio pago </th>
						<th> F. Vencimiento </th>
						<th> Importe </th>
						<th> Estado </th>
						<th> Concepto </th>
						<th> Observaciones </th>
						<th> </th>
			    	</tr>
			  	</thead>
			  	<tbody>";

			  		$contrato=datosRegistro('contratos_en_facturas',$datos['codigo'],'codigoFactura');

			  		$i=0;
			  		conexionBD();
			  		
			  		if($datos){
			  			
			  			$consulta=consultaBD("SELECT * FROM vencimientos_facturas WHERE codigoFactura=".$datos['codigo']);
			  			while($datosVencimiento=mysql_fetch_assoc($consulta)){
			  				imprimeLineaTablaVencimientos($datosVencimiento,$i,$datos);
			  				campoOculto($datosVencimiento['codigo'],'codigoVencimiento'.$i);
			  				$i++;
			  			}

			  			if($i==0){
			  				$factura=consultaBD('SELECT * FROM contratos_en_facturas WHERE codigoContrato='.$contrato['codigoContrato'].' AND codigoFactura!='.$datos['codigo'],false,true);
			  				
			  				if($factura){
			  					campoOculto($factura['codigoFactura'],'codigoFacturaVencimientos');

			  					$consulta=consultaBD("SELECT * FROM vencimientos_facturas WHERE codigoFactura=".$factura['codigoFactura']);
			  					while($datosVencimiento=mysql_fetch_assoc($consulta)){
			  						imprimeLineaTablaVencimientos($datosVencimiento,$i,$datos);
			  						campoOculto($datosVencimiento['codigo'],'codigoVencimiento'.$i);
			  						$i++;
			  					}
			  				}
			  			}
			  		}

			  		if($i==0){
			  			imprimeLineaTablaVencimientos(false,0,false);
			  		}

			  		cierraBD();

	echo "		</tbody>
			</table>
			<div class='centro'>
				<button type='button' class='btn btn-small btn-success' onclick='insertaFilaTablaOcultos(\"tablaVencimientos\");'><i class='icon-plus'></i> Añadir vencimiento</button> 
				<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaVencimientos\");'><i class='icon-trash'></i> Eliminar vencimiento</button> ";


				if($_SESSION['tipoUsuario']=='CONTABILIDAD' || $_SESSION['tipoUsuario']=='ADMIN'){
					echo "<!--button type='button' class='btn btn-small btn-primary' onclick='mostrarOcultos(\"#tablaVencimientos\");' estado='mostrar'><i class='icon-search'></i> Mostrar ocultos</button> 
						  <button type='button' class='btn btn-small btn-warning' onclick='ocultarSeleccionados(\"#tablaVencimientos\");'><i class='icon-eye-slash'></i> Ocultar seleccionado/s</button>
						  <button type='button' class='btn btn-small btn-info' onclick='desOcultarSeleccionados(\"#tablaVencimientos\");'><i class='icon-eye'></i> Des-ocultar seleccionado/s</button-->";
				}

	echo "
			</div>
		</div>
	</div><br /><br />";
}

function imprimeLineaTablaVencimientos($datosVencimiento,$i,$datosFactura){
	$j=$i+1;
	

	echo "
	<tr>";
		campoFormaPago($datosVencimiento['codigoFormaPago'],'codigoFormaPago'.$i,1,false);
		campoFechaTabla('fechaVencimiento'.$i,$datosVencimiento['fechaVencimiento']);
		campoTextoSimbolo('importeVencimiento'.$i,'','€',formateaNumeroWeb($datosVencimiento['importe']),'input-mini pagination-right',1);
		campoSelectEstadoVencimiento($datosVencimiento,$i,1,true,false);
		campoSelectConceptoVencimiento($datosVencimiento,$i,$datosFactura);
		areaTextoTabla('observacionesVencimiento'.$i,$datosVencimiento['observaciones'],''); 
		campoOculto('','fechaDevolucion'.$i);
		campoOculto('','gastosDevolucion'.$i);
		
	echo "<td class='centro'>";
			campoOculto($datosVencimiento['llamadaAsesoria'],'llamadaAsesoria'.$i,'NO');
			campoOculto($datosVencimiento['llamadaEmpresa'],'llamadaEmpresa'.$i,'NO');
	echo "	<input type='checkbox' name='filasTabla[]' value='$j'>
		  </td>
	</tr>";
}


function campoSelectConceptoVencimiento($datos,$i,$datosFactura){
	campoTextoTabla('conceptoVencimiento'.$i,$datos['concepto']);

	//Comentado temporalmente, a espera de ver si prefieren el campo de texto
	/*if(!$datos){
		campoSelect('codigoConcepto'.$i,'',array(),array(),false,'selectpicker span3 show-tick','data-live-search="true"',1);
	}
	else{
		$query=obtieneConsultaConceptosVencimiento($datosFactura['codigo'],$datosFactura['tipo']);
		campoSelectConsulta('codigoConcepto'.$i,'',$query,$datos['codigoConcepto'],'selectpicker span3 show-tick','data-live-search="true"','',1);
	}*/
}


function obtieneConsultaConceptosVencimiento($codigoFactura,$tipo){
	$res='';

	if($tipo=='FORMACION'){
		$res="SELECT grupos.codigo, CONCAT(accionFormativa,' - ',grupos.grupo,' - ',accion) AS texto FROM acciones_formativas INNER JOIN grupos ON acciones_formativas.codigo=grupos.codigoAccionFormativa INNER JOIN grupos_en_facturas ON grupos.codigo=grupos_en_facturas.codigoGrupo WHERE codigoFactura=$codigoFactura ORDER BY accionFormativa;";
	}
	elseif($tipo=='SERVICIOS'){
		$res="SELECT codigo, DATE_FORMAT(ventas_servicios.fecha,'%d/%m/%Y') AS texto FROM ventas_servicios INNER JOIN ventas_en_facturas ON ventas_servicios.codigo=ventas_en_facturas.codigoVentaServicio WHERE codigoFactura=$codigoFactura ORDER BY fecha;";
	}
	elseif($tipo=='OTROS'){
		$res="SELECT codigo, concepto AS texto FROM conceptos_libres_facturas WHERE codigoFactura=$codigoFactura ORDER BY concepto;";
	}

	return $res;
}


function creaTablaCobros($datos){
	echo "	
	<div class='control-group'>                     
		<label class='control-label'>Cobros:</label>
	    <div class='controls'>
			<div class='table-responsive'>
				<table class='table table-striped tabla-simple' id='tablaCobros'>
				  	<thead>
				    	<tr>
				            <th> Medio pago </th>
							<th> F. Cobro </th>
							<th> Importe </th>
							<th> Banco </th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		conexionBD();
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM cobros_facturas WHERE codigoFactura=".$datos['codigo']);
				  			while($datosCobro=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaCobros($datosCobro,$i);
				  				campoOculto($datosCobro['codigo'],'codigoCobro'.$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaCobros(false,0);
				  		}
				  		cierraBD();

	echo "			</tbody>
				</table>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='insertaFilaTablaOcultos(\"tablaCobros\");'><i class='icon-plus'></i> Añadir cobro</button> 
					<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaCobros\");'><i class='icon-trash'></i> Eliminar cobro</button> ";

					if($_SESSION['tipoUsuario']=='CONTABILIDAD' || $_SESSION['tipoUsuario']=='ADMIN'){
						echo "<!--button type='button' class='btn btn-small btn-primary' onclick='mostrarOcultos(\"#tablaCobros\");' estado='mostrar'><i class='icon-search'></i> Mostrar ocultos</button> 
							  <button type='button' class='btn btn-small btn-warning' onclick='ocultarSeleccionados(\"#tablaCobros\");'><i class='icon-eye-slash'></i> Ocultar seleccionado/s</button>
							  <button type='button' class='btn btn-small btn-info' onclick='desOcultarSeleccionados(\"#tablaCobros\");'><i class='icon-eye'></i> Des-ocultar seleccionado/s</button-->";
					}

	echo "
				</div>
			</div>
		</div>
	</div>";
}

function imprimeLineaTablaCobros($datos,$i){
	$j=$i+1;

	echo "
	<tr>";
	
		campoFormaPago($datos['codigoFormaPago'],'codigoFormaPagoCobro'.$i,1,false);
		campoFechaTabla('fechaCobro'.$i,$datos['fechaCobro']);
		campoTextoSimbolo('importeCobro'.$i,'','€',formateaNumeroWeb($datos['importe']),'input-mini pagination-right',1);
		campoSelectConsulta('codigoCuentaPropia'.$i,'',"SELECT codigo, nombre AS texto FROM cuentas_propias WHERE activo='SI' ORDER BY nombre;",$datos['codigoCuentaPropia'],'selectpicker span3 show-tick','data-live-search="true"','',1);
	
	echo "<td class='centro'>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		  </td>
	</tr>";
}



function imprimeContratosPendientes(){
	global $_CONFIG;

	conexionBD();
	$where=defineWhereEjercicio('WHERE 1=1','contratos.fechaInicio');
	$consulta=consultaBD("SELECT clientes.EMPNOMBRE,clientes.codigo AS codigoCliente, ofertas.total, contratos.*
						  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
						  ".$where." AND contratos.codigo NOT IN(SELECT codigoContrato FROM contratos_en_facturas WHERE codigoContrato IS NOT NULL AND codigoFactura IS NOT NULL);");
	

	$iconoC=array('SI'=>'<i style="color:#5DB521;font-size:24px;" class="icon-check"></i>','NO'=>'<i style="color:#FF0000;font-size:24px;" class="icon-close"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		$cliente=datosRegistro('clientes',$datos['codigoCliente']);
		$renovacion=consultaBD('SELECT * FROM contratos_renovaciones WHERE codigoContrato='.$datos['codigo'].' ORDER BY fecha DESC');
		$renovacion=mysql_fetch_assoc($renovacion);
		echo "
			<tr>
				<td>".formateaReferenciaContrato($cliente,$datos)."</td>
				<td>".formateaFechaWeb($datos['fechaInicio'])."</td>
				<td>".formateaFechaWeb($datos['fechaFin'])."</td>
				<td>".formateaFechaWeb($renovacion['fecha'])."</td>
				<td>".strtoupper($cliente['EMPNOMBRE'])."</td>
				<td>".formateaNumeroWeb($datos['total'])." €</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	cierraBD();
}



function ventasPendientesSeleccionadas(){
	abreVentanaGestion('Gestión de ventas pendientes','index.php');
	echo "<p class='justificado'>Indique por favor la serie y el emisor de cada una de las facturas generadas:</p>

	<table class='table table-striped tabla-simple' id='tablaVentasRegistradas'>
        <thead>
          <tr>
          	<th>Contrato</th>
          	<th>Cliente</th>
          	<th>Fecha</th>
            <th>Base imponible</th>
            <th>Serie</th>
            <th>Emisor</th>
          </tr>
        </thead>
        <tbody>";

        	conexionBD();

        	$facturas=creaFacturasVentasPendientes();

        	$i=0;
        	foreach($facturas as $codigoFactura){
        		$datos=consultaBD("SELECT facturas.codigo, facturas.fecha, codigoSerieFactura, clientes.EMPNOMBRE AS cliente, 
        						   clientes.EMPCIF, clientes.EMPCP, clientes.EMPID, contratos.codigoInterno, facturas.baseImponible, 
        						   contratos.fechaInicio, facturas.total, facturas.anulada, facturas.activo, clientes.codigo AS codigoCliente
								   FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
								   LEFT JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
								   LEFT JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
								   WHERE facturas.codigo='$codigoFactura';",false,true);

        		imprimeLineaTablaVentasSeleccionadas($codigoFactura,$datos,$i);
        		$i++;
        	}

        	cierraBD();

	echo "          
    	</tbody>
    </table>";



	cierraVentanaGestion('index.php',false,true,'Guardar','icon-check',true,'Cancelar','icon-remove');
}

function imprimeLineaTablaVentasSeleccionadas($codigoFactura,$datos,$i){
	echo "<tr>
			<td>".formateaReferenciaContrato($datos,$datos)."</td>
			<td>".$datos['cliente']."</td>
			<td>".formateaFechaWeb($datos['fecha'])."</td>
			<td>".formateaNumeroWeb($datos['baseImponible'])." €</td>";
			campoSelectConsulta('codigoSerieFactura'.$i,'',"SELECT codigo, serie AS texto FROM series_facturas WHERE activo='SI' ORDER BY serie",false,'span2 selectpicker serie show-tick','data-live-search="true"','',1,false);
			campoSelectConsulta('codigoEmisor'.$i,'','SELECT codigo, razonSocial AS texto FROM emisores WHERE activo="SI" ORDER BY razonSocial;',false,'selectpicker emisor show-tick span3','data-live-search="true"','',1,false);
			campoOculto(0,'numero'.$i);
	echo "</tr>";

	campoOculto($codigoFactura,'codigoFactura'.$i);
}


function creaFacturasVentasPendientes(){
	$res=array();
	$datos=arrayFormulario();
	$fecha=fechaBD();

	conexionBD();
	foreach($datos['codigoLista'] as $codigoContrato) {
		$datosCliente=consultaBD("SELECT ofertas.importe_iva,clientes.*, subtotal, subtotalRM, total, formaPago, contratos.incrementoIPC, contratos.incrementoIPCAplicado
								  FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
								  INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
								  WHERE contratos.codigo='$codigoContrato';",false,true);

		// CUALQUIER FACTURA DE VENTA PENDIENTE SE LE APLICARA EL IPC NUEVO
		$datosCliente['subtotal'] 		= compruebaImporteContrato($datos['subtotal'],$datos,false);
		$datosCliente['importe_iva'] 	= compruebaImporteContrato($datos['importe_iva'],$datos,false);
		$datosCliente['subtotalRM'] 	= compruebaImporteContrato($datos['subtotalRM'], $datos, false);
		$datosCliente['baseImponible'] = $datosCliente['subtotal'] + $datosCliente['subtotalRM'];
		$datosCliente['total'] 		= $datos['importe_iva'] + $datosCliente['baseImponible'];


		$consulta=consultaBD("INSERT INTO facturas(codigo,fecha,codigoCliente,tipoFactura,baseImponible,total,activo,codigoUsuarioFactura)
							  VALUES(NULL,'$fecha','".$datosCliente['codigo']."','PROFORMA','".$datosCliente['baseImponible']."',
							  '".$datosCliente['total']."','SI',".$_SESSION['codigoU'].");");

		if($consulta){
			$codigoFactura=mysql_insert_id();
			notificaProforma($codigoContrato,$datosCliente['codigo']);
			array_push($res,$codigoFactura);

			$consulta=$consulta && consultaBD("INSERT INTO contratos_en_facturas VALUES(NULL,$codigoContrato,$codigoFactura)");

			$consulta=$consulta && consultaBD("INSERT INTO vencimientos_facturas(codigo,codigoFactura,codigoFormaPago,importe) 
											   VALUES(NULL,$codigoFactura,NULL,".$datosCliente['total'].");");
		}
	}
	cierraBD();

	return $res;
}


function actualizaFacturacionPendientes(){
	$res=true;
	$_POST['codigoCliente']='';//Para que entre en mensajeResultado
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos['codigoFactura'.$i]);$i++){
		$codigoFactura=$datos['codigoFactura'.$i];
		$codigoSerieFactura=$datos['codigoSerieFactura'.$i];
		$numero=$datos['numero'.$i];
		$codigoEmisor=$datos['codigoEmisor'.$i];


		//Actualización de la factura creada con los datos obtenidos
		$res=$res && consultaBD("UPDATE facturas SET codigoSerieFactura=$codigoSerieFactura, numero=$numero, codigoEmisor=$codigoEmisor WHERE codigo=$codigoFactura");
	}

	cierraBD();

	return $res;
}



function eliminaFacturasVentasPendientes(){
	$res=true;
	$datos=arrayFormulario();

	/*conexionBD();
	foreach ($datos['facturas'] as $codigoFactura){
		$res=$res && consultaBD("DELETE FROM facturas WHERE codigo=$codigoFactura");
	}
	cierraBD();*/

	if($res){
		$res='ok';
	}
	else{
		$res='error';
	}

	echo $res;
}


function notificaProforma($contrato,$cliente){
	$res=true;
	
	$cliente=datosRegistro('clientes',$cliente);
	$contrato=datosRegistro('contratos',$contrato);
	$codigo=formateaReferenciaContrato($cliente,$contrato);
	

	$mensaje="Se ha creado una nueva factura proforma del contrato <b>".$codigo." </b>";
	
	
	if (!enviaEmail('vigilanciasalud@isanesco.com', 'Notificación de factura proforma', $mensaje)){
		$res=false;
	}

	if($res){
		$res=consultaBD('INSERT INTO correos VALUES(NULL,"vigilanciasalud@isanesco.com","'.fechaBD().'","'.horaBD().'","Notificación de factura proforma","'.$mensaje.'","","'.$_SESSION['codigoU'].'","PROFORMA",NULL,"")',true);
	}
	
	return $res;
}

//Fin parte de facturas