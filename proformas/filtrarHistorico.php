<?php
  $seccionActiva=30;
  include_once("../cabecera.php");
?>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
         <div class="span12 margenAb">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-file-text"></i>
              <h3>Filtrar facturas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content centro">
              <h3>Seleccione el año:</h3><br><br>
				      <form action='facturasHistorico.php' method='post'>
      					<div class='control-group'>                     
      					  <div class='controls'>
      						<input type='text' class='input-small' id='anio' name='anio' value='<?php echo date('Y')-1; ?>'> 
      					  </div> <!-- /controls -->       
      					</div> <!-- /control-group -->

      					<br />
					
                <a href='index.php' class='btn btn-default'><i class='icon-chevron-left'></i> Volver </a>
    					  <button type="submit" class="btn btn-propio">Seleccionar <i class="icon-chevron-right"></i></button>
    				  </form>
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		</div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>