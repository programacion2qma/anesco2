<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de colaboradores

function operacionesColaboradores(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('colaboradores');
	}
	elseif(isset($_POST['razonSocial'])){
		$res=insertaDatos('colaboradores');
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoColaborador($_POST['elimina']);
	}

	mensajeResultado('razonSocial',$res,'colaborador');
    mensajeResultado('elimina',$res,'colaborador', true);
}


function gestionColaborador(){
	operacionesColaboradores();
	
	abreVentanaGestionConBotones('Gestión de colaboradores','index.php','span3');
	$datos=compruebaDatos('colaboradores');

	campoTexto('razonSocial','Razón social',$datos);
	campoTexto('cif','CIF',$datos,'input-small');
	campoTexto('direccion','Dirección',$datos);
	campoTexto('cp','Código postal',$datos,'input-mini pagination-right');
	campoTexto('localidad','Localidad',$datos);
	
	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto('provincia','Provincia',$datos);
	campoTexto('contacto','Persona de contacto',$datos);
	campoTextoSimbolo('telefono','Teléfono',"<i class='icon-phone'></i>",$datos,'input-small pagination-right');
	campoTextoSimbolo('email','eMail',"<i class='icon-envelope'></i>",$datos,'input-large');
	campoTextoSimbolo('comision','Comisión',"%",$datos);

	campoOculto($datos,'eliminado','NO');

	cierraVentanaGestion('index.php',true);
}


function imprimeColaboradores($estado='NO'){
	$consulta=consultaBD("SELECT * FROM colaboradores WHERE eliminado='$estado';",true);
	while($datos=mysql_fetch_assoc($consulta)){

		echo "<tr>
				<td> ".$datos['razonSocial']." </td>
				<td> ".$datos['cif']." </td>
				<td> ".$datos['contacto']." </td>
				<td><a href='tel:".str_replace(' ','',$datos['telefono'])."' class='noAjax'>".$datos['telefono']."</a></td>
				<td> <a href='mailto:".$datos['email']."' class='noAjax'>".$datos['email']."</a></td>
				<td> ".$datos['comision']." </td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>
				<td class='centro'>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' />
        	  	</td>
			 </tr>";
	}
}


function cambiaEstadoColaborador($estado){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();
    for($i=0;isset($datos['codigo'.$i]);$i++){
        $res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],'colaboradores',false);
    }
    cierraBD();

    return $res;
}

//Fin parte de colaboradores