<?php
session_start();
include_once('funciones.php');



echo "
<!DOCTYPE html>
<html>
<head>
	<title>Actualización masiva de inmobiliarias</title>
	<style type='text/css'>
		th,td{
			font-size:12px;
		}

		table{
			width:100%;
			border:1px solid #000;
			border-collapse:collapse;
		}

		th{
			border:1px solid #000;
			text-align:center;
			border-collapse:collapse;
			background-color:#AAA;
		}

		td{
			padding:5px;
			border:1px solid #000;
			border-collapse:collapse;
		}

		tr:nth-child(odd) td{
			background-color:#EEE;
		}
	</style>
</head>
<body>";

$res=true;
$codigosReconocimientosProcesados=array();

conexionBD();

$i=1;
$consulta=consultaBD("SELECT codigo, codigoContrato, fecha FROM facturas_reconocimientos_medicos WHERE eliminado='NO' AND codigoContrato IS NOT NULL");
while($datos=mysql_fetch_assoc($consulta)){
	
	extract($datos);
	
	$tabla=obtieneEmpleadosFactura($codigo,$codigoContrato,$fecha,$codigosReconocimientosProcesados,$res);
	
	echo "<h3>Factura $i: ID $codigo del ".formateaFechaWeb($fecha)."</h3>$tabla<br /><br />";
	
	$i++;

}

cierraBD();

if($res){
	echo "Todo ha ido bien :D";
}
else{
	echo "Algo ha fallado: ".mysql_errno();
}

echo "
</body>
</html>";


function obtieneEmpleadosFactura($codigoFactura,$codigoContrato,$fechaFactura,&$codigosReconocimientosProcesados,&$resPrincipal){
	$res="
	<table class='tablaEmpleados'>
		<tr>
			<th>Fecha reco</th>
			<th>Empleado</th>
			<th>DNI</th>
			<th>Codigo reconocimiento</th>
		</tr>";

	$consulta=consultaBD("SELECT codigo, codigoEmpleado FROM reconocimientos_medicos_en_facturas WHERE codigoFacturaReconocimientoMedico='$codigoFactura';");
	
	$i=0;
	while($datos=mysql_fetch_assoc($consulta)){

		/*
			Como los reconocimientos médicos no está vinculados directamente con las facturas, lo que hago es pasarle la fecha de la factura
			por si diese la casualidad de que un mismo empleado se realiza más de un reconocimiento médico en el mismo contrato (caso factura RM-48)
		*/
		$empleado=consultaBD("SELECT nombre, apellidos, dni, reconocimientos_medicos.fecha, reconocimientos_medicos.codigo AS codigoReconocimiento
							  FROM empleados INNER JOIN reconocimientos_medicos ON empleados.codigo=reconocimientos_medicos.codigoEmpleado
							  WHERE reconocimientos_medicos.codigoContrato='$codigoContrato' AND reconocimientos_medicos.eliminado='NO'
							  AND reconocimientos_medicos.fecha<='$fechaFactura' AND empleados.codigo='".$datos['codigoEmpleado']."';",false,true);

		if($empleado){
			$claseFila='';
			if($i%2==0){
				$claseFila="class='filaPar'";
			}

			if(!in_array($empleado['codigoReconocimiento'],$codigosReconocimientosProcesados)){
				array_push($codigosReconocimientosProcesados,$empleado['codigoReconocimiento']);

				$resPrincipal=$resPrincipal && consultaBD("UPDATE reconocimientos_medicos_en_facturas SET codigoReconocimientoMedico='".$empleado['codigoReconocimiento']."' WHERE codigo='".$datos['codigo']."';");
			}
			else{
				$empleado['codigoReconocimiento'].=' - REPETIDO';
				$resPrincipal=$resPrincipal && consultaBD("DELETE FROM reconocimientos_medicos_en_facturas WHERE codigo='".$datos['codigo']."';");
			}

			$res.="
			<tr $claseFila>
				<td class='segundaColumna'>".formateaFechaWeb($empleado['fecha'])."</td>
				<td class='primeraColumna'>".$empleado['nombre']." ".$empleado['apellidos']."</td>
				<td class='segundaColumna'>".$empleado['dni']."</td>
				<td class='segundaColumna'>".$empleado['codigoReconocimiento']."</td>
			</tr>";

			$i++;
		}
	}

	$res.="</table>";

	if($i==0){
		$res='';
	}
	
	return $res;
}