<?php
include_once('funciones.php');
conexionBD();
$listado=consultaBD('SELECT * FROM evaluacion_general');
while($item=mysql_fetch_assoc($listado)){
	//echo '- Evaluación: '.$item['codigo'].'<br/>';
	$riesgos=consultaBD('SELECT * FROM riesgos_evaluacion_general WHERE codigoEvaluacion='.$item['codigo']);
	while($r=mysql_fetch_assoc($riesgos)){
		$primer=true;
		$codigos=array();
		$x=0;
		$codigos[$x]=$r['codigo'];
		//echo '--- Riesgo: '.$r['codigo'].'<br/>';
		$descripciones=consultaBD('SELECT * FROM evaluacion_descripciones WHERE codigoRiesgo='.$r['codigo']);
		while($d=mysql_fetch_assoc($descripciones)){
			//echo '------ Descripción '.$d['codigo'].'<br/>';
			if($primer){
				$sql='UPDATE riesgos_evaluacion_general SET descripcion2="'.$d['descripcion'].'" WHERE codigo='.$codigos[$x];
				$res=consultaBD($sql);
			} else {
				$sql='INSERT INTO riesgos_evaluacion_general VALUES (NULL,'.$r['codigoRiesgo'].',"'.$r['descripcionRiesgo'].'","'.$r['probabilidad'].'","'.$r['consecuencias'].'","'.$r['prioridad'].'","'.$r['total'].'","'.$r['graficoBarra'].'",'.$r['codigoEvaluacion'].',"'.$d['descripcion'].'","'.$r['medida'].'");';
				$res=consultaBD($sql);
				$x++;
				$codigos[$x]=mysql_insert_id();
			}
			//echo '------ '.$sql.'<br/>';
			$imagenes=consultaBD('SELECT * FROM evaluacion_imagenes WHERE codigoDescripcion='.$d['codigo']);
			while($i=mysql_fetch_assoc($imagenes)){
				//echo '--------- Imagen '.$i['codigo'].'<br/>';
				$sql='UPDATE evaluacion_imagenes SET codigoDescripcion="'.$codigos[$x].'" WHERE codigo='.$i['codigo'];
				//echo '--------- '.$sql.'<br/>';
				$res=consultaBD($sql);
			}
			$primer=false;

		}
		$x=0;
		$primer=true;
		$medidas=consultaBD('SELECT medidas_riesgos_evaluacion_general.* FROM medidas_riesgos_evaluacion_general INNER JOIN gestion_evaluacion_general ON medidas_riesgos_evaluacion_general.codigoGestion=gestion_evaluacion_general.codigo WHERE codigoEvaluacion='.$r['codigo']);
		while($m=mysql_fetch_assoc($medidas)){
			//echo '------ Medida '.$m['codigo'].'<br/>';
			if($primer){
				$sql='UPDATE riesgos_evaluacion_general SET medida="'.$m['recomendacion'].'" WHERE codigo='.$codigos[$x];
				$res=consultaBD($sql);
			} else {
				$x++;
				if(isset($codigos[$x])){
					$sql='UPDATE riesgos_evaluacion_general SET medida="'.$m['recomendacion'].'" WHERE codigo='.$codigos[$x];
					$res=consultaBD($sql);
				} else {
					$sql='INSERT INTO riesgos_evaluacion_general VALUES (NULL,'.$r['codigoRiesgo'].',"'.$r['descripcionRiesgo'].'","'.$r['probabilidad'].'","'.$r['consecuencias'].'","'.$r['prioridad'].'","'.$r['total'].'","'.$r['graficoBarra'].'",'.$r['codigoEvaluacion'].',"'.$r['descripcion2'].'","'.$m['recomendacion'].'");';
					$res=consultaBD($sql);
					$codigos[$x]=mysql_insert_id();
				}
			}
			//echo '------ '.$sql.'<br/>';
			$imagenes=consultaBD('SELECT * FROM evaluacion_medidas_imagenes WHERE codigoMedida='.$m['codigo']);
			while($i=mysql_fetch_assoc($imagenes)){
				//echo '--------- Imagen '.$i['codigo'].'<br/>';
				$sql='UPDATE evaluacion_medidas_imagenes SET codigoMedida="'.$codigos[$x].'" WHERE codigo='.$i['codigo'];
				//echo '--------- '.$sql.'<br/>';
				$res=consultaBD($sql);
			}
			$primer=false;
		}
	}
	//echo '--------------<br/><br/>';
}
cierraBD();
?>