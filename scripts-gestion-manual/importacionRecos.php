<?php
include_once('config.php');//Carga del archivo de configuración
include_once('../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('funciones.php');//Carga de las funciones globales
session_start();

//Carga de PHP Excel
require_once('../api/phpexcel/PHPExcel.php');
require_once('../api/phpexcel/PHPExcel/Reader/Excel2007.php');
require_once('../api/phpexcel/PHPExcel/Writer/Excel2007.php');

$res=true;


$objReader = new PHPExcel_Reader_Excel2007();

$documento=$objReader->load('reconocimientos.xlsx');
$documento->setActiveSheetIndex(0);

$fila=2;

conexionBD();

$valores=obtieneValorFila($documento,$fila);

while($valores['A']!=''){
	
	extract($valores);

	$query="INSERT INTO reconocimientos_medicos VALUES(
				NULL,
				$C,
				NULL,
				NULL,
				'NO',
				'$E',
				'',
				'$A',
				'$CS',
				'$CT',
				'',
				'',
				'',
				'',
				'$CU',
				'$AP',
				'$U',
				'$CO',
				'$F',
				'$H',
				'$K',
				'$L',
				'$O',
				'$P',
				'$T',
				'$J',
				'',
				'$CP',
				'$M',
				'$Q',
				'$bebidasEstimulantes',
				'$checkTratamientoHabitual',
				'$CR',
				'$checkVacunaciones',
				'',
				'',
				'',
				'',
				'$AB',
				'$AC',
				'$AD',
				'$DU',
				'',
				'',
				'$fracturas',
				'$V',
				'$AA',
				'$CW',
				'',
				'$Z',
				'$CY',
				'$minusvalia',
				'$CV',
				'$bajasProlongadas',
				'$CZ',
				'$DN',
				'$AE',
				'$DR',
				'$DA',
				'$DB',
				'$DC',
				'$AH',
				'$AL',
				'$AG',
				'$DM',
				'$AK',
				'$DD',
				'$BG',
				'$BH',
				'$BJ',
				'$BI',
				'$DJ',
				'$DK',
				'$DL',
				'$BL',
				'$BM',
				'$BN',
				'$BO',
				'$BP',
				'$BQ',
				'$BR',
				'$BS',
				'$BT',
				'$BU',
				'$BV',
				'$BW',
				'$BH',
				'$BF',
				'',
				'$CQ',
				'$AX',
				'$AY',
				'$AZ',
				'$BX',
				'$observacionesAudiometria',
				'$CA',
				'$CB',
				'$CC',
				'$CD',
				'$CE',
				'$CF',
				'$CG',
				'$CH',
				'$CI',
				'$CJ',
				'$CK',
				'$CL',
				'$CM',
				'$CN',
				'$BA',
				'$AR',
				'SI',
				'$AS',
				'$AT',
				'',
				'',
				'$DO',
				'$AU',
				'$DS',
				'NO',
				'$DF',
				'$DE',
				'$DH',
				'$AN',
				'$AN',
				'$AO',
				NULL,
				'',
				'$recomendaciones',
				'',
				'NO',
				'SI',
				'NO',
				'',
				'',
				'',
				'',
				'$DI',
				'NO',
				'',
				'',
				'',
				'',
				'$DG',
				'SI',
				NULL,
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'$AM',
				'',
				'',
				'',
				'',
				'',
				'NO',
				'NO',
				'$I',
				'$X',
				'$AD',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'SI',
				'',
				''
			);";


	$res=$res && consultaBD($query);

	if($res && $DQ!='' && $DQ>0){
		$codigoReconocimiento=mysql_insert_id();

		$res=$res && consultaBD("INSERT INTO bebidas_estimulantes_reconocimiento_medico VALUES(NULL,'$DQ','Café',$codigoReconocimiento);");
	}

	$fila++;
	
	$valores=obtieneValorFila($documento,$fila);
}

cierraBD();


if($res){
	echo "Todo ha ido bien :). Reconocimientos registrados: ".($fila-2);
}
else{
	echo "Algo ha fallado :(<br /><br />".mysql_error();
}


function obtieneValorFila($documento,$fila){
	$res=array();

	//Recorro todas las columnas y meto sus valores en un array con la columna como índice
	foreach(getColumnRange('A','ED') as $columna){
		
		$coordenadas=$columna.$fila;

		$res[$columna]=obtieneValorCelda($documento,$coordenadas);

	}

	//Parte de formateo de datos
	if($res['A']!=''){
	
		//Fecha
		$res['A']=PHPExcel_Shared_Date::ExcelToPHP($res['A']);
		$res['A']=date("Y-m-d",$res['A']);

		//Código cliente
		$res['B']=obtieneCodigoClienteDesdeNombre($res['B']);

		//Trabajador
		$res['C']=obtieneCodigoTrabajadorDesdeNombre($res['C'],$res['D'],$res['F'],$res['DW'],$res['DX'],$res['DY'],$res['B']);

		//Tipo reconocimiento
		$res['E']=formateaTipoReconocimiento($res['E']);

		//Bebidas estimulantes
		if($res['DQ']!='' && $res['DQ']>0){
			$res['bebidasEstimulantes']='SI';
		}
		else{
			$res['bebidasEstimulantes']='NO';	
		}

		//Tratamiento habitual
		if($res['CR']!=''){
			$res['checkTratamientoHabitual']='SI';
		}
		else{
			$res['checkTratamientoHabitual']='NO';	
		}

		//Vacunas
		if($res['Y']!=''){
			$res['checkVacunaciones']='SI';
		}
		else{
			$res['checkVacunaciones']='NO';
		}

		//Ejercicio físico
		$res['AB']=formateaEjercicioFisico($res['AB']);

		//Alimentación
		$res['AC']=formateaAlimentacion($res['AC']);

		//Sueño
		$res['AD']=formateaSuenio($res['AD']);

		//Fracturas
		if($res['V']=='' || substr_count(strtolower($res['V']),'no refiere')>0){
			$res['fracturas']='NO';
		}
		else{
			$res['fracturas']='SI';
		}

		//Minusvalía
		$res['CY']=strtoupper($res['CY']);
		if($res['CY']!='NO'){
			$res['minusvalia']=str_replace('SI','',$res['CY']);
		}
		else{
			$res['minusvalia']='';
		}

		//TODO: EPIs?

		//Bajas prolongadas
		if($res['CZ']!='' && strtolower($res['CZ']!='no')){
			$res['bajasProlongadas']='SI';
		}
		else{
			$res['bajasProlongadas']='NO';
		}

		//Visión
		$res['BG']=formateaCampoSiNoOriginal($res['BG']);
		$res['BJ']=formateaCampoSiNoOriginal($res['BJ']);
		$res['BI']=formateaCampoSiNoOriginal($res['BI']);
		$res['DL']=str_replace('.','',$res['DL']);

		//Espirometría
		$res['BF']=formateaCampoSiNoOriginal($res['BF']);

		//Audiometría
		$res['BX']=formateaCampoSiNoOriginal($res['BX']);
		$res['observacionesAudiometria']=$res['BY'].' '.$res['BZ'];

		//Electrocardiograma
		$res['BA']=formateaCampoSiNoOriginal($res['BA']);
		$res['DO']=formateaCampoSiNoOriginal($res['DO']);

		//Radiología
		$res['DF']=formateaCampoSiNoOriginal($res['DF']);

		//Dinamometría
		$res['DH']=formateaCampoSiNoOriginal($res['DH']);

		//Debe acudir a su médico
		$res['AO']=formateaCampoSiNoOriginal($res['AO']);

		//Recomendaciones
		$res['recomendaciones']=$res['AM']."\n".$res['DZ']."\n".$res['EA']."\n".$res['EB']."\n".$res['EC']."\n".$res['ED'];



	}


	return $res;
}

function obtieneValorCelda($documento,$coordenadas){
	return $documento->getActiveSheet()->getCell($coordenadas)->getValue();
}


function obtieneCodigoClienteDesdeNombre($nombreCliente){
	$res='NULL';
	
	$cliente=consultaBD("SELECT codigo FROM clientes WHERE EMPNOMBRE='$nombreCliente' OR EMPMARCA='$nombreCliente';",false,true);
	
	if($cliente){
		$res=$cliente['codigo'];
	}
	else{
		echo "Atención: cliente <strong>$nombreCliente</strong> no encontrado en el sistema. Se registra...<br />";

		$insercion=consultaBD("INSERT INTO clientes(codigo,activo,EMPNOMBRE,EMPMARCA,eliminado) VALUES(NULL,'SI','$nombreCliente','$nombreCliente','NO')");
		if($insercion){
			$res=mysql_insert_id();
		}

	}

	return $res;
}


function obtieneCodigoTrabajadorDesdeNombre($nombreCompleto,$dni,$direccion,$localidad,$provincia,$cp,$codigoCliente){
	$res='NULL';
	
	$nombreCompleto=explode(', ',$nombreCompleto);
	$apellidos=$nombreCompleto[0];
	$nombre=$nombreCompleto[1];

	$empleado=consultaBD("SELECT codigo FROM empleados WHERE nombre='$nombre' AND apellidos='$apellidos';",false,true);
	
	if($empleado){
		$res=$empleado['codigo'];
	}
	else{
		echo "Atención: empleado <strong>$nombre $apellidos</strong> no encontrado en el sistema. Se registra...<br />";

		$insercion=consultaBD("INSERT INTO empleados(codigo,codigoCliente,dni,nombre,apellidos,direccion,cp,ciudad,provincia,eliminado,aprobado) 
							   VALUES(NULL,$codigoCliente,'$dni','$nombre','$apellidos','$direccion','$cp','$localidad','$provincia','NO','SI')");
		if($insercion){
			$res=mysql_insert_id();
		}
		
	}

	return $res;
}


function formateaTipoReconocimiento($tipoReconocimiento){

	if(substr_count(strtolower($tipoReconocimiento),'baja')>0){
		$res='Tras baja de larga duración';
	}
	elseif($tipoReconocimiento='PERIODICO'){
		$res='Periódico';
	}
	elseif($tipoReconocimiento=='INICIAL'){
		$res='Inicial';
	}
	else{
		$res='Otros';
	}

	return $res;
}


function formateaEjercicioFisico($ejercicio){

	$ejercicio=strtolower($ejercicio);

	if(substr_count($ejercicio,'no realiza')>0){
		$res='No realiza';
	}
	elseif(substr_count($ejercicio,'realiza')>0){
		$res='Realiza ejercicio físico';
	}
	else{
		$res='Desconocido';
	}

	return $res;
}

function formateaAlimentacion($alimentacion){
	$res=$alimentacion;

	if(substr_count($alimentacion,'equilibrada-variada')>0){
		$res='Su alimentación es equilibrada-variada';
	}
	elseif($alimentacion=='Control de Peso'){
		$res='Su alimentación es hipocalorica';
	}

	return $res;
}


function formateaSuenio($suenio){
	$res=$suenio;

	if(substr_count($suenio,'Bien: 6 ')>0){
		$res='Bien: 6 o más horas';
	}

	return $res;
}

function formateaCampoSiNoOriginal($valor){
	$res='NO';

	if(substr_count(strtoupper($valor),'S')>0){
		$res='SI';
	}

	return $res;
}

//Funciones de https://www.php.net/manual/es/function.range.php
function getColumnRange($min,$max){
      $pointer=strtoupper($min);
      $output=array();
      while(positionalcomparison($pointer,strtoupper($max))<=0){
         array_push($output,$pointer);
         $pointer++;
      }
      return $output;
}

function positionalcomparison($a,$b){
   $a1=stringtointvalue($a); $b1=stringtointvalue($b);
   if($a1>$b1)return 1;
   else if($a1<$b1)return -1;
   else return 0;
}

/*
* e.g. A=1 - B=2 - Z=26 - AA=27 - CZ=104 - DA=105 - ZZ=702 - AAA=703
*/
function stringtointvalue($str){
   $amount=0;
   $strarra=array_reverse(str_split($str));

   for($i=0;$i<strlen($str);$i++){
      $amount+=(ord($strarra[$i])-64)*pow(26,$i);
   }
   return $amount;
}