<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de incidencias

function operacionesCuestionario(){
	$res=true;

	if(isset($_POST['nombre'])){
		$res=gestionaIncidencia();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('incidencias');
	}

	mensajeResultado('nombre',$res,'Incidencia');
    mensajeResultado('elimina',$res,'Incidencia', true);
}

function gestionaIncidencia(){
	$res=true;
	if(isset($_POST['codigo'])){
		$res=actualizaDatos('incidencias');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('incidencias');
	}
	if($_POST['enviarMail']=='SI'){
		$res=notificaIncidencia();
	}
	return $res;
}

function notificaIncidencia(){
	$res=true;
	
	$mensaje="Se ha notificado una nueva incidencia<br><br/><br/>
	- Persona que notifica: <b>".$_POST['persona']." </b><br/>
	- Nombre de la incidencia: <b>".$_POST['nombre']." </b><br/>
	- Fecha: <b>".$_POST['fecha']." </b><br/>
	- Descripción: <b>".$_POST['descripcion']." </b>";
	$correos='tecnico5@anescoprl.es, tecnico6@anescoprl.es';
	
	if (!enviaEmail($correos, 'Notificación de nueva incidencia', $mensaje)){
		$res=false;
	}

	if($res){
		$res=consultaBD('INSERT INTO correos VALUES(NULL,"'.$correos.'","'.fechaBD().'","'.horaBD().'","Notificación de nueva incidencia","'.$mensaje.'","","'.$_SESSION['codigoU'].'","INCIDENCIA",NULL,"")',true);
	}
	
	return $res;
}


function gestionIncidencia(){
	operacionesCuestionario();
	
	abreVentanaGestionConBotones('Gestión de Incidencias','index.php');
	$datos=compruebaDatos('incidencias');
	campoOculto('NO','enviarMail');
	campoTexto('nombre','Nombre',$datos);
	campoFecha('fecha','Fecha',$datos);
	areaTexto('descripcion','Descripción',$datos);
	campoTexto('persona','Persona que notífica',$datos);
	campoSelect('departamento','Departamento',array('Administración','Comercial','Técnico'),array('ADMINISTRACION','COMERCIAL','TECNICO'),$datos);
	campoSelect('prioridad','Prioridad',array('Normal','Baja','Alta'),array('NORMAL','BAJA','ALTA'),$datos);
	campoSelect('estado','Estado',array('Pendiente','En proceso','Resuelta'),array('PENDIENTE','ENPROCESO','RESUELTA'),$datos);
	echo "<div id='cajaFecha'>";
	campoFecha('fechaResolucion','Fecha de resolución',$datos);
	echo "</div>";

	cierraVentanaGestion('index.php');
}


function imprimeIncidencias(){
	$consulta=consultaBD("SELECT * FROM incidencias",true);
	while($datos=mysql_fetch_assoc($consulta)){
		$departamento=obtieneDepartamentoIncidencia($datos['departamento']);
		$prioridad=obtienePrioridadIncidencia($datos['prioridad']);
		$estado=obtieneEstadoIncidencia($datos['estado']);

		echo "<tr>
				<td> ".$datos['nombre']." </td>
				<td> ".formateaFechaWeb($datos['fecha'])." </td>
				<td> ".$datos['persona']." </td>
				<td> $departamento </td>
				<td> $prioridad </td>
				<td> $estado </td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	  	</td>
			 </tr>";
	}
}

function obtieneDepartamentoIncidencia($departamento){
	$departamentos=array('ADMINISTRACION'=>'Administración','COMERCIAL'=>'Comercial','TECNICO'=>'Técnico');
	return $departamentos[$departamento];
}

function obtienePrioridadIncidencia($prioridad){
	$prioridades=array('NORMAL'=>"<span class='label label-info'>Normal</span>",'ALTA'=>"<span class='label label-danger'>Alta</span>",'BAJA'=>"<span class='label'>Baja</span>");
	return $prioridades[$prioridad];
}

function obtieneEstadoIncidencia($estado){
	$estados=array('PENDIENTE'=>"<span class='label label-danger'>Pendiente</span>",'RESUELTA'=>"<span class='label label-success'>Resuelta</span>",'ENPROCESO'=>"<span class='label label-warning'>En proceso</span>");
	return $estados[$estado];
}




function generaDatosGraficoIncidencias(){
	$datos=array('PENDIENTE'=>0,'ENPROCESO'=>0,'RESUELTA'=>0);

	$consulta=consultaBD("SELECT * FROM incidencias;",true);	
	while($datosConsulta=mysql_fetch_assoc($consulta)){
		$datos=clasificaEstadoIncidencia($datosConsulta['estado'],$datos);
	}

	return $datos;
}

function clasificaEstadoIncidencia($estado,$datos){
	if(isset($datos[$estado])){
		$datos[$estado]++;
	}
	else{
		$datos[$estado]=1;	
	}

	return $datos;
}

//Fin parte de incidencias