<?php
  $seccionActiva=7;
  include_once("../cabecera.php");
  gestionIncidencia();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $('.selectpicker').selectpicker();

    oyenteResolucion();
    $('select[name=estado]').change(function(){
    	oyenteResolucion();
    });

    $(':submit').unbind().click(function(e){
      e.preventDefault();
      res=confirm("¿Enviar notificación de nueva incidencia?");
      if(res==true){
        $('#enviarMail').val('SI');
      }
      $('form').submit();
    });
  });

  function oyenteResolucion(){
  	if($('select[name=estado]').val()=='RESUELTA'){
  		$('#cajaFecha').removeClass('hide');
  	}
  	else{
  		$('#cajaFecha').addClass('hide');
  	}
  }
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>