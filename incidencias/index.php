<?php
  $seccionActiva=7;
  include_once('../cabecera.php');
    
  operacionesCuestionario();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de incidencias:</h6>
                  <div id="big_stats" class="cf">
                    <canvas id="graficoCircular" class="chart-holder" height="250" width="538"></canvas>
                    <div class="leyenda" id="leyenda"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Incidencias</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva incidencia</span> </a>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>


      <div class="span12">        
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Incidencias registradas</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Nombre </th>
                  <th> Fecha de alta </th>
                  <th> Persona que notifica </th>
                  <th> Departamento </th>
                  <th> Prioridad </th>
                  <th> Estado </th>
                  <th></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

                <?php
                  imprimeIncidencias();
                ?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content --> 
        </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/chart.js" type="text/javascript"></script>

<script type="text/javascript">
    <?php
      $datos=generaDatosGraficoIncidencias();
    ?>
    var datosGrafico=[
        {
            value: <?php echo $datos['PENDIENTE']; ?>,
            color: "#B02B2C",
            highlight:"#C13C3D",
            label: 'Pendientes'
        },
        {
            value: <?php echo $datos['ENPROCESO']; ?>,
            color: "#f89406",
            highlight: "#f9A517",
            label: 'En proceso'
        },
        {
            value: <?php echo $datos['RESUELTA']; ?>,
            color: "#6BBA70",
            highlight: "#7CCB81",
            label: 'Resueltas'
        }
      ];

    var grafico=new Chart(document.getElementById("graficoCircular").getContext("2d")).Pie(datosGrafico);    
</script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/eventoGrafico.js"></script>


<!-- /contenido --></div>

<?php include_once('../pie.php'); ?>