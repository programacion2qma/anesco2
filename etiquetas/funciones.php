<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de etiquetas

function estadisticasGestorDocumental(){
	$res=array();

	conexionBD();
	
	$datos=consultaBD("SELECT COUNT(codigo) AS total FROM espaciosTrabajo;",false,true);
	$res['espaciosTrabajo']=$datos['total'];

	$datos=consultaBD("SELECT COUNT(codigo) AS total FROM documentos;",false,true);
	$res['documentos']=$datos['total'];
	
	cierraBD();

	return $res;
}

function operacionesEtiquetas(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('etiquetas');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('etiquetas');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('etiquetas');
	}

	mensajeResultado('nombre',$res,'Etiqueta');
	mensajeResultado('elimina',$res,'Etiqueta', true);
}



function gestionEtiquetas(){
	operacionesEtiquetas();

	abreVentanaGestion('Gestión de Etiquetas','?','','icon-edit','margenAb');
	$datos=compruebaDatos('etiquetas');

	campoTexto('nombre','Nombre',$datos);
	campoTexto('color','Color',$datos,'input-mini');

	cierraVentanaGestion('index.php',true);
}

function imprimeEtiquetas(){
	global $_CONFIG;

	$consulta=consultaBD("SELECT * FROM etiquetas;",true);
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td>".$datos['nombre']."</td>
				<td><span class='label colorEtiqueta' style='background-color:".$datos['color']."'>".$datos['color']."</span></td>
				<td class='centro'>
					<a href='".$_CONFIG['raiz']."etiquetas/gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-edit'></i> Editar</i></a>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
}

//Fin parte de etiquetas