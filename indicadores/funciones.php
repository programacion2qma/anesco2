<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

function operacionesIndicadores(){
	$res=true;

  	if(isset($_POST['codigo'])){
  		if(isset($_POST['valorFinal'])){
    		$res=actualizaMapa();
  		} else {
  			$res=actualizaDatos('indicadores');
  		}
  	}
  	elseif(isset($_POST['valorFinal'])){
    	$res=insertaMapa();
  	}
  	elseif(isset($_POST['nombre'])){
    	$res=insertaDatos('indicadores');
  	}
  	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    	$res=eliminaDatos('mapaProcesos');
  	}
  	elseif(isset($_GET['eliminaProceso'])){
    	$_POST['codigo0'] = $_GET['codigo'];
    	$_POST['codigoMapa']=obtieneMapa($_GET['codigo']);
    	$res=eliminaDatos("indicadores");
    	
  	}

	mensajeResultado('descripcion',$res,'Indicador');
    mensajeResultado('elimina',$res,'Indicador', true);
}

function insertaMapa(){
	$res=insertaDatos('mapaProcesos');
	$_POST['codigo'] =mysql_insert_id();
	$res=insertaValores($_POST['codigo']);
	return $res;
}

function actualizaMapa(){
	$res=actualizaDatos('mapaProcesos');
	$res=insertaValores($_POST['codigo']);
	return $res;
}

function insertaValores($mapa){
	$res = true;
	conexionBD();

	$datos=arrayFormulario();
	$consulta=consultaBD("DELETE FROM valores_mapa WHERE codigoMapa='$mapa';");
	$i=0;
	$indicador = datosRegistro('mapaProcesos',$mapa);
	$tipo = datosRegistro('indicadores',$indicador['indicador']);
	if($indicador['valor_real'] == 'ESTATICO'){
		while(isset($datos['valor'.$i])){
			$valor = $datos['valor'.$i] == '' ? 0 : $datos['valor'.$i];
			$check = isset($datos['checkValor'.$i]) ? 'SI':'NO';
			$res=$res && consultaBD("INSERT INTO valores_mapa VALUES(NULL, '$mapa', '$valor','','','$check');");
			$i++;
		}
	} else {
		while(isset($datos['valorN'.$i])){
			$valorN = $datos['valorN'.$i] == '' ? 0 : $datos['valorN'.$i];
			$valorD = $datos['valorD'.$i] == '' ? 0 : $datos['valorD'.$i];
			$check = isset($datos['checkValor'.$i]) ? 'SI':'NO';
			$res=$res && consultaBD("INSERT INTO valores_mapa VALUES(NULL, '$mapa', '', '$valorN', '$valorD', '$check');");
			$i++;
		}
	}

	cierraBD();
	return $res;
}

/*function imprimeIndicadores($area){
	$consulta=consultaBD("SELECT codigo, indicador, descripcion, tolerancia, condicion, valorFinal, numerador, denominador, frecuencia FROM mapaProcesos WHERE indicador='$area' ORDER BY descripcion",true);
	$frecuencias=array('' => '',1 =>'Anual',2 =>'Semestral',4 =>'Trimestral',6 => 'Bimestral',12 => 'Mensual');
	while($datos=mysql_fetch_assoc($consulta)){
		$tipo = consultaBD("SELECT valor_real FROM indicadores WHERE codigo=".$datos['indicador'],true,true);

		if ($tipo['valor_real'] == 'ESTATICO'){
			$valorReal=consultaBD("select count(codigo) as numero, sum(valor) as total FROM valores_mapa WHERE codigoMapa=".$datos['codigo']." AND checkValor='SI'",true,true);
			$valorReal = round($valorReal['total'] / $valorReal['numero']);
			
			$tdValor = "<td> ".$valorReal." </td>";
			$indicador=calculaIndicador($datos['condicion'],$datos['tolerancia'],$datos['valorFinal'],$valorReal);
		} else {
			$valorReal=consultaBD("select count(codigo) as numero, sum(valorN) as numerador, sum(valorD) denominador FROM valores_mapa WHERE codigoMapa=".$datos['codigo']." AND checkValor='SI'",true,true);
			$tdValor = "<td> ".$valorReal['numerador']." / ".$valorReal['denominador']."</td>";
			$valorReal = (100 * $valorReal['numerador']) / $valorReal['denominador'];
			$valorFinal = (100 * $datos['numerador']) / $datos['denominador'];
 			$datos['valorFinal'] = $datos['numerador']." / ".$datos['denominador'];
			$indicador=calculaIndicador($datos['condicion'],$datos['tolerancia'],$valorFinal,$valorReal);
		}

		echo "
		<tr>
			<td> ".$datos['descripcion']."</td>"
			.$tdValor."
			<td class='centro'> ".$datos['condicion']."</td>
			<td> ".$datos['valorFinal']." </td>
			<td> ".$frecuencias[$datos['frecuencia']]." </td>
        	<td class='centro'> <img src='../img/indicador".$indicador.".png' /></td>
        	<td class='centro'>
        		<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-edit'></i> Ver</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
    	</tr>";
	}
}*/



function listadoIndicadores($area){
	$ejercicio=obtieneEjercicioParaWhere();
	$frecuencias=array('' => '',1 =>'Anual',2 =>'Semestral',4 =>'Trimestral',6 => 'Bimestral',12 => 'Mensual');

	$columnas=array(
		'descripcion',
		'(numerador/denominador)',
		'condicion',
		'valorFinal',
		'frecuencia',
		'tolerancia',
		'codigo',
		'codigo'
	);

	$having=obtieneWhereListado("HAVING YEAR(fechaFinIndicador)='$ejercicio' AND indicador='$area'",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT codigo, indicador, descripcion, tolerancia, condicion, valorFinal, numerador, denominador, frecuencia, fechaFinIndicador,valor_real
			FROM mapaProcesos 
			$having";

	
	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		obtieneValorRealConIconoDeIndicador($datos);

		$fila=array(
			$datos['descripcion'],
			$datos['valorReal'],
			"<div class='centro'>".$datos['condicion']."</div>",
			$datos['valorFinal'],
			$frecuencias[$datos['frecuencia']],
        	"<div class='centro'> <img src='../img/indicador".$datos['indicador'].".png' /></div>",
        	"<div class='centro'><a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a></div>",
        	"<div class='centro'><input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' /></div>",
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}
	cierraBD();

	echo json_encode($res);
}


function obtieneValorRealConIconoDeIndicador(&$datos){
	//$tipo=consultaBD("SELECT valor_real FROM indicadores WHERE codigo=".$datos['indicador'],false,true);
	$tipo=consultaBD("SELECT valor_real FROM mapaProcesos WHERE indicador=".$datos['indicador'],false,true);

	if($datos['valor_real'] == 'ESTATICO'){
		$valorReal=consultaBD("select count(codigo) as numero, sum(valor) as total FROM valores_mapa WHERE codigoMapa=".$datos['codigo']." AND checkValor='SI'",false,true);
		
		if($valorReal['numero']==0){
			$valorReal['numero']=1;
		}

		$valorReal = round($valorReal['total'] / $valorReal['numero']);
		
		$datos['indicador']=calculaIndicador($datos['condicion'],$datos['tolerancia'],$datos['valorFinal'],$valorReal);
		$datos['valorReal']=$valorReal;
	} 
	else{
		$valorReal=consultaBD("select count(codigo) as numero, sum(valorN) as numerador, sum(valorD) denominador FROM valores_mapa WHERE codigoMapa=".$datos['codigo']." AND checkValor='SI'",false,true);

		if($valorReal['denominador']==0){
			$valorReal['denominador']=1;
		}

		$tdValor = "<td> ".$valorReal['numerador']." / ".$valorReal['denominador']."</td>";
		$valorReal = $valorReal['numerador'] / $valorReal['denominador'];
		$valorFinal = $datos['numerador'] / $datos['denominador'];
		
		$datos['valorFinal']=$datos['numerador']." / ".$datos['denominador'];
		$datos['indicador']=calculaIndicador($datos['condicion'],$datos['tolerancia'],$valorFinal,$valorReal);
		$datos['valorReal']=$valorReal;
	}
}



function gestionIndicadores(){
	operacionesIndicadores();

	$area=obtieneAreaIndicador();
  	$nombreArea=obtieneNombreAreaIndicador($area);
  	$codigoMapa=obtieneMapa($area);
  	
	abreVentanaGestion('Nuevo indicador para el área ' .$nombreArea,'index.php');
	$datos=compruebaDatos('mapaProcesos');
	campoOculto($codigoMapa,'codigoMapa');
	if($datos){
		$indicador = $datos['indicador'];
	} else{
		$indicador = $area;
	}

	campoSelectConsulta('indicador','Área',"SELECT codigo, nombre AS texto FROM indicadores",$indicador);
	campoRadio('valor_real','Valor real',$datos,'ESTATICO',array('Estático','Fórmula'),array('ESTATICO','FORMULA'));
    campoTexto('descripcion','Descripción del indicador',$datos,'span5');
    campoFecha('fechaInicioIndicador','Fecha inicio',$datos);
    campoFecha('fechaFinIndicador','Fecha fin',$datos);

	campoSelect('condicion','Condición',array('IGUAL','MAYOR O IGUAL','MAYOR','MENOR O IGUAL','MENOR'),array('==','>=','>','<=','<'),$datos,'selectpicker span2 show-tick');
	//$tipo = consultaBD("SELECT valor_real FROM indicadores WHERE codigo=".$indicador,true,true);
	campoOculto($datos['valor_real'],'tipoIndicador');
    /*if ($datos['valor_real'] == 'ESTATICO'){
    	campoNumero('valorFinal','Valor final esperado',$datos);
    	campoOculto('0','numerador');
    	campoOculto('0','denominador');
	} else {
		campoNumeroIndicador('numerador','Valor final esperado','left',$datos);
		campoNumeroIndicador('denominador','','none',$datos);
		campoOculto('0','valorFinal');
	}*/
	echo "<div id='estatico' style='display:block'>";
    	campoNumero('valorFinal','Valor final esperado',$datos);
	echo "</div>";	
	echo "<div id='formula' style='display:none'>";
		campoNumeroIndicador('numerador','Valor final esperado','left',$datos);
		campoNumeroIndicador('denominador','','none',$datos);
	echo "</div>";

	campoNumero('tolerancia','Tolerancia',$datos);
    campoSelect('frecuencia','Frecuencia',array('','Mensual','Bimestral','Trimestral','Semestral','Anual'),array('0','12','6','4','2','1'),$datos);
    if($datos){
    	$paraAjax =consultaBD("SELECT COUNT(codigo) AS codigo FROM valores_mapa WHERE codigoMapa=".$datos['codigo'],true,true);
    	campoOculto($paraAjax['codigo'],'paraAjax','0');
	}
    echo "<div id='inputsFrecuencia'></div>";
    areaTexto('observaciones','Observaciones',$datos);

	cierraVentanaGestion('indicadoresArea.php?area='.$indicador);
}

function gestionMapaProcesos($enlace){
	operacionesIndicadores();

	abreVentanaGestion('Gestión de areas para el Mapa de procesos','?');
	$datos=compruebaDatos('indicadores');
	if($datos){
		$codigoMapa=datosRegistro('tipos_procesos',$datos['tipo']);
		$codigoMapa=$codigoMapa['codigoMapa'];
	} else {
		$codigoMapa=$_GET['codigoMapa'];
	}
	campoOculto($codigoMapa,'codigoMapa');
	campoTexto('nombre','Nombre',$datos);
	campoSelectConsulta('tipo','Tipo de proceso','SELECT codigo, nombre AS texto FROM tipos_procesos WHERE codigoMapa='.$codigoMapa,$datos);
	//campoRadio('valor_real','Valor real',$datos,'ESTATICO',array('Estático','Fórmula'),array('ESTATICO','FORMULA'));
	
	 abreColumnaCampos();
	 	areaTexto('descripcion','Descripción',$datos);
    	areaTexto('referencias_documentales','Referencias documentales',$datos);
    	areaTexto('proveedores','Proveedores',$datos);
    	areaTexto('entradas','Entradas',$datos);
    	areaTexto('responsable','Responsable',$datos);
    cierraColumnaCampos();

    abreColumnaCampos();
    	areaTexto('recursos','Recursos',$datos);
    	areaTexto('clientes','Clientes',$datos);
    	areaTexto('salidas','Salidas',$datos);
    	areaTexto('registros','Registros',$datos);
    	areaTexto('inspecciones','Inspecciones',$datos);
    cierraColumnaCampos();
   
	campoOculto($enlace,'enlace');
	
	cierraVentanaGestion($enlace.'?codigo='.$codigoMapa, true);
}

function obtieneValoresIndicadores($codigoMapa){
	$ejercicio=obtieneEjercicioParaWhere();
	$datos=array();

	conexionBD();
	$consulta=consultaBD("SELECT indicadores.* FROM indicadores INNER JOIN tipos_procesos WHERE codigoMapa=".$codigoMapa." ORDER BY codigo");
	while($indicador=mysql_fetch_assoc($consulta)){
		$colores = array('Verde'=>0,'Amarillo'=>0,'Rojo'=>0,'Gris'=>0);
		$marcadores = consultaBD("SELECT * FROM mapaProcesos WHERE YEAR(fechaFinIndicador)='$ejercicio' AND indicador=".$indicador['codigo']);
		while($marcador=mysql_fetch_assoc($marcadores)){
			if ($marcador['valor_real'] == 'ESTATICO'){
				$valorReal=consultaBD("select count(codigo) as numero, sum(valor) as total FROM valores_mapa WHERE codigoMapa=".$marcador['codigo']." AND checkValor='SI'",true,true);
				
				if($valorReal['numero']==0){
					$valorReal['numero']=1;
				}

				$valorReal = round($valorReal['total'] / $valorReal['numero']);

				$color=calculaIndicador($marcador['condicion'],$marcador['tolerancia'],$marcador['valorFinal'],$valorReal);
			} else {
				$valorReal=consultaBD("select count(codigo) as numero, sum(valorN) as numerador, sum(valorD) denominador FROM valores_mapa WHERE codigoMapa=".$marcador['codigo']." AND checkValor='SI'",true,true);
				
				if($valorReal['denominador']==0){
					$valorReal['denominador']=1;
				}

				$valorReal = (100 * $valorReal['numerador']) / $valorReal['denominador'];
				$valorFinal = (100 * $marcador['numerador']) / $marcador['denominador'];

				$color=calculaIndicador($marcador['condicion'],$marcador['tolerancia'],$valorFinal,$valorReal);
			}
			$colores[$color]++;
		}
		unset($colores['Gris']);
		if($colores['Verde'] == 0 && $colores['Amarillo'] == 0 && $colores['Rojo']== 0){
			$datos[$indicador['codigo']] = "Gris";
		} 
		elseif (($colores['Verde'] == $colores['Rojo'] && $colores['Rojo'] == $colores['Amarillo']) || ($colores['Rojo'] > $colores['Amarillo'] && $colores['Rojo'] > $colores['Verde'])){
			$datos[$indicador['codigo']] = "Rojo";
		} 
		elseif ($colores['Rojo'] == $colores['Amarillo'] && $colores['Rojo'] > $colores['Verde']){
			$datos[$indicador['codigo']] = "Rojo";
		} 
		elseif ($colores['Verde'] == $colores['Amarillo'] || $colores['Amarillo'] > $colores['Verde']){
			$datos[$indicador['codigo']] = "Amarillo";
		} 
		else {
			$datos[$indicador['codigo']] = "Verde";
		}
	}
	
	cierraBD();

	return $datos;
}

function calculaIndicador($condicion,$tolerancia,$valorFinal,$valorReal){
	$res="Gris";

	/*$cinco=($valorFinal*5)/100;//El indicador será amarillo cuando se encuentre entre el +/- 5% del valor final
	$tres=($valorFinal*3)/100;
	$mediaAbajoCinco=$valorFinal-$cinco;
	$mediaArribaCinco=$valorFinal+$cinco;
	$mediaAbajoTres=$valorFinal-$tres;
	$mediaArribaTres=$valorFinal+$tres;*/

	switch ($condicion) {
		case '==':
			$marcado = false;
			if($valorReal == $valorFinal){
				$res="Verde";
				$marcado = true;
			} 
			if(!$marcado){
				for($i=1;$i<=$tolerancia;$i++){
					if(($valorReal == ($valorFinal+$i)) || ($valorReal == ($valorFinal-$i))){
						$res="Amarillo";
						$marcado = true;
					}
				}
			}
			if(!$marcado){
				$res="Rojo";
			}
			break;
		case '>=':
			if($valorReal >= $valorFinal){
				$res="Verde";
			} elseif($valorReal >= ($valorFinal-$tolerancia)){
				$res="Amarillo";
			} else {
				$res="Rojo";
			}
			break;
		case '>':
			if($valorReal > $valorFinal){
				$res="Verde";
			} elseif($valorReal > ($valorFinal-$tolerancia)){
				$res="Amarillo";
			} else {
				$res="Rojo";
			}
			break;
		case '<=':
			if($valorReal <= $valorFinal){
				$res="Verde";
			} elseif($valorReal <= ($valorFinal+$tolerancia)){
				$res="Amarillo";
			} else {
				$res="Rojo";
			}
			break;
		case '<':
			if($valorReal < $valorFinal){
				$res="Verde";
			} elseif($valorReal < ($valorFinal+$tolerancia)){
				$res="Amarillo";
			} else {
				$res="Rojo";
			}
			break;
	}

	return $res;
}

function generaGraficoIndicadores($area){
	$ejercicio=obtieneEjercicioParaWhere();

	conexionBD();
	
	$indicador=datosRegistro('indicadores',$area);
	$colores = array('Verde'=>0,'Amarillo'=>0,'Rojo'=>0,'Gris'=>0);
	$marcadores = consultaBD("SELECT * FROM mapaProcesos WHERE YEAR(fechaFinIndicador)='$ejercicio' AND indicador=".$area);
	while($marcador=mysql_fetch_assoc($marcadores)){
		if ($marcador['valor_real'] == 'ESTATICO'){
			$valorReal=consultaBD("SELECT count(codigo) AS numero, sum(valor) AS total FROM valores_mapa WHERE codigoMapa=".$marcador['codigo']." AND checkValor='SI'",true,true);

			if($valorReal['numero']==0){
				$valorReal['numero']=1;
			}

			$valorReal = round($valorReal['total'] / $valorReal['numero']);

			$color=calculaIndicador($marcador['condicion'],$marcador['tolerancia'],$marcador['valorFinal'],$valorReal);
		} 
		else {
			$valorReal=consultaBD("SELECT count(codigo) AS numero, sum(valorN) AS numerador, sum(valorD) AS denominador FROM valores_mapa WHERE codigoMapa=".$marcador['codigo']." AND checkValor='SI'",true,true);
			if($valorReal['denominador']==0){
				$valorReal['denominador']=1;
			}

			$valorReal = (100 * $valorReal['numerador']) / $valorReal['denominador'];
			$valorFinal = (100 * $marcador['numerador']) / $marcador['denominador'];

			$color=calculaIndicador($marcador['condicion'],$marcador['tolerancia'],$valorFinal,$valorReal);
		}
		$colores[$color]++;
	}
	
	cierraBD();

	return $colores;
}

function obtieneAreaIndicador(){
	if(isset($_GET['area'])){
		$_SESSION['areaIndicador']=$_GET['area'];
	}

	return $_SESSION['areaIndicador'];
}

function obtieneNombreAreaIndicador($area){
	$consulta = consultaBD("SELECT * FROM indicadores WHERE codigo = ".$area,true,true);

	return $consulta['nombre'];
}

function obtieneMapa($area){
	$consulta = consultaBD("SELECT codigoMapa FROM indicadores INNER JOIN tipos_procesos ON indicadores.tipo=tipos_procesos.codigo WHERE indicadores.codigo = ".$area,true,true);

	return $consulta['codigoMapa'];
}

function creaEstadisticasIndicadores($area){
	//$tipo = consultaBD("SELECT valor_real FROM indicadores WHERE codigo=".$area,true,true);
	$tipo = consultaBD("SELECT valor_real FROM mapaProcesos WHERE indicador=".$area,true,true);
    if ($tipo['valor_real'] == 'ESTATICO'){
		$datos=consultaBD("SELECT SUM(valorFinal) AS valorFinal, SUM(valorReal) AS valorReal FROM mapaProcesos WHERE indicador='$area';",true,true);
	} else {
		$datos=consultaBD("SELECT SUM(valorFinal) AS valorFinal, SUM(numerador/denominador) AS valorReal FROM mapaProcesos WHERE indicador='$area';",true,true);
	}
	$cinco=($datos['valorFinal']*5)/100;//El indicador será amarillo cuando se encuentre entre el +/- 5% del valor final
	$tres=($datos['valorFinal']*3)/100;
	$mediaAbajoCinco=$datos['valorFinal']-$cinco;
	$mediaArribaCinco=$datos['valorFinal']+$cinco;
	$mediaAbajoTres=$datos['valorFinal']-$tres;
	$mediaArribaTres=$datos['valorFinal']+$tres;

	if($datos['valorFinal']=='' || $datos['valorReal']==''){
		$res="<div class='stat'> <i class='icon-circle-o valorIndicador indicadorGris'></i> <br>No se ha introducido valores</div>";
	}
	elseif($datos['valorReal']>=$mediaArribaCinco || $datos['valorReal']<=$mediaAbajoCinco){
		$res="<div class='stat'> <i class='icon-exclamation-triangle valorIndicador indicadorRojo'></i> <br>Desviación superior o igual al 5%</div>";
	}
	elseif($datos['valorReal']>=$mediaArribaTres || $datos['valorReal']<=$mediaAbajoTres){
		$res="<div class='stat'> <i class='icon-exclamation-triangle valorIndicador indicadorRojo'></i> <br>Desviación superior al 3%</div>";
	}
	else{
		$res="<div class='stat'> <i class='icon-check-circle valorIndicador indicadorVerde'></i> <br>Sin desviación</div>";
	}

	return $res;
}

function obtieneIndicadores($tipo){
	return consultaBD("SELECT * FROM indicadores WHERE tipo ='".$tipo."' ORDER BY codigo;",true);
}

function obtieneValores($codigo){
	$valores='';
	$consulta=consultaBD("SELECT * FROM valores_mapa WHERE codigoMapa=".$codigo,true);// 9 PARA PROBAR
	$i=0;
	while($datos=mysql_fetch_assoc($consulta)){
		$valores[$i] = $datos;
		$i++;
	}
	echo json_encode($valores);
}


function obtieneDocumentosIndicador($codigoIndicador){
	conexionBD();

	echo "<br />";

	obtieneDocumentosIndicadorPorTipo($codigoIndicador,'Internos','ficheroInterno');
	obtieneDocumentosIndicadorPorTipo($codigoIndicador,'Externos','ficheroExterno');
	obtieneDocumentosIndicadorPorTipo($codigoIndicador,'Distribucion','ficheroDistribucion');
	obtieneDocumentosIndicadorPorTipo($codigoIndicador,'Registros','ficheroRegistro');

	cierraBD();
}


function obtieneDocumentosIndicadorPorTipo($codigoIndicador,$tipoDocumento,$campoFichero){
	$consulta=consultaBD("SELECT nombreDocumento, $campoFichero AS fichero FROM documentos$tipoDocumento WHERE codigoIndicador='$codigoIndicador';");

	$directorio='../documentos/documentacion/'.strtolower($tipoDocumento).'/';
	while($datos=mysql_fetch_assoc($consulta)){
		$ruta=$directorio.$datos['fichero'];
		echo "<a href='$ruta' class='btn btn-small btn-primary documentoMapaProcesos noAjax' target='_blank'><i class='icon-file-text'></i> ".$datos['nombreDocumento']."</a> ";
	}
}