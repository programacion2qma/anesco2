<?php
  $seccionActiva=9;
  include_once('../cabecera.php');
  
  operacionesIndicadores();  
  $area=obtieneAreaIndicador();
  $nombreArea=obtieneNombreAreaIndicador($area);

  //$estadistica=creaEstadisticasIndicadores($area);
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estado de los indicadores del área de <?php echo $nombreArea; ?>:</h6>
                  <canvas id="pie-chart" class="chart-holder" height="250" width="538"></canvas>
                  <div class="leyenda" id="leyenda"></div>
                </div>
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div> 
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de los indicadores de <?php echo $nombreArea; ?></h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="index.php?codigo=<?php echo obtieneMapa($area); ?>" class="shortcut"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label">Volver</span> </a>
                  <a href="gestion.php?enlace=indicadoresArea.php&codigo=<?php echo $area; ?>" class="shortcut"><i class="shortcut-icon icon-edit"></i><span class="shortcut-label">Editar proceso</span> </a>
                  <a href="index.php?codigo=<?php echo $area; ?>&eliminaProceso" onclick="return confirm('¿Estás seguro que desea eliminar este proceso?');" id='eliminarProceso' class="shortcut noAjax"><i class="shortcut-icon icon-trash-o"></i><span class="shortcut-label">Eliminar proceso</span> </a>
                  <br />
                  <a href="gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label"> Nuevo indicador</span> </a>
                  <a href="javascript:void" id="eliminar" class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a
                ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

      </div><!-- /row -->

      <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-list"></i>
          <h3>Indicadores registrados para el área de <?php echo $nombreArea; ?></h3>
        </div>
        <!-- /widget-header -->
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable" id="tablaIndicadores">
            <thead>
              <tr>
                <th> Descripción </th>
                <th> Valor Real </th>
                <th class="centro"></th>
                <th> Valor Final Esperado </th>
                <th> Frecuencia </th>
                <th> Estado </th>
                <th class="centro"></th>
                <th><input type='checkbox' id="todo"></th>
              </tr>
            </thead>
            <tbody>            
            </tbody>
          </table>
        </div>
        <!-- /widget-content --> 
      </div>


    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>

<script src="../../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="../../api/js/filtroTablaAJAX.js" type="text/javascript"></script>
<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>

<script src="../../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="../../api/js/chart.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function(){
        listadoTabla('#tablaIndicadores','../listadoAjax.php?include=indicadores&funcion=listadoIndicadores("<?php echo $area; ?>");');
    });

    <?php
      $datos=generaGraficoIndicadores($area);
    ?>
    var pieData = [
        {
            value: <?php echo $datos['Verde']; ?>,
            color: "#88C423",
            label: "Superado"
        },
        {
            value: <?php echo $datos['Amarillo']; ?>,
            color: "#FDCD60",
            label: "En tolerancia"
        },
        {
            value: <?php echo $datos['Rojo']; ?>,
            color: "#F16553",
            label: "No alcanzado"
        }
      ];

    var grafico=new Chart(document.getElementById("pie-chart").getContext("2d")).Pie(pieData);
</script>

<script type="text/javascript" src="../../api/js/eventoGrafico.js"></script>
