<?php
	$seccionActiva=9;
	include_once('../cabecera.php');
	operacionesIndicadores();
	if(isset($_POST['codigoMapa'])){
		$datos=datosRegistro('mapas',$_POST['codigoMapa']);
	} else {
		$datos=datosRegistro('mapas',$_GET['codigo']);
	}
	$valores=obtieneValoresIndicadores($datos['codigo']);
?>

<center id="contenido">
	<h3 class="tituloMapaProcesos">MAPA DE PROCESOS</h3><br>
	<table class="tablaMapaProcesos">
		
			<?php
			$tipos=consultaBD('SELECT * FROM tipos_procesos WHERE codigoMapa='.$datos['codigo'],true);
			$rowspan=consultaBD('SELECT COUNT(codigo) AS total FROM tipos_procesos WHERE codigoMapa='.$datos['codigo'],true,true);
			$rowspan=($rowspan['total']*2)-1;
			$i=0;
			while($tipo=mysql_fetch_assoc($tipos)){
				if($i>0){
				?>
				<tr>
					<th><img src="../img/flechaMapaProceso_a.png" /></th>
					<th><img src="../img/flechaMapaProceso_ar.png" /></th>
				</tr>
				<tr>
			<?php 
				} else {?>
				<tr>
					<td rowspan="<?php echo $rowspan; ?>" class="celdaVertical"><p><?php echo $datos['entrada']; ?></p></td>
					<td rowspan="<?php echo $rowspan; ?>" class="celdaFlecha"><img src="../img/flechaMapaProceso_d.png" /></td>
			<?php
				}	
			?>
			<td colspan="2">
				<table class="tablaInterna" style="background-color: <?php echo $tipo['color']; ?>;">
					<tr>
						<th colspan="11">Procesos <?php echo $tipo['nombre']; ?></th>
					</tr>
					<tr>
						<td colspan="11">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<?php
							$procesos = obtieneIndicadores($tipo['codigo']);
							while($item=mysql_fetch_assoc($procesos)){
						?>
						<td class="cajasContenedor" id="id<?php echo $item['codigo']; ?>">
							<img src="../img/indicador<?php echo $valores[$item['codigo']]; ?>.png" />
							<?php 
								echo "<strong>".$item['nombre']."</strong>"; 
							?>
						</td>
						<td>&nbsp;</td>
						<?php
							}
						?>
					</tr>
					<tr>
						<td colspan="11">&nbsp;</td>
					</tr>
				</table>
			</td>
			<?php
			if($i==0){?>
				<td rowspan="<?php echo $rowspan; ?>" class="celdaFlecha"><img src="../img/flechaMapaProceso_d.png" /></td>
				<td rowspan="<?php echo $rowspan; ?>" class="celdaVertical"><p><?php echo $datos['salida']; ?></p></td>
			<?php 
			} ?>
		</tr>
		<?php
		$i++;
		} ?>
	</table>
<div class="margenMapa"></div>
<div align='center' class='divBoton'>
	<a class='btn btn-propio botonMapaProcesos' href='../mapas/'> <i class='icon-arrow-left'></i> Volver</a>
	<a class='btn btn-propio botonMapaProcesos' href='gestion.php?enlace=index.php&codigoMapa=<?php echo $datos['codigo']; ?>'> <i class='icon-plus-circle'></i> Nuevo proceso</a>
</div>
</center>
<?php
include_once('../pie.php');
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.cajasContenedor').click(function(e){
			if(e.target.nodeName!='A'){
				var area=$(this).attr('id');
				area = area.replace('id','');
				window.location='indicadoresArea.php?area='+area;
			}
		});
	});
</script>
