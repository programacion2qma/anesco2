<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de incrementos del IPC


function operacionesIncrementoIPC(){
	$res=false;

	if(isset($_POST['codigo'])){
		$_POST['incremento']=formateaNumeroWeb($_POST['incremento'],true);
		$res=actualizaDatos('incrementos_ipc');
	}
	elseif(isset($_POST['ejercicio'])){
		$_POST['incremento']=formateaNumeroWeb($_POST['incremento'],true);
		$res=insertaDatos('incrementos_ipc');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('incrementos_ipc');
	}

	mensajeResultado('ejercicio',$res,'incrementos IPC');
    mensajeResultado('elimina',$res,'incrementos IPC', true);
}



function gestionIncrementoIPC(){
	operacionesIncrementoIPC();
	
	abreVentanaGestionConBotones('Gestión de incremento del IPC','index.php');
	$datos=compruebaDatos('incrementos_ipc');
	
	campoTexto('ejercicio','Año',$datos,'input-mini pagination-right');
	campoTextoSimbolo('incremento','Incremento','%',formateaNumeroWeb($datos['incremento']));
	
	cierraVentanaGestion('index.php');
}

function imprimeIncrementosIPC(){

	$consulta=consultaBD("SELECT * FROM incrementos_ipc;",true);
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
		<tr>
			<td>".$datos['ejercicio']."</td>
			<td>".formateaNumeroWeb($datos['incremento'])." %</td>
			<td class='centro'>
				<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-edit'></i> Editar</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
        </tr>";
	}
}

//Fin parte de incrementos del IPC