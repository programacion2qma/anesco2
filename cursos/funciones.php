<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de incidencias

function operacionesCursos(){
	$res=true;

	if (isset($_POST['codigo'])) {
		$res=actualizaCurso();
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaCurso();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('cursos');
	}

	mensajeResultado('nombre',$res,'Curso');
    mensajeResultado('elimina',$res,'Curso', true);
}


function insertaCurso(){
	$res=true;
	$res=insertaDatos('cursos');
	$res=$res && insertaModulos($res);
	return $res;
}

function actualizaCurso(){
	$res=true;
	$res=actualizaDatos('cursos');
	$res=$res && insertaModulos($_POST['codigo']);
	return $res;
}

function insertaModulos($codigo){	
	$res = true;

	conexionBD();

	$res = $res && consultaBD('DELETE FROM cursos_modulos WHERE codigoCurso = '.$codigo);
	
	$i = 0;
	while (isset($_POST['numero'.$i])) {
		if ($_POST['nombre'.$i] != '') {

			$titulo = !isset($_POST['titulo'.$i]) || $_POST['titulo'.$i] != 'SI' ? 'NO' : 'SI';

			$sql = 'INSERT INTO cursos_modulos VALUES(
						NULL,
						'.$codigo.',
						"'.$_POST['numero'.$i].'",
						"'.$_POST['nombre'.$i].'",
						"'.$_POST['numeroMostrar'.$i].'",
						"'.$titulo.'"
					)';

			$res = $res && consultaBD($sql);
		}
		
		$i++;
	}

	cierraBD();

	return $res;
}

function gestionCursos(){
	operacionesCursos();
	
	abreVentanaGestionConBotones('Gestión de Cursos','index.php');
	$datos=compruebaDatos('cursos');
	abreColumnaCampos();
		campoTexto('nombre','Nombre de curso',$datos,'span7');
		campoSelect('tipo','Tipo',array('','Online','Presencial','A distancia'),array('','Online','Presencial','A distancia'),$datos);
	cierraColumnaCampos();

	tablaModulos($datos);
	cierraVentanaGestion('index.php',true);
}

function tablaModulos($datos){
	echo "
		<br clear='all'>
		<h3 class='apartadoFormulario'>Módulos</h3>
	    <div class='control-group'>                     
	        <div class='controls'>
	            <table class='tabla-simple mitadAncho' id='tablaModulos'>
	                <thead>
	                    <tr>
	                    	<th> Orden </th>
							<th> Nº Mostrar </th>
	                        <th> Módulo </th>
	                        <th> Título </th>
							<th> </th>
	                    </tr>
	                </thead>
	                <tbody>
	";
	        
	$i = 0;
	$j = 1;
	            
	if ($datos) {
		$consulta = consultaBD("SELECT * FROM cursos_modulos WHERE codigoCurso=".$datos['codigo']." ORDER BY numero", true);
		while ($item = mysql_fetch_assoc($consulta)) {	                	
			echo '<tr>';
			campoTextoTabla('numero'.$i,$item['numero'], 'input-mini');
			campoTextoTabla('numeroMostrar'.$i,$item['numeroMostrar'], 'input-mini');
			campoTextoTabla('nombre'.$i,$item['nombre'],'span6');
			echo "<td>";
			campoCheckSolo('titulo'.$i, $item['titulo']);
			echo "
					</td>
					<td>
						<input type='checkbox' name='filasTabla[]' value='$j'>
					</td>
				</tr>
			";
	        
			$i++;
			$j++;
		}
	}
	            
	if ($i == 0) {	            	
		echo '<tr>';
		campoTextoTabla('numero'.$i, '','input-mini');
		campoTextoTabla('numeroMostrar'.$i, '', 'input-mini');
		campoTextoTabla('nombre'.$i, '','span6');
		echo "<td>";
		campoCheckSolo('titulo'.$i);
		echo "
				</td>
				<td>
					<input type='checkbox' name='filasTabla[]' value='$j'>
				</td>
			</tr>
		";
	}

	            
	      
	echo "
	      			</tbody>
	            </table>
	            <center>
	    	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaModulos\");'><i class='icon-plus'></i> Añadir módulo</button> 
		            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaModulos\");'><i class='icon-trash'></i> Eliminar módulo</button>
	            </center>
	        </div>
	    </div>
	";
}


function imprimeCursos(){
	global $_CONFIG;
	$consulta=consultaBD("SELECT cursos.*, COUNT(cursos_modulos.codigo) AS modulos FROM cursos LEFT JOIN cursos_modulos ON cursos.codigo=cursos_modulos.codigoCurso GROUP BY cursos.codigo",true);
	while($datos=mysql_fetch_assoc($consulta)){
		echo "<tr>
				<td> ".$datos['nombre']." </td>
				<td> ".$datos['tipo']." </td>
				<td class='centro'> ".$datos['modulos']."</td>
				<td class='centro'>
                	<a href='".$_CONFIG['raiz']."cursos/gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio noAjax'><i class='icon-search-plus'></i> Detalles</i></a>
            	</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	  	</td>
			 </tr>";
	}
}