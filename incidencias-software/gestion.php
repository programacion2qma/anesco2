<?php
  $seccionActiva=7;
  include_once("../cabecera.php");  

                      abreVentanaGestionConBotones('Notificación de incidencia en software','index.php','','icon-edit','',true,'noAjax');
                      campoOculto($_CONFIG['codigoIncidenciasSoftware'],'codigoCliente');
          					  campoOculto('SI','creadoCliente');
          					  campoOculto('','textoOtros');

                      campoSelect('tipoTarea','Tipo de tarea',array('Modificación/Mejora','Error de funcionamiento'),array('MODIFICACION','INCIDENCIA'),false,'selectpicker span3 show-tick','');
          					  campoOculto('NULL','codigoUsuario');
          					  campoOculto('1','realizado');
          					  campoOculto('','fechaFinalizacion');
          					  campoOculto('','tiempoEmpleado');
                      areaTexto('observaciones','Indique su incidencia','','areaInforme');
                      campoOculto('','comentariosProgramacion');
                      creaTablaFicheros(0);
                    ?>
                    
                    <br />                      
                    <div class="form-actions">
                      <button type="submit" class="btn btn-propio"><i class="icon-check"></i> Notificar incidencia</button> 
					            <a href="index.php" class="btn btn-default"><i class="icon-remove"></i> Cancelar</a>
                    </div> <!-- /form-actions -->
                  </fieldset>
                </form>
                </div>


            </div>
            <!-- /widget-content --> 
          </div>

      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $('select').selectpicker();
    $(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
  });
</script>

<!-- /contenido --></div>

<?php include_once('../pie.php'); ?>