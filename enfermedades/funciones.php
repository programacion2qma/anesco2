<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión de epis


function operacionesEnfermedades(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('enfermedades');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('enfermedades');
	}
	elseif(isset($_POST['elimina'])){
		$res=eliminaEnfermedades($_POST['elimina']);
	}

	mensajeResultado('nombre',$res,'Enfermedad');
    mensajeResultado('elimina',$res,'Enfermedad', true);
}



function gestionEnfermedades(){
	operacionesEnfermedades();
	
	abreVentanaGestionConBotones('Gestión de Enfermedades','index.php');
	$datos=compruebaDatos('enfermedades');
	
	abreColumnaCampos();
		campoTexto('referencia','Código CIE',$datos,'input-mini');
		campoTexto('nombre','Nombre',$datos,'span6');
		campoNumero('numCampo','Nº de campo',$datos,'input-mini pagination-right',1,9);	
	cierraColumnaCampos(true);
	abreColumnaCampos();
		areaTexto('descripcion','Observaciones',$datos,'areaInforme'); 
	cierraColumnaCampos();
	
	campoOculto($datos,'eliminado','NO');
	
	cierraVentanaGestion('index.php',true);
}


function imprimeEnfermedades($eliminado){

	$consulta=consultaBD("SELECT * FROM enfermedades WHERE eliminado='$eliminado';",true);
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td>".$datos['referencia']."</td>
				<td>".$datos['nombre']."</td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-edit'></i> Editar</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
	        </tr>";
	}
}

function eliminaEnfermedades($valor){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();
    for($i=0;isset($datos['codigo'.$i]);$i++){
        $res=$res && cambiaValorCampo('eliminado',$valor,$datos['codigo'.$i],'enfermedades',false);
    }
    cierraBD();

    return $res;
}
//Fin parte de gestión de epis