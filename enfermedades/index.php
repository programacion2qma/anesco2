<?php
  $seccionActiva=10;
  include_once('../cabecera.php');
  
  $res=operacionesEnfermedades();

  $eliminado='NO';
  if(isset($_GET['eliminado'])){
    $eliminado=$_GET['eliminado'];
  }

  $estadisticas=estadisticasGenericas('enfermedades',false,"eliminado='".$eliminado."'");
  //$estadisticas=estadisticasGenericas('epis',false,"codigoCliente=".$cliente);
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de Enfermedades:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-medkit"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Enfermedade/s</div>
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de enfermedades</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                  <?php
                  if($eliminado=='NO'){?>
                  
                  <a href="<?php echo $_CONFIG['raiz']; ?>enfermedades/gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva enfermedad</span> </a>
                  <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
                  <a href="<?php echo $_CONFIG['raiz']; ?>enfermedades/index.php?eliminado=SI" class="shortcut"><i class="shortcut-icon icon-trash-o"></i><span class="shortcut-label">Eliminadas</span> </a>
                <?php } else { ?>
                  <a href="<?php echo $_CONFIG['raiz']; ?>enfermedades/index.php?eliminado=NO" class="shortcut noAjax"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label"> Volver</span> </a>
                  <a href="javascript:void" id='reactivar' class="shortcut noAjax"><i class="shortcut-icon icon-check-circle"></i><span class="shortcut-label">Reactivar</span> </a>
                <?php } ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Enfermedades registradas</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Código CIE</th>
                  <th> Nombre</th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeEnfermedades($eliminado);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>