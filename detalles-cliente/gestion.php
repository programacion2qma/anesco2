<?php
  $seccionActiva=1;
  include_once("../cabecera.php");
  gestionDetalles();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
	});
	$(".cif").focusout(function(){
    		isValidCifNif($(this).val());
		});
		
	$(".nif").focusout(function(){
    		isValidNif($(this).val());
	}); 
	
	$(".submit").click(function() {
		$('#accion').attr('value',$(this).attr('value'));
		$('#edit-profile').submit();
	}); 

	$('#telefono_gestoria, #telefono_informatico, #telefono_prl').attr('maxlength','9');

	/*$('#diferente_arco input[type=radio]').change(function(){
			if(this.value == 'SI'){
				$('.arco').prop('disabled','');
			} else {
				$('.arco').prop('disabled','disabled');
			}
		});
		
	$('#copias input[type=radio]').change(function(){
			if(this.value == 'SI'){
				$('.copiaseguridad').prop('disabled','');
			} else {
				$('.copiaseguridad').prop('disabled','disabled');
			}
		});*/
		
	function insertaFila(tabla){
    	//Clono la última fila de la tabla
    	var $tr = $('#'+tabla).find("tbody tr:last").clone();
    	//Obtengo el atributo name para los inputs y selects
   		$tr.find("input:not([type=checkbox]),select,textarea").attr("name", function(){
        //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
        var parts = this.id.match(/(\D+)(\d*)$/);
        //Creo un nombre nuevo incrementando el número de fila (++parts[2])
        return parts[1] + ++parts[2];
    }).attr("id", function(){//Hago lo mismo con los IDs
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });
    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody tr:last").after($tr);
	
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>