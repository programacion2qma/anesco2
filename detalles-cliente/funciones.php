<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión Clientes


function operacionesDetalles(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=insertaDetalles();
		$res=$res && finalizarDetalles($_POST['codigoCliente'],$_POST['accion']);
	}
	
	elseif(isset($_POST['actividad_principal'])){
		$res=insertaDetalles();
		$res=$res && finalizarDetalles($_POST['codigoCliente'],$_POST['accion']);
	}

	//Primer parámetro: índice $_POST que se usará para mostrar o no el mensaje (si existe se muestra, si no, no)
	mensajeResultadoAdicional('accion',$res,'Detalles');
}

function finalizarDetalles($cliente,$accion){
	$res=true;//Siempre que vayamos a usarlo con un AND, inicializarlo a true
	if ($accion == 'Finalizar'){
		conexionBD();
		
		$consulta=consultaBD("UPDATE actuaciones_cliente SET toma_datos = true WHERE codigoCliente='$cliente';");
	
		cierraBD();
	}

	return $res;
}

function insertaDetalles(){

	if($_POST['diferente_arco'] == 'NO'){
		$_POST['direccion_arco'] = 'NULL';
		$_POST['provincia_arco'] = 'NULL';
		$_POST['poblacion_arco'] = 'NULL';
		$_POST['cp_arco'] = 'NULL';
		$_POST['email_arco'] = 'NULL';
	}
	
	if($_POST['otros'] == 'NO'){
		$_POST['otros_soporte'] = 'NULL';
	}
	
	if($_POST['copias'] == 'NO'){
		$_POST['periocidad'] = 'NULL';
		$_POST['encargado_realizar'] = 'NULL';
		$_POST['encargado_custodiar'] = 'NULL';
		$_POST['lugar_almacenamiento'] = 'NULL';
		$_POST['cd_dvd'] = 'NULL';
		$_POST['usb'] = 'NULL';
		$_POST['disco_duro'] = 'NULL';
		$_POST['disco_duro_externo'] = 'NULL';
		$_POST['nube'] = 'NULL';
		$_POST['otros'] = 'NULL';
		$_POST['otros_soporte'] = 'NULL';
	}
	
	
	
	if(isset($_POST['codigo'])){
		$res=actualizaDatos('detalles_cliente');
		$res=$res && actualizaDatos('ficheros_cliente','','',"WHERE codigoCliente=".$_POST['codigoCliente']);//Hacemos un AND con $res para que se tenga en cuenta el resultado de todas las operaciones (si pisas el valor de $res solo tiene en cuenta el último)
		$res=$res && actualizaDatos('copias_seguridad','','',"WHERE codigoCliente=".$_POST['codigoCliente']);
		$res=$res && actualizaDatos('gestorias_cliente','','',"WHERE codigoCliente=".$_POST['codigoCliente']);
		$res=$res && actualizaDatos('informaticos_cliente','','',"WHERE codigoCliente=".$_POST['codigoCliente']);
		$res=$res && actualizaDatos('prl_cliente','','',"WHERE codigoCliente=".$_POST['codigoCliente']);
	}
	elseif(isset($_POST['actividad_principal'])){
		$codigo=insertaDatos('detalles_cliente');
		$res=insertaDatos('ficheros_cliente');
		$res=$res && insertaDatos('copias_seguridad');
		$res=$res && insertaDatos('gestorias_cliente');
		$res=$res && insertaDatos('informaticos_cliente');
		$res=$res && insertaDatos('prl_cliente');
		$_REQUEST['codigo'] = $codigo;
	}
	
	$res=$res && insertaAplicaciones($_POST['codigoCliente']);
	$res=$res && insertaRepresentantes($_POST['codigoCliente']);

	return $res;//Que se te había olvidado!!
}


function insertaAplicaciones($cliente){
	$res=true;//Siempre que vayamos a usarlo con un AND, inicializarlo a true

	conexionBD();
	
	$datos=arrayFormulario();
	$consulta=consultaBD("DELETE FROM aplicaciones_cliente WHERE codigoCliente='$cliente';");
	
	$i=0;
	while(isset($datos['nombreApp'.$i])){
		if($datos['nombreApp'.$i] != ''){
			$res=$res && consultaBD("INSERT INTO aplicaciones_cliente VALUES(NULL, '$cliente', '".$datos['nombreApp'.$i]."', '".
			$datos['finalidad'.$i]."');");
		}
		$i++;
	}
	
	cierraBD();

	return $res;
}

function insertaRepresentantes($cliente){
	$res=true;//Siempre que vayamos a usarlo con un AND, inicializarlo a true

	conexionBD();
	
	$datos=arrayFormulario();
	$consulta=consultaBD("DELETE FROM representantes_cliente WHERE codigoCliente='$cliente';");
	$i=0;
	//echo 'hola '.$datos['nombreRepresentante'.$i];
	while(isset($datos['nombreRepresentante'.$i])){
		if($datos['nombreRepresentante'.$i] != ''){
			$res=$res && consultaBD("INSERT INTO representantes_cliente VALUES(NULL, '$cliente', '".$datos['nombreRepresentante'.$i]."', '".
			$datos['dniRepresentante'.$i]."');");
		}
		$i++;
	}
	
	cierraBD();

	return $res;
}

function obtieneCodigoDetallesCliente(){
	if(isset($_GET['codigoCliente'])){
   		$_SESSION['codigoCliente']=$_GET['codigoCliente'];
 	}

 	return $_SESSION['codigoCliente'];
}

function obtieneNombreCliente(){
	if(isset($_GET['codigoCliente'])){
   		$cliente=consultaBD("SELECT razon_s FROM clientes WHERE codigo=".$_GET['codigoCliente'].";",true,true);
		$_SESSION['nombreCliente']=$cliente['razon_s'];
 	}

 	return $_SESSION['nombreCliente'];
}

function gestionDetalles(){
	operacionesDetalles();
	
	//$datos=compruebaDatos('detalles_cliente');// IMPORTANTE: compruebaDatos() debe ir detras de abreVentanaGestion, para que el input oculto que crea esté dentro del formulario
	$codigoCliente=obtieneCodigoDetallesCliente();
	$nombreCliente=obtieneNombreCliente();

	abreVentanaGestion("Detalle del cliente $nombreCliente",'index.php');
	
	if(isset($_GET['codigoCliente'])){
   		$datos=consultaBD("SELECT * FROM detalles_cliente WHERE codigoCliente=".$_GET['codigoCliente'],true,true);
 		if($datos == TRUE){
 			campoOculto($datos,'codigo');
 		}
 	} else {
		$datos=compruebaDatos('detalles_cliente');
	}
	
	if($datos){
		$datosFicheros=consultaBD("SELECT * FROM ficheros_cliente WHERE codigoCliente=".$datos['codigoCliente'],true,true);
		$datosCopia=consultaBD("SELECT * FROM copias_seguridad WHERE codigoCliente=".$datos['codigoCliente'],true,true);
		$datosGestoria=consultaBD("SELECT * FROM gestorias_cliente WHERE codigoCliente=".$datos['codigoCliente'],true,true);
		$datosInformatico=consultaBD("SELECT * FROM informaticos_cliente WHERE codigoCliente=".$datos['codigoCliente'],true,true);
		$datosPRL=consultaBD("SELECT * FROM prl_cliente WHERE codigoCliente=".$datos['codigoCliente'],true,true);
	}
	cierraColumnaCampos();
			
	creaPestaniasLOPD();
		abrePestania(1,'active');
			abreColumnaCampos();
				campoOculto($datos,'codigoCliente',$codigoCliente);
				campoTexto('actividad_principal','Actividad principal',$datos);
				campoTexto('cnae','CNAE',$datos);
				campoTexto('web','Sitio Web',$datos);
				campoTexto('facebook','Facebook',$datos);
				campoTexto('responsable_seguridad','Responsable de seguridad',$datos);
				campoTexto('nif_responsable','NIF',$datos,'input-large nif');
			cierraColumnaCampos();
			abreColumnaCampos();
				$disabled = $datos['diferente_arco'] == '' || $datos['diferente_arco'] == 'NO' ? true : false;
				echo "<div id='diferente_arco'>";
					campoRadio('diferente_arco','Se establece un lugar diferente al indicando  en los Datos de la Empresa para ejercitar los derechos (A.R.C.O.)?',$datos,'NO',array('No','Si'),array('NO','SI'));
				echo "</div>";
				campoTexto('direccion_arco','Dirección',$datos,'input-large arco');
				campoSelectProvincia('provincia_arco',$datos);
				campoTexto('poblacion_arco','Población',$datos,'input-large arco');
				campoTexto('cp_arco','Código postal',$datos,'input-large arco');
				campoTexto('email_arco','Email',$datos,'input-large arco');
				cierraColumnaCampos();
		cierraPestania();
		
		abrePestania(2);
			$i=tablaRepresentantes($datos);
		cierraPestania();
		
		abrePestania(3);
			abreColumnaCampos('span5');
				campoRadio('clausula_videovigilancia','Videovigilancia',$datos,'NO',array('No','Si'),array('NO','SI'));
				campoRadio('clausula_trabajadores','Trabajadores',$datos,'NO',array('No','Si'),array('NO','SI'));
				campoRadio('clausula_trabajadorespracticas','Trabajadores en prácticas',$datos,'NO',array('No','Si'),array('NO','SI'));
				campoRadio('clausula_cv','Entrada de Currículum Vitae',$datos,'NO',array('No','Si'),array('NO','SI'));
			cierraColumnaCampos();
			abreColumnaCampos('span5');
				campoRadio('clausula_contratosclientes','Contratos con clientes',$datos,'NO',array('No','Si'),array('NO','SI'));
				campoRadio('clausula_derechosimagen','Derechos de imagen',$datos,'NO',array('No','Si'),array('NO','SI'));
				campoRadio('clausula_datosinternacionales','Ventas o transferencias de datos internacionales',$datos,'NO',array('No','Si'),array('NO','SI'));
				campoRadio('clausula_accesodatos','Registro de acceso físico a datos (Nivel alto)',$datos,'NO',array('No','Si'),array('NO','SI'));
			cierraColumnaCampos();
		cierraPestania();
		
		abrePestania(4);
			abreColumnaCampos('span5');
				campoRadio('clientes_y_proveedores','Clientes y proveedores',!isset($datosFicheros)?'':$datosFicheros['clientes_y_proveedores'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('nominas_personal','Nóminas personal y R.R.H.H.',!isset($datosFicheros)?'':$datosFicheros['nominas_personal'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('videovigilancia','Videovigilancia',!isset($datosFicheros)?'':$datosFicheros['videovigilancia'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('contacto_web','Contacto web',!isset($datosFicheros)?'':$datosFicheros['contacto_web'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('historial_clinico','Historial clínico',!isset($datosFicheros)?'':$datosFicheros['historial_clinico'],'NO',array('No','Si'),array('NO','SI'));
			cierraColumnaCampos();
			abreColumnaCampos('span5');
				campoRadio('arrendatarios','Arrendatarios',!isset($datosFicheros)?'':$datosFicheros['arrendatarios'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('renta','Renta',!isset($datosFicheros)?'':$datosFicheros['renta'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('gestion_vehiculos','Gestión de vehículos',!isset($datosFicheros)?'':$datosFicheros['gestion_vehiculos'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('comunidades','Comunidades',!isset($datosFicheros)?'':$datosFicheros['comunidades'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('correduria_seguros','Correduría seguros',!isset($datosFicheros)?'':$datosFicheros['correduria_seguros'],'NO',array('No','Si'),array('NO','SI'));
			cierraColumnaCampos();
		cierraPestania();
		
		abrePestania(5);
			abreColumnaCampos('span5');
				$disabled = !isset($datosCopia) || $datosCopia['periocidad'] == '' ? true : false;
				$copia = $disabled ? 'NO':'SI';
				echo "<div id='copias'>";
					campoRadio('copias','¿Se realizan copias de seguridad?',$copia,'NO',array('No','Si'),array('NO','SI'));
				echo "</div>";
				campoRadio('periocidad','Periocidad',!isset($datosCopia)?'':$datosCopia['periocidad'],'DIARIA',array('Diaria','Semanal','Mensual'),array('DIARIA','SEMANAL','MENSUAL'));
				campoTexto('encargado_realizar','Encargado de realizar las copias',!isset($datosCopia)?'':$datosCopia['encargado_realizar'],'input-large copiaseguridad');
				campoTexto('encargado_custodiar','Encargado de custodiarlas',!isset($datosCopia)?'':$datosCopia['encargado_custodiar'],'input-large copiaseguridad');
				campoTexto('lugar_almacenamiento','Lugar de almacenamiento',!isset($datosCopia)?'':$datosCopia['lugar_almacenamiento'],'input-large copiaseguridad');
			cierraColumnaCampos();
			abreColumnaCampos('span5');
				campoRadio('cd_dvd','CD/DVD',!isset($datosCopia)?'':$datosCopia['cd_dvd'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('usb','USB',!isset($datosCopia)?'':$datosCopia['usb'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('disco_duro','Disco duro',!isset($datosCopia)?'':$datosCopia['disco_duro'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('disco_duro_externo','Disco duro externo',!isset($datosCopia)?'':$datosCopia['disco_duro_externo'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('nube','Nube (Cloud)',!isset($datosCopia)?'':$datosCopia['nube'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('otros','Otros',!isset($datosCopia)?'':$datosCopia['otros'],'NO',array('No','Si'),array('NO','SI'));
				campoTexto('otros_soporte','Soporte',!isset($datosCopia)?'':$datosCopia['otros_soporte'],'input-large copiaseguridad');
			cierraColumnaCampos();
		cierraPestania();
		
		abrePestania(6);
			$i=tablaAplicaciones($datos);
		cierraPestania();
		
		abrePestania(7);
			abreColumnaCampos('span5');
				campoTexto('razon_social_gestoria','Razón social',!isset($datosGestoria)?'':$datosGestoria['razon_social_gestoria']);
				campoTexto('cif_gestoria','CIF',!isset($datosGestoria)?'':$datosGestoria['cif_gestoria'],'input-large cif');
				campoTexto('domicilio_gestoria','Domicilio',!isset($datosGestoria)?'':$datosGestoria['domicilio_gestoria']);
				campoTexto('telefono_gestoria','Teléfono',!isset($datosGestoria)?'':$datosGestoria['telefono_gestoria']);
				campoTexto('email_gestoria','Email',!isset($datosGestoria)?'':$datosGestoria['email_gestoria']);
				campoTexto('responsable_gestoria','Responsable',!isset($datosGestoria)?'':$datosGestoria['responsable_gestoria']);
			cierraColumnaCampos();
			abreColumnaCampos('span5');
				campoRadio('laboral_gestoria','Laboral',!isset($datosGestoria)?'':$datosGestoria['laboral_gestoria'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('contable_gestoria','Contable',!isset($datosGestoria)?'':$datosGestoria['contable_gestoria'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('fiscal_gestoria','Fiscal',!isset($datosGestoria)?'':$datosGestoria['fiscal_gestoria'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('juridica_gestoria','Jurídica',!isset($datosGestoria)?'':$datosGestoria['juridica_gestoria'],'NO',array('No','Si'),array('NO','SI'));
			cierraColumnaCampos();
		cierraPestania();
		
		abrePestania(8);
			abreColumnaCampos('span5');
				campoTexto('razon_social_informatico','Razón social',!isset($datosInformatico)?'':$datosInformatico['razon_social_informatico']);
				campoTexto('cif_informatico','CIF',!isset($datosInformatico)?'':$datosInformatico['cif_informatico'],'input-large cif');
				campoTexto('domicilio_informatico','Domicilio',!isset($datosInformatico)?'':$datosInformatico['domicilio_informatico']);
				campoTexto('telefono_informatico','Teléfono',!isset($datosInformatico)?'':$datosInformatico['telefono_informatico']);
				campoTexto('email_informatico','Email',!isset($datosInformatico)?'':$datosInformatico['email_informatico']);
				campoTexto('responsable_informatico','Responsable',!isset($datosInformatico)?'':$datosInformatico['responsable_informatico']);
			cierraColumnaCampos();
			abreColumnaCampos('span5');
				campoRadio('acceso_remoto_informatico','Acceso remoto',!isset($datosInformatico)?'':$datosInformatico['acceso_remoto_informatico'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('mantenimiento_sistema_informatico','Mantenimiento del sistema',!isset($datosInformatico)?'':$datosInformatico['mantenimiento_sistema_informatico'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('mantenimiento_aplicaciones_informatico','Mantenimiento aplicaciones',!isset($datosInformatico)?'':$datosInformatico['mantenimiento_aplicaciones_informatico'],'NO',array('No','Si'),array('NO','SI'));
				campoRadio('mantenimiento_web_informatico','Mantenimiento web',!isset($datosInformatico)?'':$datosInformatico['mantenimiento_web_informatico'],'NO',array('No','Si'),array('NO','SI'));
			cierraColumnaCampos();
		cierraPestania();
		
		abrePestania(9);
			abreColumnaCampos();
				campoTexto('razon_social_prl','Razón social',!isset($datosPRL)?'':$datosPRL['razon_social_prl']);
				campoTexto('cif_prl','CIF',!isset($datosPRL)?'':$datosPRL['cif_prl'],'input-large cif');
				campoTexto('domicilio_prl','Domicilio',!isset($datosPRL)?'':$datosPRL['domicilio_prl']);
				campoTexto('telefono_prl','Teléfono',!isset($datosPRL)?'':$datosPRL['telefono_prl']);
				campoTexto('email_prl','Email',!isset($datosPRL)?'':$datosPRL['email_prl']);
				campoTexto('responsable_prl','Responsable',!isset($datosPRL)?'':$datosPRL['responsable_prl']);
			campoOculto('','accion');
		cierraPestania();
	cierraVentanaGestionAdicional('Guardar y finalizar','../clientes/index.php', true);
	return $i;
}

function creaPestaniasLOPD(){
	echo "<div class='tabbable tabs-lopd'>
	       	<ul class='nav nav-tabs tabs-small'>
		        <li class='active'><a href='#1' class='noAjax' data-toggle='tab'>Detalle de cliente</a></li>
		        <li><a href='#2' class='noAjax' data-toggle='tab'>Representantes legales</a></li>
		        <li><a href='#3' class='noAjax' data-toggle='tab'>Clausulas a insertar en el documento de seguridad</a></li>
		        <li><a href='#4' class='noAjax' data-toggle='tab'>Relación de ficheros a inscribir en la A.E.P.D</a></li>
		        <li><a href='#5' class='noAjax' data-toggle='tab'>Copias de seguridad</a></li>
		        <li><a href='#6' class='noAjax' data-toggle='tab'>Aplicaciones</a></li>
		        <li><a href='#7' class='noAjax' data-toggle='tab'>Gestoría</a></li>
		        <li><a href='#8' class='noAjax' data-toggle='tab'>Informático</a></li>
		        <li><a href='#9' class='noAjax' data-toggle='tab'>Protección de riesgos laborales</a></li>
	       	</ul>

       <div class='tab-content'>";
}

//Parte de campos personalizados

function campoSelectProvincia($nombreCampo,$datos){
	$valores=array();
	$nombres=array();
	$provinciasAgencia=array('01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');
	
	foreach ($provinciasAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}

	campoSelect($nombreCampo,'Provincia',$nombres,$valores,$datos);
} 

function tablaAplicaciones($datos){
	echo"	<p>Para eliminar, dejar el campo nombre vacío</p>
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaAplicaciones'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Aplicación </th>
							<th> Finalidad </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    $i=0;
	if($datos){
		$consulta=consultaBD("SELECT * FROM aplicaciones_cliente WHERE codigoCliente=".$datos['codigoCliente'],true);				  
		while($aplicacion=mysql_fetch_assoc($consulta)){
		 	echo "<tr>";
				campoTextoTabla('nombreApp'.$i,$aplicacion['nombre'],'input-xxlarge pagination-right');
				campoTextoTabla('finalidad'.$i,$aplicacion['finalidad'],'input-xxlarge pagination-right');
			echo"
				</tr>";
			$i++;
		}
	}

	if($i==0 || !$datos){
	 	echo "<tr>";
			campoTextoTabla('nombreApp'.$i,'','input-xxlarge pagination-right');
			campoTextoTabla('finalidad'.$i,'','input-xxlarge pagination-right');
		echo"</tr>";
	}

	echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaAplicaciones\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
		 	";
			
	return $i;
}

function tablaRepresentantes($datos){
	echo"	<p>Para eliminar, dejar el campo nombre vacío</p>
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaRepresentantes'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Representante </th>
							<th> DNI </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    $i=0;
	if($datos){
		$consulta=consultaBD("SELECT * FROM representantes_cliente WHERE codigoCliente=".$datos['codigoCliente'],true);				  
		while($representante=mysql_fetch_assoc($consulta)){
		 	echo "<tr>";
				campoTextoTabla('nombreRepresentante'.$i,$representante['nombre'],'input-xxlarge pagination-right');
				campoTextoTabla('dniRepresentante'.$i,$representante['dni'],'input-xlarge pagination-right nif');
			echo"
				</tr>";
			$i++;
		}
	}

	if($i==0 || !$datos){
	 	echo "<tr>";
			campoTextoTabla('nombreRepresentante'.$i,'','input-xxlarge pagination-right');
			campoTextoTabla('dniRepresentante'.$i,'','input-xlarge pagination-right nif');
		echo"</tr>";
	}

	echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaRepresentantes\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
		 	";
			
	return $i;
}