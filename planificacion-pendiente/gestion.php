<?php
  $seccionActiva=9;
  include_once("../cabecera.php");
  if(isset($_GET['toma_datos'])){
  	tomaDatos();
  	echo '<script src="../js/filasTablaPlanificacion.js" type="text/javascript"></script>';
  } else {
  	gestionFormularioPRL();
  	echo '<script src="'.$_CONFIG['raiz'].'../../api/js/filasTabla.js" type="text/javascript"></script>';
  }
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/filasTablaPlanificacion.js" type="text/javascript"></script>

<script src="../../api/js/firma/jquery.signaturepad.js" type="text/javascript"></script>
<script src="../js/funcionesTomaDatos.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
		$('#fechaAprobada').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');modificarFechas($(this).val());});
		$(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});

		recorrerRadios();

		$("input[type='radio']").change(function(){
			compruebaRadio($(this));
		});

		$("input[name='aprobado']").change(function(){
			if($(this).val()=='SI'){
				$('#fechaAprobada').datepicker('setValue',fechaActual());
				modificarFechas(fechaActual());
			}
			//compruebaRadio($(this));
		});
		
		/*$(".submit").click(function() {
  			$('#accion').attr('value',$(this).attr('value'));
  			$('#edit-profile').submit();
		});*/ 
		
		$('.horas').change(function(){
			modificarHoras();
		});

		$('.minutos').change(function(){
			modificarHoras();
		});

		$('.camposGeneralesTomaDatos').find('input[type=text],select').each(function(){
			$(this).attr('readonly',true);
			if($(this).hasClass('selectpicker')){
				$(this).selectpicker('refresh');
			}
		});

		$('.selectEmpleadoFunciones').change(function(){
			obtenerPuestoEmpleadosFunciones($(this));
		})

		mostrarDivs();
		$('input[type=radio]').change(function(){
			mostrarDivs();
		});
	});


function modificarFechas(val){
	obtenerFecha('#fechaPrevista0',val,'P7D');
	obtenerFecha('#fechaPlanPrev',val,'P30D');
	obtenerFecha('#fechaEr',val,'P30D');
	obtenerFecha('#fechaPap',val,'P60D');
	obtenerFecha('#fechaInfo',val,'P60D');
	obtenerFecha('#fechaCert',val,'P60D');
	obtenerFecha('#fechaMemoria',val,'P90D');
	obtenerFecha('#fechaVs',val,'P90D');
}


function recorrerRadios(){
	$("input[type='radio']:checked").each(function (){
		compruebaRadio($(this));
	});
}

function compruebaRadio(elem){
	var val = $(elem).val();
  	var id = '#div'+obtieneID($(elem).attr('name'));
  	if ($(id).length) {
  		mostrarCampos(val,id);
  	}
}

function obtieneID(id){
	return id.replace('pregunta','');
}

function mostrarCampos(val,div){
	if($(div).attr('metodo') == 'inverso'){
		if(val == 'NO'){
			$(div).removeClass('hidden');
			$(div+'_1').removeClass('hidden');
		} else {
			$(div).addClass('hidden');
			$(div+'_1').addClass('hidden');
		}
	} else {
		if(val == 'SI'){
			$(div).removeClass('hidden');
			$(div+'_1').removeClass('hidden');
		} else {
			$(div).addClass('hidden');
			$(div+'_1').addClass('hidden');
		}
	}
}

function obtenerFecha(campo,fecha,suma){
	var consulta=$.post('obtenerFecha.php?',{'fecha':fecha,'suma':suma});

	consulta.done(function(respuesta){
		if(respuesta=='fallo'){
			alert('Ha habido un problema al obtener la fecha límite.\nInténtelo de nuevo en unos segundos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
		}
		else{
			$(campo).datepicker('setValue', respuesta)
		}
	});
}

function modificarHoras(){
	var horas=0;
	var minutos=0;
	$('.horas').each(function(){
		if($(this).val()!=''){
    		horas=horas+parseInt($(this).val());
    	}
	});
	$('.minutos').each(function(){
		if($(this).val()!=''){
    		minutos=minutos+parseInt($(this).val());
    	}
	});
	horas=horas+parseInt(minutos/60);
	minutos=minutos%60;
	if(horas<10){
		horas='0'+horas;
	}
	if(minutos<10){
		minutos='0'+minutos;
	}
	$('#totalHoras').val(horas+':'+minutos);
}

function mostrarDivs(){
	var campos=['planPrev','pap','info','accidentes','memoria','vs'];
	var valor='NO';
	for(var i=0;i<campos.length;i++){
		valor=$('input[name='+campos[i]+']:checked').val();
		if(valor=='SI'){
			$('#div_'+campos[i]).removeClass('hide');
		} else {
			$('#div_'+campos[i]).addClass('hide');
		}
	}
}
	
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>