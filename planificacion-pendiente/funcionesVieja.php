<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión de Formularios PRL


function operacionesFormularioPRL(){
	$res=false;
		
	if(isset($_POST['codigo'])){
		$res=actualizaPlanificacion();
	}
	elseif(isset($_POST['codigoContrato'])){
		$res=insertaPlanificacion();
	}
	elseif(isset($_POST['codigoPRL'])){
		$res=insertaFormulario();
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoPLanificacion($_POST['elimina']);
		//$res=eliminaPLanificacion();
	}
	
	mensajeResultadoAdicional('fecha',$res,'Formulario');
	mensajeResultadoAdicional('codigoPRL',$res,'Formulario');
    mensajeResultado('elimina',$res,'Formulario', true);
}

function insertaPlanificacion(){
	definirPOST();
	$res=insertaDatos('formularioPRL');
	
	if($res){
		$codigo=$res;

		$res=insertaDocumentacion($codigo,'documentos_plan_prev','plan');
		$res=$res && insertaDocumentacion($codigo,'documentos_info','info');
	}

	return $res;
}

function actualizaPlanificacion(){
	definirPOST();
	$res=actualizaDatos('formularioPRL');

	if($res){
		eliminarFicheros('documentos_plan_prev','plan');
		eliminarFicheros('documentos_info','info');

		$codigo=$_POST['codigo'];

		$res=insertaDocumentacion($codigo,'documentos_plan_prev','plan');
		$res=$res && insertaDocumentacion($codigo,'documentos_info','info');
	}

	return $res;
}

function insertaFormulario(){
	$res=true;
	$datos=arrayFormulario();
	
	$i=0;
	$query='';
	while(isset($datos['pregunta'.$i])){
		if($i>0){
			$query.="&{}&";
		}
		$query.="pregunta$i=>".$datos['pregunta'.$i];
		$existe = true;
		$i++;
	}

	if($existe){
		$ficheroPlano=subeFicheroPlano();

		conexionBD();
		$res = consultaBD("UPDATE formularioPRL SET formulario = '".$query."', firmaTecnico='".$datos['firmaTecnico']."' ".$ficheroPlano." WHERE codigo=".$datos['codigoPRL']);
		/*$res = $res && insertaEmpleadosPRL($datos['codigoPRL']);
		$res = $res && insertaCentrosPRL($datos['codigoPRL']);*/

		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'botiquines_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','ubicacionBotiquines','senializadoBotiquines'),'ubicacionBotiquines');
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'extintores_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','numero','kg','tipo','eficacia','senializado','revisado','ubicacionExtintor'),'numero');
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'bie_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','numeroBIE','medidaBIE','presion','senializado','revisado','ubicacion'),'numeroBIE');
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'locales_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','ubicacionLocales','senializadoLocales'),'ubicacionLocales');
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'medios_humanos_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','ubicacionMedios'),'ubicacionMedios');
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'materias_primas_formulario_prl',$datos['codigoPRL'],array('materiaPrima','lugar','cantidad','codigoFormularioPRL'),'materiaPrima');
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'puestos_trabajo_formulario_prl',$datos['codigoPRL'],array('codigoPuestoTrabajoSI','codigoFormularioPRL','sensibleSI'),'codigoPuestoTrabajoSI');
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'puestos_trabajo_formulario_prl',$datos['codigoPRL'],array('codigoPuestoTrabajoNO','codigoFormularioPRL','sensibleNO'),'codigoPuestoTrabajoNO',false);
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'instalaciones_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','codigoInstalacion'),'codigoInstalacion');

		cierraBD();

		$res=$res && compruebaCambioDatosContrato();
	}
	return $res;
}

function cambiaEstadoEliminadoPLanificacion($estado){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],'formularioPRL',false);
	}
	cierraBD();

	return $res;
}

function subeFicheroPlano(){
	$res='';

	if(isset($_FILES['ficheroPlano'])){
		$fichero=subeDocumento('ficheroPlano',time(),'../documentos/planificacion');
		$res=", ficheroPlano='".$fichero."'";
	}

	return $res;
}

function insertaDatosTablaAsociadaFormularioPRL($datos,$tabla,$codigoPRL,$campos,$campoCondicion,$borrado=true){
	$res=true;

	if($borrado){
		$res=consultaBD("DELETE FROM $tabla WHERE codigoFormularioPRL='$codigoPRL';");
	}
	

	for($i=0;isset($datos[$campoCondicion.$i]);$i++){
		$valores='';

		for($j=0;$j<count($campos);$j++){
			if($campos[$j]=='codigoFormularioPRL'){
				$valores.="'".$codigoPRL."', ";
			}
			elseif(substr_count($campos[$j],'codigo')>0){
				$valores.=$datos[$campos[$j].$i].", ";//Si es un campo código, no pone las comillas!! ('NULL' da error)
			}
			else{
				$valores.="'".$datos[$campos[$j].$i]."', ";
			}
		}

		$valores=quitaUltimaComa($valores);

		$res=$res && consultaBD("INSERT INTO $tabla VALUES(NULL,$valores);");
	}

	return $res;
}

/*function insertaEmpleadosPRL($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM empleados_prl WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['nombreEmpleadoPRL'.$i])){
		if($datos['nombreEmpleadoPRL'.$i] != ''){
		$res = $res && consultaBD("INSERT INTO empleados_prl VALUES (NULL,'".$codigo."','".$datos['nombreEmpleadoPRL'.$i]."',
			'".$datos['dniEmpleadoPRL'.$i]."','".$datos['puestoEmpleadoPRL'.$i]."','".$datos['funcionesEmpleadoPRL'.$i]."','".$datos['salenEmpleadoPRL'.$i]."',
			'".$datos['vehiculoEmpleadoPRL'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function insertaCentrosPRL($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM centros_prl WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['nombreCentroPRL'.$i])){
		if($datos['nombreCentroPRL'.$i] != ''){
		$res = $res && consultaBD("INSERT INTO centros_prl VALUES (NULL,'".$codigo."','".$datos['nombreCentroPRL'.$i]."',
			'".$datos['delegadoCentroPRL'.$i]."','".$datos['comiteCentroPRL'.$i]."','".$datos['trabajadorCentroPRL'.$i]."','".$datos['consultaCentroPRL'.$i]."');");
		}

		$i++;
	}

	return $res;
}*/

function eliminarFicheros($tabla,$prefijo){
	$res = true;

	for($i=0;$i<$_POST['ficherosTotales_'.$prefijo];$i++){
		if(isset($_POST['eliminaFichero_'.$prefijo.$i])){
			$codigo=$_POST['eliminaFichero_'.$prefijo.$i];
			$fichero=datosRegistro($tabla,$codigo);
			$res=consultaBD('DELETE FROM '.$tabla.' WHERE codigo='.$codigo,true);
			unlink('ficheros/'.$fichero['fichero']);
		}
	}

	return $res;
}

function insertaDocumentacion($codigo,$tabla,$prefijo){
	$res=true;//Siempre que vayamos a usarlo con un AND, inicializarlo a true

	conexionBD();
	
	$datos=arrayFormulario();
	$i=0;
	while(isset($datos['codigoFichero_'.$prefijo.$i])){
		$res=$res && consultaBD('UPDATE '.$tabla.' SET nombre="'.$datos['nombreSubido_'.$prefijo.$i].'" WHERE codigo='.$datos['codigoFichero_'.$prefijo.$i]);
		$i++;
	}
	$i=0;
	while(isset($_FILES['fichero_'.$prefijo.$i])){
		if($_FILES['fichero_'.$prefijo.$i]['tmp_name'] != ''){
			$fichero=subeDocumento('fichero_'.$prefijo.$i,time(),'../documentos/planificacion');

			$res=$res && consultaBD("INSERT INTO ".$tabla." VALUES(NULL, '$codigo', '".$datos['nombre_'.$prefijo.$i]."', '".$fichero."');");
		}
		$i++;
	}
	
	cierraBD();

	return $res;
}


function gestionFormularioPRL(){
	$mesesNombres=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	operacionesFormularioPRL();
	
	abreVentanaGestionConBotones('Gestión de Formularios PRL','index.php','','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('formularioPRL');

	abreColumnaCampos();
		//campoFecha('fecha','Fecha',$datos);
		campoOculto(formateaFechaWeb($datos['fecha']),'fecha',fecha());

		if(!$datos){
			$numero = generaNumero();
		} else {
			$numero = $datos['codigoInterno'];
		}
		campoTexto('codigoInterno','Num',$numero,'input-mini pagination-right');
		campoOculto($datos,'formulario');
		campoOculto($datos,'ficheroPlano','NO');
		campoOculto($datos,'eliminado','NO');
		$where=defineWhereEmpleado('WHERE activo="SI" AND enVigor="SI" AND opcion<>"2"');
		campoSelectContrato($datos);
		campoSelectConsulta('codigoUsuarioTecnico','Técnico/Enfermera','SELECT codigo, CONCAT(nombre, " ", apellidos) AS texto FROM usuarios WHERE tipo="TECNICO" OR tipo="ENFERMERIA";',$datos);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadio('aprobado','Aprobado',$datos);
		campoFechaBlanco('fechaAprobada','Fecha aprobado',$datos);
	cierraColumnaCampos();
	echo "<br clear='all'>";
	abreColumnaCampos();
		campoRadio('visitada','Visitada',$datos);
		campoFechaBlanco('fechaVisita','Fecha prevista',$datos);
		campoFechaBlanco('fechaVisitaReal','Fecha real',$datos);
		campoNumero('numVisitas','Número de visitas',$datos['numVisitas']);
		campoHorasReales('horasVisita',$datos);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadio('planPrev','Plan prevención',$datos);
		campoFechaBlanco('fechaPlanPrev','Fecha prevista',$datos);
		campoFechaBlanco('fechaPlanPrevReal','Fecha real',$datos);
		campoHorasReales('horasPlanPrev',$datos);
	cierraColumnaCampos();
	
	abreColumnaCampos('sinFlotar');
		echo "<h3 class='apartadoFormulario'>Documentos</h3>";
		tablaFicheros($datos);
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoRadio('pap','Evaluación de riesgos \\ Planificación preventiva',$datos);
		campoFechaBlanco('fechaPap','Fecha prevista',$datos);
		campoFechaBlanco('fechaPapReal','Fecha real',$datos);
		campoHorasReales('horasPap',$datos);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadio('info','Información a los trabajadores \\ Certificados de formación',$datos);
		campoFechaBlanco('fechaInfo','Fecha prevista',$datos);
		campoFechaBlanco('fechaInfoReal','Fecha real',$datos);
		campoHorasReales('horasInfo',$datos);
	cierraColumnaCampos();

	abreColumnaCampos('sinFlotar');
		echo "<h3 class='apartadoFormulario'>Documentos</h3>";
		tablaFicheros($datos,2);
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoRadio('memoria','Memoria',$datos);
		campoFechaBlanco('fechaMemoria','Fecha prevista',$datos);
		campoFechaBlanco('fechaMemoriaReal','Fecha real',$datos);
		campoHorasReales('horasMemoria',$datos);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadio('vs','F. prevista para  Rec. Médicos',$datos);
		campoFechaBlanco('fechaVs','Fecha prevista',$datos);
		campoFechaBlanco('fechaVsReal','Fecha real',$datos);
		campoHorasReales('horasVs',$datos);
	cierraColumnaCampos();

	echo "<br clear='all'><br/>";
	abreColumnaCampos();
	//campoRadio('corrientePago','Esta al corriente de pago',$datos);
	campoNumero('horasPrevistas','Horas previstas',$datos['horasPrevistas']);
	campoTexto('totalHoras','Total horas dedicadas',$datos,'input-small',true);
	campoTexto('mail','Mail informativo, formacion y vigilancia de la salud',$datos);
	campoFechaBlanco('fechaMail','Fecha email',$datos);
	areaTexto('observaciones','Observaciones',$datos,'areaInforme');
	campoSelect('mesVencimiento','Mes vencimiento',$mesesNombres,$mesesNombres,$datos,'selectpicker span2 show-tick');
	cierraColumnaCampos();

	campoOculto($datos,'firmaTecnico','');

	cierraVentanaGestion('index.php',true);
}

function campoHorasReales($nombreCampo,$valor=false){
	/*$datos=array();
	if(!$valor || ($valor && $valor[$nombreCampo]=='')){
		$datos[0]='';
		$datos[1]='';
	} 
	else {
		$datos=explode(':',$valor[$nombreCampo]);	
		if(!isset($datos[1])){
			$datos[1]='';		
		}
	}
	campoNumero($nombreCampo,'Horas dedicadas',formateaHoraWeb($datos[$nombreCampo]),'input-mini pagination-right horas',0);
	campoNumero($nombreCampo.'_minutos','Minutos',$datos[1],'input-mini pagination-right minutos',0);*/

	campoNumero($nombreCampo,'Horas dedicadas',$valor);
    
}

function definirPOST(){
	/*$campos=array('horasVisita','horasPlanPrev','horasPap','horasInfo','horasMemoria','horasVs');
	foreach ($campos as $campo) {
		$_POST[$campo]=$_POST[$campo.'_horas'].':'.$_POST[$campo.'_minutos'];
	}*/
}

function completarDatosGeneralesPRL($formulario,$cliente){
	$cliente = datosRegistro('clientes',$cliente);

	$formulario['pregunta3'] = $formulario['pregunta3'] == '' ? $cliente['EMPNOMBRE'] : $formulario['pregunta3']; 
	$formulario['pregunta4'] = $formulario['pregunta4'] == '' ? $cliente['EMPDIR'] : $formulario['pregunta4']; 
	$formulario['pregunta5'] = $formulario['pregunta5'] == '' ? $cliente['EMPLOC'] : $formulario['pregunta5']; 
	$formulario['pregunta6'] = $formulario['pregunta6'] == '' ? $cliente['EMPTEL1'] : $formulario['pregunta6']; 
	$formulario['pregunta12'] = $formulario['pregunta12'] == '' ? $cliente['EMPEMAIL'] : $formulario['pregunta12']; 
	$formulario['pregunta9'] = $formulario['pregunta9'] == '' ? $cliente['EMPCIF'] : $formulario['pregunta9']; 
	$formulario['pregunta10'] = $formulario['pregunta10'] == '' ? $cliente['EMPCP'] : $formulario['pregunta10']; 
	$formulario['pregunta11'] = $formulario['pregunta11'] == '' ? $cliente['EMPPROV'] : $formulario['pregunta11']; 
	$formulario['pregunta13'] = $formulario['pregunta13'] == '' ? $cliente['EMPACTIVIDAD'] : $formulario['pregunta13']; 

	return $formulario;
}


function campoSelectContrato($datos){
	$contratos=consultaBD('SELECT * FROM contratos',true);	
	$nombres=array();
	$valores=array();
	while($contrato=mysql_fetch_assoc($contratos)){
		$cliente=consultaBD('SELECT clientes.* FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo='.$contrato['codigoOferta'],true,true);
		$referencia=formateaReferenciaContrato($cliente,$contrato);
		array_push($nombres, 'Nº: '.$referencia.' - '.$cliente['EMPMARCA']);
		array_push($valores, $contrato['codigo']);
	}
	campoSelect('codigoContrato','Contrato',$nombres,$valores,$datos);
}

//Parte de Toma de Datos

function tomaDatos(){
	abreVentanaGestionConBotones('Toma de datos PRL','index.php','','icon-edit','',true,'noAjax');
	$datos=compruebaDatosTomaDatos();

	camposDatosGenerales($datos);
	camposTrabajadoresSensibles($datos);
	camposEmergencia($datos);
	camposMateriasPrimas($datos);
	camposCentro($datos);
	camposInstalaciones($datos);
	camposAlarmas($datos);
	camposIncendio($datos);
	camposPrimerosAuxilios($datos);
	camposVestuarios($datos);
	camposPuestosTrabajos($datos);
	camposObservaciones($datos);
	campoFirma($datos,'firmaTecnico','Firma del acompañante');

	cierraVentanaGestion('index.php',true);
}

function compruebaDatosTomaDatos(){
	$codigoPRL=$_GET['codigoPRL'];

	campoOculto($codigoPRL,'codigoPRL');

	$datos=datosRegistro('formularioPRL',$codigoPRL);

	$formulario=explode('&{}&',$datos['formulario']);

    if(count($formulario)>1){

        foreach($formulario as $pregunta){
            $partes=explode('=>',$pregunta);

            $datos[$partes[0]]=$partes[1];
        }
    } 
    else {

    	$cliente=consultaBD("SELECT EMPNOMBRE, EMPNTRAB, EMPCIF, EMPDIR, EMPCP, EMPTELPRINC, EMPACTIVIDAD, EMPCNAE, EMPMUTUA
    						 FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='".$datos['codigoContrato']."';",true,true);


    	$datos['pregunta0']=$cliente['EMPNOMBRE'];
    	$datos['pregunta1']=$cliente['EMPCIF'];
    	$datos['pregunta2']=$cliente['EMPDIR'];
    	$datos['pregunta3']=$cliente['EMPCP'];
    	$datos['pregunta4']=$cliente['EMPACTIVIDAD'];
    	$datos['pregunta5']=$cliente['EMPCNAE'];
    	//$datos['pregunta6']=formateaCentrosTrabajoTomaDatos($cliente);
    	$datos['pregunta6']='';
    	$datos['pregunta7']=$cliente['EMPTELPRINC'];
    	$datos['pregunta8']='';
    	$datos['pregunta9']='';
    	$datos['pregunta10']='';
    	$datos['pregunta11']='';
    	$datos['pregunta12']=$cliente['EMPNTRAB'];
    	$datos['pregunta50']=$cliente['EMPMUTUA'];
    	$datos['pregunta51']='';
    	$datos['pregunta52']='';
    	$datos['pregunta53']='';
    	$datos['pregunta54']='';

        for($i=13;$i<=49;$i++){//El 50 es la mutua!
			$datos['pregunta'.$i]='';
        }
    }

    return $datos;
}

function obtenerCentrosTrabajo($cliente){
	$cliente=consultaBD('SELECT clientes.codigo FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='.$cliente,true,true);
	$res=array();
	$res['valores']=array();
	$res['nombres']=array();
	$centros=consultaBD('SELECT * FROM clientes_centros WHERE codigoCliente='.$cliente['codigo'],true);
	while($centro=mysql_fetch_assoc($centros)){
		array_push($res['valores'], $centro['codigo']);
		array_push($res['nombres'], $centro['direccion'].', '.$centro['localidad'].' - CP: '.$centro['cp'].' ('.$centro['provincia'].')');
	}
	return $res;
}

function camposDatosGenerales($datos){
	$centros=array();
	$centros=obtenerCentrosTrabajo($datos['codigoContrato']);
	echo "<h3 class='apartadoFormulario'>Datos Generales</h3>";
	abreColumnaCampos('span3 camposGeneralesTomaDatos');

	campoOculto($datos['pregunta15'],'pregunta15Anterior','NO');

	campoRadioFormulario(15,'Cambio datos del contrato',$datos);
	campoTextoFormulario(0,'Razón social',$datos,'input-large');
	campoTextoFormulario(1,'CIF',$datos,'input-small');
	campoTextoFormulario(2,'Domicilio',$datos);
	campoTextoFormulario(3,'Código Postal',$datos,'input-mini pagination-right');
	campoSelectConsultaFormulario(4,'Actividad','SELECT codigo,nombre AS texto FROM actividades ORDER BY nombre',$datos);
	campoCNAEFormulario(5,'CNAE',$datos);
	//campoTextoFormulario(6,'Centro/s de trabajo',$datos);
	campoSelectFormulario(6,'Centro/s de trabajo',$datos,0,$centros['nombres'],$centros['valores']);
	campoTextoFormulario(7,'Teléfono',$datos,'input-small pagination-right');
	
	cierraColumnaCampos();
	abreColumnaCampos();
	
	campoTextoFormulario(8,'Acompañado en la visita por',$datos);
	campoFechaFormulario(9,'Fecha',$datos);
	campoTextoFormulario(10,'Horarios',$datos);
	campoRadioFormulario(11,'Preferencia documentación',$datos,'Formato electrónico',array('Formato electrónico','Formato papel'),array('Formato electrónico','Formato papel'));
	campoTextoFormulario(12,'Nº trabajadores',$datos,'input-mini pagination-right');
	campoTextoFormulario(50,'Mutua',$datos);
	campoRadioFormulario(13,'Trabajadores autónomos',$datos);
	campoRadioFormulario(14,'Realiza obras de construcción',$datos);
	campoObservacionesDatosGenerales($datos);

	cierraColumnaCampos(true);
}

function campoObservacionesDatosGenerales($datos){
	echo "<div class='hide' id='cajaObservacionesDatosGenerales'>";
	areaTextoFormulario(16,'Observaciones',$datos);
	echo "</div>";
}


function camposTrabajadoresSensibles($datos){
	echo "<h3 class='apartadoFormulario'>Trabajadores sensibles</h3>";
	abreColumnaCampos();

	campoRadioFormulario(17,'Menores de edad',$datos);
	campoRadioFormulario(18,'Embarazadas',$datos);
	campoRadioFormulario(51,'Minusvalía - incapacidad',$datos);
	campoTextoFormulario(19,'Otros (indicar el supuesto)',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	creaTablaPuestosTomaDatos($datos['codigo'],$datos['codigoContrato'],'SI');

	cierraColumnaCampos(true);
}

function creaTablaPuestosTomaDatos($codigoPRL,$codigoCliente,$sensible){
		$cliente=consultaBD('SELECT clientes.codigo FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='.$codigoCliente,true,true);
		$codigoCliente=$cliente['codigo'];
	    echo "
	    <div class='control-group'>                     
	        <label class='control-label'>Indica el/los puesto/s:</label>
	        <div class='controls'>
	            <table class='tabla-simple mitadAncho' id='tablaPuestosTrabajo".$sensible."'>
	                <thead>
	                    <tr>
	                        <th> Puesto de trabajo </th>
	                        <th> </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($codigoPRL!=false){
	                $consulta=consultaBD("SELECT codigoPuestoTrabajo FROM puestos_trabajo_formulario_prl WHERE codigoFormularioPRL='$codigoPRL' AND sensible='$sensible'");
	                while($puesto=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaPuestosTrabajo($i,$codigoCliente,$puesto,$sensible);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaPuestosTrabajo(0,$codigoCliente,false,$sensible);
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaPuestosTrabajo".$sensible."\");'><i class='icon-plus'></i> Añadir puesto</button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaPuestosTrabajo".$sensible."\");'><i class='icon-trash'></i> Eliminar puesto</button>
	        </div>
	    </div>";
}

function imprimeLineaTablaPuestosTrabajo($i,$codigoCliente,$datos,$sensible){
	$j=$i+1;

    echo "<tr>";
        campoSelectConsulta('codigoPuestoTrabajo'.$sensible.$i,'',"SELECT codigo, nombre AS texto FROM puestos_trabajo ORDER BY nombre;",$datos['codigoPuestoTrabajo'],'selectpicker span3 show-tick','data-live-search="true"','',1,false);
        campoOculto($sensible,'sensible'.$sensible.$i);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}


function camposEmergencia($datos){
	echo "<h3 class='apartadoFormulario'>Procedimientos de emergencia</h3>";
	
	abreColumnaCampos();
	
	campoRadioFormulario(20,'Dispone la empresa de algún procedimiento de emergencia',$datos);
	
	cierraColumnaCampos();
	abreColumnaCampos();

	areaTextoFormulario(52,'Comentarios',$datos);
	
	cierraColumnaCampos(true);
}


function camposMateriasPrimas($datos){
	    echo "<br />
	    <h3 class='apartadoFormulario'>Materias primas utilizadas</h3>
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaMateriasPrimas'>
	            <thead>
	                <tr>
	                    <th> Materia </th>
	                    <th> Lugar </th>
	                    <th> Cantidad </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM materias_primas_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."';");
	                while($materia=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaMateriasPrimas($i,$materia);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaMateriasPrimas(0,false);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaMateriasPrimas\");'><i class='icon-plus'></i> Añadir materia prima</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaMateriasPrimas\");'><i class='icon-trash'></i> Eliminar materia prima</button>
	    </div><br />";
}

function imprimeLineaTablaMateriasPrimas($i,$datos){
	$j=$i+1;

    echo "<tr>";
        	campoTextoTabla('materiaPrima'.$i,$datos['materia']);
        	campoTextoTabla('lugar'.$i,$datos['lugar']);
        	campoTextoTabla('cantidad'.$i,$datos['cantidad']);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}


function camposCentro($datos){
	echo "<h3 class='apartadoFormulario'>Características del centro</h3>";
	abreColumnaCampos();

	campoNumeroFormulario(21,'Superficie total (m<sup>2</sup>)',$datos);
	campoNumeroFormulario(22,'Ocupación (nº personas)',$datos);
	campoNumeroFormulario(23,'Nº de plantas',$datos);
	//areaTextoFormulario(51,'Enumeración de instalaciones',$datos,'campoInstalaciones');
	creaTablaInstalaciones($datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	campoRadioFormulario(24,'Sectorización',$datos);
	campoTextoFormulario(25,'Tipo de estructura',$datos);
	campoRadioFormulario(49,'Planos disponibles',$datos);
	campoFicheroPlano($datos);

	cierraColumnaCampos(true);
}

function campoFicheroPlano($datos){
	echo "<div class='hide' id='cajaPlanos'>";
			campoFichero('ficheroPlano','Fichero de planos',0,$datos,'../documentos/planificacion/','Ver/Descargar');
	echo "</div>";
}

function camposInstalaciones($datos){
	echo "<h3 class='apartadoFormulario'>Instalaciones que pueden generar una emergencia</h3>";
	abreColumnaCampos('span4');
	campoDato('Suministro eléctrico','');
	campoNumeroFormulario(26,'Voltaje (V)',$datos);
	campoNumeroFormulario(27,'Potencia contratada (Kw)',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	areaTextoFormulario(28,'Riesgos especiales (explosión, inundación, etc)',$datos);

	cierraColumnaCampos(true);
}

function camposAlarmas($datos){
	echo "<h3 class='apartadoFormulario'>Medidas de alarma</h3>";
	abreColumnaCampos();

	campoRadioFormulario(29,'Alarma general',$datos);
	campoRadioFormulario(30,'Detección',$datos);
	campoRadioFormulario(31,'Walky Talky',$datos);
	areaTextoFormulario(35,'Observaciones',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();

	campoRadioFormulario(32,'Pulsadora de alarma',$datos);
	campoRadioFormulario(33,'Teléfono interno',$datos);
	campoRadioFormulario(34,'Otros',$datos);;

	cierraColumnaCampos(true);
}

function camposIncendio($datos){
	echo "<h3 class='apartadoFormulario'>Medidas de protección contra incendios</h3>";
	
	tablaExtintores($datos);
	tablaBIE($datos);
	tablaOtros($datos);
}

function tablaExtintores($datos){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaExtintores'>
	            <thead>
	            	<tr>
	            		<th colspan='8'>EXTINTORES</th>
	            	</tr>
	                <tr>
	                    <th> Nº </th>
	                    <th> KG </th>
	                    <th> Tipo </th>
	                    <th> Eficacia </th>
	                    <th> Señalizado </th>
	                    <th> Revisado </th>
	                    <th> Ubicación </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM extintores_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."';");
	                while($extintor=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaExtintores($i,$extintor);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaExtintores(0,false);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaExtintores\");'><i class='icon-plus'></i> Añadir extintor</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaExtintores\");'><i class='icon-trash'></i> Eliminar extintor</button>
	    </div><br />";
}

function imprimeLineaTablaExtintores($i,$datos){
	$j=$i+1;

    echo "<tr>";
        	campoTextoTabla('numero'.$i,$datos['numero'],'input-small pagination-right');
        	campoTextoTabla('kg'.$i,$datos['kg'],'input-mini pagination-right');
        	campoTextoTabla('tipo'.$i,$datos['tipo'],'input-small pagination-right');
        	campoTextoTabla('eficacia'.$i,$datos['eficacia'],'input-small pagination-right');
        	campoSelect('senializado'.$i,'',array('Si','No'),array('SI','NO'),$datos['senializado'],'selectpicker span2 show-tick',
        		'data-live-search="true"',1);
        	campoSelect('revisado'.$i,'',array('Si','No'),array('SI','NO'),$datos['revisado'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	areaTextoTabla('ubicacionExtintor'.$i,$datos['ubicacion']);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function tablaBIE($datos){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaBIE'>
	            <thead>
	            	<tr>
	            		<th colspan='7'>BOCAS DE INCENDIO EQUIPADAS (BIE)</th>
	            	</tr>
	                <tr>
	                    <th> Nº </th>
	                    <th> Medida </th>
	                    <th> Presión </th>
	                    <th> Señalizado </th>
	                    <th> Revisado </th>
	                    <th> Ubicación </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM bie_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."';");
	                while($extintor=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaBIE($i,$extintor);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaBIE(0,false);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaBIE\");'><i class='icon-plus'></i> Añadir BIE</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaBIE\");'><i class='icon-trash'></i> Eliminar BIE</button>
	    </div><br />";
}

function imprimeLineaTablaBIE($i,$datos){
	$j=$i+1;

    echo "<tr>";
        	campoTextoTabla('numeroBIE'.$i,$datos['numero'],'input-small pagination-right');
        	campoSelect('medidaBIE'.$i,'',array('25 mm','45 mm'),array('25','45'),$datos['medida'],'selectpicker span2 show-tick',
        		'data-live-search="true"',1);
        	campoTextoTabla('presion'.$i,$datos['presion'],'input-small pagination-right');
        	campoSelect('senializado'.$i,'',array('Si','No'),array('SI','NO'),$datos['senializado'],'selectpicker span2 show-tick',
        		'data-live-search="true"',1);
        	campoSelect('revisado'.$i,'',array('Si','No'),array('SI','NO'),$datos['revisado'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	areaTextoTabla('ubicacion'.$i,$datos['ubicacion']);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function tablaOtros($datos){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaOtros'>
	            <thead>
	            	<tr>
	            		<th colspan='2'>OTROS</th>
	            	</tr>
	            </thead>
	            <tbody>";
	        	echo '<tr><td>Columna seca</td>';
	          	campoSelectFormulario(35,'',$datos,1);
	          	echo '</tr><tr><td>Hidrante exterior</td>';
	          	campoSelectFormulario(36,'',$datos,1);
	          	echo '</tr><tr><td>Medios humanos (Equipos de emergencia convenientemente formados)</td>';
	          	campoSelectFormulario(37,'',$datos,1);
	          	echo '</tr><tr><td>Punto de reunión señalizado</td>';
	          	campoSelectFormulario(38,'',$datos,1);
	          	echo '</tr><tr><td>Lugar</td>';
	          	campoTextoTablaFormulario(39,$datos);
	    echo "	</tbody>
	        </table>
	    </div><br />";
}

function camposPrimerosAuxilios($datos){
	echo "<h3 class='apartadoFormulario'>Primeros auxilios</h3>";
	abreColumnaCampos();
	campoTextoFormulario(40,'Cantidad de botiquines',$datos,'input-mini pagination-right');
	tablaPrimeros($datos,'botiquines_formulario_prl','Botiquines');
	campoTextoFormulario(41,'Cantidad de locales de primeros auxilios',$datos,'input-mini pagination-right');
	tablaPrimeros($datos,'locales_formulario_prl','Locales');
	cierraColumnaCampos();
	abreColumnaCampos();
	campoTextoFormulario(42,'Cantidad de medios humanos',$datos,'input-mini pagination-right');
	tablaPrimeros($datos,'medios_humanos_formulario_prl','Medios');

	campoRadioFormulario(53,'Formación en primeros auxilios',$datos);
	campoRadioFormulario(54,'Desfibrilador',$datos);

	cierraColumnaCampos(true);
}

function tablaPrimeros($datos,$tabla,$prefijo){
		
		$th='';
		if($prefijo!='Medios'){
			$th='<th> Señalizado </th>';
		} 
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='".$tabla."'>
	            <thead>
	                <tr>
	                    <th> Lugar </th>
	                    ".$th."
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM ".$tabla." WHERE codigoFormularioPRL='".$datos['codigo']."';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaPrimeros($i,$item,$prefijo);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaPrimeros(0,false,$prefijo);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"".$tabla."\");'><i class='icon-plus'></i> Añadir lugar</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"".$tabla."\");'><i class='icon-trash'></i> Eliminar lugar</button>
	    </div><br />";
}

function imprimeLineaTablaPrimeros($i,$datos,$prefijo){
	$j=$i+1;

    echo "<tr>";
        	campoTextoTabla('ubicacion'.$prefijo.$i,$datos['ubicacion']);
        	if($prefijo!='Medios'){
        	campoSelect('senializado'.$prefijo.$i,'',array('Si','No'),array('SI','NO'),$datos['senializado'],'selectpicker span2 show-tick',
        		'data-live-search="true"',1);
        	}
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function camposVestuarios($datos){
	echo "<h3 class='apartadoFormulario'>Vestuarios</h3>";
	abreColumnaCampos();

	campoRadioFormulario(41,'Lavabo/s',$datos);
	campoTextoFormulario(42,'Cantidad',$datos,'input-mini pagination-right');
	campoRadioFormulario(43,'Ducha/s',$datos);
	campoTextoFormulario(44,'Cantidad',$datos,'input-mini pagination-right');
	cierraColumnaCampos();
	abreColumnaCampos();

	campoRadioFormulario(45,'¿Mixto?',$datos);
	campoRadioFormulario(46,'Taquillas',$datos);
	campoRadioFormulario(47,'En caso anterior afirmativo, ¿disponen de compartimentos separados?',$datos);;

	cierraColumnaCampos(true);
}

function camposPuestosTrabajos($datos){
	echo "<h3 class='apartadoFormulario'>Puestos de trabajos</h3>";
	abreColumnaCampos();
	creaTablaPuestosTomaDatos($datos['codigo'],$datos['codigoContrato'],'NO');
	cierraColumnaCampos(true);
}

function camposObservaciones($datos){
	echo "<h3 class='apartadoFormulario'>Observaciones</h3>";
	abreColumnaCampos();
	areaTextoFormulario(48,'Observaciones',$datos,'areaInforme');
	cierraColumnaCampos(true);
}

//Fin parte Toma de Datos

/*function tomaDatos(){
	$prl =datosRegistro('formularioPRL',$_GET['codigoPRL']);
	$formulario = recogerFormularioServicios($prl);
	$formulario = completarDatosGeneralesPRL($formulario,$prl['codigoCliente']);

	abreVentanaGestionConBotones('Toma de datos PRL','index.php','','icon-edit','margenAb');
	campoOculto($_GET['codigoPRL'],'codigoPRL');
	abreColumnaCampos();
		campoTextoFormulario(1,'Consultor',$formulario);
		//campoRadio('toma_datos'.'Toma de datos finalizada',$trabajo['toma_datos']);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoFechaFormulario(2,'Fecha',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DATOS GENERALES</h3>";

	abreColumnaCampos();
		campoTextoFormulario(3,'Razón Social',$formulario);
		campoTextoFormulario(4,'Dirección Social',$formulario);
		campoTextoFormulario(10,'Código postal',$formulario);
		campoTextoFormulario(8,'Representate legal',$formulario);
		campoTextoFormulario(6,'Teléfono',$formulario);
		campoTextoFormulario(7,'Nº de trabajadores',$formulario);
		campoRadioFormulario(16,'¿Hay trabajadores en centros ajenos?',$formulario);
		campoTextoFormulario(67,'Sector',$formulario);
		campoTextoFormulario(68,'Persona de contacto',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(9,'CIF',$formulario);
		campoTextoFormulario(5,'Localidad',$formulario);
		campoTextoFormulario(11,'Provincia',$formulario);
		campoTextoFormulario(14,'NIF',$formulario);
		campoTextoFormulario(12,'Email',$formulario);
		campoTextoFormulario(15,'Nº de centros',$formulario);
		echo "<br/><br/><br/>";
		campoTextoFormulario(13,'Actividad empresarial',$formulario);
		campoTextoFormulario(69,'Responsable del Plan de Prevención',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DESCRIPCIÓN DEL CENTRO</h3>";

	abreColumnaCampos();
		campoTextoFormulario(17,'Localización (zona industrial, comercial, residencial)',$formulario);
		campoTextoFormulario(19,'Nº de plantas',$formulario);
		campoTextoFormulario(21,'Aforo máximo',$formulario);
		
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(18,'Superficie m2',$formulario);
		echo "<br/>";
		campoTextoFormulario(20,'Tipo de plantas (bajos, primera planta, ático)',$formulario);
		campoRadioFormulario(22,'¿Mismo nivel?',$formulario);
	cierraColumnaCampos();

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Nombre y apellidos' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaEmpleadosPRL'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
                  			<th colspan='6'>Trabajadores</th>
                  		</tr>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Nombre y apellidos </th>
							<th> DNI </th>
							<th> Puesto de trabajo </th>
							<th> Descripción funciones </th>
							<th> ¿Salen fuera del centro? </th>
							<th> En caso afirmativo ¿Tipo de vehículo? </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($prl){
        	$empleados = consultaBD("SELECT * FROM empleados_prl WHERE codigoTrabajo=".$prl['codigo'],true);
        	while($empleado = mysql_fetch_assoc($empleados)){
        		echo "<tr>";
    				campoTextoTabla('nombreEmpleadoPRL'.$indice,$empleado['nombre']);
    				campoTextoTabla('dniEmpleadoPRL'.$indice,$empleado['dni'],'input-small');
    				campoTextoTabla('puestoEmpleadoPRL'.$indice,$empleado['puesto'],'input-small');
    				areaTextoTabla('funcionesEmpleadoPRL'.$indice,$empleado['funciones']);
    				campoSelect('salenEmpleadoPRL'.$indice,'',array('Si','No'),array('SI','NO'),$empleado['salen'],'selectpicker span2 show-tick','data-live-search="true"',1);
    				campoTextoTabla('vehiculoEmpleadoPRL'.$indice,$empleado['vehiculo'],'input-small');
				echo"
					</tr>";
        		$indice++;
        	}
        }
    	for($indice;$indice<5;$indice++){
    		echo "<tr>";
    			campoTextoTabla('nombreEmpleadoPRL'.$indice);
    			campoTextoTabla('dniEmpleadoPRL'.$indice,'','input-small');
    			campoTextoTabla('puestoEmpleadoPRL'.$indice,'','input-small');
    			areaTextoTabla('funcionesEmpleadoPRL'.$indice);
    			campoSelect('salenEmpleadoPRL'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
    			campoTextoTabla('vehiculoEmpleadoPRL'.$indice,'','input-small');
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaEmpleadosPRL\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Centro de trabajo' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaCentrosPRL'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
                  			<th colspan='5'>Centros de trabajo</th>
                  		</tr>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Centro de trabajo </th>
							<th> Nº delegados<br/>de prevención </th>
							<th> Comite de<br/>seguridad y salud </th>
							<th> Trabajador designado </th>
							<th> Consulta directa <br/>a los trabajadores </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($prl){
        	$centros = consultaBD("SELECT * FROM centros_prl WHERE codigoTrabajo=".$prl['codigo'],true);
        	while($centro = mysql_fetch_assoc($centros)){
        		echo "<tr>";
    				campoTextoTabla('nombreCentroPRL'.$indice,$centro['nombre']);
    				campoTextoTabla('delegadoCentroPRL'.$indice,$centro['delegado'],'input-small');
    				campoTextoTabla('comiteCentroPRL'.$indice,$centro['comite']);
    				campoTextoTabla('trabajadorCentroPRL'.$indice,$centro['trabajador']);
    				campoSelect('consultaCentroPRL'.$indice,'',array('Si','No'),array('SI','NO'),$centro['consulta'],'selectpicker span2 show-tick','data-live-search="true"',1);
				echo"
					</tr>";
        		$indice++;
        	}
        }

    		echo "<tr>";
    			campoTextoTabla('nombreCentroPRL'.$indice);
    			campoTextoTabla('delegadoCentroPRL'.$indice,'','input-small');
    			campoTextoTabla('comiteCentroPRL'.$indice);
    			campoTextoTabla('trabajadorCentroPRL'.$indice);
    			campoSelect('consultaCentroPRL'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
			echo"
				</tr>";
    
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaCentrosPRL\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";

	echo "<br clear='all'><h3 class='apartadoFormulario'> INSTALACIONES</h3>";
	abreColumnaCampos();
		campoRadioFormulario(23,'Dispone de instalación de acondicionamiento de aire (calefacción, aire)',$formulario);
		campoRadioFormulario(32,'¿Dispone de cuadro electríco',$formulario);
		echo "<div id='div32' class='hidden'>";
			campoRadioFormulario(33,'Está señalizado',$formulario);
			campoTextoFormulario(34,'Ubicación',$formulario);
		echo "</div>";
		campoRadioFormulario(25,'Dispone de instalación de extracción de humos (campana extractora)',$formulario);


	cierraColumnaCampos();

	abreColumnaCampos();
		echo "<div id='div23' class='hidden'>";
		campoRadioFormulario(24,'¿Cuales?',$formulario,'CALEFACCION',array('Calefaccion','Aire'),array('CALEFACCION','AIRE'));
		echo "</div>";
		echo "<br/><br/>";
		campoRadioFormulario(26,'Dispone de Montacargas/ascensores',$formulario);
		echo "<div id='div26' class='hidden'>";
			campoTextoFormulario(27,'¿Cuantos?',$formulario);
			campoTextoFormulario(28,'Empresa de mantenimiento',$formulario);
		echo "</div>";
		echo "Dispones de otras:";
		campoRadioFormulario(29,'Aire',$formulario);
		campoRadioFormulario(30,'Gas',$formulario);
		campoRadioFormulario(31,'Frigos',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> ORGANIZACIÓN EMPRESA</h3>";

	abreColumnaCampos();
		campoRadioFormulario(75,'Dirección',$formulario,'INTERMEDIO',array('Mando intermedio','Trabajadores'),array('INTERMEDIO','TRABAJADORES'),true);
		campoRadioFormulario(35,'Qué medio de comunicación se utiliza para la prevención',$formulario,'ORAL',array('Oral','Correo electrónico','Carta'),array('ORAL','CORREO','CARTA'),true);
		campoRadioFormulario(36,'Qué recursos dispone para esta actividad:',$formulario,'OFICINA',array('Oficina','Equipos informáticos','Impresora','Otros'),array('OFICINA','EQUIPOS','IMPRESORA','OTROS'),true);
		campoTextoFormulario(71,'Trabajador responsable de la seguridad',$formulario);
		campoTextoFormulario(72,'Trabajador responsable de la ergonomía y psicología',$formulario);

	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(76,'Tipo de prevención',$formulario,'EMPRESARIO',array('Empresario','Trabajador designado'),array('EMPRESARIO','TRABAJADOR'),true);
		campoRadioFormulario(66,'Formación del responsable de prevención',$formulario,'BASICO',array('Básico','Medio','Superior'),array('BASICO','MEDIO','SUPERIOR'),true);
		campoRadioFormulario(70,'Servicio de prevencion:',$formulario,'PROPIO',array('Propio','Mancomunado','Ajeno','Sistema Mixto'),array('PROPIO','MANCOMUNADO','AJENO','MIXTO'),true);
		campoTextoFormulario(73,'Trabajador responsable de la higiene',$formulario);
		campoTextoFormulario(74,'Trabajador responsable de la vigilancia de la salud',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> MEDIDAS DE EMERGENCIA</h3>";

	abreColumnaCampos();
		campoRadioFormulario(37,'¿Alguien en la empresa dispone de formación en primeros auxilios?',$formulario);
		echo "<br/>";
		campoTextoFormulario(38,'Instalación de seguridad contraincendios perteneciente al edificio donde se ubica el local o la oficina',$formulario);
		campoTextoFormulario(41,'Nº de Bies',$formulario);
		echo "<br/>";
		campoRadioFormulario(43,'Sistema de detección automática de incendios',$formulario);
		campoRadioFormulario(45,'Extintores',$formulario);
		echo "<div id='div45' class='hidden'>";
			echo "Tipos y carácteristicas:";
			campoRadioFormulario(47,'ABC',$formulario);
			campoRadioFormulario(48,'Espuma',$formulario);
			campoRadioFormularioConLogo(51,'Señal de seguridad de cada extintor',$formulario,'iconExtintor.jpg');
		echo "</div>";
		campoRadioFormulario(53,'En todo caso existirá el alumbrado de emergencia en las puertas de salida',$formulario);
		campoRadioFormulario(55,'Sistema de alarmas',$formulario,'AUTOMATICA',array('Alarma automática de incendios','Megafonía/telefonía','Alarma manual de incendios (con pulsador de alarma)'),array('AUTOMATICA','MEGAFONIA','MANUAL'),true);

	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(39,'¿Alguien en la empresa dispone de formación en extinción de incendios?',$formulario);
		campoTextoFormulario(40,'Instalación de seguridad contraincendios formada por red de agua y Bies (Bocas de incendio equipadas)',$formulario);
		campoRadioFormularioConLogo(42,'Señal de Seguridad de cada BIE',$formulario,'iconBie.png');
		campoRadioFormulario(44,'Sistema de actuación automática de incendios (sprinkkles)',$formulario);
		echo "<div id='div45_1' class='hidden'>";
			campoTextoFormulario(46,'Nº de Extintores',$formulario);
			echo "<br/>";
			campoRadioFormulario(49,'CO2',$formulario);
			campoRadioFormulario(50,'Otros',$formulario);
		echo "</div>";
		echo "<br/>";
		campoRadioFormulario(52,'Alumbrado de emergencia',$formulario);
		campoRadioFormulario(54,'Nº de lámparas alumbrado de emergencia',$formulario);
		echo "<div id='div26_1' class='hidden'>";
			campoRadioFormularioConLogo(56,'Si dispone de ascensores/montacargas deberá contar con la preceptiva señal de seguridad',$formulario,'iconAscensor.png');
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> RECURSOS TÉCNICOS EN CASO DE EVACUACIÓN</h3>";
	abreColumnaCampos();
		campoRadioFormularioConLogo(57,'Señal de salvamento. Recorrido de evacuación horizontal',$formulario,'iconHorizontal.jpg');
		campoRadioFormularioConLogo(58,'Señal de salvamento. Recorrido de evacuación vertica',$formulario,'iconVertical.png');
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormularioConLogo(59,'Señal de salvamento. Recorrido de evacuación',$formulario,'iconDireccion.png');
		campoRadioFormularioConLogo(60,'Señal de salvamento. Teléfono de salvamento',$formulario,'iconTelefono.png');
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> RECURSOS TÉCNICOS EN CASO DE PRIMEROS AUXILIOS</h3>";
	abreColumnaCampos();
		campoRadioFormularioConLogo(61,'Botiquín. En todo caso existirá en el centro de trabajo un botiquín de primeros auxilios junto con la señalización informativa correspondiente',$formulario,'iconBotiquin.png');
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormularioConLogo(62,'Sistema lavaojos. Señal informativa',$formulario,'iconLavaojos.png');
		campoRadioFormulario(63,'Otros',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> VIGILANCIA DE SALUD</h3>";
	abreColumnaCampos();
		campoRadioFormulario(64,'¿Dispone usted de un servicio de Vigilancia de la Salud (reconocimientos médicos)?',$formulario);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormulario(65,'¿En caso negativo, querría que le pusiéramos en contacto con un colaborador nuestro?',$formulario);
	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}*/

function imprimeFormularioPRL($where){
	global $_CONFIG;

	$where=defineWhereEjercicio($where,array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaVs','fechaMail'));
	if($_SESSION['tipoUsuario']!='ADMIN'){
		$where.=' AND tecnico='.$_SESSION['codigoU'];
	}
	conexionBD();
	$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, visitada, planPrev, pap, info, f.horasPrevistas, memoria, fechaVisita, fechaPlanPrev, fechaPap, fechaInfo,  fechaMemoria, vs, fechaVs, baja, tecnico, EMPMARCA AS cliente,f.codigoContrato, ofertas.codigoCliente FROM formularioPRL f LEFT JOIN contratos ON f.codigoContrato=contratos.codigo LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND f.eliminado='NO' AND f.aprobado='NO';");


	/*if($_SESSION['tipoUsuario'] == 'ADMIN'){
		$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, c.razon_s AS cliente FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo ORDER BY c.razon_s;");
	} else {
		$director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],true, true);
		if($director['director'] == 'SI'){
			$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, c.razon_s AS cliente FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE c.empleado IN (SELECT codigo FROM usuarios WHERE tipo LIKE '".$director['tipo']."') ORDER BY c.razon_s;");
		} else {
			$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, c.razon_s AS cliente FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE c.empleado LIKE ".$_SESSION['codigoU']." ORDER BY c.razon_s;");
		}
	}*/
	
	$tabla='';
	$iconoValidado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		
		$referencia='';
		if($datos['codigoContrato']!=NULL){
			$contrato=consultaBD('SELECT * FROM contratos WHERE codigo='.$datos['codigoContrato'],false,true);
			$cliente=consultaBD('SELECT * FROM clientes WHERE codigo='.$datos['codigoCliente'],false,true);
			$referencia=formateaReferenciaContrato($cliente,$contrato);
		}
		$tabla.= "
			<tr>
				<td>".$referencia."</td>
				<td>".$datos['cliente']."</td>
				<td class='centro'>".$iconoValidado[$datos['visitada']];
				if($datos['visitada'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaVisita']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['planPrev']];
				if($datos['planPrev'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaPlanPrev']);
                }
                $tabla.="</td>		
				<td class='centro'>".$iconoValidado[$datos['pap']];
				if($datos['pap'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaPap']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['info']];
				if($datos['info'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaInfo']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['memoria']];
				if($datos['memoria'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaMemoria']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['vs']];
				if($datos['vs'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaVs']);
                }
                $tabla.="</td>	
				<td class='centro'>".$datos['horasPrevistas'];
                $tabla.="</td>					 
				<td class='centro'>
					<div class='btn-group'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."planificacion-pendiente/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
						</ul>
					</div>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	cierraBD();
	echo $tabla;
}

function imprimeFormularioPRLeliminados($where=false){
	global $_CONFIG;

	$where=defineWhereEjercicio($where,array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaVs','fechaMail'));
	if($_SESSION['tipoUsuario']!='ADMIN'){
		$where.=' AND tecnico='.$_SESSION['codigoU'];
	}
	conexionBD();
	$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, visitada, planPrev, pap, info, f.horasPrevistas, memoria, fechaVisita, fechaPlanPrev, fechaPap, fechaInfo,  fechaMemoria, vs, fechaVs, baja, tecnico, EMPMARCA AS cliente,f.codigoContrato, ofertas.codigoCliente FROM formularioPRL f LEFT JOIN contratos ON f.codigoContrato=contratos.codigo LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND f.eliminado='SI' AND f.aprobado='NO';");
	
	$tabla='';
	$iconoValidado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		
		$referencia='';
		if($datos['codigoContrato']!=NULL){
			$contrato=consultaBD('SELECT * FROM contratos WHERE codigo='.$datos['codigoContrato'],false,true);
			$cliente=consultaBD('SELECT * FROM clientes WHERE codigo='.$datos['codigoCliente'],false,true);
			$referencia=formateaReferenciaContrato($cliente,$contrato);
		}
		$tabla.= "
			<tr>
				<td>".$referencia."</td>
				<td>".$datos['cliente']."</td>
				<td class='centro'>".$iconoValidado[$datos['visitada']];
				if($datos['visitada'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaVisita']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['planPrev']];
				if($datos['planPrev'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaPlanPrev']);
                }
                $tabla.="</td>		
				<td class='centro'>".$iconoValidado[$datos['pap']];
				if($datos['pap'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaPap']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['info']];
				if($datos['info'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaInfo']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['memoria']];
				if($datos['memoria'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaMemoria']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['vs']];
				if($datos['vs'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaVs']);
                }
                $tabla.="</td>	
				<td class='centro'>".$datos['horasPrevistas'];
                $tabla.="</td>					 
				<td class='centro'>
					<div class='btn-group'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."planificacion-pendiente/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
						</ul>
					</div>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	echo $tabla;
	cierraBD();
	
}

function obtieneFechaLimite($fecha){
	$fechaFinal = formateaFechaWeb($fecha);
	$fecha = new DateTime($fecha);
	$fecha->sub(new DateInterval('P7D'));
	$fechaWarning = $fecha->format('d/m/Y');
	$label = 'success';
	if(compruebaFechaRevision($fechaFinal)){
		$label = 'danger';
	} else if(compruebaFechaRevision($fechaWarning)){
		$label = 'warning';
	}
	return "<label class='label label-".$label."'>".$fechaFinal."</label>";
}

function compruebaFechaRevision($fechaRevision){
	$res=false;
	if($fechaRevision != ''){
		$fecha=explode('/',$fechaRevision);

		if($fecha[2]<date('Y')){
			$res=true;
		}
		elseif($fecha[2]==date('Y') && $fecha[1]<date('m')){
			$res=true;
		}
		elseif($fecha[2]==date('Y') && $fecha[1]==date('m') && $fecha[0]<date('d')){
			$res=true;
		}
	}

	return $res;
}

function obtieneCliente(){
	$datos=arrayFormulario();//Recibirá el codigo como si vienera de un campo del formulario
	$cliente=consultaBD("SELECT * FROM clientes WHERE codigo=".$datos['codigo'],true,true);

	echo json_encode($cliente);//IMPORTANTE: un echo, no un return, y convierte el array en formato JSON.
}

function estadisticasFormulariosRestrict($where){
	$res=array();

	conexionBD();
	$where=defineWhereEjercicio($where,array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaVs','fechaMail'));
	if($_SESSION['tipoUsuario']!='ADMIN'){
		$where.=' AND tecnico='.$_SESSION['codigoU'];
	}
	$consulta=consultaBD("SELECT COUNT(f.codigo) AS total FROM formularioPRL f LEFT JOIN contratos ON f.codigoContrato=contratos.codigo LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND f.aprobado='NO' AND f.eliminado='NO' ORDER BY EMPNOMBRE;",false,true);

	/*if($_SESSION['tipoUsuario'] == 'ADMIN'){
		$consulta=consultaBD("SELECT COUNT(f.codigo) AS total FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo ORDER BY c.razon_s;",false,true);
	} else {
		$director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],false, true);
		if($director['director'] == 'SI'){
			$consulta=consultaBD("SELECT COUNT(f.codigo) AS total FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE c.empleado IN (SELECT codigo FROM usuarios WHERE tipo LIKE '".$director['tipo']."') ORDER BY c.razon_s;",false,true);
		} else {
			$consulta=consultaBD("SELECT COUNT(f.codigo) AS total FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE c.empleado LIKE ".$_SESSION['codigoU']." ORDER BY c.razon_s;",false,true);
		}
	}*/

	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

function estadisticasPlanificacionesEliminadas($eliminado){
	$res=array();

	conexionBD();
	$where=defineWhereEjercicio("WHERE f.eliminado='$eliminado' AND f.aprobado='NO'",array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaVs','fechaMail'));
	if($_SESSION['tipoUsuario']!='ADMIN'){
		$where.=' AND tecnico='.$_SESSION['codigoU'];
	}
	$consulta=consultaBD("SELECT COUNT(f.codigo) AS total FROM formularioPRL f LEFT JOIN contratos ON f.codigoContrato=contratos.codigo LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." ORDER BY EMPNOMBRE;",false,true);


	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

function generaNumero(){
	$consulta=consultaBD("SELECT MAX(codigoInterno) AS numero FROM formularioPRL", true, true);
	
	return $consulta['numero']+1;
}

function generaEventosAgenda(){	
	conexionBD();
	$consulta=consultaBD("SELECT * FROM formularioPRL",true);
	$tareas=array();
	$i=0;
	while($prl=mysql_fetch_assoc($consulta)){
		$cliente=consultaBD('SELECT clientes.codigo FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='.$prl['codigoContrato'],true,true);
		if($prl['visitada']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaVisita'];
			$tareas[$i]['tarea']='Visita';
			$i++;
		}
		if($prl['planPrev']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaPlanPrev'];
			$tareas[$i]['tarea']='Plan prevención';
			$i++;
		}
		if($prl['er']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaEr'];
			$tareas[$i]['tarea']='Evaluación de riesgos';
			$i++;
		}
		if($prl['pap']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaPap'];
			$tareas[$i]['tarea']='Planificación preventiva';
			$i++;
		}
		if($prl['info']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaInfo'];
			$tareas[$i]['tarea']='Información a los trabajadores';
			$i++;
		}
		if($prl['cert']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaCert'];
			$tareas[$i]['tarea']='Certificados de formación';
			$i++;
		}
		if($prl['memoria']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaMemoria'];
			$tareas[$i]['tarea']='Memoria';
			$i++;
		}
		if($prl['vs']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaVs'];
			$tareas[$i]['tarea']='Vigilancia de la salud';
			$i++;
		}
	}
	$calendario="";
	
	foreach ($tareas as $datos) {
		$cliente=datosRegistro('clientes',$datos['codigoCliente']);
		$fechaInicio=explode('-',$datos['fecha']);
		$fechaFin=explode('-',$datos['fecha']);
		$fechaInicio[1]--;
		$fechaFin[1]--;
		$horaInicio=explode(':','08:00');
		$horaFin=explode(':','08:00');

	    $calendario.="
	    	{
	    		id: ".$datos['codigoInterno'].",
	    		title: '".$cliente['EMPNOMBRE']." - ".$datos['tarea']."',
			    start: new Date(".$fechaInicio[0].", ".$fechaInicio[1].", ".$fechaInicio[2].", ".$horaInicio[0].", ".$horaInicio[1]."),
		    	end: new Date(".$fechaFin[0].", ".$fechaFin[1].", ".$fechaFin[2].", ".$horaFin[0].", ".$horaFin[1]."),
		    	allDay: true,
		    	backgroundColor: '#009F93', 
		    	borderColor: '#009F93',
		    	codigoGestion: 'NO'
	    	},";
  	}

  	cierraBD();

  	$tareas=substr_replace($calendario, '', strlen($calendario)-1, strlen($calendario));//Para quitar última coma  	
  	echo $tareas;
}

function obtieneFechaLimiteAJAX($fecha,$suma){
	$fecha = formateaFechaBD($fecha);
	$fecha = new DateTime($fecha);
	$fecha->add(new DateInterval($suma));
	return $fecha->format('d/m/Y');
}

function tablaFicheros($datos,$num=1){
	if($num==1){
		$tabla='tablaDocumentos1';
		$bd='documentos_plan_prev';
		$prefijo='plan';
	} else {
		$tabla='tablaDocumentos2';
		$bd='documentos_info';
		$prefijo='info';
	}
	echo"
	 		<center>
	 		
				<table class='table table-bordered mitadAncho' id='".$tabla."'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Nombre </th>
							<th> Fichero </th>
							<!--th> Eliminar </th-->
                    	</tr>
                  	</thead>
                  	<tbody>";

					    $i=0;
						if($datos){
							$consulta=consultaBD("SELECT * FROM ".$bd." WHERE codigoFormulario=".$datos['codigo'],true);				  
							while($fichero=mysql_fetch_assoc($consulta)){
							 	echo "<tr>";
							 		campoOculto($fichero['codigo'],'codigoFichero_'.$prefijo.$i);
									campoTextoTabla('nombreSubido_'.$prefijo.$i,$fichero['nombre']);
									campoFichero('ficheroSubido_'.$prefijo.$i,'',1,$fichero['fichero'],'../documentos/planificacion/','Ver/descargar');
									//echo "<td><input type='checkbox' name='eliminaFichero_".$prefijo.$i."' value='".$fichero['codigo']."'></td>";
								echo"
									</tr>";
								$i++;
							}
						}
						campoOculto($i,'ficherosTotales_'.$prefijo);
						$i=0;
						echo "<tr>";
							campoTextoTabla('nombre_'.$prefijo.'0');
							campoFichero('fichero_'.$prefijo.'0','Documento',1);
							//echo "<td></td>";
						echo "</tr>";

						echo "
					</tbody>
	                </table>
	            

					<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"".$tabla."\");'><i class='icon-plus'></i> Añadir documento</button>
				</center><br />";
}

function eliminaPlanificacion(){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();

    for($i=0;isset($datos['codigo'.$i]);$i++){
		$imagenes=consultaBD("SELECT * FROM documentos_plan_prev WHERE codigoFormulario='".$datos['codigo'.$i]."';");
		while($imagen=mysql_fetch_assoc($imagenes)){
        	if($imagen['fichero'] != '' && $imagen['fichero'] != 'NO'){
            	unlink('ficheros/'.$imagen['fichero']);
        	}
    	}
    	$imagenes=consultaBD("SELECT * FROM documentos_info WHERE codigoFormulario='".$datos['codigo'.$i]."';");
		while($imagen=mysql_fetch_assoc($imagenes)){
        	if($imagen['fichero'] != '' && $imagen['fichero'] != 'NO'){
            	unlink('ficheros/'.$imagen['fichero']);
        	}
    	}
        $consulta=consultaBD("DELETE FROM formularioPRL WHERE codigo='".$datos['codigo'.$i]."';");
        if(!$consulta){
            $res=false;
            echo mysql_error();
        }
    }
    cierraBD();

    return $res;
}


function compruebaCambioDatosContrato(){
	global $_CONFIG;
	$res=true;
	$datos=arrayFormulario();

	if($datos['pregunta15']=='SI' && $datos['pregunta15Anterior']=='NO'){//Se cambia por primera vez...

		$headers="From: avisos@crmparapymes.com.es\r\n";
		$headers.= "MIME-Version: 1.0\r\n";
		$headers.= "Content-Type: text/html; charset=UTF-8";
		$mensaje="El motivo de este mensaje es informarle que se han modificado los datos del contrato para el cliente cliente ".$datos['pregunta0']." en su toma de 
				  datos del apartado Planificación (https://crmparapymes.com.es".$_CONFIG['raiz'].").<br><br/>
				  En las observaciones de la misma se ha indicado lo siguiente:<br /><br />
				  <strong><i>".nl2br($datos['pregunta16'])."</i></strong><br /><br />
				  Este mensaje se ha generado automáticamente desde el software. Por favor, no lo responda.";


		$res=mail('webmaster@qmaconsultores.com', 'Cambio de información de contrato en Toma de Datos - Anesco', $mensaje ,$headers);
	}

	return $res;
}



function creaTablaInstalaciones($datos){
	    echo "
	    <div class='control-group'>                     
	        <label class='control-label'>Enumeración de instalaciones:</label>
	        <div class='controls'>
	            <table class='tabla-simple mitadAncho' id='tablaInstalaciones'>
	                <thead>
	                    <tr>
	                        <th> Instalaciones </th>
	                        <th> </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos){
	                $consulta=consultaBD("SELECT codigoInstalacion FROM instalaciones_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."';");
	                while($instalacion=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaInstalaciones($i,$instalacion);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaInstalaciones(0,false);
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaInstalaciones\");'><i class='icon-plus'></i> Añadir instalación</button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaInstalaciones\");'><i class='icon-trash'></i> Eliminar instalación</button>
	        </div>
	    </div>";
}

function imprimeLineaTablaInstalaciones($i,$datos){
	$j=$i+1;

    echo "<tr>";
        campoSelectConsulta('codigoInstalacion'.$i,'',"SELECT codigo, nombre AS texto FROM instalaciones ORDER BY nombre;",$datos['codigoInstalacion'],'selectpicker span4 show-tick','data-live-search="true"','',1,false);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}