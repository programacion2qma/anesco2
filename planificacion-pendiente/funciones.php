<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión de Formularios PRL


function operacionesFormularioPRL(){
	$res=false;
		
	if(isset($_POST['codigo'])){
		$res=actualizaPlanificacion();
	}
	elseif(isset($_POST['codigoContrato'])){
		//$res=insertaPlanificacion();
	}
	elseif(isset($_POST['codigoPRL'])){
		$res=insertaFormulario();
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoPLanificacion($_POST['elimina']);
		//$res=eliminaPLanificacion();
	}
	
	mensajeResultadoAdicional('fecha',$res,'Formulario');
	mensajeResultadoAdicional('codigoPRL',$res,'Formulario');
    mensajeResultado('elimina',$res,'Formulario', true);
}

function insertaPlanificacion(){
	definirPOST();
	$res=insertaDatos('formularioPRL');
	
	if($res){
		$codigo=$res;
		$res=insertaVisitas($codigo);
		//$res=insertaDocumentacion($codigo,'documentos_plan_prev','plan');
		//$res=$res && insertaDocumentacion($codigo,'documentos_info','info');
	}

	return $res;
}

function actualizaPlanificacion(){
	definirPOST();
	$res=actualizaDatos('formularioPRL');

	if($res){
		//eliminarFicheros('documentos_plan_prev','plan');
		//eliminarFicheros('documentos_info','info');

		$codigo=$_POST['codigo'];
		$res=compruebaFormularioRenovado($codigo);
		$res=insertaVisitas($codigo);
		//$res=insertaDocumentacion($codigo,'documentos_plan_prev','plan');
		//$res=$res && insertaDocumentacion($codigo,'documentos_info','info');
	}

	return $res;
}

function insertaFormulario(){
	$res=true;
	$datos=arrayFormulario();
	
	$formulario=obtienePreguntasDatosGenerales($datos);

	conexionBD();

	foreach($datos['codigosCentros'] as $codigoCentro){

		$formulario.=obtienePreguntasCentro($datos,$codigoCentro);

		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'extintores_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','numero','kg','tipo','eficacia','senializado','revisado','ubicacionExtintor'),'numero',$codigoCentro);
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'bie_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','numeroBIE','medidaBIE','presion','senializado','revisado','ubicacion'),'numeroBIE',$codigoCentro);
		
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'botiquines_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','ubicacionBotiquines','senializadoBotiquines','numeroBotiquines','revisadoBotiquines'),'ubicacionBotiquines',$codigoCentro);
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'locales_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','ubicacionLocales','senializadoLocales','numeroLocales','revisadoLocales'),'ubicacionLocales',$codigoCentro);
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'medios_humanos_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','ubicacionMedios','numeroMedios','revisadoMedios','senializadoMedios'),'ubicacionMedios',$codigoCentro);
		
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'materias_primas_formulario_prl',$datos['codigoPRL'],array('materiaPrima','lugar','cantidad','codigoFormularioPRL'),'materiaPrima',$codigoCentro);
		
		//$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'puestos_trabajo_formulario_prl',$datos['codigoPRL'],array('codigoPuestoTrabajoSI','codigoFormularioPRL','sensibleSI'),'codigoPuestoTrabajoSI',$codigoCentro);
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'empleados_sensibles_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','codigoEmpleado'),'codigoEmpleado',$codigoCentro);

		/*$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'puestos_trabajo_formulario_prl',$datos['codigoPRL'],array('codigoPuestoTrabajoNO','codigoFormularioPRL','sensibleNO','seccionPuestoTrabajoNO'),'codigoPuestoTrabajoNO',$codigoCentro);*/
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'instalaciones_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','codigoInstalacion'),'codigoInstalacion',$codigoCentro);

		$res=$res && insertaFuncionesResponsabilidades($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaMaquinaria($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaHigienicos($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaIluminacion($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaGas($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaEvacuacion($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaRecursosHumanos($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaRecursosTecnicos($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaRecursosEconomicos($datos,$datos['codigoPRL'],$codigoCentro);
	}

	$formulario=quitaUltimaComa($formulario,4);//Para quitar el último separador

	$res = consultaBD("UPDATE formularioPRL SET formulario = '".$formulario."' WHERE codigo=".$datos['codigoPRL']);

	cierraBD();

	$res=$res && compruebaCambioDatosContrato();
	
	return $res;
}

function obtienePreguntasDatosGenerales($datos){
	$res='';
	
	for($i=0;$i<23;$i++){

		$res.="pregunta$i=>".$datos['pregunta'.$i]."&{}&";
	}

	return $res;
}


function obtienePreguntasCentro($datos,$codigoCentro){
	$res='';
	
	for($i=17;$i<=70;$i++){

		if($i!=55){
			$res.="pregunta$codigoCentro$i=>".$datos['pregunta'.$codigoCentro.$i];
		}
		else{//El índice 55 corresponde al fichero con los planos
			$res.=subeFicheroPlano('pregunta'.$codigoCentro.$i);
		}

		$res.="&{}&";
	}

	return $res;
}


function cambiaEstadoEliminadoPLanificacion($estado){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],'formularioPRL',false);
	}
	cierraBD();

	return $res;
}

function subeFicheroPlano($indice){
	$res="$indice=>NO";

	if(isset($_FILES[$indice])){
		$fichero=subeDocumento($indice,time(),'../documentos/planificacion');
		$res="$indice=>$fichero";
	}

	return $res;
}

function insertaDatosTablaAsociadaFormularioPRL($datos,$tabla,$codigoPRL,$campos,$campoCondicion,$codigoCentro,$borrado=true){
	$res=true;

	if($borrado){
		$res=consultaBD("DELETE FROM $tabla WHERE codigoFormularioPRL='$codigoPRL' AND codigoCentroTrabajo='$codigoCentro';");
	}
	

	for($i=0;isset($datos[$campoCondicion.$codigoCentro.'_'.$i]);$i++){
		$valores='';

		for($j=0;$j<count($campos);$j++){
			if($campos[$j]=='codigoFormularioPRL'){
				$valores.="'".$codigoPRL."', ";
			}
			elseif($campos[$j]=='sensibleSI'){
				$valores.="'SI', ";
			}
			elseif($campos[$j]=='sensibleNO'){
				$valores.="'NO', ";
			}
			elseif(substr_count($campos[$j],'codigo')>0){
				$valores.=$datos[$campos[$j].$codigoCentro.'_'.$i].", ";//Si es un campo código, no pone las comillas!! ('NULL' da error)
			}
			else{
				$valores.="'".$datos[$campos[$j].$codigoCentro.'_'.$i]."', ";
			}
		}

		$valores.="'".$codigoCentro."'";

		$res=$res && consultaBD("INSERT INTO $tabla VALUES(NULL,$valores);");
	}

	return $res;
}

function insertaFuncionesResponsabilidades($datos,$codigoPRL,$codigoCentro){
	$res=true;

	$codigos='(0';
	for($i=0;isset($datos['codigoEmpleadoFunciones'.$codigoCentro.'_'.$i]);$i++){
		if(isset($datos['existeEmpleadoFunciones'.$codigoCentro.'_'.$i])){
			$res=consultaBD('UPDATE funciones_formulario_prl SET funciones="'.$datos['funcionesFunciones'.$codigoCentro.'_'.$i].'",codigoEmpleado='.$datos['codigoEmpleadoFunciones'.$codigoCentro.'_'.$i].', area="'.$datos['areaFunciones'.$codigoCentro.'_'.$i].'", productos="'.$datos['productosFunciones'.$codigoCentro.'_'.$i].'", maquinaria="'.$datos['maquinariaFunciones'.$codigoCentro.'_'.$i].'", herramientas="'.$datos['herramientasFunciones'.$codigoCentro.'_'.$i].'", medio="'.$datos['medioFunciones'.$codigoCentro.'_'.$i].'" WHERE codigo='.$datos['existeEmpleadoFunciones'.$codigoCentro.'_'.$i]);
			$codigos.=','.$datos['existeEmpleadoFunciones'.$codigoCentro.'_'.$i];
		} else {
			$res=consultaBD('INSERT INTO funciones_formulario_prl VALUES(NULL,'.$codigoPRL.',"'.$datos['funcionesFunciones'.$codigoCentro.'_'.$i].'",'.$datos['codigoEmpleadoFunciones'.$codigoCentro.'_'.$i].',"'.$datos['areaFunciones'.$codigoCentro.'_'.$i].'",'.$codigoCentro.',"'.$datos['productosFunciones'.$codigoCentro.'_'.$i].'","'.$datos['maquinariaFunciones'.$codigoCentro.'_'.$i].'","'.$datos['herramientasFunciones'.$codigoCentro.'_'.$i].'","'.$datos['medioFunciones'.$codigoCentro.'_'.$i].'")');
			$codigos.=','.mysql_insert_id();
		}
	}
	$codigos.=')';
	$res=consultaBD('DELETE FROM funciones_formulario_prl WHERE codigo NOT IN '.$codigos.' AND codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	return $res;
}

function insertaMaquinaria($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM maquinaria_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$tipos=array('FIJA','MANUAL');
	foreach ($tipos as $tipo) {
		$i=0;
		while(isset($datos['maquinaria'.$tipo.$codigoCentro.'_'.$i])){
			if($datos['maquinaria'.$tipo.$codigoCentro.'_'.$i]!=''){
				$res=consultaBD('INSERT INTO maquinaria_formulario_prl VALUES(NULL,'.$codigoPRL.',"'.$datos['maquinaria'.$tipo.$codigoCentro.'_'.$i].'","'.$tipo.'",'.$codigoCentro.')');
			}
			$i++;
		}
		
	}

	return $res;
}

function insertaHigienicos($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM higienicos_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$i=0;
	while(isset($datos['nombreHigienico'.$codigoCentro.'_'.$i])){
		if($datos['nombreHigienico'.$codigoCentro.'_'.$i]!=''){
			$res=consultaBD('INSERT INTO higienicos_formulario_prl VALUES(NULL,'.$codigoPRL.','.$codigoCentro.',"'.$datos['nombreHigienico'.$codigoCentro.'_'.$i].'","'.$datos['descripcionHigienico'.$codigoCentro.'_'.$i].'")');
		}
		$i++;
	}
	return $res;
}

function insertaIluminacion($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM iluminacion_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$i=0;
	while(isset($datos['nombreIluminacion'.$codigoCentro.'_'.$i])){
		if($datos['nombreIluminacion'.$codigoCentro.'_'.$i]!=''){
			$res=consultaBD('INSERT INTO iluminacion_formulario_prl VALUES(NULL,'.$codigoPRL.','.$codigoCentro.',"'.$datos['nombreIluminacion'.$codigoCentro.'_'.$i].'","'.$datos['descripcionIluminacion'.$codigoCentro.'_'.$i].'")');
		}
		$i++;
	}
	return $res;
}

function insertaGas($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM gas_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$i=0;
	while(isset($datos['zonaGas'.$codigoCentro.'_'.$i])){
		if($datos['zonaGas'.$codigoCentro.'_'.$i]!=''){
			$res=consultaBD('INSERT INTO gas_formulario_prl VALUES(NULL,'.$codigoPRL.','.$codigoCentro.',"'.$datos['zonaGas'.$codigoCentro.'_'.$i].'","'.$datos['sueloGas'.$codigoCentro.'_'.$i].'","'.$datos['m2Gas'.$codigoCentro.'_'.$i].'","'.$datos['m3Gas'.$codigoCentro.'_'.$i].'")');
		}
		$i++;
	}
	return $res;
}

function insertaEvacuacion($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM evacuacion_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$i=0;
	while(isset($datos['tipoEvacuacion'.$codigoCentro.'_'.$i])){
		if($datos['tipoEvacuacion'.$codigoCentro.'_'.$i]!='0'){
			$res=consultaBD('INSERT INTO evacuacion_formulario_prl VALUES(NULL,'.$codigoPRL.','.$codigoCentro.',"'.$datos['tipoEvacuacion'.$codigoCentro.'_'.$i].'","'.$datos['zonaEvacuacion'.$codigoCentro.'_'.$i].'","'.$datos['descripcionEvacuacion'.$codigoCentro.'_'.$i].'")');
		}
		$i++;
	}
	return $res;
}

function insertaRecursosHumanos($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM recursos_humanos_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$i=0;
	while(isset($datos['recursoRecursosH'.$codigoCentro.'_'.$i])){
		if($datos['recursoRecursosH'.$codigoCentro.'_'.$i]!='0'){
			$res=consultaBD('INSERT INTO recursos_humanos_formulario_prl VALUES(NULL,'.$codigoPRL.','.$codigoCentro.',"'.$datos['recursoRecursosH'.$codigoCentro.'_'.$i].'","'.$datos['identificacionRecursosH'.$codigoCentro.'_'.$i].'","'.$datos['tipoRecursosH'.$codigoCentro.'_'.$i].'","'.$datos['puestoRecursosH'.$codigoCentro.'_'.$i].'")');
		}
		$i++;
	}
	return $res;
}

function insertaRecursosTecnicos($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM recursos_tecnicos_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$i=0;
	while(isset($datos['recursoRecursosT'.$codigoCentro.'_'.$i])){
		if($datos['recursoRecursosT'.$codigoCentro.'_'.$i]!='0'){
			$res=consultaBD('INSERT INTO recursos_tecnicos_formulario_prl VALUES(NULL,'.$codigoPRL.','.$codigoCentro.',"'.$datos['recursoRecursosT'.$codigoCentro.'_'.$i].'","'.$datos['tipoRecursosT'.$codigoCentro.'_'.$i].'")');
		}
		$i++;
	}
	return $res;
}

function insertaRecursosEconomicos($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM recursos_economicos_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$i=0;
	while(isset($datos['recursoRecursosE'.$codigoCentro.'_'.$i])){
		if($datos['recursoRecursosE'.$codigoCentro.'_'.$i]!='0'){
			$$datos['importeRecursosE'.$codigoCentro.'_'.$i]=str_replace(',','.', $datos['importeRecursosE'.$codigoCentro.'_'.$i]);
			$res=consultaBD('INSERT INTO recursos_economicos_formulario_prl VALUES(NULL,'.$codigoPRL.','.$codigoCentro.',"'.$datos['recursoRecursosE'.$codigoCentro.'_'.$i].'","'.$datos['tipoRecursosE'.$codigoCentro.'_'.$i].'","'.$datos['descripcionRecursosE'.$codigoCentro.'_'.$i].'","'.$datos['importeRecursosE'.$codigoCentro.'_'.$i].'")');
		}
		$i++;
	}
	return $res;
}

/*function insertaEmpleadosPRL($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM empleados_prl WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['nombreEmpleadoPRL'.$i])){
		if($datos['nombreEmpleadoPRL'.$i] != ''){
		$res = $res && consultaBD("INSERT INTO empleados_prl VALUES (NULL,'".$codigo."','".$datos['nombreEmpleadoPRL'.$i]."',
			'".$datos['dniEmpleadoPRL'.$i]."','".$datos['puestoEmpleadoPRL'.$i]."','".$datos['funcionesEmpleadoPRL'.$i]."','".$datos['salenEmpleadoPRL'.$i]."',
			'".$datos['vehiculoEmpleadoPRL'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function insertaCentrosPRL($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM centros_prl WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['nombreCentroPRL'.$i])){
		if($datos['nombreCentroPRL'.$i] != ''){
		$res = $res && consultaBD("INSERT INTO centros_prl VALUES (NULL,'".$codigo."','".$datos['nombreCentroPRL'.$i]."',
			'".$datos['delegadoCentroPRL'.$i]."','".$datos['comiteCentroPRL'.$i]."','".$datos['trabajadorCentroPRL'.$i]."','".$datos['consultaCentroPRL'.$i]."');");
		}

		$i++;
	}

	return $res;
}*/

function eliminarFicheros($tabla,$prefijo){
	$res = true;

	for($i=0;$i<$_POST['ficherosTotales_'.$prefijo];$i++){
		if(isset($_POST['eliminaFichero_'.$prefijo.$i])){
			$codigo=$_POST['eliminaFichero_'.$prefijo.$i];
			$fichero=datosRegistro($tabla,$codigo);
			$res=consultaBD('DELETE FROM '.$tabla.' WHERE codigo='.$codigo,true);
			unlink('ficheros/'.$fichero['fichero']);
		}
	}

	return $res;
}

function insertaDocumentacion($codigo,$tabla,$prefijo){
	$res=true;//Siempre que vayamos a usarlo con un AND, inicializarlo a true

	conexionBD();
	
	$datos=arrayFormulario();
	$i=0;
	while(isset($datos['codigoFichero_'.$prefijo.$i])){
		$res=$res && consultaBD('UPDATE '.$tabla.' SET nombre="'.$datos['nombreSubido_'.$prefijo.$i].'" WHERE codigo='.$datos['codigoFichero_'.$prefijo.$i]);
		$i++;
	}
	$i=0;
	while(isset($_FILES['fichero_'.$prefijo.$i])){
		if($_FILES['fichero_'.$prefijo.$i]['tmp_name'] != ''){
			$fichero=subeDocumento('fichero_'.$prefijo.$i,time(),'../documentos/planificacion');

			$res=$res && consultaBD("INSERT INTO ".$tabla." VALUES(NULL, '$codigo', '".$datos['nombre_'.$prefijo.$i]."', '".$fichero."');");
		}
		$i++;
	}
	
	cierraBD();

	return $res;
}


function gestionFormularioPRL(){
	$mesesNombres=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	operacionesFormularioPRL();
	
	if($_SESSION['tipoUsuario']!='FACTURACION'){
		abreVentanaGestionConBotones('Gestión de planificaciones de PRL','index.php','','icon-edit','',true,'noAjax');
	}
	else{
		abreVentanaGestionConBotones('Gestión de planificaciones de PRL','index.php','','icon-edit','',true,'noAjax','index.php',false);
	}

	$datos=compruebaDatos('formularioPRL');

	abreColumnaCampos();
		//campoFecha('fecha','Fecha',$datos);
		campoOculto(formateaFechaWeb($datos['fecha']),'fecha',fecha());

		if(!$datos){
			$numero = generaNumero();
		} else {
			$numero = $datos['codigoInterno'];
		}
		campoTexto('codigoInterno','Num',$numero,'input-mini pagination-right');
		campoOculto($datos,'formulario');
		campoOculto($datos,'eliminado','NO');
		$where=defineWhereEmpleado('WHERE activo="SI" AND enVigor="SI" AND opcion<>"2"');
		campoSelectContrato($datos);
		campoUsuarioTecnicoPlanificacion($datos);
		campoNumero('horasPrevistas','Horas previstas',$datos['horasPrevistas']);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadio('aprobado','Aprobado',$datos);
		campoFechaBlanco('fechaAprobada','Fecha aprobado',$datos);
	cierraColumnaCampos(true);
	
	
	echo "<h3 class='apartadoFormulario'>Visitas</h3>";
	abreColumnaCampos();
		echo '<div class="hide">';
		campoRadio('visitada','Visitada',$datos);
		campoFechaBlanco('fechaVisita','Fecha prevista',$datos);
		campoFechaBlanco('fechaVisitaReal','Fecha real',$datos);
		campoNumero('numVisitas','Número de visitas',$i);
		echo '</div>';
		$i=creaTablaVisitasPlanificacion($datos);
	cierraColumnaCampos();
	abreColumnaCampos();

	campoDato('Número de visitias',$i);

	cierraColumnaCampos(true);


	echo "<h3 class='apartadoFormulario'>Plan de prevención</h3>";

	abreColumnaCampos();
		
	campoRadio('planPrev','Plan prevención',$datos);
	cierraColumnaCampos(true);
	echo '<div id="div_planPrev">';
		abreColumnaCampos();
			campoFechaBlanco('fechaPlanPrev','Fecha prevista',$datos);
			echo '<div style="margin-left:220px;">';
			campoCheckIndividual('checkVisiblePlanPrev','Visible',$datos);
			echo '</div>';
		cierraColumnaCampos();
		abreColumnaCampos();
			campoFechaBlanco('fechaPlanPrevReal','Fecha real',$datos);
			campoNumero('horasPlanPrev','Horas',$datos);
		cierraColumnaCampos(true);
	echo '</div>';
	echo "<h3 class='apartadoFormulario'>Evaluación de Riesgos/Planificación Preventiva</h3>";
	abreColumnaCampos();
		campoRadio('pap','Evaluación de riesgos \\ Planificación preventiva',$datos);
	cierraColumnaCampos(true);
	echo '<div id="div_pap">';
		abreColumnaCampos();
			campoFechaBlanco('fechaPap','Fecha prevista',$datos);
		cierraColumnaCampos();
		abreColumnaCampos();
			campoFechaBlanco('fechaPapReal','Fecha real',$datos);
			campoNumero('horasPap','Horas',$datos);
		cierraColumnaCampos(true);
	echo '</div>';
	echo "<h3 class='apartadoFormulario'>Información/Formación a los Trabajadores</h3>";
	abreColumnaCampos();
		campoRadio('info','Información a los trabajadores \\ Certificados de formación',$datos);
	cierraColumnaCampos(true);
	echo '<div id="div_info">';
		abreColumnaCampos();
			campoFechaBlanco('fechaInfo','Fecha prevista',$datos);
		cierraColumnaCampos();
		abreColumnaCampos();
			campoFechaBlanco('fechaInfoReal','Fecha real',$datos);
			campoNumero('formacionPresencial','Nº trab. formación presencial',$datos);
			campoNumero('formacionDistancia','Nº trab. formación a distancia',$datos);
			campoNumero('formacionOnline','Nº trab. formación online',$datos);
		cierraColumnaCampos(true);
	echo '</div>';
	echo "<h3 class='apartadoFormulario'>Accidentes de trabajo</h3>";
	abreColumnaCampos();
		campoRadio('accidentes','Accidentes de trabajo',$datos);
	cierraColumnaCampos(true);
	echo '<div id="div_accidentes">';
		abreColumnaCampos();
			campoFechaBlanco('fechaAccidentes','Fecha prevista',$datos);
		cierraColumnaCampos();
		abreColumnaCampos();
			campoFechaBlanco('fechaAccidentesReal','Fecha real',$datos);
			campoNumero('accidentesLeves','Leves',$datos);
			campoNumero('accidentesGraves','Graves',$datos);
			campoNumero('accidentesMortales','Mortales',$datos);
		cierraColumnaCampos(true);


	/*abreColumnaCampos('sinFlotar');
		echo "<h3 class='apartadoFormulario'>Documentos</h3>";
		tablaFicheros($datos,2);
	cierraColumnaCampos(true);*/
	echo '</div>';
	echo "<h3 class='apartadoFormulario'>Memoria</h3>";
	abreColumnaCampos();
		campoRadio('memoria','Memoria',$datos);
	cierraColumnaCampos(true);
	echo '<div id="div_memoria">';
		abreColumnaCampos();
			campoFechaBlanco('fechaMemoria','Fecha prevista',$datos);
		cierraColumnaCampos();
		abreColumnaCampos();	
			campoFechaBlanco('fechaMemoriaReal','Fecha real',$datos);
		cierraColumnaCampos(true);
	echo '</div>';
	echo "<h3 class='apartadoFormulario'>Reconocimiento médicos</h3>";
	abreColumnaCampos();
		campoRadio('vs','F. prevista para  Rec. Médicos',$datos);
	cierraColumnaCampos(true);
	echo '<div id="div_vs">';
		abreColumnaCampos();
			campoMesPlanificacion('mesVS',$datos);
			campoAnioPlanificacion('anioVS',$datos);
			campoFechaBlanco('fechaVsReal','Fecha real',$datos);
			$tiempo='00:00';
			$tareas=consultaBD('SELECT tiempo FROM tareas_parte_diario WHERE codigoContrato='.$datos['codigoContrato'],true);
			while($tarea=mysql_fetch_assoc($tareas)){
				$tiempo=sumaTiempo($tiempo,$tarea['tiempo']);
			}
			campoTexto('totalHoras','Total horas dedicadas',$tiempo,'input-small',true);
		cierraColumnaCampos();
		abreColumnaCampos();
			//campoRadio('corrientePago','Esta al corriente de pago',$datos);
			campoTexto('mail','Mail informativo, formacion y vigilancia de la salud',$datos);
			campoFechaBlanco('fechaMail','Fecha email',$datos);
			areaTexto('observaciones','Observaciones',$datos,'areaInforme');
			campoSelect('mesVencimiento','Mes vencimiento',$mesesNombres,$mesesNombres,$datos,'selectpicker span2 show-tick');
			cierraColumnaCampos(true);
	echo '</div>';

	/*abreColumnaCampos('sinFlotar');
		echo "<h3 class='apartadoFormulario'>Documentación</h3>";
		tablaFicheros($datos);
	cierraColumnaCampos(true);*/

	campoOculto($datos,'ficheroOrganigrama','NO');
	campoOculto($datos,'ficheroEstructura','NO');

	if($_SESSION['tipoUsuario']!='FACTURACION'){
		cierraVentanaGestion('index.php',true);
	}
	else{
		cierraVentanaGestion('index.php',true,false);
	}
}

function campoHorasReales($nombreCampo,$valor=false){
	/*$datos=array();
	if(!$valor || ($valor && $valor[$nombreCampo]=='')){
		$datos[0]='';
		$datos[1]='';
	} 
	else {
		$datos=explode(':',$valor[$nombreCampo]);	
		if(!isset($datos[1])){
			$datos[1]='';		
		}
	}
	campoNumero($nombreCampo,'Horas dedicadas',formateaHoraWeb($datos[$nombreCampo]),'input-mini pagination-right horas',0);
	campoNumero($nombreCampo.'_minutos','Minutos',$datos[1],'input-mini pagination-right minutos',0);*/

	campoNumero($nombreCampo,'Horas dedicadas',$valor);
    
}

function definirPOST(){
	/*$campos=array('horasVisita','horasPlanPrev','horasPap','horasInfo','horasMemoria','horasVs');
	foreach ($campos as $campo) {
		$_POST[$campo]=$_POST[$campo.'_horas'].':'.$_POST[$campo.'_minutos'];
	}*/
}

function completarDatosGeneralesPRL($formulario,$cliente){
	$cliente = datosRegistro('clientes',$cliente);

	$formulario['pregunta3'] = $formulario['pregunta3'] == '' ? $cliente['EMPNOMBRE'] : $formulario['pregunta3']; 
	$formulario['pregunta4'] = $formulario['pregunta4'] == '' ? $cliente['EMPDIR'] : $formulario['pregunta4']; 
	$formulario['pregunta5'] = $formulario['pregunta5'] == '' ? $cliente['EMPLOC'] : $formulario['pregunta5']; 
	$formulario['pregunta6'] = $formulario['pregunta6'] == '' ? $cliente['EMPTEL1'] : $formulario['pregunta6']; 
	$formulario['pregunta12'] = $formulario['pregunta12'] == '' ? $cliente['EMPEMAIL'] : $formulario['pregunta12']; 
	$formulario['pregunta9'] = $formulario['pregunta9'] == '' ? $cliente['EMPCIF'] : $formulario['pregunta9']; 
	$formulario['pregunta10'] = $formulario['pregunta10'] == '' ? $cliente['EMPCP'] : $formulario['pregunta10']; 
	$formulario['pregunta11'] = $formulario['pregunta11'] == '' ? $cliente['EMPPROV'] : $formulario['pregunta11']; 
	$formulario['pregunta13'] = $formulario['pregunta13'] == '' ? $cliente['EMPACTIVIDAD'] : $formulario['pregunta13']; 

	return $formulario;
}


function campoSelectContrato($datos){
	$contratos=consultaBD('SELECT * FROM contratos',true);	
	$nombres=array();
	$valores=array();
	while($contrato=mysql_fetch_assoc($contratos)){
		$cliente=consultaBD('SELECT clientes.* FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo='.$contrato['codigoOferta'],true,true);
		$referencia=formateaReferenciaContrato($cliente,$contrato);
		array_push($nombres, 'Nº: '.$referencia.' - '.$cliente['EMPMARCA']);
		array_push($valores, $contrato['codigo']);
	}
	campoSelect('codigoContrato','Contrato',$nombres,$valores,$datos);
}

//Parte de Toma de Datos

function tomaDatos(){
	abreVentanaGestionConBotones('Toma de datos PRL','index.php','','icon-edit','',true,'noAjax');
	
	$codigosCentros=creaPestaniasFormularioPRL();
	$datos=compruebaDatosTomaDatos($codigosCentros);

	abrePestaniaAPI(1,true);
	camposDatosGenerales($datos);
	abreColumnaCampos();
		areaTextoFormulario(20,'Descripción de la empresa',$datos,'areaInforme areaDescripcion'); 
	cierraColumnaCampos();
	cierraPestaniaAPI();

	$i=2;
	foreach ($codigosCentros as $codigoCentro) {
		abrePestaniaAPI($i);
		
		campoOculto($codigoCentro,'codigosCentros[]');

		camposDescripcionCentro($datos,$codigoCentro);
		camposFuncionesResponsabilidades($datos,$codigoCentro);
		camposTrabajadoresSensibles($datos,$codigoCentro);
		camposMaquinarias($datos,$codigoCentro);
		camposAgentes($datos,$codigoCentro);
		camposEmergencia($datos,$codigoCentro);
		camposMateriasPrimas($datos,$codigoCentro);
		camposCentro($datos,$codigoCentro);
		camposHigienicos($datos,$codigoCentro);
		camposIluminacion($datos,$codigoCentro);
		camposInstalaciones($datos,$codigoCentro);
		camposAlarmas($datos,$codigoCentro);
		camposIncendio($datos,$codigoCentro);
		camposEvacuacion($datos,$codigoCentro);
		camposPrimerosAuxilios($datos,$codigoCentro);
		camposVestuarios($datos,$codigoCentro);
		camposRecursos($datos,$codigoCentro);
		//camposPuestosTrabajos($datos,$codigoCentro);
		camposObservaciones($datos,$codigoCentro);
		campoFirma($datos,'pregunta'.$codigoCentro.'54','Firma del acompañante');

		cierraPestaniaAPI();

		$i++;
	}

	cierraPestaniasAPI();

	cierraVentanaGestion('index.php',true);
}

function compruebaDatosTomaDatos($codigosCentros){
	$codigoPRL=$_GET['codigoPRL'];

	campoOculto($codigoPRL,'codigoPRL');

	$datos=datosRegistro('formularioPRL',$codigoPRL);

	$formulario=explode('&{}&',$datos['formulario']);

    if(count($formulario)>1){

        foreach($formulario as $pregunta){
            $partes=explode('=>',$pregunta);

            $datos[$partes[0]]=$partes[1];
        }
    } 
    else {

    	$cliente=consultaBD("SELECT EMPNOMBRE, ofertas.numEmpleados, EMPCIF, EMPDIR, EMPCP, EMPTELPRINC, EMPACTIVIDAD, EMPCNAE, EMPMUTUA
    						 FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='".$datos['codigoContrato']."';",true,true);


    	$datos['pregunta0']=$cliente['EMPNOMBRE'];
    	$datos['pregunta1']=$cliente['EMPCIF'];
    	$datos['pregunta2']=$cliente['EMPDIR'];
    	$datos['pregunta3']=$cliente['EMPCP'];
    	$datos['pregunta4']=$cliente['EMPACTIVIDAD'];
    	$datos['pregunta5']=$cliente['EMPCNAE'];
    	//$datos['pregunta6']=formateaCentrosTrabajoTomaDatos($cliente);
    	$datos['pregunta6']='';
    	$datos['pregunta7']=$cliente['EMPTELPRINC'];
    	$datos['pregunta8']='';
    	$datos['pregunta9']='';
    	$datos['pregunta10']='';
    	$datos['pregunta11']='';
    	$datos['pregunta12']=$cliente['numEmpleados'];
    	$datos['pregunta13']='';
    	$datos['pregunta14']='';
    	$datos['pregunta15']=$cliente['EMPMUTUA'];
    	$datos['pregunta16']='';

    	foreach ($codigosCentros as $codigoCentro){
    		for($i=17;$i<=55;$i++){//El índice 54 es la firma del técnico y el 55 es el fichero con los planos
				$datos['pregunta'.$codigoCentro.$i]='';
	        }
    	}

    }

    return $datos;
}

function obtenerCentrosTrabajo($cliente){
	$cliente=consultaBD('SELECT clientes.codigo FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='.$cliente,true,true);
	$res=array();
	$res['valores']=array();
	$res['nombres']=array();
	$centros=consultaBD('SELECT * FROM clientes_centros WHERE codigoCliente='.$cliente['codigo'],true);
	while($centro=mysql_fetch_assoc($centros)){
		array_push($res['valores'], $centro['codigo']);
		array_push($res['nombres'], $centro['direccion'].', '.$centro['localidad'].' - CP: '.$centro['cp'].' ('.$centro['provincia'].')');
	}
	return $res;
}

function camposDatosGenerales($datos){
	$centros=array();
	$centros=obtenerCentrosTrabajo($datos['codigoContrato']);
	//echo "<h3 class='apartadoFormulario'>Datos Generales</h3>";
	abreColumnaCampos('span3 camposGeneralesTomaDatos');

	campoOculto($datos['pregunta6'],'pregunta6Anterior','NO');

	campoRadioFormulario(6,'Cambio datos del contrato',$datos);
	campoTextoFormulario(0,'Razón social',$datos,'input-large');
	campoTextoFormulario(1,'CIF',$datos,'input-small');
	campoTextoFormulario(2,'Domicilio',$datos);
	campoTextoFormulario(3,'Código Postal',$datos,'input-mini pagination-right');
	campoSelectConsultaFormulario(4,'Actividad','SELECT codigo,nombre AS texto FROM actividades ORDER BY nombre',$datos);
	campoCNAEFormulario(5,'CNAE',$datos);
	
	//campoTextoFormulario(6,'Centro/s de trabajo',$datos);
	//campoSelectFormulario(6,'Centro/s de trabajo',$datos,0,$centros['nombres'],$centros['valores']);

	campoTextoFormulario(7,'Teléfono',$datos,'input-small pagination-right');
	
	cierraColumnaCampos();
	abreColumnaCampos();
	
	campoTextoFormulario(8,'Acompañado en la visita por',$datos);
	campoFechaFormulario(9,'Fecha',$datos);
	campoTextoFormulario(10,'Horarios',$datos);
	campoRadioFormulario(11,'Preferencia documentación',$datos,'Formato electrónico',array('Formato electrónico','Formato papel'),array('Formato electrónico','Formato papel'));
	campoTextoFormulario(12,'Nº trabajadores',$datos,'input-mini pagination-right');
	campoTextoFormulario(15,'Mutua',$datos);
	campoRadioFormulario(13,'Trabajadores autónomos',$datos);
	campoRadioFormulario(14,'Realiza obras de construcción',$datos);
	campoFechaFormulario(21,'Desde',$datos);
	campoObservacionesDatosGenerales($datos);

	cierraColumnaCampos(true);
}

function campoObservacionesDatosGenerales($datos){
	echo "<div class='hide' id='cajaObservacionesDatosGenerales'>";
	areaTextoFormulario(16,'Observaciones',$datos);
	echo "</div>";
}


function camposTrabajadoresSensibles($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Trabajadores sensibles</h3>";
	abreColumnaCampos();

	campoRadioFormulario($codigoCentro.'17','Menores de edad',$datos);
	campoRadioFormulario($codigoCentro.'18','Embarazadas',$datos);
	campoRadioFormulario($codigoCentro.'50','Minusvalía - incapacidad',$datos);
	campoTextoFormulario($codigoCentro.'19','Otros (indicar el supuesto)',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	//creaTablaPuestosTomaDatos($datos['codigo'],$datos['codigoContrato'],'SI',$codigoCentro);
	creaTablaTrabajadoresSensibles($datos['codigo'],$datos['codigoContrato'],$codigoCentro);

	cierraColumnaCampos(true);
}


function creaTablaTrabajadoresSensibles($codigoFormularioPRL,$codigoContrato,$codigoCentro){
	    echo "
	    <div class='control-group'>                     
	        <label class='control-label'>Indica el/los trabajadores:</label>
	        <div class='controls'>
	            <table class='tabla-simple mitadAncho' id='tablaEmpleadosSensibles".$codigoCentro."'>
	                <thead>
	                    <tr>
	                        <th> Empleados sensibles </th>
	                        <th> </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($codigoFormularioPRL!=false){
	                $consulta=consultaBD("SELECT codigoEmpleado FROM empleados_sensibles_formulario_prl WHERE codigoFormularioPRL='$codigoFormularioPRL' AND codigoCentroTrabajo='$codigoCentro'");
	                while($empleado=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaTrabajadoresSensibles($i,$codigoContrato,$empleado,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaTrabajadoresSensibles(0,$codigoContrato,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaEmpleadosSensibles".$codigoCentro."\");'><i class='icon-plus'></i> Añadir empleado</button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaEmpleadosSensibles".$codigoCentro."\");'><i class='icon-trash'></i> Eliminar empleado</button>
	        </div>
	    </div>";
}

function imprimeLineaTablaTrabajadoresSensibles($i,$codigoContrato,$datos,$codigoCentro){
	$j=$i+1;

	$queryEmpleados="SELECT empleados.codigo, CONCAT(nombre,' ',apellidos) AS texto 
					 FROM empleados INNER JOIN clientes ON empleados.codigoCliente=clientes.codigo 
					 INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente 
					 INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
					 WHERE contratos.codigo='$codigoContrato' AND trabajadorSensible='SI' AND aprobado='SI' AND empleados.eliminado='NO'
					 ORDER BY nombre, apellidos;";

    echo "<tr>";
        campoSelectConsulta('codigoEmpleado'.$codigoCentro.'_'.$i,'',$queryEmpleados,$datos['codigoEmpleado'],'selectpicker span4 show-tick','data-live-search="true"','',1,false);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function camposMaquinarias($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Equipos de trabajo / Maquinas</h3>";
	abreColumnaCampos();
		creaTablaMaquinaria($datos['codigo'],$datos['codigoContrato'],$codigoCentro,'FIJA');
	cierraColumnaCampos();
	abreColumnaCampos();
		creaTablaMaquinaria($datos['codigo'],$datos['codigoContrato'],$codigoCentro,'MANUAL');
	cierraColumnaCampos(true);

	creaTablaFuncionesResponsabilidades2($datos['codigo'],$datos['codigoContrato'],$codigoCentro);
}

function creaTablaMaquinaria($codigoFormularioPRL,$codigoContrato,$codigoCentro,$tipo){
		$titulo='Maquinaria fija';
		$tabla="tablaMaquinariaFija".$codigoCentro;
		if($tipo=='MANUAL'){
			$titulo='Herramientas y útiles';
			$tabla="tablaMaquinariaManual".$codigoCentro;
		}
	    echo "
	    <div class='control-group'>                     
	        <div class='controls'>
	            <table class='tabla-simple mitadAncho' id='".$tabla."'>
	                <thead>
	                    <tr>
	                        <th> ".$titulo." </th>
	                        <th> </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($codigoFormularioPRL!=false){
	                $consulta=consultaBD("SELECT * FROM maquinaria_formulario_prl WHERE codigoFormularioPRL='$codigoFormularioPRL' AND codigoCentroTrabajo='$codigoCentro' AND tipo='$tipo'");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaMaquinaria($i,$codigoContrato,$item,$codigoCentro,$tipo);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaMaquinaria(0,$codigoContrato,false,$codigoCentro,$tipo);
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"".$tabla."\");'><i class='icon-plus'></i> Añadir </button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"".$tabla."\");'><i class='icon-trash'></i> Eliminar</button>
	        </div>
	    </div>";
}

function imprimeLineaTablaMaquinaria($i,$codigoContrato,$datos,$codigoCentro,$tipo){
	$j=$i+1;

    echo "<tr>";
        campoTextoTabla('maquinaria'.$tipo.$codigoCentro.'_'.$i,$datos['nombre']);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}


function camposFuncionesResponsabilidades($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Funciones y responsabilidades</h3>";

	creaTablaFuncionesResponsabilidades($datos['codigo'],$datos['codigoContrato'],$codigoCentro);

}


function creaTablaFuncionesResponsabilidades($codigoFormularioPRL,$codigoContrato,$codigoCentro){
	    echo "
	    <div class='control-group'>                     
	        <div class='controls' style='margin-left:10px;'>
	            <table class='tabla-simple' id='tablaFunciones".$codigoCentro."'>
	                <thead>
	                    <tr>
	                    	<th> Empleado </th>
	                    	<th> Puesto de trabajo </th>
	                    	<th> Función </th>
	                        <th> Sección / Area </th>
	                        <th> </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($codigoFormularioPRL!=false){
	                $consulta=consultaBD("SELECT * FROM funciones_formulario_prl WHERE codigoFormularioPRL='$codigoFormularioPRL' AND codigoCentroTrabajo='$codigoCentro'");
	                while($empleado=mysql_fetch_assoc($consulta)){
	                   imprimeLineaTablaFuncionesResponsabilidades($i,$codigoContrato,$empleado,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaFuncionesResponsabilidades(0,$codigoContrato,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFilaFunciones(\"tablaFunciones".$codigoCentro."\");'><i class='icon-plus'></i> Añadir función</button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFilaFunciones(\"tablaFunciones".$codigoCentro."\");'><i class='icon-trash'></i> Eliminar función</button>
	        </div>
	    </div>";
}

function imprimeLineaTablaFuncionesResponsabilidades($i,$codigoContrato,$datos,$codigoCentro){
	$j=$i+1;

	$centro=consultaBD('SELECT codigoCliente FROM clientes_centros WHERE codigo='.$codigoCentro,false,true);
	if($datos!=false){
		campoOculto($datos['codigo'],'existeEmpleadoFunciones'.$codigoCentro.'_'.$i,$valorPorDefecto='',$clase='hide');
	}
    echo "<tr>";
    	campoSelectConsulta('codigoEmpleadoFunciones'.$codigoCentro.'_'.$i,'','SELECT codigo, CONCAT(nombre," ",apellidos) AS texto FROM empleados WHERE codigoCliente='.$centro['codigoCliente'].' ORDER BY nombre,apellidos',$datos['codigoEmpleado'],'selectpicker span4 show-tick selectEmpleadoFunciones',"data-live-search='true'",'',1);
    	if($datos['codigoEmpleado']!=NULL){
    		$puesto=consultaBD('SELECT puestos_trabajo.nombre FROM empleados INNER JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo WHERE empleados.codigo='.$datos['codigoEmpleado'],false,true);
    	} else {
    		$puesto=false;
    	}
    	campoTextoTabla('puestoEmpleadoFunciones'.$codigoCentro.'_'.$i,$puesto['nombre'],'input-large',true);
    	campoTextoTabla('funcionesFunciones'.$codigoCentro.'_'.$i,$datos['funciones']);
    	campoTextoTabla('areaFunciones'.$codigoCentro.'_'.$i,$datos['area'],'input-small');
    	areaTextoTabla('productosFunciones'.$codigoCentro.'_'.$i,$datos['productos'],'areaProductosSustancias');
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function creaTablaFuncionesResponsabilidades2($codigoFormularioPRL,$codigoContrato,$codigoCentro){
	    echo "
	    <div class='control-group'>                     
	        <div class='controls' style='margin-left:10px;'>
	            <table class='tabla-simple' id='tablaFunciones".$codigoCentro."_2'>
	                <thead>
	                    <tr>
	                    	<th> Empleado </th>
	                    	<th> Maquinaria y equipos usados </th>
	                    	<th> Herramientas </th>
	                        <th> Medios mecanicos de carga </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($codigoFormularioPRL!=false){
	                $consulta=consultaBD("SELECT * FROM funciones_formulario_prl WHERE codigoFormularioPRL='$codigoFormularioPRL' AND codigoCentroTrabajo='$codigoCentro'");
	                while($empleado=mysql_fetch_assoc($consulta)){
	                   imprimeLineaTablaFuncionesResponsabilidades2($i,$codigoContrato,$empleado,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaFuncionesResponsabilidades2(0,$codigoContrato,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	        </div>
	    </div>";
}

function imprimeLineaTablaFuncionesResponsabilidades2($i,$codigoContrato,$datos,$codigoCentro){

    echo "<tr>";
    	if($datos['codigoEmpleado']!=NULL){
    		$empleado=consultaBD('SELECT CONCAT(nombre," ",apellidos) AS empleado FROM empleados WHERE codigo='.$datos['codigoEmpleado'],false,true);
    		$empleado=$empleado['empleado'];
    	} else {
    		$empleado=false;
    	}
    	campoTextoTabla('nombreEmpleadoFunciones'.$codigoCentro.'_'.$i,$empleado,'input-large',true);
    	areaTextoTabla('maquinariaFunciones'.$codigoCentro.'_'.$i,$datos['maquinaria'],'areaMaquinaria'); 
    	areaTextoTabla('herramientasFunciones'.$codigoCentro.'_'.$i,$datos['herramientas'],'areaMaquinaria'); 
    	areaTextoTabla('medioFunciones'.$codigoCentro.'_'.$i,$datos['medio'],'areaMaquinaria'); 
    echo "
    </tr>";
}




function creaTablaPuestosTomaDatos($codigoPRL,$codigoCliente,$sensible,$codigoCentro){
		$cliente=consultaBD('SELECT clientes.codigo FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='.$codigoCliente,true,true);
		$codigoCliente=$cliente['codigo'];
	    echo "
	    <div class='control-group'>                     
	        <label class='control-label'>Indica el/los puesto/s:</label>
	        <div class='controls'>
	            <table class='tabla-simple mitadAncho' id='tablaPuestosTrabajo".$codigoCentro.$sensible."'>
	                <thead>
	                    <tr>
	                        <th> Puesto de trabajo </th>
	                        <th> Sección / Area </th>
	                        <th> </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($codigoPRL!=false){
	                $consulta=consultaBD("SELECT codigoPuestoTrabajo, seccionPuestoTrabajo FROM puestos_trabajo_formulario_prl WHERE codigoFormularioPRL='$codigoPRL' AND sensible='$sensible' AND codigoCentroTrabajo='$codigoCentro'");
	                while($puesto=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaPuestosTrabajo($i,$codigoCliente,$puesto,$sensible,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaPuestosTrabajo(0,$codigoCliente,false,$sensible,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaPuestosTrabajo".$codigoCentro.$sensible."\");'><i class='icon-plus'></i> Añadir puesto</button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaPuestosTrabajo".$codigoCentro.$sensible."\");'><i class='icon-trash'></i> Eliminar puesto</button>
	        </div>
	    </div>";
}

function imprimeLineaTablaPuestosTrabajo($i,$codigoCliente,$datos,$sensible,$codigoCentro){
	$j=$i+1;

    echo "<tr>";
        campoSelectConsulta('codigoPuestoTrabajo'.$sensible.$codigoCentro.$i,'',"SELECT codigo, nombre AS texto FROM puestos_trabajo ORDER BY nombre;",$datos['codigoPuestoTrabajo'],'selectpicker span3 show-tick','data-live-search="true"','',1,false);
        campoTextoTabla('seccionPuestoTrabajo'.$sensible.$codigoCentro.$i,$datos['seccionPuestoTrabajo']);
        campoOculto($sensible,'sensible'.$sensible.$codigoCentro.$i);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}


function camposEmergencia($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Procedimientos de emergencia</h3>";
	
	abreColumnaCampos();
	
	campoRadioFormulario($codigoCentro.'20','Dispone la empresa de algún procedimiento de emergencia',$datos);
	
	cierraColumnaCampos();
	abreColumnaCampos();

	areaTextoFormulario($codigoCentro.'51','Comentarios',$datos);
	
	cierraColumnaCampos(true);
}


function camposMateriasPrimas($datos,$codigoCentro){
	    echo "<br />
	    <h3 class='apartadoFormulario'>Materias primas utilizadas</h3>
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaMateriasPrimas".$codigoCentro."'>
	            <thead>
	                <tr>
	                    <th> Materia </th>
	                    <th> Lugar </th>
	                    <th> Cantidad </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM materias_primas_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($materia=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaMateriasPrimas($i,$materia,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaMateriasPrimas(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaMateriasPrimas".$codigoCentro."\");'><i class='icon-plus'></i> Añadir materia prima</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaMateriasPrimas".$codigoCentro."\");'><i class='icon-trash'></i> Eliminar materia prima</button>
	    </div><br />";
}

function imprimeLineaTablaMateriasPrimas($i,$datos,$codigoCentro){
	$j=$i+1;

    echo "<tr>";
        	campoTextoTabla('materiaPrima'.$codigoCentro.'_'.$i,$datos['materia']);
        	campoTextoTabla('lugar'.$codigoCentro.'_'.$i,$datos['lugar']);
        	campoTextoTabla('cantidad'.$codigoCentro.'_'.$i,$datos['cantidad']);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}


function camposCentro($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Características del centro</h3>";
	abreColumnaCampos();

	campoNumeroFormulario($codigoCentro.'21','Superficie total (m<sup>2</sup>)',$datos);
	campoNumeroFormulario($codigoCentro.'22','Ocupación (nº personas)',$datos);
	campoNumeroFormulario($codigoCentro.'23','Nº de plantas',$datos);
	//areaTextoFormulario(51,'Enumeración de instalaciones',$datos,'campoInstalaciones');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoRadioFormulario($codigoCentro.'24','Sectorización',$datos);
	campoTextoFormulario($codigoCentro.'25','Tipo de estructura',$datos);
	campoRadioFormulario($codigoCentro.'49','Planos disponibles',$datos);
	campoFicheroPlano($datos,$codigoCentro);

	cierraColumnaCampos(true);

	echo "<br clearr='all'><h3 class='apartadoFormulario'>Instalaciones</h3>";

	abreColumnaCampos();
		creaTablaInstalaciones($datos,$codigoCentro);
		areaTextoFormulario($codigoCentro.'70','Instalación de gas-oil',$datos);
	cierraColumnaCampos();

	abreColumnaCampos();
		areaTextoFormulario($codigoCentro.'68','Instalación eléctrica',$datos);
		areaTextoFormulario($codigoCentro.'69','Instalación de aire comprimido',$datos);
	cierraColumnaCampos();

	tablaGas($datos,$codigoCentro);
}

function tablaGas($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaGas$codigoCentro'>
	            <thead>
	            	<tr>
	                    <th colspan='4'> Dimensiones reglamentarias de los lugares de trabajo (R.D. 486/1997):  </th>
	                    <th> </th>
	                </tr>
	                <tr>
	                    <th> Zona </th>
	                    <th> m. suelo - techo </th>
	                    <th> m<sup>2</sup>. por trabajador</th>
	                    <th> m<sup>3</sup>. por trabajador</th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM gas_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaGas($i,$item,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaGas(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaGas$codigoCentro\");'><i class='icon-plus'></i> Añadir</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaGas$codigoCentro\");'><i class='icon-trash'></i> Eliminar</button>
	    </div><br />";
}

function imprimeLineaTablaGas($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
        	campoTextoTabla('zonaGas'.$codigoCentro.'_'.$i,$datos['zona']);
        	campoTextoTabla('sueloGas'.$codigoCentro.'_'.$i,$datos['suelo'],'input-mini');
        	campoTextoTabla('m2Gas'.$codigoCentro.'_'.$i,$datos['m2'],'input-mini');
        	campoTextoTabla('m3Gas'.$codigoCentro.'_'.$i,$datos['m3'],'input-mini');
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function campoFicheroPlano($datos,$codigoCentro){
	echo "<div class='hide' id='div".$codigoCentro."49'>";
			campoFichero('pregunta'.$codigoCentro.'55','Fichero de planos',0,$datos,'../documentos/planificacion/','Ver/Descargar',false);
	echo "</div>";
}

function camposHigienicos($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Servicios higiénicos</h3>";
	abreColumnaCampos();
		areaTextoFormulario($codigoCentro.'65','Descripción',$datos);
	cierraColumnaCampos(true);
	tablaHigienicos($datos,$codigoCentro);
}

function tablaHigienicos($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaHigienicos$codigoCentro'>
	            <thead>
	                <tr>
	                    <th> Instalación </th>
	                    <th> Descripción </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM higienicos_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaHigienicos($i,$item,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaHigienicos(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaHigienicos$codigoCentro\");'><i class='icon-plus'></i> Añadir</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaHigienicos$codigoCentro\");'><i class='icon-trash'></i> Eliminar</button>
	    </div><br />";
}

function imprimeLineaTablaHigienicos($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
        	campoTextoTabla('nombreHigienico'.$codigoCentro.'_'.$i,$datos['nombre'],'span4');
        	areaTextoTabla('descripcionHigienico'.$codigoCentro.'_'.$i,$datos['descripcion'],'areaDescripcionCentro');
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function camposIluminacion($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Iluminación</h3>";
	abreColumnaCampos();
		areaTextoFormulario($codigoCentro.'66','Descripción',$datos);
	cierraColumnaCampos(true);
	tablaIluminacion($datos,$codigoCentro);
	abreColumnaCampos();
		areaTextoFormulario($codigoCentro.'67','Iluminación de emergencia',$datos);
	cierraColumnaCampos(true);
}

function tablaIluminacion($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaIluminacion$codigoCentro'>
	            <thead>
	                <tr>
	                    <th> Iluminacion </th>
	                    <th> Descripción </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM iluminacion_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaIluminacion($i,$item,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaIluminacion(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaIluminacion$codigoCentro\");'><i class='icon-plus'></i> Añadir</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaIluminacion$codigoCentro\");'><i class='icon-trash'></i> Eliminar</button>
	    </div><br />";
}

function imprimeLineaTablaIluminacion($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
        	campoTextoTabla('nombreIluminacion'.$codigoCentro.'_'.$i,$datos['nombre'],'span4');
        	areaTextoTabla('descripcionIluminacion'.$codigoCentro.'_'.$i,$datos['descripcion'],'areaDescripcionCentro');
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function camposEvacuacion($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Medidas para casos de emergencia y evacuación</h3>";
	
	abreColumnaCampos();
	
	tablaEvacuacion($datos,$codigoCentro);
	
	cierraColumnaCampos(true);
}

function tablaEvacuacion($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaEvacuacion$codigoCentro'>
	            <thead>
	                <tr>
	                    <th colspan='2'> Zona </th>
	                    <th> Descripción </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM evacuacion_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaEvacuacion($i,$item,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaEvacuacion(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaEvacuacion$codigoCentro\");'><i class='icon-plus'></i> Añadir</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaEvacuacion$codigoCentro\");'><i class='icon-trash'></i> Eliminar</button>
	    </div><br />";
}

function imprimeLineaTablaEvacuacion($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
    		campoSelect('tipoEvacuacion'.$codigoCentro.'_'.$i,'',array('','Pasillos y superficies de transito','Puertas y salidas','Señalizaciones','Vías y salidas de emergencia','Equipos de extinción de incendios','Primeros auxilios'),array(0,1,2,3,4,5,6),$datos['tipo'],'selectpicker span4 show-tick',"data-live-search='true'",1);
        	campoTextoTabla('zonaEvacuacion'.$codigoCentro.'_'.$i,$datos['zona'],'span4');
        	areaTextoTabla('descripcionEvacuacion'.$codigoCentro.'_'.$i,$datos['descripcion'],'areaDescripcionCentro');
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function camposRecursos($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Recursos</h3>";
	
	abreColumnaCampos('sinFlotar');
	
	tablaRecursosH($datos,$codigoCentro);
	
	cierraColumnaCampos(true);

	abreColumnaCampos('sinFlotar');
	
	tablaRecursosT($datos,$codigoCentro);
	
	cierraColumnaCampos(true);

	abreColumnaCampos('sinFlotar');
	
	tablaRecursosE($datos,$codigoCentro);
	
	cierraColumnaCampos(true);
}

function tablaRecursosH($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaRecursosH$codigoCentro'>
	            <thead>
	            	<tr>
	                    <th colspan='5'> Recursos humanos de los que se dispone </th>
	                </tr>
	                <tr>
	                    <th> Recurso </th>
	                    <th> Identificación </th>
	                    <th> Tipo </th>
	                    <th> Puesto </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM recursos_humanos_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaRecursosH($i,$item,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaRecursosH(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaRecursosH$codigoCentro\");'><i class='icon-plus'></i> Añadir</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaRecursosH$codigoCentro\");'><i class='icon-trash'></i> Eliminar</button>
	    </div><br />";
}

function imprimeLineaTablaRecursosH($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
    		campoTextoTabla('recursoRecursosH'.$codigoCentro.'_'.$i,$datos['recurso']);
    		campoTextoTabla('identificacionRecursosH'.$codigoCentro.'_'.$i,$datos['identificacion']);
    		campoSelect('tipoRecursosH'.$codigoCentro.'_'.$i,'',array('','Recurso propio','Recurso externo'),array(0,'PROPIO','EXTERNO'),$datos['tipo'],'selectpicker span3 show-tick',"data-live-search='true'",1);
        	campoTextoTabla('puestoRecursosH'.$codigoCentro.'_'.$i,$datos['puesto']);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function tablaRecursosT($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaRecursosT$codigoCentro'>
	            <thead>
	            	<tr>
	                    <th colspan='3'> Recursos materiales y técnicos de los que se dispone </th>
	                </tr>
	                <tr>
	                    <th> Recurso </th>
	                    <th> Tipo </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM recursos_tecnicos_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaRecursosT($i,$item,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaRecursosT(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaRecursosT$codigoCentro\");'><i class='icon-plus'></i> Añadir</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaRecursosT$codigoCentro\");'><i class='icon-trash'></i> Eliminar</button>
	    </div><br />";
}

function imprimeLineaTablaRecursosT($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
    		campoTextoTabla('recursoRecursosT'.$codigoCentro.'_'.$i,$datos['recurso'],'span4');
    		campoSelect('tipoRecursosT'.$codigoCentro.'_'.$i,'',array('','Espacios','Técnicos','Equipos'),array(0,'ESPACIOS','TECNICOS','EQUIPOS'),$datos['tipo'],'selectpicker span3 show-tick',"data-live-search='true'",1);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function tablaRecursosE($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaRecursosE$codigoCentro'>
	            <thead>
	            	<tr>
	                    <th colspan='5'> Recursos económicos de los que se dispone </th>
	                </tr>
	                <tr>
	                    <th> Recurso </th>
	                    <th> Tipo </th>
	                    <th> Descripción </th>
	                    <th> Importe asignado (€ / año)</th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM recursos_economicos_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaRecursosE($i,$item,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaRecursosE(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaRecursosE$codigoCentro\");'><i class='icon-plus'></i> Añadir</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaRecursosE$codigoCentro\");'><i class='icon-trash'></i> Eliminar</button>
	    </div><br />";
}

function imprimeLineaTablaRecursosE($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
    		campoTextoTabla('recursoRecursosE'.$codigoCentro.'_'.$i,$datos['recurso']);
    		campoSelect('tipoRecursosE'.$codigoCentro.'_'.$i,'',array('','Obligatorio','No obligatorio'),array(0,'OBLIGATORIO','NOOBLIGATORIO'),$datos['tipo'],'selectpicker span3 show-tick',"data-live-search='true'",1);
    		areaTextoTabla('descripcionRecursosE'.$codigoCentro.'_'.$i,$datos['descripcion'],'areaDescripcionCentro');
    		campoTextoTabla('importeRecursosE'.$codigoCentro.'_'.$i,$datos['importe']);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function camposInstalaciones($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Instalaciones que pueden generar una emergencia</h3>";
	abreColumnaCampos('span4');
	campoDato('Suministro eléctrico','');
	campoNumeroFormulario($codigoCentro.'26','Voltaje (V)',$datos);
	campoNumeroFormulario($codigoCentro.'27','Potencia contratada (Kw)',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	areaTextoFormulario($codigoCentro.'28','Riesgos especiales (explosión, inundación, etc)',$datos);

	cierraColumnaCampos(true);
}

function camposAlarmas($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Medidas de alarma</h3>";
	abreColumnaCampos();

	campoRadioFormulario($codigoCentro.'29','Alarma general',$datos);
	campoRadioFormulario($codigoCentro.'30','Detección',$datos);
	campoRadioFormulario($codigoCentro.'31','Walky Talky',$datos);
	areaTextoFormulario($codigoCentro.'35','Observaciones',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();

	campoRadioFormulario($codigoCentro.'32','Pulsadora de alarma',$datos);
	campoRadioFormulario($codigoCentro.'33','Teléfono interno',$datos);
	campoRadioFormulario($codigoCentro.'34','Otros',$datos);;

	cierraColumnaCampos(true);
}

function camposIncendio($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Medidas de protección contra incendios</h3>";
	
	tablaExtintores($datos,$codigoCentro);
	tablaBIE($datos,$codigoCentro);
	tablaOtros($datos,$codigoCentro);
}

function tablaExtintores($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaExtintores$codigoCentro'>
	            <thead>
	            	<tr>
	            		<th colspan='8'>EXTINTORES</th>
	            	</tr>
	                <tr>
	                    <th> Nº </th>
	                    <th> KG </th>
	                    <th> Tipo </th>
	                    <th> Eficacia </th>
	                    <th> Señalizado </th>
	                    <th> Revisado </th>
	                    <th> Ubicación </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM extintores_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($extintor=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaExtintores($i,$extintor,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaExtintores(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaExtintores$codigoCentro\");'><i class='icon-plus'></i> Añadir extintor</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaExtintores$codigoCentro\");'><i class='icon-trash'></i> Eliminar extintor</button>
	    </div><br />";
}

function imprimeLineaTablaExtintores($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
        	campoTextoTabla('numero'.$codigoCentro.'_'.$i,$datos['numero'],'input-small pagination-right');
        	campoTextoTabla('kg'.$codigoCentro.'_'.$i,$datos['kg'],'input-mini pagination-right');
        	campoTextoTabla('tipo'.$codigoCentro.'_'.$i,$datos['tipo'],'input-small pagination-right');
        	campoTextoTabla('eficacia'.$codigoCentro.'_'.$i,$datos['eficacia'],'input-small pagination-right');
        	campoSelect('senializado'.$codigoCentro.'_'.$i,'',array('Si','No'),array('SI','NO'),$datos['senializado'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	campoSelect('revisado'.$codigoCentro.'_'.$i,'',array('Si','No'),array('SI','NO'),$datos['revisado'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	areaTextoTabla('ubicacionExtintor'.$codigoCentro.'_'.$i,$datos['ubicacion']);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function tablaBIE($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaBIE$codigoCentro'>
	            <thead>
	            	<tr>
	            		<th colspan='7'>BOCAS DE INCENDIO EQUIPADAS (BIE)</th>
	            	</tr>
	                <tr>
	                    <th> Nº </th>
	                    <th> Medida </th>
	                    <th> Presión </th>
	                    <th> Señalizado </th>
	                    <th> Revisado </th>
	                    <th> Ubicación </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM bie_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($extintor=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaBIE($i,$extintor,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaBIE(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaBIE$codigoCentro\");'><i class='icon-plus'></i> Añadir BIE</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaBIE$codigoCentro\");'><i class='icon-trash'></i> Eliminar BIE</button>
	    </div><br />";
}

function imprimeLineaTablaBIE($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
        	campoTextoTabla('numeroBIE'.$codigoCentro.'_'.$i,$datos['numero'],'input-small pagination-right');
        	campoSelect('medidaBIE'.$codigoCentro.'_'.$i,'',array('25 mm','45 mm'),array('25','45'),$datos['medida'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	campoTextoTabla('presion'.$codigoCentro.'_'.$i,$datos['presion'],'input-small pagination-right');
        	campoSelect('senializado'.$codigoCentro.'_'.$i,'',array('Si','No'),array('SI','NO'),$datos['senializado'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	campoSelect('revisado'.$codigoCentro.'_'.$i,'',array('Si','No'),array('SI','NO'),$datos['revisado'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	areaTextoTabla('ubicacion'.$codigoCentro.'_'.$i,$datos['ubicacion']);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function tablaOtros($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaOtros$codigoCentro'>
	            <thead>
	            	<tr>
	            		<th colspan='2'>OTROS</th>
	            	</tr>
	            </thead>
	            <tbody>";
	        	echo '<tr><td>Columna seca</td>';
	          	campoSelectFormulario($codigoCentro.'35','',$datos,1);
	          	echo '</tr><tr><td>Hidrante exterior</td>';
	          	campoSelectFormulario($codigoCentro.'36','',$datos,1);
	          	echo '</tr><tr><td>Medios humanos (Equipos de emergencia convenientemente formados)</td>';
	          	campoSelectFormulario($codigoCentro.'37','',$datos,1);
	          	echo '</tr><tr><td>Punto de reunión señalizado</td>';
	          	campoSelectFormulario($codigoCentro.'38','',$datos,1);
	          	echo '</tr><tr><td>Lugar</td>';
	          	campoTextoTablaFormulario($codigoCentro.'39',$datos);
	    echo "	</tbody>
	        </table>
	    </div><br />";
}

function camposPrimerosAuxilios($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Primeros auxilios</h3>";
	abreColumnaCampos('sinFlotar');

	//campoTextoFormulario($codigoCentro.'40','Cantidad de botiquines',$datos,'input-mini pagination-right');
	campoOculto('','pregunta'.$codigoCentro.'40');
	tablaPrimeros('Botiquines',$datos,'botiquines_formulario_prl','Botiquines',$codigoCentro);
	//campoTextoFormulario($codigoCentro.'41','Cantidad de locales de primeros auxilios',$datos,'input-mini pagination-right');
	campoOculto('','pregunta'.$codigoCentro.'41');
	tablaPrimeros('Locales de primeros auxilios',$datos,'locales_formulario_prl','Locales',$codigoCentro);
	
	//campoTextoFormulario($codigoCentro.'42','Cantidad de medios humanos',$datos,'input-mini pagination-right');
	campoOculto('','pregunta'.$codigoCentro.'42');
	tablaPrimeros('Medios humanos',$datos,'medios_humanos_formulario_prl','Medios',$codigoCentro);
	campoRadioFormulario($codigoCentro.'52','Formación en primeros auxilios',$datos);
	campoRadioFormulario($codigoCentro.'53','Desfibrilador',$datos);

	cierraColumnaCampos(true);
}

function tablaPrimeros($texto,$datos,$tabla,$prefijo,$codigoCentro){
	    echo "
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='".$tabla.$codigoCentro."'>
	            <thead>
	            	<tr>
	            		<th colspan='5'>".$texto."</th>
	            	</tr>
	                <tr>
	                	<th> Nº </th>
	                    <th> Señalizado </th>
	                    <th> Revisado </th>
	                    <th> Ubicación </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM ".$tabla." WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaPrimeros($i,$item,$prefijo,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaPrimeros(0,false,$prefijo,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"".$tabla.$codigoCentro."\");'><i class='icon-plus'></i> Añadir $prefijo</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"".$tabla.$codigoCentro."\");'><i class='icon-trash'></i> Eliminar $prefijo</button>
	    </div><br />";
}

function imprimeLineaTablaPrimeros($i,$datos,$prefijo,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
    		campoTextoTabla('numero'.$prefijo.$codigoCentro.'_'.$i,$datos['numero'],'input-mini pagination-right');
        	campoSelect('senializado'.$prefijo.$codigoCentro.'_'.$i,'',array('Si','No'),array('SI','NO'),$datos['senializado'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	campoSelect('revisado'.$prefijo.$codigoCentro.'_'.$i,'',array('Si','No'),array('SI','NO'),$datos['revisado'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	areaTextoTabla('ubicacion'.$prefijo.$codigoCentro.'_'.$i,$datos['ubicacion']);
    echo "	<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    	  </tr>";
}

function camposVestuarios($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Vestuarios</h3>";
	abreColumnaCampos();

	campoRadioFormulario($codigoCentro.'41','Lavabo/s',$datos);
	campoTextoFormulario($codigoCentro.'42','Cantidad',$datos,'input-mini pagination-right');
	campoRadioFormulario($codigoCentro.'43','Ducha/s',$datos);
	campoTextoFormulario($codigoCentro.'44','Cantidad',$datos,'input-mini pagination-right');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoRadioFormulario($codigoCentro.'45','¿Mixto?',$datos);
	campoRadioFormulario($codigoCentro.'46','Taquillas',$datos);
	campoRadioFormulario($codigoCentro.'47','En caso anterior afirmativo, ¿disponen de compartimentos separados?',$datos);;

	cierraColumnaCampos(true);
}

function camposPuestosTrabajos($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Puestos de trabajos</h3>";
	abreColumnaCampos();
	creaTablaPuestosTomaDatos($datos['codigo'],$datos['codigoContrato'],'NO',$codigoCentro);
	cierraColumnaCampos(true);
}

function camposObservaciones($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Observaciones</h3>";
	abreColumnaCampos();
	areaTextoFormulario($codigoCentro.'48','Observaciones',$datos,'areaInforme');
	cierraColumnaCampos(true);
}

//Fin parte Toma de Datos

/*function tomaDatos(){
	$prl =datosRegistro('formularioPRL',$_GET['codigoPRL']);
	$formulario = recogerFormularioServicios($prl);
	$formulario = completarDatosGeneralesPRL($formulario,$prl['codigoCliente']);

	abreVentanaGestionConBotones('Toma de datos PRL','index.php','','icon-edit','margenAb');
	campoOculto($_GET['codigoPRL'],'codigoPRL');
	abreColumnaCampos();
		campoTextoFormulario(1,'Consultor',$formulario);
		//campoRadio('toma_datos'.'Toma de datos finalizada',$trabajo['toma_datos']);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoFechaFormulario(2,'Fecha',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DATOS GENERALES</h3>";

	abreColumnaCampos();
		campoTextoFormulario(3,'Razón Social',$formulario);
		campoTextoFormulario(4,'Dirección Social',$formulario);
		campoTextoFormulario(10,'Código postal',$formulario);
		campoTextoFormulario(8,'Representate legal',$formulario);
		campoTextoFormulario(6,'Teléfono',$formulario);
		campoTextoFormulario(7,'Nº de trabajadores',$formulario);
		campoRadioFormulario(16,'¿Hay trabajadores en centros ajenos?',$formulario);
		campoTextoFormulario(67,'Sector',$formulario);
		campoTextoFormulario(68,'Persona de contacto',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(9,'CIF',$formulario);
		campoTextoFormulario(5,'Localidad',$formulario);
		campoTextoFormulario(11,'Provincia',$formulario);
		campoTextoFormulario(14,'NIF',$formulario);
		campoTextoFormulario(12,'Email',$formulario);
		campoTextoFormulario(15,'Nº de centros',$formulario);
		echo "<br/><br/><br/>";
		campoTextoFormulario(13,'Actividad empresarial',$formulario);
		campoTextoFormulario(69,'Responsable del Plan de Prevención',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DESCRIPCIÓN DEL CENTRO</h3>";

	abreColumnaCampos();
		campoTextoFormulario(17,'Localización (zona industrial, comercial, residencial)',$formulario);
		campoTextoFormulario(19,'Nº de plantas',$formulario);
		campoTextoFormulario(21,'Aforo máximo',$formulario);
		
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(18,'Superficie m2',$formulario);
		echo "<br/>";
		campoTextoFormulario(20,'Tipo de plantas (bajos, primera planta, ático)',$formulario);
		campoRadioFormulario(22,'¿Mismo nivel?',$formulario);
	cierraColumnaCampos();

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Nombre y apellidos' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaEmpleadosPRL'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
                  			<th colspan='6'>Trabajadores</th>
                  		</tr>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Nombre y apellidos </th>
							<th> DNI </th>
							<th> Puesto de trabajo </th>
							<th> Descripción funciones </th>
							<th> ¿Salen fuera del centro? </th>
							<th> En caso afirmativo ¿Tipo de vehículo? </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($prl){
        	$empleados = consultaBD("SELECT * FROM empleados_prl WHERE codigoTrabajo=".$prl['codigo'],true);
        	while($empleado = mysql_fetch_assoc($empleados)){
        		echo "<tr>";
    				campoTextoTabla('nombreEmpleadoPRL'.$indice,$empleado['nombre']);
    				campoTextoTabla('dniEmpleadoPRL'.$indice,$empleado['dni'],'input-small');
    				campoTextoTabla('puestoEmpleadoPRL'.$indice,$empleado['puesto'],'input-small');
    				areaTextoTabla('funcionesEmpleadoPRL'.$indice,$empleado['funciones']);
    				campoSelect('salenEmpleadoPRL'.$indice,'',array('Si','No'),array('SI','NO'),$empleado['salen'],'selectpicker span2 show-tick','data-live-search="true"',1);
    				campoTextoTabla('vehiculoEmpleadoPRL'.$indice,$empleado['vehiculo'],'input-small');
				echo"
					</tr>";
        		$indice++;
        	}
        }
    	for($indice;$indice<5;$indice++){
    		echo "<tr>";
    			campoTextoTabla('nombreEmpleadoPRL'.$indice);
    			campoTextoTabla('dniEmpleadoPRL'.$indice,'','input-small');
    			campoTextoTabla('puestoEmpleadoPRL'.$indice,'','input-small');
    			areaTextoTabla('funcionesEmpleadoPRL'.$indice);
    			campoSelect('salenEmpleadoPRL'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
    			campoTextoTabla('vehiculoEmpleadoPRL'.$indice,'','input-small');
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaEmpleadosPRL\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Centro de trabajo' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaCentrosPRL'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
                  			<th colspan='5'>Centros de trabajo</th>
                  		</tr>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Centro de trabajo </th>
							<th> Nº delegados<br/>de prevención </th>
							<th> Comite de<br/>seguridad y salud </th>
							<th> Trabajador designado </th>
							<th> Consulta directa <br/>a los trabajadores </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($prl){
        	$centros = consultaBD("SELECT * FROM centros_prl WHERE codigoTrabajo=".$prl['codigo'],true);
        	while($centro = mysql_fetch_assoc($centros)){
        		echo "<tr>";
    				campoTextoTabla('nombreCentroPRL'.$indice,$centro['nombre']);
    				campoTextoTabla('delegadoCentroPRL'.$indice,$centro['delegado'],'input-small');
    				campoTextoTabla('comiteCentroPRL'.$indice,$centro['comite']);
    				campoTextoTabla('trabajadorCentroPRL'.$indice,$centro['trabajador']);
    				campoSelect('consultaCentroPRL'.$indice,'',array('Si','No'),array('SI','NO'),$centro['consulta'],'selectpicker span2 show-tick','data-live-search="true"',1);
				echo"
					</tr>";
        		$indice++;
        	}
        }

    		echo "<tr>";
    			campoTextoTabla('nombreCentroPRL'.$indice);
    			campoTextoTabla('delegadoCentroPRL'.$indice,'','input-small');
    			campoTextoTabla('comiteCentroPRL'.$indice);
    			campoTextoTabla('trabajadorCentroPRL'.$indice);
    			campoSelect('consultaCentroPRL'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
			echo"
				</tr>";
    
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaCentrosPRL\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";

	echo "<br clear='all'><h3 class='apartadoFormulario'> INSTALACIONES</h3>";
	abreColumnaCampos();
		campoRadioFormulario(23,'Dispone de instalación de acondicionamiento de aire (calefacción, aire)',$formulario);
		campoRadioFormulario(32,'¿Dispone de cuadro electríco',$formulario);
		echo "<div id='div32' class='hidden'>";
			campoRadioFormulario(33,'Está señalizado',$formulario);
			campoTextoFormulario(34,'Ubicación',$formulario);
		echo "</div>";
		campoRadioFormulario(25,'Dispone de instalación de extracción de humos (campana extractora)',$formulario);


	cierraColumnaCampos();

	abreColumnaCampos();
		echo "<div id='div23' class='hidden'>";
		campoRadioFormulario(24,'¿Cuales?',$formulario,'CALEFACCION',array('Calefaccion','Aire'),array('CALEFACCION','AIRE'));
		echo "</div>";
		echo "<br/><br/>";
		campoRadioFormulario(26,'Dispone de Montacargas/ascensores',$formulario);
		echo "<div id='div26' class='hidden'>";
			campoTextoFormulario(27,'¿Cuantos?',$formulario);
			campoTextoFormulario(28,'Empresa de mantenimiento',$formulario);
		echo "</div>";
		echo "Dispones de otras:";
		campoRadioFormulario(29,'Aire',$formulario);
		campoRadioFormulario(30,'Gas',$formulario);
		campoRadioFormulario(31,'Frigos',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> ORGANIZACIÓN EMPRESA</h3>";

	abreColumnaCampos();
		campoRadioFormulario(75,'Dirección',$formulario,'INTERMEDIO',array('Mando intermedio','Trabajadores'),array('INTERMEDIO','TRABAJADORES'),true);
		campoRadioFormulario(35,'Qué medio de comunicación se utiliza para la prevención',$formulario,'ORAL',array('Oral','Correo electrónico','Carta'),array('ORAL','CORREO','CARTA'),true);
		campoRadioFormulario(36,'Qué recursos dispone para esta actividad:',$formulario,'OFICINA',array('Oficina','Equipos informáticos','Impresora','Otros'),array('OFICINA','EQUIPOS','IMPRESORA','OTROS'),true);
		campoTextoFormulario(71,'Trabajador responsable de la seguridad',$formulario);
		campoTextoFormulario(72,'Trabajador responsable de la ergonomía y psicología',$formulario);

	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(76,'Tipo de prevención',$formulario,'EMPRESARIO',array('Empresario','Trabajador designado'),array('EMPRESARIO','TRABAJADOR'),true);
		campoRadioFormulario(66,'Formación del responsable de prevención',$formulario,'BASICO',array('Básico','Medio','Superior'),array('BASICO','MEDIO','SUPERIOR'),true);
		campoRadioFormulario(70,'Servicio de prevencion:',$formulario,'PROPIO',array('Propio','Mancomunado','Ajeno','Sistema Mixto'),array('PROPIO','MANCOMUNADO','AJENO','MIXTO'),true);
		campoTextoFormulario(73,'Trabajador responsable de la higiene',$formulario);
		campoTextoFormulario(74,'Trabajador responsable de la vigilancia de la salud',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> MEDIDAS DE EMERGENCIA</h3>";

	abreColumnaCampos();
		campoRadioFormulario(37,'¿Alguien en la empresa dispone de formación en primeros auxilios?',$formulario);
		echo "<br/>";
		campoTextoFormulario(38,'Instalación de seguridad contraincendios perteneciente al edificio donde se ubica el local o la oficina',$formulario);
		campoTextoFormulario(41,'Nº de Bies',$formulario);
		echo "<br/>";
		campoRadioFormulario(43,'Sistema de detección automática de incendios',$formulario);
		campoRadioFormulario(45,'Extintores',$formulario);
		echo "<div id='div45' class='hidden'>";
			echo "Tipos y carácteristicas:";
			campoRadioFormulario(47,'ABC',$formulario);
			campoRadioFormulario(48,'Espuma',$formulario);
			campoRadioFormularioConLogo(51,'Señal de seguridad de cada extintor',$formulario,'iconExtintor.jpg');
		echo "</div>";
		campoRadioFormulario(53,'En todo caso existirá el alumbrado de emergencia en las puertas de salida',$formulario);
		campoRadioFormulario(55,'Sistema de alarmas',$formulario,'AUTOMATICA',array('Alarma automática de incendios','Megafonía/telefonía','Alarma manual de incendios (con pulsador de alarma)'),array('AUTOMATICA','MEGAFONIA','MANUAL'),true);

	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(39,'¿Alguien en la empresa dispone de formación en extinción de incendios?',$formulario);
		campoTextoFormulario(40,'Instalación de seguridad contraincendios formada por red de agua y Bies (Bocas de incendio equipadas)',$formulario);
		campoRadioFormularioConLogo(42,'Señal de Seguridad de cada BIE',$formulario,'iconBie.png');
		campoRadioFormulario(44,'Sistema de actuación automática de incendios (sprinkkles)',$formulario);
		echo "<div id='div45_1' class='hidden'>";
			campoTextoFormulario(46,'Nº de Extintores',$formulario);
			echo "<br/>";
			campoRadioFormulario(49,'CO2',$formulario);
			campoRadioFormulario(50,'Otros',$formulario);
		echo "</div>";
		echo "<br/>";
		campoRadioFormulario(52,'Alumbrado de emergencia',$formulario);
		campoRadioFormulario(54,'Nº de lámparas alumbrado de emergencia',$formulario);
		echo "<div id='div26_1' class='hidden'>";
			campoRadioFormularioConLogo(56,'Si dispone de ascensores/montacargas deberá contar con la preceptiva señal de seguridad',$formulario,'iconAscensor.png');
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> RECURSOS TÉCNICOS EN CASO DE EVACUACIÓN</h3>";
	abreColumnaCampos();
		campoRadioFormularioConLogo(57,'Señal de salvamento. Recorrido de evacuación horizontal',$formulario,'iconHorizontal.jpg');
		campoRadioFormularioConLogo(58,'Señal de salvamento. Recorrido de evacuación vertica',$formulario,'iconVertical.png');
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormularioConLogo(59,'Señal de salvamento. Recorrido de evacuación',$formulario,'iconDireccion.png');
		campoRadioFormularioConLogo(60,'Señal de salvamento. Teléfono de salvamento',$formulario,'iconTelefono.png');
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> RECURSOS TÉCNICOS EN CASO DE PRIMEROS AUXILIOS</h3>";
	abreColumnaCampos();
		campoRadioFormularioConLogo(61,'Botiquín. En todo caso existirá en el centro de trabajo un botiquín de primeros auxilios junto con la señalización informativa correspondiente',$formulario,'iconBotiquin.png');
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormularioConLogo(62,'Sistema lavaojos. Señal informativa',$formulario,'iconLavaojos.png');
		campoRadioFormulario(63,'Otros',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> VIGILANCIA DE SALUD</h3>";
	abreColumnaCampos();
		campoRadioFormulario(64,'¿Dispone usted de un servicio de Vigilancia de la Salud (reconocimientos médicos)?',$formulario);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormulario(65,'¿En caso negativo, querría que le pusiéramos en contacto con un colaborador nuestro?',$formulario);
	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}*/

function imprimeFormularioPRL($where){
	global $_CONFIG;
	$tipos=array(1=>'SPA 4 Especialidades',2=>'SPA Vigilancia de la salud',3=>'SPA Especialides técnicas',4=>'Otras actuaciones');

	$whereEjercicio=defineWhereEjercicio('',array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaMail'));
	$where.=str_replace('WHERE',' AND (',$whereEjercicio).' OR (visitada="NO" OR planPrev="NO" OR pap="NO" OR info="NO" OR memoria="NO" OR vs="NO")';
	
	if($_SESSION['ejercicio']!='' && $_SESSION['ejercicio']!='Todos'){
		$where.=' AND YEAR(f.fecha)<='.$_SESSION['ejercicio'].')';
	}

	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FACTURACION'){
		$where.=' AND codigoUsuarioTecnico='.$_SESSION['codigoU'];
	}


	conexionBD();
	$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, visitada, planPrev, pap, info, f.horasPrevistas, memoria, fechaVisita, fechaPlanPrev, fechaPap, fechaInfo,  
						  fechaMemoria, vs, codigoUsuarioTecnico, CONCAT(EMPNOMBRE,' / ',EMPMARCA) AS cliente,f.codigoContrato, ofertas.codigoCliente, mesVS, anioVS, opcion, suspendido, (ofertas.total-ofertas.importe_iva) AS importe, incrementoIPC, incrementoIpcAplicado

						  FROM formularioPRL f LEFT JOIN contratos ON f.codigoContrato=contratos.codigo 
						  LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo 
						  LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo 

						  ".$where." AND f.eliminado='NO' AND f.aprobado='NO';");


	/*if($_SESSION['tipoUsuario'] == 'ADMIN'){
		$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, c.razon_s AS cliente FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo ORDER BY c.razon_s;");
	} else {
		$director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],true, true);
		if($director['director'] == 'SI'){
			$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, c.razon_s AS cliente FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE c.empleado IN (SELECT codigo FROM usuarios WHERE tipo LIKE '".$director['tipo']."') ORDER BY c.razon_s;");
		} else {
			$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, c.razon_s AS cliente FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE c.empleado LIKE ".$_SESSION['codigoU']." ORDER BY c.razon_s;");
		}
	}*/
	
	$tabla='';
	$iconoValidado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		
		$referencia='';
		if($datos['codigoContrato']!=NULL){
			$contrato=consultaBD('SELECT * FROM contratos WHERE codigo='.$datos['codigoContrato'],false,true);
			$cliente=consultaBD('SELECT * FROM clientes WHERE codigo="'.$datos['codigoCliente'].'"',false,true);
			$referencia=formateaReferenciaContrato($cliente,$contrato);
		}
		$suspendido=$datos['suspendido']=='SI'?'<i class="icon-exclamation iconoFactura icon-danger animated infinite flash"></i> ':'';
		$visitada=consultaBD('SELECT * FROM planificacion_visitas WHERE codigoFormulario='.$datos['codigo'],false,true);
		
		if($visitada){
			if($visitada['fechaReal']=='' || $visitada['fechaReal']=='0000-00-00'){
				$datos['visitada']='NO';
			} else{
				$datos['visitada']='SI';
			}
		}
		$tabla.= "
			<tr>
				<td>".$referencia."</td>
				<td>".$tipos[$datos['opcion']]."</td>
				<td>".$suspendido.$datos['cliente']."</td>
				<td class='centro'>".$iconoValidado[$datos['visitada']];
				if($datos['visitada'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaVisita']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['planPrev']];
				if($datos['planPrev'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaPlanPrev']);
                }
                $tabla.="</td>		
				<td class='centro'>".$iconoValidado[$datos['pap']];
				if($datos['pap'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaPap']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['info']];
				if($datos['info'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaInfo']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['memoria']];
				if($datos['memoria'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaMemoria']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['vs']];
				if($datos['vs'] == 'NO'){
					if($datos['mesVS']!='00' && $datos['anioVS']!='00'){
                    	$tabla.="<br/>".obtieneFechaLimite($datos['anioVS'].'-'.$datos['mesVS'].'-01');
                	}
                }
                $tabla.="</td>	
				<td class='centro'>".$datos['horasPrevistas'];
                $tabla.="</td>	
                <td> ".compruebaImporteContrato($datos['importe'],$datos)." € </td>				 
				<td class='centro'>
					<a class='btn btn-propio' href='".$_CONFIG['raiz']."planificacion-pendiente/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	cierraBD();
	echo $tabla;
}

function imprimeFormularioPRLeliminados($where=false){
	global $_CONFIG;

	$where=defineWhereEjercicio($where,array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaMail'));
	if($_SESSION['tipoUsuario']!='ADMIN'){
		$where.=' AND codigoUsuarioTecnico='.$_SESSION['codigoU'];
	}
	conexionBD();
	$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, visitada, planPrev, pap, info, f.horasPrevistas, memoria, fechaVisita, fechaPlanPrev, fechaPap, fechaInfo,  fechaMemoria, vs, codigoUsuarioTecnico, EMPMARCA AS cliente,f.codigoContrato, ofertas.codigoCliente, mesVS, anioVS FROM formularioPRL f LEFT JOIN contratos ON f.codigoContrato=contratos.codigo LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND f.eliminado='SI' AND f.aprobado='NO';");
	
	$tabla='';
	$iconoValidado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		
		$referencia='';
		if($datos['codigoContrato']!=NULL){
			$contrato=consultaBD('SELECT * FROM contratos WHERE codigo="'.$datos['codigoContrato'].'";',false,true);
			$cliente=consultaBD('SELECT * FROM clientes WHERE codigo="'.$datos['codigoCliente'].'";',false,true);
			$referencia=formateaReferenciaContrato($cliente,$contrato);
		}
		$tabla.= "
			<tr>
				<td>".$referencia."</td>
				<td>".$datos['cliente']."</td>
				<td class='centro'>".$iconoValidado[$datos['visitada']];
				if($datos['visitada'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaVisita']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['planPrev']];
				if($datos['planPrev'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaPlanPrev']);
                }
                $tabla.="</td>		
				<td class='centro'>".$iconoValidado[$datos['pap']];
				if($datos['pap'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaPap']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['info']];
				if($datos['info'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaInfo']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['memoria']];
				if($datos['memoria'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaMemoria']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['vs']];
				if($datos['vs'] == 'NO'){
                    if($datos['mesVS']!='00' && $datos['anioVS']!='00'){
                    	$tabla.="<br/>".obtieneFechaLimite($datos['anioVS'].'-'.$datos['mesVS'].'-01');
                	}
                }
                $tabla.="</td>	
				<td class='centro'>".$datos['horasPrevistas'];
                $tabla.="</td>					 
				<td class='centro'>
					<div class='btn-group'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."planificacion-pendiente/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
						</ul>
					</div>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	echo $tabla;
	cierraBD();
	
}



function obtieneCliente(){
	$datos=arrayFormulario();//Recibirá el codigo como si vienera de un campo del formulario
	$cliente=consultaBD("SELECT * FROM clientes WHERE codigo=".$datos['codigo'],true,true);

	echo json_encode($cliente);//IMPORTANTE: un echo, no un return, y convierte el array en formato JSON.
}

function estadisticasFormulariosRestrict($where){
	$res=array();

	conexionBD();
	$whereEjercicio=defineWhereEjercicio('',array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaMail'));
	$where.=str_replace('WHERE',' AND (',$whereEjercicio).' OR (visitada="NO" OR planPrev="NO" OR pap="NO" OR info="NO" OR memoria="NO" OR vs="NO")';

	if($_SESSION['ejercicio']!='' && $_SESSION['ejercicio']!='Todos'){
		$where.='AND YEAR(f.fecha)<='.$_SESSION['ejercicio'].')';
	}

	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FACTURACION'){
		$where.=' AND codigoUsuarioTecnico='.$_SESSION['codigoU'];
	}
	$consulta=consultaBD("SELECT COUNT(f.codigo) AS total FROM formularioPRL f LEFT JOIN contratos ON f.codigoContrato=contratos.codigo LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND f.aprobado='NO' AND f.eliminado='NO' ORDER BY EMPNOMBRE;",false,true);

	/*if($_SESSION['tipoUsuario'] == 'ADMIN'){
		$consulta=consultaBD("SELECT COUNT(f.codigo) AS total FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo ORDER BY c.razon_s;",false,true);
	} else {
		$director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],false, true);
		if($director['director'] == 'SI'){
			$consulta=consultaBD("SELECT COUNT(f.codigo) AS total FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE c.empleado IN (SELECT codigo FROM usuarios WHERE tipo LIKE '".$director['tipo']."') ORDER BY c.razon_s;",false,true);
		} else {
			$consulta=consultaBD("SELECT COUNT(f.codigo) AS total FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE c.empleado LIKE ".$_SESSION['codigoU']." ORDER BY c.razon_s;",false,true);
		}
	}*/

	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

function estadisticasPlanificacionesEliminadas($eliminado){
	$res=array();

	conexionBD();
	$where=defineWhereEjercicio("WHERE f.eliminado='$eliminado' AND f.aprobado='NO'",array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaMail'));
	if($_SESSION['tipoUsuario']!='ADMIN'){
		$where.=' AND codigoUsuarioTecnico='.$_SESSION['codigoU'];
	}
	$consulta=consultaBD("SELECT COUNT(f.codigo) AS total FROM formularioPRL f LEFT JOIN contratos ON f.codigoContrato=contratos.codigo LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." ORDER BY EMPNOMBRE;",false,true);


	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

function generaNumero(){
	$consulta=consultaBD("SELECT MAX(codigoInterno) AS numero FROM formularioPRL", true, true);
	
	return $consulta['numero']+1;
}

function generaEventosAgenda(){	
	conexionBD();
	$consulta=consultaBD("SELECT * FROM formularioPRL",true);
	$tareas=array();
	$i=0;
	while($prl=mysql_fetch_assoc($consulta)){
		$cliente=consultaBD('SELECT clientes.codigo FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='.$prl['codigoContrato'],true,true);
		if($prl['visitada']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaVisita'];
			$tareas[$i]['tarea']='Visita';
			$i++;
		}
		if($prl['planPrev']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaPlanPrev'];
			$tareas[$i]['tarea']='Plan prevención';
			$i++;
		}
		if($prl['er']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaEr'];
			$tareas[$i]['tarea']='Evaluación de riesgos';
			$i++;
		}
		if($prl['pap']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaPap'];
			$tareas[$i]['tarea']='Planificación preventiva';
			$i++;
		}
		if($prl['info']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaInfo'];
			$tareas[$i]['tarea']='Información a los trabajadores';
			$i++;
		}
		if($prl['cert']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaCert'];
			$tareas[$i]['tarea']='Certificados de formación';
			$i++;
		}
		if($prl['memoria']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaMemoria'];
			$tareas[$i]['tarea']='Memoria';
			$i++;
		}
		if($prl['vs']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaVs'];
			$tareas[$i]['tarea']='Vigilancia de la salud';
			$i++;
		}
	}
	$calendario="";
	
	foreach ($tareas as $datos) {
		$cliente=datosRegistro('clientes',$datos['codigoCliente']);
		$fechaInicio=explode('-',$datos['fecha']);
		$fechaFin=explode('-',$datos['fecha']);
		$fechaInicio[1]--;
		$fechaFin[1]--;
		$horaInicio=explode(':','08:00');
		$horaFin=explode(':','08:00');

	    $calendario.="
	    	{
	    		id: ".$datos['codigoInterno'].",
	    		title: '".$cliente['EMPNOMBRE']." - ".$datos['tarea']."',
			    start: new Date(".$fechaInicio[0].", ".$fechaInicio[1].", ".$fechaInicio[2].", ".$horaInicio[0].", ".$horaInicio[1]."),
		    	end: new Date(".$fechaFin[0].", ".$fechaFin[1].", ".$fechaFin[2].", ".$horaFin[0].", ".$horaFin[1]."),
		    	allDay: true,
		    	backgroundColor: '#009F93', 
		    	borderColor: '#009F93',
		    	codigoGestion: 'NO'
	    	},";
  	}

  	cierraBD();

  	$tareas=substr_replace($calendario, '', strlen($calendario)-1, strlen($calendario));//Para quitar última coma  	
  	echo $tareas;
}

function obtieneFechaLimiteAJAX($fecha,$suma){
	$fecha = formateaFechaBD($fecha);
	$fecha = new DateTime($fecha);
	$fecha->add(new DateInterval($suma));
	return $fecha->format('d/m/Y');
}

function tablaFicheros($datos,$num=1){
	if($num==1){
		$tabla='tablaDocumentos1';
		$bd='documentos_plan_prev';
		$prefijo='plan';
	} else {
		$tabla='tablaDocumentos2';
		$bd='documentos_info';
		$prefijo='info';
	}
	echo"
	 		<center>
	 		
				<table class='table table-bordered mitadAncho' id='".$tabla."'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Nombre </th>
							<th> Fichero </th>
							<!--th> Eliminar </th-->
                    	</tr>
                  	</thead>
                  	<tbody>";

					    $i=0;
						if($datos){
							$consulta=consultaBD("SELECT * FROM ".$bd." WHERE codigoFormulario=".$datos['codigo'],true);				  
							while($fichero=mysql_fetch_assoc($consulta)){
							 	echo "<tr>";
							 		campoOculto($fichero['codigo'],'codigoFichero_'.$prefijo.$i);
									campoTextoTabla('nombreSubido_'.$prefijo.$i,$fichero['nombre']);
									campoFichero('ficheroSubido_'.$prefijo.$i,'',1,$fichero['fichero'],'../documentos/planificacion/','Ver/descargar');
									//echo "<td><input type='checkbox' name='eliminaFichero_".$prefijo.$i."' value='".$fichero['codigo']."'></td>";
								echo"
									</tr>";
								$i++;
							}
						}
						campoOculto($i,'ficherosTotales_'.$prefijo);
						$i=0;
						echo "<tr>";
							campoTextoTabla('nombre_'.$prefijo.'0');
							campoFichero('fichero_'.$prefijo.'0','Documento',1);
							//echo "<td></td>";
						echo "</tr>";

						echo "
					</tbody>
	                </table>
	            

					<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"".$tabla."\");'><i class='icon-plus'></i> Añadir documento</button>
				</center><br />";
}

function eliminaPlanificacion(){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();

    for($i=0;isset($datos['codigo'.$i]);$i++){
		$imagenes=consultaBD("SELECT * FROM documentos_plan_prev WHERE codigoFormulario='".$datos['codigo'.$i]."';");
		while($imagen=mysql_fetch_assoc($imagenes)){
        	if($imagen['fichero'] != '' && $imagen['fichero'] != 'NO'){
            	unlink('ficheros/'.$imagen['fichero']);
        	}
    	}
    	$imagenes=consultaBD("SELECT * FROM documentos_info WHERE codigoFormulario='".$datos['codigo'.$i]."';");
		while($imagen=mysql_fetch_assoc($imagenes)){
        	if($imagen['fichero'] != '' && $imagen['fichero'] != 'NO'){
            	unlink('ficheros/'.$imagen['fichero']);
        	}
    	}
        $consulta=consultaBD("DELETE FROM formularioPRL WHERE codigo='".$datos['codigo'.$i]."';");
        if(!$consulta){
            $res=false;
            echo mysql_error();
        }
    }
    cierraBD();

    return $res;
}


function compruebaCambioDatosContrato(){
	global $_CONFIG;
	$res=true;
	$datos=arrayFormulario();

	if($datos['pregunta6']=='SI' && $datos['pregunta6Anterior']=='NO'){//Se cambia por primera vez...

		$mensaje="El motivo de este mensaje es informarle que se han modificado los datos del contrato para el cliente cliente ".$datos['pregunta0']." en su toma de 
				  datos del apartado Planificación (https://crmparapymes.com.es".$_CONFIG['raiz'].").<br><br/>
				  En las observaciones de la misma se ha indicado lo siguiente:<br /><br />
				  <strong><i>".nl2br($datos['pregunta16'])."</i></strong><br /><br />
				  Este mensaje se ha generado automáticamente desde el software. Por favor, no lo responda.";


		$res=enviaEmail('info@anescoprl.es', 'Cambio de información de contrato en Toma de Datos - Anesco', $mensaje );
	}

	return $res;
}



function creaTablaInstalaciones($datos,$codigoCentro){
	    echo "
	    <div class='control-group'>                     
	        <label class='control-label'>Enumeración de instalaciones:</label>
	        <div class='controls'>
	            <table class='tabla-simple mitadAncho' id='tablaInstalaciones".$codigoCentro."'>
	                <thead>
	                    <tr>
	                        <th> Instalaciones </th>
	                        <th> </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos){
	                $consulta=consultaBD("SELECT codigoInstalacion FROM instalaciones_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($instalacion=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaInstalaciones($i,$instalacion,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaInstalaciones(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaInstalaciones".$codigoCentro."\");'><i class='icon-plus'></i> Añadir instalación</button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaInstalaciones".$codigoCentro."\");'><i class='icon-trash'></i> Eliminar instalación</button>
	        </div>
	    </div>";
}

function imprimeLineaTablaInstalaciones($i,$datos,$codigoCentro){
	$j=$i+1;

    echo "<tr>";
        campoSelectConsulta('codigoInstalacion'.$codigoCentro.'_'.$i,'',"SELECT codigo, nombre AS texto FROM instalaciones ORDER BY nombre;",$datos['codigoInstalacion'],'selectpicker span4 show-tick','data-live-search="true"','',1,false);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function campoUsuarioTecnicoPlanificacion($datos){
	if($_SESSION['tipoUsuario']=='ADMIN'){
		campoSelectConsulta('codigoUsuarioTecnico','Técnico/Enfermera','SELECT codigo, CONCAT(nombre, " ", apellidos,IF(habilitado="NO"," (Inactivo)","")) AS texto FROM usuarios WHERE tipo IN("TECNICO","ENFERMERIA","MEDICO") AND (habilitado="SI" OR codigo="'.$datos['codigoUsuarioTecnico'].'");',$datos);
	}
	else{
		campoOculto($datos,'codigoUsuarioTecnico',$_SESSION['codigoU']);
	}
}


function creaPestaniasFormularioPRL(){
	$codigoFormulario=$_GET['codigoPRL'];
	$res=array();
	$paginas=array('<i class="icon-file-text-o"></i> Datos generales');

	$consulta=consultaBD("SELECT clientes_centros.codigo, direccion 
						  FROM clientes_centros INNER JOIN ofertas ON clientes_centros.codigoCliente=ofertas.codigoCliente
						  INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
						  INNER JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
						  WHERE direccion!='' AND formularioPRL.codigo=$codigoFormulario",true);

	while($centro=mysql_fetch_assoc($consulta)){
		array_push($paginas,"<i class='icon-building-o'></i> Centro en ".$centro['direccion']);
		array_push($res,$centro['codigo']);
	}

	creaPestaniasAPI($paginas);

	return $res;
}