<?php
  $seccionActiva=9;
  include_once('../cabecera.php');
  
  $res=operacionesFormularioPRL();
  $where='WHERE ((enVigor="SI" AND contratos.eliminado="NO") OR (enVigor="NO" AND contratos.fechaInicio>CURDATE()) OR codigoContrato IS NULL)';//Planificaciones con contratos en vigor, futuribles o sin contrato
  $texto='';
  $boton="<a href='".$_CONFIG['raiz']."planificacion-pendiente/index.php?baja' class='shortcut'><i class='shortcut-icon icon-arrow-down'></i><span class='shortcut-label'>De clientes/contratos de baja</span> </a>";
  if(isset($_GET['baja'])){
    /*$where='WHERE clientes.codigo NOT IN (SELECT clientes.codigo FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE activo="SI" AND contratos.enVigor="SI" AND clientes.eliminado="NO")';*/
    $where='WHERE ((enVigor="NO" AND contratos.fechaInicio<=CURDATE()) OR contratos.eliminado="SI")';
    $texto=' de clientes de baja';
    $boton="<a href='".$_CONFIG['raiz']."planificacion-pendiente/index.php?' class='shortcut'><i class='shortcut-icon icon-chevron-left'></i><span class='shortcut-label'>Volver</span> </a>";
  }
  $estadisticas=estadisticasFormulariosRestrict($where);

  /*$documentos=consultaBD('SELECT * FROM formularioPRL',true);
  while($documento=mysql_fetch_assoc($documentos)){
    echo $documento['codigo'].' - '.$documento['fechaVs'].'<br/>';
    if($documento['fechaVs']!='0000-00-00' && $documento['fechaVs']!=''){
      $fecha=explode('-', $documento['fechaVs']);
      $sql='UPDATE formularioPRL SET mesVS="'.$fecha[1].'",anioVS="'.$fecha[0].'" WHERE codigo='.$documento['codigo'];
      echo $sql.'<br/>';
      consultaBD($sql,true);
    }
  }*/
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">

      <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de planificaciones pendientes:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-flag"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Pendientes de aprobar<?php echo $texto;?></div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de planificaciones</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				
                <?php 
                    if($_SESSION['tipoUsuario']!='FACTURACION'){
                        echo '
                        <!--a href="'.$_CONFIG['raiz'].'planificacion-pendiente/gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva planificación</span> </a-->
                        '.$boton.'
                        <a href="javascript:void" id="eliminar" class="shortcut noAjax"><i class="shortcut-icon icon-times-circle"></i><span class="shortcut-label">Eliminar</span> </a>
                        <a href="eliminadas.php" class="shortcut"><i class="shortcut-icon icon-trash-o"></i><span class="shortcut-label">Eliminadas</span> </a>';
                    }
                    else{
                        echo $boton;
                    }
                ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Planificaciones pendientes <?php echo $texto;?></h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable tablaTextoPeque">
              <thead>
                <tr>
                  <th> Nº de contrato </th>
                  <th> Tipo </th>
                  <th> Cliente </th>
                  <th> Visitada </th>
                  <th> Plan prevención</th>
                  <th> Evaluación de riesgo \<br/> Planificación preventiva </th>
                  <th> Información de los trabajadores \<br/>Certificados de formación</th>
                  <th> Memoria </th>
                  <th> Vigilancia de la salud </th>
                  <th> Horas previstas</th>
                  <th> Importe</th>
                  <th></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeFormularioPRL($where);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		    </div>
       
        <!--div class="span12">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-calendar"></i>
               <h3>Agenda de tareas</h3>
            </div>
            <div class="widget-content">
              <div id='calendario'></div>
            </div>
          </div>
        </div-->


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/full-calendar/jquery-ui.custom.min.js" type="text/javascript" ></script><!-- Habilita el drag y el resize -->
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/full-calendar/fullcalendar.js" type="text/javascript"></script>

<!-- contenido --></div>


<!--script type="text/javascript">
$(document).ready(function() {

  var calendario = $('#calendario').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    defaultView: 'agendaWeek',
    selectable:false,
    selectHelper: false,
    editable:false,
    select: function(start, end, allDay, event, resourceId) {
      abreVentana();
      $('#registrar').click(function(){     
          var tarea=$('#tarea').val();
          var usuario=$('select[name=codigoUsuario]').val();
          var codigoIncidencia=$('select[name=codigoIncidencia]').val();
          var prioridad=$('select[name=prioridad]').val();
          var observaciones=$('#observaciones').val();

          cierraVentana();//Después de obtener los valores de los campos porque está función los resetea

          var fechaInicio = $.fullCalendar.formatDate(start, 'dd/MM/yyyy');
          var fechaFin = $.fullCalendar.formatDate(end, 'dd/MM/yyyy');
          var horaInicio = $.fullCalendar.formatDate(start, 'HH:mm');
          var horaFin = $.fullCalendar.formatDate(end, 'HH:mm');
          
          

          //Creación-renderización del evento
          var creacion=$.post("gestionEventos.php", {tarea: tarea, codigoUsuario: usuario, prioridad: prioridad, observaciones: observaciones, fechaInicio:fechaInicio, fechaFin:fechaFin, horaInicio: horaInicio, horaFin: horaFin, todoDia:allDay, estado:'PENDIENTE', tipo:'creacion', codigoCliente:'NULL', codigoIncidencia:codigoIncidencia, fechaRecordatorio:'0000-00-00'});
          creacion.done(function(datos){
            datos=datos.split('-');//Formato de valor devuelto: codigoTarea-colorTarea

            calendario.fullCalendar('renderEvent',
            {
                id: datos[0],
                title: tarea,
                start: start,
                end: end,
                allDay: allDay,
                backgroundColor: datos[1],
                borderColor: datos[1]
              },
              true
            );
            calendario.fullCalendar('rerenderEvents');//Re-renderización de los eventos (necesario para que coja los cambios).
          });
          //Fin creación-renderización
          

        });
      calendario.fullCalendar('unselect');
    },

    eventResize: function( event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view ) {//Actualización del evento cuando se modifica tu tamaño (duración)
      var fechaInicio = $.fullCalendar.formatDate(event.start, 'dd/MM/yyyy');
      var fechaFin = $.fullCalendar.formatDate(event.end, 'dd/MM/yyyy');
      var horaInicio = $.fullCalendar.formatDate(event.start, 'HH:mm');
      var horaFin = $.fullCalendar.formatDate(event.end, 'HH:mm');

      var creacion=$.post("gestionEventos.php", {codigo: event._id, fechaInicio:fechaInicio, fechaFin:fechaFin, horaIncio: horaInicio, horaFin: horaFin, todoDia:event.allDay, tipo:'actualizacion'});
      creacion.done(function(datos){
        calendario.fullCalendar('rerenderEvents');//Re-renderización de los eventos (necesario para que coja los cambios).
      });
    },
    eventDrop: function (event, dayDelta, minuteDelta) {//Actualización del evento cuando se mueve de hora
      var fechaInicio = $.fullCalendar.formatDate(event.start, 'dd/MM/yyyy');
      var fechaFin = $.fullCalendar.formatDate(event.end, 'dd/MM/yyyy');
      var horaInicio = $.fullCalendar.formatDate(event.start, 'HH:mm');
      var horaFin = $.fullCalendar.formatDate(event.end, 'HH:mm');
      
      var creacion=$.post("gestionEventos.php", {codigo: event._id, fechaInicio:fechaInicio, fechaFin:fechaFin, horaIncio: horaInicio, horaFin: horaFin, todoDia:event.allDay, tipo:'actualizacion'});
      creacion.done(function(datos){
        calendario.fullCalendar('rerenderEvents');//Re-renderización de los eventos (necesario para que coja los cambios).
      });
    },
    eventClick:function(evento){
      if(evento.codigoGestion=='NO'){
        abreVentanaOpciones(evento);
      }
      else{
        window.location.href='../gestion-de-riesgos/gestion.php?codigo='+evento.codigoGestion; 
      }
    },
    firstDay:1,//Añadido por mi
    titleFormat: {
      month: 'MMMM yyyy',
      week: "d MMM [ yyyy]{ '&#8212;' d [ MMM] yyyy}",
      day: 'dddd, d MMM, yyyy'
    },
    columnFormat: {
      month: 'ddd',
      week: 'ddd d/M',
      day: 'dddd d/M'
    },
    axisFormat: 'H:mm',
    minTime:7,
    maxTime:23,
    firstHour:7,
    weekends:true,
    allDaySlot:true,
    hoursSlot:false,
    slotMinutes:30,
    timeFormat: '-',//Fin añadido por mi
    events: [
      <?php
        //generaEventosAgenda();
      ?>
    ]
  });
});
</script-->

<?php include_once('../pie.php'); ?>