<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión Contratos VS

function imprimeContratosVS(){
	global $_CONFIG;
	$tiposContrato=array('1'=>'VS + PT','2'=>'VS');

	conexionBD();
	$consulta=consultaBD("SELECT contratos.codigo, EMPID, EMPNOMBRE, EMPMARCA, opcion, ofertas.numEmpleados
						  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
						  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
						  WHERE contratos.eliminado='NO' AND contratos.enVigor='SI' AND opcion IN(1,2)
						  GROUP BY contratos.codigo;");

	while($datos=mysql_fetch_assoc($consulta)){
		$contratos=obtieneReconocimientosContratos($datos['codigo']);

		echo "
			<tr>
				<td>".$datos['EMPID']."</td>
				<td>".$datos['EMPNOMBRE']." (".$datos['EMPMARCA'].")</td>
				<td>".$tiposContrato[$datos['opcion']]."</td>
				<td>".$datos['numEmpleados']."</td>
				<td>".$contratos['totales']."</td>
				<td>".$contratos['negativos']."</td>
			</tr>";
	}
	cierraBD();
}

function obtieneReconocimientosContratos($codigoContrato){
	$res=array('totales'=>0,'negativos'=>0);

	$consulta=consultaBD("SELECT tipoReconocimiento FROM reconocimientos_medicos WHERE codigoContrato='$codigoContrato' AND eliminado='NO';");
	while($datos=mysql_fetch_assoc($consulta)){

		if($datos['tipoReconocimiento']=='Se niega'){
			$res['negativos']++;
		}
		else{
			$res['totales']++;
		}
		
	}

	return $res;
}


//Fin parte de gestión Contratos VS