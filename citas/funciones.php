<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agenda de agenda de eventos

function campoFechaCalendario($codigoCentro){
	echo "<div id='fechaCalendario'>
			<span>Ir a: </span>";
			campoTextoSolo('campoFechaCalendario',fecha(),'input-small datepicker hasDatepicker');
	echo "</div>
		 <div id='filtroCentroCalendario'>
			<span>Centro: </span>
			<form action='?' method='post' id='formularioCentro'>";
				campoSelectConsulta('codigoCentro','',"SELECT codigo, nombre AS texto FROM centros_medicos WHERE eliminado='NO' ORDER BY nombre",$codigoCentro,'selectpicker span3 show-tick',"data-live-search='true'",'',2);
		echo "
			</form>
		</div>";
}

function obtieneCodigoCentro(){
	if(isset($_POST['codigoCentro'])){
		$res=$_POST['codigoCentro'];
	}
	else{
		$consulta=consultaBD("SELECT codigo FROM centros_medicos ORDER BY nombre LIMIT 0,1",true,true);
		$res=$consulta['codigo'];
	}

	return $res;
}

function obtieneSalasCalendarioCitas($codigoCentro){
	$salas='resources:[';

	$consulta=consultaBD("SELECT codigo, nombre FROM salas_centros_medicos WHERE codigoCentroMedico='$codigoCentro' ORDER BY nombre;",true);
	while($datos=mysql_fetch_assoc($consulta)){
		$salas.="{ name: '".$datos['nombre']."', id: '".$datos['codigo']."' }, ";
	}
	quitaUltimaComa($salas);
    
    $salas.='],';

    echo $salas;
}

function cargaEventosCalendario(){
	$colores=array(
		''				=>	'#000',
		'TERMINADA'		=>	'#000',//Sin uso, por retrocompatibilidad
		'SINCONFIRMAR'	=>	'#E49F19',
		'CONFIRMADA'	=>	'#18B807',
		'ENCONSULTA'	=>	'#2CAFF2',
		'MARCHADO'		=>	'#9B9797',
		'NOACUDE'		=>	'#F03833',
		'ANULACITA'		=>	'#B06EFE'
	);
	$estados=array(
		''				=>	'-',
		'TERMINADA'		=>	'-',//Sin uso, por retrocompatibilidad
		'SINCONFIRMAR'	=>	'Pendiente de confirmar',
		'CONFIRMADA'	=>	'Confirmada',	
		'ENCONSULTA'	=>	'Paciente en consulta',
		'MARCHADO'		=>	'Paciente se ha marchado',
		'NOACUDE'		=>	'No acude a cita',
		'ANULACITA'		=>	'Anula la cita'
	);


	$inicio=$_GET['start'];
	$fin=$_GET['end'];

	$tareas="";

	$ejercicio=obtieneEjercicioParaWhere();
	$inicioVista=formateaFechaVistaCalendario($inicio);
	$finVista=formateaFechaVistaCalendario($fin,true);

	//La subconsulta en el último LEFT JOIN de esta consulta sirve para que, en caso de que se haya mandado más de un email de notificación para la misma cita, no aparezcan N registros de la misma cita
	$consulta=consultaBD("SELECT citas.codigo, citas.fechaInicio, citas.fechaFin, citas.horaInicio, citas.horaFin, citas.estado, clientes.EMPNOMBRE AS cliente, 
						  CONCAT(empleados.nombre,' ',empleados.apellidos) AS empleado
						  FROM citas LEFT JOIN empleados ON citas.codigoEmpleado=empleados.codigo
						  LEFT JOIN clientes ON empleados.codigoCliente=clientes.codigo 
						  INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
						  INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
						  WHERE (citas.fechaInicio>='$inicioVista' OR citas.fechaInicio<='$finVista' OR citas.fechaFin>='$inicioVista' OR citas.fechaFin<='$finVista') AND contratos.eliminado='NO' AND contratos.enVigor='SI' AND ofertas.opcion IN(1,2)
						  GROUP BY citas.codigo",true);
	
	while($datos=mysql_fetch_assoc($consulta)){
		$fechaInicio=explode('-',$datos['fechaInicio']);
		$fechaFin=explode('-',$datos['fechaFin']);
		
		$horaInicio=explode(':',$datos['horaInicio']);
		$horaFin=explode(':',$datos['horaFin']);

		$tareas.='
			{
				"id": "'.$datos['codigo'].'",
	    		"title": "'.$datos['empleado'].' - '.$estados[$datos['estado']].'",
			    "start": "'.$fechaInicio[0]."-".$fechaInicio[1]."-".$fechaInicio[2]." ".$horaInicio[0].":".$horaInicio[1].':00",
		    	"end": "'.$fechaFin[0]."-".$fechaFin[1]."-".$fechaFin[2]." ".$horaFin[0].":".$horaFin[1].':00",
		    	"allDay": false,
		    	"tarea": "'.$datos['empleado'].'",
		    	"descripcion": "<ul><li>Empresa: '.$datos['cliente'].'</li><li>Empleado: '.$datos['empleado'].'</li><li>Estado: '.$estados[$datos['estado']].'</ul>",
		    	"backgroundColor": "'.$colores[$datos['estado']].'",
		    	"borderColor": "'.$colores[$datos['estado']].'"
	    	},';
  	}

  	if($tareas!=''){
  		$tareas=substr_replace($tareas, '', strlen($tareas)-1, strlen($tareas));//Para quitar última coma
  	}

  	echo '['.$tareas.']';
}

function formateaFechaVistaCalendario($timestamp,$fechaFin=false){
	$fecha=date('Y-m-d',$timestamp);
	
	if($fechaFin){//Porque la fecha de fin transmitida por el calendario es 1 día más que el final
		$array=explode('-',$fecha);

		$array[2]--;
		$fecha=$array[0].'-'.$array[1].'-'.$array[2];
	}

	return $fecha;
}


function actualizaEventoCalendario(){
	echo actualizaDatos('citas');
}

function creaEventoCalendario(){
	echo insertaDatos('citas');
}

function gestionTarea(){
	//if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='MEDICO'){
		campoFecha('fechaInicio','Fecha inicio');
		campoTexto('horaInicio','Hora inicio',false,'input-mini pagination-right');
		campoFecha('fechaFin','Fecha fin');
		campoTexto('horaFin','Hora fin',false,'input-mini pagination-right');

		$queryClientes="SELECT clientes.codigo, CONCAT(EMPNOMBRE,' (',EMPMARCA,')') AS texto 
						FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
						INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
						WHERE contratos.enVigor='SI' AND clientes.eliminado='NO'
						GROUP BY clientes.codigo
						ORDER BY EMPNOMBRE;";

		campoSelectConsulta('codigoCliente','Cliente',$queryClientes);
		campoSelect('codigoContrato','Contrato',array(''),array('NULL'));
		campoSelectConsultaPlusDoble('codigoEmpleado','Empleado',"SELECT codigo, CONCAT(nombre,' ',apellidos,' - ',dni) AS texto FROM empleados WHERE aprobado='SI' AND eliminado='NO' ORDER BY nombre, apellidos",false,'selectpicker span3 selectPlus show-tick',"data-live-search='true'",'',0,false,'btn-success',"<i class='icon-plus'></i>");

		campoSelectEstadoCita();
	//}
}

function campoSelectEstadoCita(){
	$estados=array(
		'Pendiente de confirmar'	=>	'SINCONFIRMAR',
		'Confirmada'				=>	'CONFIRMADA',
		'Paciente en consulta'		=>	'ENCONSULTA',
		'Paciente se ha marchado'	=>	'MARCHADO',
		'No acude a cita'			=>	'NOACUDE',
		'Anula la cita'				=>	'ANULACITA'
	);

	campoSelect('estado','Estado cita',array_keys($estados),array_values($estados),false,'selectpicker span3 show-tick',"");
}

function obtieneContratosClienteParaAjax(){
    $datos=arrayFormulario();

    $res=obtieneContratosCliente($datos['codigoCliente']);

    echo $res;
}

function obtieneContratosCliente($codigoCliente){
	$res="<option value='NULL'></option>";

    if($codigoCliente!='NULL'){
        $consulta=consultaBD("SELECT contratos.codigo, EMPCP, EMPID, contratos.fechaInicio, contratos.codigoInterno
        					  FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
        					  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
        					  WHERE clientes.codigo='$codigoCliente' AND enVigor='SI' AND opcion IN('1','2') AND contratos.eliminado='NO';",true);

        while($contrato=mysql_fetch_assoc($consulta)){
        	$referencia=formateaReferenciaContrato($contrato,$contrato);

            $res.="<option value='".$contrato['codigo']."'>".$referencia."</option>";
        }
    }

    return $res;
}


function consultaDatosTarea(){
	$codigoCita=$_POST['codigo'];
	$datos=datosRegistro('citas',$codigoCita);
	
	if($datos['codigoEmpleado']!=NULL){
		$datos['botonRM']=obtieneBotonRmCita($datos['codigoEmpleado'],$datos['fechaInicio'],$datos['codigoContrato']);
	}

	$datos['fechaInicio']=formateaFechaWeb($datos['fechaInicio']);
	$datos['fechaFin']=formateaFechaWeb($datos['fechaFin']);
	$datos['horaInicio']=formateaHoraWeb($datos['horaInicio']);
	$datos['horaFin']=formateaHoraWeb($datos['horaFin']);
	obtieneEmpleadosConsultaTarea($datos);
	$datos['contratos']=obtieneContratosCliente($datos['codigoCliente']);

	echo json_encode($datos);
}

function obtieneBotonRmCita($codigoEmpleado,$fechaInicio,$codigoContrato){
	$res="<a href='../reconocimientos-medicos/gestion.php?codigoEmpleadoCita=".$codigoEmpleado."&codigoContratoCita=".$codigoContrato."&fechaInicio=".$fechaInicio."' class='btn btn-warning' id='creaRM'><i class='icon-stethoscope'></i> Nuevo reconocimiento</a>";

	$datos=consultaBD("SELECT codigo FROM reconocimientos_medicos WHERE codigoEmpleado='$codigoEmpleado' AND fecha='$fechaInicio' AND eliminado='NO';",true,true);
	if($datos){
		$res="<a href='../reconocimientos-medicos/gestion.php?codigo=".$datos['codigo']."' class='btn btn-warning' id='creaRM'><i class='icon-stethoscope'></i> Ver reconocimiento</a>";
	}

	return $res;
}

function obtieneEmpleadosConsultaTarea(&$datos){
	$res="<option value='NULL'></option>";

    conexionBD();

    $empleado=consultaBD("SELECT codigoCliente FROM empleados WHERE codigo='".$datos['codigoEmpleado']."';",false,true);

    $datos['codigoCliente']=$empleado['codigoCliente'];

    $consulta=consultaBD("SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM empleados WHERE aprobado='SI' AND eliminado='NO' AND codigoCliente='".$empleado['codigoCliente']."' ORDER BY nombre, apellidos;");
    while($empleado=mysql_fetch_assoc($consulta)){
        $res.="<option value='".$empleado['codigo']."'>".$empleado['texto']."</option>";
    }

 	$datos['empleados']=$res;
}


function obtieneOptionClienteTarea($codigoCliente){
	$res="<option value='NULL'></option>";

	if($codigoCliente!=NULL){
		$consulta=consultaBD("SELECT codigo, razonSocial AS texto FROM clientes WHERE codigo=$codigoCliente",true,true);
		$res.="<option value='".$consulta['codigo']."'>".$consulta['texto']."</option>";
	}

	return $res;
}

function eliminarTarea(){
	$codigoTarea=$_POST['codigo'];

	echo consultaBD("DELETE FROM citas WHERE codigo=$codigoTarea",true);
}

function obtieneClienteDeEmpleado(){
	$datos=arrayFormulario();
	$empleado=datosRegistro('empleados',$_POST['codigo']);
	echo $empleado['codigoCliente'];
}


function enviaMensajeCita(){
	$datos=arrayFormulario();
	extract($datos);

	$datos=consultaBD("SELECT empleados.nombre, empleados.apellidos, empleados.email AS emailEmpleado, empleados.dni, empleados.fechaNacimiento, empleados.puestoManual,
					   IF(reconocimientos_medicos.codigo IS NULL,'Inicial','Periódica') AS tipoReconocimiento, clientes.EMPNOMBRE AS empresa, clientes.EMPEMAILPRINC AS emailEmpresa, 
					   clientes.EMPDIR as direccion, clientes.EMPCP AS cp
					   
					   FROM empleados LEFT JOIN clientes ON empleados.codigoCliente=clientes.codigo
					   LEFT JOIN reconocimientos_medicos ON empleados.codigo=reconocimientos_medicos.codigoEmpleado

					   WHERE empleados.codigo='$codigoEmpleado';",true,true);


    $mensaje="
    <div style='width:100%;text-align:right'>
    	".$datos['empresa']."<br />
    	".$datos['direccion']."<br />
    	".$datos['cp']."<br />
    </div>

    Sevilla, a ".fecha().".

    Muy Sr./a Nuestro/a:<br />
    <br />
    Le remitimos su cita para Reconocimiento Médico:<br />
    <br />
   
   	<table style='width:100%;border-collapse: collapse;border:1px solid #000;'>
   		<thead>
   			<tr>
   				<th style='background-color:#83A3CD;border:1px solid #000;'>NOMBRE</th>
   				<th style='background-color:#83A3CD;border:1px solid #000;'>APELLIDOS</th>
   				<th style='background-color:#83A3CD;border:1px solid #000;'>DNI</th>
   				<th style='background-color:#83A3CD;border:1px solid #000;'>NACIMIENTO</th>
   				<th style='background-color:#83A3CD;border:1px solid #000;'>PUESTO DE TRABAJO</th>
   				<th style='background-color:#83A3CD;border:1px solid #000;'>TIPO</th>
   				<th style='background-color:#83A3CD;border:1px solid #000;'>FECHA</th>
   				<th style='background-color:#83A3CD;border:1px solid #000;'>HORA</th>
   			</tr>
   		</thead>
   		<tbody>
   			<tr>
   				<td style='border:1px solid #000;'>".$datos['nombre']."</td>
   				<td style='border:1px solid #000;'>".$datos['apellidos']."</td>
   				<td style='border:1px solid #000;'>".$datos['dni']."</td>
   				<td style='border:1px solid #000;'>".formateaFechaWeb($datos['fechaNacimiento'])."</td>
   				<td style='border:1px solid #000;'>".$datos['puestoManual']."</td>
   				<td style='border:1px solid #000;'>".$datos['tipoReconocimiento']."</td>
   				<td style='border:1px solid #000;'>".formateaFechaWeb($fechaInicio)."</td>
   				<td style='border:1px solid #000;'>".$horaInicio."</td>
   			</tr>
   		</tbody>
   	</table>	

    <br />
    <br />
    
    Lugar: ANESCO Plaza de la Magdalena nº7, entrada por calle Murillo nº1,  41001 Sevilla - <a href='https://goo.gl/maps/zVfLriHYM3p'>https://goo.gl/maps/zVfLriHYM3p</a><br />
	Los datos son los comunicados por la empresa; si alguno de ellos no fuese correcto, le rogamos nos lo comunique a la mayor brevedad posible a <a href='mailto:citas@anescoprl.es'>citas@anescoprl.es</a>.
    <br />
    <br />

    <div style='background-color:#83A3CD;border:1px solid #000;width:100%;padding:5px 0px;text-align:center'>
    	Recomendaciones para el Trabajador, previas a la realización del Examen de Salud
    </div>
    <br />
	<ul>
		<li>No olvide traer sus gafas correctoras o lentillas para poderle realizar correctamente el control visión.</li>
		<li>Traiga el carnet de vacunación, si dispone de este.</li>
		<li>Debe tomarse la medicación que toma de forma habitual.</li>
		<li>En el caso de que su examen de salud requiera de analítica de sangre (es su caso),  es necesario que acuda a nuestro centro en ayunas. </li>
		<li>Si dicho examen se realiza a última hora de la mañana podrá tomar un desayuno ligero 4 horas antes, es decir deben presentarse por lo menos con 4 horas de ayuno. Si la citación es por la tarde, tome igualmente alimentos ligeros y pobres en grasas respetando el margen de 4 horas. </li>
		<li>Puede beber agua. No tome alcohol.</li>
		<li>En caso de tener alguna enfermedad o incapacidad traiga copia de informe médico.</li>
	</ul>
	<br /><br />
    <img src='https://crmparapymes.com.es/anesco2/img/logo.png' /><br />
    <div style='color:#075581;font-weight:bold'>
        <i>ANESCO SERVICIO DE PREVENCIÓN</i><br />
        Tlf .954.10.92.93<br />
        info@anescoprl.es · www.anescoprl.es<br />
        C/ Murillo 1, 2ª P. 41001 - Sevilla.
    </div>";

   $res=enviaEmail($datos['emailEmpleado'],'Recordatorio de cita - ANESCO',$mensaje);
}


//Fin parte de agenda de eventos