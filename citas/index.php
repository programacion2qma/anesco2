<?php
  $seccionActiva=3;
  include_once('../cabecera.php');

  marcaReconocimientosUsuarioComoEditables();
  $codigoCentro=obtieneCodigoCentro();
?> 

<div class="main sinMargenAb" id="contenido">
  <div class="main-inner">
    <div class="container">
      
      <a href='../centros-citas/' class='btn btn-default'><i class='icon-hospital-o'></i> Centros</a>
      <a href='../profesionales-citas/' class='btn btn-default'><i class='icon-user-md'></i> Profesionales</a>
      <div class="widget widget-nopad widget-agenda">
        <div class="widget-header"> <i class="icon-calendar"></i>
          <h3>Agenda de citas</h3>
          <?php campoFechaCalendario($codigoCentro); ?>
        </div>

        <div class="widget-content">
          <div id='calendarioCitas'></div>
        </div>

      </div>
    </div>
  </div>


  <!-- Caja para nueva tarea -->
  <div id='cajaCreacion' class='modal hide fade'> 
    <div class='modal-header'> 
      <h3> <i class="icon-calendar"></i><i class="icon-chevron-right"></i><i class="icon-list-alt"></i> &nbsp; Gestión de cita</h3> 
    </div>
    <div class='modal-body'>
      <form id="edit-profile" class="form-horizontal" method="post">
          <?php
            gestionTarea($codigoCentro);
          ?>
      </form>
    </div> 
    <div class='modal-footer'> 
      <span id='botonReconocimiento'></span>
      <button type="button" class="btn btn-propio" id="creaTarea"><i class="icon-check"></i> Registrar cita</button>  
      <button type="button" class="btn btn-success" id="creaTareaConMensaje"><i class="icon-envelope"></i> Registrar y enviar email</button>  
      <button type="button" class="btn btn-propio hide" id="actualizaTarea"><i class="icon-refresh"></i> Actualizar cita</button>  
      <button type="button" class="btn btn-success hide" id="enviaMensaje"><i class="icon-envelope"></i> Enviar email</button>  
      <button type="button" class="btn btn-danger hide" id="eliminarTarea"><i class="icon-trash"></i> Eliminar</button>  
      <button type="button" class="btn btn-default" id="cerrar"><i class="icon-remove"></i> Cancelar</button> 
    </div> 
  </div>
  <!-- Fin caja para nueva tarea -->

  <?php
    creaVentanaCreacionEmpleado();
  ?>

<script type="text/javascript" src="../../api/js/full-calendar/jquery-ui.custom.min.js"></script><!-- Habilita el drag y el resize -->
<script type="text/javascript" src="../../api/js/full-calendar/fullcalendar.js"></script>
<script type="text/javascript" src="../../api/js/jquery.cookie.js"></script>

<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script src="../js/funcionesCreacionEmpleado.js" type="text/javascript"></script>

<script type="text/javascript">

$(document).ready(function() {
  $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');
      var d=new Date(e.date.valueOf());
      $('#calendarioCitas').fullCalendar('gotoDate', d);//MODIFICACIÓN 09/06/2015: para que el calendario se vaya a la fecha seleccionada por el datepicker.
  });

  $('#cajaCreacion .hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});


  $('.selectpicker').selectpicker();

  $('input[name=todoDia]').change(function(){
    $('#cajaHora').toggleClass('hide');
  });

  $('#cerrar').click(function(){
    cierraVentana();
  });

  $('#codigoCliente').change(function(){
    oyenteCliente($(this).val());
  });

  $('#codigoEmpleado').change(function(){
    recogeCliente($(this).val());
  });

  $('#codigoCentro').change(function(){
    if($(this).val()!='NULL'){
        $('#formularioCentro').submit();
    }
  });
  
  if($.cookie('diaElegidoCalendarioCitas')!=undefined){
    var ultimaFecha=$.cookie('diaElegidoCalendarioCitas');
    ultimaFecha=ultimaFecha.split('/');
    ultimaFecha[1]--;//El formato de mes que acepta fullcalendar empieza en 0
  }
  else{
    var f=new Date();
    var ultimaFecha=[f.getDay()+1,f.getMonth(),f.getFullYear()];
  }
  

  var calendario = $('#calendarioCitas').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    defaultView: 'agendaWeek',
    year:ultimaFecha[2],
    month:ultimaFecha[1],
    date:ultimaFecha[0],
    viewRender: function(view, element){
      fechaActual=$.fullCalendar.formatDate(view.start, 'dd/MM/yyyy');
      $.cookie('diaElegidoCalendarioCitas', fechaActual);
    },
    selectable:true,
    selectHelper: true,
    editable:true,
    select: function(start, end, allDay, event, resourceId) {
      var fechaInicio = $.fullCalendar.formatDate(start, 'dd/MM/yyyy');
      var fechaFin = $.fullCalendar.formatDate(end, 'dd/MM/yyyy');
      var horaInicio = $.fullCalendar.formatDate(start, 'HH:mm');
      var horaFin = $.fullCalendar.formatDate(end, 'HH:mm');
      
      abreVentana(fechaInicio,fechaFin,horaInicio,horaFin);

      $('#creaTarea').click(function(){
          creaTarea(calendario);
      });

      $('#creaTareaConMensaje').click(function(){
          creaTarea(calendario);
          enviaMensajeCita();
      });

      calendario.fullCalendar('unselect');
    },
    eventResize: function( event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view ) {//Actualización del evento cuando se modifica tu tamaño (duración)
      var fechaInicio = $.fullCalendar.formatDate(event.start, 'dd/MM/yyyy');
      var fechaFin = $.fullCalendar.formatDate(event.end, 'dd/MM/yyyy');
      var horaI = $.fullCalendar.formatDate(event.start, 'HH:mm');
      var horaF = $.fullCalendar.formatDate(event.end, 'HH:mm');

      var creacion=$.post("../listadoAjax.php?include=citas&funcion=actualizaEventoCalendario();", {codigo: event._id, fechaInicio:fechaInicio, fechaFin:fechaFin, horaInicio: horaI, horaFin: horaF});
      creacion.done(function(datos){
        calendario.fullCalendar('refetchEvents');//Recarga los eventos del calendario, para obtener las actualizaciones
      });
      calendario.fullCalendar('unselect');
    },
    eventDrop: function (event, dayDelta, minuteDelta) {//Actualización del evento cuando se mueve de hora
      var fechaInicio = $.fullCalendar.formatDate(event.start, 'dd/MM/yyyy');
      var fechaFin = $.fullCalendar.formatDate(event.end, 'dd/MM/yyyy');
      var horaI = $.fullCalendar.formatDate(event.start, 'HH:mm');
      var horaF = $.fullCalendar.formatDate(event.end, 'HH:mm');
      
      var creacion=$.post("../listadoAjax.php?include=citas&funcion=actualizaEventoCalendario();", {codigo: event._id, fechaInicio:fechaInicio, fechaFin:fechaFin, horaInicio: horaI, horaFin: horaF});
      creacion.done(function(datos){
        calendario.fullCalendar('refetchEvents');//Recarga los eventos del calendario, para obtener las actualizaciones
      });
      calendario.fullCalendar('unselect');
    },
    eventClick:function(evento){
      abreVentanaOpciones(evento._id);
      $('#actualizaTarea').click(function(){
        
        var fechaInicio=$('#fechaInicio').val();
        var horaInicio=$('#horaInicio').val();
        var fechaFin=$('#fechaFin').val();
        var horaFin=$('#horaFin').val();
        var estado=$('#estado').val();
        var codigoContrato=$('#codigoContrato').val();
        var codigoEmpleado=$('#codigoEmpleado').val();

        //Actualización-renderización del evento
        var actualizacion=$.post("../listadoAjax.php?include=citas&funcion=actualizaEventoCalendario();",{codigo:evento._id, 'fechaInicio':fechaInicio,'horaInicio':horaInicio,'fechaFin':fechaFin,'horaFin':horaFin,'estado':estado,'codigoContrato':codigoContrato,'codigoEmpleado':codigoEmpleado});

        actualizacion.done(function(datos){
          calendario.fullCalendar('refetchEvents');//Recarga los eventos del calendario, para obtener las actualizaciones
          cierraVentana();
        });
        calendario.fullCalendar('unselect');
        //Fin actualización-renderización
      });
      $('#eliminarTarea').click(function(){
        //Eliminación-renderización del evento
        var eliminacion=$.post("../listadoAjax.php?include=citas&funcion=eliminarTarea();",{codigo:evento._id});

        eliminacion.done(function(datos){
          calendario.fullCalendar('refetchEvents');//Recarga los eventos del calendario, para obtener las actualizaciones
          cierraVentana();
        });
        calendario.fullCalendar('unselect');
        //Fin eliminación-renderización
      });

      $('#enviaMensaje').click(function(){
        enviaMensajeCita();
        $('#cajaCreacion').modal('hide');
      });
    },
    eventRender: function(event, element){//Para generar popover
        var posicion=obtienePosicionPopOver();

        $('.popover.in').remove();//Elimina los popovers creados anteriormente, evitando un bug que hace que al mover un evento el popover se quede fijo en la posición anterior, sin ocultarse.
        element.popover({//Crea el popover
            title: event.tarea,
            content: event.descripcion,
            placement:posicion
        });
        event.popOver=true;
    },
    
    eventAfterRender: function(event, element, view) {
        var ancho=$('.fc-col0').width();
        ancho-=4;
        element.css('width',ancho);
    },

    firstDay:1,
    titleFormat: {
      month: 'MMMM yyyy',
      week: "d MMM [ yyyy]{ '&#8212;' d [ MMM] yyyy}",
      day: 'dddd, d MMM, yyyy'
    },
    columnFormat: {
      month: 'ddd',
      week: 'ddd d/M',
      day: 'dddd d/M'
    },
    axisFormat: 'H:mm',
    weekends:true,
    slotMinutes:15,
    allDaySlot:false,
    minTime:'7:00:00',
    maxTime:'22:00',
    timeFormat: 'H:mm { - H:mm}',
    events: "../listadoAjax.php?include=citas&funcion=cargaEventosCalendario(<?=$codigoCentro?>);",
    height:2000
  });
});


function creaTarea(calendario){
    var codigoContrato=$('#codigoContrato').val();
    var codigoEmpleado=$('#codigoEmpleado').val();
    var fechaInicio=$('#fechaInicio').val();
    var horaInicio=$('#horaInicio').val();
    var fechaFin=$('#fechaFin').val();
    var horaFin=$('#horaFin').val();
    var estado=$('#estado').val();

    //Creación-renderización del evento
    var creacion=$.post("../listadoAjax.php?include=citas&funcion=creaEventoCalendario();",{'fechaInicio':fechaInicio,'horaInicio':horaInicio,'fechaFin':fechaFin,'horaFin':horaFin,'estado':estado,'codigoContrato':codigoContrato,'codigoEmpleado':codigoEmpleado});

    creacion.done(function(datos){
        calendario.fullCalendar('refetchEvents');//Recarga los eventos del calendario, para obtener las actualizaciones
        cierraVentana();
    });
    //Fin creación-renderización
}

function enviaMensajeCita(){
    var codigoEmpleado=$('#codigoEmpleado').val();
    var fechaInicio=$('#fechaInicio').val();
    var horaInicio=$('#horaInicio').val();
    
    //No es necesario validar el retorno:
    $.post('../listadoAjax.php?include=citas&funcion=enviaMensajeCita();',{'codigoEmpleado':codigoEmpleado,'fechaInicio':fechaInicio,'horaInicio':horaInicio});
}


function abreVentana(fechaInicio,fechaFin,horaInicio,horaFin){
  $('#fechaInicio').val(fechaInicio);
  $('#fechaFin').val(fechaFin);
  $('#horaInicio').val(horaInicio);
  $('#horaFin').val(horaFin);

  $('#creaTarea,#creaTareaConMensaje').removeClass('hide');
  $('#actualizaTarea,#enviaMensaje').addClass('hide');
  $('#creaRM').addClass('hide');
  $('#eliminarTarea').addClass('hide');

  $('#cajaCreacion').modal({'show':true,'backdrop':'static','keyboard':false});
}

function abreVentanaOpciones(codigoTarea){
  $('#creaTarea,#creaTareaConMensaje').addClass('hide');
  $('#creaRM').removeClass('hide');
  $('#actualizaTarea,#enviaMensaje').removeClass('hide');
  $('#eliminarTarea').removeClass('hide');

  $.post('../listadoAjax.php?include=citas&funcion=consultaDatosTarea();',{'codigo':codigoTarea},
  function(respuesta){
    $('#fechaInicio').val(respuesta.fechaInicio);
    $('#fechaFin').val(respuesta.fechaFin);
    $('#horaInicio').val(respuesta.horaInicio);
    $('#horaFin').val(respuesta.horaFin);
    $('#estado').val(respuesta.estado);
    $('#codigoCliente').val(respuesta.codigoCliente);

    $('#codigoEmpleado').html(respuesta.empleados);
    $('#codigoEmpleado').val(respuesta.codigoEmpleado);

    $('#codigoContrato').html(respuesta.contratos);
    $('#codigoContrato').val(respuesta.codigoContrato);
    
    $('#botonReconocimiento').html(respuesta.botonRM);

    $('#cajaCreacion .selectpicker').selectpicker('refresh');
  },'json');

  $('#cajaCreacion').modal({'show':true,'backdrop':'static','keyboard':false});
}


function cierraVentana(){
  $('#tarea').val('');
  $('#cajaCreacion').modal('hide');
  $('#creaTarea,#creaTareaConMensaje').unbind();
  $('#actualizaTarea,#enviaMensaje').unbind();
  $('#eliminarTarea').unbind();
}

//La siguiente función define dónde debe mostrarse por defecto el popover de las citas, ya que los de la primera y última columna no se mostrarían correctamente si la posición es siempre 'top'
function obtienePosicionPopOver(){
  var res='bottom';
          
  return res;
}

function oyenteCliente(codigoCliente,codigoEmpleado=0){
    var consulta=$.post('../listadoAjax.php?include=citas&funcion=obtieneEmpleadosClienteParaAjax();',{'codigoCliente':codigoCliente,'codigoEmpleado':codigoEmpleado});
    consulta.done(function(respuesta){
        $('#codigoEmpleado').html(respuesta).selectpicker('refresh');
    });

    var consulta=$.post('../listadoAjax.php?include=citas&funcion=obtieneContratosClienteParaAjax();',{'codigoCliente':codigoCliente});
    consulta.done(function(respuesta){
        $('#codigoContrato').html(respuesta).selectpicker('refresh');
    });
}

function recogeCliente(codigoEmpleado){
    if($('#codigoCliente option:selected').val()=='NULL'){
      var consulta=$.post('../listadoAjax.php?include=citas&funcion=obtieneClienteDeEmpleado();',{'codigo':codigoEmpleado});
      consulta.done(function(respuesta){
        $('#codigoCliente').val(respuesta).selectpicker('refresh');
        oyenteCliente(respuesta,codigoEmpleado);
      });
    }
}

</script>

<!-- contenido --></div>
<?php include_once('../pie.php'); ?>