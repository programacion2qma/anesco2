<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas



function imprimeRelacionContratos(){
	global $_CONFIG;


	$consulta=consultaBD("SELECT (ofertas.total-ofertas.importe_iva) AS importe, clientes.EMPNOMBRE,clientes.EMPMARCA, clientes.codigo AS codigoCliente, contratos.*, 
						  contratos.tecnico, ofertas.opcion, IFNULL(facturas.facturaCobrada,'NO') AS facturaCobrada, 
						  clientes.EMPID, formularioPRL.codigoUsuarioTecnico, contratoRevisado, contratos.fichero, facturas.codigo AS codigoFactura, suspendido, 
						  incrementoIPC, incrementoIpcAplicado, emitirPlanificacionSinPagar, clientes.EMPCP, CONCAT(serie,'/',facturas.numero) AS factura,
						  formularioPRL.codigo AS codigoPlanificacion, formularioPRL.aprobado

						  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
						  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
						  INNER JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
						  INNER JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO' AND facturas.eliminado='NO'
						  INNER JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
						  LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo

						  WHERE contratos.eliminado='NO' AND enVigor='SI' AND (contratos_en_facturas.codigo IS NULL OR facturas.eliminado='NO')

						  GROUP BY contratos.codigo;",true);


	while($datos=mysql_fetch_assoc($consulta)){

		$estado=obtieneEstadoContratoListado($datos);

		if($estado!='Pendiente cobrar'){

			$enlace='planificacion-pendiente';
			if($datos['aprobado']=='SI'){
				$enlace='planificacion';
			}

			echo "
			<tr>
				<td class='nowrap'>".$datos['EMPID']."</td>
				<td class='centro'><a href='../contratos/gestion.php?codigo=".$datos['codigo']."' class='btn btn-small btn-propio noAjax' target='_blank'><i class='icon-external-link'></i> ".formateaReferenciaContrato($datos,$datos)."</a></td>
				<td>".formateaFechaWeb($datos['fechaInicio'])."</td>
				<td>".formateaFechaWeb($datos['fechaFin'])."</td>
				<td>".$datos['EMPNOMBRE']."</td>
				<td>".$datos['EMPMARCA']."</td>
				<td class='centro'><a href='../facturas/gestion.php?codigo=".$datos['codigoFactura']."' class='btn btn-small btn-propio noAjax' target='_blank'><i class='icon-external-link'></i> ".$datos['factura']."</a></td>
				<td class='centro'><a href='../".$enlace."/gestion.php?codigo=".$datos['codigoPlanificacion']."' class='btn btn-small btn-propio noAjax' target='_blank'><i class='icon-external-link'></i> Ver</a></td>
			</tr>";
		}
	}

}