<?php
  $seccionActiva=48;
  include_once('../cabecera.php');
  
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> 
            <i class="icon-file-text"></i> <i class="icon-chevron-right"></i> <i class="icon-eur"></i> <i class="icon-chevron-right"></i> <i class="icon-check-square-o"></i>
            <h3>Relación de contratos en vigor con sus facturas y planificaciones</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Nº Cliente </th>
                  <th> Contrato </th>
                  <th> Inicio </th>
                  <th> Fin </th>
                  <th> Razón social </th>
                  <th> Nombre comercial </th>
                  <th> Factura </th>
                  <th> Planificación</th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeRelacionContratos();
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>