 <?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales
@include_once('../../api/js/firma/signature-to-image.php');
@include_once('../Mobile_Detect.php');
//Inicio de funciones específicas


//Parte de gestión de memoria

function campoSelectContrato($datos=false){
	$contratos=consultaBD('SELECT * FROM contratos WHERE eliminado="NO"',true);	
	$nombres=array('');
	$valores=array('NULL');
	while($contrato=mysql_fetch_assoc($contratos)){
		$cliente=consultaBD('SELECT clientes.* FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo='.$contrato['codigoOferta']." AND ofertas.eliminado='NO'",true,true);
		$referencia=formateaReferenciaContrato($cliente,$contrato);
		array_push($nombres, 'Nº: '.$referencia.' - '.$cliente['EMPNOMBRE']);
		array_push($valores, $contrato['codigo']);
	}
	campoSelect('codigoContrato','Seleccione un contrato para acceder a su histórico de tareas',$nombres,$valores,$datos);
}


function imprimeMemoriaContrato($codigoContrato){
	conexionBD();

	imprimeParteMemoriaContrato("SELECT codigo, tarea, 'TAREA' AS tipo, tiempoEmpleado FROM tareas WHERE codigoContrato='$codigoContrato' AND estado='REALIZADA';");

	imprimeParteMemoriaContrato("SELECT formularioPRL.codigo, nombre AS tarea, 'DOCUMENTO' AS tipo, '-' AS tiempoEmpleado FROM documentos_info INNER JOIN formularioPRL ON documentos_info.codigoFormulario=formularioPRL.codigo WHERE codigoContrato='$codigoContrato';");

	imprimeParteMemoriaContrato("SELECT formularioPRL.codigo, nombre AS tarea, 'DOCUMENTO' AS tipo, '-' AS tiempoEmpleado FROM  documentos_plan_prev INNER JOIN formularioPRL ON  documentos_plan_prev.codigoFormulario=formularioPRL.codigo WHERE codigoContrato='$codigoContrato';");

	cierraBD();
}

function imprimeParteMemoriaContrato($query){
	global $_CONFIG;
	$consulta=consultaBD($query);

	while($datos=mysql_fetch_assoc($consulta)){
		$enlace="planificacion/gestion.php?codigo=".$datos['codigo'];
		$tarea=$datos['tarea'];

		if($datos['tipo']=='TAREA'){
			$enlace="tareas/gestion.php?codigo=".$datos['codigo'];
			$tarea=obtieneNombreTarea($datos['tarea']);
		}

		echo "
		<tr>
			<td>".$tarea."</td>
			<td>".$datos['tiempoEmpleado']."</td>
			<td class='centro'><a class='btn btn-propio noAjax' href='".$_CONFIG['raiz'].$enlace."' target='_blank'><i class='icon-external-link'></i> Detalles</i></a></td>
		</tr>";
	}
}


function creaEstadisticasMemoria($codigoContrato){
	conexionBD();

	$res=consultaBD("SELECT IFNULL(COUNT(tareas.codigo),0) AS total, IFNULL(SUM(tiempoEmpleado),0) AS tiempoEmpleado FROM tareas WHERE codigoContrato='$codigoContrato';",false,true);
	$horas=$res['tiempoEmpleado']/60;
	$minutos=$res['tiempoEmpleado']%60;
	$res['tiempoEmpleado']=intval($horas).':'.$minutos;

	$consulta=consultaBD("SELECT IFNULL(COUNT(documentos_info.codigo),0) AS total FROM documentos_info INNER JOIN formularioPRL ON documentos_info.codigoFormulario=formularioPRL.codigo WHERE codigoContrato='$codigoContrato';",false,true);
	$res['total']+=$consulta['total'];

	$consulta=consultaBD("SELECT IFNULL(COUNT(documentos_plan_prev.codigo),0) AS total FROM  documentos_plan_prev INNER JOIN formularioPRL ON  documentos_plan_prev.codigoFormulario=formularioPRL.codigo WHERE codigoContrato='$codigoContrato';",false,true);
	$res['total']+=$consulta['total'];

	$consulta=consultaBD('SELECT horasPrevistas FROM formularioPRL WHERE codigoContrato='.$codigoContrato,false,true);
	$res['horasPrevistas']=$consulta['horasPrevistas']!=''?$consulta['horasPrevistas']:'0:00';

	cierraBD();

	return $res;
}


function obtieneReferenciaContrato($codigoContrato){
	$datos=consultaBD("SELECT clientes.EMPCP, clientes.EMPID, contratos.fechaInicio, contratos.codigoInterno
					   FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
					   INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
					   WHERE contratos.codigo='$codigoContrato';",true,true);

	return formateaReferenciaContrato($datos,$datos);
}

function filtroMemorias(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoSelectConsulta(0,'Cliente','SELECT codigo,EMPNOMBRE as texto FROM clientes ORDER BY EMPNOMBRE');

	cierraColumnaCampos();
	abreColumnaCampos();

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function listadoMemorias(){
	global $_CONFIG;

	$columnas=array(
		'clientes.codigo',
		'EMPNOMBRE',
		'codigoInterno',
		'tecnico',
		'horasPrevistas',
		'tiempo',
	);

	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden='';
	$orden=obtieneOrdenListado($columnas);
	//$limite=obtieneLimitesListado();
	
	$query="SELECT clientes.codigo, clientes.EMPNOMBRE, contratos.codigoInterno, formularioPRL.horasPrevistas, tiempo, contratos.codigo AS codigoContrato, clientes.codigo AS codigoCliente, 
			GROUP_CONCAT(DISTINCT CONCAT(nombre,' ',apellidos) ORDER BY nombre SEPARATOR '<br/>') AS tecnico
			FROM tareas_parte_diario INNER JOIN parte_diario ON tareas_parte_diario.codigoParteDiario=parte_diario.codigo
			INNER JOIN contratos ON tareas_parte_diario.codigoContrato=contratos.codigo
			INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
			INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
			LEFT JOIN formularioPRL ON formularioPRL.codigoContrato=contratos.codigo 
			LEFT JOIN usuarios ON parte_diario.codigoUsuario=usuarios.codigo
			GROUP BY contratos.codigo 
			$having";

	conexionBD();
	$consulta=consultaBD($query." $orden");
	$consultaPaginacion=consultaBD($query);
	cierraBD();
	$iconosEstado=array('SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$listado='';
		$contrato=datosRegistro('contratos',$datos['codigoContrato']);
		$cliente=datosRegistro('clientes',$datos['codigoCliente']);
		$tiempo='00:00';
		$tareas=consultaBD('SELECT tareas_parte_diario.codigo, tiempo, tarea, CONCAT(nombre," ",apellidos) AS tecnico FROM tareas_parte_diario INNER JOIN tareas ON tareas_parte_diario.codigoTarea=tareas.codigo LEFT JOIN usuarios ON tareas.codigoUsuario=usuarios.codigo WHERE tareas_parte_diario.codigoContrato='.$contrato['codigo'],true);
		while($tarea=mysql_fetch_assoc($tareas)){
			$tiempo=sumaTiempo($tiempo,$tarea['tiempo']);
			$listado.='- '.$tarea['codigo'].' - '.$tarea['tiempo'].'<br/>';
		}
		$horasPrevistas=formateaHoraPrevista($datos['horasPrevistas']);
		$comp=compararTiempos($horasPrevistas,$tiempo);
		$fila=array(
			'<div style="text-align:center;">'.$iconosEstado[$comp['cumplido']].'</div>',
			$datos['EMPNOMBRE'],
			formateaReferenciaContrato($cliente,$contrato),
			$datos['tecnico'],
			'<div style="text-align:center;">'.$horasPrevistas.'</div>',
			'<div class="'.$comp['clase'].'">'.$tiempo.' <a href="#" class="enlacePopOverMemoria"><i class="icon-info-circle" style="font-size:14px;color:#FFF;"></i></a> <div class="hide">'.$listado.'</div></div>',
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	if(isset($_GET['iSortCol_0'])){//Si se está ordenando (indistintamente del sentido) por la primera columna...
		$res['aaData']=ordenaListadoMemoriasPorTiempoCumplido($res['aaData'],$_GET['sSortDir_0']);
	}

	if(isset($_GET['iDisplayStart']) && $_GET['iDisplayLength']!='-1') {//Si el listado está paginado...
		$res['aaData']=paginaListado($res['aaData'],$_GET['iDisplayStart'],$_GET['iDisplayLength']);
	}

	echo json_encode($res);
}

function ordenaListadoMemoriasPorTiempoCumplido($datos,$sentido){
	$res=array();

	foreach($datos as $fila){
		if(substr_count($fila[0],'icon-danger')>0){//Si el tiempo NO está cumplido
			array_unshift($res,$fila);//Se añade la fila por el PRINCIPIO del nuevo array de filas
		}
		else{
			array_push($res,$fila);//Si el tiempo está cumplido, se añade por el final
		}
	}

	if($sentido=='desc'){
		$res=array_reverse($res);
	}

	return $res;
}

function paginaListado($datos,$inicio,$fin){
	return array_slice($datos,$inicio,$fin);
}

//Fin parte de gestión de memoria