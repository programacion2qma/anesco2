<?php
  $seccionActiva=4;
  include_once('../cabecera.php');
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">


        <div class="span12">        
          <div class="widget widget-table action-table">
              <div class="widget-header"> <i class="icon-list"></i>
                  <h3>Memoria de tiempos registrados</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
                  </div>
              </div>
              <!-- /widget-header -->
              <div class="widget-content">
                  <?php
                      filtroMemorias();
                  ?>
              <table class="table table-striped table-bordered datatable" id="tablaMemorias">
                <thead>
                  <tr>
                    <th> Tiempo cumplido</th>
                    <th> Cliente</th>
                    <th> Contrato</th>
                    <th> Técnicos </th>
                    <th class="sumatorio"> Tiempo previsto </th>
                    <th class="sumatorio"> Tiempo empleado </th>
                  </tr>
                </thead>
                <tbody>              
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
        


        </div>
		

	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-select.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.selectpicker').selectpicker();
    listadoTabla('#tablaMemorias','../listadoAjax.php?include=memoria&funcion=listadoMemorias();');
    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaMemorias');
});
</script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>