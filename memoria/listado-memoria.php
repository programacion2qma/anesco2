<?php
  $seccionActiva=4;
  include_once('../cabecera.php');
  
  $codigoContrato=$_POST['codigoContrato'];
  $nombreContrato=obtieneReferenciaContrato($codigoContrato);
  $estadisticas=creaEstadisticasMemoria($codigoContrato);
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas para el contrato <?=$nombreContrato?>:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-calendar"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Tarea/s registrada/s</div>
                     <div class="stat"> <i class="icon-clock-o"></i> <span class="value"><?php echo $estadisticas['horasPrevistas']?></span><br />Hora/s previstas/s</div>
                     <div class="stat"> <i class="icon-clock-o"></i> <span class="value"><?php echo $estadisticas['tiempoEmpleado']?></span><br />Hora/s empleada/s</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de memoria</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="index.php" class="shortcut noAjax"><i class="shortcut-icon icon-exchange"></i><span class="shortcut-label">Cambiar contrato</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Tareas y documentos registrados para el contrato <?=$nombreContrato?></h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Tarea/documento </th>
                  <th> Tiempo empleado (minutos) </th>
                  <th class="centro"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeMemoriaContrato($codigoContrato);
        				?>
            
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  
      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>