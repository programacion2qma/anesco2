<?php
  $seccionActiva=0;
  include_once('cabecera.php');

  $meses=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
  compruebaVencimientosRemesas();
?>

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget" id="target-2">
            <div class="widget-header"> <i class="icon-th"></i>
              <h3>Opciones</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="javascript:void(0);" id='guia' class="shortcut"><i class="shortcut-icon icon-book"></i><span class="shortcut-label">Guía rápida</span> </a>
                <a href="cerrarSesion.php" class="shortcut"><i class="shortcut-icon icon-power-off"></i><span class="shortcut-label">Cerrar sesión</span> </a>
              </div>
              <!-- /shortcuts -->
            </div>
            <!-- /widget-content -->
          </div>
        </div>

         <div class="span12">
          <div class="widget widget-nopad" id="target-1">
            <div class="widget-header"> <i class="icon-home"></i>
              <h3>Inicio</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Bienvenido/a <?php echo $_SESSION['usuario']; ?>. A continuación se muestra el estado del sistema:</h6>
                      <?php

                      if($_SESSION['tipoUsuario']=='ADMIN'){

                        abrePanelEstadisticas('Clientes',false);
                            creaEstadisticaInicio('icon-question-circle','Clientes potenciales','clientes','codigo','activo="NO"','posibles-clientes/');
                            creaEstadisticaInicio('icon-users','Clientes activos','clientes','codigo','activo="SI" AND eliminado="NO"','clientes/');
                            creaEstadisticaInicio('icon-arrow-down','Clientes de baja','clientes','codigo','activo="SI" AND clientes.eliminado="NO" AND clientes.codigo NOT IN (SELECT clientes.codigo FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE activo="SI" AND contratos.enVigor="SI" AND clientes.eliminado="NO")','clientes/index.php?baja');
                            creaEstadisticaInicio('icon-trash-o','Clientes eliminados','clientes','codigo','eliminado="SI"','clientes/eliminadas.php');

                        abrePanelEstadisticas('Ofertas de '.$meses[date('m')]);
                            creaEstadisticaInicio('icon-tags','Registradas','ofertas','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'"','ofertas/');
                            creaEstadisticaInicio('icon-check-circle','Aceptadas','ofertas','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'" AND aceptado="SI" AND ofertas.rechazada="NO" AND eliminado="NO"','ofertas/ofertasAceptadas.php');
                            creaEstadisticaInicio('icon-ban','Rechazadas','ofertas','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'" AND ofertas.rechazada="SI" AND eliminado="NO"','ofertas/ofertasRechazadas.php');
                            creaEstadisticaInicio('icon-trash-o','Eliminadas','ofertas','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'" AND eliminado="SI"','ofertas/eliminadas.php');


                        abrePanelEstadisticas('Facturación');
                            $estadisticasFacturas=estadisticasFacturas();
                            echo '<a href="'.$_CONFIG['raiz'].'facturas/" class="stat"><i class="icon-eur"></i> <span class="value">'.formateaNumeroWeb($estadisticasFacturas['facturacionTotal']).' €</span><br />Facturado</a>';
                            echo '<a href="'.$_CONFIG['raiz'].'facturas/" class="stat"><i class="icon-check-circle"></i> <span class="value">'.formateaNumeroWeb($estadisticasFacturas['totalCobrado']).' €</span><br />Cobrado</a>';
                            echo '<a href="'.$_CONFIG['raiz'].'facturas/" class="stat"><i class="icon-flag"></i> <span class="value">'.formateaNumeroWeb($estadisticasFacturas['totalPendienteCobro']).' €</span><br />Pendiente cobro</a>';
                            echo '<a href="'.$_CONFIG['raiz'].'facturas/deBaja.php" class="stat"><i class="icon-arrow-down"></i> <span class="value">'.formateaNumeroWeb($estadisticasFacturas['totalDeBaja']).' €</span><br />De baja</a>';



                        abrePanelEstadisticas('Contratos que comienzan en '.$meses[date('m')]);
                            creaEstadisticaInicio('icon-file-text-o','Registrados','contratos','codigo','MONTH(fechaInicio)="'.date('m').'" AND YEAR(fechaInicio)="'.date('Y').'"','contratos/');
                            creaEstadisticaInicio('icon-search','Revisados','contratos','codigo','MONTH(fechaInicio)="'.date('m').'" AND YEAR(fechaInicio)="'.date('Y').'" AND contratoRevisado="SI" AND contratos.eliminado="NO"','contratos/');
                            creaEstadisticaInicio('icon-eur','Pendientes de cobrar','contratos LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!="ABONO"','contratos.codigo','MONTH(fechaInicio)="'.date('m').'" AND YEAR(fechaInicio)="'.date('Y').'" AND contratos.eliminado="NO" AND enVigor="SI" AND (facturas.facturaCobrada IS NULL OR facturas.facturaCobrada="NO")','contratos/index.php?pendiente');
                            creaEstadisticaInicio('icon-trash-o','Eliminados','contratos','codigo','MONTH(fechaInicio)="'.date('m').'" AND YEAR(fechaInicio)="'.date('Y').'" AND eliminado="SI"','contratos/eliminados.php');


                        abrePanelEstadisticas('Renovaciones de contratos');
                            $estadisticasRenovaciones=estadisticasInicialesContratos();
                            echo '<a href="'.$_CONFIG['raiz'].'contratos/" class="stat"><i class="icon-calendar-check-o"></i> <span class="value">'.$estadisticasRenovaciones['renovaciones'].'</span><br />Realizadas</a>';
                            echo '<a href="'.$_CONFIG['raiz'].'contratos/renovaciones.php" class="stat"><i class="icon-exclamation"></i> <span class="value">'.$estadisticasRenovaciones['aRenovar'].'</span><br />Pendientes</a>';
                            echo '<a href="'.$_CONFIG['raiz'].'contratos/renovaciones-fuera-plazo.php" class="stat"><i class="icon-calendar-times-o"></i> <span class="value">'.$estadisticasRenovaciones['fueraPlazo'].'</span><br />Vencidas</a>';

                        abrePanelEstadisticas('Planificaciones');
                            creaEstadisticaInicio('icon-check-square-o','Registradas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'"','planificacion/');
                            creaEstadisticaInicio('icon-check-circle','Aprobadas en '.$meses[date('m')],'formularioPRL','codigo','aprobado="SI" AND MONTH(fechaAprobada)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'"','planificacion/');
                            creaEstadisticaInicio('icon-check-circle','Registradas y aprobadas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'" AND aprobado="SI" AND MONTH(fechaAprobada)="'.date('m').'" AND YEAR(fechaAprobada)="'.date('Y').'"','planificacion/');
                            creaEstadisticaInicio('icon-trash-o','Eliminadas','formularioPRL','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'" AND eliminado="SI"','planificacion/eliminadas.php');

                        abrePanelEstadisticas('Gestiones de planificaciones');
                            creaEstadisticaInicio('icon-building','Visitas previstas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaVisita)="'.date('m').'" AND YEAR(fechaVisita)="'.date('Y').'"','planificacion/');
                            creaEstadisticaInicio('icon-check','Visitas hechas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaVisitaReal)="'.date('m').'" AND YEAR(fechaVisitaReal)="'.date('Y').'"','planificacion/');
                            creaEstadisticaInicio('icon-file-text','Planes de prevenciones previstos en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaPlanPrev)="'.date('m').'" AND YEAR(fechaPlanPrev)="'.date('Y').'"','planificacion/');
                            creaEstadisticaInicio('icon-check','Planes de prevenciones hechos en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaPlanPrevReal)="'.date('m').'" AND YEAR(fechaPlanPrevReal)="'.date('Y').'"','planificacion/');
                            echo '</div>
                            <div id="big_stats" class="cf">';
                            creaEstadisticaInicio('icon-check-square-o','Evaluaciones de riesgos/planificaciones preventivas previstas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaPap)="'.date('m').'" AND YEAR(fechaPap)="'.date('Y').'"','planificacion/');
                            creaEstadisticaInicio('icon-check','Evaluaciones de riesgos/planificaciones preventivas hechas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaPapReal)="'.date('m').'" AND YEAR(fechaPapReal)="'.date('Y').'"','planificacion/');
                            creaEstadisticaInicio('icon-book','Información/Formación a los Trabajadores previstas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaInfo)="'.date('m').'" AND YEAR(fechaInfo)="'.date('Y').'"','planificacion/');
                            creaEstadisticaInicio('icon-check','Información/Formación a los Trabajadores hechas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaInfoReal)="'.date('m').'" AND YEAR(fechaInfoReal)="'.date('Y').'"','planificacion/');
                            echo '</div>
                            <div id="big_stats" class="cf">';
                            creaEstadisticaInicio('icon-list','Memorias previstas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaMemoria)="'.date('m').'" AND YEAR(fechaMemoria)="'.date('Y').'"','planificacion/');
                            creaEstadisticaInicio('icon-check','Memorias hechas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaMemoriaReal)="'.date('m').'" AND YEAR(fechaMemoriaReal)="'.date('Y').'"','planificacion/');
                            creaEstadisticaInicio('icon-stethoscope','Reconocimientos médicos previstos en '.$meses[date('m')],'formularioPRL','codigo','mesVs="'.date('m').'" AND anioVs="'.date('Y').'"','planificacion/');
                            creaEstadisticaInicio('icon-check','Reconocimientos médicos hechos en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaVsReal)="'.date('m').'" AND YEAR(fechaVsReal)="'.date('Y').'"','planificacion/');

                        abrePanelEstadisticas('Actuaciones técnicas en '.$meses[date('m')]);
                            creaEstadisticaInicio('icon-edit','Inspecciones de seguridad','visitas','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'"','visitas_seguridad/');
                            creaEstadisticaInicio('icon-support','Asistencia de inspecciones','asistencia_a_inspecciones','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'"','asistencia/');
                            creaEstadisticaInicio('icon-exclamation-triangle','Evaluaciones de riesgos','evaluacion_general','codigo','MONTH(fechaEvaluacion)="'.date('m').'" AND YEAR(fechaEvaluacion)="'.date('Y').'"','evaluacion-de-riesgos/');
                            creaEstadisticaInicio('icon-paste','Informes de riesgos','informes','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'"','generacion-de-informes/');

                        abrePanelEstadisticas('Accidentes de trabajo en '.$meses[date('m')]);
                            creaEstadisticaInicio('icon-user','Producidos en '.$meses[date('m')],'accidentes','codigo','MONTH(fechaAccidente)="'.date('m').'" AND YEAR(fechaAccidente)="'.date('Y').'"','accidentes/');
                            creaEstadisticaInicio('icon-phone','Comunicados en '.$meses[date('m')],'accidentes','codigo','MONTH(fechaComunicacion)="'.date('m').'" AND YEAR(fechaComunicacion)="'.date('Y').'"','accidentes/');
                            creaEstadisticaInicio('icon-user-secret','Investigados en '.$meses[date('m')],'accidentes','codigo','MONTH(fechaInvestigacion)="'.date('m').'" AND YEAR(fechaInvestigacion)="'.date('Y').'"','accidentes/');
                            creaEstadisticaInicio('icon-pencil-square-o','Redactados en '.$meses[date('m')],'accidentes','codigo','MONTH(fechaRecepcionInforme)="'.date('m').'" AND YEAR(fechaRecepcionInforme)="'.date('Y').'"','accidentes/');

                        abrePanelEstadisticas('Tareas en '.$meses[date('m')]);
                            creaEstadisticaInicio('icon-calendar','Registradas','tareas','codigo','MONTH(fechaInicio)="'.date('m').'" AND YEAR(fechaInicio)="'.date('Y').'"','tareas/');
                            creaEstadisticaInicio('icon-check','Realizadas','tareas','codigo','MONTH(fechaInicio)="'.date('m').'" AND YEAR(fechaInicio)="'.date('Y').'" AND estado="REALIZADA"','tareas/');
                            creaEstadisticaInicio('icon-pause','Pendientes','tareas','codigo','MONTH(fechaInicio)="'.date('m').'" AND YEAR(fechaInicio)="'.date('Y').'" AND estado="PENDIENTE"','tareas/');

                        abrePanelEstadisticas('Incidencias');
                            creaEstadisticaInicio('icon-exclamation-circle','Comunicadas en '.$meses[date('m')],'incidencias','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'"','incidencias/');
                            creaEstadisticaInicio('icon-check','Resueltas en '.$meses[date('m')],'incidencias','codigo','MONTH(fechaResolucion)="'.date('m').'" AND YEAR(fechaResolucion)="'.date('Y').'"','incidencias/');
                            creaEstadisticaInicio('icon-check','Comunicadas y resueltas en '.$meses[date('m')],'incidencias','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'" AND MONTH(fechaResolucion)="'.date('m').'" AND YEAR(fechaResolucion)="'.date('Y').'"','incidencias/');

                        abrePanelEstadisticas('Facturas en '.$meses[date('m')]);
                            creaEstadisticaInicio('icon-file-text-o','Proformas','facturas','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'" AND tipoFactura="PROFORMA"','proformas/');
                            creaEstadisticaInicio('icon-file-text','Finales','facturas','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'" AND tipoFactura="NORMAL"','facturas/');
                            creaEstadisticaInicio('icon-reply','Abonos','facturas','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'" AND tipoFactura="ABONO"','abonos/');
                            creaEstadisticaInicio('icon-trash-o','Eliminadas','facturas','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'" AND eliminado="NO"','facturas/eliminadas.php');

                        echo "</div>";
                      }
                      elseif($_SESSION['tipoUsuario']=='TECNICO'){
                        $codigoUsuarioTecnico=$_SESSION['codigoU'];

                        abrePanelEstadisticas('Planificaciones');
                            creaEstadisticaInicio('icon-check-square-o','Registradas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'" AND codigoUsuarioTecnico='.$codigoUsuarioTecnico,'planificacion/');
                            creaEstadisticaInicio('icon-check-circle','Aprobadas en '.$meses[date('m')],'formularioPRL','codigo','aprobado="SI" AND MONTH(fechaAprobada)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'" AND codigoUsuarioTecnico='.$codigoUsuarioTecnico,'planificacion/');
                            creaEstadisticaInicio('icon-check-circle','Registradas y aprobadas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'" AND aprobado="SI" AND MONTH(fechaAprobada)="'.date('m').'" AND YEAR(fechaAprobada)="'.date('Y').'" AND codigoUsuarioTecnico='.$codigoUsuarioTecnico,'planificacion/');
                            creaEstadisticaInicio('icon-trash-o','Eliminadas','formularioPRL','codigo','MONTH(fecha)="'.date('m').'" AND YEAR(fecha)="'.date('Y').'" AND eliminado="SI" AND codigoUsuarioTecnico='.$codigoUsuarioTecnico,'planificacion/eliminadas.php');

                        abrePanelEstadisticas('Gestiones de planificaciones');
                            creaEstadisticaInicio('icon-building','Visitas previstas en '.$meses[date('m')],'formularioPRL INNER JOIN planificacion_visitas ON formularioPRL.codigo=planificacion_visitas.codigoFormulario','planificacion_visitas.codigo','MONTH(fechaPrevista)="'.date('m').'" AND YEAR(fechaPrevista)="'.date('Y').'" AND codigoUsuarioTecnico='.$codigoUsuarioTecnico,'planificacion/');
                            creaEstadisticaInicio('icon-check','Visitas hechas en '.$meses[date('m')],'formularioPRL INNER JOIN planificacion_visitas ON formularioPRL.codigo=planificacion_visitas.codigoFormulario','planificacion_visitas.codigo','MONTH(fechaReal)="'.date('m').'" AND YEAR(fechaReal)="'.date('Y').'" AND codigoUsuarioTecnico='.$codigoUsuarioTecnico,'planificacion/');
                            creaEstadisticaInicio('icon-file-text','Planes de prevenciones previstos en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaPlanPrev)="'.date('m').'" AND YEAR(fechaPlanPrev)="'.date('Y').'" AND codigoUsuarioTecnico='.$codigoUsuarioTecnico,'planificacion/');
                            creaEstadisticaInicio('icon-check','Planes de prevenciones hechos en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaPlanPrevReal)="'.date('m').'" AND YEAR(fechaPlanPrevReal)="'.date('Y').'" AND codigoUsuarioTecnico='.$codigoUsuarioTecnico,'planificacion/');
                            echo '</div>
                            <div id="big_stats" class="cf">';
                            creaEstadisticaInicio('icon-check-square-o','Evaluaciones de riesgos/planificaciones preventivas previstas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaPap)="'.date('m').'" AND YEAR(fechaPap)="'.date('Y').'" AND codigoUsuarioTecnico='.$codigoUsuarioTecnico,'planificacion/');
                            creaEstadisticaInicio('icon-check','Evaluaciones de riesgos/planificaciones preventivas hechas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaPapReal)="'.date('m').'" AND YEAR(fechaPapReal)="'.date('Y').'" AND codigoUsuarioTecnico='.$codigoUsuarioTecnico,'planificacion/');
                            creaEstadisticaInicio('icon-book','Información/Formación a los Trabajadores previstas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaInfo)="'.date('m').'" AND YEAR(fechaInfo)="'.date('Y').'" AND codigoUsuarioTecnico='.$codigoUsuarioTecnico,'planificacion/');
                            creaEstadisticaInicio('icon-check','Información/Formación a los Trabajadores hechas en '.$meses[date('m')],'formularioPRL','codigo','MONTH(fechaInfoReal)="'.date('m').'" AND YEAR(fechaInfoReal)="'.date('Y').'" AND codigoUsuarioTecnico='.$codigoUsuarioTecnico,'planificacion/');
                      }
                      elseif($_SESSION['tipoUsuario']=='FACTURACION'){
                         abrePanelEstadisticas('Facturación');
                            $estadisticasFacturas=estadisticasFacturas();
                            echo '<a href="'.$_CONFIG['raiz'].'facturas/" class="stat"><i class="icon-eur"></i> <span class="value">'.formateaNumeroWeb($estadisticasFacturas['facturacionTotal']).' €</span><br />Facturado</a>';
                            echo '<a href="'.$_CONFIG['raiz'].'facturas/" class="stat"><i class="icon-check-circle"></i> <span class="value">'.formateaNumeroWeb($estadisticasFacturas['totalCobrado']).' €</span><br />Cobrado</a>';
                            echo '<a href="'.$_CONFIG['raiz'].'facturas/" class="stat"><i class="icon-flag"></i> <span class="value">'.formateaNumeroWeb($estadisticasFacturas['totalPendienteCobro']).' €</span><br />Pendiente cobro</a>';
                            echo '<a href="'.$_CONFIG['raiz'].'facturas/deBaja.php" class="stat"><i class="icon-arrow-down"></i> <span class="value">'.formateaNumeroWeb($estadisticasFacturas['totalDeBaja']).' €</span><br />De baja</a>';

                      }
                      ?>
                   
                </div> <!-- /widget-content -->
                <!-- /widget-content -->

              </div>
            </div>
          </div>

        </div>
      </div><!-- /row -->
      <?php if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='TECNICO'){?>
      <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-shopping-cart"></i>
          <h3>Clientes</h3>
        </div>
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable" id='target-3'>
            <thead>
              <tr>
                  <th> Nº Cliente </th>
                  <th> Razón Social </th>
                  <th> Nombre comercial </th>
                  <th> Ofertas </th>
                  <th> Contratos </th>
                  <th> Planificaciones </th>
                  <th> Facturas </th>
                  <th> Es cliente </th>
              </tr>
            </thead>
            <tbody>

              <?php
                imprimeClientesInicio();
              ?>

            </tbody>
          </table>
        </div>
      </div>

      <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-calendar"></i>
          <h3>Tareas para hoy y fuera de plazo</h3>
        </div>
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                  <th> Fecha </th>
                  <th> Empresa </th>
                  <th> Localidad </th>
                  <th> Teléfono </th>
                  <th> Tarea </th>
                  <th> Técnico </th>
                  <th> Prioridad </th>
                  <th> </th>
              </tr>
            </thead>
            <tbody>

              <?php
                imprimeTareasInicio();
              ?>

            </tbody>
          </table>
        </div>
      </div>
      <? 
      }
      elseif($_SESSION['tipoUsuario']=='CLIENTE'){
        $cliente=obtieneCodigoCliente();
      ?>

      <div class="row">
        <div class="span12">        
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Empleados registrados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable" id='target-3'>
                <thead>
                  <tr>
                    <th> Nº Empleado</th>
                    <th> Nombre </th>
                    <th> Apellidos </th>
                    <th id="target-3"> DNI </th>
                    <th> Sexo </th>
                    <th> Teléfono </th>
                    <th> eMail </th>
                    <th class="centro"></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    imprimeEmpleados($cliente,'NO',true);
                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

        </div>
      </div>

      <?php
      }
      ?>

    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/guidely/guidely.min.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){

  guidely.add ({
    attachTo: '#target-1'
    , anchor: 'top-left'
    , title: 'Panel de Estadísticas'
    , text: 'En el lado izquierdo de su pantalla encontrará el Panel de Estadísticas, que le servirá para ver de forma rápida y visual el estado del sistema.'
  });

  guidely.add ({
    attachTo: '#target-2'
    , anchor: 'top-right'
    , title: 'Panel de Acciones'
    , text: 'En este panel se agrupan las distintas opciones disponibles en cada sección (crear un nuevo elemento, acceder a una subsección, generar un fichero, ...).<br>Para acceder a una opción solo tiene que pulsar el botón correspondiente.'
  });

  guidely.add ({
    attachTo: '#target-3'
    , anchor: 'middle-middle'
    , title: 'Listado de Elementos'
    , text: 'Esta tabla muestra un listado en el que podrá consultar los elementos creados en cada sección. Utilice la caja de <b>Búsqueda</b> para filtrar los registros a visualizar.'
  });

  guidely.add ({
    attachTo: '#target-4'
    , anchor: 'middle-middle'
    , title: 'Menú principal'
    , text: 'Una vez que accede al sistema, el Menú Principal está siempre disponible para permitirle navegar entre los distintos módulos de la herramienta. Solo tiene que pulsar en la sección correspondiente para acceder a ella.'
  });

    guidely.add ({
    attachTo: '#target-5'
    , anchor: 'bottom-right'
    , title: 'Menú de Usuario'
    , text: 'Por último, el Menú de Usuario le recuerda en cada momento con que cuenta está trabajando. Además, haciendo click en él se le proporciona un acceso directo para cerrar su sesión.'
  });

  $('#guia').click(function(){
    guidely.init({welcome: true, startTrigger: false, welcomeTitle: 'Guía de uso básico', welcomeText: 'Bienvenido a la Guía de uso del sistema. Pulsando en el botón Iniciar, la plataforma le mostrará unas sencillas explicaciones sobre los distintos elementos que encontrará en la mayoría de las secciones.' });
  });

});

</script>

<!-- /contenido -->
</div>

<?php include_once('pie.php'); ?>
