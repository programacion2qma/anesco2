<?php
$seccionActiva=46;
include_once('../cabecera.php');


?> 

<div class="main" id="contenido">
    <div class="main-inner">
        <div class="container">
            <div class="row">

                <div class="span12">

                    <div class="widget widget-nopad">
                        <div class="widget-header"> <i class="icon-paste"></i>
                            <h3>Informe de rentabilidad desde el <?=$_POST['fechaDesde']?> hasta el <?=$_POST['fechaHasta']?></h3>
                            <div class='pull-right'>
                                <a href='index.php' class='btn btn-small btn-default'><i class='icon-chevron-left'></i> Volver</a>
                            </div>
                        </div>
                        <!-- /widget-header -->
                        <div class="widget-content">
                            <div class="widget big-stats-container">
                                <div class="widget-content">
                                    
                                    <?php
                                        generaInformeRentabilidad();
                                    ?>
                                    
                                </div> <!-- /widget-content -->                
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <!-- /container --> 
        </div>
        <!-- /main-inner --> 
    </div>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>