<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de informe rentabilidad

function preFiltro(){

	abreVentanaGestionConBotones('Informe de rentabilidad - filtro previo por fechas','genera-informe.php','span3','icon-calendar','margenAb',false,'noAjax','',$botonGuardar=true,'Generar informe','icon-cogs',false);

		campoFecha('fechaDesde','Fecha desde');
		
	cierraColumnaCampos();
	abreColumnaCampos();

		campoFecha('fechaHasta','Hasta');

	cierraVentanaGestion('index.php',true,true,'Generar informe','icon-cogs',false);
}


function generaInformeRentabilidad(){
	
	$estadisticas=array(
		'facturacionTotal'		=>	0,
		'gastosTotal'			=>	0,
		'balanceTotal'			=>	0,
		'rentabilidadGlobal'	=>	0,
		'facturacionVS'			=>	0,
		'gastosVS'				=>	0,
		'balanceVS'				=>	0,
		'rentabilidadPT'		=>	0,
		'facturacionPT'			=>	0,
		'gastosPT'				=>	0,
		'balancePT'				=>	0,
		'rentabilidadPT'		=>	0
		//Faltan los índices de los usuarios (se definen dinámicamente con las llamadas a la función obtieneDatosUsuario())
	);

	$datos=arrayFormulario();
	extract($datos);

	conexionBD();

	obteneDatosGlobalesInforme($estadisticas,$fechaDesde,$fechaHasta);
	obtieneDatosTipoConcepto($estadisticas,$fechaDesde,$fechaHasta,'RM','Vigilancia de la Salud','VS');
	obtieneDatosTipoConcepto($estadisticas,$fechaDesde,$fechaHasta,'PT','Tecnico','PT');    


	echo "
	<h6 class='bigstats'>Datos globales:</h6>
	<div class='big_stats cf'>
        <div class='stat textoVerde'> <i class='icon-sign-in textoVerde'></i> <span class='value textoVerde'>".formateaNumeroWeb($estadisticas['facturacionTotal'])." €</span><br />Total facturado</div>
        <div class='stat textoRojo'> <i class='icon-sign-out textoRojo'></i> <span class='value textoRojo'>".formateaNumeroWeb($estadisticas['gastosTotal'])." €</span><br />Total gastado</div>
        <div class='stat textoAzul'> <i class='icon-balance-scale textoAzul'></i> <span class='value textoAzul'>".$estadisticas['balanceTotal']." €</span><br />Balance</div>
        <div class='stat textoAmarillo'> <i class='icon-line-chart textoAmarillo'></i> <span class='value textoAmarillo'>".formateaNumeroWeb($estadisticas['rentabilidadGlobal'])." %</span><br />Rentabilidad global</div>
    </div>
    <hr />
    <h6 class='bigstats'>Datos sobre vigilancia de la salud:</h6>
    <div class='big_stats cf'>
        <div class='stat'> <i class='icon-sign-in'></i> <span class='value'>".formateaNumeroWeb($estadisticas['facturacionVS'])." €</span><br />Facturado serie RM</div>
        <div class='stat'> <i class='icon-sign-out'></i> <span class='value'>".formateaNumeroWeb($estadisticas['gastosVS'])." €</span><br />Gastado en VS</div>
        <div class='stat'> <i class='icon-balance-scale'></i> <span class='value'>".$estadisticas['balanceVS']." €</span><br />Balance</div>
        <div class='stat'> <i class='icon-line-chart'></i> <span class='value'>".formateaNumeroWeb($estadisticas['rentabilidadVS'])." %</span><br />Rentabilidad global</div>
    </div>
    <hr />
    <h6 class='bigstats'>Datos sobre parte técnica:</h6>
    <div class='big_stats cf'>
        <div class='stat'> <i class='icon-sign-in'></i> <span class='value'>".formateaNumeroWeb($estadisticas['facturacionPT'])." €</span><br />Facturado serie PT</div>
        <div class='stat'> <i class='icon-sign-out'></i> <span class='value'>".formateaNumeroWeb($estadisticas['gastosPT'])." €</span><br />Gastado en parte técnica</div>
        <div class='stat'> <i class='icon-balance-scale'></i> <span class='value'>".$estadisticas['balancePT']." €</span><br />Balance</div>
        <div class='stat'> <i class='icon-line-chart'></i> <span class='value'>".formateaNumeroWeb($estadisticas['rentabilidadPT'])." %</span><br />Rentabilidad global</div>
    </div>";

    $consulta=consultaBD("SELECT codigo, CONCAT(nombre,' ',apellidos) AS nombre FROM usuarios WHERE usuario!='soporte' AND tipo='TECNICO' ORDER BY nombre");
    while($usuario=mysql_fetch_assoc($consulta)){
    	$codigoUsuario=$usuario['codigo'];
    	$nombreUsuario=$usuario['nombre'];

    	obtieneDatosUsuario($estadisticas,$fechaDesde,$fechaHasta,$codigoUsuario);
    	echo "
    	<hr />
	    <h6 class='bigstats'>Datos para el trabajador $nombreUsuario:</h6>
	    <div class='big_stats cf'>
	        <div class='stat'> <i class='icon-sign-in'></i> <span class='value'>".formateaNumeroWeb($estadisticas['facturacion'.$codigoUsuario])." €</span><br />Facturado</div>
	        <div class='stat'> <i class='icon-sign-out'></i> <span class='value'>".formateaNumeroWeb($estadisticas['gastos'.$codigoUsuario])." €</span><br />Gastado</div>
	        <div class='stat'> <i class='icon-balance-scale'></i> <span class='value'>".$estadisticas['balance'.$codigoUsuario]." €</span><br />Balance</div>
	        <div class='stat'> <i class='icon-line-chart'></i> <span class='value'>".formateaNumeroWeb($estadisticas['rentabilidad'.$codigoUsuario])." %</span><br />Rentabilidad global</div>
	    </div>";
    }


    cierraBD();
}


function obteneDatosGlobalesInforme(&$estadisticas,$fechaDesde,$fechaHasta){

	//Facturación total
    $datos=consultaBD("SELECT IFNULL(SUM(total),0) AS total 
    				   FROM facturas 
    				   WHERE codigoSerieFactura IS NOT NULL AND (tipoFactura='NORMAL' OR tipoFactura='PROFORMA' OR tipoFactura='LIBRE') 
    				   AND facturas.eliminado='NO' AND fecha>='$fechaDesde' AND fecha<='$fechaHasta';",false,true);

    $estadisticas['facturacionTotal']=$datos['total'];

    
    $datos=consultaBD("SELECT SUM(total) AS total 
    				   FROM facturas_reconocimientos_medicos 
    				   WHERE codigoSerieFactura IS NOT NULL AND eliminado='NO' AND fecha>='$fechaDesde' AND fecha<='$fechaHasta';",false,true);

    $estadisticas['facturacionTotal']+=$datos['total'];


    //Gastos totales
    $datos=consultaBD("SELECT SUM(total) AS total
    				   FROM facturacion_gastos 
    				   WHERE fecha>='$fechaDesde' AND fecha<='$fechaHasta';",false,true);

    $estadisticas['gastosTotal']=$datos['total'];

    $datos=consultaBD("SELECT SUM(total) AS total
    				   FROM gastos_tarea INNER JOIN tareas ON gastos_tarea.codigoTarea=tareas.codigo
    				   WHERE fechaFin>='$fechaDesde' AND fechaFin<='$fechaHasta';",false,true);

    $estadisticas['gastosTotal']+=$datos['total'];


	//Cálculo balance
	$estadisticas['balanceTotal']=$estadisticas['facturacionTotal']-$estadisticas['gastosTotal'];
    $estadisticas['balanceTotal']=formateaNumeroWeb($estadisticas['balanceTotal']);
    if($estadisticas['balanceTotal']>0){
    	$estadisticas['balanceTotal']='+'.$estadisticas['balanceTotal'];
    }

    //Cálculo porcentaje rentabilidad
    if($estadisticas['gastosTotal']>0){
    	$estadisticas['rentabilidadGlobal']=($estadisticas['facturacionTotal']-$estadisticas['gastosTotal'])/$estadisticas['gastosTotal']*100;
    }
    else{
    	$estadisticas['rentabilidadGlobal']=($estadisticas['facturacionTotal']-$estadisticas['gastosTotal'])/1*100;
    }
    

}


function obtieneDatosTipoConcepto(&$estadisticas,$fechaDesde,$fechaHasta,$serie,$tipoGasto,$sufijoEstadisticas){

	//Facturación total
    $datos=consultaBD("SELECT IFNULL(SUM(total),0) AS total 
    				   FROM facturas INNER JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
    				   WHERE codigoSerieFactura IS NOT NULL AND (tipoFactura='NORMAL' OR tipoFactura='PROFORMA' OR tipoFactura='LIBRE') 
    				   AND facturas.eliminado='NO' AND fecha>='$fechaDesde' AND fecha<='$fechaHasta' AND serie='$serie';",false,true);

    $estadisticas['facturacion'.$sufijoEstadisticas]=$datos['total'];

    
    $datos=consultaBD("SELECT SUM(total) AS total 
    				   FROM facturas_reconocimientos_medicos INNER JOIN series_facturas ON facturas_reconocimientos_medicos.codigoSerieFactura=series_facturas.codigo
    				   WHERE codigoSerieFactura IS NOT NULL AND eliminado='NO' AND fecha>='$fechaDesde' AND fecha<='$fechaHasta' AND serie='$serie';",false,true);

    $estadisticas['facturacion'.$sufijoEstadisticas]+=$datos['total'];


    //Gastos totales
    $datos=consultaBD("SELECT SUM(total) AS total
    				   FROM facturacion_gastos 
    				   WHERE fecha>='$fechaDesde' AND fecha<='$fechaHasta' AND tipo='$tipoGasto';",false,true);

    $estadisticas['gastos'.$sufijoEstadisticas]=$datos['total'];

    if($tipoGasto=='Tecnico'){
	    $datos=consultaBD("SELECT SUM(total) AS total
	    				   FROM gastos_tarea INNER JOIN tareas ON gastos_tarea.codigoTarea=tareas.codigo
	    				   WHERE fechaFin>='$fechaDesde' AND fechaFin<='$fechaHasta';",false,true);

	    $estadisticas['gastos'.$sufijoEstadisticas]+=$datos['total'];
	}


	//Cálculo balance
	$estadisticas['balance'.$sufijoEstadisticas]=$estadisticas['facturacion'.$sufijoEstadisticas]-$estadisticas['gastos'.$sufijoEstadisticas];
    $estadisticas['balance'.$sufijoEstadisticas]=formateaNumeroWeb($estadisticas['balance'.$sufijoEstadisticas]);
    if($estadisticas['balance'.$sufijoEstadisticas]>0){
    	$estadisticas['balance'.$sufijoEstadisticas]='+'.$estadisticas['balance'.$sufijoEstadisticas];
    }

    //Cálculo porcentaje rentabilidad
    if($estadisticas['gastos'.$sufijoEstadisticas]>0){
    	$estadisticas['rentabilidad'.$sufijoEstadisticas]=($estadisticas['facturacion'.$sufijoEstadisticas]-$estadisticas['gastos'.$sufijoEstadisticas])/$estadisticas['gastos'.$sufijoEstadisticas]*100;
    }
    else{
    	$estadisticas['rentabilidad'.$sufijoEstadisticas]=($estadisticas['facturacion'.$sufijoEstadisticas]-$estadisticas['gastos'.$sufijoEstadisticas])/1*100;	
    }
    

}


function obtieneDatosUsuario(&$estadisticas,$fechaDesde,$fechaHasta,$codigoUsuario){

	//Facturación total
    $datos=consultaBD("SELECT IFNULL(SUM(total),0) AS total 
    				   FROM facturas INNER JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
    				   INNER JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
    				   WHERE codigoSerieFactura IS NOT NULL AND (tipoFactura='NORMAL' OR tipoFactura='PROFORMA' OR tipoFactura='LIBRE') 
    				   AND facturas.eliminado='NO' AND fecha>='$fechaDesde' AND fecha<='$fechaHasta' AND contratos.tecnico='$codigoUsuario';",false,true);

    $estadisticas['facturacion'.$codigoUsuario]=$datos['total'];


    //Gastos totales
    $datos=consultaBD("SELECT SUM(total) AS total
    				   FROM facturacion_gastos 
    				   WHERE fecha>='$fechaDesde' AND fecha<='$fechaHasta' AND codigoUsuario='$codigoUsuario';",false,true);

    $estadisticas['gastos'.$codigoUsuario]=$datos['total'];

    
    $datos=consultaBD("SELECT SUM(total) AS total
    				   FROM gastos_tarea INNER JOIN tareas ON gastos_tarea.codigoTarea=tareas.codigo
    				   WHERE fechaFin>='$fechaDesde' AND fechaFin<='$fechaHasta' AND codigoUsuario='$codigoUsuario';",false,true);

    $estadisticas['gastos'.$codigoUsuario]+=$datos['total'];



	//Cálculo balance
	$estadisticas['balance'.$codigoUsuario]=$estadisticas['facturacion'.$codigoUsuario]-$estadisticas['gastos'.$codigoUsuario];
    $estadisticas['balance'.$codigoUsuario]=formateaNumeroWeb($estadisticas['balance'.$codigoUsuario]);
    if($estadisticas['balance'.$codigoUsuario]>0){
    	$estadisticas['balance'.$codigoUsuario]='+'.$estadisticas['balance'.$codigoUsuario];
    }

    //Cálculo porcentaje rentabilidad
    if($estadisticas['gastos'.$codigoUsuario]>0){
    	$estadisticas['rentabilidad'.$codigoUsuario]=($estadisticas['facturacion'.$codigoUsuario]-$estadisticas['gastos'.$codigoUsuario])/$estadisticas['gastos'.$codigoUsuario]*100;
    }
    else{
    	$estadisticas['rentabilidad'.$codigoUsuario]=($estadisticas['facturacion'.$codigoUsuario]-$estadisticas['gastos'.$codigoUsuario])/1*100;
    }
    

}

//Fin parte de informe rentabilidad