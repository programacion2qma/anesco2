<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de incidencias

function operacionesActividades(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaActividad();
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaActividad();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('actividades');
	}

	mensajeResultado('nombre',$res,'Actividad');
    mensajeResultado('elimina',$res,'Actividad', true);
}

function insertaActividad(){
	$res=true;
	$res=insertaDatos('actividades');
	$res=insertaProcesos($res);
	return $res;
}

function actualizaActividad(){
	$res=true;
	$res=actualizaDatos('actividades');
	$res=insertaProcesos($_POST['codigo']);
	return $res;
}

function insertaProcesos($codigo){
	$res=true;
	$res=consultaBD('DELETE FROM actividades_procesos WHERE codigoActividad='.$codigo,true);
	$i=0;
	while(isset($_POST['proceso'.$i])){
		if($_POST['proceso'.$i]!=''){
			$res=consultaBD('INSERT INTO actividades_procesos VALUES(NULL,'.$codigo.',"'.$_POST['proceso'.$i].'")',true);
		}
		$i++;
	}
	return $res;
}


function gestionActividades(){
	operacionesActividades();
	
	abreVentanaGestionConBotones('Gestión de Actividades','index.php');
	$datos=compruebaDatos('actividades');

	campoTexto('nombre','Nombre',$datos);
	tablaProcesos($datos);

	cierraVentanaGestion('index.php');
}

function tablaProcesos($datos){
	echo "
	    <div class='control-group'>                     
	        <label class='control-label'>Procesos más habituales a desarrollar:</label>
	        <div class='controls'>
	            <table class='tabla-simple mitadAncho' id='tablaProcesos'>
	                <thead>
	                    <tr>
	                        <th> Procesos </th>
	                        <th> </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;
	            $j=1;
	            conexionBD();

	            if($datos){
	                $consulta=consultaBD("SELECT * FROM actividades_procesos WHERE codigoActividad=".$datos['codigo']);
	                while($proceso=mysql_fetch_assoc($consulta)){
	                	echo '<tr>';
	                    campoTextoTabla('proceso'.$i,$proceso['nombre'],'span6');
	                    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>";
	                    echo '</tr>';
	                    $i++;
	                    $j++;
	                }
	            }
	            
	            if($i==0){
	            	$procesos=array('Control/gestión documentación y expedientes','Atención a los clientes y centralita telefónica','Tramitación de pedidos y ofertas. Correspondencia con terceros','Realización y revisión de documentos. Informatización de cartas, estudios','Facturación','Gestión contabilidad. Gestiones en bancos y otras entidades','Recogida de correspondencia','Gestión de expedientes','Apertura y mantenimiento de expedientes de ventas, petición de certificados, consultas, etc','Petición de, piezas, accesorios, productos, etc','Tramitación de ofertas y garantías a los clientes');
	            	foreach ($procesos as $proceso) {
	            		echo '<tr>';
	                	campoTextoTabla('proceso'.$i,$proceso,'span6');
	                	echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>";
	                	echo '</tr>';
	                	$i++;
	                	$j++;
	                }
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaProcesos\");'><i class='icon-plus'></i> Añadir proceso</button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaProcesos\");'><i class='icon-trash'></i> Eliminar proceso</button>
	        </div>
	    </div>";
}


function imprimeActividades(){
	$consulta=consultaBD("SELECT * FROM actividades ORDER BY nombre ASC",true);
	while($datos=mysql_fetch_assoc($consulta)){

		echo "<tr>
				<td> ".$datos['nombre']." </td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	  	</td>
			 </tr>";
	}
}

function estadisticasActividadesRestrict($where=''){
	$res=array();

	conexionBD();

	$where=defineWhereEmpleado($where);
	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM actividades",false, true);

	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

//Fin parte de incidencias