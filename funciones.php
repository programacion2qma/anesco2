<?php
@include_once('config.php');//Carga del archivo de configuración
@include_once('../api/nucleo.php');//Carga del núcleo de funciones comunes

//Inicio de funciones específicas

/*
	La siguiente función, que es llamada en /cabecera.php, actúa como una suerte de middleware que verifica, primero,
	si el usuario está correctamente logueado (con compruebaSesion) y posteriormente, el nivel de acceso según su perfil 
	(a través de la llamada a compruebaPermisoAccesoPerfilSeccion).
	
	Si el nivel de acceso del perfil no incluye la página que se está solicitando, se envía un error 404 y se para la ejecución
*/
function compruebaPermisos(){
	global $_CONFIG;

	compruebaSesion();

	$paginaSolicitada=$_SERVER['PHP_SELF'];
	
	if(substr_count($paginaSolicitada,$_CONFIG['raiz'])==0){//El usuario está intentando acceder a una página fuera del sistema de secciones
		$permiso=false;
	}
	else{
		//Las siguientes 2 líneas realizan un tratamiento del string con la url solicitada para extraer de ella la sección
		$paginaSolicitada=explode($_CONFIG['raiz'],$paginaSolicitada);

        if($paginaSolicitada[1]=='inicio.php'){
            $paginaSolicitada='inicio';
        }
        else{
            $paginaSolicitada=substr($paginaSolicitada[1],0,strrpos($paginaSolicitada[1],'/'));
        }
        
		$permiso=compruebaPermisoAccesoPerfilSeccion($paginaSolicitada);
	}
	

	if(!$permiso){
		//Si el usuario no tiene permiso de acceso a la sección, se le envía un error 404 y se para la ejecución
		header("HTTP/1.0 404 Not Found");
		die();
	}

}

function compruebaPermisoAccesoPerfilSeccion($seccion){
	$res=false;//El acceso por defecto se prohíbe, y las comprobaciones son para habilitarlo
    
	$perfil=$_SESSION['tipoUsuario'];
    
	if($perfil=='CLIENTE'){//Acceso a todo
		if(
			$seccion=='inicio' || $seccion=='empleados' || $seccion=='documentacion-cliente'
		){
			$res=true;
		}
	}
	elseif($perfil=='EMPLEADO'){//Acceso a todo
		if(
			$seccion=='documentacion-empleado' || $seccion=='accidentes-empleado'
		){
			$res=true;
		}
	}
	elseif($perfil!='CLIENTE' && $perfil!='EMPLEADO'){
        $res=true;
	}


	return $res;
}

//Fin seguridad

function loginAnesco($usuario,$clave){
    global $_CONFIG;

    $usuario=addslashes($usuario);
    $clave=addslashes($clave);

    if($usuario!='' && $clave!=''){
        conexionBD();
        $consulta=consultaBD("SELECT codigo, usuario, tipo, superadmin FROM usuarios WHERE usuario='$usuario' AND clave='$clave';");
        cierraBD();
        if(mysql_num_rows($consulta)==1){
            $datos=mysql_fetch_assoc($consulta);
            $_SESSION['codigoU']=$datos['codigo'];
            $_SESSION['usuario']=$datos['usuario'];
            $_SESSION['tipoUsuario']=$datos['tipo'];
            $_SESSION['superadmin']=$datos['superadmin'];
            $_SESSION['ejercicio']=date('Y');

            if(isset($_POST['sesion']) && $_POST['sesion']=="si"){//Crea cookie de Sesión
                $valor=md5(rand());
                conexionBD();
                $consulta=consultaBD("UPDATE usuarios SET sesion='$valor' WHERE usuario='$usuario' AND clave='$clave';");
                cierraBD();
                // Caduca en un año
                setcookie($_CONFIG['nombreCookie'],$valor, time() + 365 * 24 * 60 * 60);
            }
        
            if($usuario!='soporte'){//MODIFICACIÓN 04/09/2015: para incluir el usuario "fantasma" de soporte
                registraAcceso();
            }

            header('Location: '.$_CONFIG['raiz'].'inicio.php');
        }
    }
    return 1;
}

//Parte de inicio

function estadisticasInicio(){
    conexionBD();

    if($_SESSION['tipoUsuario']=='CLIENTE'){
       $res=estadisticasInicioCliente($_SESSION['codigoU']);
    }
    elseif($_SESSION['tipoUsuario']=='TECNICO'){
        $res=estadisticasInicioTecnico($_SESSION['codigoU']);
    }
    else{
        $res=estadisticasInicioAdmin();
    }

    cierraBD();

    return $res;
}

function estadisticasInicioAdmin(){
    $res=array();

    $datos=consultaBD("SELECT COUNT(codigo) AS total FROM clientes;",false,true);
    $res['clientes']=$datos['total'];

    $datos=consultaBD("SELECT COUNT(codigo) AS total FROM formularioPRL;",false,true);
    $res['prl']=$datos['total'];

    return $res;
}

function estadisticasInicioTecnico($codigoU){
    $res=array();

    $where=defineWhereEmpleado('WHERE activo="SI"');
    $datos=consultaBD("SELECT COUNT(DISTINCT clientes.codigo) AS total 
        FROM clientes
        INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
            INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
            LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
             ".$where."
        AND (contratos.tecnico=".$_SESSION['codigoU']." || formularioPRL.codigoUsuarioTecnico=".$_SESSION['codigoU'].");",false,true);
    $res['clientes']=$datos['total'];

    $datos=consultaBD("SELECT COUNT(formularioPRL.codigo) AS total FROM formularioPRL INNER JOIN contratos ON formularioPRL.codigoContrato=contratos.codigo WHERE tecnico='$codigoU';",false,true);
    $res['prl']=$datos['total'];


    return $res;
}

function estadisticasInicioCliente($codigoUsuarioCliente){
    $res=array();

    $datos=consultaBD("SELECT IFNULL(COUNT(formularioPRL.codigo),0) AS total 
                       FROM formularioPRL INNER JOIN contratos ON formularioPRL.codigoContrato=contratos.codigo
                       INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
                       INNER JOIN usuarios_clientes ON ofertas.codigoCliente=usuarios_clientes.codigoCliente
                       WHERE usuarios_clientes.codigoUsuario='$codigoUsuarioCliente';",false,true);
    
    $res['prl']=$datos['total'];

    return $res;
}


function compruebaContratosVigor(){
    $res=true;

    conexionBD();

    // $codigosOferta=consultaArray("SELECT codigoOferta FROM contratos WHERE fechaInicio>=CURDATE()");
    // $codigosOferta=implode(',',$codigosOferta);
    
    // if($codigosOferta==''){
    //     $codigosOferta='-1';
    // }
    
    //La siguiente consulta cancela todos los contratos cuya fecha de finalización ha llegado
    $res=consultaBD("UPDATE contratos SET enVigor='NO' WHERE fechaFin<CURDATE() AND enVigor='SI' AND eliminado='NO';");

    //Esta otra, por su parte, marca como en vigor los contratos cuya fecha de vigor ya ha llegado
    $res=$res && consultaBD("UPDATE contratos SET enVigor='SI' WHERE fechaInicio<=CURDATE() AND fechaFin>=CURDATE() AND eliminado='NO';");

    cierraBD();

    if(!$res){
        mensajeError("no se ha podido procesar la baja de contratos fuera de plazo.");
    }
}

//Fin parte de inicio

function estadisticasClientes(){
    $res=array();

    conexionBD();

    if($_SESSION['tipoUsuario'] == 'ADMIN'){
        $consulta=consultaBD("SELECT COUNT(codigo) AS total FROM clientes ORDER BY razon_s;");
    } else {
        $director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],true, true);
        if($director['director'] == 'SI'){
            $sql="SELECT codigo, fecha, razon_s, telefono, email FROM clientes WHERE empleado IN (SELECT codigo FROM usuarios WHERE tipo LIKE '".$director['tipo']."') ORDER BY razon_s;";
            $consulta=consultaBD($sql);
        } else {
            $consulta=consultaBD("SELECT codigo, fecha, razon_s, telefono, email FROM clientes WHERE empleado LIKE ".$_SESSION['codigoU']." ORDER BY razon_s;");
        }
    }

    cierraBD();

    return $res;
}

//Parte de manejo de listados

function obtieneLimitesListado(){
    $limite='';
    if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength']!='-1') {
        $limite='LIMIT '.(int)$_GET['iDisplayStart'].', '.(int)$_GET['iDisplayLength'];
    }
    return $limite;
}

function obtieneOrdenListado($columnas){
    $orden = '';
    if (isset($_GET['iSortCol_0'])) {
        $orden = 'ORDER BY  ';
        for ($i=0 ; $i<(int)$_GET['iSortingCols'] ; $i++) {
            if ( $_GET[ 'bSortable_'.(int)$_GET['iSortCol_'.$i] ] == 'true' ) {
                $orden .= $columnas[ (int)$_GET['iSortCol_'.$i] ].' '.
                    ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
            }
        }

        $orden = substr_replace($orden, '', -2);
        if (trim($orden) == 'ORDER BY') {
            $orden = '';
        }
    }
    return $orden;
}

/*
    El algoritmo de creación del WHERE/HAVING de esta función es específico de "La Academia Empresas", ya que tiene búsquedas por columnas.
    El filtrado de usuario, determinado por el parámetro $compruebaPerfil, se realiza siguiendo los siguientes criterios:
        Perfil COMERCIAL, ADMINISTRACION2 y TELEMARKETING: se filtran los registros por el código del usuario actual.
        Perfil DIRECTOR y DELEGADO: se filtran los registros cuyos código de usuario dependan del que tiene el usuario actual
        Resto de perfiles: no se comprueba el usuario.
*/
function obtieneWhereListado($where,$columnas,$compruebaPerfil=false,$listadoClientes=false){
    $camposFecha=array();//Este array sirve para ir almacenando las columnas de tipo fecha, e identificar así si la condición debe ser >= ó <=

    $res=obtieneWhereEjercicioListado($columnas,$listadoClientes);

    for($i=0; $i<count($columnas) ; $i++){
        if(isset($_GET['sSearch']) && $_GET['sSearch']!='') {//Búsqueda normal (global)
            $_GET['sSearch']=str_replace("'",'"',$_GET['sSearch']);//Sustituye la comilla simple por una doble, para que no haya conflictos de sintaxis

            $res.="CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$_GET['sSearch']."%' USING latin1) AS binary) USING utf8)  OR  ";
        }

        if(isset($_GET['sSearch_'.$i]) && $_GET['sSearch_'.$i]!=''){//Búsqueda por columnas (nótese el índice _$i)
            $_GET['sSearch_'.$i]=str_replace("'",'"',$_GET['sSearch_'.$i]);//Sustituye la comilla simple por una doble, para que no haya conflictos de sintaxis

            if($_GET['sSearch_'.$i]=='ISNUL'){//Para los casos de NULL
                $res.=$columnas[$i]." IS NULL AND ";
            }
            elseif($_GET['sSearch_'.$i]=='NOTNUL'){//Para los casos de NOT NULL
                $res.=$columnas[$i]." IS NOT NULL AND ";
            }
            elseif(substr_count($columnas[$i],'fecha')>0 && !in_array($columnas[$i],$camposFecha)){
                $campoFecha=formateaFechaBD($_GET['sSearch_'.$i]);
                $res.=$columnas[$i].">='".$campoFecha."' AND ";
                
                array_push($camposFecha,$columnas[$i]);
            }
            elseif(substr_count($columnas[$i],'fecha')>0){
                $campoFecha=formateaFechaBD($_GET['sSearch_'.$i]);
                $res.=$columnas[$i]."<='".$campoFecha."' AND ";
            }
            elseif(substr_count($columnas[$i],'precio')>0 || substr_count($columnas[$i],'importe')>0){//Para que la búsqueda de 9,99 coincida con el valor 9.99
                $campoNumero=formateaNumeroFiltro($_GET['sSearch_'.$i]);
                $res.=$columnas[$i]." LIKE '%".$campoNumero."%' AND ";
            }
            elseif(substr_count($columnas[$i],'codigo')>0 || substr_count($columnas[$i],'avanceScorm')>0 || substr_count($columnas[$i],'estado')>0 || substr_count($columnas[$i],'tiempoScorm')>0 || substr_count($columnas[$i],'stock')>0){//Cuando se está filtrando por un código, la ocurrencia debe ser exacta (si se usa LIKE, el código 123 está contenido en el 7364123).
                $res.=$columnas[$i]."='".$_GET['sSearch_'.$i]."' AND ";
            }
            elseif(substr_count($_GET['sSearch_'.$i],'>')>0){//Para filtrar cantidades
                $valor=str_replace('>','',$_GET['sSearch_'.$i]);
                $valor=trim($valor);
                $res.=$columnas[$i].">'".$valor."' AND ";
            }
            elseif(substr_count($_GET['sSearch_'.$i],'<')>0){//Para filtrar cantidades
                $valor=str_replace('<','',$_GET['sSearch_'.$i]);
                $valor=trim($valor);
                $res.=$columnas[$i]."<'".$valor."' AND ";
            }
            elseif(substr_count($columnas[$i],'sentencia')>0){
                 $res.="CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".base64_encode($_GET['sSearch_'.$i])."%' USING latin1) AS binary) USING utf8) AND ";
            }
            else{//Casos normales de cadenas (hay que hacerle conversiones de codificación de caracteres para que no haga cosas raras con tildes y ñ)
                $res.="CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$_GET['sSearch_'.$i]."%' USING latin1) AS binary) USING utf8) AND ";//La búsqueda por columnas es con AND, para que los filtros sean acumulativos
            }
        }
    }

    $res=substr_replace($res,'',-4);
    $res.=')';

    if($res==' )'){//No se ha realizado búsquedas...
        $res='';
    }

    if($compruebaPerfil){
        $res.=filtraListadoPorUsuario($listadoClientes);
    }

    return $where.$res;
}

function obtieneOrdenListadoExcel($columnas){
    $res='';

    $datos=arrayFormulario();

    if(isset($datos['columnaOrdenada'])){
        $columnaOrdenada=$columnas[$datos['columnaOrdenada']];
        $sentido=$datos['sentidoOrden'];

        $res="ORDER BY $columnaOrdenada $sentido";
    }

    return $res;
}

function obtieneWhereListadoExcel($where,$columnas,$compruebaPerfil=false,$listadoClientes=false){
    $datos=arrayFormulario();
    $camposFecha=array();

    $res=obtieneWhereEjercicioListado($columnas,$listadoClientes);

    for($i=0; $i<count($columnas) ; $i++){
        if(isset($datos['global']) && $datos['global']!='') {
            $datos['global']=str_replace("'",'"',$datos['global']);

            $res.="CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$datos['global']."%' USING latin1) AS binary) USING utf8)  OR  ";
        }

        if(isset($datos[$i]) && $datos[$i]!='' && $datos[$i]!='NULL'){//Condición extra con respecto a función original: $datos[$i]!='NULL'
            $datos[$i]=str_replace("'",'"',$datos[$i]);

            if($datos[$i]=='ISNUL'){
                $res.=$columnas[$i]." IS NULL AND ";
            }
            elseif($datos[$i]=='NOTNUL'){
                $res.=$columnas[$i]." IS NOT NULL AND ";
            }
            elseif(substr_count($columnas[$i],'fecha')>0 && !in_array($columnas[$i],$camposFecha)){
                $campoFecha=formateaFechaBD($datos[$i]);
                $res.=$columnas[$i].">='".$campoFecha."' AND ";
                
                array_push($camposFecha,$columnas[$i]);
            }
            elseif(substr_count($columnas[$i],'fecha')>0){
                $campoFecha=formateaFechaBD($datos[$i]);
                $res.=$columnas[$i]."<='".$campoFecha."' AND ";
            }
            elseif(substr_count($columnas[$i],'precio')>0 || substr_count($columnas[$i],'importe')>0){//Para que la búsqueda de 9,99 coincida con el valor 9.99
                $campoNumero=formateaNumeroFiltro($datos[$i]);
                $res.=$columnas[$i]." LIKE '%".$campoNumero."%' AND ";
            }
            elseif(substr_count($columnas[$i],'codigo')>0 || substr_count($columnas[$i],'avanceScorm')>0 || substr_count($columnas[$i],'estado')>0 || substr_count($columnas[$i],'tiempoScorm')>0 || substr_count($columnas[$i],'stock')>0){//Cuando se está filtrando por un código, la ocurrencia debe ser exacta (si se usa LIKE, el código 123 está contenido en el 7364123).
                $res.=$columnas[$i]."='".$datos[$i]."' AND ";
            }
            elseif(substr_count($datos[$i],'>')>0){//Para filtrar cantidades
                $valor=str_replace('>','',$datos[$i]);
                $valor=trim($valor);
                $res.=$columnas[$i].">'".$valor."' AND ";
            }
            elseif(substr_count($datos[$i],'<')>0){//Para filtrar cantidades
                $valor=str_replace('<','',$datos[$i]);
                $valor=trim($valor);
                $res.=$columnas[$i]."<'".$valor."' AND ";
            }
            elseif(substr_count($columnas[$i],'sentencia')>0){
                 $res.="CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".base64_encode($datos[$i])."%' USING latin1) AS binary) USING utf8) AND ";
            }
            else{//Casos normales de cadenas (hay que hacerle conversiones de codificación de caracteres para que no haga cosas raras con tildes y ñ)
                $res.="CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$datos[$i]."%' USING latin1) AS binary) USING utf8) AND ";//La búsqueda por columnas es con AND, para que los filtros sean acumulativos
            }
        }
    }

    $res=substr_replace($res,'',-4);
    $res.=')';

    if($res==' )'){//No se ha realizado búsquedas...
        $res='';
    }

    if($compruebaPerfil){
        $res.=filtraListadoPorUsuario($listadoClientes);
    }

    return $where.$res;
}

//La siguiente función genera una condición WHERE/HAVING para todos los campos fecha de los listados
function obtieneWhereEjercicioListado($columnas,$listadoClientes){
    $cadenaColumnas=implode(',',$columnas);
    $res=' AND(';

    if(isset($_SESSION['ejercicio']) && strpos($cadenaColumnas,'fecha')){//Si se ha activado el filtro de ejercicios y el listado contiene fechas...
        $anio=$_SESSION['ejercicio'];
        $operador=defineOperadorFiltroEjercicios($listadoClientes);
        $fechasSinFiltro=array(     'fechaDevolucion',
                                    'trabajos.fechaTomaDatos',
                                    'trabajos.fechaRevision',
                                    'trabajos.fechaEnvio',
                                    'trabajos.fechaMantenimiento',
                                    'trabajos.fechaPrevista',
                                    'trabajos.fechaEmision',
                                    'trabajos.fechaEntrega',
                                    'fechaCobroListadoVentas',
                                    'fechaFacturaListadoVentas',
                                    'fechaInicio',
                                    'fechaFin',
                                    'fechaNacimiento',
                                    'fechaFacturacionVencimiento'
                        );

        for($i=0,$filtradas=0;$i<count($columnas);$i++){


            if(substr_count($columnas[$i],'fecha')>0 && !in_array($columnas[$i],$fechasSinFiltro)){
                $res.="(YEAR(".$columnas[$i].")$operador $anio OR YEAR(".$columnas[$i].") IS NULL OR YEAR(".$columnas[$i].")=0) AND ";//...se aplica utilizando la función YEAR() de MySQL
                $filtradas++;
            }
        }

        if($filtradas>0 && isset($_GET['sSearch']) && $_GET['sSearch']!=''){//Si existe una búsqueda global, cierro la apertura del paréntesis y abro otro nuevo, porque en la búsqueda global se usa OR
            $res=quitaUltimaComa($res,5);
            $res.=') AND(';
        }
    }

    return $res;
}

function defineOperadorFiltroEjercicios($seccionClientes){
    $res='=';

    if($seccionClientes){
        $res='<=';
    }
    
    return $res;
}

function inicializaArrayListado($consulta,$consultaPaginacion){
    $intervalo=25;
    if(isset($_GET['sEcho'])){
        $intervalo=intval($_GET['sEcho']);
    }

    $seleccionado=mysql_num_rows($consulta);
    $total=mysql_num_rows($consultaPaginacion);

    return array(
        'sEcho' => $intervalo,
        'iTotalRecords' => $seleccionado,
        'iTotalDisplayRecords' => $total,
        'aaData' => array()
    );
}

//Fin parte manejo listados


//Parte de satisfacción de clientes


function creaTablaSatisfaccion(){
    echo '
    <center>
        <table class="table table-striped table-bordered mitadAncho">
          <tbody>
            <tr>
              <th> Apartado Analizado </th>
              <th> Valoración </th>
            </tr>';
}

function cierraTablaSatisfaccion(){
    echo '</tbody>
        </table>
    </center>';
}

function creaPreguntaTablaEncuesta($texto,$numero,$datos=false){    
    echo "
    <tr>
        <td class='justificado'>$texto</td>
        <td class='centro'>
            <input type='number' name='pregunta".$numero."' id='pregunta".$numero."' class='rating' data-min='1' data-max='5'";
    
    if($datos!=false){      
        echo "value=".$datos['pregunta'.$numero];       
    }

    echo" />
        </td>
    </tr>";
}

function creaAreaTextoTablaEncuesta($datos=false){
    $valor=compruebaValorCampo($datos,'comentarios');
    echo "
    <tr>
        <td colspan='2'>Si desea realizar algún comentario adicional, por favor, hágalo a continuación:</td>
    </tr>
    <tr>
        <td colspan='2'>
            <textarea name='comentarios' class='textarea-amplia' id='comentarios'>$valor</textarea>
        </td>
    </tr>";
}

function textoCuestionario(){
    global $_CONFIG;

    echo "
    <p class='justificado parrafo-margenes'>
      <span class='negritaCursiva'>Estimado cliente:</span><br />en ".$_CONFIG['tituloGeneral']." nos preocupa mucho su satisfacción con los servicios
      contratados con nuestra entidad y la calidad final de los proyectos ejecutados. Por ello, le agradecemos que pueda 
      hacernos llegar la presente encuesta de satisfacción, indicando además en el apartado observaciones cualquier 
      comentario que crea que pueda ayudarnos a mejorar la calidad de nuestros servicios o a ajustar los mismos a sus 
      necesidades. Sin duda, creemos firmemente que en sus opiniones y sugerencias se encuentran nuestras mayores 
      oportunidades de mejorar.<br />
      Gracias por anticipado.<br /><br />
      <strong>Puntúe de en una escala del 1 al 5 cada uno de los siguientes aspectos:</strong>
    </p>";
}

//Fin parte de satisfacción de clientes

//Parte de agenda/tareas

function compruebaBotonesAgenda($codigo,$estado){
    global $_CONFIG;

    $res="<a href='gestion.php?codigo=".$codigo."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>";
    if($estado=='PENDIENTE'){
        $res="<div class='btn-group'>
                <button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
                <ul class='dropdown-menu' role='menu'>
                    <li><a href='".$_CONFIG['raiz']."tareas/gestion.php?codigo=".$codigo."'><i class='icon-search-plus'></i> Detalles</a></li>
                    <li class='divider'></li>
                    <li><a href='?&tarea=".$codigo."&realizada'><i class='icon-check-circle'></i> Marcar como realizada</a></li>
                </ul>
             </div>";
    }
    return $res;
}


/*function operacionesAgenda(){
    $res=true;

    if(isset($_POST['repetir'])){
        $res=repiteEventoSemanalmente();
    }
    elseif(isset($_GET['realizada'])){
        $res=cambiaValorCampo('estado','REALIZADA',$_GET['tarea'],'tareas');
    }
    elseif(isset($_POST['codigo'])){
        $res=actualizaDatos('tareas');
    }
    elseif(isset($_POST['tarea'])){
        $res=insertaDatos('tareas');
    }
    elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
        $res=eliminaDatos('tareas');
    }

    mensajeResultado('tarea',$res,'sesión');
    mensajeResultado('repetir',$res,'sesión');
    mensajeResultado('elimina',$res,'sesión', true);
}*/

//Fin parte de agenda/tareas

//Nuevas funciones de cerrar formulario

function cierraVentanaGestionAdicional($textoFinalizado,$destino,$columnas=false,$botonGuardar=true,$texto='Guardar',$icono='icon-check',$botonVolver=true,$textoVolver='Volver',$iconoVolver='icon-chevron-left'){
    if($columnas){
        echo "        </fieldset>
                      <fieldset class='sinFlotar'>";
    }

    echo "
                        <br />                      
                        <div class='form-actions'>";
    if($botonVolver){
        echo "            <a href='$destino' class='btn btn-default'><i class='$iconoVolver'></i> $textoVolver</a>";
    }

    if($botonGuardar){                      
        echo "            <button type='button' name='Guardar' value='Guardar' class='btn btn-propio submit'><i class='$icono'></i> $texto</button>";
    }
    
        echo "            <button type='button' name='Finalizar' value='Finalizar' class='btn btn-propio submit'><i class='$icono'></i> $textoFinalizado</button>";


    echo "
                        </div> <!-- /form-actions -->
                      </fieldset>
                    </form>
                    </div>


                </div>
                <!-- /widget-content --> 
              </div>

          </div>
        </div>
        <!-- /container --> 
      </div>
      <!-- /main-inner --> 
    </div>
    <!-- /main -->";
}

//Comprobación de resultados de operaciones para mostrar mensajes
function mensajeResultadoAdicional($indice,$res,$campo,$eliminacion=false){
    if(isset($_REQUEST[$indice])){
        if($res && $eliminacion){
            mensajeOk("Eliminación realizada correctamente."); 
        }
        elseif($res && !$eliminacion){
            if($_REQUEST[$indice] == 'Guardar'){
                mensajeOk("Datos de $campo guardados correctamente."); 
            } else {
                mensajeOk("Datos de $campo guardados correctamente y gestión finalizada."); 
            }
        }
        else{
          mensajeError("se ha producido un error al gestionar los datos. Compruebe la información introducida."); 
        }
    }
}

//Fin de nuevas funciones de cerrar formulario

// Funciones para la creación y cierre de pestañas
function cierraPestanias(){
    echo '</div><!-- /tab-content -->
        </div><!-- /tabbable -->
    </div>';
}

function abrePestania($seccion,$activa=''){
    echo "<div class='tab-pane $activa' id='$seccion'>";
}

function cierraPestania(){
    echo "</div><!-- /tab-pane -->";
}

//FUNCIONES DE AUDITORÍAS
function creaPreguntaAuditoria($datos,$texto,$num,$tam="input-large"){
    echo "
    <div class='control-group'>                     
      <label class='control-label' for='pregunta$num'>$texto</label>
      <div class='controls'>
        <input type='text' class='$tam' id='pregunta$num' name='pregunta$num' value='".$datos["pregunta$num"]."'>
      </div>
    </div>";
}


function creaPreguntaAuditoriaFecha($datos,$texto,$num,$tam="input-small"){
    echo "
    <div class='control-group'>                     
      <label class='control-label' for='pregunta$num'>$texto</label>
      <div class='controls'>
        <input type='text' class='$tam' id='pregunta$num' name='pregunta$num' value='".$datos["pregunta$num"]."'>
      </div>
    </div>";
}


function creaPreguntaAuditoriaDes($texto,$valor,$tam="input-medium"){
    echo "
    <div class='control-group'>                     
      <label class='control-label' for='norma'>$texto</label>
      <div class='controls'>
        <input type='text' class='$tam' id='norma' name='norma' disabled='disabled' value='$valor'>
      </div>
    </div>";
}

function creaTablaAuditoria(){
    echo '
    <table class="table table-striped table-bordered tablaAuditoria">
      <tbody>
        <tr>
          <th> Apartado Analizado </th>
          <th> Resultado </th>
          <th> Observaciones, comentarios y evidencias </th>
        </tr>';
}

function cierraTablaAuditoria(){
    echo '
      </tbody>
    </table>
    ';
}

function creaTablaVisitas(){
    echo '
    <table class="table table-striped table-bordered tablaVisitas">
      <tbody>
        <tr>
          <th colspan="2"> Control </th>
          <th> ¿Cumple? </th>
          <th> Observaciones </th>
        </tr>';
}

function cierraTablaVisitas(){
    echo '
      </tbody>
    </table>
    ';
}

function creaEncabezadoTablaVisitas($texto,$filas){
    echo '<tr>
            <td rowspan="'.$filas.'">'.$texto.'</td>
            </tr>';
}

function creaTituloTablaVisitas($texto){
    echo "
    <tr>
        <td colspan='2' class='justificado'>$texto</td>
    </tr>";
}

function creaPreguntaTablaVisitas($datos,$texto,$num){
    $sel=array('SI'=>'','NO'=>'','NP'=>'');
    $sel[$datos["pregunta$num"]]='selected="selected"';
    echo "
    <tr>
        <td class='justificado'>$texto</td>
        <td>
            <select name='pregunta$num' class='selectpicker show-tick span2'>
                <option ".$sel['SI'].">SI</option>
                <option ".$sel['NO'].">NO</option>
                <option ".$sel['NP'].">NP</option>
            </select>
        </td>
        <td><textarea name='observacion".$num."' id='observacion".$num."' class='areaTexto'>".$datos['observacion'.$num]."</textarea></td>
    </tr>";
}

function creaEncabezadoTablaAuditoria($texto){
    echo "
    <tr>
        <th colspan='3'>$texto</th>
    </tr>";
}

function creaPreguntaTablaAuditoria($datos,$texto,$num){
    $sel=array('EDA'=>'','NC'=>'','OBS'=>'');
    $sel[$datos["pregunta$num"]]='selected="selected"';
    echo "
    <tr>
        <td class='justificado'>$texto</td>
        <td>
            <select name='pregunta$num' class='selectpicker show-tick span2'>
                <option ".$sel['EDA'].">EDA</option>
                <option ".$sel['NC'].">NC</option>
                <option ".$sel['OBS'].">OBS</option>
            </select>
        </td>
        <td>
            <input type='text' name='observacion$num' class='span3' value='".$datos["observacion$num"]."'>
        </td>
    </tr>";
}



function compruebaAuditoriaInterna(){
    $codigo=false;

    //No pongo la conexión y el cierre a la base de datos porque están en la función padre
    $consulta=consultaBD("SELECT codigo FROM auditoriaInterna WHERE enviado='NO';");

    if(mysql_num_rows($consulta)==1){
        $consulta=mysql_fetch_assoc($consulta);
        $codigo=$consulta['codigo'];
    }

    return $codigo;
}


function datosAuditoriaSinEnviar($codigo){
    $datos=array();

    conexionBD();
    if($codigo==false){
        for($i=0;$i<104;$i++){
            $datos["pregunta$i"]="";
            $datos["observacion$i"]="";

        }
    }
    else{
        
        $consulta=consultaBD("SELECT cuestionario FROM auditorias_internas WHERE codigo='$codigo';");
        $consulta=mysql_fetch_assoc($consulta);
        $array=explode('&{}&',$consulta['cuestionario']);

        foreach($array as $indice=>$valor){//Recorro el array de campos
            $par=explode("=>",$valor);//Separo la clave del valor
            $datos[$par[0]]=$par[1];//Creo array asociativo (sustituyendo los _ por .)
        }
    }

    cierraBD();

    return $datos;
}

function datosAuditoria($codigo){
    $datos=array();

    conexionBD();

    $consulta=consultaBD("SELECT datos FROM auditoriaInterna WHERE codigo='$codigo';");
    $consulta=mysql_fetch_assoc($consulta);
    $array=explode('&{}&',$consulta['datos']);

    foreach($array as $indice=>$valor){//Recorro el array de campos
        $par=explode("=>",$valor);//Separo la clave del valor
        $datos[$par[0]]=$par[1];//Creo array asociativo (sustituyendo los _ por .)
    }

    cierraBD();

    return $datos;
}


function imprimeAuditoriasInternas(){
    conexionBD();

    $consulta=consultaBD("SELECT codigo, SUBSTRING_INDEX(datos,'&{}&',3) AS datos FROM auditoriaInterna WHERE enviado='SI';");
    $datos=mysql_fetch_assoc($consulta);
    while($datos!=false){
        $array=explode('&{}&',$datos['datos']);
        $empresa=explode('=>',$array[0]);
        $fecha=explode('=>',$array[2]);

        echo "
        <tr>
            <td> ".$empresa[1]." </td>
            <td> ".$fecha[1]." </td>
            <td class='centro'>
                <a href='detallesAuditoriaInterna.php?codigo=".$datos['codigo']."' class='btn btn-azul'><i class='icon-zoom-in'></i> Detalles</i></a>
                <a href='generaInformeAuditoria.php?codigo=".$datos['codigo']."' class='btn btn-primary'><i class='icon-download-alt'></i> Descargar Auditoría</i></a>
            </td>
            <td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
        </tr>";
        $datos=mysql_fetch_assoc($consulta);
    }
    cierraBD(); 
}


function actualizaAuditoriaInterna(){
    $res=true;

    $datos=arrayFormulario();

    $query="";
    for($i=0;$i<6;$i++){
        $query.="pregunta$i=>".$datos['pregunta'.$i]."&{}&";
    }

    for($i=6;$i<138;$i++){
        $query.="pregunta$i=>".$datos['pregunta'.$i]."&{}&";
        $query.="observacion$i=>".$datos['observacion'.$i]."&{}&";

    }
    $query.='conclusion=>'.$datos['conclusion'];


    conexionBD();

    $consulta=consultaBD("UPDATE auditoriaInterna SET datos='$query' WHERE codigo='".$datos['codigo']."';");    

    cierraBD();

    if(!$consulta){
        $res=false;
    }

    return $res;
}

function generaDatosGraficoAuditoriaInterna(){
    $datos=array('EDA'=>0,'NC'=>0,'OBS'=>0);
    
    $consulta="SELECT cuestionario FROM auditorias_internas WHERE finalizado='SI' AND codigoCliente IS NOT NULL;";
    if($_SESSION['tipoUsuario']=='TECNICO'){
        $consulta="SELECT cuestionario FROM auditorias_internas INNER JOIN clientes ON auditorias_internas.codigoCliente=clientes.codigo WHERE finalizado='SI' AND empleado='".$_SESSION['codigoU']."';";
    }

    conexionBD();

    $consulta=consultaBD($consulta);
    $reg=mysql_fetch_assoc($consulta);

    while($reg!=false){
        $array=explode('&{}&',$reg['cuestionario']);

        foreach($array as $indice=>$valor){//Recorro el array de campos
            $par=explode("=>",$valor);//Separo la clave del valor
            if($par[1]=='NC'){
                $datos['NC']++;
            }
            elseif($par[1]=='OBS'){
                $datos['OBS']++;
            }
            elseif($par[1]=='EDA'){
                $datos['EDA']++;
            }
        }

        $reg=mysql_fetch_assoc($consulta);
    }


    cierraBD();

    return $datos;
}
// Fin

//Parte de avisos de declaraciones pendientes de revisión o auditoría anual

function compruebaDeclaracionesPendientes(){
    conexionBD();

    if($_SESSION['tipoUsuario'] == 'ADMIN'){
        $consulta=consultaBD("SELECT revision, nuevaAuditoria, fecha FROM declaraciones WHERE numeroRegistro!='';");
    } 
    else{
        $director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],false,true);
        if($director['director'] == 'SI'){
            $consulta=consultaBD("SELECT revision, nuevaAuditoria, declaraciones.fecha FROM declaraciones LEFT JOIN clientes c ON declaraciones.codigoCliente=c.codigo  WHERE numeroRegistro!='' AND c.empleado IN (SELECT codigo FROM usuarios WHERE tipo LIKE '".$director['tipo']."')  ORDER BY razon_s;");
        } 
        else {
            $consulta=consultaBD("SELECT revision, nuevaAuditoria, declaraciones.fecha FROM declaraciones LEFT JOIN clientes c ON declaraciones.codigoCliente=c.codigo WHERE numeroRegistro!='' AND c.empleado LIKE ".$_SESSION['codigoU']."  ORDER BY razon_s;");
        }
    }
    
    cierraBD();

    $i=0;
    while($datos=mysql_fetch_assoc($consulta)){
        if(compruebaFechaDeclaracion($datos['fecha'],$datos['revision'],$datos['nuevaAuditoria'])){
            $i++;
        }
    }

    echo $i;
}

function compruebaFechaDeclaracion($fechaDeclaracion,$revision,$nuevaAuditoria){
    $res=false;

    $tiempo1=date_create_from_format('Y-m-d',$fechaDeclaracion);
    $tiempo2=date_create_from_format('Y-m-d',fechaBD());

    $diferencia=date_diff($tiempo1,$tiempo2);
    $meses=date_interval_format($diferencia,'%m');

    if($meses>=12 && $nuevaAuditoria=='NO'){
        $res=true;
    }
    elseif($meses>=6 && $revision=='NO'){
        $res=true;
    }

    return $res;
}

//FIn parte de avisos de declaraciones pendientes de revisión o auditoría anual

function recogerFormularioServicios($trabajo){
    $formulario=split('&{}&', $trabajo['formulario']);
    $preguntas=array();
    if(count($formulario) > 1){
        foreach ($formulario as $pregunta){
            $parts=split('=>', $pregunta);
            $preguntas[$parts[0]] = $parts[1];
        }
    } else {
        for($i=1;$i<77;$i++){
            $preguntas['pregunta'.$i] = '';
        }
    }

    return $preguntas;
    
}

function campoTextoFormulario($numero,$texto,$valor='',$clase='input-large',$readonly=false,$tipo=0){
    campoTexto('pregunta'.$numero,$texto,$valor['pregunta'.$numero],$clase,$readonly,$tipo);
}

function campoTextoFormularioConLogo($numero,$texto,$valor='',$logo='',$clase='input-large',$readonly=false,$tipo=0){
    echo "";
    campoTexto('pregunta'.$numero,$texto,$valor['pregunta'.$numero],$clase,$readonly,$tipo);
    echo "</div>";
}

function campoTextoTablaFormulario($numero,$valor='',$clase='input-large',$disabled=false){
    campoTextoTabla('pregunta'.$numero,$valor['pregunta'.$numero],$clase,$disabled);
}


function campoNumeroFormulario($numero,$texto,$valor){
    campoNumero('pregunta'.$numero,$texto,$valor['pregunta'.$numero]);
}

function campoFechaFormulario($numero,$texto,$valor=false,$solo=false,$disabled=false){//Modificada por Jose Luis el 15/09/2014 (añadido opción disabled).
    if(!$valor){
        //$valor=fecha();
    }
    else{
        $valor=compruebaValorCampo($valor,'pregunta'.$numero);
    }

    if($solo){
        campoTextoSolo('pregunta'.$numero,$valor,'input-small datepicker hasDatepicker',$disabled);
    }
    else{
        campoTexto('pregunta'.$numero,$texto,$valor,'input-small datepicker hasDatepicker',$disabled);
    }
}

function campoFechaTablaFormulario($numero,$valor=false){
    echo "<td>";
    campoFechaFormulario($numero,'',$valor,true);
    echo "</td>";
}

function campoRadioFormulario($numero,$texto,$valor='',$valorPorDefecto='NO',$textosCampos=array('Si','No'),$valoresCampos=array('SI','NO'),$salto=false){
    if($valor['pregunta'.$numero]!=''){
        $valor=compruebaValorCampo($valor,'pregunta'.$numero);
    }
    else{
        $valor=$valorPorDefecto;
    }
    echo "
    <div class='control-group'>                     
      <label class='control-label'>$texto:</label>
      <div class='controls nowrap'>";//MODIFICACIÓN 29/06/2015: añadida la clase "nowrap"

    for($i=0;$i<count($textosCampos);$i++){
        echo "<label class='radio inline'>
                <input type='radio' name='pregunta".$numero."' value='".$valoresCampos[$i]."'";
        if($valoresCampos[$i]==$valor){
            echo " checked='checked'";
        } 
        echo ">".$textosCampos[$i]."</label>";
        if($salto){
            echo "<br />";
        }
    }
    echo "
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}

function campoRadioFormularioConLogo($numero,$texto,$valor='',$logo='',$valorPorDefecto='NO',$textosCampos=array('Si','No'),$valoresCampos=array('SI','NO'),$salto=false){
     if($valor['pregunta'.$numero]!=''){
        $valor=compruebaValorCampo($valor,'pregunta'.$numero);
    }
    else{
        $valor=$valorPorDefecto;
    }

    echo "
    <div class='control-group'>                     
      <label class='control-label' style='background:url(../img/".$logo.") 95% bottom no-repeat;padding-right:50px;'>$texto:</label>
      <div class='controls nowrap'>";//MODIFICACIÓN 29/06/2015: añadida la clase "nowrap"

    for($i=0;$i<count($textosCampos);$i++){
        echo "<label class='radio inline'>
                <input type='radio' name='pregunta".$numero."' value='".$valoresCampos[$i]."'";
        if($valoresCampos[$i]==$valor){
            echo " checked='checked'";
        } 
        echo ">".$textosCampos[$i]."</label>";
        if($salto){
            echo "<br />";
        }
    }
    echo "
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}


function campoRadioTablaFormulario($numero,$valor='',$valorPorDefecto='NO',$textosCampos=array('Si','No'),$valoresCampos=array('SI','NO'),$salto=false){//MODIFICACIÓN 09/04/2015 NO RETROCOMPATIBLE: añadido el parámetro $valorPorDefecto | MODIFICACIÓN 05/01/2015: añadido el parámetro $salto | MODIFICACIÓN 21/07/2014 DE OFICINA: $valor antes que textos. $campos, $textosCampos y $valoresCampos arrays con misma longitud
     if($valor['pregunta'.$numero]!=''){
        $valor=compruebaValorCampo($valor,'pregunta'.$numero);
    }
    else{
        $valor=$valorPorDefecto;
    }

    echo "
      <td class='controls nowrap'>";//MODIFICACIÓN 29/06/2015: añadida la clase "nowrap"

    for($i=0;$i<count($textosCampos);$i++){
        echo "<label class='radio inline'>
                <input type='radio' name='pregunta".$numero."' value='".$valoresCampos[$i]."'";
        if($valoresCampos[$i]==$valor){
            echo " checked='checked'";
        } 
        echo ">".$textosCampos[$i]."</label>";
        if($salto){
            echo "<br />";
        }
    }
    echo "
      </td> <!-- /controls -->";
}

function areaTextoFormulario($numero,$texto,$valor='',$clase='areaTexto',$bloqueado=false){
    areaTexto('pregunta'.$numero,$texto,$valor['pregunta'.$numero],$clase,$bloqueado);
}

function areaTextoTablaFormulario($numero,$valor='',$clase='',$bloqueado=false){
    areaTextoTabla('pregunta'.$numero,$valor['pregunta'.$numero],$clase,$bloqueado);
}

function campoFirmaFormulario($numero,$texto,$valor){
    campoFirma($valor['pregunta'.$numero],'pregunta'.$numero,$texto);
}

function campoSelectFormulario($numero,$texto='',$valor=false,$tipo=0,$nombres=array('Si','NO'),$valores=array('SI','NO'),$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'"){
    if($tipo == 1){
        $clase=' selectpicker span2 show-tick';
    }
    campoSelect('pregunta'.$numero,$texto,$nombres,$valores,$valor['pregunta'.$numero],$clase,$busqueda,$tipo);
}

function campoSelectConsultaFormulario($numero,$texto='',$consulta='',$valor=false,$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'",$disabled='',$tipo=0,$conexion=true,$multiple=''){

    campoSelectConsulta('pregunta'.$numero,$texto,$consulta,$valor,$clase,$busqueda,$disabled,$tipo,$conexion,$multiple);
}

function campoFirma($datos,$nombreCampo='firma',$texto='Firma para documentos'){
    echo "
    <div id='contenedor-$nombreCampo'>
        <h3 class='apartadoFormulario'>$texto</h3>";

    $datos=compruebaValorCampo($datos,$nombreCampo);
    echo "
        <!-- Área de firma -->
        <div class='centro ".$nombreCampo."'>";
        campoOculto($datos,$nombreCampo,'','hide output');
        echo "
            <div class='clearButton'><a href='#clear' class='btn btn-danger noAjax'><i class='icon-trash'></i> Borrar</a></div>
            <canvas class='pad' width='1000' height='500'></canvas>
        </div>
        <!-- Fin ára de firma -->
        <br /><br />
    </div>";
}

function campoFechaBlanco($nombreCampo,$texto,$valor=false,$solo=false,$disabled=false){//Modificada por Jose Luis el 15/09/2014 (añadido opción disabled).
    if(!$valor){
        $valor='';
    }
    else{
        $valor=compruebaValorCampo($valor,$nombreCampo);
        $valor=formateaFechaWeb($valor);
    }

    if($solo){
        campoTextoSolo($nombreCampo,$valor,'input-small datepicker hasDatepicker',$disabled);
    }
    else{
        campoTexto($nombreCampo,$texto,$valor,'input-small datepicker hasDatepicker',$disabled);
    }
}

function campoFechaBlancoTabla($nombreCampo,$valor=false){
    echo "<td>";
    campoFechaBlanco($nombreCampo,'',$valor,true);
    echo "</td>";
}

function campoHoraBlanco($nombreCampo,$texto,$valor=false,$tipo=0,$disabled=false){//MODIFICACIÓN (NUEVA FUNCIÓN) 25/03/2015
    if(!$valor){
        $valor='';
    }
    else{
        $valor=compruebaValorCampo($valor,$nombreCampo);
        $valor=formateaHoraWeb($valor);
        if($valor=='00:00'){
            $valor='';
        }
    }

    if($tipo==0){
        campoTexto($nombreCampo,$texto,$valor,'input-mini pagination-right',$disabled);
    }
    elseif($tipo==1){
        campoTextoTabla($nombreCampo,$valor,'input-mini pagination-right',$disabled);
    }
}

function eliminaClientes($tabla){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();
    for($i=0;isset($datos['codigo'.$i]);$i++){
        $usuario=consultaBD('SELECT * FROM usuarios_clientes WHERE codigoCliente='.$datos['codigo'.$i],false,true);
        $consulta=consultaBD("DELETE FROM usuarios WHERE codigo='".$usuario['codigoUsuario']."';");
        eliminaEmpleadosDeCliente($datos['codigo'.$i]);
        $consulta=consultaBD("DELETE FROM $tabla WHERE codigo='".$datos['codigo'.$i]."';");
        if(!$consulta){
            $res=false;
            echo mysql_error();
        }
    }
    cierraBD();

    return $res;
}

function eliminaEmpleadosDeCliente($codigo){
    $empleados=consultaBD('SELECT * FROM empleados WHERE codigoCliente='.$codigo);
    while($empleado=mysql_fetch_assoc($empleados)){
        $usuario=consultaBD('SELECT * FROM usuarios_empleados WHERE codigoEmpleado='.$empleado['codigo'],false,true);
        $consulta=consultaBD("DELETE FROM usuarios WHERE codigo=".$usuario['codigoUsuario']);
        if($empleado['ficheroFoto']!='NO' && $empleado['ficheroFoto']!=''){
            unlink('../documentacion/fotos/'.$empleado['ficheroFoto']);
        }
        $consulta=consultaBD("DELETE FROM empleados WHERE codigo=".$empleado['codigo']);
    }
}

function eliminaEmpleado($tabla){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();
    for($i=0;isset($datos['codigo'.$i]);$i++){
        $usuario=consultaBD('SELECT * FROM usuarios_empleados WHERE codigoEmpleado='.$datos['codigo'.$i],false,true);
        $consulta=consultaBD("DELETE FROM usuarios WHERE codigo='".$usuario['codigoUsuario']."';");
        $empleado=consultaBD("SELECT * FROM empleados WHERE codigo=".$datos['codigo'.$i],false,true);
        if($empleado['ficheroFoto']!='NO' && $empleado['ficheroFoto']!=''){
            unlink('../documentacion/fotos/'.$empleado['ficheroFoto']);
        }
        $consulta=consultaBD("DELETE FROM $tabla WHERE codigo='".$datos['codigo'.$i]."';");
        if(!$consulta){
            $res=false;
            echo mysql_error();
        }
    }
    cierraBD();

    return $res;
}

function obtenerCodigoCliente($conexion){
    $cliente=consultaBD('SELECT * FROM usuarios_clientes WHERE codigoUsuario='.$_SESSION['codigoU'],$conexion,true);
    return $cliente['codigoCliente'];
}

function obtenerCodigoEmpleado($conexion){
    $cliente=consultaBD('SELECT * FROM usuarios_empleados WHERE codigoUsuario='.$_SESSION['codigoU'],$conexion,true);
    return $cliente['codigoEmpleado'];
}

function cajaFiltroEjercicio(){
    defineFiltroEjercicio();
    if(!isset($_SESSION['ejercicio'])){
        $anio=date('Y');
    } else {
        $anio=$_SESSION['ejercicio'];
    }
    echo '
    <ul class="nav pull-right cajaUsuario" id="cajaFiltroEjercicio" data-date="01-01-'.$anio.'" data-date-format="dd-mm-yyyy">
        <li class="dropdown"  id="target-5"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-calendar"></i> Ejercicio: ';
            
            campoFiltroEjercicio();

    echo '</a>
        </li>
    </ul>';
}

function campoFiltroEjercicio(){
    $valor=obtieneEjercicioFiltro();
    campoTexto('filtroEjercicio','',$valor,'filtroEjercicio',true,2);
}

function obtieneEjercicioFiltro(){
    $res='Todos';

    if(isset($_SESSION['ejercicio'])){
        $res=$_SESSION['ejercicio'];
    }

    return $res;
}

function obtieneEjercicioParaWhere(){//Similar a obtieneEjercicioFiltro(). Usar cuando se necesite poner el resultado directamente como condición para un campo fecha en un WHERE (en cuyo caso 'Todos' no valdría)
    $res=obtieneEjercicioFiltro();
    
    if($res=='Todos'){
        $res=date('Y');
    }

    return $res;
}

function defineFiltroEjercicio(){
    if(isset($_GET['ejercicio']) && $_GET['ejercicio']=='Todos'){
        unset($_SESSION['ejercicio']);
    }
    elseif(isset($_GET['ejercicio'])){
        $_SESSION['ejercicio']=$_GET['ejercicio'];
    }
}

function defineWhereEjercicio($where,$campos){
    $res=$where;
    if(isset($_SESSION['ejercicio'])){
        if($where==''){
            $res='WHERE';
        } else {
            $res=$where.' AND';
        }
        if(is_array($campos)){
            $primer=true;
            $res.='(';
            foreach ($campos as $campo) {
                if($primer){
                    $res.=' YEAR('.$campo.')="'.$_SESSION['ejercicio'].'"';
                } else {
                    $res.=' OR YEAR('.$campo.')="'.$_SESSION['ejercicio'].'"';
                }
                $primer=false;
            }
            $res.=')';
        } else {
            $res.=' YEAR('.$campos.')="'.$_SESSION['ejercicio'].'"';
        }
    }
    $res=defineWhereEmpleado($res); 
    return $res;
}

function defineWhereEmpleado($where,$conexion=true){
    $res=$where;
    if($_SESSION['tipoUsuario'] == 'TECNICO') {
        $director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],$conexion, true);
    } elseif($_SESSION['tipoUsuario'] == 'COMERCIAL') {
        $director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],$conexion, true);
    }
    return $res;
}

function defineWhereEmpleadoTarea($conexion=true){
    $res='WHERE 1=1';
    $tipoUsuario=$_SESSION['tipoUsuario'];

    if($tipoUsuario=='ADMIN' && isset($_POST['codigoUsuarioFiltro'])){
        $res=" WHERE tareas.codigoUsuario=".$_POST['codigoUsuarioFiltro'];
    } 
    elseif($tipoUsuario=='TECNICO') {
        
        $director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],$conexion, true);
        $res=" WHERE tareas.codigoUsuario=".$_SESSION['codigoU'];

    } 
    elseif($tipoUsuario=='COMERCIAL') {
        
        $director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],$conexion, true);
        $res=" WHERE (clientes.comercial='".$_SESSION['codigoU']."' OR tareas.codigoUsuario=".$_SESSION['codigoU'].")";

    } 
    elseif($tipoUsuario=='MEDICO') {
        
        $director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],$conexion, true);
        $res=" WHERE tareas.codigoUsuario=".$_SESSION['codigoU'];

    }
    
    return $res;
}


function obtieneNombreCliente($codigoCliente,$enlace=true){
    $res='';

    $datos=consultaBD("SELECT EMPNOMBRE AS nombre FROM clientes WHERE codigo='$codigoCliente';",true,true);

    $res=$datos['nombre'];

    if($enlace){
        $res="<a href='../clientes/gestion.php?codigo=".$codigoCliente."' target='_blank' class='noAjax'>".$res."</a>";
    }

    return $res;
}


function formateaReferenciaContrato($cliente,$datos){
    $cp=substr($cliente['EMPCP'], 0,2);
    $idCliente=substr(str_pad($cliente['EMPID'], 3,0,STR_PAD_LEFT),-3);
    $anio=explode('-', $datos['fechaInicio']);
    $anio=substr($anio[0],-2);
    $idContrato=str_pad($datos['codigoInterno'], 3,0,STR_PAD_LEFT);
    return $cp.'-'.$idCliente.'-'.$anio.'-'.$idContrato;
}


//Parte común de facturas y abonos

function insertaVencimientosFactura($datos,$codigoFactura){
    //Los registros de vencimientos_facturas no se pueden eliminar al actualizar de forma normal, pues están vinculados con las remesas
    $res=true;

    if(isset($datos['codigoFacturaVencimientos'])){
        $codigoFactura=$datos['codigoFacturaVencimientos'];
    }

    $pagada=true;//Para establecer, si todos los vencimientos tienen el estado "PAGADO", la factura como pagada

    for($i=0;isset($datos["codigoVencimiento$i"]) || isset($datos["codigoFormaPago$i"]);$i++){
        if(isset($datos['codigoFormaPago'.$i])){
            $codigoFormaPago=$datos['codigoFormaPago'.$i];
            $fechaVencimiento=$datos['fechaVencimiento'.$i];
            $importeVencimiento=formateaNumeroWeb($datos['importeVencimiento'.$i],true);
            
            $codigoEstado=$datos['codigoEstadoRecibo'.$i];
            if($codigoEstado==''){
                $codigoEstado='NULL';
                $pagada=false;
            }
            elseif($codigoEstado!=2){
                $pagada=false;
            }

            $concepto=$datos['conceptoVencimiento'.$i];
            $fechaDevolucion=$datos['fechaDevolucion'.$i];
            $gastosDevolucion=formateaNumeroWeb($datos['gastosDevolucion'.$i],true);
            $llamadaAsesoria=compruebaValorCheckVencimiento('llamadaAsesoria'.$i,$datos);
            $llamadaEmpresa=compruebaValorCheckVencimiento('llamadaEmpresa'.$i,$datos);
            
            $observaciones='';
            if(isset($datos['observacionesVencimiento'.$i])){
                $observaciones=$datos['observacionesVencimiento'.$i];
            }
        }
       
        $suspendido='';
        if(isset($datos['codigoVencimiento'.$i]) && isset($datos['codigoFormaPago'.$i])){//Actualización
            $vencimiento=consultaBD('SELECT * FROM vencimientos_facturas WHERE codigo='.$datos['codigoVencimiento'.$i],false,true);
            if($vencimiento['codigoEstadoRecibo']!=2 && $codigoEstado==2){
                $suspendido='NO';
            } 
            elseif($vencimiento['codigoEstadoRecibo']!=5 && $codigoEstado==5){
                $suspendido='SI';
            }
            
            $res=$res && consultaBD("UPDATE vencimientos_facturas SET codigoFormaPago=$codigoFormaPago, fechaVencimiento='$fechaVencimiento', importe=$importeVencimiento, codigoEstadoRecibo=$codigoEstado, concepto='$concepto', observaciones='$observaciones' WHERE codigo=".$datos['codigoVencimiento'.$i].";");
        }
        elseif(!isset($datos['codigoVencimiento'.$i]) && isset($datos['codigoFormaPago'.$i])){//Inserción
            $res=$res && consultaBD("INSERT INTO vencimientos_facturas VALUES(NULL,$codigoFactura,$codigoFormaPago,'$fechaVencimiento','$fechaDevolucion','$gastosDevolucion','$importeVencimiento','$concepto','$llamadaAsesoria','$llamadaEmpresa','','NO',$codigoEstado,'$observaciones');");
            
            if($codigoEstado==2){
                $suspendido='NO';
            } 
            elseif($codigoEstado==5){
                $suspendido='SI';
            }
        }
        else{//Eliminación
            $res=$res && consultaBD("DELETE FROM vencimientos_facturas WHERE codigo='".$datos['codigoVencimiento'.$i]."';");
        }

        if($suspendido!=''){
            $contrato=consultaBD('SELECT codigoContrato 
                                  FROM contratos_en_facturas 
                                  WHERE codigoFactura='.$codigoFactura,false,true);
                                  
            $res=cambiaValorCampo('suspendido',$suspendido,$contrato['codigoContrato'],'contratos',false);
        }
    }



    if($pagada && $datos['tipoFactura']=='PROFORMA'){
        $res=$res && consultaBD("UPDATE facturas SET tipoFactura='NORMAL' WHERE codigo='$codigoFactura';");
    }


    return $res;
}

function compruebaValorCheckVencimiento($indice,$datos){
    $res='NO';

    if(isset($datos[$indice])){
        $res=$datos[$indice];
    }

    return $res;
}


function obtieneWhereEjercicioEstadisticas($campo){
    $res='';

    if(isset($_SESSION['ejercicio'])){//Si se ha activado el filtro de ejercicios...
        $anio=$_SESSION['ejercicio'];

        $res=" AND ((YEAR($campo)=$anio) OR (YEAR($campo) IS NULL) OR (YEAR($campo)=0))";
    }

    return $res;
}

/*
    La siguiente función elimina los div creados por el textarea vitaminado, ya que crea un montón de ellos que provocan saltos de línea no deseados.
    Después, sustituye los saltos de línea por <br /> y elimina los <br /> que estén solos en una línea (para no crear saltos innecesarios)
*/
function formateaObservacionesFactura($observaciones){
    $res=str_replace('<div>','',$observaciones);
    $res=str_replace('</div>','',$res);
    $res=nl2br($res);
    $res=preg_replace("/(^|\t|\s+)\<br \/\>/",'',$res);

    //Parte de eliminación de saltos de línea internos en el código de etiquetas (<span style="border-collapse:<br />)

    $ocurrencias=array();
    preg_match_all('/style="[^"]+<br \/>/',$res,$ocurrencias);

    foreach($ocurrencias[0] as $ocurrencia){
        
        $nuevaCadena=str_replace('<br />','',$ocurrencia);

        $res=str_replace($ocurrencia,$nuevaCadena,$res);
    }

    //Fin parte de eliminación de saltos de línea internos en el código de etiquetas

    return $res;
}

function eliminaItem($tabla,$campo){
    $res=true;
    $datos=arrayFormulario();
    conexionBD();
    for($i=0;isset($datos['codigo'.$i]);$i++){
        $usuario=consultaBD("SELECT * FROM usuarios_".$tabla." WHERE ".$campo."='".$datos['codigo'.$i]."';",false,true);
        $consulta=consultaBD("DELETE FROM usuarios WHERE codigo='".$usuario['codigoUsuario']."';");
        $consulta=consultaBD("DELETE FROM ".$tabla." WHERE codigo='".$datos['codigo'.$i]."';");
        if(!$consulta){
            $res=false;
            echo mysql_error();
        }
    }
    cierraBD();

    return $res;
}

function campoCNAE($nombreCampo,$texto,$datos,$clase='selectpicker show-tick span3',$disabled=''){
    $listado=arrayCNAE();
    $nombres=array();
    $valores=array();
    foreach ($listado as $key => $value) {
        array_push($nombres, $key.' - '.$value);
        array_push($valores, $key);
    }
    campoSelect($nombreCampo,$texto,$nombres,$valores,$datos,$clase,"data-live-search='true'",$disabled);
}

function campoCNAEFormulario($numero,$texto,$datos,$disabled=''){
    campoCNAE('pregunta'.$numero,$texto,$datos,'selectpicker show-tick span3',$disabled);
}

function defineCNAE($codigo){
    $nombres=arrayCNAE();
    if($codigo=='' || $codigo='NULL'){
        $nombre='';
    } else {
        $nombre=$nombres[$codigo];
    }
    return $nombre;
}

function arrayCNAE(){
    return array('01' => 'Agricultura, ganadería, caza y servicios relacionados con las mismas', '011' => 'Cultivos no perennes', '0111' => 'Cultivo de cereales (excepto arroz), leguminosas y semillas oleaginosas', '0112' => 'Cultivo de arroz', '0113' => 'Cultivo de hortalizas, raíces y tubérculos', '0114' => 'Cultivo de caña de azúcar', '0115' => 'Cultivo de tabaco', '0116' => 'Cultivo de plantas para fibras textiles', '0119' => 'Otros cultivos no perennes', '012' => 'Cultivos perennes', '0121' => 'Cultivo de la vid', '0122' => 'Cultivo de frutos tropicales y subtropicales', '0123' => 'Cultivo de cítricos', '0124' => 'Cultivo de frutos con hueso y pepitas', '0125' => 'Cultivo de otros árboles y arbustos frutales y frutos secos', '0126' => 'Cultivo de frutos oleaginosos', '0127' => 'Cultivo de plantas para bebidas', '0128' => 'Cultivo de especias, plantas aromáticas, medicinales y farmacéuticas', '0129' => 'Otros cultivos perennes', '013' => 'Propagación de plantas', '0130' => 'Propagación de plantas', '014' => 'Producción ganadera', '0141' => 'Explotación de ganado bovino para la producción de leche', '0142' => 'Explotación de otro ganado bovino y búfalos', '0143' => 'Explotación de caballos y otros equinos', '0144' => 'Explotación de camellos y otros camélidos', '0145' => 'Explotación de ganado ovino y caprino', '0146' => 'Explotación de ganado porcino', '0147' => 'Avicultura', '0149' => 'Otras explotaciones de ganado', '015' => 'Producción agrícola combinada con la producción ganadera', '0150' => 'Producción agrícola combinada con la producción ganadera', '016' => 'Actividades de apoyo a la agricultura, a la ganadería y de preparación posterior a la cosecha', '0161' => 'Actividades de apoyo a la agricultura', '0162' => 'Actividades de apoyo a la ganadería', '0163' => 'Actividades de preparación posterior a la cosecha', '0164' => 'Tratamiento de semillas para reproducción', '017' => 'Caza, captura de animales y servicios relacionados con las mismas', '0170' => 'Caza, captura de animales y servicios relacionados con las mismas', '02' => 'Silvicultura y explotación forestal', '021' => 'Silvicultura y otras actividades forestales', '0210' => 'Silvicultura y otras actividades forestales', '022' => 'Explotación de la madera', '0220' => 'Explotación de la madera', '023' => 'Recolección de productos silvestres, excepto madera', '0230' => 'Recolección de productos silvestres, excepto madera', '024' => 'Servicios de apoyo a la silvicultura', '0240' => 'Servicios de apoyo a la silvicultura', '03' => 'Pesca y acuicultura', '031' => 'Pesca', '0311' => 'Pesca marina', '0312' => 'Pesca en agua dulce', '032' => 'Acuicultura', '0321' => 'Acuicultura marina', '0322' => 'Acuicultura en agua dulce', '05' => 'Extracción de antracita, hulla y lignito', '051' => 'Extracción de antracita y hulla', '0510' => 'Extracción de antracita y hulla', '052' => 'Extracción de lignito', '0520' => 'Extracción de lignito', '06' => 'Extracción de crudo de petróleo y gas natural', '061' => 'Extracción de crudo de petróleo', '0610' => 'Extracción de crudo de petróleo', '062' => 'Extracción de gas natural', '0620' => 'Extracción de gas natural', '07' => 'Extracción de minerales metálicos', '071' => 'Extracción de minerales de hierro', '0710' => 'Extracción de minerales de hierro', '072' => 'Extracción de minerales metálicos no férreos', '0721' => 'Extracción de minerales de uranio y torio', '0729' => 'Extracción de otros minerales metálicos no férreos', '08' => 'Otras industrias extractivas', '081' => 'Extracción de piedra, arena y arcilla', '0811' => 'Extracción de piedra ornamental y para la construcción, piedra caliza, yeso, creta y pizarra', '0812' => 'Extracción de gravas y arenas; extracción de arcilla y caolín', '089' => 'Industrias extractivas n.c.o.p.', '0891' => 'Extracción de minerales para productos químicos y fertilizantes', '0892' => 'Extracción de turba', '0893' => 'Extracción de sal', '0899' => 'Otras industrias extractivas n.c.o.p.', '09' => 'Actividades de apoyo a las industrias extractivas', '091' => 'Actividades de apoyo a la extracción de petróleo y gas natural', '0910' => 'Actividades de apoyo a la extracción de petróleo y gas natural', '099' => 'Actividades de apoyo a otras industrias extractivas', '0990' => 'Actividades de apoyo a otras industrias extractivas', '10' => 'Industria de la alimentación', '101' => 'Procesado y conservación de carne y elaboración de productos cárnicos', '1011' => 'Procesado y conservación de carne', '1012' => 'Procesado y conservación de volatería', '1013' => 'Elaboración de productos cárnicos y de volatería', '102' => 'Procesado y conservación de pescados, crustáceos y moluscos', '1021' => 'Procesado de pescados, crustáceos y moluscos', '1022' => 'Fabricación de conservas de pescado', '103' => 'Procesado y conservación de frutas y hortalizas', '1031' => 'Procesado y conservación de patatas', '1032' => 'Elaboración de zumos de frutas y hortalizas', '1039' => 'Otro procesado y conservación de frutas y hortalizas', '104' => 'Fabricación de aceites y grasas vegetales y animales', '1042' => 'Fabricación de margarina y grasas comestibles similares', '1043' => 'Fabricación de aceite de oliva', '1044' => 'Fabricación de otros aceites y grasas', '105' => 'Fabricación de productos lácteos', '1052' => 'Elaboración de helados', '1053' => 'Fabricación de quesos', '1054' => 'Preparación de leche y otros productos lácteos', '106' => 'Fabricación de productos de molinería, almidones y productos amiláceos', '1061' => 'Fabricación de productos de molinería', '1062' => 'Fabricación de almidones y productos amiláceos', '107' => 'Fabricación de productos de panadería y pastas alimenticias', '1071' => 'Fabricación de pan y de productos frescos de panadería y pastelería', '1072' => 'Fabricación de galletas y productos de panadería y pastelería de larga duración', '1073' => 'Fabricación de pastas alimenticias, cuscús y productos similares', '108' => 'Fabricación de otros productos alimenticios', '1081' => 'Fabricación de azúcar', '1082' => 'Fabricación de cacao, chocolate y productos de confitería', '1083' => 'Elaboración de café, té e infusiones', '1084' => 'Elaboración de especias, salsas y condimentos', '1085' => 'Elaboración de platos y comidas preparados', '1086' => 'Elaboración de preparados alimenticios homogeneizados y alimentos dietéticos', '1089' => 'Elaboración de otros productos alimenticios n.c.o.p.', '109' => 'Fabricación de productos para la alimentación animal', '1091' => 'Fabricación de productos para la alimentación de animales de granja', '1092' => 'Fabricación de productos para la alimentación de animales de compañía', '11' => 'Fabricación de bebidas', '110' => 'Fabricación de bebidas', '1101' => 'Destilación, rectificación y mezcla de bebidas alcohólicas', '1102' => 'Elaboración de vinos', '1103' => 'Elaboración de sidra y otras bebidas fermentadas a partir de frutas', '1104' => 'Elaboración de otras bebidas no destiladas, procedentes de la fermentación', '1105' => 'Fabricación de cerveza', '1106' => 'Fabricación de malta', '1107' => 'Fabricación de bebidas no alcohólicas; producción de aguas minerales y otras aguas embotelladas', '12' => 'Industria del tabaco', '120' => 'Industria del tabaco', '1200' => 'Industria del tabaco', '13' => 'Industria textil', '131' => 'Preparación e hilado de fibras textiles', '1310' => 'Preparación e hilado de fibras textiles', '132' => 'Fabricación de tejidos textiles', '1320' => 'Fabricación de tejidos textiles', '133' => 'Acabado de textiles', '1330' => 'Acabado de textiles', '139' => 'Fabricación de otros productos textiles', '1391' => 'Fabricación de tejidos de punto', '1392' => 'Fabricación de artículos confeccionados con textiles, excepto prendas de vestir', '1393' => 'Fabricación de alfombras y moquetas', '1394' => 'Fabricación de cuerdas, cordeles, bramantes y redes', '1395' => 'Fabricación de telas no tejidas y artículos confeccionados con ellas, excepto prendas de vestir', '1396' => 'Fabricación de otros productos textiles de uso técnico e industrial', '1399' => 'Fabricación de otros productos textiles n.c.o.p.', '14' => 'Confección de prendas de vestir', '141' => 'Confección de prendas de vestir, excepto de peletería', '1411' => 'Confección de prendas de vestir de cuero', '1412' => 'Confección de ropa de trabajo', '1413' => 'Confección de otras prendas de vestir exteriores', '1414' => 'Confección de ropa interior', '1419' => 'Confección de otras prendas de vestir y accesorios', '142' => 'Fabricación de artículos de peletería', '1420' => 'Fabricación de artículos de peletería', '143' => 'Confección de prendas de vestir de punto', '1431' => 'Confección de calcetería', '1439' => 'Confección de otras prendas de vestir de punto', '15' => 'Industria del cuero y del calzado', '151' => 'Preparación, curtido y acabado del cuero; fabricación de artículos de marroquinería, viaje y de guarnicionería y talabartería; preparación y teñido de pieles', '1511' => 'Preparación, curtido y acabado del cuero; preparación y teñido de pieles', '1512' => 'Fabricación de artículos de marroquinería, viaje y de guarnicionería y talabartería', '152' => 'Fabricación de calzado', '1520' => 'Fabricación de calzado', '16' => 'Industria de la madera y del corcho, excepto muebles; cestería y espartería', '161' => 'Aserrado y cepillado de la madera', '1610' => 'Aserrado y cepillado de la madera', '162' => 'Fabricación de productos de madera, corcho, cestería y espartería', '1621' => 'Fabricación de chapas y tableros de madera', '1622' => 'Fabricación de suelos de madera ensamblados', '1623' => 'Fabricación de otras estructuras de madera y piezas de carpintería y ebanistería para la construcción', '1624' => 'Fabricación de envases y embalajes de madera', '1629' => 'Fabricación de otros productos de madera; artículos de corcho, cestería y espartería', '17' => 'Industria del papel', '171' => 'Fabricación de pasta papelera, papel y cartón', '1711' => 'Fabricación de pasta papelera', '1712' => 'Fabricación de papel y cartón', '172' => 'Fabricación de artículos de papel y de cartón', '1721' => 'Fabricación de papel y cartón ondulados; fabricación de envases y embalajes de papel y cartón', '1722' => 'Fabricación de artículos de papel y cartón para uso doméstico, sanitario e higiénico', '1723' => 'Fabricación de artículos de papelería', '1724' => 'Fabricación de papeles pintados', '1729' => 'Fabricación de otros artículos de papel y cartón', '18' => 'Artes gráficas y reproducción de soportes grabados', '181' => 'Artes gráficas y servicios relacionados con las mismas', '1811' => 'Artes gráficas y servicios relacionados con las mismas', '1812' => 'Otras actividades de impresión y artes gráficas', '1813' => 'Servicios de preimpresión y preparación de soportes', '1814' => 'Encuadernación y servicios relacionados con la misma', '182' => 'Reproducción de soportes grabados', '1820' => 'Reproducción de soportes grabados', '19' => 'Coquerías y refino de petróleo', '191' => 'Coquerías', '1910' => 'Coquerías', '192' => 'Refino de petróleo', '1920' => 'Refino de petróleo', '20' => 'Industria química', '201' => 'Fabricación de productos químicos básicos, compuestos nitrogenados, fertilizantes, plásticos y caucho sintético en formas primarias', '2011' => 'Fabricación de gases industriales', '2012' => 'Fabricación de colorantes y pigmentos', '2013' => 'Fabricación de otros productos básicos de química inorgánica', '2014' => 'Fabricación de otros productos básicos de química orgánica', '2015' => 'Fabricación de fertilizantes y compuestos nitrogenados', '2016' => 'Fabricación de plásticos en formas primarias', '2017' => 'Fabricación de caucho sintético en formas primarias', '202' => 'Fabricación de pesticidas y otros productos agroquímicos', '2020' => 'Fabricación de pesticidas y otros productos agroquímicos', '203' => 'Fabricación de pinturas, barnices y revestimientos similares; tintas de imprenta y masillas', '2030' => 'Fabricación de pinturas, barnices y revestimientos similares; tintas de imprenta y masillas', '204' => 'Fabricación de jabones, detergentes y otros artículos de limpieza y abrillantamiento; fabricación de perfumes y cosméticos', '2041' => 'Fabricación de jabones, detergentes y otros artículos de limpieza y abrillantamiento', '2042' => 'Fabricación de perfumes y cosméticos', '205' => 'Fabricación de otros productos químicos', '2051' => 'Fabricación de explosivos', '2052' => 'Fabricación de colas', '2053' => 'Fabricación de aceites esenciales', '2059' => 'Fabricación de otros productos químicos n.c.o.p.', '206' => 'Fabricación de fibras artificiales y sintéticas', '2060' => 'Fabricación de fibras artificiales y sintéticas', '21' => 'Fabricación de productos farmacéuticos', '211' => 'Fabricación de productos farmacéuticos de base', '2110' => 'Fabricación de productos farmacéuticos de base', '212' => 'Fabricación de especialidades farmacéuticas', '2120' => 'Fabricación de especialidades farmacéuticas', '22' => 'Fabricación de productos de caucho y plásticos', '221' => 'Fabricación de productos de caucho', '2211' => 'Fabricación de neumáticos y cámaras de caucho; reconstrucción y recauchutado de neumáticos', '2219' => 'Fabricación de otros productos de caucho', '222' => 'Fabricación de productos de plástico', '2221' => 'Fabricación de placas, hojas, tubos y perfiles de plástico', '2222' => 'Fabricación de envases y embalajes de plástico', '2223' => 'Fabricación de productos de plástico para la construcción', '2229' => 'Fabricación de otros productos de plástico', '23' => 'Fabricación de otros productos minerales no metálicos', '231' => 'Fabricación de vidrio y productos de vidrio', '2311' => 'Fabricación de vidrio plano', '2312' => 'Manipulado y transformación de vidrio plano', '2313' => 'Fabricación de vidrio hueco', '2314' => 'Fabricación de fibra de vidrio', '2319' => 'Fabricación y manipulado de otro vidrio, incluido el vidrio técnico', '232' => 'Fabricación de productos cerámicos refractarios', '2320' => 'Fabricación de productos cerámicos refractarios', '233' => 'Fabricación de productos cerámicos para la construcción', '2331' => 'Fabricación de azulejos y baldosas de cerámica', '2332' => 'Fabricación de ladrillos, tejas y productos de tierras cocidas para la construcción', '234' => 'Fabricación de otros productos cerámicos', '2341' => 'Fabricación de artículos cerámicos de uso doméstico y ornamental', '2342' => 'Fabricación de aparatos sanitarios cerámicos', '2343' => 'Fabricación de aisladores y piezas aislantes de material cerámico', '2344' => 'Fabricación de otros productos cerámicos de uso técnico', '2349' => 'Fabricación de otros productos cerámicos', '235' => 'Fabricación de cemento, cal y yeso', '2351' => 'Fabricación de cemento', '2352' => 'Fabricación de cal y yeso', '236' => 'Fabricación de elementos de hormigón, cemento y yeso', '2361' => 'Fabricación de elementos de hormigón para la construcción', '2362' => 'Fabricación de elementos de yeso para la construcción', '2363' => 'Fabricación de hormigón fresco', '2364' => 'Fabricación de mortero', '2365' => 'Fabricación de fibrocemento', '2369' => 'Fabricación de otros productos de hormigón, yeso y cemento', '237' => 'Corte, tallado y acabado de la piedra', '2370' => 'Corte, tallado y acabado de la piedra', '239' => 'Fabricación de productos abrasivos y productos minerales no metálicos n.c.o.p.', '2391' => 'Fabricación de productos abrasivos', '2399' => 'Fabricación de otros productos minerales no metálicos n.c.o.p.', '24' => 'Metalurgia; fabricación de productos de hierro, acero y ferroaleaciones', '241' => 'Fabricación de productos básicos de hierro, acero y ferroaleaciones', '2410' => 'Fabricación de productos básicos de hierro, acero y ferroaleaciones', '242' => 'Fabricación de tubos, tuberías, perfiles huecos y sus accesorios, de acero', '2420' => 'Fabricación de tubos, tuberías, perfiles huecos y sus accesorios, de acero', '243' => 'Fabricación de otros productos de primera transformación del acero', '2431' => 'Estirado en frío', '2432' => 'Laminación en frío', '2433' => 'Producción de perfiles en frío por conformación con plegado', '2434' => 'Trefilado en frío', '244' => 'Producción de metales preciosos y de otros metales no férreos', '2441' => 'Producción de metales preciosos', '2442' => 'Producción de aluminio', '2443' => 'Producción de plomo, zinc y estaño', '2444' => 'Producción de cobre', '2445' => 'Producción de otros metales no férreos', '2446' => 'Procesamiento de combustibles nucleares', '245' => 'Fundición de metales', '2451' => 'Fundición de hierro', '2452' => 'Fundición de acero', '2453' => 'Fundición de metales ligeros', '2454' => 'Fundición de otros metales no férreos', '25' => 'Fabricación de productos metálicos, excepto maquinaria y equipo', '251' => 'Fabricación de elementos metálicos para la construcción', '2511' => 'Fabricación de estructuras metálicas y sus componentes', '2512' => 'Fabricación de carpintería metálica', '252' => 'Fabricación de cisternas, grandes depósitos y contenedores de metal', '2521' => 'Fabricación de radiadores y calderas para calefacción central', '2529' => 'Fabricación de otras cisternas, grandes depósitos y contenedores de metal', '253' => 'Fabricación de generadores de vapor, excepto calderas de calefacción central', '2530' => 'Fabricación de generadores de vapor, excepto calderas de calefacción central', '254' => 'Fabricación de armas y municiones', '2540' => 'Fabricación de armas y municiones', '255' => 'Forja, estampación y embutición de metales; metalurgia de polvos', '2550' => 'Forja, estampación y embutición de metales; metalurgia de polvos', '256' => 'Tratamiento y revestimiento de metales; ingeniería mecánica por cuenta de terceros', '2561' => 'Tratamiento y revestimiento de metales', '2562' => 'Ingeniería mecánica por cuenta de terceros', '257' => 'Fabricación de artículos de cuchillería y cubertería, herramientas y ferretería', '2571' => 'Fabricación de artículos de cuchillería y cubertería', '2572' => 'Fabricación de cerraduras y herrajes', '2573' => 'Fabricación de herramientas', '259' => 'Fabricación de otros productos metálicos', '2591' => 'Fabricación de bidones y toneles de hierro o acero', '2592' => 'Fabricación de envases y embalajes metálicos ligeros', '2593' => 'Fabricación de productos de alambre, cadenas y muelles', '2594' => 'Fabricación de pernos y productos de tornillería', '2599' => 'Fabricación de otros productos metálicos n.c.o.p.', '26' => 'Fabricación de productos informáticos, electrónicos y ópticos', '261' => 'Fabricación de componentes electrónicos y circuitos impresos ensamblados', '2611' => 'Fabricación de componentes electrónicos', '2612' => 'Fabricación de circuitos impresos ensamblados', '262' => 'Fabricación de ordenadores y equipos periféricos', '2620' => 'Fabricación de ordenadores y equipos periféricos', '263' => 'Fabricación de equipos de telecomunicaciones', '2630' => 'Fabricación de equipos de telecomunicaciones', '264' => 'Fabricación de productos electrónicos de consumo', '2640' => 'Fabricación de productos electrónicos de consumo', '265' => 'Fabricación de instrumentos y aparatos de medida, verificación y navegación; fabricación de relojes', '2651' => 'Fabricación de instrumentos y aparatos de medida, verificación y navegación', '2652' => 'Fabricación de relojes', '266' => 'Fabricación de equipos de radiación, electromédicos y electroterapéuticos', '2660' => 'Fabricación de equipos de radiación, electromédicos y electroterapéuticos', '267' => 'Fabricación de instrumentos de óptica y equipo fotográfico', '2670' => 'Fabricación de instrumentos de óptica y equipo fotográfico', '268' => 'Fabricación de soportes magnéticos y ópticos', '2680' => 'Fabricación de soportes magnéticos y ópticos', '27' => 'Fabricación de material y equipo eléctrico', '271' => 'Fabricación de motores, generadores y transformadores eléctricos, y de aparatos de distribución y control eléctrico', '2711' => 'Fabricación de motores, generadores y transformadores eléctricos', '2712' => 'Fabricación de aparatos de distribución y control eléctrico', '272' => 'Fabricación de pilas y acumuladores eléctricos', '2720' => 'Fabricación de pilas y acumuladores eléctricos', '273' => 'Fabricación de cables y dispositivos de cableado', '2731' => 'Fabricación de cables de fibra óptica', '2732' => 'Fabricación de otros hilos y cables electrónicos y eléctricos', '2733' => 'Fabricación de dispositivos de cableado', '274' => 'Fabricación de lámparas y aparatos eléctricos de iluminación', '2740' => 'Fabricación de lámparas y aparatos eléctricos de iluminación', '275' => 'Fabricación de aparatos domésticos', '2751' => 'Fabricación de electrodomésticos', '2752' => 'Fabricación de aparatos domésticos no eléctricos', '279' => 'Fabricación de otro material y equipo eléctrico', '2790' => 'Fabricación de otro material y equipo eléctrico', '28' => 'Fabricación de maquinaria y equipo n.c.o.p.', '281' => 'Fabricación de maquinaria de uso general', '2811' => 'Fabricación de motores y turbinas, excepto los destinados a aeronaves, vehículos automóviles y ciclomotores', '2812' => 'Fabricación de equipos de transmisión hidráulica y neumática', '2813' => 'Fabricación de otras bombas y compresores', '2814' => 'Fabricación de otra grifería y válvulas', '2815' => 'Fabricación de cojinetes, engranajes y órganos mecánicos de transmisión', '282' => 'Fabricación de otra maquinaria de uso general', '2821' => 'Fabricación de hornos y quemadores', '2822' => 'Fabricación de maquinaria de elevación y manipulación', '2823' => 'Fabricación de máquinas y equipos de oficina, excepto equipos informáticos', '2824' => 'Fabricación de herramientas eléctricas manuales', '2825' => 'Fabricación de maquinaria de ventilación y refrigeración no doméstica', '2829' => 'Fabricación de otra maquinaria de uso general n.c.o.p.', '283' => 'Fabricación de maquinaria agraria y forestal', '2830' => 'Fabricación de maquinaria agraria y forestal', '284' => 'Fabricación de máquinas herramienta para trabajar el metal y otras máquinas herramienta', '2841' => 'Fabricación de máquinas herramienta para trabajar el metal', '2849' => 'Fabricación de otras máquinas herramienta', '289' => 'Fabricación de otra maquinaria para usos específicos', '2891' => 'Fabricación de maquinaria para la industria metalúrgica', '2892' => 'Fabricación de maquinaria para las industrias extractivas y de la construcción', '2893' => 'Fabricación de maquinaria para la industria de la alimentación, bebidas y tabaco', '2894' => 'Fabricación de maquinaria para las industrias textil, de la confección y del cuero', '2895' => 'Fabricación de maquinaria para la industria del papel y del cartón', '2896' => 'Fabricación de maquinaria para la industria del plástico y el caucho', '2899' => 'Fabricación de otra maquinaria para usos específicos n.c.o.p.', '29' => 'Fabricación de vehículos de motor, remolques y semirremolques', '291' => 'Fabricación de vehículos de motor', '2910' => 'Fabricación de vehículos de motor', '292' => 'Fabricación de carrocerías para vehículos de motor; fabricación de remolques y semirremolques', '2920' => 'Fabricación de carrocerías para vehículos de motor; fabricación de remolques y semirremolques', '293' => 'Fabricación de componentes, piezas y accesorios para vehículos de motor', '2931' => 'Fabricación de equipos eléctricos y electrónicos para vehículos de motor', '2932' => 'Fabricación de otros componentes, piezas y accesorios para vehículos de motor', '30' => 'Fabricación de otro material de transporte', '301' => 'Construcción naval', '3011' => 'Construcción de barcos y estructuras flotantes', '3012' => 'Construcción de embarcaciones de recreo y deporte', '302' => 'Fabricación de locomotoras y material ferroviario', '3020' => 'Fabricación de locomotoras y material ferroviario', '303' => 'Construcción aeronáutica y espacial y su maquinaria', '3030' => 'Construcción aeronáutica y espacial y su maquinaria', '304' => 'Fabricación de vehículos militares de combate', '3040' => 'Fabricación de vehículos militares de combate', '309' => 'Fabricación de otro material de transporte n.c.o.p.', '3091' => 'Fabricación de motocicletas', '3092' => 'Fabricación de bicicletas y de vehículos para personas con discapacidad', '3099' => 'Fabricación de otro material de transporte n.c.o.p.', '31' => 'Fabricación de muebles', '310' => 'Fabricación de muebles', '3101' => 'Fabricación de muebles de oficina y de establecimientos comerciales', '3102' => 'Fabricación de muebles de cocina', '3103' => 'Fabricación de colchones', '3109' => 'Fabricación de otros muebles', '32' => 'Otras industrias manufactureras', '321' => 'Fabricación de artículos de joyería, bisutería y similares', '3211' => 'Fabricación de monedas', '3212' => 'Fabricación de artículos de joyería y artículos similares', '3213' => 'Fabricación de artículos de bisutería y artículos similares', '322' => 'Fabricación de instrumentos musicales', '3220' => 'Fabricación de instrumentos musicales', '323' => 'Fabricación de artículos de deporte', '3230' => 'Fabricación de artículos de deporte', '324' => 'Fabricación de juegos y juguetes', '3240' => 'Fabricación de juegos y juguetes', '325' => 'Fabricación de instrumentos y suministros médicos y odontológicos', '3250' => 'Fabricación de instrumentos y suministros médicos y odontológicos', '329' => 'Industrias manufactureras n.c.o.p.', '3291' => 'Fabricación de escobas, brochas y cepillos', '3299' => 'Otras industrias manufactureras n.c.o.p.', '33' => 'Reparación e instalación de maquinaria y equipo', '331' => 'Reparación de productos metálicos, maquinaria y equipo', '3311' => 'Reparación de productos metálicos', '3312' => 'Reparación de maquinaria', '3313' => 'Reparación de equipos electrónicos y ópticos', '3314' => 'Reparación de equipos eléctricos', '3315' => 'Reparación y mantenimiento naval', '3316' => 'Reparación y mantenimiento aeronáutico y espacial', '3317' => 'Reparación y mantenimiento de otro material de transporte', '3319' => 'Reparación de otros equipos', '332' => 'Instalación de máquinas y equipos industriales', '3320' => 'Instalación de máquinas y equipos industriales', '35' => 'Suministro de energía eléctrica, gas, vapor y aire acondicionado', '351' => 'Producción, transporte y distribución de energía eléctrica', '3512' => 'Transporte de energía eléctrica', '3513' => 'Distribución de energía eléctrica', '3514' => 'Comercio de energía eléctrica', '3515' => 'Producción de energía hidroeléctrica', '3516' => 'Producción de energía eléctrica de origen térmico convencional', '3517' => 'Producción de energía eléctrica de origen nuclear', '3518' => 'Producción de energía eléctrica de origen eólico', '3519' => 'Producción de energía eléctrica de otros tipos', '352' => 'Producción de gas; distribución por tubería de combustibles gaseosos', '3521' => 'Producción de gas', '3522' => 'Distribución por tubería de combustibles gaseosos', '3523' => 'Comercio de gas por tubería', '353' => 'Suministro de vapor y aire acondicionado', '3530' => 'Suministro de vapor y aire acondicionado', '36' => 'Captación, depuración y distribución de agua', '360' => 'Captación, depuración y distribución de agua', '3600' => 'Captación, depuración y distribución de agua', '37' => 'Recogida y tratamiento de aguas residuales', '370' => 'Recogida y tratamiento de aguas residuales', '3700' => 'Recogida y tratamiento de aguas residuales', '38' => 'Recogida, tratamiento y eliminación de residuos; valorización', '381' => 'Recogida de residuos', '3811' => 'Recogida de residuos no peligrosos', '3812' => 'Recogida de residuos peligrosos', '382' => 'Tratamiento y eliminación de residuos', '3821' => 'Tratamiento y eliminación de residuos no peligrosos', '3822' => 'Tratamiento y eliminación de residuos peligrosos', '383' => 'Valorización', '3831' => 'Separación y clasificación de materiales', '3832' => 'Valorización de materiales ya clasificados', '39' => 'Actividades de descontaminación y otros servicios de gestión de residuos', '390' => 'Actividades de descontaminación y otros servicios de gestión de residuos', '3900' => 'Actividades de descontaminación y otros servicios de gestión de residuos', '41' => 'Construcción de edificios', '411' => 'Promoción inmobiliaria', '4110' => 'Promoción inmobiliaria', '412' => 'Construcción de edificios', '4121' => 'Construcción de edificios residenciales', '4122' => 'Construcción de edificios no residenciales', '42' => 'Ingeniería civil', '421' => 'Construcción de carreteras y vías férreas, puentes y túneles', '4211' => 'Construcción de carreteras y autopistas', '4212' => 'Construcción de vías férreas de superficie y subterráneas', '4213' => 'Construcción de puentes y túneles', '422' => 'Construcción de redes', '4221' => 'Construcción de redes para fluidos', '4222' => 'Construcción de redes eléctricas y de telecomunicaciones', '429' => 'Construcción de otros proyectos de ingeniería civil', '4291' => 'Obras hidráulicas', '4299' => 'Construcción de otros proyectos de ingeniería civil n.c.o.p.', '43' => 'Actividades de construcción especializada', '431' => 'Demolición y preparación de terrenos', '4311' => 'Demolición', '4312' => 'Preparación de terrenos', '4313' => 'Perforaciones y sondeos', '432' => 'Instalaciones eléctricas, de fontanería y otras instalaciones en obras de construcción', '4321' => 'Instalaciones eléctricas', '4322' => 'Fontanería, instalaciones de sistemas de calefacción y aire acondicionado', '4329' => 'Otras instalaciones en obras de construcción', '433' => 'Acabado de edificios', '4331' => 'Revocamiento', '4332' => 'Instalación de carpintería', '4333' => 'Revestimiento de suelos y paredes', '4334' => 'Pintura y acristalamiento', '4339' => 'Otro acabado de edificios', '439' => 'Otras actividades de construcción especializada', '4391' => 'Construcción de cubiertas', '4399' => 'Otras actividades de construcción especializada n.c.o.p.', '45' => 'Venta y reparación de vehículos de motor y motocicletas', '451' => 'Venta de vehículos de motor', '4511' => 'Venta de automóviles y vehículos de motor ligeros', '4519' => 'Venta de otros vehículos de motor', '452' => 'Mantenimiento y reparación de vehículos de motor', '4520' => 'Mantenimiento y reparación de vehículos de motor', '453' => 'Comercio de repuestos y accesorios de vehículos de motor', '4531' => 'Comercio al por mayor de repuestos y accesorios de vehículos de motor', '4532' => 'Comercio al por menor de repuestos y accesorios de vehículos de motor', '454' => 'Venta, mantenimiento y reparación de motocicletas y de sus repuestos y accesorios', '4540' => 'Venta, mantenimiento y reparación de motocicletas y de sus repuestos y accesorios', '46' => 'Comercio al por mayor e intermediarios del comercio, excepto de vehículos de motor y motocicletas', '461' => 'Intermediarios del comercio', '4611' => 'Intermediarios del comercio de materias primas agrarias, animales vivos, materias primas textiles y productos semielaborados', '4612' => 'Intermediarios del comercio de combustibles, minerales, metales y productos químicos industriales', '4613' => 'Intermediarios del comercio de la madera y materiales de construcción', '4614' => 'Intermediarios del comercio de maquinaria, equipo industrial, embarcaciones y aeronaves', '4615' => 'Intermediarios del comercio de muebles, artículos para el hogar y ferretería', '4616' => 'Intermediarios del comercio de textiles, prendas de vestir, peletería, calzado y artículos de cuero', '4617' => 'Intermediarios del comercio de productos alimenticios, bebidas y tabaco', '4618' => 'Intermediarios del comercio especializados en la venta de otros productos específicos', '4619' => 'Intermediarios del comercio de productos diversos', '462' => 'Comercio al por mayor de materias primas agrarias y de animales vivos', '4621' => 'Comercio al por mayor de cereales, tabaco en rama, simientes y alimentos para animales', '4622' => 'Comercio al por mayor de flores y plantas', '4623' => 'Comercio al por mayor de animales vivos', '4624' => 'Comercio al por mayor de cueros y pieles', '463' => 'Comercio al por mayor de productos alimenticios, bebidas y tabaco', '4631' => 'Comercio al por mayor de frutas y hortalizas', '4632' => 'Comercio al por mayor de carne y productos cárnicos', '4633' => 'Comercio al por mayor de productos lácteos, huevos, aceites y grasas comestibles', '4634' => 'Comercio al por mayor de bebidas', '4635' => 'Comercio al por mayor de productos del tabaco', '4636' => 'Comercio al por mayor de azúcar, chocolate y confitería', '4637' => 'Comercio al por mayor de café, té, cacao y especias', '4638' => 'Comercio al por mayor de pescados y mariscos y otros productos alimenticios', '4639' => 'Comercio al por mayor, no especializado, de productos alimenticios, bebidas y tabaco', '464' => 'Comercio al por mayor de artículos de uso doméstico', '4641' => 'Comercio al por mayor de textiles', '4642' => 'Comercio al por mayor de prendas de vestir y calzado', '4643' => 'Comercio al por mayor de aparatos electrodomésticos', '4644' => 'Comercio al por mayor de porcelana, cristalería y artículos de limpieza', '4645' => 'Comercio al por mayor de productos perfumería y cosmética', '4646' => 'Comercio al por mayor de productos farmacéuticos', '4647' => 'Comercio al por mayor de muebles, alfombras y aparatos de iluminación', '4648' => 'Comercio al por mayor de artículos de relojería y joyería', '4649' => 'Comercio al por mayor de otros artículos de uso doméstico', '465' => 'Comercio al por mayor de equipos para las tecnologías de la información y las comunicaciones', '4651' => 'Comercio al por mayor de ordenadores, equipos periféricos y programas informáticos', '4652' => 'Comercio al por mayor de equipos electrónicos y de telecomunicaciones y sus componentes', '466' => 'Comercio al por mayor de otra maquinaria, equipos y suministros', '4661' => 'Comercio al por mayor de maquinaria, equipos y suministros agrícolas', '4662' => 'Comercio al por mayor de máquinas herramienta', '4663' => 'Comercio al por mayor de maquinaria para la minería, la construcción y la ingeniería civil', '4664' => 'Comercio al por mayor de maquinaria para la industria textil y de máquinas de coser y tricotar', '4665' => 'Comercio al por mayor de muebles de oficina', '4666' => 'Comercio al por mayor de otra maquinaria y equipo de oficina', '4669' => 'Comercio al por mayor de otra maquinaria y equipo', '467' => 'Otro comercio al por mayor especializado', '4671' => 'Comercio al por mayor de combustibles sólidos, líquidos y gaseosos, y productos similares', '4672' => 'Comercio al por mayor de metales y minerales metálicos', '4673' => 'Comercio al por mayor de madera, materiales de construcción y aparatos sanitarios', '4674' => 'Comercio al por mayor de ferretería, fontanería y calefacción', '4675' => 'Comercio al por mayor de productos químicos', '4676' => 'Comercio al por mayor de otros productos semielaborados', '4677' => 'Comercio al por mayor de chatarra y productos de desecho', '469' => 'Comercio al por mayor no especializado', '4690' => 'Comercio al por mayor no especializado', '47' => 'Comercio al por menor, excepto de vehículos de motor y motocicletas', '471' => 'Comercio al por menor en establecimientos no especializados', '4711' => 'Comercio al por menor en establecimientos no especializados, con predominio en productos alimenticios, bebidas y tabaco', '4719' => 'Otro comercio al por menor en establecimientos no especializados', '472' => 'Comercio al por menor de productos alimenticios, bebidas y tabaco en establecimientos especializados', '4721' => 'Comercio al por menor de frutas y hortalizas en establecimientos especializados', '4722' => 'Comercio al por menor de carne y productos cárnicos en establecimientos especializados', '4723' => 'Comercio al por menor de pescados y mariscos en establecimientos especializados', '4724' => 'Comercio al por menor de pan y productos de panadería, confitería y pastelería en establecimientos especializados', '4725' => 'Comercio al por menor de bebidas en establecimientos especializados', '4726' => 'Comercio al por menor de productos de tabaco en establecimientos especializados', '4729' => 'Otro comercio al por menor de productos alimenticios en establecimientos especializados', '473' => 'Comercio al por menor de combustible para la automoción en establecimientos especializados', '4730' => 'Comercio al por menor de combustible para la automoción en establecimientos especializados', '474' => 'Comercio al por menor de equipos para las tecnologías de la información y las comunicaciones en establecimientos especializados', '4741' => 'Comercio al por menor de ordenadores, equipos periféricos y programas informáticos en establecimientos especializados', '4742' => 'Comercio al por menor de equipos de telecomunicaciones en establecimientos especializados', '4743' => 'Comercio al por menor de equipos de audio y vídeo en establecimientos especializados', '475' => 'Comercio al por menor de otros artículos de uso doméstico en establecimientos especializados', '4751' => 'Comercio al por menor de textiles en establecimientos especializados', '4752' => 'Comercio al por menor de ferretería, pintura y vidrio en establecimientos especializados', '4753' => 'Comercio al por menor de alfombras, moquetas y revestimientos de paredes y suelos en establecimientos especializados', '4754' => 'Comercio al por menor de aparatos electrodomésticos en establecimientos especializados', '4759' => 'Comercio al por menor de muebles, aparatos de iluminación y otros artículos de uso doméstico en establecimientos especializados', '476' => 'Comercio al por menor de artículos culturales y recreativos en establecimientos especializados', '4761' => 'Comercio al por menor de libros en establecimientos especializados', '4762' => 'Comercio al por menor de periódicos y artículos de papelería en establecimientos especializados', '4763' => 'Comercio al por menor de grabaciones de música y vídeo en establecimientos especializados', '4764' => 'Comercio al por menor de artículos deportivos en establecimientos especializados', '4765' => 'Comercio al por menor de juegos y juguetes en establecimientos especializados', '477' => 'Comercio al por menor de otros artículos en establecimientos especializados', '4771' => 'Comercio al por menor de prendas de vestir en establecimientos especializados', '4772' => 'Comercio al por menor de calzado y artículos de cuero en establecimientos especializados', '4773' => 'Comercio al por menor de productos farmacéuticos en establecimientos especializados', '4774' => 'Comercio al por menor de artículos médicos y ortopédicos en establecimientos especializados', '4775' => 'Comercio al por menor de productos cosméticos e higiénicos en establecimientos especializados', '4776' => 'Comercio al por menor de flores, plantas, semillas, fertilizantes, animales de compañía y alimentos para los mismos en establecimientos especializados', '4777' => 'Comercio al por menor de artículos de relojería y joyería en establecimientos especializados', '4778' => 'Otro comercio al por menor de artículos nuevos en establecimientos especializados', '4779' => 'Comercio al por menor de artículos de segunda mano en establecimientos', '478' => 'Comercio al por menor en puestos de venta y en mercadillos', '4781' => 'Comercio al por menor de productos alimenticios, bebidas y tabaco en puestos de venta y en mercadillos', '4782' => 'Comercio al por menor de productos textiles, prendas de vestir y calzado en puestos de venta y en mercadillos', '4789' => 'Comercio al por menor de otros productos en puestos de venta y en mercadillos', '479' => 'Comercio al por menor no realizado ni en establecimientos, ni en puestos de venta ni en mercadillos', '4791' => 'Comercio al por menor por correspondencia o Internet', '4799' => 'Otro comercio al por menor no realizado ni en establecimientos, ni en puestos de venta ni en mercadillos', '49' => 'Transporte terrestre y por tubería', '491' => 'Transporte interurbano de pasajeros por ferrocarril', '4910' => 'Transporte interurbano de pasajeros por ferrocarril', '492' => 'Transporte de mercancías por ferrocarril', '4920' => 'Transporte de mercancías por ferrocarril', '493' => 'Otro transporte terrestre de pasajeros', '4931' => 'Transporte terrestre urbano y suburbano de pasajeros', '4932' => 'Transporte por taxi', '4939' => 'tipos de transporte terrestre de pasajeros n.c.o.p.', '494' => 'Transporte de mercancías por carretera y servicios de mudanza', '4941' => 'Transporte de mercancías por carretera', '4942' => 'Servicios de mudanza', '495' => 'Transporte por tubería', '4950' => 'Transporte por tubería', '50' => 'Transporte marítimo y por vías navegables interiores', '501' => 'Transporte marítimo de pasajeros', '5010' => 'Transporte marítimo de pasajeros', '502' => 'Transporte marítimo de mercancías', '5020' => 'Transporte marítimo de mercancías', '503' => 'Transporte de pasajeros por vías navegables interiores', '5030' => 'Transporte de pasajeros por vías navegables interiores', '504' => 'Transporte de mercancías por vías navegables interiores', '5040' => 'Transporte de mercancías por vías navegables interiores', '51' => 'Transporte aéreo', '511' => 'Transporte aéreo de pasajeros', '5110' => 'Transporte aéreo de pasajeros', '512' => 'Transporte aéreo de mercancías y transporte espacial', '5121' => 'Transporte aéreo de mercancías', '5122' => 'Transporte espacial', '52' => 'Almacenamiento y actividades anexas al transporte', '521' => 'Depósito y almacenamiento', '5210' => 'Depósito y almacenamiento', '522' => 'Actividades anexas al transporte', '5221' => 'Actividades anexas al transporte terrestre', '5222' => 'Actividades anexas al transporte marítimo y por vías navegables interiores', '5223' => 'Actividades anexas al transporte aéreo', '5224' => 'Manipulación de mercancías', '5229' => 'Otras actividades anexas al transporte', '53' => 'Actividades postales y de correos', '531' => 'Actividades postales sometidas a la obligación del servicio universal', '5310' => 'Actividades postales sometidas a la obligación del servicio universal', '532' => 'Otras actividades postales y de correos', '5320' => 'Otras actividades postales y de correos', '55' => 'Servicios de alojamiento', '551' => 'Hoteles y alojamientos similares', '5510' => 'Hoteles y alojamientos similares', '552' => 'Alojamientos turísticos y otros alojamientos de corta estancia', '5520' => 'Alojamientos turísticos y otros alojamientos de corta estancia', '553' => 'Campings y aparcamientos para caravanas', '5530' => 'Campings y aparcamientos para caravanas', '559' => 'Otros alojamientos', '5590' => 'Otros alojamientos', '56' => 'Servicios de comidas y bebidas', '561' => 'Restaurantes y puestos de comidas', '5610' => 'Restaurantes y puestos de comidas', '562' => 'Provisión de comidas preparadas para eventos y otros servicios de comidas', '5621' => 'Provisión de comidas preparadas para eventos', '5629' => 'Otros servicios de comidas', '563' => 'Establecimientos de bebidas', '5630' => 'Establecimientos de bebidas', '58' => 'Edición', '581' => 'Edición de libros, periódicos y otras actividades editoriales', '5811' => 'Edición de libros', '5812' => 'Edición de directorios y guías de direcciones postales', '5813' => 'Edición de periódicos', '5814' => 'Edición de revistas', '5819' => 'Otras actividades editoriales', '582' => 'Edición de programas informáticos', '5821' => 'Edición de videojuegos', '5829' => 'Edición de otros programas informáticos', '59' => 'Actividades cinematográficas, de vídeo y de programas de televisión, grabación de sonido y edición musical', '591' => 'Actividades cinematográficas, de vídeo y de programas de televisión', '5912' => 'Actividades de postproducción cinematográfica, de vídeo y de programas de televisión', '5914' => 'Actividades de exhibición cinematográfica', '5915' => 'Actividades de producción cinematográfica y de vídeo', '5916' => 'Actividades de producciones de programas de televisión', '5917' => 'Actividades de distribución cinematográfica y de vídeo', '5918' => 'Actividades de distribución de programas de televisión', '592' => 'Actividades de grabación de sonido y edición musical', '5920' => 'Actividades de grabación de sonido y edición musical', '60' => 'Actividades de programación y emisión de radio y televisión', '601' => 'Actividades de radiodifusión', '6010' => 'Actividades de radiodifusión', '602' => 'Actividades de programación y emisión de televisión', '6020' => 'Actividades de programación y emisión de televisión', '61' => 'Telecomunicaciones', '611' => 'Telecomunicaciones por cable', '6110' => 'Telecomunicaciones por cable', '612' => 'Telecomunicaciones inalámbricas', '6120' => 'Telecomunicaciones inalámbricas', '613' => 'Telecomunicaciones por satélite', '6130' => 'Telecomunicaciones por satélite', '619' => 'Otras actividades de telecomunicaciones', '6190' => 'Otras actividades de telecomunicaciones', '62' => 'Programación, consultoría y otras actividades relacionadas con la informática', '620' => 'Programación, consultoría y otras actividades relacionadas con la informática', '6201' => 'Actividades de programación informática', '6202' => 'Actividades de consultoría informática', '6203' => 'Gestión de recursos informáticos', '6209' => 'Otros servicios relacionados con las tecnologías de la información y la informática', '63' => 'Servicios de información', '631' => 'Proceso de datos, hosting y actividades relacionadas; portales web', '6311' => 'Proceso de datos, hosting y actividades relacionadas', '6312' => 'Portales web', '639' => 'Otros servicios de información', '6391' => 'Actividades de las agencias de noticias', '6399' => 'Otros servicios de información n.c.o.p.', '64' => 'Servicios financieros, excepto seguros y fondos de pensiones', '641' => 'Intermediación monetaria', '6411' => 'Banco central', '6419' => 'Otra intermediación monetaria', '642' => 'Actividades de las sociedades holding', '6420' => 'Actividades de las sociedades holding', '643' => 'Inversión colectiva, fondos y entidades financieras similares', '6430' => 'Inversión colectiva, fondos y entidades financieras similares', '649' => 'Otros servicios financieros, excepto seguros y fondos de pensiones', '6491' => 'Arrendamiento financiero', '6492' => 'Otras actividades crediticias', '6499' => 'Otros servicios financieros, excepto seguros y fondos de pensiones n.c.o.p.', '65' => 'Seguros, reaseguros y fondos de pensiones, excepto Seguridad Social obligatoria', '651' => 'Seguros', '6511' => 'Seguros de vida', '6512' => 'Seguros distintos de los seguros de vida', '652' => 'Reaseguros', '6520' => 'Reaseguros', '653' => 'Fondos de pensiones', '6530' => 'Fondos de pensiones', '66' => 'Actividades auxiliares a los servicios financieros y a los seguros', '661' => 'Actividades auxiliares a los servicios financieros, excepto seguros y fondos de pensiones', '6611' => 'Administración de mercados financieros', '6612' => 'Actividades de intermediación en operaciones con valores y otros activos', '6619' => 'Otras actividades auxiliares a los servicios financieros, excepto seguros y fondos de pensiones', '662' => 'Actividades auxiliares a seguros y fondos de pensiones', '6621' => 'Evaluación de riesgos y daños', '6622' => 'Actividades de agentes y corredores de seguros', '6629' => 'Otras actividades auxiliares a seguros y fondos de pensiones', '663' => 'Actividades de gestión de fondos', '6630' => 'Actividades de gestión de fondos', '68' => 'Actividades inmobiliarias', '681' => 'Compraventa de bienes inmobiliarios por cuenta propia', '6810' => 'Compraventa de bienes inmobiliarios por cuenta propia', '682' => 'Alquiler de bienes inmobiliarios por cuenta propia', '6820' => 'Alquiler de bienes inmobiliarios por cuenta propia', '683' => 'Actividades inmobiliarias por cuenta de terceros', '6831' => 'Agentes de la propiedad inmobiliaria', '6832' => 'Gestión y administración de la propiedad inmobiliaria', '69' => 'Actividades jurídicas y de contabilidad', '691' => 'Actividades jurídicas', '6910' => 'Actividades jurídicas', '692' => 'Actividades de contabilidad, teneduría de libros, auditoría y asesoría fiscal', '6920' => 'Actividades de contabilidad, teneduría de libros, auditoría y asesoría fiscal', '70' => 'Actividades de las sedes centrales; actividades de consultoría de gestión empresarial', '701' => 'Actividades de las sedes centrales', '7010' => 'Actividades de las sedes centrales', '702' => 'Actividades de consultoría de gestión empresarial', '7021' => 'Relaciones públicas y comunicación', '7022' => 'Otras actividades de consultoría de gestión empresarial', '71' => 'Servicios técnicos de arquitectura e ingeniería; ensayos y análisis técnicos', '711' => 'Servicios técnicos de arquitectura e ingeniería y otras actividades relacionadas con el asesoramiento técnico', '7111' => 'Servicios técnicos de arquitectura', '7112' => 'Servicios técnicos de ingeniería y otras actividades relacionadas con el asesoramiento técnico', '712' => 'Ensayos y análisis técnicos', '7120' => 'Ensayos y análisis técnicos', '72' => 'Investigación y desarrollo', '721' => 'Investigación y desarrollo experimental en ciencias naturales y técnicas', '7211' => 'Investigación y desarrollo experimental en biotecnología', '7219' => 'Otra investigación y desarrollo experimental en ciencias naturales y técnicas', '722' => 'Investigación y desarrollo experimental en ciencias sociales y humanidades', '7220' => 'Investigación y desarrollo experimental en ciencias sociales y humanidades', '73' => 'Publicidad y estudios de mercado', '731' => 'Publicidad', '7311' => 'Agencias de publicidad', '7312' => 'Servicios de representación de medios de comunicación', '732' => 'Estudio de mercado y realización de encuestas de opinión pública', '7320' => 'Estudio de mercado y realización de encuestas de opinión pública', '74' => 'Otras actividades profesionales, científicas y técnicas', '741' => 'Actividades de diseño especializado', '7410' => 'Actividades de diseño especializado', '742' => 'Actividades de fotografía', '7420' => 'Actividades de fotografía', '743' => 'Actividades de traducción e interpretación', '7430' => 'Actividades de traducción e interpretación', '749' => 'Otras actividades profesionales, científicas y técnicas n.c.o.p.', '7490' => 'Otras actividades profesionales, científicas y técnicas n.c.o.p.', '75' => 'Actividades veterinarias', '750' => 'Actividades veterinarias', '7500' => 'Actividades veterinarias', '77' => 'Actividades de alquiler', '771' => 'Alquiler de vehículos de motor', '7711' => 'Alquiler de automóviles y vehículos de motor ligeros', '7712' => 'Alquiler de camiones', '772' => 'Alquiler de efectos personales y artículos de uso doméstico', '7721' => 'Alquiler de artículos de ocio y deportivos', '7722' => 'Alquiler de cintas de vídeo y discos', '7729' => 'Alquiler de otros efectos personales y artículos de uso doméstico', '773' => 'Alquiler de otra maquinaria, equipos y bienes tangibles', '7731' => 'Alquiler de maquinaria y equipo de uso agrícola', '7732' => 'Alquiler de maquinaria y equipo para la construcción e ingeniería civil', '7733' => 'Alquiler de maquinaria y equipo de oficina, incluidos ordenadores', '7734' => 'Alquiler de medios de navegación', '7735' => 'Alquiler de medios de transporte aéreo', '7739' => 'Alquiler de otra maquinaria, equipos y bienes tangibles n.c.o.p.', '774' => 'Arrendamiento de la propiedad intelectual y productos similares, excepto trabajos protegidos por los derechos de autor', '7740' => 'Arrendamiento de la propiedad intelectual y productos similares, excepto trabajos protegidos por los derechos de autor', '78' => 'Actividades relacionadas con el empleo', '781' => 'Actividades de las agencias de colocación', '7810' => 'Actividades de las agencias de colocación', '782' => 'Actividades de las empresas de trabajo temporal', '7820' => 'Actividades de las empresas de trabajo temporal', '783' => 'Otra provisión de recursos humanos', '7830' => 'Otra provisión de recursos humanos', '79' => 'Actividades de agencias de viajes, operadores turísticos, servicios de reservas y actividades relacionadas con los mismos', '791' => 'Actividades de agencias de viajes y operadores turísticos', '7911' => 'Actividades de las agencias de viajes', '7912' => 'Actividades de los operadores turísticos', '799' => 'Otros servicios de reservas y actividades relacionadas con los mismos', '7990' => 'Otros servicios de reservas y actividades relacionadas con los mismos', '80' => 'Actividades de seguridad e investigación', '801' => 'Actividades de seguridad privada', '8010' => 'Actividades de seguridad privada', '802' => 'Servicios de sistemas de seguridad', '8020' => 'Servicios de sistemas de seguridad', '803' => 'Actividades de investigación', '8030' => 'Actividades de investigación', '81' => 'Servicios a edificios y actividades de jardinería', '811' => 'Servicios integrales a edificios e instalaciones', '8110' => 'Servicios integrales a edificios e instalaciones', '812' => 'Actividades de limpieza', '8121' => 'Limpieza general de edificios', '8122' => 'Otras actividades de limpieza industrial y de edificios', '8129' => 'Otras actividades de limpieza', '813' => 'Actividades de jardinería', '8130' => 'Actividades de jardinería', '82' => 'Actividades administrativas de oficina y otras actividades auxiliares a las empresas', '821' => 'Actividades administrativas y auxiliares de oficina', '8211' => 'Servicios administrativos combinados', '8219' => 'Actividades de fotocopiado, preparación de documentos y otras actividades especializadas de oficina', '822' => 'Actividades de los centros de llamadas', '8220' => 'Actividades de los centros de llamadas', '823' => 'Organización de convenciones y ferias de muestras', '8230' => 'Organización de convenciones y ferias de muestras', '829' => 'Actividades de apoyo a las empresas n.c.o.p.', '8291' => 'Actividades de las agencias de cobros y de información comercial', '8292' => 'Actividades de envasado y empaquetado', '8299' => 'Otras actividades de apoyo a las empresas n.c.o.p.', '84' => 'Administración Pública y defensa; Seguridad Social obligatoria', '841' => 'Administración Pública y de la política económica y social', '8411' => 'Actividades generales de la Administración Pública', '8412' => 'Regulación de las actividades sanitarias, educativas y culturales y otros servicios sociales, excepto Seguridad Social', '8413' => 'Regulación de la actividad económica y contribución a su mayor eficiencia', '842' => 'Prestación de servicios a la comunidad en general', '8421' => 'Asuntos exteriores', '8422' => 'Defensa', '8423' => 'Justicia', '8424' => 'Orden público y seguridad', '8425' => 'Protección civil', '843' => 'Seguridad Social obligatoria', '8430' => 'Seguridad Social obligatoria', '85' => 'Educación', '851' => 'Educación preprimaria', '8510' => 'Educación preprimaria', '852' => 'Educación primaria', '8520' => 'Educación primaria', '853' => 'Educación secundaria', '8531' => 'Educación secundaria general', '8532' => 'Educación secundaria técnica y profesional', '854' => 'Educación postsecundaria', '8541' => 'Educación postsecundaria no terciaria', '8543' => 'Educación universitaria', '8544' => 'Educación terciaria no universitaria', '855' => 'Otra educación', '8551' => 'Educación deportiva y recreativa', '8552' => 'Educación cultural', '8553' => 'Actividades de las escuelas de conducción y pilotaje', '8559' => 'Otra educación n.c.o.p.', '856' => 'Actividades auxiliares a la educación', '8560' => 'Actividades auxiliares a la educación', '86' => 'Actividades sanitarias', '861' => 'Actividades hospitalarias', '8610' => 'Actividades hospitalarias', '862' => 'Actividades médicas y odontológicas', '8621' => 'Actividades de medicina general', '8622' => 'Actividades de medicina especializada', '8623' => 'Actividades odontológicas', '869' => 'Otras actividades sanitarias', '8690' => 'Otras actividades sanitarias', '87' => 'Asistencia en establecimientos residenciales', '871' => 'Asistencia en establecimientos residenciales con cuidados sanitarios', '8710' => 'Asistencia en establecimientos residenciales con cuidados sanitarios', '872' => 'Asistencia en establecimientos residenciales para personas con discapacidad intelectual, enfermedad mental y drogodependencia', '8720' => 'Asistencia en establecimientos residenciales para personas con discapacidad intelectual, enfermedad mental y drogodependencia', '873' => 'Asistencia en establecimientos residenciales para personas mayores y con discapacidad física', '8731' => 'Asistencia en establecimientos residenciales para personas mayores', '8732' => 'Asistencia en establecimientos residenciales para personas con discapacidad física', '879' => 'Otras actividades de asistencia en establecimientos residenciales', '8790' => 'Otras actividades de asistencia en establecimientos residenciales', '88' => 'Actividades de servicios sociales sin alojamiento', '881' => 'Actividades de servicios sociales sin alojamiento para personas mayores y con discapacidad', '8811' => 'Actividades de servicios sociales sin alojamiento para personas mayores', '8812' => 'Actividades de servicios sociales sin alojamiento para personas con discapacidad', '889' => 'Otros actividades de servicios sociales sin alojamiento', '8891' => 'Actividades de cuidado diurno de niños', '8899' => 'Otros actividades de servicios sociales sin alojamiento n.c.o.p.', '90' => 'Actividades de creación, artísticas y espectáculos', '900' => 'Actividades de creación, artísticas y espectáculos', '9001' => 'Artes escénicas', '9002' => 'Actividades auxiliares a las artes escénicas', '9003' => 'Creación artística y literaria', '9004' => 'Gestión de salas de espectáculos', '91' => 'Actividades de bibliotecas, archivos, museos y otras actividades culturales', '910' => 'Actividades de bibliotecas, archivos, museos y otras actividades culturales', '9102' => 'Actividades de museos', '9103' => 'Gestión de lugares y edificios históricos', '9104' => 'Actividades de los jardines botánicos, parques zoológicos y reservas naturales', '9105' => 'Actividades de bibliotecas', '9106' => 'Actividades de archivos', '92' => 'Actividades de juegos de azar y apuestas', '920' => 'Actividades de juegos de azar y apuestas', '9200' => 'Actividades de juegos de azar y apuestas', '93' => 'Actividades deportivas, recreativas y de entretenimiento', '931' => 'Actividades deportivas', '9311' => 'Gestión de instalaciones deportivas', '9312' => 'Actividades de los clubes deportivos', '9313' => 'Actividades de los gimnasios', '9319' => 'Otras actividades deportivas', '932' => 'Actividades recreativas y de entretenimiento', '9321' => 'Actividades de los parques de atracciones y los parques temáticos', '9329' => 'Otras actividades recreativas y de entretenimiento', '94' => 'Actividades asociativas', '941' => 'Actividades de organizaciones empresariales, profesionales y patronales', '9411' => 'Actividades de organizaciones empresariales y patronales', '9412' => 'Actividades de organizaciones profesionales', '942' => 'Actividades sindicales', '9420' => 'Actividades sindicales', '949' => 'Otras actividades asociativas', '9491' => 'Actividades de organizaciones religiosas', '9492' => 'Actividades de organizaciones políticas', '9499' => 'Otras actividades asociativas n.c.o.p.', '95' => 'Reparación de ordenadores, efectos personales y artículos de uso doméstico', '951' => 'Reparación de ordenadores y equipos de comunicación', '9511' => 'Reparación de ordenadores y equipos periféricos', '9512' => 'Reparación de equipos de comunicación', '952' => 'Reparación de efectos personales y artículos de uso doméstico', '9521' => 'Reparación de aparatos electrónicos de audio y vídeo de uso doméstico', '9522' => 'Reparación de aparatos electrodomésticos y de equipos para el hogar y el jardín', '9523' => 'Reparación de calzado y artículos de cuero', '9524' => 'Reparación de muebles y artículos de menaje', '9525' => 'Reparación de relojes y joyería', '9529' => 'Reparación de otros efectos personales y artículos de uso doméstico', '96' => 'Otros servicios personales', '960' => 'Otros servicios personales', '9601' => 'Lavado y limpieza de prendas textiles y de piel', '9602' => 'Peluquería y otros tratamientos de belleza', '9603' => 'Pompas fúnebres y actividades relacionadas', '9604' => 'Actividades de mantenimiento físico', '9609' => 'Otras servicios personales n.c.o.p.', '97' => 'Actividades de los hogares como http://localhost:8888/anesco2/clientes/gestion.php?codigo=1022empleadores de personal doméstico', '970' => 'Actividades de los hogares como empleadores de personal doméstico', '9700' => 'Actividades de los hogares como empleadores de personal doméstico', '98' => 'Actividades de los hogares como productores de bienes y servicios para uso propio', '981' => 'Actividades de los hogares como productores de bienes para uso propio', '9810' => 'Actividades de los hogares como productores de bienes para uso propio', '982' => 'Actividades de los hogares como productores de servicios para uso propio', '9820' => 'Actividades de los hogares como productores de servicios para uso propio', '99' => 'Actividades de organizaciones y organismos extraterritoriales', '990' => 'Actividades de organizaciones y organismos extraterritoriales', '9900' => 'Actividades de organizaciones y organismos extraterritoriales');
}

function defineActividad($codigo){
    $actividad=datosRegistro('actividades',$codigo);
    return $actividad['nombre'];
}

function campoClaveAleatoria($nombreCampo,$texto,$valor='',$claseBoton='btn-propio',$clase='input-small',$disabled=false){
    $des='';
    if($disabled){
        $des='disabled="disabled"';
    }
    $valor=compruebaValorCampo($valor,$nombreCampo);

    echo "
    <div class='control-group'>                     
      <label class='control-label' for='$nombreCampo'>$texto:</label>
      <div class='controls'>
        <input type='password' class='$clase form-control' id='$nombreCampo' name='$nombreCampo' value=\"$valor\" $des>
        <span class='input-group-btn'>
            <button class='btn btn-small $claseBoton' type='button' id='generarClave' estado='ver'>Generar</button>
            <button class='btn btn-small $claseBoton' type='button' id='mostrarClave' estado='ver'>Mostrar</button>
        </span>
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}

function estadoFactura($codigo){
    $factura=datosRegistro('facturas',$codigo,'codigoCliente');
    $estadoFactura='No tiene factura';
    if($factura){

        $estadoFactura='Factura enviada';

        if($factura['facturaCobrada']=='SI'){
            $estadoFactura.=' y cobrada';
        } 
        else {
            $estadoFactura.=' y NO cobrada';
        }
        
    }
    return $estadoFactura;
}

function estadoContrato($codigo){
    $contrato=consultaBD('SELECT contratos.* FROM contratos LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo WHERE ofertas.codigoCliente='.$codigo,true,true);
    $vencimiento=array();
    $vencimiento['vencimiento']='No tiene contrato';
    $vencimiento['tecnico']='No tiene contrato';
    $meses=array('00'=>'','01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
    if($contrato){
        $fecha=explode('-', $contrato['fechaFin']);
        $vencimiento['vencimiento']=$meses[$fecha[1]].' / '.substr($fecha[0], 2);
        $tecnico=datosRegistro('usuarios',$contrato['tecnico']);
        if($tecnico){
            $vencimiento['tecnico']=$tecnico['nombre']." ".$tecnico['apellidos'];
        } else {
            $vencimiento['tecnico']='El contrato no tiene asignado técnico';
        }
    }
    return $vencimiento;
}

function estadoPlanificacion($codigo,$excel=false){
    $planificacion=consultaBD('SELECT formularioPRL.* FROM formularioPRL INNER JOIN contratos ON formularioPRL.codigoContrato=contratos.codigo INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo WHERE codigoCliente='.$codigo,true,true);
    $res='No tiene planificación';
    $arrayCampos=array('visitada','planPrev','pap','info','memoria');
    $arrayFechas=array('fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria');
    $iconosEstado=array(
                    'SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>',
                    'NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
    if($planificacion){
        $visita=consultaBD("SELECT fechaPrevista FROM planificacion_visitas WHERE codigoFormulario=".$planificacion['codigo']." ORDER BY fechaPrevista DESC LIMIT 1;",true,true);
        $planificacion['fechaVisita']=$visita['fechaPrevista'];
        $res='SI';
        for($i=0;$i<count($arrayCampos);$i++){
            if($res=='SI' && $planificacion[$arrayCampos[$i]] == 'NO' && comparaFechas(date('Y-m-d'),$planificacion[$arrayFechas[$i]])==-1){
                $res='NO';
            }
        }
        
        if(!$excel){
            $res=$iconosEstado[$res];
        }
    } 
    return $res;
}

function campoIBAN($nombreCampo,$texto,$valor='',$readonly=false,$tipo=0){//MODIFICACIÓN 21/07/2014 DE OFICINA: Añadido parámetro $disabled para desactivar campo
//MODIFICACIÓN 06/07/2016 DE DAVID: Sustituido el parametro $disabled por $readonly
    $des='';
    if($readonly){
        $des='readonly';
    }
    $valor=compruebaValorCampo($valor,$nombreCampo);

    if($tipo==0){
        echo "
        <div class='control-group'>                     
          <label class='control-label' for='$nombreCampo'>$texto:</label>
          <div class='controls'>";
    }
    elseif($tipo==1){
        echo "$texto: ";
    }
    if($valor!=''){
        $valores=array();
        $start=0;
        $end=4;
        for($i=0;$i<6;$i++){
            $valores[$i]=substr($valor, $start, $end);
            $start=$start+$end;
        }
    } else {
        $valores=array('','','','','','');
    }
        
    for($i=0;$i<6;$i++){
        echo "<input type='text' class='input-iban iban' id='".$nombreCampo.$i."' name='".$nombreCampo.$i."' value='".$valores[$i]."' $des> ";
    }

    echo "<br/><br/><button type='button' id='validarIban' class='btn btn-propio'>Validar IBAN</button>";
    
    if($tipo==0){
        echo "
          </div> <!-- /controls -->       
        </div> <!-- /control-group -->";
    }
}

function creaIBAN(){
    $_POST['EMPIBAN']='';
    $i=0;
    while(isset($_POST['EMPIBAN'.$i])){
        $_POST['EMPIBAN'].=$_POST['EMPIBAN'.$i];
        $i++;
    }
}

function imprimeCorreos(){
    global $_CONFIG;
    $where=compruebaPerfilParaWhere('correos.codigoUsuario');
    $consulta=consultaBD("SELECT correos.codigo AS codigo, usuario, destinatarios, fecha, hora, asunto, correos.tipo FROM correos INNER JOIN usuarios ON correos.codigoUsuario=usuarios.codigo $where ORDER BY fecha, hora DESC;",true);

    while($datos=mysql_fetch_assoc($consulta)){
        $tipoCorreo=formateaTipoCorreo($datos['tipo']);

        echo "
        <tr>
            <td> ".ucfirst($datos['usuario'])." </td>
            <td> ".$datos['destinatarios']." </td>
            <td class='centro'>".formateaFechaWeb($datos['fecha'])."</td>
            <td class='centro'>".formateaHoraWeb($datos['hora'])."</td>
            <td> ".$datos['asunto']." </td>
            <td> ".$tipoCorreo." </td>
            <td class='centro'>
                <a href='".$_CONFIG['raiz']."correos/gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio noAjax'><i class='icon-search-plus'></i> Detalles</i></a>
            </td>
            <td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
        </tr>";
    }
}

function formateaTipoCorreo($tipoCorreo){
    $res=$tipoCorreo;

    if(substr_count($res,'VS')>0){
        $tiposCorreoPlanificacionVS=array(
            'VS0'   =>  'Email presentación',
            'VS1'   =>  'Email presentación técnico',
            'VS2'   =>  'Email técnico 2 (sin respuesta el 1)',
            'VS3'   =>  'Email informe E.R.',
            'VS4'   =>  'Email recordatorio V.S.',
            'VS5'   =>  'Email renovación contrato',
            'VS6'   =>  'Email memoria y cuestionario',
            'VS7'   =>  'Email renovación técnico'
        );

        $res=$tiposCorreoPlanificacionVS[$res];
    }

    return $res;
}

function datosCorreo($codigo){
    return consultaBD("SELECT usuario, email AS remitente, destinatarios, fecha, hora, asunto, mensaje, ficheroAdjunto FROM correos INNER JOIN usuarios ON correos.codigoUsuario=usuarios.codigo WHERE correos.codigo='$codigo';",true,true);
}

function compruebaAdjuntos($fichero){
    if(trim($fichero)!=''){
        echo '
        <div class="control-group">                     
          <label class="control-label" for="adjunto">Archivo adjunto:</label>
          <div class="controls enlaceForm">
            <a class="btn btn-propio" href="'.$fichero.'" id="adjunto" target="_blank">Ver fichero</a>
          </div>
        </div>';
    }
}

function enviaCorreos(){

    $datos=arrayFormulario();
    
    $codigoU=$_SESSION['codigoU'];
    conexionBD();
    
    $consulta=consultaBD("SELECT email FROM usuarios WHERE codigo='$codigoU';");
    $consulta=mysql_fetch_assoc($consulta);

    
    $adjunto=false;

    if(isset($_FILES['ficheroAdjunto']) && $_FILES['ficheroAdjunto']['name']!=""){

        $mensaje=$datos['mensaje'];

        $nombreFichero=nombreFicheroAdjunto($_FILES['ficheroAdjunto']['name'],time());

        if (!move_uploaded_file($_FILES['ficheroAdjunto']['tmp_name'], "../documentos/adjuntosCorreos/$nombreFichero")){ 
            $nombreFichero='';
        }

        $adjunto="../documentos/adjuntosCorreos/$nombreFichero";
        
    }
    elseif(isset($datos['ofertaPDF']) && $datos['ofertaPDF']!=""){

        $mensaje=$datos['mensaje'];

        $adjunto=$datos['ofertaPDF'];
    }
    elseif(isset($datos['nombreDoc']) && $datos['nombreDoc']!=""){

        $mensaje=$datos['mensaje'];

        $adjunto=$datos['nombreDoc'];
    }
    else{
        $mensaje=$datos['mensaje'];
    }

    $bcc='';
    if(isset($datos['copiaOculta'])){
        $bcc=$datos['copiaOculta'];
    }

    $res=enviaEmail($datos['destinatarios'], $datos['asunto'], $mensaje ,$adjunto,$bcc);
    
    if($res){
        $fecha=date('Y')."-".date('m')."-".date('d');
        $hora=date('H').":".date('i').":00";

        $codigoContrato='NULL';
        if(isset($datos['codigoContrato'])){
            $codigoContrato=$datos['codigoContrato'];
        }

        $res=consultaBD("INSERT INTO correos VALUES(NULL,'".$datos['destinatarios']."', '$fecha', '$hora', '".$datos['asunto']."', '".$datos['mensaje']."', '$adjunto', '$codigoU','".$datos['tipo']."',$codigoContrato,'$bcc');");

        if(isset($datos['codigoDocumentoVS']) && $datos['codigoDocumentoVS']!=""){
            $res=$res && consultaBD("UPDATE documentosVS SET enviado='SI', fechaEnvio='$fecha' WHERE codigo=".$datos['codigoDocumentoVS']);
        }
        
        $id=mysql_insert_id();

        if(isset($_FILES['ficheroAdjunto'])){

            $res=$res && consultaBD("UPDATE correos SET ficheroAdjunto='$nombreFichero' WHERE codigo='$id';");
        }

    }

    cierraBD();
    
    return $res;
}

function compruebaUsuario(){
    $res='ok';

    $datos=arrayFormulario();
    extract($datos);

    $consulta=consultaBD("SELECT usuario FROM usuarios WHERE usuario='$usuario' AND codigo!='$codigoUsuario';",true);
    if(mysql_num_rows($consulta)>0){
        $res='error';
    }

    echo $res;
}

function nombreFicheroAdjunto($nombreTemporal,$id){
    $arrayNombre=explode('.',$nombreTemporal);//Creo un array con el nombre y la extensión en un índice distinto
    $nombre=reset($arrayNombre);//Extrae el nombre del fichero.
    $nombre=str_replace(' ','_',$nombre);//Quita los espacios
    $extension=end($arrayNombre);//Extrae la extensión del fichero.
    $nombreFichero=$nombre.'_'.$id.'.'.$extension;//El fichero pasa a igual, solo que entre el nombre y la extensión está el id (para evitar sobrescrituras)

    return $nombreFichero;
}

function renuevaContratos(){
    $res=0;
    $contratos=consultaBD('SELECT * FROM contratos WHERE fechaFin<="'.date('Y-m-d').'"');
    while ($contrato=mysql_fetch_assoc($contratos)) {
        if($contrato['checkNoRenovar']=='NO'){
            $fechaHoy=date('Y-m-d');
            $nuevafecha = strtotime ('+1 year',strtotime($fechaHoy));
            $nuevafecha = date ( 'Y-m-d' ,$nuevafecha);

            $sql='UPDATE contratos SET fechaFin="'.$nuevafecha.'",horasPrevistas="" WHERE codigo='.$contrato['codigo'];
            $consulta=consultaBD($sql,true);
            $sql='INSERT INTO contratos_renovaciones VALUES(NULL,'.$contrato['codigo'].',"'.date('Y-m-d').'")';
            $consulta=consultaBD($sql,true);
            $res++;
        } else {
            if($contrato['enVigor']=='SI'){
                $sql='UPDATE contratos SET enVigor="NO" WHERE codigo='.$contrato['codigo'];
                $consulta=consultaBD($sql,true);
            }
            
        }
    }
    return $res;
}

function cajaHistoricoSQL(){
    global $_CONFIG;

    if($_SESSION['codigoU']==147){//El código 1 corresponde a soporte/soporte15
        echo '
        <div class="nav-collapse">
            <ul class="nav pull-right cajaUsuario" id="cajaFiltroEjercicio" data-date="01-01-2016" data-date-format="dd-mm-yyyy">
                <li class="dropdown">
                    <a href="'.$_CONFIG['raiz'].'historico-sql/" class="noAjax"><i class="icon-database"></i> Histórico SQL</a>
                </li>
            </ul>
        </div>';
    }
}

function abreCajaBusqueda($id="cajaFiltros"){
    echo '<div class="cajaFiltros form-horizontal hide" id="'.$id.'">
            <h3 class="apartadoFormulario">Filtrar por:</h3>';
}

function cierraCajaBusqueda(){
    echo '</div>';
}

function campoSelectEstadoVencimiento($datos,$i,$tipo=1,$proforma=false,$conexion=true){
    if($datos['codigoEstadoRecibo']=='' || $datos['codigoEstadoRecibo']==NULL){
        $datos['codigoEstadoRecibo']=3;
    }
    if(!$proforma){
        $where='AND nombre NOT LIKE "%PROFORMA%"';
    } else {
        $where='AND nombre NOT LIKE "DEVUELTO"';
    }
    campoSelectConsulta('codigoEstadoRecibo'.$i,'Estado',"SELECT codigo, nombre AS texto FROM estados_recibos WHERE eliminado='NO' $where ORDER BY nombre;",$datos['codigoEstadoRecibo'],'selectpicker span3 show-tick tdEstado',"data-live-search='true'",'',$tipo,$conexion);
}

function campoSelectSiNoFiltro($nombreCampo,$texto,$datos=false){
    campoSelect($nombreCampo,$texto,array('','Si','No'),array('','SI','NO'),$datos,'selectpicker span1 show-tick','');
}

function campoSelectTieneFiltro($nombreCampo,$texto,$datos=false){
    campoSelect($nombreCampo,$texto,array('','Si','No'),array('','NOTNUL','ISNUL'),$datos,'selectpicker span1 show-tick','');//Uso "NUL" porque datatables quita la cadena "NULL" de los valores
}

//Fin parte común de facturas y abonos

//Fin parte común entre contratos, facturas y abonos

function compruebaImporteContrato($importe, $datos, $formatear=true){
    
    if($datos['incrementoIPC']=='SI'){
        $ipc=$datos['incrementoIpcAplicado'];

        $importe=$importe*(1+$ipc/100);
    }

    $res=$importe;
    if ($formatear) {
        $res=formateaNumeroWeb($importe);
    }

    return $res;
}

function campoFormaPago($datos,$nombreCampo='codigoFormaPago',$tipo=0,$conexion=true){
    $where="WHERE eliminado='NO'";
    if($datos){
        $where='';//Para que en los detalles de una ficha se muestren formas de pago definidas anteriormente que ahora pueden estar borradas, de forma que no aparezca el campo vacío.
    }

    campoSelectConsulta($nombreCampo,'Forma de pago',"SELECT codigo, CONCAT(forma,' - Nº de pagos: ',numPagos) AS texto FROM formas_pago $where ORDER BY forma",$datos,'selectpicker span3 show-tick',"data-live-search='true'",'',$tipo,$conexion);
}


function enviaEmailFacturaProforma($codigoProformas){
    require_once('../../api/html2pdf/html2pdf.class.php');
    $documentos=array();
    $i=1;

    conexionBD();

    foreach ($codigoProformas as $codigoProforma) {
        $codigoFactura=$codigoProforma;
        $res=consultaBD("INSERT INTO facturas_envios VALUES(NULL,".$codigoFactura.",'".date('Y-m-d')."');");
        $contenido=generaPDFFacturaProforma($codigoProforma);

        $html2pdf=new HTML2PDF('P','A4','es',true,'UTF-8',array(20,10,10,15));
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->WriteHTML($contenido[1]);
    
        $html2pdf->Output($contenido[0].'_'.$i.'.pdf','f');
        array_push($documentos, $contenido[0].'_'.$i.'.pdf');
        $i++;
    }

    $texto='la factura';
    $asunto='Factura';
    if($i>2){
        $texto='las facturas';
        $asunto='Facturas';
    }

    $factura=consultaBD("SELECT clientes.EMPEMAILPRINC AS emailCliente FROM facturas
                         INNER JOIN clientes ON facturas.codigoCliente=clientes.codigo 
                         WHERE facturas.codigo=".$codigoFactura,false,true);

    cierraBD();

    $zip = new ZipArchive();
    $nameZip = 'facturas_proforma.zip';
    if(file_exists($nameZip)){
        unlink($nameZip);
    }
    $ficheroZip = $nameZip;
    if($zip->open($ficheroZip,ZIPARCHIVE::CREATE)===true) {
        foreach ($documentos as $doc){
            $zip->addFile($doc , $doc);
        }
    }
    if(!$zip->close()){
        echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
        echo $zip->getStatusString();
    }

    $mensaje="<i>Estimado cliente,</i><br /><br />
             Gracias por confiar en los servicios de <strong>ANESCO SERVICIO DE PREVENCIÓN</strong>. Adjunto remitimos ".$texto." proforma correspondiente al inicio del contrato suscrito con nuestra entidad.<br /><br />
             Aprovechamos para recordarle que, una vez confirmemos el pago de la misma en virtud de la forma de pago indicada en el contrato, procederemos a confirmar el alta en nuestro sistema informático y enviarle sus claves de usuario, 
             desde las que podrá acceder a la información y documentos de su apreciada organización en tiempo real. <br /><br />
             Agradeciendo de antemano su atención, reciba un cordial saludo.<br /><br />
             <img src='https://crmparapymes.com.es/anesco2/img/logo.png' /><br />
             <div style='color:#075581;font-weight:bold'>
                 <i>ANESCO SERVICIO DE PREVENCION</i><br />
                 Tlf .954.10.92.93<br />
                 info@anescoprl.es · www.anescoprl.es<br />
                 C/ Murillo 1, 2ª P. 41001 - Sevilla.
             </div>";

    $adjunto='facturas_proforma.zip';
    
    $res=enviaEmail($factura['emailCliente'],$asunto.' proforma - Anesco',$mensaje,$adjunto);
    
    //unlink($nombre);

    return $res;
}


function generaPDFFacturaProforma($codigoFactura){
    $nombreFichero=array();
    
    $datos=consultaBD("SELECT facturas.*, serie, clientes.EMPNOMBRE AS cliente, clientes.EMPCIF AS cifCliente, clientes.EMPDIR AS domicilioCliente, 
                       clientes.EMPCP AS cpCliente, clientes.EMPLOC AS localidadCliente, clientes.EMPPROV AS provinciaCliente,
                       cuentas_propias.iban AS cuentaEmisor,
                       facturas.numero, clientes.EMPIBAN,
                       clientes.EMPCP, clientes.EMPID, contratos.fechaInicio, contratos.fechaFin, contratos.codigoInterno, IFNULL(formas_pago.codigo,'Pago') AS formaPago,
                       contratos.incrementoIPC, contratos.incrementoIpcAplicado
                       
                       FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
                       LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
                       LEFT JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
                       LEFT JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
                       LEFT JOIN formas_pago ON contratos.codigoFormaPago=formas_pago.codigo
                       LEFT JOIN cuentas_propias ON contratos.codigoCuentaPropiaContrato=cuentas_propias.codigo
                       
                       WHERE facturas.codigo=$codigoFactura
                       
                       GROUP BY facturas.codigo",false,true);

    $emisor=consultaBD("SELECT emisores.razonSocial AS emisor, emisores.cif AS cifEmisor, emisores.domicilio AS domicilioEmisor,
                        emisores.cp AS cpEmisor, emisores.localidad AS localidadEmisor, emisores.provincia AS provinciaEmisor,
                        emisores.telefono, emisores.fax, emisores.web, emisores.email, emisores.registro, emisores.ficheroLogo, 
                        emisores.iban, emisores.registro AS registroPie
                       
                        FROM emisores 
                       
                        WHERE codigo=1",false,true);

    $referenciaContrato=formateaReferenciaContrato($datos,$datos);

    $nombreFichero[0]='PROFORMA-'.trim(limpiaCadena($datos['cliente']));
    $conceptos=obtieneConceptosFactura($codigoFactura,$datos,$referenciaContrato,$datos['total'],$datos['baseImponible']);

    $vencimientos=obtieneVencimientosFactura($datos['codigo'],$datos['EMPIBAN'],$datos['cuentaEmisor'],$datos['formaPago'],$datos);

    $observaciones=formateaObservacionesFactura($datos['observaciones']);

    $logo='../documentos/emisores/'.trim($emisor['ficheroLogo']);
    if(!file_exists($logo)){//Para evitar que la factura no se genere por no encontrar la imagen
        $logo='../img/logo.png';
    }

    $contenido = "
    <style type='text/css'>
    <!--
        .tablaCabecera{
            width:100%;
        }

        .tablaCabecera td{
            width:50%;
        }

        .tablaCabecera .emisor{
            text-align:right;
            font-weight:bold;
            line-height:15px;
        }

        .tablaEmisorCliente{
            margin-top:10px;
            font-size:12px;
            width:100%;
            border-collapse:collapse;
        }

        .tablaBottom{
            top:740px;
            position: absolute;
        }

        .tablaEmisorCliente td{
            border-bottom:1px solid #000;
        }

        .celdaEmisor{
            width:49%;
            padding:10px 30px 1px 30px;
            border:1px solid #000;
        }

        .celdaCliente{
            border:1px solid #000;
            width:49%;
            padding:10px 30px 1px 30px;
            line-height:15px;
            text-align:center;
        }

        .celdaTabla{
            padding:0px;
            width:49%;
        }
        .celdaBlanca{
            width:2%;
            border-top:0px;
            border-bottom:0px;
        }

        .cajaNumero{
            margin-top:20px;
            font-size:12px;
        }

        .cajaFecha{
            text-align:right;
            margin-right:240px;
        }

        .cabeceraConceptos, .tablaConceptos{
            margin-top:20px;
            border:1px solid #000;
            width:100%;
        }
        .celdaConcepto{
            width:85%;
            padding:10px;
        }
        .celdaImporte{
            width:15%;
            text-align:right;
            padding:10px;
        }

        .tablaConceptos{
            border-collapse:collapse;
            margin-top:0px;
        }

        .tablaConceptos td, .tablaConceptos th{
            font-size:12px;
            border:1px solid #000;
            line-height:18px;
        }

        .tablaConceptos th{
            background:#EEE;
        }

        .tablaConceptos .celdaConcepto{
            border-left:1px solid #000;
        }

        .tablaConceptos .celdaImporte{
            border-right:1px solid #000;
        }

        .tablaTotales{
            width:100%;
        }

        .tablaTotales .celdaConcepto{
            width:60%;
        }
        .tablaTotales .celdaImporte{
            width:40%;
        }

        .cajaMedioPago{
            max-width:100%;
            font-size:12px;
            line-height:18px;
            margin-top:5px;
        }

        .cajaMedioPago ol{
            margin-top:-15px;
        }

        .pie{
            font-size:10px;
            text-align:center;
            line-height:18px;
            color:#0062B3;
        }

        .textoAdvertencia{
            font-size:11px;
            text-align:justify;
            font-weight:bold;
        }
        .centrado{
            text-align:center;
        }

        .cajaObservaciones{
            width:100%;
        }

        .logo{
            width:30%;
        }

        .titulo{
            background:#CCC;
            width:100%;
            text-align:center;
            margin:0px;
            padding:0px;
            font-weight:bold;
            font-size:28px;
            font-style:italic;
        }

        .tablaDatos{
            width:100%;
            border-collapse: collapse;
        }

        .tablaDatos th{
            font-weight:normal;
            border:1px solid #000;
            background:#EEE;
            padding:5px;
        }

        .tablaDatos td{
            font-weight:normal;
            border:1px solid #000;
            padding:5px;
        }

        .tablaDatos .a5{
            width:5%;
            text-align:center;
        }

        .tablaDatos .a9{
            width:9%;
            text-align:center;
        }

        .tablaDatos .a17{
            width:17%;
            text-align:center;
        }

        .tablaDatos .a35{
            width:35%;
        }

        .tablaDatos .a100{
            width:100%;
            padding:10px;
        }

        .centro{
            text-align:center;
        }

        ul li{
            font-size:14px;
        }

        .h200{
            height:200px;
        }

        .textoLateralFactura{
            position:absolute;
            height:60%;
            left:-50px;
            top:300px;
        }

        .enlacePie{
            display:block;
            position:absolute;
            bottom:20px;
            left:33%;
            text-align:center;
            color:#0062B3;
            font-size:11px;
            line-height:14px;
        }

        .enlacePie a{
            text-decoration:none;
            color:#0062B3;
        }
    -->
    </style>
    <page footer='page'>

        <table class='tablaCabecera'>
            <tr>
                <td><img src='".$logo."' class='logo' /><br /></td>
                <td class='emisor'>
                    ".$emisor['emisor']."<br />
                    ".$emisor['cifEmisor']."<br/>
                    ".$emisor['domicilioEmisor']."<br />
                    ".$emisor['cpEmisor']." ".$emisor['localidadEmisor']."<br/>".$emisor['provinciaEmisor']."<br />
                    
                </td>
            </tr>
        </table>
        <br/>
        <div class='titulo'>
            FACTURA
        </div>
        <table class='tablaEmisorCliente'>
            <tr>
                <td>Factura</td>
                <td class='celdaBlanca'></td>
                <td>Cliente</td>
            </tr>
            <tr>
                <td class='celdaEmisor'>
                    Factura: <strong>".$datos['serie']." - PROFORMA</strong><br />
                    Fecha: <strong>".formateaFechaWeb($datos['fecha'])."</strong><br />
                    Contrato: <strong>".$referenciaContrato."</strong>
                </td>
                <td class='celdaBlanca' style='border-right:1px solid #000'></td>
                <td class='celdaCliente'>
                    <strong>".$datos['cliente']."</strong><br />
                    ".$datos['domicilioCliente']."<br />
                    ".$datos['cpCliente']." ".$datos['localidadCliente']." (".$datos['provinciaCliente'].")<br />
                    CIF: ".$datos['cifCliente']."                    
                </td>
            </tr>
        </table>
        <br/>
        <table class='tablaDatos'>
            <tr>
                <th class='a5 centro'>Ud.</th>
                <th class='a35'>Concepto</th>
                <th class='a17 centro'>Sujeto I.V.A</th>
                <th class='a17 centro'>Exento I.V.A</th>
                <th class='a9 centro'>I.V.A</th>
                <th class='a17 centro'>Total</th>
            </tr>
            ".$conceptos['html']."  
        </table>

        <table class='tablaEmisorCliente tablaBottom'>
        <tr>
                <td>Observaciones</td>
                <td class='celdaBlanca'></td>
                <td style='border-bottom:0px'></td>
        </tr>
        <tr>
            <td class='celdaEmisor'>".$observaciones."".$conceptos['rm']."</td>
            <td class='celdaBlanca' style='border-right:0px solid #000'></td>
            <td class='celdaTabla'>
                <table class='tablaConceptos tablaTotales'>
                    <tr>
                        <td class='celdaConcepto'>Exento de IVA:</td>
                        <td class='celdaImporte'>".formateaNumeroWeb($conceptos['baseExenta'])." €</td>
                    </tr>
                    <tr>
                        <td class='celdaConcepto'>Sujeto a IVA:</td>
                        <td class='celdaImporte'>".formateaNumeroWeb($conceptos['baseImponible'])." €</td>
                    </tr>
                    <tr>
                        <td class='celdaConcepto'>I.V.A. (21%):</td>
                        <td class='celdaImporte'>".formateaNumeroWeb($conceptos['importeIva'])." €</td>
                    </tr>
                    <tr>
                        <td class='celdaConcepto'></td>
                        <td class='celdaImporte'></td>
                    </tr>
                    <tr>
                        <th class='celdaConcepto'><b>Total factura:</b></th>
                        <th class='celdaImporte'>".formateaNumeroWeb($conceptos['total'])." €</th>
                    </tr>
                </table>
            </td>
        </tr>
        </table>

        <div class='cajaMedioPago'>
            <strong>
                ".$vencimientos."
            </strong>
        </div>
        <img src='../img/textoLateralFactura.png' class='textoLateralFactura' />

        <page_footer>
            <div class='enlacePie'>
                <strong><i>ANESCO SERVICIO DE PREVENCION</i></strong><br />
                Tlf .954.10.92.93 <a href='mailto:administracion@anescoprl.es'>administracion@anescoprl.es</a><br />
                <a href='http://www.anescoprl.es'>www.anescoprl.es</a>
            </div>

            <div class='pie'>
                ".$emisor['registroPie']."
                <b><i>Aviso: La presente factura no representa el inicio o validez del contrato. Para ello, la factura debe haber sido abonada.</i></b>
            </div>
        </page_footer>
    </page>";

    $nombreFichero[1]=$contenido;

    return $nombreFichero;
}


function obtieneVencimientosFactura($codigoFactura,$cuentaCliente,$cuentaEmisor,$formaPago,$contrato){
    $formaPago=consultaBD('SELECT * FROM formas_pago WHERE codigo='.$formaPago,false,true);
    $res="Forma de pago: ".$formaPago['forma'].defineFormaPago($cuentaCliente,$cuentaEmisor,$formaPago['forma']);
    //$res="Forma de pago: ";

    $consulta=consultaBD("SELECT fechaVencimiento, SUM(importe) AS importe, formas_pago.forma AS forma, formas_pago.codigo AS codigoForma, concepto FROM vencimientos_facturas LEFT JOIN formas_pago ON vencimientos_facturas.codigoFormaPago=formas_pago.codigo WHERE codigoFactura=$codigoFactura GROUP BY fechaVencimiento, forma ORDER BY fechaVencimiento;");
    $i=1;
    
    if(mysql_num_rows($consulta)>0){
        $res.='<br/>';

        while($datos=mysql_fetch_assoc($consulta)){
            $res.="&nbsp;Vencimiento ".$i.": ".formateaNumeroWeb($datos['importe'])." € el ".formateaFechaWeb($datos['fechaVencimiento']);
            if($datos['forma']!=$formaPago['forma']){
                $res.=' - '.$datos['forma'].defineFormaPago($cuentaCliente,$cuentaEmisor,$datos['forma']);
            }

            $res.='<br />';

            $i++;
        }
    }
    if($i==1){
        $contratoEnFactura=consultaBD('SELECT * FROM contratos_en_facturas WHERE codigoFactura='.$codigoFactura,false,true);
        $factura=consultaBD('SELECT * FROM contratos_en_facturas WHERE codigoContrato='.$contratoEnFactura['codigoContrato'].' AND codigoFactura!='.$codigoFactura,true,true);
        if($factura){
            $consulta=consultaBD("SELECT fechaVencimiento, SUM(importe) AS importe, formas_pago.forma AS forma, formas_pago.codigo AS codigoForma, concepto FROM vencimientos_facturas LEFT JOIN formas_pago ON vencimientos_facturas.codigoFormaPago=formas_pago.codigo WHERE codigoFactura=".$factura['codigoFactura']." GROUP BY fechaVencimiento, forma ORDER BY fechaVencimiento;",true);
            if(mysql_num_rows($consulta)>0){
                $res.='<br/>';

                while($datos=mysql_fetch_assoc($consulta)){
                    $res.="&nbsp;Vencimiento ".$i.": ".formateaNumeroWeb($datos['importe'])." € el ".formateaFechaWeb($datos['fechaVencimiento']);
                    if($datos['forma']!=$formaPago['forma']){
                        $res.=' - '.$datos['forma'].defineFormaPago($cuentaCliente,$cuentaEmisor,$datos['forma']);
                    }

                    $res.='<br />';

                    $i++;
                }
            }
        }
    }

    return $res;
}

function defineFormaPago($cuentaCliente,$cuentaEmisor,$formaPago){
    $res='';
    
    $formaPago=strtolower($formaPago);

    if(substr_count($formaPago,'recibo')>0 || substr_count($formaPago,'domiciliación')>0 || substr_count($formaPago,'transferencia')>0){

        $res='- IBAN: ';

        if(substr_count($formaPago,'recibo')>0 || substr_count($formaPago,'domiciliación')>0){
            
            if($cuentaCliente==''){
                $res.='No registrado';
            } 
            else {
                $dato=substr($cuentaCliente, 20);
                $res.='**** **** **** **** **** '.$dato;
            }

        } 
        else {
            
            if($cuentaEmisor==''){
                $res.='No registrado';
            } 
            else {
                $res.=$cuentaEmisor;
            }
        }
    }
    
    return $res;
}
function obtieneConceptosFactura($codigoFactura,$factura,$referenciaContrato,$totalFactura,$subtotalFactura){
    $html='';
    $rm='<br/><br/>* No incluye los reconocimientos médicos que serán objeto de facturación al mes cumplido de su realización';
    $baseImponible=0;
    $importeIva=0;
    $baseExenta=0;
    $total=0;
    $anexo='';
    $consulta=consultaBD("SELECT ofertas.total, ofertas.subtotal, ofertas.subtotalRM, ofertas.opcion, ofertas.otraOpcion, ofertas.numRM, ofertas.importe_iva, ofertas.iva, ofertas.especialidadTecnica,
                          contratos.incrementoIPC, contratos.incrementoIpcAplicado
                          FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
                          INNER JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
                          WHERE codigoFactura=$codigoFactura");

    while($datos=mysql_fetch_assoc($consulta)){
        //Parte de actividades
        $html.="<tr>
                    <td class='a5'>1</td>
                    <td class='a35'>";


            if($factura['serie']=='RM' && $datos['opcion']!=4){
                $html.="Reconocimientos médicos sujetos al contrato:<br />
                        Nº de contrato: ".$referenciaContrato."<br />
                        Vigencia desde: ".formateaFechaWeb($factura['fechaInicio']).' hasta '.formateaFechaWeb($factura['fechaFin'])."<br />";
                $rm='';
            }
            elseif($datos['opcion']==1){
                $html.="Cuota única de Servicio de Prevención Ajeno con:<br />
                        Nº de contrato: ".$referenciaContrato."<br />
                        Vigencia desde: ".formateaFechaWeb($factura['fechaInicio']).' hasta '.formateaFechaWeb($factura['fechaFin'])."<br />
                        Concierto Servicio de Prevención Ajeno para las 4 especialidades:<br />
                        Seguridad en el Trabajo,<br />
                        Higiene Industrial,<br />
                        Ergonomía y Psicosociología,<br />
                        Vigilancia de la Salud*";
            }
            elseif($datos['opcion']==2){
                $html.="Cuota única de Servicio de Prevención Ajeno con:<br />
                        Nº de contrato: ".$referenciaContrato."<br />
                        Vigencia desde: ".formateaFechaWeb($factura['fechaInicio']).' hasta '.formateaFechaWeb($factura['fechaFin'])."<br />
                        Concierto Servicio de Prevención Ajeno para Vigilancia de la Salud*";
            }
            elseif($datos['opcion']==3){
                $especialidades=explode('&{}&',$datos['especialidadTecnica']);
                $especialidades=implode(', ',$especialidades);

                $html.="Cuota única de Servicio de Prevención Ajeno con:<br />
                        Nº de contrato: ".$referenciaContrato."<br />
                        Vigencia desde: ".formateaFechaWeb($factura['fechaInicio']).' hasta '.formateaFechaWeb($factura['fechaFin'])."<br />
                        Concierto Servicio de Prevención Ajeno para la especialidad de ".$especialidades;
            }
            elseif($datos['opcion']==4){
                $html.='Cuota única de otras actuaciones: ';
                if(strlen($datos['otraOpcion'])>900){
                    $html.='<br/>* Debido al tamaño de la descripción del concepto, esté ira indicado en un anexo junto a la factura';
                    $anexo='<tr><td class="a100">'.nl2br($datos['otraOpcion']).'</td></tr>';
                } else {
                    $html.=nl2br($datos['otraOpcion']);
                }

                $rm='';
            }

        $html.="    </td>";


        if (isset($datos['subtotalRM']) && $factura['codigo']>4248) { // El ID 4248 es el de la última factura emitida previa a la actualización masiva de los IPCs
            $datos['subtotalRM'] = compruebaImporteContrato($datos['subtotalRM'], $datos, false);
        }

        if($factura['codigo']>4248){ // El ID 4248 es el de la última factura emitida previa a la actualización masiva de los IPCs
            $datos['subtotal'] = compruebaImporteContrato($datos['subtotal'],$datos,false);
            $datos['importe_iva'] = compruebaImporteContrato($datos['importe_iva'],$datos,false);
        }
        else{
            $datos['subtotal'] = $factura['baseImponible'];
            $datos['total'] = $factura['total'];
            $datos['importe_iva'] = $datos['total'] - $datos['subtotal'];
        }

        if($factura['serie']=='RM' && $datos['opcion']!=4 && $totalFactura>=$datos['subtotalRM'] && $datos['subtotalRM']>0){//Caso de factura RM normal
            $html.="<td class='a17'></td><td class='a17'>".formateaNumeroWeb($datos['subtotalRM'])."</td><td class='a9'></td><td class='a17'>".formateaNumeroWeb($datos['subtotalRM'])."</td>";
            $baseExenta+=$datos['subtotalRM'];
            $total+=$datos['subtotalRM']+$importeIva;
        } 
        elseif($factura['serie']=='RM' && $datos['opcion']!=4 && $totalFactura>=$datos['subtotalRM'] && $datos['subtotalRM']==0){//Caso de factura RM sin reconocimientos médicos incluidos
            $html.="<td class='a17'></td><td class='a17'>".formateaNumeroWeb($totalFactura)."</td><td class='a9'></td><td class='a17'>".formateaNumeroWeb($totalFactura)."</td>";
            $baseExenta+=$totalFactura;
            $total+=$totalFactura+$importeIva;
        } 
        elseif($factura['serie']=='RM' && $datos['opcion']!=4 && $totalFactura<$datos['subtotalRM']){//Caso de contrato de RM partido en varias facturas
            $html.="<td class='a17'></td><td class='a17'>".formateaNumeroWeb($totalFactura)."</td><td class='a9'></td><td class='a17'>".formateaNumeroWeb($totalFactura)."</td>";
            $baseExenta+=$totalFactura;
            $total+=$totalFactura+$importeIva;
        } 
        elseif($totalFactura<$datos['total']){//Caso de contrato de servicios partido en varias facturas
            
            if($datos['iva']=='SI'){
                $html.="<td class='a17'>".formateaNumeroWeb($subtotalFactura)."</td><td class='a17'></td><td class='a9'></td><td class='a17'>".formateaNumeroWeb($totalFactura)."</td>";
                $baseImponible+=$subtotalFactura;
                $importeIva+=($totalFactura-$subtotalFactura);
                $total+=$totalFactura;
            }
            else{
                $html.="<td class='a17'></td><td class='a17'>".formateaNumeroWeb($totalFactura)."</td><td class='a9'></td><td class='a17'>".formateaNumeroWeb($totalFactura)."</td>";
                $baseExenta+=$totalFactura;
                $total+=$totalFactura;
            }
            
        } 
        elseif($datos['iva']=='SI'){//Factura de servicios normal con IVA
            $totalIva=floatval($datos['subtotal'])+floatval($datos['importe_iva']);
            $html.="<td class='a17'>".formateaNumeroWeb($datos['subtotal'])."</td><td class='a17'></td><td class='a9'>".formateaNumeroWeb($datos['importe_iva'])."</td><td class='a17'>".formateaNumeroWeb($totalIva)."</td>";
            $baseImponible+=$datos['subtotal'];
            $importeIva+=$datos['importe_iva'];
            $total+=$datos['subtotal']+$importeIva;
        } 
        else {//Factura de servicios normal sin IVA
            $html.="<td class='a17'></td><td class='a17'>".formateaNumeroWeb($datos['subtotal'])."</td><td class='a9'></td><td class='a17'>".formateaNumeroWeb($datos['subtotal'])."</td>";
            //$baseImponible+=$datos['subtotal'];
            $baseExenta+=$totalFactura;
            $importeIva+=$datos['importe_iva'];
            $total+=$datos['subtotal']+$importeIva;
        }

        $html.="</tr>";

        //Fin parte de actividades
        
        //$html.="<tr><td colspan='6' class='a100 h200'></td></tr>";
    }

    $res=array(
        'html'=>$html,
        'rm'=>$rm,
        'baseImponible'=>$baseImponible,
        'importeIva'=>$importeIva,
        'baseExenta'=>$baseExenta,
        'total'=>$total,
        'anexo'=>$anexo
    );

    return $res;
}


function obtieneConceptosLibreFacturaPDF($datos){
    $sujetoIva='0';
    $exentoIva=$datos['baseImponible'];
    $iva='0';

    if($datos['baseImponible']!=$datos['total']){
        $diferencia=$datos['total']-$datos['baseImponible'];

        $sujetoIva=$datos['baseImponible'];
        $exentoIva='0';
        $iva=$diferencia;
    }

    $conceptos="
    <tr>
        <td class='a5'>1</td>
        <td class='a35'>".nl2br($datos['conceptoManualFactura'])."</td>
        <td class='a17'>".formateaNumeroWeb($sujetoIva)." €</td>
        <td class='a17'>".formateaNumeroWeb($exentoIva)." €</td>
        <td class='a9'>".formateaNumeroWeb($iva)." €</td>
        <td class='a17'>".formateaNumeroWeb($datos['total'])." €</td>
    </tr>";


    $res=array(
        'html'=>$conceptos,
        'rm'=>'',
        'baseImponible'=>$datos['baseImponible'],
        'importeIva'=>$iva,
        'baseExenta'=>$exentoIva,
        'total'=>$datos['total'],
        'anexo'=>''
    );

    return $res;
}

//Fin parte común entre contratos, facturas y abonos

//Parte común entre ofertas y contratos

function obtieneNombreServicioContratado($opcion){
    $res=array(1=>'SPA 4 Especialidades',2=>'SPA Vigilancia de la salud',3=>'SPA Especialidades técnicas',4=>'Otras actuaciones',''=>'-');

    return $res[$opcion];
}

function campoSelectCuentaPropia($datos,$nombreCampo='codigoCuentaPropiaContrato'){
    echo "<div class='hide' id='cajaCuentaPropia'>";
    campoSelectConsulta($nombreCampo,'Cuenta a utilizar',"SELECT codigo, nombre AS texto FROM cuentas_propias ORDER BY nombre",$datos);
    echo "</div>";
}


function compruebaFormaPagoSelectorCuentas(){
    $res='NO';
    $datos=arrayFormulario();
    extract($datos);

    //$consulta=consultaBD("SELECT mostrarCuentas FROM formas_pago WHERE codigo='$codigoFormaPago';",true,true);
    if(in_array($codigoFormaPago, array(1,4))){
        $res='SI';
    }

    echo $res;
}

//Fin parte común entre ofertas y contratos

//Parte común entre facturas y proformas

function formateaImportesFacturas(){
    $datos=arrayFormulario();

    $_POST['baseImponible']=formateaNumeroWeb($datos['baseImponible'],true);
    $_POST['total']=formateaNumeroWeb($datos['total'],true);

    $cobrada='SI';
    for($i=0;isset($datos['codigoEstadoRecibo'.$i]) && $cobrada=='SI';$i++){
        if($datos['codigoEstadoRecibo'.$i]!='2'){
            $cobrada='NO';
        }
    }

    $_POST['facturaCobrada']=$cobrada;
}

function consultaNumeroSerieFactura(){
    $serie=$_POST['codigoSerieFactura'];
    $ejercicio=obtieneEjercicioParaWhere();

    conexionBD();
    $facturas=consultaBD("SELECT IFNULL(MAX(numero),0) AS numero FROM facturas WHERE eliminado='NO' AND YEAR(fecha)='$ejercicio' AND codigoSerieFactura=$serie AND tipoFactura!='PROFORMA';",false,true);
    $facturasRecos=consultaBD("SELECT IFNULL(MAX(numero),0) AS numero FROM facturas_reconocimientos_medicos WHERE eliminado='NO' AND YEAR(fecha)='$ejercicio' AND codigoSerieFactura=$serie;",false,true);
    cierraBD();

    $numero=$facturas['numero']+1;
    if($facturasRecos['numero']>$numero){
        $numero=$facturasRecos['numero']+1;
    }

    echo $numero;
}

function consultaNumeroSerieFacturaReconocimientos(){
    consultaNumeroSerieFactura();
}


function consultaNumeroSerieFactura2($serie,$conexion=true,$ejercicio=false){
    if(!$ejercicio){
        $ejercicio=obtieneEjercicioParaWhere();
    }

    $facturas=consultaBD("SELECT IFNULL(MAX(numero),0) AS numero FROM facturas WHERE eliminado='NO' AND YEAR(fecha)='$ejercicio' AND codigoSerieFactura='$serie' AND tipoFactura!='PROFORMA';",$conexion,true);
    $facturasRecos=consultaBD("SELECT IFNULL(MAX(numero),0) AS numero FROM facturas_reconocimientos_medicos WHERE eliminado='NO' AND YEAR(fecha)='$ejercicio' AND codigoSerieFactura='$serie';",$conexion,true);

    $numero=$facturas['numero']+1;
    if($facturasRecos['numero']>$numero){
        $numero=$facturasRecos['numero']+1;
    }

    return $numero;
}


function consultaNumeroSerieFacturaReconocimientos2($codigoSerie,$conexion=false){
    $numero=consultaNumeroSerieFactura2($codigoSerie,$conexion);

    return $numero;
}

function abreVentanaGestionConBotones($titulo,$destino,$claseCampos='',$icono='icon-edit',$margen='',$fichero=false,$claseFormulario='',$destinoVolver='index.php',$botonGuardar=true,$texto='Guardar',$iconoGuardar='icon-check',$botonVolver=true,$textoVolver='Volver',$iconoVolver='icon-chevron-left'){
    $modoTransmision='';
    if($fichero){
        $modoTransmision='enctype="multipart/form-data"';
    }

    echo "
    <div class='main' id='contenido'>
      <div class='main-inner'>
        <div class='container'>
          <div class='row'>

          <div class='span12 $margen'>
            <div class='widget'>
                <div class='widget-header'> <i class='icon-list'></i><i class='icon-chevron-right separadorSeccion'></i><i class='$icono'></i>
                  <h3>$titulo</h3>
                  <div class='pull-right'>";

                    if($botonVolver){
                        echo "<a href='$destinoVolver' class='btn btn-small btn-default'><i class='$iconoVolver'></i> $textoVolver</a> &nbsp;";
                    }

                    if($botonGuardar){                      
                        echo "<button type='button' onclick=\"$(':submit').click();\" class='btn btn-small btn-propio'><i class='$iconoGuardar'></i> $texto</button>";
                    }

    echo "         </div>
                </div>
                <!-- /widget-header -->
                <div class='widget-content'>
                  
                  <div class='tab-pane' id='formcontrols'>
                    <form id='edit-profile' class='form-horizontal $claseFormulario' action='$destino' method='post' $modoTransmision>
                      <fieldset class='$claseCampos'>";
}

function abreVentanaGestionConBotonesEvaluacion($titulo,$destino,$claseCampos='',$icono='icon-edit',$margen='',$fichero=false,$claseFormulario='',$destinoVolver='index.php',$botonGuardar=true,$texto='Guardar',$iconoGuardar='icon-check',$botonVolver=true,$textoVolver='Volver',$iconoVolver='icon-chevron-left'){
    $modoTransmision='';
    if($fichero){
        $modoTransmision='enctype="multipart/form-data"';
    }

    echo "
    <div class='main' id='contenido'>
      <div class='main-inner'>
        <div class='container'>
          <div class='row'>

          <div class='span12 $margen'>
            <div class='widget'>
                <div class='widget-header'> <i class='icon-list'></i><i class='icon-chevron-right separadorSeccion'></i><i class='$icono'></i>
                  <h3>$titulo</h3>
                  <div class='pull-right'>";

                    if(!isset($_GET['duplicar'])){
                        if(isset($_REQUEST['codigo'])){
                            $url='preseleccion.php?codigo='.$_REQUEST['codigo'];
                        } else {
                            $url='preseleccion.php';
                        }
                        echo "<a href='".$url."' class='btn btn-small btn-default'><i class='$iconoVolver'></i> Volver a selección de riesgos</a> &nbsp;";
                    }

                    if($botonVolver){
                        echo "<a href='$destinoVolver' class='btn btn-small btn-default'><i class='$iconoVolver'></i> $textoVolver</a> &nbsp;";
                    }

                    if($botonGuardar){                      
                        echo "<button type='button' onclick=\"$(':submit').click();\" class='btn btn-small btn-propio'><i class='$iconoGuardar'></i> $texto</button>";
                    }

    echo "         </div>
                </div>
                <!-- /widget-header -->
                <div class='widget-content'>
                  
                  <div class='tab-pane' id='formcontrols'>
                    <form id='edit-profile' class='form-horizontal $claseFormulario' action='$destino' method='post' $modoTransmision>
                      <fieldset class='$claseCampos'>";
}

//Fin parte común entre facturas y proformas

//Parte de actualización automática de vencimientos a los 4 días de envío a remesa

function compruebaVencimientosRemesas(){
    /*$res=consultaBD("UPDATE vencimientos_facturas, vencimientos_en_remesas, remesas
                     SET vencimientos_facturas.estado='PAGADO'
                     WHERE vencimientos_facturas.codigo=vencimientos_en_remesas.codigoVencimiento
                     AND vencimientos_en_remesas.codigoRemesa=remesas.codigo
                     AND remesas.fecha=DATE_SUB(CURDATE(),INTERVAL 4 DAY)
                     AND remesas.eliminado='NO'",true);*/
    $res=true;

    return $res;
}

//Fin parte de actualización automática de vencimientos a los 4 días de envío a remesa

//Parte común entre reconocimientos médicos y puestos

function creaTablaEpis($codigo,$tabla='epis_de_puesto_trabajo',$campoCodigo='codigoPuestoTrabajo'){
        echo "
        <div class='control-group'>                     
            <label class='control-label'>EPIs:</label>
            <div class='controls'>
                <table class='tabla-simple mitadAncho' id='tablaEpis'>
                    <thead>
                        <tr>
                            <th> EPIs </th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>";
            
                $i=0;

                conexionBD();

                if($codigo!=false){
                    $consulta=consultaBD("SELECT codigoEpi FROM $tabla WHERE $campoCodigo=".$codigo);
                    while($epi=mysql_fetch_assoc($consulta)){
                        imprimeLineaTablaEpi($i,$epi);
                        $i++;
                    }
                }
                
                if($i==0){
                    imprimeLineaTablaEpi(0);
                }

                cierraBD();
          
        echo "      </tbody>
                </table>
                <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaEpis\");'><i class='icon-plus'></i> Añadir EPI</button> 
                <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaEpis\");'><i class='icon-trash'></i> Eliminar EPI</button>
            </div>
        </div>";
}

function imprimeLineaTablaEpi($i,$datos=false){
    $j=$i+1;

    echo "<tr>";
        campoSelectConsulta('codigoEpi'.$i,'',"SELECT codigo, nombre AS texto FROM epis ORDER BY nombre;",$datos['codigoEpi'],'selectpicker span6 show-tick','data-live-search="true"','',1,false);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function creaTablaOtrasPruebas($codigo){
        echo "
        <div class='control-group'>                     
            <label class='control-label'>Estudio de pruebas funcionales:</label>
            <div class='controls'>
                <table class='tabla-simple' id='tablaOtrasPruebas'>
                    <thead>
                        <tr>
                            <th> Otras pruebas </th>
                            <th> Precio </th>
                            <th> Resultado </th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>";
            
                $i=0;

                conexionBD();

                if($codigo!=false){
                    $consulta=consultaBD("SELECT * FROM reconocimientos_medicos_otras_pruebas WHERE codigoRM=".$codigo);
                    while($item=mysql_fetch_assoc($consulta)){
                        imprimeLineaTablaOtrasPruebas($i,$item);
                        $i++;
                    }
                }
                
                if($i==0){
                    imprimeLineaTablaOtrasPruebas(0);
                }

                cierraBD();
          
        echo "      </tbody>
                </table>
                <button type='button' class='btn btn-small btn-success' onclick='insertaPrueba(\"tablaOtrasPruebas\");'><i class='icon-plus'></i> Añadir Prueba</button> 
                <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaOtrasPruebas\");'><i class='icon-trash'></i> Eliminar Prueba</button>
            </div>
        </div>";
}

function imprimeLineaTablaOtrasPruebas($i,$datos=false){
    $j=$i+1;

    echo "<tr>";
            
            campoSelectDesplegable('otrasPruebas_'.$i,'',$datos['texto'],'selectpicker span5 show-tick selectInserccion',"data-live-search='true'",'',1);
            campoTextoSimbolo('valoracionEconomica'.$i,'','€',formateaNumeroWeb($datos['valoracionEconomica']),'input-mini pagination-right',1);
            areaTextoTabla('resultado'.$i,$datos['resultado']); 

    echo "<td><input type='checkbox' name='filasTabla[]' value='$j' /></td>
    </tr>";
}


function campoChecksClasificacion($nombreCampo,$datos){
    campoCheck($nombreCampo,'Protocolo clasificación',$datos,array('General','Conductores','Cargas mov. repetidos', 'Posturas forzadas','Neuropatia por presión','Ruido','Altura','Dermatosis','Asma laboral','Asma laboral: Soldador','Silicosis y otras neumoconiosis'),array('SI','SI','SI','SI','SI','SI','SI','SI','SI','SI','SI'),true);
}

//Fin parte común entre reconocimientos médicos y puestos

function campoSelectMultiple($nombreCampo,$texto,$nombres,$valores,$valor=false,$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'",$tipo=0){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    if($tipo==0){
        echo "
        <div class='control-group'>                     
            <label class='control-label' for='$nombreCampo'>$texto:</label>
            <div class='controls'>";
    }
    elseif($tipo==1){
        echo "<td>";
    }
    
    if($valor != ''){
        $select=explode('&{}&',$valor);
    } else {
        $select = '';
    }

    echo "<select multiple name='".$nombreCampo."[]' id='".$nombreCampo."' class='$clase selectMultiple' $busqueda>";
        
    for($i=0;$i<count($nombres);$i++){
        echo "<option value='".$valores[$i]."'";

        if($select!='' && in_array($valores[$i], $select)){
            echo " selected='selected'";
        }

        echo ">".$nombres[$i]."</option>";
    }
        
    echo "</select>";

    if($tipo==0){
        echo "
            </div> <!-- /controls -->       
        </div> <!-- /control-group -->";
    }
    elseif($tipo==1){
        echo "</td>";
    }
}


function campoFechaObligatoria($nombreCampo,$texto,$valor=false,$solo=false,$disabled=false){//Modificada por Jose Luis el 15/09/2014 (añadido opción disabled).
    if(!$valor){
        $valor=fecha();
    }
    else{
        $valor=compruebaValorCampo($valor,$nombreCampo);
        $valor=formateaFechaWeb($valor);
    }

    if($solo){
        campoTextoSolo($nombreCampo,$valor,'input-small datepicker hasDatepicker obligatorio',$disabled);
    }
    else{
        campoTexto($nombreCampo,$texto,$valor,'input-small datepicker hasDatepicker obligatorio',$disabled);
    }
}


//Parte común entre ofertas y correos

function generaPDFOferta($codigo){
    $datos=datosRegistro('ofertas',$codigo);
    $cliente=datosRegistro('clientes',$datos['codigoCliente']);
    $comercial=datosRegistro('usuarios',$datos['codigoUsuario']);

    $cuentaPropia=obtieneCuentaPropiaParaPdfOferta($datos['codigoCuentaPropiaOferta']);

    if($comercial['nombre']==NULL){
        $comercial['nombre']='Departamento';
        $comercial['apellidos']='Comercial';
    }

    $consultaCentros=consultaBD('SELECT * FROM clientes_centros WHERE codigoCliente='.$cliente['codigo'].' AND activo="SI" AND eliminado="NO"',true);
    $centros=mysql_num_rows($consultaCentros);
    $riesgos=array('BAJO'=>'../img/checkOferta.png','MEDIO'=>'../img/checkOferta.png','ALTO'=>'../img/checkOferta.png');
    $riesgos['BAJO']='../img/checkOfertaMarcado.png'; 
    $anexo=array('SI'=>'../img/checkOferta.png','NO'=>'../img/checkOferta.png');
    $anexo[$cliente['EMPANEXO']]='../img/checkOfertaMarcado.png';
    $subtotal1='';
    $iva1='';
    $total1='';
    $subtotal2='';
    $iva2='';
    $total2=''; 
    $observaciones='';
    if($datos['opcion']=='1' || $datos['opcion']=='4'){
        $subtotal1=$datos['subtotal'];
        $iva1=$datos['importe_iva'];
        $total1=$subtotal1+$iva1; 
        $subtotal2=$datos['importeRM'];
        $iva2='0';
        $total2=$datos['subtotalRM']; 
        $observaciones=$datos['observaciones'];
    } elseif($datos['opcion']=='2'){
        $subtotal1=$datos['subtotalRM'];
        $subtotal2=$datos['subtotal'];
        $total1=$datos['subtotalRM'];
        $total2=$datos['total']; 
        $iva2=$datos['importe_iva'];
        $observaciones=$datos['observaciones'];
    } elseif($datos['opcion']=='3'){
        $subtotal1=$datos['subtotal'];
        $total1=$datos['total']; 
        $iva1=$total1-$subtotal1;
        $observaciones=$datos['observaciones'];
    }
    $total=$total1+$total2;
    $formasPago=array('domiciliacion'=>'../img/checkOferta.png','transferencia'=>'../img/checkOferta.png','otros'=>'../img/checkOferta.png');
    $otraForma='';
    $iban=array('','','','','','');
    /*if($datos['formaPago']=='cheque' || $datos['formaPago']=='efectivo' || $datos['formaPago']=='face'){
        $formasPago['otros']='../img/checkOfertaMarcado.png';
        if($datos['formaPago']=='cheque'){
            $otraForma='Cheque a la vista';
        } else if($datos['formaPago']=='efectivo'){
            $otraForma='Efectivo';
        } else {
            $otraForma='FACe';
        }
    } else {
        $formasPago[$datos['formaPago']]='../img/checkOfertaMarcado.png';
        $iban=array(0,0,0,0,0,0);
        if($datos['formaPago']=='domiciliacion'){
            $iban = str_split($cliente['EMPIBAN'], 4);
            $tamanio=count($iban);
            if($tamanio<6){
                for($i=$tamanio;$i<6;$i++){
                    $iban[$i]='';
                }
            }
        }
    }*/

    $especialidadesContratadas=obtieneEspecialidadesParaPDFOferta($datos,$subtotal1,$iva1,$total1,$subtotal2,$iva2,$total2,$datos['numEmpleados']);

    $contenido = "
    <style type='text/css'>
    <!--
            body{
                font-size:12px;
                font-family: helvetica;
                font-weight: lighter;
                line-height: 24px;
            }

            #container{
                background: url(../img/fondoOferta2.png) center center no-repeat;
                height:100%;
                margin-top:0px;
            }

            .logo img{
                width:80px;
            }

            h1{
                font-size:14px;
                text-align:center;
                color:#075581;
            }

            h2{
                font-size:14px;
                text-align:left;
                margin-top:-10px;
                margin-bottom:-10px;
                color:#075581;
                padding:0px;
                float:left;
                display:inline;
            }

            .alineaRight{
                float:right;
            }

            .sinBorde{
                margin-left:230px;
                width:70%;

            }

            .contratante{
                width:98%;
                border:1px solid #000;
                border-collapse:collapse;
                font-size:11px;
            }

            .contratadas{
                text-align:center;
            }


            .contratante td{
                border:1px solid #000;
                padding:3px 2px;
                width:12%;
                vertical-align:middle;
            }

            .contratante .titulo{
                background:#919EB1;
                font-weigh:bold;
            }

            th{
                text-align:center;
                color:#FFF;
                background:#4E6998;
                border:1px solid #000;
                padding:3px;
                width:100%;
            }

            table .titulo{
                background:#ADC0CE;
                font-weight:bold;
            }

            .a3{
                width:2%;
            }
            .a5{
                width:5%;
            }
            .a6{
                width:10.5%;
            }
            .a12{
                width:12%;
            }
            .a13{
                width:12.5%;
            }
            .a15{
                width:15%;
            }
            .a17{
                width:17%;
            }
            .a20{
                width:20%;
            }
            .a24{
                width:24.5%;
            }
            .a25{
                width:25.1%;
            }
            .a26{
                width:26%;
            }
            .a30{
                width:30%;
            }
            .a34{
                width:34%;
            }
            .a35{
                width:35.2%;
            }
            .a42{
                width:42%;
            }
            .a44{
                width:34%;
            }
            .a44a{
                width:44%;
            }
            .a50{
                width:50%;
            }
            .a76{
                width:76%;
            }
            .a80{
                width:80%;
            }

            .sinBorderLeft{
                border-left:0px;
            }

            .sinBorderLeftBottom{
                border-left:0px;
                border-bottom:0px;
            }

            .observaciones{
                border:1px solid #000;
                width:98%;
                height:auto;
                margin-bottom:8px;
                padding:3px;
            }   

            .pie{
                font-size:11px;
            }    

            .ofertaPresentada{
                position:absolute;
                top:70px;
                right:20px;
                line-height:24px;
            }

            .ofertaPresentada h1{
                text-align:right;
                margin-top:-30px;
            }
            
            .aliIzq{
                text-align:left;
            }

            .descripcionOtrasActuaciones{
                text-align:left;
                width:100%;
            }

            .celdasTransparentes{
                border:none;
            }
        }
    -->
    </style>
    <page footer='page'>
        <div id='container'>
            <div class='logo'>
                <img src='../img/logoOferta.png' />
            </div>

            <div class='ofertaPresentada'>
                <h1>
                    Nº DE OFERTA: ".$datos['codigoInterno']." <br />
                    PRESENTADA POR: ".$comercial['nombre']." ".$comercial['apellidos']."<br />
                    FECHA: ".formateaFechaWeb($datos['fecha'])."
                </h1>
            </div>
            <h1>OFERTA PARA LA PRESTACIÓN DE SERVICIO DE PREVENCIÓN AJENO</h1>

            <table class='contratante'>
            <tr>
                <th colspan='8'>DATOS DE LA EMPRESA CONTRATANTE</th>
            </tr>
            <tr>
                <td class='titulo a20'>RAZÓN SOCIAL</td>
                <td class='a80' colspan='7'>".strtoupper($cliente['EMPNOMBRE'])."</td>
            </tr>
            <tr>
                <td class='titulo a20'>CIF</td>
                <td class='a80' colspan='7'>".$cliente['EMPCIF']."</td>
            </tr>
            <tr>
                <td class='titulo a20'>REPRESENTANTE LEGAL</td>
                <td class='a34' colspan='4'>".strtoupper($cliente['EMPRL'])."</td>
                <td class='titulo a20'>DNI</td>
                <td class='a24' colspan='2'>".$cliente['EMPRLDNI']."</td>
            </tr>
            <tr>
                <td class='titulo a20'>DIRECCIÓN</td>
                <td class='a80' colspan='7'>".$cliente['EMPDIR']."</td>
            </tr>
            <tr>
                <td class='titulo a20'>POBLACIÓN - CP</td>
                <td class='a34' colspan='4'>".$cliente['EMPLOC']." - ".$cliente['EMPCP']."</td>
                <td class='titulo a20'>PROVINCIA</td>
                <td class='a24' colspan='2'>".$cliente['EMPPROV']."</td>
            </tr>
            <tr>
                <td class='titulo a20'>TELÉFONO</td>
                <td class='a17' colspan='2'>".$cliente['EMPTELPRINC']."</td>
                <td class='titulo a27' colspan='2'>E-MAIL</td>
                <td class='a44' colspan='3'>".$cliente['EMPEMAILPRINC']."</td>
            </tr>
            <tr>
                <td class='titulo a20'>PERSONA CONTACTO</td>
                <td class='a80' colspan='7'>".$cliente['EMPPC']."</td>
            </tr>
            <tr>
                <td class='titulo a20'>TELÉFONO</td>
                <td class='a17' colspan='2'>".$cliente['EMPPCTEL']."</td>
                <td class='titulo a27' colspan='2'>E-MAIL</td>
                <td class='a44' colspan='3'>".$cliente['EMPPCEMAIL']."</td>
            </tr>
            <tr>
                <td class='titulo a20'>ACTIVIDAD</td>
                <td class='a34' colspan='4'>".defineActividad($cliente['EMPACTIVIDAD'])."</td>
                <td class='titulo a20'>CNAE</td>
                <td class='a24' colspan='2'>".$cliente['EMPCNAE']."</td>
            </tr>
            <tr>
                <td class='titulo a20'>Nº CENTROS TRABAJO</td>
                <td class='a5'>".$centros."</td>
                <td class='titulo a12'>Nº DE TRAB</td>
                <td class='a5'>".$cliente['EMPNTRAB']."</td>
                <td class='titulo a12'>ANEXO I</td>
                <td class='a20'><img style='width:12px;height:12px' src='".$anexo['SI']."'>SI <img style='width:12px;height:12px' src='".$anexo['NO']."'>NO</td>
                <td class='a12'></td>
                <td class='a12'></td>
            </tr>";
            $i=1;
            while($centro=mysql_fetch_assoc($consultaCentros)){
                $contenido.="<tr>
                                <td class='titulo a20'>DIRECCIÓN ".$i."</td>
                                <td class='a80' colspan='7'>".$centro['direccion']."</td>
                            </tr>";
                $i++;
            }

$contenido.="</table>

            <table class='contratante contratadas'>";

            if($datos['opcion']!=4){
                $contenido.="
                <tr>
                    <th colspan='6'>ACTIVIDADES CONTRATADAS</th>
                </tr>
                <tr>
                    <td class='a30 titulo' colspan='2'>ESPECIALIDADES OFERTADAS</td>
                    <td class='a12 titulo'>Nº DE TRAB.</td>
                    <td class='a34 titulo'>IMPORTE (€)</td>
                    <td class='a12 titulo'>IVA (€)</td>
                    <td class='a13 titulo'>TOTAL (€)</td>
                </tr>";
            }
            else{
                $contenido.="
                <tr>
                    <th colspan='4'>ACTIVIDADES CONTRATADAS</th>
                </tr>
                <tr>
                    <td colspan='4' class='descripcionOtrasActuaciones'><div>".formateaTextoOtraOpcionOferta($datos['otraOpcion'])."</div></td>
                </tr>
                <tr>
                    <td class='a25 titulo'>Nº DE TRAB.</td>
                    <td class='a25 titulo'>IMPORTE (€)</td>
                    <td class='a25 titulo'>IVA (€)</td>
                    <td class='a25 titulo'>TOTAL (€)</td>
                </tr>";
            }

        $contenido.=$especialidadesContratadas;

            if($datos['opcion']!=4){
                $contenido.="
                <tr>
                    <td colspan='4' class='a76 sinBorderLeftBottom'></td>
                    <td class='a12'><b>TOTAL (€)</b></td>
                    <td class='a13'>".formateaNumeroWeb($total)."</td>
                </tr>";
            }
            else{
                $contenido.="
                <tr>
                    <td colspan='2' class='a50 sinBorderLeftBottom'></td>
                    <td class='a12'><b>TOTAL (€)</b></td>
                    <td class='a13'>".formateaNumeroWeb($total)."</td>
                </tr>"; 
            }
            $contenido.="
            </table>";

        if($datos['opcion']==4){
            $contenido.="
            <br /><br />";
        }

        $contenido.="
            <table class='contratante contratadas'>
            <tr>
                <th colspan='9'>MEDIO DE PAGO (MARCAR LA OPCIÓN SELECCIONADA)</th>
            </tr>
            <tr>
                <td class='a3'><img style='width:12px;height:12px' src='".$formasPago['domiciliacion']."'></td>
                <td class='a24'>DOMICILIACIÓN BANCARIA</td>
                <td class='a6'>IBAN</td>
                <td class='a6'>".$iban[0]."</td>
                <td class='a6'>".$iban[1]."</td>
                <td class='a6'>".$iban[2]."</td>
                <td class='a6'>".$iban[3]."</td>
                <td class='a6'>".$iban[4]."</td>
                <td class='a6'>".$iban[5]."</td>
            </tr>
            <tr>
                <td class='a3'><img style='width:12px;height:12px' src='".$formasPago['transferencia']."'></td>
                <td class='a24'>TRANSFERENCIA BANCARIA</td>
                <td class='a6'>IBAN</td>
                <td class='a6'>".$cuentaPropia[0]."</td>
                <td class='a6'>".$cuentaPropia[1]."</td>
                <td class='a6'>".$cuentaPropia[2]."</td>
                <td class='a6'>".$cuentaPropia[3]."</td>
                <td class='a6'>".$cuentaPropia[4]."</td>
                <td class='a6'>".$cuentaPropia[5]."</td>
            </tr>
            <tr>
                <td class='a3'><img style='width:12px;height:12px' src='".$formasPago['otros']."'></td>
                <td class='a24'>OTRAS MODALIDADES DE PAGO</td>
                <td class='a42' colspan='7' style='text-align:left;'>".$otraForma."</td>
            </tr>
            </table><br/>";
            if($observaciones != ''){
                $contenido.="<b><h2>OBSERVACIONES:</h2></b>
                <div class='observaciones'>
                ".$observaciones."
                </div>";
            }
$contenido.="<table style='width:100%;'>
            <tr>
            <td style='width:13%;'>
                <h2>Por el cliente: </h2><br/>
                <h2>Nombre:</h2><br/>
                <h2>Cargo:</h2><br/>
                <h2>Fecha:</h2><br/>
            </td>
            <td style='width:37%;'>
                <span style='margin-top:-10px;'><b>".strtoupper($cliente['EMPNOMBRE'])."</b></span>
            </td>
            <td style='width:50%;'>
                <h2>Firma:</h2><br/>
            </td>
            </tr></table>
        </div>";
        
    $contenido.="
    <page_footer>";

    if($datos['opcion']==1 || $datos['opcion']==2){
        $contenido.="
        <div class='pie'>
            La presente oferta incluye ".$datos['numRM']." reconocimiento/s médico/s; los reconocimientos médicos extra se facturarán a razón de ".formateaNumeroWeb($datos['importeRMExtra'])." euros cada uno.    
            Este precio es relativo a la realización del RM en las instalaciones de ANESCO SALUD Y PREVENCIÓN en C/ Murillo 1 2ª planta. Sevilla, 41001.
        </div>
        <br />";
    }
    
    $contenido.="
        <div class='pie'>
            A los efectos de lo establecido en la Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y garantía de los derechos digitales, le informamos que puede acceder a nuestra política de privacidad completa en  <a href='http://anescoprl.es/politica-de-privacidad-y-aviso-legal/'>http://anescoprl.es/politica-de-privacidad-y-aviso-legal/</a> donde se indica, además, de forma clara y sencilla como ejercitar los derechos de Acceso, Cancelación, Rectificación, Oposición y Portabilidad. 
        </div>
    </page_footer>";


    $contenido.="</page>";

    return $contenido;
}


function formateaTextoOtraOpcionOferta($texto){
    $texto=nl2br($texto);

    $saltoPagina="
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class='a25 celdasTransparentes'>&nbsp;</td>
                    <td class='a25 celdasTransparentes'>&nbsp;</td>
                    <td class='a25 celdasTransparentes'>&nbsp;</td>
                    <td class='a25 celdasTransparentes'>&nbsp;</td>
                </tr>
            </table>
        </div>
    </page>
    <page footer='page'>
        <div id='container'>
            <table class='contratante contratadas'>
                <tr>
                    <td colspan='4' class='descripcionOtrasActuaciones'>
                        <div>";

    $texto=str_replace('[salto_pagina]',$saltoPagina,$texto);

    return $texto;
}


function obtieneEspecialidadesParaPDFOferta($datos,$subtotal1,$iva1,$total1,$subtotal2,$iva2,$total2,$numTrabajadores){
    $res='';
    
    if($datos['opcion']==1){
        $res="  <tr>
                    <td class='a15 titulo' rowspan='4'>ESPECIALIDADES TÉCNICAS</td>
                    <td class='a15 titulo'>Seguridad en el trabajo (ST)</td>
                    <td class='a12'>".$numTrabajadores."</td>
                    <td class='a34' rowspan='5'>".formateaNumeroWeb($subtotal1)."</td>
                    <td class='a12' rowspan='5'>".formateaNumeroWeb($iva1)."</td>
                    <td class='a13' rowspan='5'>".formateaNumeroWeb($total1)."</td>
                </tr>
                <tr>
                    <td class='a15 titulo sinBorderLeft'>Higiene industrial (HI)</td>
                    <td class='a12'>".$numTrabajadores."</td>
                </tr>
                <tr>
                    <td class='a15 titulo sinBorderLeft'>Ergonomía (E)</td>
                    <td class='a12'>".$numTrabajadores."</td>
                </tr>
                <tr>
                    <td class='a15 titulo sinBorderLeft'>Psicosociología aplicada (PA)</td>
                    <td class='a12'>".$numTrabajadores."</td>
                </tr>
                <tr>
                    <td class='a15 titulo' rowspan='4'>VIGILANCIA DE LA SALUD</td>
                    <td class='a15 titulo'>Vigilancia Salud Colectiva (VSC)</td>
                    <td class='a12'>".$numTrabajadores."</td>
                </tr>
                <tr>
                    <td class='a15 titulo sinBorderLeft'>Vigilancia Salud Individual (VSI)</td>
                    <td class='a12' rowspan='2'>".$datos['numRM']."</td>
                    <td class='a34' rowspan='2'>".formateaNumeroWeb($subtotal2)."</td>
                    <td class='a12' rowspan='2'>".formateaNumeroWeb($iva2)."</td>
                    <td class='a13' rowspan='2'>".formateaNumeroWeb($total2)."</td>
                </tr>
                <tr>
                    <td class='a15 titulo sinBorderLeft'>Reconocimiento Médico (RM)</td>
                </tr>
                <tr>
                    <td class='a15 titulo sinBorderLeft'>Importe por RM no incluido en contrato</td>
                    <td class='a12'>A DEMANDA</td>
                    <td class='a34'>".formateaNumeroWeb($datos['importeRMExtra'])."</td>
                    <td class='a12'>N/A</td>
                    <td class='a13'></td>
                </tr>";
    }
    elseif($datos['opcion']==2){
        $subtotal2=$total2/1.21;
        $iva2=$total2-$subtotal2;
        $baseImponibleVS=$total1;//*$datos['numRM'];
        $res="  <tr>
                    <td class='a15 titulo' rowspan='4'>VIGILANCIA DE LA SALUD</td>
                    <td class='a15 titulo'>Vigilancia Salud Colectiva (VSC)</td>
                    <td class='a12'>".$numTrabajadores."</td>
                    <td class='a34'>".formateaNumeroWeb($subtotal2)."</td>
                    <td class='a12'>".formateaNumeroWeb($iva2)."</td>
                    <td class='a13'>".formateaNumeroWeb($total2)."</td>
                </tr>
                <tr>
                    <td class='a15 titulo sinBorderLeft'>Vigilancia Salud Individual (VSI)</td>
                    <td class='a12' rowspan='2'>".$datos['numRM']."</td>
                    <td class='a34' rowspan='2'>".formateaNumeroWeb($baseImponibleVS)."</td>
                    <td class='a12' rowspan='2'>N/A</td>
                    <td class='a13' rowspan='2'>".formateaNumeroWeb($subtotal1)."</td>
                </tr>
                <tr>
                    <td class='a15 titulo sinBorderLeft'>Reconocimiento Médico (RM)</td>
                </tr>
                <tr>
                    <td class='a15 titulo sinBorderLeft'>Importe por RM no incluido en contrato</td>
                    <td class='a12'>A DEMANDA</td>
                    <td class='a34'>".formateaNumeroWeb($datos['importeRMExtra'])."</td>
                    <td class='a12'>N/A</td>
                    <td class='a13'></td>
                </tr>";
    }
    elseif($datos['opcion']==3){
        
        $especialidadesTecnicas=explode('&{}&',$datos['especialidadTecnica']);
        
        $rowspan=count($especialidadesTecnicas);
        $rowspanYaAplicado=false;

        if(in_array('Ergonomía y Psicosociología',$especialidadesTecnicas)){
            $rowspan++;//Porque esta especialidad tiene su propio rowspan='2'
            $rowspanYaAplicado=true;

            $res="<tr>
                    <td class='a15 titulo' rowspan='".$rowspan."'>ESPECIALIDADES TÉCNICAS</td>
                    <td class='a15 titulo sinBorderLeft'>Ergonomía (E)</td>
                    <td class='a12'>".$numTrabajadores."</td>
                    <td class='a34' rowspan='".$rowspan."'>".formateaNumeroWeb($subtotal1)."</td>
                    <td class='a12' rowspan='".$rowspan."'>".formateaNumeroWeb($iva1)."</td>
                    <td class='a13' rowspan='".$rowspan."'>".formateaNumeroWeb($total1)."</td>
                </tr>
                <tr>
                    <td class='a15 titulo sinBorderLeft'>Psicosociología aplicada (PA)</td>
                    <td class='a12'>".$numTrabajadores."</td>

                </tr>";
        }


        if(in_array('Seguridad en el Trabajo',$especialidadesTecnicas)){
            $rowspanActual='';
            
            if(!$rowspanYaAplicado){
                $rowspanActual="
                <td class='a34' rowspan='$rowspan'>".formateaNumeroWeb($subtotal1)."</td>
                <td class='a12' rowspan='$rowspan'>".formateaNumeroWeb($iva1)."</td>
                <td class='a13' rowspan='$rowspan'>".formateaNumeroWeb($total1)."</td>";
            }

            $res.=" <tr>";
            if(!$rowspanYaAplicado){
                $res.="<td class='a15 titulo' rowspan='".$rowspan."'>ESPECIALIDADES TÉCNICAS</td>";
                $rowspanYaAplicado=true;
            }
            $res.="  <td class='a15 titulo'>Seguridad en el trabajo (ST)</td>
                    <td class='a12'>".$numTrabajadores."</td>
                    ".$rowspanActual."
                </tr>";
        }

        if(in_array('Higiene Industrial',$especialidadesTecnicas)){
            $rowspanActual='';
            
            if(!$rowspanYaAplicado){
                $rowspanActual="
                <td class='a34'>".formateaNumeroWeb($subtotal1)."</td>
                <td class='a12'>".formateaNumeroWeb($iva1)."</td>
                <td class='a13'>".formateaNumeroWeb($total1)."</td>";
            }

            $res.=" <tr>";
            if(!$rowspanYaAplicado){
                $res.="<td class='a15 titulo' rowspan='".$rowspan."'>ESPECIALIDADES TÉCNICAS</td>";
                $rowspanYaAplicado=true;
            }
            $res.="
                    <td class='a15 titulo sinBorderLeft'>Higiene industrial (HI)</td>
                    <td class='a12'>".$numTrabajadores."</td>
                    ".$rowspanActual."
                </tr>";
        }

    }
    else{
        $res="  <tr>
                    <td class='a25'>".$numTrabajadores."</td>
                    <td class='a25'>".formateaNumeroWeb($subtotal1)."</td>
                    <td class='a25'>".formateaNumeroWeb($iva1)."</td>
                    <td class='a25'>".formateaNumeroWeb($total1)."</td>
                </tr>";
    }

    return $res;
}

function obtieneCuentaPropiaParaPdfOferta($codigoCuentaPropia){
    $res=array();

    if($codigoCuentaPropia==NULL){
        $res[0]='ES52';
        $res[1]='3187';
        $res[2]='0812';
        $res[3]='8733';
        $res[4]='0804';
        $res[5]='5321';
    }
    else{
        $cuenta=consultaBD("SELECT iban FROM cuentas_propias WHERE codigo='$codigoCuentaPropia';",true,true);
        $iban=str_replace(' ','',$cuenta['iban']);

        $res[0]=substr($iban,0,4);
        $res[1]=substr($iban,4,4);
        $res[2]=substr($iban,8,4);
        $res[3]=substr($iban,12,4);
        $res[4]=substr($iban,16,4);
        $res[5]=substr($iban,20);
    }

    return $res;
}

//Fin parte común entre ofertas y correos


function obtieneNombreTarea($nombreTarea){
    $res=$nombreTarea;

    if(is_numeric($nombreTarea)){
        $consulta=consultaBD("SELECT nombre FROM tipos_tareas WHERE codigo='$nombreTarea';",false,true);    
        $res=$consulta['nombre'];
    }

    return $res;
}


function cambiaEstadoEliminado($tabla,$estado){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();
    for($i=0;isset($datos['codigo'.$i]);$i++){
        $res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],$tabla,false);
    }
    cierraBD();

    return $res;   
}

function obtieneEmpleadosClienteParaAjax(){
    $res="<option value='NULL'></option>";

    $datos=arrayFormulario();

    if($datos['codigoCliente']!='NULL'){
        $consulta=consultaBD("SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM empleados WHERE aprobado='SI' AND eliminado='NO' AND codigoCliente='".$datos['codigoCliente']."' ORDER BY nombre, apellidos;",true);
        while($empleado=mysql_fetch_assoc($consulta)){
            $res.="<option value='".$empleado['codigo']."'";
            if($datos['codigoEmpleado']>0 && $datos['codigoEmpleado']==$empleado['codigo']){
                $res.=" selected";
            }
            $res.=">".$empleado['texto']."</option>";
        }
    }

    echo $res;
}

function imprimeClientesInicio(){
    global $_CONFIG;

    conexionBD();

    if($_SESSION['tipoUsuario']=='TECNICO'){
        $consulta=consultaBD("SELECT clientes.codigo, clientes.activo, clientes.EMPID, EMPNOMBRE AS razonSocial, EMPMARCA AS nombreComercial, COUNT(ofertas.codigo) AS ofertas, COUNT(contratos.codigo) AS contratos, COUNT(formularioPRL.codigo) AS planificaciones, COUNT(facturas.codigo) AS facturas
            FROM clientes
            LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
            LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta
            LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
            LEFT JOIN facturas ON clientes.codigo=facturas.codigoCliente
            WHERE (contratos.tecnico=".$_SESSION['codigoU']." || formularioPRL.codigoUsuarioTecnico=".$_SESSION['codigoU'].") GROUP BY clientes.codigo ORDER BY EMPID;");
    } else {

        $consulta=consultaBD("SELECT clientes.codigo, clientes.activo, EMPID, EMPNOMBRE AS razonSocial, EMPMARCA AS nombreComercial, COUNT(DISTINCT ofertas.codigo) AS ofertas, COUNT(DISTINCT contratos.codigo) AS contratos, COUNT(DISTINCT formularioPRL.codigo) AS planificaciones, COUNT(DISTINCT facturas.codigo) AS facturas
            FROM clientes
            LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
            LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta
            LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
            LEFT JOIN facturas ON clientes.codigo=facturas.codigoCliente
            GROUP BY clientes.codigo ORDER BY EMPID;");
    }
    $iconoValidado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
    while($datos=mysql_fetch_assoc($consulta)){
        echo "
            <tr>
                <td>".$datos['EMPID']."</td>
                <td>".$datos['razonSocial']."</td>
                <td>".$datos['nombreComercial']."</td>
                <td class='centro'>".$datos['ofertas']."</td>
                <td class='centro'>".$datos['contratos']."</td>
                <td class='centro'>".$datos['planificaciones']."</td>
                <td class='centro'>".$datos['facturas']."</td>
                <td class='centro'>".$iconoValidado[$datos['activo']]."</td>
            </tr>";
    }
    cierraBD();
}

function imprimeTareasInicio(){
    //$codigoS=$_SESSION['codigoS'];

    conexionBD();

    $where=defineWhereEmpleadoTarea();
    $where.=" AND estado!='REALIZADA' AND fechaInicio<='".date('Y-m-d')."'";

    $consulta=consultaBD("SELECT tareas.codigo, clientes.EMPNOMBRE, clientes.EMPTELPRINC, clientes.EMPLOC, tareas.tarea, tareas.fechaInicio, tareas.estado, tareas.prioridad, tareas.codigoUsuario 
        FROM clientes RIGHT JOIN tareas ON clientes.codigo=tareas.codigoCliente $where ORDER BY tareas.fechaInicio DESC;");
    
    $prioridad=array('NORMAL'=>"<span class='label label-info'>Normal</span>",'ALTA'=>"<span class='label label-danger'>Alta</span>",'BAJA'=>"<span class='label'>Baja</span>");

    while($datos=mysql_fetch_assoc($consulta)){
        $tarea=obtieneNombreTarea($datos['tarea']);
        $fecha=formateaFechaWeb($datos['fechaInicio']);
        $tecnico=consultaBD("SELECT * FROM usuarios WHERE codigo='".$datos['codigoUsuario']."';",false,true);

        echo "
        <tr>
            <td> $fecha </td>
            <td> ".$datos['EMPNOMBRE']." </td>
            <td> ".$datos['EMPLOC']." </td>
            <td class='nowrap'> ".formateaTelefono($datos['EMPTELPRINC'])." </td>
            <td> ".$tarea." </td>
            <td> ".$tecnico['nombre']." ".$tecnico['apellidos']." </td>
            <td> ".$prioridad[$datos['prioridad']]." </td>
            <td class='centro'>
                <a href='tareas/gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio noAjax'><i class='icon-search-plus'></i> Ver datos</i></a>
            </td>";
    }
    cierraBD();
}

function calculaEdadEmpleado($fechaNacimiento){
    $res='';

    if($fechaNacimiento!='' && $fechaNacimiento!='0000-00-00'){
        $fechaNacimiento=str_replace('-','',$fechaNacimiento);
        $fechaActual=date('Ymd');

        $diferencia=$fechaActual-$fechaNacimiento;

        $res=substr($diferencia,0,2);
    }

    return $res;
}

function cambiaEstadoEliminadoItem($estado,$tabla){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();
    for($i=0;isset($datos['codigo'.$i]);$i++){
        $res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],$tabla,false);
    }
    cierraBD();

    return $res;
}

function sumaTiempo($tiempo,$masTiempo){
    $tiempo=explode(':', $tiempo);
    $masTiempo=explode(':', $masTiempo);
    $tiempo[0]=intval($tiempo[0]);
    $tiempo[1]=intval($tiempo[1]);
    $masTiempo[0]=intval($masTiempo[0]);
    
    if(!isset($masTiempo[1])){
        $masTiempo[1]='0';
    }

    $masTiempo[1]=intval($masTiempo[1]);
    $masTiempo[1]=$masTiempo[1]+$tiempo[1];
    while($masTiempo[1]>=60){
        $masTiempo[0]++;
        $masTiempo[1]=$masTiempo[1]-60;
    }
    $horas=$tiempo[0]+$masTiempo[0];
    $min=$masTiempo[1];
    $horas=$horas<10?'0'.$horas:$horas;
    $min=$min<10?'0'.$min:$min;
    return $horas.':'.$min;
}

function formateaHoraPrevista($horas){
    $horas=$horas<10?'0'.$horas:$horas;
    return $horas.':00';
}

function compararTiempos($tiempo1,$tiempo2){
    $clase='tiempoNoPasado';
    $cumplido='SI';
    $tiempo1=explode(':', $tiempo1);
    $tiempo2=explode(':', $tiempo2);
    $tiempo1[0]=isset($tiempo1[0])?intval($tiempo1[0]):0;
    $tiempo1[1]=isset($tiempo1[1])?intval($tiempo1[1]):0;
    $tiempo2[0]=isset($tiempo2[0])?intval($tiempo2[0]):0;
    $tiempo2[1]=isset($tiempo2[1])?intval($tiempo2[1]):0;
    if($tiempo1[0]<$tiempo2[0]){
        $clase='tiempoPasado';
        $cumplido='NO';
    } else if($tiempo1[0]==$tiempo2[0] && $tiempo1[1]<$tiempo2[1]){
        $clase='tiempoPasado';
        $cumplido='NO';
    }
    $res=array();
    $res['clase']=$clase;
    $res['cumplido']=$cumplido;
    return $res;
}

function insertaCentros($cliente){
    $res=true;//Siempre que vayamos a usarlo con un AND, inicializarlo a true

    conexionBD();
    $i=1;
    $centrosExisten=array();
    while(isset($_POST['codigoCentro'.$i])){
        //echo $_POST['codigoCentro'.$i].'<br/>';
        array_push($centrosExisten, $_POST['codigoCentro'.$i]);
        $i++;
    }
    $centros=consultaBD('SELECT * FROM clientes_centros WHERE codigoCliente='.$cliente,false);
    while($centro=mysql_fetch_assoc($centros)){
        if(!in_array($centro['codigo'], $centrosExisten)){
            $res=consultaBD('DELETE FROM clientes_centros WHERE codigo='.$centro['codigo']);
            //echo 'DELETE FROM clientes_centros WHERE codigo='.$centro['codigo'].'<br/>';
        }
    }
    $i=1;
    while(isset($_POST['direccion'.$i])){
        if($_POST['codigoCentro'.$i]==''){
            $res=$res && consultaBD("INSERT INTO clientes_centros VALUES(NULL, '$cliente','".$_POST['direccion'.$i]."','".$_POST['cp'.$i]."','".$_POST['localidad'.$i]."','".$_POST['provincia'.$i]."','".$_POST['nombre'.$i]."','".$_POST['activo'.$i]."','".$_POST['trabajadores'.$i]."')");
            //echo "INSERT INTO clientes_centros VALUES(NULL, '$cliente','".$_POST['direccion'.$i]."','".$_POST['cp'.$i]."','".$_POST['localidad'.$i]."','".$_POST['provincia'.$i]."','".$_POST['nombre'.$i]."','".$_POST['activo'.$i]."','".$_POST['trabajadores'.$i]."')<br/>";
        } else {
            $res=$res && consultaBD("UPDATE clientes_centros SET direccion='".$_POST['direccion'.$i]."',cp='".$_POST['cp'.$i]."',localidad='".$_POST['localidad'.$i]."',provincia='".$_POST['provincia'.$i]."',nombre='".$_POST['nombre'.$i]."',activo='".$_POST['activo'.$i]."',trabajadores='".$_POST['trabajadores'.$i]."' WHERE codigo=".$_POST['codigoCentro'.$i]);
            //echo "UPDATE clientes_centros SET direccion='".$_POST['direccion'.$i]."',cp='".$_POST['cp'.$i]."',localidad='".$_POST['localidad'.$i]."',provincia='".$_POST['provincia'.$i]."',nombre='".$_POST['nombre'.$i]."',activo='".$_POST['activo'.$i]."',trabajadores='".$_POST['trabajadores'.$i]."' WHERE codigo=".$_POST['codigoCentro'.$i]."<br/>";
        }
        $i++;
    }

    cierraBD();

    return $res;
}


function campoLogo($nombreCampo,$texto,$tipo=0,$valor=false,$ruta=false,$nombreDescarga='',$claseContenedor=''){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    if(!$valor || $valor=='NO'){
        if($tipo==0){
            echo "
            <div class='control-group $claseContenedor' id='contenedor-$nombreCampo'>                     
                <label class='control-label' for='$nombreCampo'>$texto:</label>
                <div class='controls'>
                    <input type='file' name='$nombreCampo' id='$nombreCampo' /><div class='tip'>Solo se admiten ficheros de 5 MB como máximo</div>
                </div> <!-- /controls -->       
            </div> <!-- /control-group -->";
        }
        elseif($tipo==1){
            echo "<td class='$claseContenedor' id='contenedor-$nombreCampo'><input type='file' name='$nombreCampo' id='$nombreCampo' /></td>";
        }
        else{
            echo "<span class='$claseContenedor' id='contenedor-$nombreCampo'><input type='file' name='$nombreCampo' id='$nombreCampo' /></span>";
        }
    }
    else{
        campoDescargaLogo($nombreCampo,$texto,$ruta,$valor,$tipo,$nombreDescarga);
        campoOculto($valor,$nombreCampo);
    }
}

function campoDescargaLogo($nombreCampo,$texto,$ruta,$valor,$tipo=0,$nombreDescarga=''){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    
    if($nombreDescarga==''){
        $nombreDescarga=$valor;
    }

    
    if($valor=='NO' || $valor==''){
        campoLogo($nombreCampo,$texto,$tipo,$valor,$ruta);
    }
    else{
        if($tipo==0){
            echo "
                <div class='control-group' id='contenedor-$nombreCampo'>                     
                  <label class='control-label'>$texto:</label>
                  <div class='controls'>
                    <a class='btn btn-propio noAjax' href='$ruta$valor' target='_blank'><i class='icon-cloud-download'></i> $nombreDescarga</a>
                    <button type='button' class='iconoBorrado' id='elimina-$nombreCampo'><i class='icon-trash'></i> Eliminar</button>
                  </div> <!-- /controls -->       
                </div> <!-- /control-group -->";
        }
        elseif($tipo==1){
            echo "<td><a class='btn btn-propio noAjax descargaFichero' href='$ruta$valor' target='_blank' nombre='$nombreCampo'><i class='icon-cloud-download'></i> $nombreDescarga</a></td>";
        }
        else{
            echo "<a class='btn btn-propio noAjax' href='$ruta$valor' target='_blank'><i class='icon-cloud-download'></i> $nombreDescarga</a>";
        }

        campoLogo($nombreCampo.'Oculto',$texto,$tipo,false,$ruta,'','hide');
    }
}

function campoMesPlanificacion($nombreCampo,$datos=false){
    $valor=date('m');
    if($datos){
        $valor=$datos['mesVS'];
    }
    campoSelect($nombreCampo,'Mes',array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'),array('00','01','02','03','04','05','06','07','08','09','10','11','12'),$valor,'selectpicker span1 show-tick');
}

function campoAnioPlanificacion($nombreCampo,$datos=false){
    $valor=date('Y');
    if($datos){
        $valor=$datos['anioVS'];
    }
    $anios=array();
    for($i=2016;$i<=date('Y')+2;$i++){
        array_push($anios, $i);
    }
    campoSelect($nombreCampo,'Año',$anios,$anios,$valor,'selectpicker span1 show-tick');
}

function generaZip($documentos,$correo=false){
    $zip = new ZipArchive();
    $nameZip = '../documentos/documentacion.zip';
    if(file_exists($nameZip)){
        unlink($nameZip);
    }
    $fichero = $nameZip;
    if($zip->open($fichero,ZIPARCHIVE::CREATE)===true) {
        foreach ($documentos as $documento){
            if(is_array($documento)){
                foreach ($documento as $doc){
                    $name = str_replace('../documentos/', '', $doc);
                    $zip->addFile($doc , $name);
                }
            } else {
                $name = str_replace('../documentos/', '', $documento);
                $zip->addFile($documento , $name);
            }
        }
    }
    if(!$zip->close()){
        echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
        echo $zip->getStatusString();
    }

    if(!$correo){
        header("Content-Type: application/zip");
        header("Content-Disposition: attachment; filename=documentacion.zip");
        header("Content-Transfer-Encoding: binary");

        readfile($nameZip);
    } else {
        return $nameZip;
    }
}

//Parte de documentación interna
function campoCheckEtiqueta($nombreCampo,$textoCampo,$color='#FFFFFF',$valorCampo='SI',$salto=true){

    echo "<label style='color:#FFF;padding-right:3px;padding-left:3px;background-color:".$color.";'class='checkbox inline'>
            <input type='checkbox' name='$nombreCampo' id='$nombreCampo' value='".$valorCampo."'><i class='icon-tag'></i> ".$textoCampo."</label>";

    if($salto){
        echo "<br />";
    }
}
//Fin parte de documentación interna

//Parte común entre clientes y posibles clientes
function botonEnvioCredenciales($datos){
    if($datos){
        echo "
        <div class='control-group'>                     
            <div class='controls datoSinInput'>
                <button class='btn btn-propio' type='submit' name='enviaCredenciales'><i class='icon-send'></i> Enviar credenciales</button>
            </div> <!-- /controls -->       
        </div> <!-- /control-group -->";
    }
}

function enviaCredencialesCliente(){
    global $_CONFIG;
    $res=actualizaDatos('clientes');

    $datos=arrayFormulario();
    $empresa=$datos['EMPNOMBRE'];
    $destinatario=$datos['EMPEMAILPRINC'];
    $usuario=$datos['usuario'];
    $clave=$datos['clave'];

    if($destinatario==''){
        $res=false;
        mensajeAdvertencia('La empresa no tiene definido un correo principal al que enviar las credenciales.');
    }
    else{

        $mensaje="
        <i>Estimado cliente,</i>
        <br />
        <br />
        A continuación le remitimos las credenciales con las que podrá acceder a su nube personal en el software de ANESCO SERVICIO DE PREVENCIÓN:
        <ul>
            <li>Usuario: $usuario</li>
            <li>Clave: $clave</li>
            <li>URL de acceso: <a href='https://crmparapymes.com.es".$_CONFIG['raiz']."'>https://crmparapymes.com.es".$_CONFIG['raiz']."</a></li>
        </ul> 
        <br />
        <br />
        Aprovechamos la ocasión para enviarles un cordial saludo.
        <br />
        <br />
        <img src='https://crmparapymes.com.es/anesco2/img/logo.png' /><br />
        <div style='color:#075581;font-weight:bold'>
            <i>ANESCO SERVICIO DE PREVENCION</i><br />
            Tlf .954.10.92.93<br />
            info@anescoprl.es · www.anescoprl.es<br />
            C/ Murillo 1, 2ª P. 41001 - Sevilla.
        </div>";

       $res=enviaEmail($destinatario,'Credenciales acceso - ANESCO',$mensaje);
    }

    return $res;
}
//Fin parte común entre clientes y posibles clientes

//Parte de inicio del cliente
function obtieneCodigoCliente(){
    $res='';
    
    if(isset($_GET['codigo'])){
        $res=$_GET['codigo'];
    } else if (isset($_POST['codigoCliente'])){
        $res=$_POST['codigoCliente'];
    } else if (isset($_SESSION['codigoCliente'])){
        $res=$_SESSION['codigoCliente'];
    } else {
        $res=obtenerCodigoCliente(true);
    }

    $_SESSION['codigoCliente']=$res;

    return $res;
}

function imprimeEmpleados($cliente,$eliminado,$inicio=false,$filtroFormacion=''){
    global $_CONFIG;

    conexionBD();
    if($eliminado=='SI'){
        $eliminado="eliminado='SI' AND aprobado='SI'";
    } else {;
        $eliminado="(eliminado='NO' OR (eliminado='SI' AND aprobado!='SI'))";
    }
    $consulta=consultaBD("SELECT empleados.*, usuarioFormacion 
        FROM empleados 
        LEFT JOIN usuarios_empleados ON empleados.codigo=usuarios_empleados.codigoEmpleado
        LEFT JOIN usuarios_sincronizados ON usuarios_empleados.codigoUsuario=usuarios_sincronizados.usuarioPRL WHERE ".$eliminado." AND codigoCliente = ".$cliente." ORDER BY codigoInterno;");
    cierraBD();
    $iconoRevisado=array('SI'=>'','NO'=>'<i class="icon-times-circle iconoFactura icon-danger" title="No aprobado"></i>','PENDIENTE'=>'<i class="icon-question-circle iconoFactura icon-warning-color" title="Pendiente de aprobar"></i>');
    while($datos=mysql_fetch_assoc($consulta)){
        if($datos['eliminado']=='SI'){
            $iconoRevisado['NO']='<i class="icon-times-circle iconoFactura icon-danger" title="No aprobada su eliminación"></i>';
            $iconoRevisado['PENDIENTE']='<i class="icon-question-circle iconoFactura icon-warning-color" title="Pendiente de eliminar"></i>';
        } else {
            $iconoRevisado['NO']='<i class="icon-times-circle iconoFactura icon-danger" title="No aprobado"></i>';
            $iconoRevisado['PENDIENTE']='<i class="icon-question-circle iconoFactura icon-warning-color" title="Pendiente de aprobar"></i>';
        }
        $formacion=estaEnFormacion($datos['codigo']);
        $formado='';
        $mostrar=true;
        if($formacion){
            if($filtroFormacion=='NO'){
                $mostrar=false;
            }
            conexionBDFormacion();
                $certificado=consultaBD('SELECT * FROM puestos_empleados WHERE codigoEmpleado='.$formacion['usuarioFormacion'].' ORDER BY codigo DESC LIMIT 1',false,true);
                if($certificado['fechaCertificado']!='0000-00-00'){
                    $fecha=explode('-',$certificado['fechaCertificado']);
                    $formado=certificadoCaducado($certificado['fechaCertificado']);
                    if($filtroFormacion=='CADUCADO' && $formado==''){
                        $mostrar=false;
                    }
                } else if($filtroFormacion=='CADUCADO'){
                    $mostrar=false;
                }
            cierraBD();
            if($formado==''){
                $formado='<span class="hide">Formado</span><i class="icon-check-circle iconoFactura icon-success" title="Formado"></i>';
            }
        } else {
            if($filtroFormacion=='SI' || $filtroFormacion=='CADUCADO'){
                $mostrar=false;
            }
            $formado='<span class="hide">No formado</span><i class="icon-times-circle iconoFactura icon-danger" title="No formado"></i>';
        }
        if($mostrar){
        echo "
            <tr>
                <td class='centro'>".$datos['codigoInterno']." ".$iconoRevisado[$datos['aprobado']]."</td>
                <td>".$datos['nombre']."</td>
                <td>".$datos['apellidos']."</td>
                <td>".$datos['dni']."</td>
                <td>".$datos['sexoEmpleado']."</td>
                <td><a href='tel:".$datos['telefono']."'>".formateaTelefono($datos['telefono'])."</a></td>
                <td><a href='mailto:".$datos['email']."'>".$datos['email']."</a></td>
                <td class='centro'>".$formado."</td>";
                if(($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='TECNICO') && $datos['aprobado']!='SI'){
        echo "<td class='centro'>
                    <div class='btn-group centro'>
                        <button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span> Acciones</button>
                        <ul class='dropdown-menu' role='menu'>
                            <li><a href='".$_CONFIG['raiz']."empleados/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
                            <li class='divider'></li>
                            <li><a href='".$_CONFIG['raiz']."empleados/index.php?codigoAprobar=".$datos['codigo']."'><i class='icon-check'></i> Aprobar</i></a></li> 
                        </ul>
                    </div>
                </td>";
                } else if(($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='TECNICO' || $_SESSION['tipoUsuario']=='CLIENTE') && $datos['aprobado']=='SI' && !$formacion){
        echo "<td class='centro'>
                    <div class='btn-group centro'>
                        <button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span> Acciones</button>
                        <ul class='dropdown-menu' role='menu'>
                            <li><a href='".$_CONFIG['raiz']."empleados/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
                            <li class='divider'></li>
                            <li><a class='btnFormacion noAjax' codigoCliente='".$datos['codigoCliente']."' codigoPasar='".$datos['codigo']."' href='#'><i class='icon-graduation-cap'></i> Pasar a<br/>formación</i></a></li>
                        </ul>
                    </div>
                </td>";
            } else if(($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='TECNICO' || $_SESSION['tipoUsuario']=='CLIENTE') && $datos['aprobado']=='SI' && $formacion){
        echo "<td class='centro'>
                    <div class='btn-group centro'>
                        <button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span> Acciones</button>
                        <ul class='dropdown-menu' role='menu'>
                            <li><a href='".$_CONFIG['raiz']."empleados/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> ";
                            conexionBDFormacion();
                                $empleado=consultaBD('SELECT codigo FROM puestos_empleados WHERE (firma!="" OR firmaImagen!="NO") AND codigoEmpleado='.$datos['usuarioFormacion'],false,true);
                                if($empleado){
                                    echo "<li class='divider'></li>";
                                    echo "<li><a class='noAjax' target='_blank' href='".$_CONFIG['raiz']."empleados/generaCertificado.php?empleado=".$empleado['codigo']."'><i class='icon-download'></i> Certificado</i></a></li>";
                                }
                            cierraBD();
        echo "
                        </ul>
                    </div>
                </td>";
                } else {
        echo "  <td class='centro'>
                    <a href='".$_CONFIG['raiz']."empleados/gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
                </td>";
                }
                if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='TECNICO' || ($_SESSION['tipoUsuario']=='CLIENTE' && !$inicio)){
                echo "<td>
                    <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
                </td>";
                } else
            echo "</tr>";
        }
    }
}


function consultaPuestoEmpleadosFunciones(){
    $datos=arrayFormulario();
    $puesto=consultaBD('SELECT puestos_trabajo.nombre FROM empleados INNER JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo WHERE empleados.codigo='.$datos['codigo'],true,true);
    echo $puesto['nombre'];
}

function campoNumeroIndicador($nombreCampo,$texto,$float='none',$valor='0',$clase='input-mini pagination-right',$min='',$max=''){//ACTUALIZACIÓN 20/01/2015: añadidos los parámetros min y max, que serían de la forma: "min='1'"
    $valor=compruebaValorCampo($valor,$nombreCampo);

    $campo = "<div class='control-group' style='float:$float;margin-bottom:9px;'>";
    if($texto != ''){
        $campo .= "<label class='control-label' for='$nombreCampo'>$texto:</label>";
    } else {
        $campo .= "<label style='width:auto;margin-left:5px;margin-right:5px;' class='control-label' for='$nombreCampo'>/</label>";
    }
    $campo .= "<div class='controls'>
        <input type='number' name='$nombreCampo' id='$nombreCampo' class='$clase' value='$valor' $min $max />
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";

    echo $campo;
}

function abrePanelEstadisticas($texto='',$conCierre=true){
    if($conCierre){
    echo '</div>'; 
    }
    echo '<h1>'.$texto.'</h1>
          <div id="big_stats" class="cf">';
}

function creaEstadisticaInicio($icono,$texto,$tabla,$campo,$where,$enlace){
    global $_CONFIG;
    if(strpos($campo, 'codigo')!==false){
        $campo='COUNT('.$campo.') as total';
    } else {
        $campo='SUM('.$campo.') as total';
    }
    $datos=consultaBD('SELECT '.$campo.' FROM '.$tabla.' WHERE '.$where,true,true);
    echo '<a href="'.$_CONFIG['raiz'].$enlace.'" class="stat"> <i class="'.$icono.'"></i> <span class="value">'.$datos['total'].'</span> <br /><span>'.$texto.'</span></a>';
}

function tablaEnviosFacturas($datos){
    if($datos){
        conexionBD();
        $consulta=consultaBD("SELECT * FROM facturas_envios WHERE codigoFactura=".$datos['codigo']);
        cierraBD();
        if(mysql_num_rows($consulta)>0){
            echo "<br />
                <div class='control-group'>                     
                    <label class='control-label'>Fechas de envío:</label>
                    <div class='controls'>
                        <div class='table-responsive'>
                            <table class='table table-striped tabla-simple tablaEnvios' id='tablaEnvios'>
                                <thead>
                                    <tr>
                                        <th class='centro'> Nº de envío </th>
                                        <th> Fecha </th>
                                    </tr>
                                </thead>
                                <tbody>";
                                $i=1;
                                while($item=mysql_fetch_assoc($consulta)){
                                    echo '<tr>';
                                        echo '<td class="centro">'.$i.'º</td>';
                                        campoTextoTabla('envio'.$i,formateaFechaWeb($item['fecha']),'input-small',true);
                                    echo '</tr>';
                                    $i++;
                                }
        
            echo                "</tbody>
                            </table>
                        </div>
                    </div>
                </div><br /><br />";
        }                
    }
}

function obtienePagada($datos,$conexion=true){
    $res='NO';
    if($datos['facturaCobrada']=='SI'){
        $res='SI';
    } else {
        $vencimientos=consultaBD('SELECT COUNT(vencimientos_facturas.codigo) AS total FROM vencimientos_facturas INNER JOIN estados_recibos ON vencimientos_facturas.codigoEstadoRecibo=estados_recibos.codigo WHERE estados_recibos.nombre="PAGADO" AND codigoFactura='.$datos['codigo'],$conexion,true);
        if($vencimientos['total']>0){
            $res='PARCIAL';
        }
    }
    return $res;
}

function botonAccionesConClase($opciones,$enlaces,$iconos,$enlacesExternos=false,$celda=false,$textoBoton='Acciones',$clases=false){
    global $_CONFIG;

    $res='';

    if($celda){
        $res="<td>";
    }

    $res.="<div class='centro'>
                <div class='btn-group'>
                    <button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> $textoBoton <span class='caret'></span></button>
                    <ul class='dropdown-menu' role='menu'>";


    $clase=compruebaEnlaceExternoBotonAcciones($enlacesExternos,0);

    $res.="             <li><a href='".$_CONFIG['raiz'].$enlaces[0]."' ".$clase."><i class='".$iconos[0]."'></i><span>".$opciones[0]."</span> </a></li>";

    for($i=1;$i<count($opciones);$i++){
        $clase=compruebaEnlaceExternoBotonAcciones($enlacesExternos,$i);
        if(is_array($clases) && $clases[$i]!=''){
            if($clase==''){
                $clase="class='".$clases[$i]."'";
            } else {
                $clase=str_replace('noAjax', 'noAjax '.$clases[$i], $clase);
            }
        }
        $res.="         <li class='divider'></li>";
        $res.="         <li><a href='".$_CONFIG['raiz'].$enlaces[$i]."' ".$clase."><i class='".$iconos[$i]."'></i><span>".$opciones[$i]."</span> </a></li>";
    }

    $res.="         </ul>
                </div>
            </div>";

    if($celda){
        $res.="</td>";
    }

    return $res;
}

function creaPlanificacion($codigoFactura){
    $res=true;
    $pagada=false;
    

    $factura=consultaBD("SELECT facturas.codigo, facturaCobrada, contratos_en_facturas.codigoContrato, formularioPRL.codigo AS codigoFormulario, ofertas.opcion 
                         FROM facturas LEFT JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura 
                         LEFT JOIN formularioPRL ON contratos_en_facturas.codigoContrato=formularioPRL.codigoContrato
                         LEFT JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
                         LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
                         WHERE facturas.codigo='$codigoFactura';",false,true);
    
    $vencimientos=consultaBD("SELECT vencimientos_facturas.codigo, estados_recibos.nombre 
                              FROM vencimientos_facturas INNER JOIN estados_recibos ON vencimientos_facturas.codigoEstadoRecibo=estados_recibos.codigo 
                              WHERE codigoFactura='".$factura['codigo']."';");

    while($item=mysql_fetch_assoc($vencimientos)){
        if($item['nombre']=='PAGADO'){
            $pagada=true;
        }
    }
    
    if($pagada && $factura['codigoFormulario']==NULL && $factura['opcion']!=4){

        $res=insertaPlanificacionContrato($factura['codigoContrato']);

    }


    return $res;
}

function creaPlanificacionExcepcionContrato($codigoContrato,$conexion=true){
    $res=true;
    $datos=arrayFormulario();

    if($conexion){
        conexionBD();
    }

    if($datos['emitirPlanificacionSinPagar']=='SI'){
        $consulta=consultaBD("SELECT codigo FROM formularioPRL WHERE codigoContrato='$codigoContrato' AND eliminado='NO';");

        if(mysql_num_rows($consulta)==0){
            $res=insertaPlanificacionContrato($codigoContrato);
        }
    }

    if($conexion){
        cierraBD();
    }

    return $res;
}


function insertaPlanificacionContrato($codigoContrato){
    $res=true;
    
    $consulta=consultaBD("SELECT MAX(codigoInterno) AS numero FROM formularioPRL",false,true);
        
    $codigoInterno      =   $consulta['numero']+1;
    $fecha              =   fechaBD();
    $aprobado           =   'NO';
    $eliminado          =   'NO';

    $contrato=consultaBD("SELECT contratos.codigo, contratos.codigoInterno, contratos.fechaInicio, codigoCliente 
                          FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo  
                          WHERE contratos.codigo='$codigoContrato';",false,true);

    $codigoContrato     =   $contrato['codigo'];
    $anioAnterior       =   explode('-', $contrato['fechaInicio']);
    $anioAnterior       =   $anioAnterior[0]-1;


    $contratoAnterior=consultaBD('SELECT contratos.codigo FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo WHERE contratos.codigoInterno='.$contrato['codigoInterno'].' AND codigoCliente='.$contrato['codigoCliente'].' AND YEAR(contratos.fechaInicio)="'.$anioAnterior.'";',false,true);
    $formularioPRL=false;

    if($contratoAnterior){//Comprobación de contrato anterior
        $formularioPRL=consultaBD('SELECT * FROM formularioPRL WHERE codigo='.$contratoAnterior['codigo'],false,true);
    }

    if($formularioPRL){//Comprobación de planificación anterior: puede darse el caso de que aunque tenga contrato anterior no tenga creada una planificación asociada a él
        if($formularioPRL['codigoUsuarioTecnico']=='' || $formularioPRL['codigoUsuarioTecnico']==NULL){
            $formularioPRL['codigoUsuarioTecnico']='NULL';
        }
        $contrato=consultaBD('SELECT tecnico FROM contratos WHERE codigo='.$codigoContrato,false,true);
        $anio           =   explode('-', fechaBD());
        $anio           =   $anio[0];
        $cambios        =   array('codigoInterno'=>$codigoInterno,'codigoContrato'=>$codigoContrato,'fecha'=>$fecha,'aprobado'=>$aprobado,'eliminado'=>$eliminado,'visible'=>'SI','fechaAprobada'=>'0000-00-00','fecha'=>fechaBD(),'visitada'=>'NO','fechaVisita'=>'0000-00-00','numVisitas'=>'1','planPrev'=>'NO','fechaPlanPrev'=>'0000-00-00','pap'=>'NO','fechaPap'=>'0000-00-00','info'=>'NO','fechaInfo'=>'0000-00-00','memoria'=>'NO','fechaMemoria'=>'0000-00-00','vs'=>'NO','fechaMail'=>'0000-00-00','fechaVisitaReal'=>'0000-00-00','fechaPlanPrevReal'=>'0000-00-00','fechaPapReal'=>'0000-00-00','fechaInfoReal'=>'0000-00-00','fechaMemoriaReal'=>'0000-00-00','fechaVsReal'=>'0000-00-00','anioVs'=>$anio,'codigoUsuarioTecnico'=>$contrato['tecnico']);
        $relacionadas   =   array('extintores_formulario_prl'=>'codigoFormularioPRL','bie_formulario_prl'=>'codigoFormularioPRL','botiquines_formulario_prl'=>'codigoFormularioPRL','locales_formulario_prl'=>'codigoFormularioPRL','medios_humanos_formulario_prl'=>'codigoFormularioPRL','materias_primas_formulario_prl'=>'codigoFormularioPRL','empleados_sensibles_formulario_prl'=>'codigoFormularioPRL','instalaciones_formulario_prl'=>'codigoFormularioPRL','funciones_formulario_prl'=>'codigoFormularioPRL','maquinaria_formulario_prl'=>'codigoFormularioPRL','higienicos_formulario_prl'=>'codigoFormularioPRL','iluminacion_formulario_prl'=>'codigoFormularioPRL','gas_formulario_prl'=>'codigoFormularioPRL','evacuacion_formulario_prl'=>'codigoFormularioPRL','recursos_humanos_formulario_prl'=>'codigoFormularioPRL','recursos_tecnicos_formulario_prl'=>'codigoFormularioPRL','recursos_economicos_formulario_prl'=>'codigoFormularioPRL');
        
        $res=copiaDatos('formularioPRL',$formularioPRL,$cambios,$relacionadas);
    } 
    else{
        $contrato=consultaBD('SELECT tecnico FROM contratos WHERE codigo='.$codigoContrato,false,true);
        if($contrato['tecnico']==NULL){
            $contrato['tecnico']='NULL';
        }

        $res=consultaBD("INSERT INTO formularioPRL(codigo,codigoInterno,codigoContrato,fecha,aprobado,eliminado,codigoUsuarioTecnico,vs) VALUES(NULL,'$codigoInterno','$codigoContrato','$fecha','$aprobado','$eliminado',".$contrato['tecnico'].",'NO');");
    }

    return $res;
}

function obtieneBotonSuspendido($datos){
    $res=array('texto'=>'Suspender cliente','enlace'=>'facturas/index.php?suspender=SI&codigoCliente='.$datos['codigoCliente'],'icono'=>'icon-times','enlaceExterno'=>0,'clase'=>'');
    if($datos['suspendido']=='SI'){
        $res['texto']='Dar de alta';
        $res['enlace']='facturas/index.php?suspender=NO&codigoCliente='.$datos['codigoCliente'];
        $res['icono']='icon-check';
    }
    return $res;
}

function copiaDatos($tabla,$datos,$cambios=array(),$relacionadas=array()){
    global $_CONFIG;

    $res=true;
    
    $campos=camposTabla($tabla);


    $consulta="INSERT INTO $tabla VALUES(NULL";
    
    foreach($campos as $campo){
        
        if(array_key_exists ($campo,$cambios)){
            $dato=$cambios[$campo];
        } 
        else{
            $dato=$datos[$campo];
        }

        if($dato===NULL || $dato=='NULL'){
            $consulta.=",NULL";
        } 
        else{
            $consulta.=",'".$dato."'";
        }
    }

    $consulta.=");";
    
    
    $res=consultaBD($consulta);
    if($res){
        $res=mysql_insert_id();
    }
    
    if(!empty($relacionadas)){
        
        $codigoNuevoRegistro=$res;

        foreach($relacionadas as $key => $value) {
            $items=consultaBD('SELECT * FROM '.$key.' WHERE '.$value.'='.$datos['codigo']);
            
            while($item=mysql_fetch_assoc($items)){
                $res=$res && copiaDatos($key,$item,array($value=>$codigoNuevoRegistro));
            }
        }

        if($res){
            $res=$codigoNuevoRegistro;//Para devolver el id del registro principal duplicado (no el de sus tablas hijas)
        }

    }


    return $res;
}

function compruebaFormularioRenovado($codigo){
    $res=true;
    if($_POST['aprobado']=='SI'){
        $contrato=consultaBD('SELECT codigoContrato FROM formularioPRL WHERE codigo='.$codigo,true,true);
        $contrato=consultaBD('SELECT contratos.codigo, contratos.codigoInterno,contratos.fechaInicio, codigoCliente FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo  WHERE contratos.codigo='.$contrato['codigoContrato'],true,true);
        $anioAnterior=explode('-', $contrato['fechaInicio']);
        $anioAnterior=$anioAnterior[0]-1;
        $contratoAnterior=consultaBD('SELECT contratos.codigo FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo WHERE contratos.codigoInterno='.$contrato['codigoInterno'].' AND codigoCliente='.$contrato['codigoCliente'].' AND YEAR(contratos.fechaInicio)="'.$anioAnterior.'";',true,true);
        $formularioPRL=consultaBD('SELECT codigo FROM formularioPRL WHERE codigoContrato='.$contratoAnterior['codigo'],true,true);
        if($formularioPRL){
            $res=consultaBD('UPDATE formularioPRL SET visible="NO" WHERE codigo='.$formularioPRL['codigo'],true);
        }
    }
    return $res;
}
//Fin parte de inicio del cliente

//Parte común entre contratos e inicio

function estadisticasInicialesContratos(){
    $res=array(
        'enVigor'       =>  0,
        'aRenovar'      =>  0,
        'renovaciones'  =>  0,
        'fueraPlazo'    =>  0,
        'anulados'      =>  0
    );

    conexionBD();

    $datos=consultaBD("SELECT COUNT(DISTINCT contratos.codigo) AS total
                       FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
                       INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
                       LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
                       LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
                       WHERE contratos.eliminado='NO' AND enVigor='SI' AND facturas.facturaCobrada='SI';",false,true);

    $res['enVigor']=$datos['total'];





    $datos=consultaBD("SELECT COUNT(DISTINCT contratos.codigo) AS total
                       FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
                       INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
                       LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
                       LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
                       LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
                       LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
                       WHERE enVigor='SI' AND contratos.fechaFin<=DATE_ADD(CURDATE(),INTERVAL 90 DAY) AND contratos.fechaFin>=CURDATE() 
                       AND contratos.renovado='NO' AND checkNoRenovar='NO' AND
                       (facturaCobrada='SI' OR (ofertas.total-ofertas.importe_iva)=0);",false,true);

    $res['aRenovar']=$datos['total'];


    $datos=consultaBD("SELECT COUNT(DISTINCT contratos.codigo) AS total
                       FROM contratos WHERE YEAR(fechaFin)='".date('Y')."' AND renovado='SI' AND eliminado='NO';",false,true);



    $datos=consultaBD("SELECT COUNT(DISTINCT contratos.codigo) AS total
                       FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
                       INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
                       LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
                       LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
                       LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
                       LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
                       WHERE (enVigor='SI' AND contratos.fechaFin<CURDATE() AND contratos.renovado='NO' AND facturaCobrada='SI' AND checkNoRenovar='NO') OR 
                       (contratos.codigo IN(920,1028) AND enVigor='SI')",false,true);

    $res['fueraPlazo']=$datos['total'];





    $datos=consultaBD("SELECT COUNT(DISTINCT contratos.codigo) AS total
                       FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
                       INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
                       WHERE contratos.eliminado='SI';",false,true);

    $res['anulados']=$datos['total'];

    cierraBD();


    return $res;
}

//Fin parte común entre contratos e inicio

//Parte común entre facturas e inicio

function estadisticasFacturas($anio=''){
    $res=array();

    conexionBD();

    if($anio==''){

        $res=array(
            'numeroFacturas'        =>  0,
            'facturacionTotal'      =>  0,
            'series'                =>  array(),
            'totalCobrado'          =>  0,
            'totalPendienteCobro'   =>  0,
            'numeroProformas'       =>  0
        );

        $whereEjercicio=obtieneWhereEjercicioEstadisticas('facturas.fecha');
        $whereEjercicioReco=obtieneWhereEjercicioEstadisticas('facturas_reconocimientos_medicos.fecha');

        //Número de facturas
        $consulta=consultaBD('SELECT COUNT(codigo) AS cantidad 
                              FROM facturas 
                              WHERE (tipoFactura="NORMAL" OR tipoFactura="LIBRE") 
                              '.$whereEjercicio.' AND eliminado="NO"',false,true);

        $res['numeroFacturas']=$consulta['cantidad'];

        //Facturación total
        $consulta=consultaBD('SELECT SUM(total) AS total 
                              FROM facturas 
                              WHERE codigoSerieFactura IS NOT NULL 
                              AND (tipoFactura="NORMAL" OR tipoFactura="LIBRE") 
                              '.$whereEjercicio.' AND facturas.eliminado="NO"',false,true);

        $res['facturacionTotal']=$consulta['total'];

        $consulta=consultaBD('SELECT SUM(total) AS total 
                              FROM facturas_reconocimientos_medicos 
                              WHERE codigoSerieFactura IS NOT NULL AND eliminado="NO" '.$whereEjercicioReco.'',false,true);
                              
        $res['facturacionTotal']+=$consulta['total'];

        //Por series
        $consulta=consultaBD('SELECT SUM(total) AS total, serie 
                              FROM facturas INNER JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
                              WHERE (tipoFactura="NORMAL" OR tipoFactura="LIBRE") '.$whereEjercicio.' AND facturas.eliminado="NO"
                              GROUP BY codigoSerieFactura');

        while($datos=mysql_fetch_assoc($consulta)){
            $res['series'][$datos['serie']]=$datos['total'];
        }


        $consulta=consultaBD('SELECT SUM(total) AS total, serie 
                              FROM facturas_reconocimientos_medicos INNER JOIN series_facturas ON facturas_reconocimientos_medicos.codigoSerieFactura=series_facturas.codigo
                              WHERE facturas_reconocimientos_medicos.eliminado="NO" '.$whereEjercicioReco.'
                              GROUP BY codigoSerieFactura');

        while($datos=mysql_fetch_assoc($consulta)){
            if(isset($res['series'][$datos['serie']])){
                $res['series'][$datos['serie']]+=$datos['total'];
            }
            else{
                $res['series'][$datos['serie']]=$datos['total'];       
            }
        }

        //Cobrado
        $consulta=consultaBD('SELECT SUM(total) AS total 
                              FROM facturas 
                              WHERE (tipoFactura="NORMAL" OR tipoFactura="LIBRE") '.$whereEjercicio.' 
                              AND eliminado="NO" AND facturaCobrada="SI" AND codigoSerieFactura IS NOT NULL;',false,true);

        $res['totalCobrado']=$consulta['total'];

        $consulta=consultaBD('SELECT SUM(total) AS total 
                              FROM facturas_reconocimientos_medicos 
                              WHERE codigoSerieFactura IS NOT NULL '.$whereEjercicioReco.' 
                              AND eliminado="NO" AND facturaCobrada="SI";',false,true);

        $res['totalCobrado']+=$consulta['total'];


        //Pendiente cobro
        $consulta=consultaBD('SELECT SUM(total) AS total 
                              FROM facturas 
                              WHERE (tipoFactura="NORMAL" OR tipoFactura="LIBRE") '.$whereEjercicio.' 
                              AND eliminado="NO" AND facturaCobrada="NO" AND codigoSerieFactura IS NOT NULL;',false,true);

        $res['totalPendienteCobro']=$consulta['total'];

        $consulta=consultaBD('SELECT SUM(total) AS total 
                              FROM facturas_reconocimientos_medicos 
                              WHERE codigoSerieFactura IS NOT NULL '.$whereEjercicioReco.' 
                              AND eliminado="NO" AND facturaCobrada="NO";',false,true);

        $res['totalPendienteCobro']+=$consulta['total'];

        //De abonos
        $consulta=consultaBD('SELECT IFNULL(SUM(importe),0) AS total FROM vencimientos_facturas INNER JOIN facturas ON vencimientos_facturas.codigoFactura=facturas.codigo WHERE facturas.eliminado="NO" AND codigoEstadoRecibo="6" AND facturas.tipoFactura!="ABONO" '.$whereEjercicio,false,true);
        $res['totalDeBaja']=$consulta['total'];

        $proformas=consultaBD("SELECT COUNT(facturas.codigo) AS total 
                               FROM facturas INNER JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
                               INNER JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
                               WHERE tipoFactura='PROFORMA' $whereEjercicio AND facturas.eliminado='NO'
                               AND contratos.eliminado='NO';",false,true);
    }
    else{

        $res=array(
            'numeroFacturas'    =>  0,
            'facturacionTotal'  =>  0,
            'numeroProformas'   =>  0
        );

        $consulta=consultaBD('SELECT COUNT(codigo) AS cantidad FROM facturas WHERE (tipoFactura="NORMAL" OR tipoFactura="LIBRE") AND YEAR(fecha)="'.$anio.'" AND eliminado="NO"',false,true);
        $res['numeroFacturas']=$consulta['cantidad'];


        $consulta=consultaBD('SELECT SUM(total) AS total FROM facturas WHERE (tipoFactura="NORMAL" OR tipoFactura="LIBRE") AND YEAR(fecha)="'.$anio.'" AND eliminado="NO"',false,true);
        $res['facturacionTotal']=$consulta['total'];

        $proformas=consultaBD("SELECT COUNT(facturas.codigo) AS total 
                               FROM facturas INNER JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
                               INNER JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
                               WHERE tipoFactura='PROFORMA' AND YEAR(facturas.fecha)='$anio' AND facturas.eliminado='NO'
                               AND contratos.eliminado='NO';",false,true);

    }

    cierraBD();
    
    $res['numeroProformas']=$proformas['total'];

    return $res;
}


function imprimeEstadisticasPorSeries($estadisticasSeries){

	echo "
	<div class='big_stats cf'>";

	$i=1;
	foreach ($estadisticasSeries as $serie => $total){

		$total=formateaNumeroWeb($total).' €';

		if(($i%2)==0){
			echo "
			</div>
			<div class='big_stats cf'>";
		}

		echo "
		<div class='stat'> 
			<i class='icon-list-ol'></i> 
			<span class='value'>$total</span> 
			<br />
			Serie $serie
		</div>";
	}

	echo "
	</div>";

}

//Fin parte común entre facturas e inicio


//Parte común entre citas y reconocimientos médicos

function creaVentanaCreacionEmpleado($codigoContrato=false){
    
    if($codigoContrato){
        $codigoCliente=consultaBD('SELECT codigoCliente FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo WHERE contratos.codigo='.$codigoContrato,true,true);
        $codigoCliente=$codigoCliente['codigoCliente'];
    }
    else{
        $codigoCliente=false;
    }

    abreVentanaModal('empleados');
    
    abreColumnaCampos();
    campoTexto('nombreCreacion','Nombre');
    campoTexto('apellidosCreacion','Apellidos');
    campoTexto('dniCreacion','DNI',false,'input-small');
    campoTexto('direccionCreacion','Dirección 1');
    campoTexto('direccion2Creacion','Dirección 2');
    campoTexto('cpCreacion','Código postal',false,'input-small');
    campoTexto('ciudadCreacion','Ciudad');
    campoTexto('provinciaCreacion','Provincia');
    campoSelectConsulta('codigoPuestoTrabajoCreacion','Puesto',"SELECT codigo, nombre AS texto FROM puestos_trabajo ORDER BY nombre;",false,'selectpicker show-tick');
    $puestos=consultaBD("SELECT COUNT(codigo) AS total FROM protocolizacion WHERE codigoCliente='$codigoCliente';",true,true);
    if($puestos['total']>0){
        campoOculto('','puestoManual');
    } else {
        campoTexto('puestoManual','Puesto manual');
    }
    campoRadio('trabajadorSensibleCreacion','Trabajador sensible');
    cierraColumnaCampos();

    abreColumnaCampos();
    campoFecha('fechaNacimientoCreacion','Fecha de nacimiento');
    campoTexto('telefonoCreacion','Teléfono',false,'input-small pagination-right');
    campoTexto('emailCreacion','eMail');
    campoTexto('paisCreacion','País');
    campoTexto('movilCreacion','Móvil');
    campoFecha('fechaAltaCreacion','Fecha alta');
    areaTexto('objetivoCreacion','Objetivos'); 
    areaTexto('observacionesCreacion','Observaciones'); 
    cierraColumnaCampos(true);

    cierraVentanaModal();
}


function registraEmpleadoAJAX(){
    $datos=arrayFormulario();
    extract($datos);

    $codigoInterno=generaNumeroReferencia('empleados','codigoInterno',"codigoCliente='$codigoCliente'");

    conexionBD();

    $res=consultaBD("INSERT INTO empleados(codigo,codigoCliente,codigoInterno,dni,nombre,apellidos,telefono,email,fechaNacimiento,direccion,direccion2,cp,ciudad,provincia,codigoPuestoTrabajo,trabajadorSensible,pais,movil,fechaAlta,objetivo,observaciones,aprobado)
                     VALUES(
                            NULL,
                            $codigoCliente,
                            '$codigoInterno',
                            '$dni',
                            '$nombre',
                            '$apellidos',
                            '$telefono',
                            '$email',
                            '$fechaNacimiento',
                            '$direccion',
                            '$direccion2',
                            '$cp',
                            '$ciudad',
                            '$provincia',
                            $codigoPuestoTrabajo,
                            '$trabajadorSensible',
                            '$pais',
                            '$movil',
                            '$fechaAlta',
                            '$objetivo',
                            '$observaciones',
                            'SI'
                     );");

    if($res){
        $res=mysql_insert_id();
        $res2=consultaBD("INSERT INTO usuarios(codigo,nombre,apellidos,dni,email,telefono,usuario,clave,tipo) VALUES (NULL,'$nombre','$apellidos','$dni','$email','$telefono','$usuario','$clave','EMPLEADO')");
        if($res2){
            $res2=mysql_insert_id();
            $res3=consultaBD("INSERT INTO usuarios_empleados VALUES (NULL,".$res2.",".$res.")");
        }
    }
    else{
        $res=mysql_error();
    }

    cierraBD();

    echo $res;
}


function campoSelectConsultaPlusDoble($nombreCampo,$texto,$consulta,$valor=false,$clase='selectpicker selectPlus show-tick',$busqueda="data-live-search='true'",$disabled='',$tipo=0,$conexion=true,$claseBoton='btn-propio',$textoBoton="<i class='icon-plus'></i>"){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    if($disabled){
        $disabled="disabled='disabled'";
    }

    if($tipo==0){
        echo "
        <div class='control-group'>                     
            <label class='control-label' for='$nombreCampo'>$texto:</label>
            <div class='controls nowrap'>";
    }
    elseif($tipo==1){
        echo "<td>";
    }

    echo "<select name='$nombreCampo' class='$clase' id='$nombreCampo' $busqueda $disabled>
            <option value='NULL'></option>";
        
        $consulta=consultaBD($consulta,$conexion);
        $datos=mysql_fetch_assoc($consulta);
        while($datos!=false){
            echo "<option value='".$datos['codigo']."'";

            if($valor!=false && $valor==$datos['codigo']){
                echo " selected='selected'";
            }

            echo ">".$datos['texto']."</option>";
            $datos=mysql_fetch_assoc($consulta);
        }
        
    echo "</select> <button class='btn $claseBoton btn-small botonSelectPlus' type='button' id='boton-$nombreCampo'>$textoBoton</button> <button class='btn btn-default btn-small botonSelectPlus2' type='button' id='boton-$nombreCampo-2'><i class='icon-external-link'></i></button>";

    if($tipo==0){
        echo "
            </div> <!-- /controls -->       
        </div> <!-- /control-group -->";
    }
    elseif($tipo==1){
        echo "</td>";
    }
}

function marcaReconocimientosUsuarioComoEditables(){
    $codigoUsuario=$_SESSION['codigoU'];

    $res=consultaBD("UPDATE reconocimientos_medicos SET editable='SI', codigoUsuario=NULL WHERE editable='NO' AND codigoUsuario='$codigoUsuario';",true);

    if(!$res){
        mensajeError('hubo un problema al desbloquear los reconocimientos en edición. Código de error: '.mysql_errno());
    }
}

//Fin parte común entre citas y reconocimientos médicos

//Parte común entre reconocimientos médicos y protocolización

function campoSelectDesplegable($nombreCampo,$texto,$datos=false,$clase='selectpicker span8 show-tick selectInserccion',$busqueda="data-live-search='true'",$disabled='',$tipo=0,$conexion=true,$multiple=''){
    $textoSQL=explode('_', $nombreCampo);
    $textoSQL=$textoSQL[0];
    $consulta='SELECT codigo, texto FROM desplegables WHERE campo LIKE "'.$textoSQL.'%";';
    campoSelectConsulta($nombreCampo,$texto,$consulta,$datos,$clase,$busqueda,$disabled,$tipo,$conexion,$multiple,'');
}

//Fin parte común entre reconocimientos médicos y protocolización

function esSuperAdmin(){
    return $_SESSION['tipoUsuario']=='ADMIN' && $_SESSION['superadmin']=='SI';
}

//Parte común entre las distintas secciones de Planificación

function camposAgentes($datos,$codigoCentro){
    echo "<h3 class='apartadoFormulario'>Exposición a agentes físicos</h3>";
    
    abreColumnaCampos();
    
    areaTextoFormulario($codigoCentro.'58','Ruido',$datos);
    areaTextoFormulario($codigoCentro.'59','Radiaciones',$datos);
    areaTextoFormulario($codigoCentro.'60','Luz',$datos);
    
    cierraColumnaCampos();
    abreColumnaCampos();

    areaTextoFormulario($codigoCentro.'61','Vibraciones',$datos);
    areaTextoFormulario($codigoCentro.'62','Temperatura, humedad y velocidad del aire',$datos);
    
    cierraColumnaCampos(true);

    
    echo "<h3 class='apartadoFormulario'>Exposición a agentes biológicos</h3>";
    
    abreColumnaCampos();
        areaTextoFormulario($codigoCentro.'63','Descripción',$datos);
    cierraColumnaCampos(true);
    
    echo "<h3 class='apartadoFormulario'>Exposición a agentes químicos</h3>";
    abreColumnaCampos();
        areaTextoFormulario($codigoCentro.'64','Descripción',$datos);
    cierraColumnaCampos(true);

    
}

function obtieneEstadoOfertaListado($datos){
    $res='';

    if($datos['rechazada']=='SI'){
        $res='Rechazada';
    }
    elseif($datos['aceptado']=='SI'){
        $res='Aceptada';
    }
    elseif($datos['rechazada']=='NO' && $datos['aceptado']=='NO'){
        $res='Pendiente';
    }


    return $res;
}

function obtieneEstadoContratoListado($datos,$compruebaImporte=false,$importe=false){
    $res='';

    $fechaSesentaDias=sumaDiasFecha(fechaBD(),60);//Para obtener la fecha para comprar los contratos a renovar.


    if(
        $datos['codigo']==920 || $datos['codigo']==1028 ||
        ($compruebaImporte && $importe=='0,00') //Contratos de VS a importe 0
    ){
        $datos['facturaCobrada']='SI';
    }

    
    if($datos['suspendido']=='SI'){
        $res='Suspendido';
    }
    elseif($datos['enVigor']=='NO' && comparaFechas($datos['fechaInicio'],fechaBD())<0){
        $res='Futurible';
    }
    elseif($datos['enVigor']=='NO'){
        $res='Histórico';
    }
    elseif($datos['enVigor']=='SI' && $datos['renovado']=='NO' && $datos['checkNoRenovar']=='NO' && $datos['facturaCobrada']=='SI' && comparaFechas($datos['fechaFin'],$fechaSesentaDias)>=0){
        $res='En vigor - para renovar';
    }
    elseif($datos['enVigor']=='SI' && $datos['facturaCobrada']=='SI'){
        $res='En vigor';
    }
    if($datos['enVigor']=='SI' && $datos['facturaCobrada']=='NO' && $datos['emitirPlanificacionSinPagar']=='SI'){
        $res='En vigor - en periodo de pago';
    }
    elseif($datos['enVigor']=='SI' && $datos['facturaCobrada']=='NO'){
        $res='Pendiente cobrar';
    }

    return $res;
}

function tablaGestionesClientes($codigoCliente){
    global $_CONFIG;
    conexionBD();
    echo "
        <table class='table table-striped table-bordered'>
            <thead>
                <tr>
                    <th> Gestión </th>
                    <th> Fecha </th>
                    <th> Nº </th>
                    <th> Contrato asociado </th>
                    <th> Servicio / Tarea / Concepto </th>
                    <th> Importe </th>
                    <th> Estado </th>
                    <th> Técnico / Trabajador </th>
                    <th> Facturado </th>
                </tr>
            </thead>
            <tbody>";
                    $body=array();
                    $iconos=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
                    $where='WHERE 1=1';
                    if($_SESSION['tipoUsuario'] == 'COLABORADOR'){
                        $listado=consultaBD("SELECT ofertas.*, clientes.EMPNOMBRE, clientes.EMPMARCA, ofertas.opcion FROM ofertas LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND codigoCliente IN (SELECT codigo FROM clientes WHERE EMPASESORIA=".$_SESSION['codigoU'].") AND mostrar='SI' AND codigoCliente=".$codigoCliente." ORDER BY fecha;");
                    }
                    else if($_SESSION['tipoUsuario'] == 'TECNICO'){
                        $listado=consultaBD("SELECT ofertas.*, clientes.EMPNOMBRE, clientes.EMPMARCA, ofertas.opcion 
                              FROM ofertas LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo
                              LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta
                              LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato 
                              $where AND mostrar='SI'
                              AND (contratos.codigo IS NULL OR contratos.tecnico=".$_SESSION['codigoU']." OR formularioPRL.codigoUsuarioTecnico=".$_SESSION['codigoU']." OR ofertas.codigoUsuario=".$_SESSION['codigoU'].")  AND codigoCliente=".$codigoCliente."
                              GROUP BY ofertas.codigo ORDER BY fecha;");
                    } 
                    else{
                        $listado=consultaBD("SELECT ofertas.*, clientes.EMPNOMBRE, clientes.EMPMARCA, ofertas.opcion FROM ofertas LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND mostrar='SI'  AND codigoCliente=".$codigoCliente." ORDER BY fecha;");
                    }
                    while($item=mysql_fetch_assoc($listado)){
                        $servicio=obtieneNombreServicioContratado($item['opcion']);
                        if($item['eliminado']=='SI'){
                            $estado='Eliminado';
                        } else {
                            $estado=obtieneEstadoOfertaListado($item);
                        }
                        $tr="<tr>";
                        $tr.="<td><a class='noAjax' href='".$_CONFIG['raiz']."ofertas/gestion.php?codigo=".$item['codigo']."' target='_blank'>OFERTA</a></td>";
                        $tr.="<td>".formateaFechaWeb($item['fecha'])."</td>";
                        $tr.="<td class='nowrap'>".$item['codigoInterno']."</td>";
                        $tr.="<td></td>";
                        $tr.="<td>".$servicio."</td>";
                        $tr.="<td class='nowrap'>".formateaNumeroWeb($item['total']-$item['importe_iva'])." €</td>";
                        $tr.="<td>".$estado."</td>";
                        $tr.="<td></td>";
                        $tr.="<td></td>";
                        $tr.='</tr>';
                        array_push($body,array('tr'=>$tr,'fecha'=>$item['fecha']));
                    }
                    $where="WHERE (contratos_en_facturas.codigo IS NULL OR facturas.eliminado='NO')";
                    if($_SESSION['tipoUsuario'] == 'COLABORADOR'){
                        $listado=consultaBD("SELECT (ofertas.total-ofertas.importe_iva) AS importe, clientes.EMPNOMBRE,clientes.EMPMARCA,clientes.codigo AS codigoCliente, contratos.*,MAX(contratos_renovaciones.fecha), ofertas.opcion,
                              IFNULL(facturas.facturaCobrada,'NO') AS facturaCobrada, clientes.EMPID, formularioPRL.codigoUsuarioTecnico, contratoRevisado, contratos.fichero, suspendido, incrementoIPC, incrementoIpcAplicado
                              FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
                              INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
                              LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
                              LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
                              LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO' AND facturas.eliminado='NO'
                              LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
                              ".$where."
                              AND codigoCliente IN (SELECT codigo FROM clientes WHERE EMPASESORIA=".$_SESSION['codigoU'].") AND ofertas.codigoCliente=".$codigoCliente."
                              GROUP BY contratos.codigo 
                              ORDER BY contratos.fechaInicio;");
                    }
                    elseif($_SESSION['tipoUsuario'] == 'TECNICO'){
                        $listado=consultaBD("SELECT (ofertas.total-ofertas.importe_iva) AS importe, clientes.EMPNOMBRE,clientes.EMPMARCA,clientes.codigo AS codigoCliente,contratos.*, MAX(contratos_renovaciones.fecha) AS renovacion, 
                              contratos.tecnico, ofertas.opcion, IFNULL(facturas.facturaCobrada,'NO') AS facturaCobrada, clientes.EMPID, formularioPRL.codigoUsuarioTecnico, contratoRevisado, contratos.fichero, suspendido, incrementoIPC, incrementoIpcAplicado
                              FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
                              INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
                              LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
                              LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
                              LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO' AND facturas.eliminado='NO'
                              LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
                              ".$where."
                              AND (contratos.tecnico=".$_SESSION['codigoU']." || formularioPRL.codigoUsuarioTecnico=".$_SESSION['codigoU'].") AND ofertas.codigoCliente=".$codigoCliente."
                              GROUP BY contratos.codigo 
                              ORDER BY contratos.fechaInicio;");

                    }
                    else {
                        $listado=consultaBD("SELECT (ofertas.total-ofertas.importe_iva) AS importe, clientes.EMPNOMBRE,clientes.EMPMARCA,clientes.codigo AS codigoCliente,contratos.*, MAX(contratos_renovaciones.fecha) AS renovacion, 
                              contratos.tecnico, ofertas.opcion, IFNULL(facturas.facturaCobrada,'NO') AS facturaCobrada, clientes.EMPID, formularioPRL.codigoUsuarioTecnico, contratoRevisado, contratos.fichero,facturas.codigo AS codigoFactura, suspendido, incrementoIPC, incrementoIpcAplicado
                              FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
                              INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
                              LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
                              LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
                              LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO' AND facturas.eliminado='NO'
                              LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
                              ".$where." AND ofertas.codigoCliente=".$codigoCliente."
                              GROUP BY contratos.codigo 
                              ORDER BY contratos.fechaInicio;");
                    }
                cierraBD();
                    while($item=mysql_fetch_assoc($listado)){
                        $cliente=datosRegistro('clientes',$item['codigoCliente']);
                        $servicio=obtieneNombreServicioContratado($item['opcion']);
                       
                        if($item['tecnico']!=NULL){
                            $tecnico=datosRegistro('usuarios',$item['tecnico']);
                        } else {
                            $tecnico=datosRegistro('usuarios',$item['codigoUsuarioTecnico']);
                        }
                      
                        if($item['codigoFactura']!=NULL){
                            $facturado='SI';
                        } else {
                            $facturado='NO';
                        }
                      
                        if($item['eliminado']=='SI'){
                            $estado='Eliminado';
                        } else {
                            $estado=obtieneEstadoContratoListado($item);
                        }
                        $tr="<tr>";
                        $tr.="<td><a class='noAjax' href='".$_CONFIG['raiz']."contratos/gestion.php?codigo=".$item['codigo']."' target='_blank'>CONTRATO</a></td>";
                        $tr.="<td>De ".formateaFechaWeb($item['fechaInicio'])." a ".formateaFechaWeb($item['fechaFin'])."</td>";
                        $tr.="<td class='nowrap'>".formateaReferenciaContrato($cliente,$item)."</td>";
                        $tr.="<td></td>";
                        $tr.="<td>".$servicio."</td>";
                        $tr.="<td class='nowrap'>".compruebaImporteContrato($item['importe'],$item)." €</td>";
                        $tr.="<td>".$estado."</td>";
                        $tr.="<td>".$tecnico['nombre']." ".$tecnico['apellidos']."</td>";
                        $tr.="<td class='centro'>".$iconos[$facturado]."</td>";
                        $tr.='</tr>';
                        array_push($body,array('tr'=>$tr,'fecha'=>$item['fechaInicio']));
                    }
                    if($_SESSION['tipoUsuario']!='ADMIN'){
                        $where=' WHERE codigoUsuarioTecnico='.$_SESSION['codigoU'];
                    }
                    $listado=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, visitada, planPrev, pap, info, f.horasPrevistas, memoria, fechaVisita, fechaPlanPrev, fechaPap, fechaInfo,  
                          fechaMemoria, vs, mesVS, anioVS, codigoUsuarioTecnico, CONCAT(EMPNOMBRE,' / ',EMPMARCA) AS cliente,f.codigoContrato, ofertas.codigoCliente, formulario, opcion, f.aprobado, f.eliminado,
                          suspendido, f.fechaAprobada, f.codigoInterno AS codigoPlanificacion, contratos.fechaInicio, contratos.codigoInterno, contratos.codigo AS codigoContrato
                          FROM formularioPRL f 
                          LEFT JOIN contratos ON f.codigoContrato=contratos.codigo 
                          LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo 
                          LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
                          LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
                          LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo
                          ".$where." AND f.visible='SI' AND ofertas.codigoCliente=".$codigoCliente.";",true);
                    $tipos=array(1=>'SPA 4 Especialidades',2=>'SPA Vigilancia de la salud',3=>'SPA Especialides técnicas',4=>'Otras actuaciones');
                    while($item=mysql_fetch_assoc($listado)){
                        $cliente=datosRegistro('clientes',$item['codigoCliente']);
                        $fecha='Sin fecha de aprobado';
                        if($item['eliminado']=='SI'){
                            $estado='Eliminada';
                        } else if($item['aprobado']=='SI'){
                            $estado='Aprobada';
                            if($item['fechaAprobada']!='0000-00-00'){
                                $fecha=formateaFechaWeb($item['fechaAprobada']);
                            }
                        } else {
                            $estado='Pendiente';
                        }
                        $servicio=$tipos[$item['opcion']];
    
                        $tecnico=datosRegistro('usuarios',$item['codigoUsuarioTecnico']);
                        $tr="<tr>";
                        $tr.="<td><a class='noAjax' href='".$_CONFIG['raiz']."planificacion/gestion.php?codigo=".$item['codigo']."' target='_blank'>PLANIFICACIÓN</a></td>";
                        $tr.="<td>".$fecha."</td>";
                        $tr.="<td>".$item['codigoPlanificacion']."</td>";
                        $tr.="<td><a class='noAjax' href='".$_CONFIG['raiz']."contratos/gestion.php?codigo=".$item['codigoContrato']."' target='_blank'>".formateaReferenciaContrato($cliente,$item)."</a></td>";
                        $tr.="<td>".$servicio."</td>";
                        $tr.="<td></td>";
                        $tr.="<td>".$estado."</td>";
                        $tr.="<td>".$tecnico['nombre']." ".$tecnico['apellidos']."</td>";
                        $tr.="<td class='centro'></td>";
                        $tr.='</tr>';
                        array_push($body,array('tr'=>$tr,'fecha'=>$item['fechaAprobada']));
                    }

                    $where=defineWhereEmpleadoTarea();
                    $where.=" AND tareas.codigoCliente=".$codigoCliente;
                    $listado=consultaBD("SELECT tareas.codigo,tareas.tarea, tareas.fechaInicio, tareas.estado, tareas.prioridad, tareas.codigoUsuario, contratos.codigo AS codigoContrato, contratos.codigoInterno, contratos.fechaInicio, tareas.estado, tareas.codigoCliente 
                        FROM tareas
                        LEFT JOIN contratos ON tareas.codigoContrato=contratos.codigo $where ORDER BY tareas.fechaInicio DESC;",true);
                    $estados=array('PENDIENTE'=>"Pendiente",'REALIZADA'=>"Realizada");
                    while($item=mysql_fetch_assoc($listado)){
                        $cliente=datosRegistro('clientes',$item['codigoCliente']);
                        $fecha=formateaFechaWeb($item['fechaInicio']);
                        if($item['codigoContrato']=='NULL'){
                            $contrato="<td></td>";
                        } else {
                            $contrato="<td><a class='noAjax' href='".$_CONFIG['raiz']."contratos/gestion.php?codigo=".$item['codigoContrato']."' target='_blank'>".formateaReferenciaContrato($cliente,$item)."</a></td>";
                        }
                        $estado=$estados[$item['estado']];
                        $servicio=$item['tarea'];
    
                        $tecnico=datosRegistro('usuarios',$item['codigoUsuario']);
                        $tr="<tr>";
                        $tr.="<td><a class='noAjax' href='".$_CONFIG['raiz']."tareas/gestion.php?codigo=".$item['codigo']."' target='_blank'>AGENDA</a></td>";
                        $tr.="<td>".$fecha."</td>";
                        $tr.="<td></td>";
                        $tr.=$contrato;
                        $tr.="<td>".$servicio."</td>";
                        $tr.="<td></td>";
                        $tr.="<td>".$estado."</td>";
                        $tr.="<td>".$tecnico['nombre']." ".$tecnico['apellidos']."</td>";
                        $tr.="<td class='centro'></td>";
                        $tr.='</tr>';
                        array_push($body,array('tr'=>$tr,'fecha'=>$item['fechaInicio']));
                    }

                    $having="HAVING empleados.codigoCliente=".$codigoCliente;
                    $listado=consultaBD("SELECT reconocimientos_medicos.codigo, reconocimientos_medicos.fecha, CONCAT(clientes.EMPNOMBRE,' (',clientes.EMPMARCA,')') AS EMPNOMBRE, CONCAT(empleados.apellidos,', ',empleados.nombre) AS empleado, empleados.dni, apto, checkAudiometria, checkOtrasPruebasFuncionales, checkEspirometria, checkElectrocardiograma, advertencia, cerrado, reconocimientos_medicos.codigoUsuario, reconocimientos_medicos.editable, reconocimientos_medicos.eliminado, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS usuario, mesSiguienteReconocimiento, anioSiguienteReconocimiento, clientes.EMPID, codigoProfesionalExterno, reconocimientos_medicos.numero, codigoFacturaReconocimientoMedico, aptoVisible, reconocimientoVisible,bloqueado, reconocimientos_medicos.codigoContrato, contratos.codigoInterno, contratos.fechaInicio, empleados.codigo AS codigoEmpleado, empleados.codigoCliente
                        FROM reconocimientos_medicos LEFT JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
                        LEFT JOIN clientes ON empleados.codigoCliente=clientes.codigo 
                        LEFT JOIN usuarios ON reconocimientos_medicos.codigoUsuario=usuarios.codigo
                        LEFT JOIN reconocimientos_medicos_en_facturas ON reconocimientos_medicos.codigo=reconocimientos_medicos_en_facturas.codigoReconocimientoMedico
                        LEFT JOIN facturas_reconocimientos_medicos ON reconocimientos_medicos_en_facturas.codigoFacturaReconocimientoMedico=facturas_reconocimientos_medicos.codigo AND reconocimientos_medicos.codigoContrato=facturas_reconocimientos_medicos.codigoContrato
                        LEFT JOIN contratos ON reconocimientos_medicos.codigoContrato=contratos.codigo
                        GROUP BY reconocimientos_medicos.codigo $having",true);
                    while($item=mysql_fetch_assoc($listado)){
                        $cliente=datosRegistro('clientes',$item['codigoCliente']);
                        $fecha=formateaFechaWeb($item['fecha']);
                        if($item['codigoContrato']=='NULL'){
                            $contrato="<td></td>";
                        } else {
                            $contrato="<td><a class='noAjax' href='".$_CONFIG['raiz']."contratos/gestion.php?codigo=".$item['codigoContrato']."' target='_blank'>".formateaReferenciaContrato($cliente,$item)."</a></td>";
                        }
                        $estado='No cerrado';
                        if($item['cerrado']=='SI'){
                            if($item['aptoVisible']=='SI'){
                                $estado='Cerrado (APTO)';
                            } else {
                                $estado='Cerrado (NO APTO)';
                            }
                        }
    
                        $tecnico="<td><a class='noAjax' href='".$_CONFIG['raiz']."empleados/gestion.php?codigo=".$item['codigoEmpleado']."' target='_blank'>".$item['empleado']."</a></td>";
                        $facturado='NO';
                        if($item['codigoFacturaReconocimientoMedico']!=NULL){
                            $facturado='SI';
                        }
                        $tr="<tr>";
                        $tr.="<td><a class='noAjax' href='".$_CONFIG['raiz']."reconocimientos-medicos/gestion.php?codigo=".$item['codigo']."' target='_blank'>RECONOCIMIENTO</a></td>";
                        $tr.="<td>".$fecha."</td>";
                        $tr.="<td></td>";
                        $tr.=$contrato;
                        $tr.="<td></td>";
                        $tr.="<td></td>";
                        $tr.="<td>".$estado."</td>";
                        $tr.=$tecnico;
                        $tr.="<td class='centro'>".$iconos[$facturado]."</td>";
                        $tr.='</tr>';
                        array_push($body,array('tr'=>$tr,'fecha'=>$item['fecha']));
                    }

                    $having1="HAVING facturas.codigoCliente=".$codigoCliente;
                    $having2="HAVING facturas_reconocimientos_medicos.codigoClienteParaConceptoManual=".$codigoCliente;
                    $listado=consultaBD("SELECT facturas.codigo, facturas.fecha, serie, facturas.numero, CONCAT(clientes.EMPNOMBRE,' (',clientes.EMPMARCA,')') AS cliente, clientes.EMPCIF AS cif, 
                        facturas.baseImponible, facturas.total, facturas.activo, facturas.facturaCobrada,
                        cobros_facturas.codigo AS codigoCobro, clientes.codigo AS codigoCliente, abonos.codigo AS codigoAbono, clientes.EMPCP, clientes.EMPID, 
                        contratos.fechaInicio, contratos.codigoInterno, contratos.enVigor, contratos.suspendido, facturas.tipoFactura, facturas.eliminado, facturas.codigoSerieFactura, contratos.codigo AS codigoContrato, facturas.codigoCliente
                        FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
                        LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
                        LEFT JOIN cobros_facturas ON facturas.codigo=cobros_facturas.codigoFactura AND cobros_facturas.importe>0
                        LEFT JOIN facturas AS abonos ON facturas.codigo=abonos.codigoFacturaAsociada AND abonos.tipoFactura='ABONO'
                        LEFT JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
                        LEFT JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
            
                        GROUP BY facturas.codigo

                        $having1

                        UNION ALL

                        SELECT facturas_reconocimientos_medicos.codigo, facturas_reconocimientos_medicos.fecha, serie, facturas_reconocimientos_medicos.numero, 
                        IFNULL(CONCAT(clientes.EMPNOMBRE,' (',clientes.EMPMARCA,')'),CONCAT(clientes2.EMPNOMBRE,' (',clientes2.EMPMARCA,')')) AS cliente, 
                        IFNULL(clientes.EMPCIF,clientes2.EMPCIF) AS cif,  facturas_reconocimientos_medicos.total AS baseImponible, facturas_reconocimientos_medicos.total, 'SI' AS activo, 
                        facturas_reconocimientos_medicos.facturaCobrada, NULL AS codigoCobro, clientes.codigo AS codigoCliente, NULL AS codigoAbono, 
                        clientes.EMPCP, clientes.EMPID, contratos.fechaInicio, contratos.codigoInterno, contratos.enVigor, contratos.suspendido, 'RECONOCIMIENTOS' AS tipoFactura, 
                        facturas_reconocimientos_medicos.eliminado, codigoSerieFactura, contratos.codigo AS codigoContrato, facturas_reconocimientos_medicos.codigoClienteParaConceptoManual
                        FROM facturas_reconocimientos_medicos LEFT JOIN contratos ON facturas_reconocimientos_medicos.codigoContrato=contratos.codigo
                        LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
                        LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo
                        LEFT JOIN clientes clientes2 ON facturas_reconocimientos_medicos.codigoClienteParaConceptoManual=clientes2.codigo
                        LEFT JOIN series_facturas ON facturas_reconocimientos_medicos.codigoSerieFactura=series_facturas.codigo
            
                        GROUP BY facturas_reconocimientos_medicos.codigo

                        $having2;",true);
                    while($item=mysql_fetch_assoc($listado)){
                        $cliente=datosRegistro('clientes',$item['codigoCliente']);
                        $fecha=formateaFechaWeb($item['fecha']);
                        if($item['codigoContrato']=='NULL'){
                            $contrato="<td></td>";
                        } else {
                            $contrato="<td><a class='noAjax' href='".$_CONFIG['raiz']."contratos/gestion.php?codigo=".$item['codigoContrato']."' target='_blank'>".formateaReferenciaContrato($cliente,$item)."</a></td>";
                        }
                        if($item['eliminado']=='SI'){
                            $estado='Eliminado';
                        } else {
                            $estado='Proforma';
                            if($item['tipoFactura']!='PROFORMA'){
                                if($item['facturaCobrada']=='SI'){
                                    $estado='Cobrada';
                                } else {
                                    $estado='No cobrada';
                                }
                            }
                        }

                        $tr="<tr>";
                        $tr.="<td><a class='noAjax' href='".$_CONFIG['raiz']."facturas/gestion.php?codigo=".$item['codigo']."' target='_blank'>FACTURA</a></td>";
                        $tr.="<td>".$fecha."</td>";
                        $tr.="<td>".$item['serie']."-".$item['numero']."</td>";
                        $tr.=$contrato;
                        $tr.="<td></td>";
                        $tr.="<td>".formateaNumeroWeb($item['total'])." €</td>";
                        $tr.="<td>".$estado."</td>";
                        $tr.="<td></td>";
                        $tr.="<td class='centro'></td>";
                        $tr.='</tr>';
                        array_push($body,array('tr'=>$tr,'fecha'=>$item['fecha']));
                    }

                    $where=" WHERE ofertas.codigoCliente=".$codigoCliente;
                    $listado=consultaBD("SELECT facturacion_gastos.*, contratos.codigo AS codigoContrato, contratos.codigoInterno, contratos.fechaInicio, ofertas.codigoCliente FROM facturacion_gastos INNER JOIN contratos ON facturacion_gastos.codigoContrato=contratos.codigo INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo $where",true);
                    while($item=mysql_fetch_assoc($listado)){
                        $cliente=datosRegistro('clientes',$item['codigoCliente']);
                        $fecha=formateaFechaWeb($item['fecha']);
                        if($item['codigoContrato']=='NULL'){
                            $contrato="<td></td>";
                        } else {
                            $contrato="<td><a class='noAjax' href='".$_CONFIG['raiz']."contratos/gestion.php?codigo=".$item['codigoContrato']."' target='_blank'>".formateaReferenciaContrato($cliente,$item)."</a></td>";
                        }
                        $servicio=$item['concepto'];
    
                        $tecnico=datosRegistro('usuarios',$item['codigoUsuario']);
                        $tr="<tr>";
                        $tr.="<td><a class='noAjax' href='".$_CONFIG['raiz']."gastos/gestion.php?codigo=".$item['codigo']."' target='_blank'>GASTO</a></td>";
                        $tr.="<td>".$fecha."</td>";
                        $tr.="<td></td>";
                        $tr.=$contrato;
                        $tr.="<td>".$servicio."</td>";
                        $tr.="<td>".formateaNumeroWeb($item['total'])." €</td>";
                        $tr.="<td></td>";
                        $tr.="<td>".$tecnico['nombre']." ".$tecnico['apellidos']."</td>";
                        $tr.="<td class='centro'></td>";
                        $tr.='</tr>';
                        array_push($body,array('tr'=>$tr,'fecha'=>$item['fecha']));
                    }
                    $auxVacio=array();
                    $aux=array();
                    for($i=0;$i<count($body);$i++){
                        if($body[$i]['fecha']!='0000-00-00'){
                            $body[$i]['fecha']=strtotime($body[$i]['fecha']);
                            array_push($aux,$body[$i]);
                        } else {
                            array_push($auxVacio,$body[$i]);
                        }
                    }
                    uasort($aux, 'sort_by_date');
                    foreach ($auxVacio as $key => $value) {
                        echo $value['tr'];
                    }
                    foreach ($aux as $key => $value) {
                        echo $value['tr'];
                    }
    echo "
        </tbody>
    </table>";

}

function sort_by_date ($a, $b) {
    return $a['fecha'] > $b['fecha'];
}

function creaTablaVisitasPlanificacion($datos){
        echo "
        <div class='control-group'>                     
            <div class='controls' style='margin-left:10px;'>
                <table class='tabla-simple' id='tablaVisitas'>
                    <thead>
                        <tr>
                            <th> Fecha prevista </th>
                            <th> Fecha real </th>
                            <th> Hora inicio </th>
                            <th> Hora Fin </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>";
            
                $i=0;

                conexionBD();

                if($datos){
                    $consulta=consultaBD("SELECT planificacion_visitas.* FROM planificacion_visitas WHERE codigoFormulario=".$datos['codigo']);
                    while($v=mysql_fetch_assoc($consulta)){
                        $j=$i+1;
                        echo '<tr>';
                        campoOculto($v['codigo'],'codigoVisitaExiste'.$i);
                        campoFechaBlancoTabla('fechaPrevista'.$i,$v['fechaPrevista']);
                        campoFechaBlancoTabla('fechaReal'.$i,$v['fechaReal']);
                        campoHoraBlanco('horaInicio'.$i,'',$v['horaInicio'],1);
                        campoHoraBlanco('horaFin'.$i,'',$v['horaFin'],1);
                        echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td></tr>";
                        $i++;
                    }
                }
                
                if($i==0){
                    $j=$i+1;
                    echo '<tr>';
                    campoFechaBlancoTabla('fechaPrevista'.$i);
                    campoFechaBlancoTabla('fechaReal'.$i);
                    campoHoraBlanco('horaInicio'.$i,'',false,1);
                    campoHoraBlanco('horaFin'.$i,'',false,1);
                    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td></tr>";
                    $i++;
                }

                cierraBD();
          
        echo "      </tbody>
                </table>
                <center>
                <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaVisitas\");'><i class='icon-plus'></i></button> 
                <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaVisitas\");'><i class='icon-trash'></i></button>
                </center>
            </div>
        </div>";

        return $i;
}

function insertaVisitas($codigo){
    $res=true;
    conexionBD();
    $i=0;
    $notIn='(0';
    while(isset($_POST['fechaPrevista'.$i])){
        if($_POST['fechaPrevista'.$i]!='' || $_POST['fechaReal'.$i]!=''){
            if($_POST['fechaReal'.$i]!=''){
                $fecha=formateaFechaBD($_POST['fechaReal'.$i]);
                $estado='REALIZADA';
            } elseif($_POST['fechaPrevista'.$i]!=''){
                $fecha=formateaFechaBD($_POST['fechaPrevista'.$i]);
                $estado='PENDIENTE';
            }
            $todoDia='false';
            if($_POST['horaInicio'.$i]=='' && $_POST['horaFin'.$i]==''){
                $todoDia='true';
            }
            $codigoCliente=consultaBD('SELECT codigoCliente FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo WHERE contratos.codigo='.$_POST['codigoContrato'],false,true);
            if($codigoCliente){
                $codigoCliente=$codigoCliente['codigoCliente'];
            } else {
                $codigoCliente='NULL';
            }
            if(isset($_POST['codigoVisitaExiste'.$i]) && $_POST['codigoVisitaExiste'.$i]!=''){
                $codigoTarea=consultaBD('SELECT * FROM planificacion_visitas_tareas WHERE codigoVisita='.$_POST['codigoVisitaExiste'.$i],false,true);
                $res=consultaBD('UPDATE planificacion_visitas SET fechaPrevista="'.formateaFechaBD($_POST['fechaPrevista'.$i]).'",fechaReal="'.formateaFechaBD($_POST['fechaReal'.$i]).'",horaInicio="'.$_POST['horaInicio'.$i].'",horaFin="'.$_POST['horaFin'.$i].'" WHERE codigo='.$_POST['codigoVisitaExiste'.$i]);
                $res=consultaBD('UPDATE tareas SET fechaInicio="'.$fecha.'",fechaFin="'.$fecha.'",todoDia="'.$todoDia.'",codigoUsuario='.$_POST['codigoUsuarioTecnico'].',codigoCliente='.$codigoCliente.',codigoContrato='.$_POST['codigoContrato'].',horaInicio="'.$_POST['horaInicio'.$i].'",horaFin="'.$_POST['horaFin'.$i].'",estado="'.$estado.'" WHERE codigo='.$codigoTarea['codigoTarea']);
                $notIn.=','.$_POST['codigoVisitaExiste'.$i];
            } else {
                $res=consultaBD('INSERT INTO planificacion_visitas VALUES (NULL,'.$codigo.',"'.formateaFechaBD($_POST['fechaPrevista'.$i]).'","'.formateaFechaBD($_POST['fechaReal'.$i]).'","'.$_POST['horaInicio'.$i].'","'.$_POST['horaFin'.$i].'")');
                $codigoVisita=mysql_insert_id();
                $res=consultaBD('INSERT INTO tareas(codigo,tarea,fechaInicio,fechaFin,todoDia,estado,prioridad,codigoUsuario,codigoCliente,codigoContrato,horaInicio,horaFin) VALUES (NULL,"Visita","'.$fecha.'","'.$fecha.'","'.$todoDia.'","'.$estado.'","NORMAL",'.$_POST['codigoUsuarioTecnico'].','.$codigoCliente.','.$_POST['codigoContrato'].',"'.$_POST['horaInicio'.$i].'","'.$_POST['horaFin'.$i].'")');
                $codigoTarea=mysql_insert_id();
                $res=consultaBD("INSERT INTO planificacion_visitas_tareas VALUES (NULL,".$codigoVisita.",".$codigoTarea.")");
                $notIn.=','.$codigoVisita;
            }
        }
        $i++;
    }
    $notIn.=')';
    $listado=consultaBD('SELECT codigo FROM planificacion_visitas WHERE codigo NOT IN '.$notIn.' AND codigoFormulario='.$codigo);
    while($item=mysql_fetch_assoc($listado)){
        $visita=consultaBD('SELECT * FROM planificacion_visitas_tareas WHERE codigoVisita='.$item['codigo'],false,true);
        $res=consultaBD('DELETE FROM planificacion_visitas WHERE codigo='.$visita['codigoVisita']);
        $res=consultaBD('DELETE FROM tareas WHERE codigo='.$visita['codigoTarea']);
    }
    cierraBD();
    return $res;
}

//Fin parte común entre las distintas secciones de Planificación



//Nueva función de envío de email, que sustituye a la nativa de PHP mail()

function enviaEmail($destinatario,$asunto,$mensaje,$adjunto=false,$bcc=''){
    global $_CONFIG;

    /**
     * This example shows settings to use when sending via Google's Gmail servers.
     * This uses traditional id & password authentication - look at the gmail_xoauth.phps
     * example to see how to use XOAUTH2.
     * The IMAP section shows how to save this message to the 'Sent Mail' folder using IMAP commands.
     */
    //Import PHPMailer classes into the global namespace
    require_once('phpmailer-personalizado/PHPMailer.php');
    require_once('phpmailer-personalizado/SMTP.php');
    require_once('phpmailer-personalizado/Exception.php');

    //$asunto="=?ISO-8859-1?B?".base64_encode($asunto)."=?=";//Para tildes;

    //Create a new PHPMailer instance
    $mail = new PHPMailer\PHPMailer\PHPMailer;
    //Tell PHPMailer to use SMTP
    $mail->isSMTP();
    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    $mail->SMTPDebug = 0;


    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );

    //Set the hostname of the mail server
    $mail->Host = 'smtp.strato.com';
    //$mail->Host = 'smtp.gmail.com';
    // use
    // $mail->Host = gethostbyname('smtp.gmail.com');
    // if your network does not support SMTP over IPv6
    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    $mail->Port = 465;
    //Set the encryption system to use - ssl (deprecated) or tls
    $mail->SMTPSecure = 'ssl';
    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;
    //Username to use for SMTP authentication - use full email address for gmail
    $mail->Username = "noreply@anescoprl.es";
    $mail->Password = "Eddc45vff341";
    /*$mail->Username = "pruebasqma@gmail.com";
    $mail->Password = "Writemaster7";*/

    //Set who the message is to be sent from
    $mail->setFrom('noreply@anescoprl.es',$asunto);
    //Set an alternative reply-to address
    //$mail->addReplyTo($remitente,'Avisos ANESCO');
    //Set who the message is to be sent to
    
     if($_CONFIG['usuarioBD']=='root'){
        $mail->addAddress('webmaster@qmaconsultores.com');
    }
    elseif(substr_count($destinatario,',')>0){

        $destintarios=explode(',',$destinatario);
        foreach ($destintarios as $email){
            $mail->addAddress(trim($email));
        }

    }
    else{
        $mail->addAddress($destinatario);
    }


    if(substr_count($bcc,',')>0){
        $bcc=explode(',',$bcc);
        foreach ($bcc as $destinatarioBcc){
            $mail->addBCC(trim($destinatarioBcc));
        }
    }
    elseif($bcc!=''){
        $mail->addBCC($bcc);
    }
    //$mail->addBCC('tecnico5@anescoprl.es');

    // Activa la condificacción utf-8
    $mail->CharSet = 'UTF-8';
    //Set the subject line
    $mail->Subject = $asunto;
    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    $mail->msgHTML($mensaje);
    //Replace the plain text body with one created manually
    //$mail->AltBody = 'This is a plain-text message body';
    //Attach an image file
    
    if($adjunto!=false){
        $mail->addAttachment($adjunto);
    }

    //send the message, check for errors
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
        $res=false;
    } else {
        $res=true;
    }
    
    return $res;
}

function conexionBDFormacion(){
    global $_CONFIG;

    $conexion=mysql_connect($_CONFIG['servidorBD'],$_CONFIG['usuarioBDFormacion'],$_CONFIG['claveBDFormacion']);//MODIFICACIÓN 19/03/2015 EXPERIMENTAL: conexiones persistentes (innecesario el uso de cierraBD())
    if(!$conexion){ 
        echo "Error estableciendo la conexi&oacute;n a la BBDD.<br />";
    }
    else{
        
        $_CONFIG['conexionMySQLi']=$conexion;//NUEVO: se añade un índice especial al array de configuración global para usar la conexión con mysqli_query()

        if(!mysql_select_db($_CONFIG['nombreBDFormacion'])){
            echo "Error seleccionando base de datos del software.<br />";
        }
    }

}

function pasarAFormacion($codigo,$asignarCurso,$codigoCurso){
    global $_CONFIG;
    $empleado=datosRegistro('empleados',$codigo);
    $usuario=consultaBD('SELECT usuarios.* FROM usuarios INNER JOIN usuarios_empleados ON usuarios.codigo=usuarios_empleados.codigoUsuario WHERE codigoEmpleado='.$empleado['codigo'],true,true);
    $empresa=datosRegistro('clientes',$empleado['codigoCliente']);
    $usuarioEmpresa=consultaBD('SELECT usuarios.* FROM usuarios INNER JOIN usuarios_clientes WHERE codigoCliente='.$empresa['codigo'],true,true);
    $puesto=datosRegistro('puestos_trabajo',$empleado['codigoPuestoTrabajo']);

    conexionBDFormacion();
    $sql='INSERT INTO usuarios VALUES (NULL,"'.$empleado['nombre'].'","'.$empleado['apellidos'].'","'.$empleado['dni'].'","'.$empleado['email'].'","'.$empleado['telefono'].'","'.$usuario['usuario'].'","'.$usuario['clave'].'",6,"","","NO","SI");';
    $res=consultaBD($sql);
    $codigoUsuario=mysql_insert_id();

    $empresaF=consultaBD('SELECT * FROM empresas WHERE cif="'.$empresa['EMPCIF'].'";',false,true);
    if($empresaF){
        $empresaF=$empresaF['codigo'];
    } else {
        $sql='INSERT INTO usuarios VALUES (NULL,"'.$usuarioEmpresa['nombre'].'","'.$usuarioEmpresa['apellidos'].'","'.$usuarioEmpresa['dni'].'","'.$usuarioEmpresa['email'].'","'.$usuarioEmpresa['telefono'].'","'.$usuarioEmpresa['usuario'].'","'.$usuarioEmpresa['clave'].'",7,"","","NO","SI");';
        $res=consultaBD($sql);
        $usuarioEmpresa=mysql_insert_id();
        $sql='INSERT INTO empresas VALUES (NULL,"'.$empresa['EMPNOMBRE'].'","'.$empresa['EMPCIF'].'",'.$usuarioEmpresa.',"'.$empresa['EMPRLTEL'].'",NULL);';
        $res=consultaBD($sql);
        $empresaF=mysql_insert_id();
    }

    $puestoF=consultaBD('SELECT * FROM puestos_trabajos WHERE nombre="'.$puesto['nombre'].'";',false,true);
    if($puestoF){
        $puestoF=$puestoF['codigo'];
    } else {
        $sql='INSERT INTO puestos_trabajos VALUES (NULL,"'.$puesto['nombre'].'");';
        $res=consultaBD($sql);
        $puestoF=mysql_insert_id();
    }

    $puestos_empresas=consultaBD('SELECT * FROM puestos_empresas WHERE codigoEmpresa='.$empresaF.' AND codigoPuesto='.$puestoF,false,true);
    if($puestos_empresas){
        $puestos_empresas=$puestos_empresas['codigo'];
    } else {
        $sql='INSERT INTO puestos_empresas VALUES (NULL,'.$empresaF.','.$puestoF.');';
        $res=consultaBD($sql);
        $puestos_empresas=mysql_insert_id();
    }

    $sql='INSERT INTO `puestos_empleados`(`codigo`, `codigoEmpleado`, `codigoEmpresa`, `codigoPuesto`, `fechaInicio`, `fechaFin`, `actual`, `firma`, `fechaCertificado`, `numeroCertificado`, `fechaUltimoAviso`) VALUES (NULL,'.$codigoUsuario.','.$empresaF.','.$puestos_empresas.',"'.fechaBD().'","0000-00-00","SI","","0000-00-00","","0000-00-00");';
    $res=consultaBD($sql);
    $codigoEmpleado=mysql_insert_id();
    if($asignarCurso=='PUESTO'){
        $orden=consultaBD('SELECT IFNULL(MAX(orden),0)+1 AS orden FROM cursos_puestos WHERE codigoPuesto='.$puestos_empresas,false,true);
        $sql='INSERT INTO cursos_puestos VALUES(NULL,'.$codigoCurso.','.$puestos_empresas.','.$orden['orden'].',"SI")';
        $res=consultaBD($sql);
        $codigoCurso=mysql_insert_id();
        $sql='INSERT INTO `cursos_empleados`(`codigo`, `codigoEmpleado`, `codigoCurso`, `fechaInicio`, `horaInicio`, `fechaFin`, `horaFin`, `intentos`, `finalizado`) VALUES (NULL,'.$codigoEmpleado.','.$codigoCurso.',"0000-00-00","0000-00-00","0000-00-00","00:00:00",0,"NO");';
        $res=consultaBD($sql);
        $cursoEmpleado=mysql_insert_id();
    } else {
        $sql='INSERT INTO cursos_puestos VALUES(NULL,'.$codigoCurso.',NULL,1,"SI")';
        $res=consultaBD($sql);
        $codigoCurso=mysql_insert_id();
        $sql='INSERT INTO `cursos_empleados`(`codigo`, `codigoEmpleado`, `codigoCurso`, `fechaInicio`, `horaInicio`, `fechaFin`, `horaFin`, `intentos`, `finalizado`) VALUES (NULL,'.$codigoEmpleado.','.$codigoCurso.',"0000-00-00","0000-00-00","0000-00-00","00:00:00",0,"NO");';
        $res=consultaBD($sql);
        $cursoEmpleado=mysql_insert_id();

    }
    $i=0;
    while(isset($_FILES['fichero'.$i])){
        $fichero=subeDocumento('fichero'.$i,time().$i,'../../anesco-formacion/documentos/personal');
        if($fichero!='NO'){
            $res=consultaBD('INSERT INTO documentos_formacion VALUES(NULL,'.$codigoUsuario.','.$cursoEmpleado.',"'.$_POST['nombreDocumento'.$i].'","'.$fichero.'","NO","'.date('Y-m-d').'");');
        }
        $i++;
    }
    cierraBD();
    $sql='INSERT INTO usuarios_sincronizados VALUES (NULL,'.$usuario['codigo'].','.$codigoUsuario.');';
    $res=consultaBD($sql,true);

    $destinatario=$empleado['email'];
    $bcc=$empresa['EMPEMAILPRINC'].',tecnico5@anescoprl.es';
    //$bcc='dexpositosanchez@gmail.com,programacion2@qmaconsultores.com';
    $asunto='Alta en formación';
    $mensaje='Estimado trabajador<br/><br/>
    Ya puedes acceder a su curso de formación online en el siguiente enlace <a href="https://crmparapymes.com.es/anesco-formacion/" target="_blank">https://crmparapymes.com.es/anesco-formacion/</a>. Para acceder a la misma deberás utilizar este usuario y contraseña:<br/><br/>
    Usuario: '.$usuario['usuario'].'<br/><br/>
    Contraseña: '.$usuario['clave'].'<br/><br/>
    Asimismo, podrá encontrar la información sobre los riesgos de su puesto de trabajo. Dicha información se encuentra en "documentos", el cual deberá marcar como "leído" una vez que, efectivamente, se haya leído, pudiendo así emitirse el correspondiente certificado de formación e información completo, dando cumplimiento a los artículos 18 y 19 de la Ley 31/1995 de PRL.
    <br/><br/>
    Le recordamos que una vez que haya confirmado la lectura de los riesgos de su puesto y finalice el curso correctamente, superando las pruebas finales del mismo, podrá descargar su certificado, tanto en esta plataforma, como en el portal del trabajador de anesco <a href="https://crmparapymes.com.es/anesco2/" target="_blank">https://crmparapymes.com.es/anesco2/</a> usando el mismo usuario y contraseñas facilitados.<br/><br/>Saludo cordiales';
    enviaEmail($destinatario,$asunto,$mensaje,false,$bcc);
}

function estaEnFormacion($codigoEmpleado,$conexion=true){
    $res=consultaBD('SELECT usuarios_sincronizados.* FROM usuarios_sincronizados INNER JOIN usuarios ON usuarios_sincronizados.usuarioPRL=usuarios.codigo INNER JOIN usuarios_empleados ON usuarios.codigo=usuarios_empleados.codigoUsuario WHERE usuarios_empleados.codigoEmpleado='.$codigoEmpleado,$conexion,true);
    return $res;
}

function preparaArrayTemas($fichero){
    $res=array();
    if($fichero=='agencia-de-viajes.php'){
        $res=array('Introduccion','Conceptos Básicos de Prevención','Riesgos Comunes en Agencias de Viajes y su Prevención','Atrapamiento por o entre objetos','Caida de objetos desprendidos','Caída de personas a distinto nivel','Caída de personas al mismo nivel','Carga Fisica','Carga Mental','Contactos Eléctricos','Cortes','Golpes','Iluminación Inadecuada','Incendios','Ruidos','Sobreesfuerzos','Temperaturas Ambientales Extremas','Primeros Auxilios');
    } 
    else if($fichero=='for-cambrers.php'){
        $res=array('DECLARACIÓ D’INTENCIONS','DEFINICIONS','CAUSES DELS ACCIDENTS','RELACIÓ ACCIDENT-QUALITAT','DRETS I OBLIGACIONS','SEGURETAT');
    } 
    else if($fichero=='calzado.php'){
        $res=array('Introduccion','Conceptos Básicos de Prevención','Caída de personas al mismo nivel','Caída de personas a distinto nivel','Contactos eléctricos directos e indirectos','Malas  condiciones medioambientales e inadecuadas  dimensiones del puesto de trabajo','Explosiones','Riesgos Psicosociales (Estrés laboral, Acoso moral, Burnout, etc)','Incendios','Primeros Auxilios','Cortado','Rebajado','Aparado','Moldeado de contrafuertes','Montado de puntas','Montado de talones y enfranques','Lijado','Aplicación de adhesivo','Pegado de suelas','Acabado','Empaquetado','Almacenado');
    }
    else if($fichero=='oficinas-despachos.php'){
        $res=array('Introduccion','Conceptos Básicos de Prevención','Caídas a distintos e igual nivel','Sobreesfuerzos posturales','Manipulación Manual de cargas','Fatiga visual','Confort acústico','Golpes y pisadas contra/sobre objetos','Temperatura','Calidad del aire interior','Factores psicosociales- fatiga mental','Contactos eléctricos','Equipos de trabajo','Utilizaciones de PVD','Incendios','Primeros auxilios');
    } else if($fichero=='limpiador.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Sobreesfuerzos por manipulación manual de cargas','Posturas forzadas.','Caídas al mismo y diferente nivel','Inhalación o contacto con productos químicos.','Riesgo Biológico','Caída de objetos por desplome','Contactos eléctricos','Accidentes in itinere / de tránsito','Riesgos psicosociales','Utilización de equipos de trabajo','Incendios','Primeros auxilios');
    } else if($fichero=='dependientes.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Caída de personas al mismo nivel','Caída de personas a distinto nivel','Caída de objetos en manipulación Quemaduras','Golpes y cortes por objetos o herramientas','Incendios','Carga física','Sobreesfuerzos','Inhalación o contacto con productos químicos.','Estrés laboral','Primeros auxilios');
    } else if($fichero=='camareros.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Caída de personas al mismo nivel','Caída de personas a distinto nivel','Caída de objetos en manipulación','Golpes y cortes por objetos o herramientas','Contactos térmicos o quemaduras','Carga física','Sobreesfuerzos','Estrés laboral','Riesgos provocados por la maquinaria. Normas de utilización','Incendios','Primeros auxilios');
    } else if($fichero=='cocineros.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Caída de personas al mismo nivel','Caída de objetos en manipulación','Golpes y cortes por objetos o herramientas','Quemaduras','Proyección de fragmentos o partículas','Exposición a cambios de temperatura o a temperaturas extremas','Contactos eléctricos','Explosiones','Riesgo químico: manejo de productos tóxicos y corrosivos','Exposición a contaminantes biológicos','Carga física','Sobreesfuerzos','Estrés laboral','Incendios','Primeros auxilios');
    } else if($fichero=='conductores.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Caída de personas al mismo nivel','Carga física','Sobreesfuerzos por manipulación manual de cargas','Patologías o enfermedades causadas por deficiencias ergonómicas o posturales','Ruido','Exposición a Vibraciones','Contacto con sustancias químicas','Exposición a condiciones medioambientales desfavorables','Accidentes de Tráfico','Transpaletas manuales','Riesgos en almacenes','Incendios','Primeros auxilios');
    } else if($fichero=='electricistas.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Definiciones sobre Electricidad','Riesgo eléctrico','Efectos nocivos de la electricidad','Efectos de la corriente eléctrica en el cuerpo humano','Tipos de contacto eléctrico','Factores de los efectos de la corriente eléctrica','Medidas de seguridad contra riesgos eléctricos','Equipos de protección individual','Equipos de trabajo','Trabajos sin tensión','Trabajos con tensión','Sobreesfuerzos por manipulación manual de cargas','Posturas forzadas.','Caídas al mismo y diferente nivel','Cortes con herramientas ','Caída de objetos por desplome','Riesgo de incendio','Primeros auxilios');
    } else if($fichero=='carniceros.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Caída de personas al mismo nivel','Caída de personas a distinto nivel','Golpes contra objetos inmóviles','Estrés térmico','Contactos eléctricos','Incendios','Accidentes en tránsito','Primeros Auxilios','Caída de objetos por manipulación','Cortes y contactos con elementos móviles de los equipos de trabajo','Cortes por utensilios de corte','Sobreesfuerzos','Exposición a agentes biológicos','Seguridad en el manejo de los equipos de trabajo específicos en el sector del comercio minorista de la carne'); 
    } else if($fichero=='supermercados.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Caída a distinto o igual nivel','Productos químicos','Cortes por objeto o herramientas','Riesgo eléctrico','Condiciones ambientales','Sobreesfuerzos y posturas forzadas','Riesgos biológicos','Riesgos psicosociales','Riesgo por puesto de trabajo y departamento','Accidentes in itinere','Incendios','Primeros auxilios');
    } else if($fichero=='salas-de-fiestas.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Caídas a distinto y al mismo nivel','Caída de objetos por desplome o desprendidos','Caída de objetos por manipulación','Cortes por objetos y utensilios de trabajo','Golpes contra objetos inmóviles','Pisadas sobre objetos y cortes por objetos','Atrapamientos por y entre objetos','Contactos eléctricos','Incendios y medidas de emergencia','Exposición a ruido','Manipulación de productos de limpieza','Estrés o disconfort térmico','Sobreesfuerzos','Fatiga Física','Fatiga mental, estrés laboral y trabajo nocturno','Violencia de terceros. Pautas de actuación','Primeros Auxilios','Explosión e incendio');
    } else if($fichero=='pintoresVehiculos.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Falta de Orden y Limpieza en los Lugares de Trabajo','Riesgo eléctrico','Sobreesfuerzos por manipulación manual de cargas','Posturas forzadas.','Caídas al mismo y diferente nivel','Riesgos uso de Herramientas Manuales','Riesgos uso de Equipos de Trabajo','Pintor','Chapista','Incendios','Primeros auxilios');
    } else if($fichero=='mecanicos.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Caída de personas a diferente nivel','Caída de personas al mismo nivel','Caída de objetos por desplome o derrumbamiento','Caída de objetos por manipulación','Caída de objetos desprendidos','Choques contra objetos','Golpes/cortes por objetos o herramientas','Proyección de fragmentos o partículas','Atrapamientos entre piezas o transmisiones','Contactos térmicos','Contactos eléctricos','Explosiones','Atropellos, golpes y choques con vehículos','Esfuerzo físico general Posturas forzadas','Movimientos repetitivos y esfuerzo muscular localizado','Riesgo de sobreesfuerzos','Disconfort ambiental','Inhalación de gases o vapores tóxicos','Inhalación de polvo','Contacto con sustancias nocivas','Exposición a ruido','Equipos de protección individuales','Incendios','Primeros auxilios');
    } else if($fichero=='jardineros.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Sobreesfuerzos por manipulación manual de cargas','Posturas forzadas.','Caídas al mismo y diferente nivel','Uso de herramientas y maquinas','Caída de objetos por desplome','Contactos eléctricos','Riesgos de exposición a sustancias peligrosas','Calor y frío','Accidentes in itinere','Riesgos psicosociales','Incendios','Primeros auxilios');
    } else if($fichero=='camareras-pisos.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Caídas al mismo nivel','Caídas a distinto nivel','cortes, pinchazos y golpes','manipulación y uso de los productos químicos utilizados','ruido','condiciones ambientales de los lugares de trabajo','iluminación','riesgos derivados de la manipulación de cargas','posturas forzadas','riesgos derivados de los movimientos repetidos','estrés laboral','caída de objetos por desplome','contactos eléctricos','accidentes in itinere / de tránsito','Incendios','Primeros auxilios');
    } else if($fichero=='recepcionistas-conserjes.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','riesgos derivados de la manipulación de cargas','caídas a distintos e igual nivel','sobreesfuerzos posturales','fatiga visual','confort acústico','temperatura','equipos de trabajo','requisitos del entorno de trabajo','factores psicosociales','trabajos a turnos y nocturno','contactos eléctricos','accidentes in itinere / de tránsito','Incendios','Primeros auxilios');
    } else if($fichero=='peluquerias-estetica.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Orden y limpieza','Contactos térmicos y eléctricos','Riesgo de corte con objetos y herramientas (tijeras, navajas, etc.)','Inhalación o contacto con productos químicos','Riesgos biológicos','Carga física postural','Carga mental','Riesgo de radiaciones','Sobreesfuerzos por manipulación manual de cargas','accidentes in itinere / de tránsito','Incendios','Primeros auxilios');
    } else if($fichero=='barnizadores.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Caídas de personas a distinto nivel Exterior','Caídas de personas a distinto nivel Interior','Medidas preventivas en el uso de medios auxiliares para trabajos en altura','Inhalación o ingestión de sustancias nocivas','Exposición a sustancias nocivas por contacto','Riesgos causados por falta de Orden y Limpieza en los Lugares de Trabajo','Clima exterior Riesgo eléctrico','Sobreesfuerzos por manipulación manual de cargas','Posturas forzadas','Riesgos uso de Herramientas Manuales','Riesgos uso de Equipos de Trabajo','Incendios','Primeros auxilios');
    } else if($fichero=='obradores.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Caídas al mismo nivel', 'Golpes, pinchazos y cortes con objetos o herramientas manuales', 'Riesgos de cortes producidos por contacto con elementos móviles de las máquinas', 'Amasadora', 'Batidora', 'Laminadora', 'Elevador basculador', 'Contacto eléctrico', 'Contacto térmico', 'Riesgo de incendio y explosión', 'Derivados de la exposición a productos químicos', 'Derivados a la exposición a agentes físicos', 'Posturas forzadas en el trabajo', 'Manipulación manual de cargas', 'Tareas repetitivas', 'Por contacto con agentes biológicos presentes', 'Factores psicosociales: Estrés laboral', 'Condiciones ambientales', 'Equipos de protección individual', 'Incendios', 'Primeros auxilios');
    } else if($fichero=='servicios-tecnicos.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Sobreesfuerzos por manipulación manual de cargas', 'Posturas forzadas', 'Caídas al mismo y diferente nivel', 'Inhalación o contacto con productos químicos', 'Cortes con herramientas', 'Caída de objetos por desplome', 'Accidentes in itinere', 'Riesgos psicosociales', 'Incendios', 'Primeros auxilios', 'Riesgos en reparaciones eléctricas', 'Riesgo en reparaciones de fontanería', 'Riesgos en reparaciones de albañilería y pintura', 'Riesgos en tareas de jardinería', 'Mantenimiento de piscinas');
    }  else if($fichero=='personal-sanitario.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Riesgos higiénicos', 'Riesgos relacionados con Agentes Biológicos', 'Accidente laboral con riesgo de exposición a sangre o fluidos corporales ','Riesgo ergonómico relacionado con la postura del trabajo', 'Riesgo ergonómico relacionado con la manipulación manual de cargas', 'Riesgos ergonómicos durante la movilización de pacientes', 'Riesgo de caídas en el mismo plano', 'Riesgos de caídas en altura', 'Riesgos de agresión','Golpes contra objetos inmóviles','Riesgo de contacto eléctrico','Riesgos relacionados con almacenamiento','Exposición a radiaciones ionizantes','Riesgo de exposición a radiaciones no ionizantes','Riesgo de incendio','Primeros auxilios');
    } else if($fichero=='conductor-autobus.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Fatiga mental y estrés', 'Lesiones musculoesqueléticas (lumbalgias)', 'Exposición a vibraciones', 'Exposición a ruido','Accidentes de tráfico', 'Somnolencia', 'Caídas al mismo nivel', 'Atropellos y golpes contra vehículos', 'Iluminación: fatiga visual', 'Condiciones ambientales', 'Golpes, cortes, atrapamientos con objetos y herramientas','Contacto eléctrico','Acciones violentas, atracos','Incendios','Primeros auxilios');
    } else if($fichero=='prl-docentes.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Riesgos ergonómicos','Sobreesfuerzos por manipulación manual de cargas', 'Patologías o enfermedades causadas por deficiencias ergonómicas o posturales', 'Caídas al mismo y diferente nivel','Cortes con herramientas (tijeras, grapadora, etc.)', 'Golpes contra objetos inmóviles', 'Caída de objetos por desplome', 'Contactos eléctricos', 'Riesgo foniatrico', 'Accidentes in itinere','Riesgos psicosociales','Incendios','Primeros auxilios'); 
    } else if($fichero=='mantenimiento-embarcaciones.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Caidas al mismo nivel', 'Caidas a distinto nivel', 'Máquinas portátiles','Almacenamiento y manipulación de productos químicos', 'Trabajos con fibra de vidrio', 'Patologías o enfermedades causadas por movimientos repetitivos', 'Contactos eléctricos', 'Accidentes in itinere','Riesgos psicosociales', 'Incendios', 'Primeros auxilios');
    } else if($fichero=='monitor-deportivo.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Riesgos ergonómicos', 'Trabajos en el exterior', 'Afonías','Contactos eléctricos', 'Accidentes in itinere / de tránsito', 'Caídas al mismo y diferente nivel', 'Golpes contra objetos inmóviles', 'Caída de objetos por desplome','Sobreesfuerzos por manipulación manual de cargas', 'Riesgos psicosociales','Incendios', 'Primeros auxilios');
    }  else if($fichero=='mantenimiento-piscinas.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Riesgos para trabajadores de mantenimiento de piscinas','Riesgos derivados de las condiciones del terreno (suelos, etc.)', 'Riesgos de accidente de tráfico', 'Riesgos derivados de los equipos e instalaciones','Riesgo eléctrico por contacto directo o indirecto', 'Golpes y atrapamientos', 'Riesgos de golpes y otros al manipular cargas', 'Riesgo de contacto térmico o quemadura', 'Riesgo de proyecciones de partículas', 'Riesgo de caída de materiales (piezas, etc.)','Riesgo de exposición a agentes físicos','Riesgos derivados de los productos químicos','Riesgos en la limpieza de piscinas y filtros','Sobreesfuerzos por manipulación manual de cargas','Posturas forzadas','Incendios','Primeros auxilios'); 
    } else if($fichero=='joyeria-bisuteria.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Principales riesgos del sector de la joyería y bisutería','Máquinas y herramientas que se utilizan más comúnmente','Riesgos de cortes', 'Riesgo de atrapamientos', 'Riesgo de golpes','Riesgo de proyección de partículas', 'Riesgo de caídas de igual y distinto nivel', 'Riesgo de contactos eléctricos', 'Riesgo de quemaduras', 'Riesgo de ruido', 'Riesgo de exposición a sustancias nocivas o peligrosas','Riesgo de sobreesfuerzos','Riesgo de incendios','Primeros auxilios'); 
    } else if($fichero=='profesor-autoescuela.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Cuidados de la voz', 'Riesgos ergonómicos', 'El ambiente interior del vehículo. El calor','El estrés laboral en monitores de autoescuela', 'Accidentes de tráfico', 'Trabajos con PVD', 'Otros riesgos','Incendios','Primeros auxilios'); 
    } else if($fichero=='socorristas.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Principales riesgos para socorristas','Riesgos de exposición a contaminantes', 'Contactos eléctricos', 'Sobreesfuerzos por manipulación manual de cargas','Posturas forzadas', 'Caídas al mismo y diferente nivel', 'Caída de objetos por desplome', 'Afonías','Exposición al sol','Riesgo de incendio','Primeros auxilios'); 
    } else if($fichero=='patron-marinero-amarrador.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Caída de personas al mismo nivel','Caída de personas a distinto nivel', 'Postura de trabajo de pie', 'Caída de objetos por derrumbamiento o golpes por objetos','Atropellos o golpes con vehículos', 'Golpes y cortes con objetos o herramientas', 'Exposición a temperaturas extremas', 'Contactos eléctricos','Sobreesfuerzos','Estrés laboral','Explosiones e incendios','Accidentes en itinere','Contacto con sustancias causticas o corrosivos','Primeros auxilios'); 
    } else if($fichero=='guia-turistico.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Sobreesfuerzos por manipulación manual de cargas', 'Patologías o enfermedades causadas por deficiencias ergonómicas o posturales', 'Caídas al mismo y diferente nivel','Cortes con herramientas (tijeras, grapadora, etc.)', 'Caída de objetos por desplome',  'Contactos eléctricos', 'Accidentes in itinere','Afonías','Riesgos psicosociales', 'Incendios', 'Primeros auxilios');
    } else if($fichero=='guia-trabajo-exterior.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Sobreesfuerzos por manipulación manual de cargas', 'Patologías o enfermedades causadas por deficiencias ergonómicas o posturales', 'Caídas al mismo y diferente nivel','Cortes con herramientas (tijeras, grapadora, etc.)', 'Caída de objetos por desplome',  'Contactos eléctricos', 'Accidentes in itinere','Afonías','Riesgos psicosociales','Riesgo biológico','Condiciones ambientales adversas -Trabajos en el exterior-', 'Incendios', 'Primeros auxilios');
    } else if($fichero=='conductor-camion.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Caídas al mismo nivel', 'Carga física','Sobreesfuerzos por manipulación manual de cargas', 'Patologías o enfermedades causadas por deficiencias ergonómicas o posturales','Cuidados al estar de pie', 'Stress laboral','Exposición a vibraciones', 'Contactos con sustancias químicas', 'Exposición a condiciones medioambientales desfavorables', 'Accidentes de tráfico', 'Otras recomendaciones','Incendios','Primeros auxilios');
    } else if($fichero=='repartidor-moto.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Caídas al mismo nivel', 'Golpes en estanterias, armarios y cajones','Carga física','Sobreesfuerzos por manipulación manual de cargas','Patologías o enfermedades causadas por deficiencias ergonómicas o posturales','Exposición a vibraciones', 'Contactos con sustancias químicas', 'Exposición a condiciones medioambientales desfavorables', 'Accidentes de tráfico','Incendios','Primeros auxilios');
    }  else if($fichero=='carpinteria-metalica.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Riesgos para el puesto de carpintero metálico', 'Riesgos por falta de orden y limpieza', 'Riesgo eléctrico','Sobreesfuerzos por manipulación manual de cargas', 'Posturas forzadas', 'Riesgos uso de Herramientas Manuales', 'Riesgos uso de Equipos de Trabajo','Otros riesgos', 'Manipulación de productos químicos', 'Trabajos de soldadura','Exposición a ruido y vibraciones','Trabajos en el exterior del taller','Seguridad Vial', 'Incendios', 'Primeros auxilios');
    } 
     else if($fichero=='trabajadores-almacen.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Sobreesfuerzos por manipulación manual de cargas','Equipos de trabajo', 'Mantenimiento de los equipos', 'Normas de seguridad en el manejo de carretillas elevadoras','Normas de seguridad en el manejo de transpaletas Manuales', 'Normas de seguridad en el manejo de apiladores', 'Normas para la carga y cambio de batería en equipos eléctricos', 'Caída de objetos por desplome-Aspectos a tener en cuenta en el apilamiento de materiales-', 'Caídas al mismo nivel -Orden y Limpieza-', 'Caídas a distinto nivel-Escaleras de mano-','Equipos de protección individual','Riesgo eléctrico','Accidentes in itinere','Incendios','Primeros auxilios'); 
    } 
    else if($fichero=='prl-animacion.php'){
        $res = array('Introduccion','Conceptos Básicos de Prevención','Sobreesfuerzos por manipulación manual de cargas', 'Patologías o enfermedades causadas por deficiencias ergonómicas o posturales', 'Caídas al mismo y diferente nivel','Cortes con herramientas (tijeras, grapadora, etc.)', 'Golpes contra objetos inmóviles', 'Caída de objetos por desplome', 'Contactos eléctricos', 'Afonías','Exposición al sol', 'Accidentes in itinere','Riesgos psicosociales','Incendios','Primeros auxilios'); 
    } 
    else if($fichero=='seguridad-privada.php'){
        $res = array('Introduccion','Espacios y lugares de trabajo','Contactos eléctricos','Equipos de protección individual', 'Manipulación de cargas', 'Trabajo con pantallas de visualización de datos y monitores de TV','Turnicidad', 'Seguridad vial', 'Riesgos biológicos del trabajo con perros', 'Trabajar con armas de fuego', 'Riesgo de incendio', 'Actuación en caso de emergencia');
    } 
    else if($fichero=='seguridad-incendios.php'){
        $res = array('Introduccion','El fuego','Teorías sobre el fuego','Factores necesarios para que se produzca un incendio', 'El desarrollo de un incendio', 'Peligros para las personas afectadas por un incendio','La actuación contra los incendios', 'La prevención de incendios', 'La extinción de incendios', 'La organización de seguridad contra incendios','Características y métodos de utilización de los extintores portátiles y de las bocas de incendio equipadas'); 
    } 
    else if($fichero=='carpinteria-madera.php'){
        $res = array('Introduccion','Falta de orden y limpieza','Seguridad frente a riesgos eléctricos','Manipulación manual de cargas', 'Herramientas de mano y de potencia', 'Máquinas','Carretillas elevadoras', 'Posturas forzadas', 'Manipulación de productos químicos', 'Trabajos en atmósferas con riesgo de incendio y explosión','Exposición a ruido y vibraciones','Uso de dispositivos de alcance en altura','Instalaciones y montajes','Seguridad Vial','Medidas para la Prevención de Incendios','Primeros auxilios'); 
    }
    else if($fichero=='periodista.php'){
        $res = array('Introduccion','Conceptos básicos - Definiciones','Conceptos básicos - Marco normativo básicos en materia de PRL','Principales riesgos','Sobreesfuerzos por manipulación manual de cargas','Patologías o enfermedades causadas por deficiencias ergonómicas o posturales','Caídas al mismo y diferente nivel','Cortes con herramientas (tijeras, grapadora, etc.)','Golpes contra objetos inmóviles','Caída de objetos por desplome','Contactos eléctricos','Accidentes in itinere / de tránsito','Riesgos psicosociales','Incendios','Primeros auxilios'); 
    } 
    else if($fichero=='fotografo.php'){
        $res = array('Introduccion','Conceptos básicos - Definiciones','Conceptos básicos - Marco normativo básicos en materia de PRL','Principales riesgos','Sobreesfuerzos por manipulación manual de cargas','Patologías o enfermedades causadas por deficiencias ergonómicas o posturales','Caídas al mismo y diferente nivel','Cortes con herramientas','Golpes contra objetos inmóviles','Caída de objetos por desplome','Contactos eléctricos','Accidentes in itinere / de tránsito','Riesgos psicosociales','Incendios','Primeros auxilios'); 
    } 
    else if($fichero=='artes-graficas.php'){
        $res = array('Introduccion','Conceptos básicos - Definiciones','Conceptos básicos - Marco normativo básicos en materia de PRL','Riesgos específicos del sector','Preimpresión','Impresión','Riesgos y medidas de prevención','Riesgos generales','Patologías o enfermedades causadas por deficiencias ergonómicas o posturales','Caídas al mismo y diferente nivel','Cortes con herramientas','Golpes contra objetos inmóviles','Caída de objetos por desplome','Contactos eléctricos','Riesgos psicosociales','Riesgos de accidentes de tráfico','Incendios','Primeros auxilios'); 
    } 
    else if($fichero=='tecnicos-obras.php'){
        $res = array('Introduccion','Conceptos básicos - Definiciones','Conceptos básicos - Marco normativo básicos en materia de PRL','Riesgos y su prevención','Caídas al mismo nivel y pisadas sobre objetos','Caídas a diferente nivel','Golpes y cortes','Caída de objetos en manipulación o desprendidos','Sobreesfuerzos','Posturas forzadas','Contacto con productos químicos e intoxicaciones','Contactos eléctricos','Mantenimiento de los equipos de trabajo','Exposición a ruido y vibraciones','El estrés laboral','Accidentes de tráfico','Utilizaciones de PVD','Incendios','Primeros auxilios'); 
    } 
    else if($fichero=='covid19.php'){
        $res = array('Introduccion','¿Qué es el COVID-19?','¿Cómo se transmite el coronavirus?','Medidas que podemos tomar - Medidas de Seguridad y control','Medidas que podemos tomar - Medidas higiénicas generales','Medidas que podemos tomar - ¿Qué hacer si tienes uno de estos síntomas?','Medidas que podemos tomar - Medidas para manejar el estrés','Buenas prácticas en el Centro Laboral','Buenas prácticas en el Centro Laboral - Antes de ir al trabajo','Buenas prácticas en el Centro Laboral -  Desplazamiento al trabajo','Buenas prácticas en el Centro Laboral - En el centro de trabajo','Medidas organizativas','Recomendaciones a los trabajadores','Medidas de higiene en el centro de trabajo','Gestión de residuos en los centros de trabajo','Utilización de guantes de protección','Uso correcto de mascarillas','Uso correcto de mascarillas - ¿Cómo utilizar la mascarilla correctamente?','Uso correcto de mascarillas - Tipos de mascarilla y su protección','Uso correcto de mascarillas - Algunos consejos importantes','Medidas preventivas del Gobierno'); 
    } 



    return $res;

}

function pestaniasClientes($datos){
        cierraPestaniaAPI();
        abrePestaniaAPI(2);
            tablaGestionesClientes($datos['codigo']);
        cierraPestaniaAPI();
        abrePestaniaAPI(3);
            empleadosFormadosClientes($datos['codigo']);
        cierraPestaniaAPI();
        abrePestaniaAPI(4);
            creaPestaniasAPI(array('Administración','Técnico','Vigilancia de la Salud'),'doc');
                abrePestaniaAPI('doc1',true);
                tablaDocumentacionClientes($datos['codigo'],'administracion');
                cierraPestaniaAPI();
                abrePestaniaAPI('doc2',false);
                tablaDocumentacionClientes($datos['codigo'],'tecnico');
                cierraPestaniaAPI();
                abrePestaniaAPI('doc3',false);
                tablaDocumentacionClientes($datos['codigo'],'vs');
                cierraPestaniaAPI();
            cierraPestaniasAPI();
        cierraPestaniaAPI();
        abrePestaniaAPI(5);
            tablaContratosEnVigorClientes($datos['codigo']);
        cierraPestaniaAPI();
        abrePestaniaAPI(6);
            listadoAptosClientes($datos['codigo']);
        cierraPestaniaAPI();
        abrePestaniaAPI(7);
            listadoFormacionInformacionClientes($datos['codigo']);
        cierraPestaniaAPI();
        cierraPestaniasAPI();    
}

function tablaDocumentacionClientes($codigoCliente,$tipo){
    echo '
    <div class="span10">            
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Documentación disponible para descarga</h3>
          </div>
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Documento </th>';
                if($tipo=='vs'){
                  echo '
                  <th>Empleado</th>';
                }
    echo '
                  <th> Fecha </th>
                  <th class="centro"></th>
                </tr>
              </thead>
              <tbody>';
                imprimeDocumentacionCliente($tipo,$codigoCliente);
    echo '    
              </tbody>
            </table>
          </div>
        </div>
    </div>';

}

function imprimeDocumentacionCliente($tipo,$codigoCliente=''){
    $seccionCliente=false;
    if($_SESSION['tipoUsuario']=='CLIENTE'){
        $codigoUsuario=$_SESSION['codigoU'];
    } 
    elseif(isset($_POST['codigoCliente'])){
        $codigoUsuario=$_POST['codigoCliente'];
    }
    else if($codigoCliente!=''){
        $item=consultaBD('SELECT codigoUsuario FROM usuarios_clientes WHERE codigoCliente='.$codigoCliente,true,true);
        $codigoUsuario=$item['codigoUsuario'];
        $seccionCliente=true;
    }
    else {
        $codigoUsuario=false;
    }
    $ejercicio=obtieneEjercicioFiltro();

    if($tipo=='administracion'){
        //imprimeContratosDOC($codigoUsuario,$ejercicio,$seccionCliente);
        imprimeFacturasDOC($codigoUsuario,$ejercicio,$seccionCliente);
        imprimeFacturasRMDOC($codigoUsuario,$ejercicio,$seccionCliente);
        imprimeDocumentacionClientesDOC($codigoUsuario,$ejercicio,$tipo,$seccionCliente);
    }
    elseif($tipo=='tecnico'){
        imprimeDocumentosPRLDOC($codigoUsuario,$ejercicio,'documentos_info',$seccionCliente);
        imprimeDocumentosPRLDOC($codigoUsuario,$ejercicio,'documentos_plan_prev',$seccionCliente);
        imprimeInformesAccidentesDOC($codigoUsuario,$ejercicio,$seccionCliente);
        imprimeDocumentacionClientesDOC($codigoUsuario,$ejercicio,$tipo,$seccionCliente);
        imprimePlanPrevDOC($codigoUsuario,$ejercicio,$seccionCliente);
        imprimeEvaluacionRiesgosDOC($codigoUsuario,$ejercicio,$seccionCliente);
    }
    else{
        imprimeDocumentacionAptosDOC($codigoUsuario,$ejercicio,$seccionCliente);
        imprimeDocumentacionClientesDOC($codigoUsuario,$ejercicio,$tipo,$seccionCliente,true);
        imprimeDocumentosVSDOC($codigoUsuario,$seccionCliente);
    }


}

function imprimeContratosDOC($codigoUsuario,$ejercicio,$seccionCliente){
    $where="usuarios_clientes.codigoUsuario='$codigoUsuario' AND";
    if(!$codigoUsuario){
        $where='';
    }

    $whereEjercicio1=compruebaEjercicioParaWhere("YEAR(contratos.fechaInicio)",$ejercicio);
    $whereEjercicio2=compruebaEjercicioParaWhere("YEAR(contratos.fechaFin)",$ejercicio);


    $consulta=consultaBD("SELECT contratos.codigo, EMPCP, EMPID, contratos.fechaInicio, contratos.codigoInterno, EMPNOMBRE, IFNULL(CONCAT(usuarios.nombre,' ',usuarios.apellidos),'-') AS usuario
                          FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
                          INNER JOIN usuarios_clientes ON ofertas.codigoCliente=usuarios_clientes.codigoCliente
                          INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
                          LEFT JOIN usuarios ON ofertas.codigoUsuario=usuarios.codigo
                          WHERE ".$where." contratos.eliminado='NO' AND ($whereEjercicio1 OR $whereEjercicio2);",true);

    while($datos=mysql_fetch_assoc($consulta)){
        echo "
            <tr>
                <td>Contrato ".formateaReferenciaContrato($datos,$datos)."</td>";

        if($_SESSION['tipoUsuario']!='CLIENTE' && !$seccionCliente){
            echo "
                <td>".$datos['EMPID']." - ".$datos['EMPNOMBRE']."</td>
                <td>".$datos['usuario']."</td>";
        }

        echo "
                <td>".formateaFechaWeb($datos['fechaInicio'])."</td>
                <td class='centro'>
                    <a href='../contratos/generaDocumento.php?codigoContrato=".$datos['codigo']."' class='btn btn-propio noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar</i></a>
                </td>
            </tr>";
    }
}


function imprimeFacturasDOC($codigoUsuario,$ejercicio,$seccionCliente){
    $where="AND codigoUsuario='$codigoUsuario'";
    if(!$codigoUsuario){
        $where='';
    }   

    $whereEjercicio=compruebaEjercicioParaWhere("YEAR(facturas.fecha)",$ejercicio);

    $consulta=consultaBD("SELECT facturas.codigo, facturas.fecha, serie, facturas.numero, tipoFactura, EMPID, EMPNOMBRE, IFNULL(CONCAT(usuarios.nombre,' ',usuarios.apellidos),'-') AS usuario
                          FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
                          LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
                          LEFT JOIN usuarios_clientes ON facturas.codigoCliente=usuarios_clientes.codigoCliente
                          LEFT JOIN usuarios ON facturas.codigoUsuarioFactura=usuarios.codigo
                          WHERE facturas.tipoFactura!='ABONO' AND facturas.eliminado='NO' AND $whereEjercicio ".$where."
                          GROUP BY facturas.codigo",true);

    while($datos=mysql_fetch_assoc($consulta)){
        
        $numeroFactura='PROFORMA';
        $enlace="../proformas/generaFactura.php?codigo=".$datos['codigo'];
        if($datos['tipoFactura']!='PROFORMA'){
            $numeroFactura=$datos['serie'].'-'.$datos['numero'];
            $enlace="../facturas/generaFactura.php?codigo=".$datos['codigo'];
        }

        echo "<tr>
                <td>".$numeroFactura."</td>";

        if($_SESSION['tipoUsuario']!='CLIENTE' && !$seccionCliente){
            echo "
                <td>".$datos['EMPID']." - ".$datos['EMPNOMBRE']."</td>
                <td>".$datos['usuario']."</td>";
        }
        
        echo "      
                <td>".formateaFechaWeb($datos['fecha'])."</td>
                <td class='centro'>
                    <a href='$enlace' class='btn btn-propio noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar</i></a>
                </td>
            </tr>";
    }
}


function imprimeFacturasRMDOC($codigoUsuario,$ejercicio,$seccionCliente){
    $where="AND (usuarios_clientes.codigoUsuario='$codigoUsuario' OR uc2.codigoUsuario='$codigoUsuario')";
    if(!$codigoUsuario){
        $where='';
    }

    $whereEjercicio=compruebaEjercicioParaWhere("YEAR(facturas_reconocimientos_medicos.fecha)",$ejercicio); 

    $consulta=consultaBD("SELECT facturas_reconocimientos_medicos.codigo, series_facturas.serie, facturas_reconocimientos_medicos.numero, facturas_reconocimientos_medicos.fecha,
                          EMPID, EMPNOMBRE, IFNULL(CONCAT(usuarios.nombre,' ',usuarios.apellidos),'-') AS usuario

                          FROM facturas_reconocimientos_medicos LEFT JOIN contratos ON facturas_reconocimientos_medicos.codigoContrato=contratos.codigo
                          LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
                          LEFT JOIN usuarios_clientes ON ofertas.codigoCliente=usuarios_clientes.codigoCliente
                          LEFT JOIN series_facturas ON facturas_reconocimientos_medicos.codigoSerieFactura=series_facturas.codigo
                          LEFT JOIN usuarios_clientes uc2 ON facturas_reconocimientos_medicos.codigoClienteParaConceptoManual=uc2.codigoCliente
                          LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo
                          LEFT JOIN usuarios ON ofertas.codigoUsuario=usuarios.codigo

                          WHERE $whereEjercicio ".$where."
                          AND facturas_reconocimientos_medicos.eliminado='NO';",true);

    while($datos=mysql_fetch_assoc($consulta)){
        echo "<tr>
                <td>".$datos['serie'].'-'.$datos['numero']."</td>";

        if($_SESSION['tipoUsuario']!='CLIENTE' && !$seccionCliente){
            echo "
                <td>".$datos['EMPID']." - ".$datos['EMPNOMBRE']."</td>
                <td>".$datos['usuario']."</td>";
        }
        
        echo "  <td>".formateaFechaWeb($datos['fecha'])."</td>
                <td class='centro'>
                    <a href='../facturas-reconocimientos/generaFactura.php?codigo=".$datos['codigo']."' class='btn btn-propio noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar</i></a>
                </td>
            </tr>";
    }
}


function imprimeDocumentosPRLDOC($codigoUsuario,$ejercicio,$tabla,$seccionCliente){
    $where="usuarios_clientes.codigoUsuario='$codigoUsuario' AND";
    if(!$codigoUsuario){
        $where='';
    }


    $whereEjercicio=compruebaEjercicioParaWhere("YEAR(formularioPRL.fecha)",$ejercicio);

    $consulta=consultaBD("SELECT $tabla.nombre, $tabla.fichero, formularioPRL.fecha, EMPNOMBRE
                          FROM $tabla INNER JOIN formularioPRL ON $tabla.codigoFormulario=formularioPRL.codigo
                          INNER JOIN contratos ON formularioPRL.codigoContrato=contratos.codigo
                          INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
                          INNER JOIN usuarios_clientes ON ofertas.codigoCliente=usuarios_clientes.codigoCliente
                          LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo
                          WHERE ".$where." $whereEjercicio;",true);

    while($datos=mysql_fetch_assoc($consulta)){
        echo "
            <tr>
                <td>".$datos['nombre']."</td>";

        if($_SESSION['tipoUsuario']!='CLIENTE' && !$seccionCliente){
            echo "
                <td>".$datos['EMPID']." - ".$datos['EMPNOMBRE']."</td>
                <td>-</td>";
        }
        
        echo "  <td>".formateaFechaWeb($datos['fecha'])."</td>
                <td class='centro'>
                    <a href='../documentos/planificacion/".$datos['fichero']."' class='btn btn-propio noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar</i></a>
                </td>
            </tr>";
    }
}



function imprimeInformesAccidentesDOC($codigoUsuario,$ejercicio,$seccionCliente){
    $where='';
    if($codigoUsuario){
        $where="AND checkVisible='SI' AND codigoUsuario='$codigoUsuario'";
    }

    $whereEjercicio=compruebaEjercicioParaWhere("YEAR(fechaInvestigacion)",$ejercicio);

    $consulta=consultaBD("SELECT accidentes.codigo, fechaInvestigacion, EMPID, EMPNOMBRE, IFNULL(CONCAT(usuarios.nombre,' ',usuarios.apellidos),'-') AS usuario
                          FROM accidentes LEFT JOIN empleados ON accidentes.codigoEmpleado=empleados.codigo
                          LEFT JOIN usuarios_clientes ON empleados.codigoCliente=usuarios_clientes.codigoCliente
                          INNER JOIN clientes ON usuarios_clientes.codigoCliente=clientes.codigo
                          LEFT JOIN usuarios ON accidentes.codigoUsuarioTecnico=usuarios.codigo
                          WHERE $whereEjercicio ".$where,true);

    while($datos=mysql_fetch_assoc($consulta)){
        $nombre='Informe de accidente';

        echo "
            <tr>
                <td>".$nombre."</td>";

        if($_SESSION['tipoUsuario']!='CLIENTE' && !$seccionCliente){
            echo "
                <td>".$datos['EMPID']." - ".$datos['EMPNOMBRE']."</td>
                <td>".$datos['usuario']."</td>";
        }
        
        echo "  <td>".formateaFechaWeb($datos['fechaInvestigacion'])."</td>
                <td class='centro'>
                    <a target='_blank' class='btn btn-propio noAjax' href='../accidentes/generaDocumento.php?codigo=".$datos['codigo']."'><i class='icon-download'></i> Descargar</i></a>
                </td>
            </tr>";
    }
}



function imprimeDocumentacionAptosDOC($codigoUsuario,$ejercicio,$seccionCliente){
    $where="usuarios_clientes.codigoUsuario='$codigoUsuario' AND";
    if(!$codigoUsuario){
        $where='';
    }

    $whereEjercicio=compruebaEjercicioParaWhere("YEAR(reconocimientos_medicos.fecha)",$ejercicio);

    $consulta=consultaBD("SELECT MD5(reconocimientos_medicos.codigo) AS codigo, reconocimientos_medicos.fecha, CONCAT(empleados.nombre,' ',empleados.apellidos) AS empleado, 
                          EMPID, EMPNOMBRE, IFNULL(CONCAT(usuarios.nombre,' ',usuarios.apellidos),'-') AS usuario
                          FROM reconocimientos_medicos INNER JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
                          LEFT JOIN clientes ON empleados.codigoCliente=clientes.codigo
                          LEFT JOIN usuarios_clientes ON empleados.codigoCliente=usuarios_clientes.codigoCliente
                          LEFT JOIN usuarios ON reconocimientos_medicos.codigoUsuario=usuarios.codigo
                          WHERE ".$where." aptoVisible='SI'
                          AND $whereEjercicio;",true);

    while($datos=mysql_fetch_assoc($consulta)){
        echo "
            <tr>
                <td> Criterio de aptitud </td>";

        if($_SESSION['tipoUsuario']!='CLIENTE' && !$seccionCliente){
            echo "
                <td>".$datos['EMPID']." - ".$datos['EMPNOMBRE']."</td>
                <td>".$datos['usuario']."</td>";
        }
        else{
            echo "
                <td>".$datos['empleado']."</td>";
        }
        
        echo "  <td>".formateaFechaWeb($datos['fecha'])."</td>
                <td class='centro'>
                    <a href='../reconocimientos-medicos/generaApto.php?encp=".$datos['codigo']."' class='btn btn-propio noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar</i></a>
                </td>
            </tr>";
    }
}


function imprimeDocumentacionClientesDOC($codigoUsuario,$ejercicio,$tipo,$seccionCliente,$vs=false){
    $esCliente=false;
    $where='';
    if($codigoUsuario){
        $cliente=consultaBD('SELECT codigoCliente FROM usuarios_clientes WHERE codigoUsuario='.$codigoUsuario,true,true);
        
        if($_SESSION['tipoUsuario']=='CLIENTE' || $seccionCliente){
            $where="AND visible = 'SI' AND codigoCliente=".$cliente['codigoCliente'];
            $esCliente=true;
        }
        else{
            $where="AND codigoCliente=".$cliente['codigoCliente'];
        }
    }
    $tipos=array('administracion'=>'ADMON','tecnico'=>'TECNICO','vs'=>'VS');
    $tipo=$tipos[$tipo];

    $whereEjercicio=compruebaEjercicioParaWhere("YEAR(d.fecha)",$ejercicio);


    $consulta=consultaBD("SELECT d.fecha, d.nombre, c.EMPMARCA, d.fichero, EMPID, EMPNOMBRE, IFNULL(CONCAT(usuarios.nombre,' ',usuarios.apellidos),'-') AS usuario
                          FROM documentos_clientes d INNER JOIN clientes c ON d.codigoCliente=c.codigo
                          LEFT JOIN usuarios ON d.codigoUsuarioSubeDocumento=usuarios.codigo
                          WHERE $whereEjercicio AND pertenece='".$tipo."' ".$where,true);

    while($datos=mysql_fetch_assoc($consulta)){
        $nombre=$datos['nombre'];
        if(!$esCliente){
            $nombre.=" (".$datos['EMPMARCA'].")";
        }

        echo "
            <tr class='tr-success'>
                <td>".$nombre."</td>";

        if($_SESSION['tipoUsuario']!='CLIENTE' && !$seccionCliente){
            echo "
                <td>".$datos['EMPID']." - ".$datos['EMPNOMBRE']."</td>
                <td>".$datos['usuario']."</td>";
        }
        if($vs){
            echo "<td></td>";
        }
        
        echo "  <td>".formateaFechaWeb($datos['fecha'])."</td>
                <td class='centro'>
                    <a target='_blank' class='btn btn-propio noAjax' href='../documentos/planificacion/".$datos['fichero']."'><i class='icon-download'></i> Descargar</i></a>
                </td>
            </tr>";
    }
}

function imprimePlanPrevDOC($codigoUsuario,$ejercicio,$seccionCliente){
    //$campos=array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaVs','fechaMail');//Comentado hasta ver que hacer con fechaVs
    $campos=array('f.fecha','fechaAprobada','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','f.fecha','fechaMail');
    $where='';

    if($ejercicio!='Todos'){
        foreach ($campos as $key => $campo) {
            if($where!=''){
                $where.=' OR ';
            }
            $where.="YEAR(".$campo.")='$ejercicio'";
        }
        if($where!=''){
            $where.=' OR ';
        }
        $where.="YEAR(planificacion_visitas.fechaPrevista)='$ejercicio'";
    }
    else{
        $where='1=1';
    }

    $where2='';
    if($codigoUsuario){
        $cliente=consultaBD('SELECT codigoCliente FROM usuarios_clientes WHERE codigoUsuario='.$codigoUsuario,true,true);
        $where2.=" AND checkVisiblePlanPrev  = 'SI' AND codigoCliente=".$cliente['codigoCliente'];
    } else if($_SESSION['tipoUsuario']!='ADMIN'){
        $where2.=' AND codigoUsuarioTecnico='.$_SESSION['codigoU'];
    }
    conexionBD();
    $consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, EMPID, EMPNOMBRE, IFNULL(CONCAT(usuarios.nombre,' ',usuarios.apellidos),'-') AS usuario
                          FROM formularioPRL f LEFT JOIN contratos ON f.codigoContrato=contratos.codigo 
                          LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo 
                          LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
                          LEFT JOIN usuarios ON f.codigoUsuarioTecnico=usuarios.codigo
                          LEFT JOIN planificacion_visitas ON f.codigo=planificacion_visitas.codigoFormulario
                          WHERE f.eliminado='NO' AND f.aprobado='SI' AND f.formulario!='' AND (".$where.") ".$where2.";");

    while($datos=mysql_fetch_assoc($consulta)){
        $nombre='Plan de prevención';

        echo "
            <tr>
                <td>".$nombre."</td>";

        if($_SESSION['tipoUsuario']!='CLIENTE' && !$seccionCliente){
            echo "
                <td>".$datos['EMPID']." - ".$datos['EMPNOMBRE']."</td>
                <td>".$datos['usuario']."</td>";
        }
        
        echo "  <td>".formateaFechaWeb($datos['fecha'])."</td>
                <td class='centro'>
                    <a target='_blank' class='btn btn-propio noAjax' href='../planificacion/generaPlanPrevencion.php?codigo=".$datos['codigo']."'><i class='icon-download'></i> Descargar</i></a>
                </td>
            </tr>";
    }
}

function imprimeEvaluacionRiesgosDOC($codigoUsuario,$ejercicio,$seccionCliente){
    global $_CONFIG;

    $esCliente=false;
    $where='';
    if($codigoUsuario){
        $cliente=consultaBD('SELECT codigoCliente FROM usuarios_clientes WHERE codigoUsuario='.$codigoUsuario,false,true);
        $where="AND checkVisible = 'SI' AND codigoCliente=".$cliente['codigoCliente'];
        $esCliente=true;
    }

    $whereEjercicio=compruebaEjercicioParaWhere("YEAR(e.fechaEvaluacion)",$ejercicio);

    $consulta=consultaBD("SELECT i.codigo, e.fechaEvaluacion AS fecha, c.EMPMARCA, EMPID, EMPNOMBRE, IFNULL(CONCAT(usuarios.nombre,' ',usuarios.apellidos),'-') AS usuario
                          FROM evaluacion_general e INNER JOIN clientes c ON e.codigoCliente=c.codigo
                          INNER JOIN informes i ON e.codigo=i.codigoEvaluacion
                          LEFT JOIN usuarios ON i.codigoUsuarioInforme=usuarios.codigo
                          WHERE $whereEjercicio ".$where.' GROUP BY c.codigo');
    while($datos=mysql_fetch_assoc($consulta)){
        $nombre='Evaluación de riesgos';
        if(!$esCliente){
            $nombre.=" (".$datos['EMPMARCA'].")";
        }
        echo "
            <tr>
                <td>".$nombre."</td>";

        if($_SESSION['tipoUsuario']!='CLIENTE' && !$seccionCliente){
            echo "
                <td>".$datos['EMPID']." - ".$datos['EMPNOMBRE']."</td>
                <td>".$datos['usuario']."</td>";
        }
        
        echo "  <td>".formateaFechaWeb($datos['fecha'])."</td>
                <td class='centro'>
                    <a target='_blank' href='".$_CONFIG['raiz']."generacion-de-informes/generaPDF.php?codigo=".$datos['codigo']."' class='btn btn-propio noAjax'><i class='icon-download'></i> Descargar</i></a>
                </td>
            </tr>";
    }
}

function compruebaEjercicioParaWhere($campo,$ejercicio){
    $res='1=1';

    if($ejercicio!='Todos'){
        $res=$campo.'='.$ejercicio;
    }

    return $res;
}

function imprimeDocumentosVSDOC($codigoUsuario,$seccionCliente){
    global $_CONFIG;
    $tipos=array(1=>'Informe VS',4=>'Comunicación al trabajador para la realización de la VS',5=>'Programación reconocimientos médicos',8=>'Carta aptitud',12=>'Protección maternidad');

    $consulta=consultaBD("SELECT documentosVS.* 
                          FROM documentosVS INNER JOIN contratos ON documentosVS.codigoContrato=contratos.codigo 
                          INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo 
                          INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
                          INNER JOIN usuarios_clientes ON clientes.codigo=usuarios_clientes.codigoCliente
                          WHERE usuarios_clientes.codigoUsuario='$codigoUsuario'
                          GROUP BY documentosVS.codigo;",true);
    
    while($datos=mysql_fetch_assoc($consulta)){
        echo "
            <tr>
                <td>".$tipos[$datos['tipoDocumento']]."</td>
                <td>Todos</td>
                <td class='centro'>".formateaFechaWeb($datos['fechaCreacion'])."</td>
                <td class='centro'>
                    <a target='_blank' class='btn btn-propio noAjax' href='".$_CONFIG['raiz']."documentos-vs/generaDocumento.php?tipo=".$datos['tipoDocumento']."&codigo=".$datos['codigo']."'><i class='icon-cloud-download'></i> Descargar</i></a>
                </td>

            </tr>";
    }
}

function tablaContratosEnVigorClientes($codigoCliente){
    $iconoRevisado=array('SI'=>'<i class="icon-check-circle iconoFactura icon-success" title="Revisado"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger" title="Pendiente"></i>');
    echo '
        <div class="span10">           
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Contratos en vigor</h3>
          </div>
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Nº Contrato </th>
                  <th> Inicio </th>
                  <th> Fin </th> 
                  <th> Servicio </th>             
                  <th> Técnico </th>
                  <th> Importe </th>
                  <th> Revisado </th>
                  <th class="centro"></th>
                </tr>
              </thead>
              <tbody>';
              $where='WHERE contratos.eliminado="NO" AND enVigor="SI" AND facturas.facturaCobrada="SI"';
              $consulta=consultaBD("SELECT (ofertas.total-ofertas.importe_iva) AS importe, clientes.EMPNOMBRE,clientes.EMPMARCA,clientes.codigo AS codigoCliente,contratos.*, MAX(contratos_renovaciones.fecha) AS renovacion, 
                              contratos.tecnico, ofertas.opcion, IFNULL(facturas.facturaCobrada,'NO') AS facturaCobrada, clientes.EMPID, formularioPRL.codigoUsuarioTecnico, contratoRevisado, contratos.fichero, suspendido, incrementoIPC, incrementoIpcAplicado,
                              emitirPlanificacionSinPagar
                              FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
                              INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
                              LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
                              LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
                              LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO' AND facturas.eliminado='NO'
                              LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
                              ".$where."
                              AND ofertas.codigoCliente=".$codigoCliente."
                              AND (contratos_en_facturas.codigo IS NULL OR facturas.eliminado='NO')
                              GROUP BY contratos.codigo 
                              ORDER BY contratos.fechaInicio;",true);
                while($datos=mysql_fetch_assoc($consulta)){
                    $cliente=datosRegistro('clientes',$datos['codigoCliente']);
                    if($datos['tecnico']!=NULL){
                        $tecnico=datosRegistro('usuarios',$datos['tecnico']);
                    } else {
                        $tecnico=datosRegistro('usuarios',$datos['codigoUsuarioTecnico']);
                    }
                    $servicio=obtieneNombreServicioContratado($datos['opcion']);
                    echo "
                    <tr>
                        <td class='nowrap'>".formateaReferenciaContrato($cliente,$datos)."</td>
                        <td>".formateaFechaWeb($datos['fechaInicio'])."</td>
                        <td>".formateaFechaWeb($datos['fechaFin'])."</td>
                        <td>".$servicio."</td>
                        <td>".$tecnico['nombre']." ".$tecnico['apellidos']."</td>
                        <td class='nowrap'>".compruebaImporteContrato($datos['importe'],$datos)." €</td>
                        <td class='centro'>".$iconoRevisado[$datos['contratoRevisado']]."</td>
                        <td>
                            ".crearBotonContratos($datos,$cliente['EMPCIF'],$eliminado)."
                        </td>
                    </tr>";
                }
    echo '          
                </tbody>
            </table>
          </div>
        </div>
      </div>';

}

function crearBotonContratos($datos,$cif,$eliminado){
    global $_CONFIG;
    
    $res="  <div class='btn-group centro'>
                <button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
                <ul class='dropdown-menu' role='menu'>";

    if($datos['enVigor']=='SI' && $datos['facturaCobrada']=='NO' && $eliminado=='NO'){
        $res.="     <li><a href='".$_CONFIG['raiz']."contratos/gestion.php?codigo=".$datos['codigo']."&editable=CREADO'><i class='icon-search-plus'></i> Detalles</i></a></li>
                    <li class='divider'></li>
                    <li><a href='".$_CONFIG['raiz']."contratos/gestion.php?codigoContrato=".$datos['codigo']."'><i class='icon-edit'></i> Crear modificación</i></a></li>";
    }
    elseif($datos['enVigor']=='SI' && $datos['facturaCobrada']=='SI' && $eliminado=='NO'){
        $res.="     <li><a href='".$_CONFIG['raiz']."contratos/gestion.php?codigo=".$datos['codigo']."&editable=NO'><i class='icon-search-plus'></i> Detalles</i></a></li>";
    }
    elseif($datos['enVigor']=='NO' && $eliminado=='NO'){
        $res.="     <li><a href='".$_CONFIG['raiz']."contratos/gestion.php?codigo=".$datos['codigo']."&editable=HISTORICO'><i class='icon-search-plus'></i> Detalles</i></a></li>";
    }
    else{
        $res.="     <li><a href='".$_CONFIG['raiz']."contratos/gestion.php?codigo=".$datos['codigo']."&editable=ELIMINADO'><i class='icon-search-plus'></i> Detalles</i></a></li>";
    }

    if($datos['opcion']==4){
        if($datos['fichero']!='' && $datos['fichero']!='NO'){
            $res.=" <li class='divider'></li>
                    <li><a target='_blank' href='../documentos/contratos/".$datos['fichero']."' class='noAjax'><i class='icon-cloud-download'></i> Descargar contrato</i></a></li>";
        }
    } 
    elseif(file_exists('../documentos/contratos/'.$datos['codigo'].'.pdf')){
        $res.="     <li class='divider'></li>
                    <li><a class='noAjax' target='_blank' cif='$cif' href='../documentos/contratos/".$datos['codigo'].".pdf'><i class='icon-cloud-download'></i> Descargar contrato</i></a></li>";
    }
    else{
        $res.="     <li class='divider'></li>
                    <li><a class='noAjax' target='_blank' cif='$cif' href='generaDocumento.php?codigoContrato=".$datos['codigo']."'><i class='icon-cloud-download'></i> Descargar contrato</i></a></li>";
    }

    $res.="     <li class='divider'></li>
                <li><a class='noAjax' href='generaCertificadoCorrientePago.php?codigoContrato=".$datos['codigo']."'><i class='icon-cloud-download'></i> Descargar certificado pago</i></a></li>
                <li class='divider'></li>
                <li><a class='noAjax enviaCertificado' href='generaCertificadoCorrientePago.php?codigoContrato=".$datos['codigo']."&envio'><i class='icon-send'></i> Enviar certificado pago</i></a></li>";

    
    $res.="     </ul>
            </div>";


    return $res;
}

function empleadosFormadosClientes($codigoCliente){
    global $_CONFIG;
    echo '<div class="span10">          
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Empleados formados</h3>
          </div>
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable" >
              <thead>
                <tr>
                  <th> Nº Empleado</th>
                  <th> Nombre </th>
                  <th> Apellidos </th>
                  <th> DNI </th>
                  <th> Sexo </th>
                  <th> Teléfono </th>
                  <th> eMail </th>
                  <th> Formado </th>
                  <th class="centro"></th>
                </tr>
              </thead>
              <tbody>';

            $consulta=consultaBD("SELECT empleados.*, usuarioFormacion 
                FROM empleados 
                LEFT JOIN usuarios_empleados ON empleados.codigo=usuarios_empleados.codigoEmpleado
                INNER JOIN usuarios_sincronizados ON usuarios_empleados.codigoUsuario=usuarios_sincronizados.usuarioPRL WHERE codigoCliente = ".$codigoCliente." ORDER BY codigoInterno;",true);
            $iconoRevisado=array('SI'=>'','NO'=>'<i class="icon-times-circle iconoFactura icon-danger" title="No aprobado"></i>','PENDIENTE'=>'<i class="icon-question-circle iconoFactura icon-warning-color" title="Pendiente de aprobar"></i>');
            while($datos=mysql_fetch_assoc($consulta)){
                if($datos['eliminado']=='SI'){
                    $iconoRevisado['NO']='<i class="icon-times-circle iconoFactura icon-danger" title="No aprobada su eliminación"></i>';
                    $iconoRevisado['PENDIENTE']='<i class="icon-question-circle iconoFactura icon-warning-color" title="Pendiente de eliminar"></i>';
                } else {
                    $iconoRevisado['NO']='<i class="icon-times-circle iconoFactura icon-danger" title="No aprobado"></i>';
                    $iconoRevisado['PENDIENTE']='<i class="icon-question-circle iconoFactura icon-warning-color" title="Pendiente de aprobar"></i>';
                }
                $formacion=estaEnFormacion($datos['codigo']);
                $formado='';
                $mostrar=true;
                if($formacion){
                    conexionBDFormacion();
                        $certificado=consultaBD('SELECT * FROM puestos_empleados WHERE codigoEmpleado='.$formacion['usuarioFormacion'].' ORDER BY codigo DESC LIMIT 1',false,true);
                        if($certificado['fechaCertificado']!='0000-00-00'){
                            $fecha=explode('-',$certificado['fechaCertificado']);
                            $formado=certificadoCaducado($certificado['fechaCertificado']);
                        }
                    cierraBD();
                    if($formado==''){
                        $formado='<i class="icon-check-circle iconoFactura icon-success" title="Formado"></i>';
                    }
                } else {
                    $formado='<i class="icon-times-circle iconoFactura icon-danger" title="No formado"></i>';
                }
                echo "
                <tr>
                    <td class='centro'>".$datos['codigoInterno']." ".$iconoRevisado[$datos['aprobado']]."</td>
                    <td>".$datos['nombre']."</td>
                    <td>".$datos['apellidos']."</td>
                    <td>".$datos['dni']."</td>
                    <td>".$datos['sexoEmpleado']."</td>
                    <td><a href='tel:".$datos['telefono']."'>".formateaTelefono($datos['telefono'])."</a></td>
                    <td><a href='mailto:".$datos['email']."'>".$datos['email']."</a></td>
                    <th class='centro'>".$formado."</th>
                    <td class='centro'>
                        <div class='btn-group centro'>
                            <button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span> Acciones</button>
                            <ul class='dropdown-menu' role='menu'>
                                <li><a href='".$_CONFIG['raiz']."empleados/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> ";
                                conexionBDFormacion();
                                    $empleado=consultaBD('SELECT codigo FROM puestos_empleados WHERE firma!="" AND codigoEmpleado='.$datos['usuarioFormacion'],false,true);
                                    if($empleado){
                                        echo "<li class='divider'></li>";
                                        echo "<li><a class='noAjax' target='_blank' href='".$_CONFIG['raiz']."empleados/generaCertificado.php?empleado=".$empleado['codigo']."'><i class='icon-download'></i> Certificado</i></a></li>";
                                    }
                                cierraBD();
                echo "
                            </ul>
                        </div>
                    </td>
                </tr>";
            }

    echo '          
              </tbody>
            </table>
          </div>
        </div>
      </div>';
}

function certificadoCaducado($fecha){
    $icono='';
    $fecha=date("Y-m-d",strtotime($fecha."+ 1 year"));
    if(comparaFechas(date('Y-m-d'),$fecha)==-1){
        $icono="<i title='Certificado caducado' class='icon-exclamation-circle plazoSuperado iconoFactura icon-danger iconPendiente'></i>";
    }
    return $icono;
}

function listadoAptosClientes($codigoCliente){
    global $_CONFIG;
    echo '<div class="span10">            
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
                <h3>Reconocimientos médicos aptos registrados</h3>
            </div>
            <div class="widget-content">
            <table class="table table-striped table-bordered datatable" id="tablaReconocimientos">
              <thead>
                <tr>
                  <th> Fecha </th>
                  <th> Trabajador </th>
                  <th> DNI </th>
                  <th> V. apto </th>
                  <th> V. reco. </th>
                  <th> Cerrado </th>
                  <th> Facturado </th>
                  <th class="centro"></th>
                </tr>
              </thead>
              <tbody>';

            $query="SELECT reconocimientos_medicos.codigo, reconocimientos_medicos.fecha, CONCAT(clientes.EMPNOMBRE,' (',clientes.EMPMARCA,')') AS empresa, CONCAT(empleados.apellidos,', ',empleados.nombre) AS empleado,
            empleados.dni, apto, checkAudiometria, checkOtrasPruebasFuncionales, checkEspirometria, checkElectrocardiograma, advertencia,
            cerrado, reconocimientos_medicos.codigoUsuario, reconocimientos_medicos.editable, reconocimientos_medicos.eliminado, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS usuario,
            mesSiguienteReconocimiento, anioSiguienteReconocimiento, clientes.EMPID, codigoProfesionalExterno, reconocimientos_medicos.numero, facturas_reconocimientos_medicos.codigo AS codigoFacturaReconocimientoMedico, aptoVisible, reconocimientoVisible,
            bloqueado, empleados.codigoCliente

            FROM reconocimientos_medicos LEFT JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
            LEFT JOIN clientes ON empleados.codigoCliente=clientes.codigo 
            LEFT JOIN usuarios ON reconocimientos_medicos.codigoUsuario=usuarios.codigo
            LEFT JOIN reconocimientos_medicos_en_facturas ON reconocimientos_medicos.codigo=reconocimientos_medicos_en_facturas.codigoReconocimientoMedico
            LEFT JOIN facturas_reconocimientos_medicos ON reconocimientos_medicos_en_facturas.codigoFacturaReconocimientoMedico=facturas_reconocimientos_medicos.codigo AND reconocimientos_medicos.codigoContrato=facturas_reconocimientos_medicos.codigoContrato AND facturas_reconocimientos_medicos.eliminado='NO'

            GROUP BY reconocimientos_medicos.codigo
            HAVING reconocimientos_medicos.eliminado='NO' AND empleados.codigoCliente=".$codigoCliente;
            $iconoBloqueado=array('SI'=>'<i class="icon-check-circle iconoFactura icon-success" title="Bloqueado"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger" title="Sin bloquear"></i>',''=>'<i class="icon-times-circle iconoFactura icon-danger" title="Sin bloquear"></i>');
            $consulta=consultaBD($query,true);
            while($datos=mysql_fetch_assoc($consulta)){
                $apto=compruebaAptoReconocimientoMedico($datos['apto']);

                $advertencia='';
                if($datos['advertencia']=='SI'){
                    $advertencia='<i class="icon-exclamation-circle icon-danger" title="Pendiente"></i>';
                }

                $facturado='NO';
                if($datos['codigoFacturaReconocimientoMedico']!=NULL){
                    $facturado='SI';
                }
                if($apto=='APTO'){
                    echo '<tr>
                        <td>'.formateaFechaWeb($datos['fecha']).'</td>
                        <td>'.$datos['empleado'].' '.$advertencia.'</td>
                        <td>'.$datos['dni'].'</td>
                        <td class="centro">'.$datos['aptoVisible'].'</td>
                        <td class="centro">'.$datos['reconocimientoVisible'].'</td>
                        <td class="centro">'.$iconoBloqueado[$datos['bloqueado']].'</td>
                        <td class="centro">'.$facturado.'</td>
                        '.botonAccionesReconocimientoClientes($datos,null,$datos['codigoUsuario'],$datos['usuario']).'
                    </tr>';
                }
            }
    echo '              
              </tbody>
            </table>
            </div>
          </div>
        </div>';
}

function compruebaAptoReconocimientoMedico($apto){
    $res='Sin criterio';

    if($apto!=''){

        $res=str_replace('.','',$apto);

        if(substr_count($apto,':')>0){
            $res=substr($res,0,strpos($res,':'));
        }
    
    }

    return $res;
}

function botonAccionesReconocimientoClientes($datos,$codigoUsuario=null,$codigoUsuarioRM,$nombreUsuarioRM){
    $nombres=array();
    $direcciones=array();
    $iconos=array();
    $aperturaEnlaces=array();

    if($datos['editable']=='SI'){//Si el reconocimiento está editable se ven los detalles*
        array_push($nombres,'Detalles');
        array_push($direcciones,"reconocimientos-medicos/gestion.php?codigo=".$datos['codigo']);
        array_push($iconos,'icon-search-plus');
        array_push($aperturaEnlaces,0);
    }
    else{
        array_push($nombres,'En edición por '.$datos['usuario']);
        array_push($direcciones,"#' onclick='return false;");
        array_push($iconos,'icon-ban');
        array_push($aperturaEnlaces,2);
    }

    //if($datos['cerrado']=='SI' && $codigoUsuario==$codigoUsuarioRM){
        /*$res=botonAcciones(array('Detalles','Descargar Reconocimiento','Descargar Apto'),array("reconocimientos-medicos/gestion.php?codigo=".$datos['codigo'],"reconocimientos-medicos/generaReconocimiento.php?encp=".md5($datos['codigo']),"reconocimientos-medicos/generaApto.php?encp=".md5($datos['codigo'])),array('icon-search-plus','icon-cloud-download','icon-cloud-download'),array(0,1,1),true,'');*/

        array_push($nombres,'Descargar Apto');
        array_push($direcciones,"reconocimientos-medicos/generaApto.php?encp=".md5($datos['codigo']));
        array_push($iconos,'icon-cloud-download');
        array_push($aperturaEnlaces,1);
    //}

    if(empty($nombres)){
        $res="<td class='centro'><div class='centro'><div class='btn-group'><button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown' disabled='disabled' title='Bloqueado. Solo disponible para ".$nombreUsuarioRM."'><i class='icon-cogs'></i> <span class='caret'></span></button></div></div></td>";
    }
    else{
        $res=botonAcciones($nombres,$direcciones,$iconos,$aperturaEnlaces,true,'');
    }

    return $res;
}

function campoFicheroEliminar($nombreCampo,$i,$texto,$tipo=0,$datos=false,$ruta=false,$nombreDescarga=''){
    $valor=compruebaValorCampo($datos,$nombreCampo);
    $nombreCampo.=$i;
    if(!$valor || $valor=='NO'){
        if($tipo==0){
            echo "
            <div class='control-group'>                     
                <label class='control-label' for='$nombreCampo'>$texto:</label>
                <div class='controls'>
                    <input type='file' name='$nombreCampo' id='$nombreCampo' />
                </div> <!-- /controls -->       
            </div> <!-- /control-group -->";
        }
        elseif($tipo==1){//MODIFICACIÓN 25/09/2015: sustituido else por elseif para que haya una tercera opción
            echo "<td><input type='file' name='$nombreCampo' id='$nombreCampo' /></td>";
        }
        else{
            echo "<input type='file' name='$nombreCampo' id='$nombreCampo' />";
        }
    }
    else{
        campoDescargaEliminar($nombreCampo,$texto,$ruta,$valor,$tipo,$nombreDescarga,$datos);
        campoOculto($valor,$nombreCampo);
    }
}

function campoDescargaEliminar($nombreCampo,$texto,$ruta,$valor,$tipo=0,$nombreDescarga='',$datos=false){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    
    if($nombreDescarga==''){
        $nombreDescarga=$valor;
    }

   
    if($valor=='NO' || $valor==''){
        campoFichero($nombreCampo,$texto,$tipo,$valor,$ruta);
    }
    elseif($tipo==0){
        echo "
            <div class='control-group ficheroNum".$datos['codigo']."'>                     
              <label class='control-label'>$texto:</label>
              <div class='controls'>
                <a class='btn btn-propio noAjax' href='$ruta$valor' target='_blank'><i class='icon-cloud-download'></i> $nombreDescarga</a>
                <a style='cursor:pointer' codigo='".$datos['codigo']."' idAsociado='$nombreCampo' class='eliminaFichero noAjax'><i style='font-size:18px;'class='icon-trash'></i></a>
              </div> <!-- /controls -->       
            </div> <!-- /control-group -->";
    }
    elseif($tipo==1){
        echo "<td><div class='ficheroNum".$datos['codigo']."'><a class='btn btn-propio descargaFichero noAjax' href='$ruta$valor' target='_blank' nombre='$nombreCampo'><i class='icon-cloud-download'></i> $nombreDescarga</a> <a style='cursor:pointer' codigo='".$datos['codigo']."' idAsociado='$nombreCampo' class='eliminaFichero noAjax'><i style='font-size:18px;'class='icon-trash'></i></a></div></td>";
    }
    else{
        echo "<a class='btn btn-propio noAjax' href='$ruta$valor' target='_blank'><i class='icon-cloud-download'></i> $nombreDescarga</a>";
    }

}

function obtenerGestionCertificado($empleado,$codigoCurso,$tipo){
    $res=array(0);
    if($tipo=='CURSO'){
        $item=consultaBD('SELECT gestion FROM certificados_gestion WHERE codigoEmpleado='.$empleado.' AND codigoCurso='.$codigoCurso,false,true);
    } else {
        $item=consultaBD('SELECT gestion FROM certificados_gestion WHERE codigoEmpleado='.$empleado.' AND codigoCurso IS NULL',false,true);
    }
    if($item){
        $gestion=explode(';',$item['gestion']);
        foreach ($gestion as $key => $value) {
            if($value!=''){
                array_push($res,$value);
            }
        }
    } else {
        $res=false;
    }
    return $res;
}


function listadoFormacionInformacionClientes($codigoCliente) {
	global $_CONFIG;
    $tabla= '
    <div class="span10">           
    <div class="widget widget-table action-table">
      <div class="widget-header"> <i class="icon-list"></i>
        <h3>Información de formación</h3>
      </div>
      <div class="widget-content">
        <table class="table table-striped table-bordered datatable">
          <thead>
            <tr>
                <th> Fecha </th>
                <th> Cliente </th>
                <th> Alumno </th>
                <th> Puesto </th>
                <th> Técnico </th>
                <th> Curso </th>
                <th class="centro"></th>
            </tr>
          </thead>
          <tbody>';


    $consulta=consultaBD("SELECT c.codigo, c.fecha, cl.EMPNOMBRE, cl.EMPMARCA, CONCAT(e.nombre,' ',e.apellidos) AS empleado, p.nombre AS puesto, CONCAT(u.nombre,' ',u.apellidos) AS tecnico, cursos.nombre AS curso FROM certificados c LEFT JOIN clientes cl ON c.codigoCliente=cl.codigo LEFT JOIN empleados e ON c.codigoEmpleado=e.codigo LEFT JOIN puestos_trabajo p ON c.codigoPuesto=p.codigo LEFT JOIN usuarios u ON c.codigoTecnico=u.codigo LEFT JOIN cursos ON c.codigoCurso=cursos.codigo
    WHERE cl.codigo=".$codigoCliente,true);
          while($datos=mysql_fetch_assoc($consulta)){
              $nombre='';
              if($datos['EMPNOMBRE']!=''){
                  $nombre=$datos['EMPNOMBRE'];
              }
              if($datos['EMPMARCA']!=''){
                  if($nombre==''){
                      $nombre=$datos['EMPMARCA'];
                  } else {
                      if($nombre!=$datos['EMPMARCA']){
                          $nombre.=' / '.$datos['EMPMARCA'];
                      }
                  }
              }
              $boton="<li><a target='_blank' class='noAjax' href='".$_CONFIG['raiz']."certificados/generaCertificado.php?codigo=".$datos['codigo']."'><i class='icon-cloud-download'></i> Descargar</i></a></li>";
              if($datos['curso']==''){
                  $boton="<li><a class='noAjax' onClick='alert(\"Este certificado no tiene seleccionado ningún curso\");' href='javascript:void(0);'><i class='icon-cloud-download'></i> Descargar</i></a></li>";
              }
              $tabla.= "<tr>
                      <td> ".formateaFechaWeb($datos['fecha'])." </td>
                      <td> ".$nombre." </td>
                      <td> ".$datos['empleado']." </td>
                      <td> ".$datos['puesto']." </td>
                      <td> ".$datos['tecnico']." </td>
                      <td> ".$datos['curso']." </td>
                      <td>
                          <div class='btn-group centro'>
                              <button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
                              <ul class='dropdown-menu' role='menu'>
                                  <li><a href='".$_CONFIG['raiz']."certificados/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li>
                                  <li class='divider'></li>
                                  ".$boton."
                              </ul>
                          </div>
                      </td>
                   </tr>";
          }


    /*
	$tipos=array(1=>'SPA 4 Especialidades',2=>'SPA Vigilancia de la salud',3=>'SPA Especialides técnicas',4=>'Otras actuaciones');
    $where='WHERE ((enVigor="SI" AND contratos.eliminado="NO") OR (enVigor="NO" AND contratos.fechaInicio>CURDATE()) OR codigoContrato IS NULL)';//Planificaciones con contratos en vigor, futuribles o sin contrato
	$whereEjercicio=defineWhereEjercicio('',array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaMail'));
	$where.=str_replace('WHERE',' AND (',$whereEjercicio).' OR (visitada="NO" OR planPrev="NO" OR pap="NO" OR info="NO" OR memoria="NO" OR vs="NO")';

	if($_SESSION['ejercicio']!='' && $_SESSION['ejercicio']!='Todos'){
		$where.='AND YEAR(f.fecha)<='.$_SESSION['ejercicio'].')';
	}
	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FACTURACION'){
		$where.=' AND codigoUsuarioTecnico='.$_SESSION['codigoU'];
	}
	conexionBD();
	$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, visitada, planPrev, pap, info, f.horasPrevistas, memoria, fechaVisita, fechaPlanPrev, fechaPap, fechaInfo,  
						  fechaMemoria, vs, mesVS, anioVS, codigoUsuarioTecnico, CONCAT(EMPNOMBRE,' / ',EMPMARCA) AS cliente,f.codigoContrato, ofertas.codigoCliente, formulario, opcion, 
						  suspendido, (ofertas.total-ofertas.importe_iva) AS importe, incrementoIPC, incrementoIpcAplicado
						  FROM formularioPRL f 
						  LEFT JOIN contratos ON f.codigoContrato=contratos.codigo 
						  LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo 
						  LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
						  ".$where." AND f.eliminado='NO' AND clientes.codigo=".$codigoCliente." AND f.aprobado='SI' AND f.visible='SI';");




	$iconoValidado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		
        $suspendido=$datos['suspendido']=='SI'?'<i class="icon-exclamation iconoFactura icon-danger animated infinite flash"></i> ':'';
		$referencia='';
		
		if($datos['codigoContrato']!=NULL){
			$contrato=consultaBD('SELECT * FROM contratos WHERE codigo="'.$datos['codigoContrato'].'";',false,true);
			$cliente=consultaBD('SELECT * FROM clientes WHERE codigo="'.$datos['codigoCliente'].'";',false,true);
			$referencia=formateaReferenciaContrato($cliente,$contrato);
		}
		
		$visitada=consultaBD('SELECT * FROM planificacion_visitas WHERE codigoFormulario='.$datos['codigo'],false,true);
		
		if($visitada){
			if($visitada['fechaReal']=='' || $visitada['fechaReal']=='0000-00-00'){
				$datos['visitada']='NO';
			} else{
				$datos['visitada']='SI';
			}
		}
        
		$tabla.= "
			<tr>
				<td>".$referencia."</td>
				<td>".$tipos[$datos['opcion']]."</td>
				<td class='centro'>".$iconoValidado[$datos['visitada']];
				if($datos['visitada'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaVisita']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['planPrev']];
				if($datos['planPrev'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaPlanPrev']);
                }
                $tabla.="</td>		
				<td class='centro'>".$iconoValidado[$datos['pap']];
				if($datos['pap'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaPap']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['info']];
				if($datos['info'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaInfo']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['memoria']];
				if($datos['memoria'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaMemoria']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['vs']];
				if($datos['vs'] == 'NO'){
                    if($datos['mesVS']!='00' && $datos['anioVS']!='00'){
                    	$tabla.="<br/>".obtieneFechaLimite($datos['anioVS'].'-'.$datos['mesVS'].'-01');
                	}
                }
                $tabla.="</td>	
				<td class='centro'>".$datos['horasPrevistas'];
                $tabla.="</td>	
                <td> ".compruebaImporteContrato($datos['importe'],$datos)." € </td>				 
				<td class='centro'>
					<div class='btn-group'>
					<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
					<ul class='dropdown-menu' role='menu'>
						<li><a href='".$_CONFIG['raiz']."planificacion/gestion.php?codigo=".$datos['codigo']."' ><i class='icon-search-plus'></i> Detalles</a></li>
						<li class='divider'></li>
						<li><a href='".$_CONFIG['raiz']."planificacion/generaMemoriaExcel.php?codigo=".$datos['codigo']."' class='noAjax' ><i class='icon-cloud-download'></i> Memoria</a></li>
					</ul>
				</td>
			</tr>";
	}
	cierraBD();
   */

        $tabla.= '              
        </tbody>
      </table>
        </div>
    </div>
    </div>';

	echo $tabla;
}

function obtieneFechaLimite($fecha){
	$fechaFinal = formateaFechaWeb($fecha);
	$fecha = new DateTime($fecha);
	$fecha->sub(new DateInterval('P7D'));
	$fechaWarning = $fecha->format('d/m/Y');
	$label = 'success';
	if(compruebaFechaRevision($fechaFinal)){
		$label = 'danger';
	} else if(compruebaFechaRevision($fechaWarning)){
		$label = 'warning';
	}
	return "<label class='label label-".$label."'>".$fechaFinal."</label>";
}

function compruebaFechaRevision($fechaRevision){
	$res=false;
	if($fechaRevision != ''){
		$fecha=explode('/',$fechaRevision);

		if($fecha[2]<date('Y')){
			$res=true;
		}
		elseif($fecha[2]==date('Y') && $fecha[1]<date('m')){
			$res=true;
		}
		elseif($fecha[2]==date('Y') && $fecha[1]==date('m') && $fecha[0]<date('d')){
			$res=true;
		}
	}

	return $res;
}
