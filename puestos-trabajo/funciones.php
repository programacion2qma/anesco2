<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión de puestos


function operacionesPuestos(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaPuesto();
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaPuesto();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaEmpleado('puestos_trabajo');
	}

	mensajeResultado('nombre',$res,'Puestos');
    mensajeResultado('elimina',$res,'Puestos', true);
}

function insertaPuesto(){
	$res=true;
	$res=insertaDatos('puestos_trabajo');
	$res=insertaEpis(mysql_insert_id());
	return $res;
}

function actualizaPuesto(){
	$res=true;
	$res=actualizaDatos('puestos_trabajo');
	$res=insertaEpis($_POST['codigo']);
	return $res;
}

function insertaEpis($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM epis_de_puesto_trabajo WHERE codigoPuestoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['codigoEpi'.$i])){
		if($datos['codigoEpi'.$i]!='' && $datos['codigoEpi'.$i] != 'NULL')
		$res = $res && consultaBD("INSERT INTO epis_de_puesto_trabajo VALUES (NULL,'".$codigo."','".$datos['codigoEpi'.$i]."');");
		$i++;
	}

	return $res;
}



function gestionPuesto(){
	operacionesPuestos();
	
	abreVentanaGestionConBotones('Gestión de Puestos de Trabajo','index.php','span3');
	$datos=compruebaDatos('puestos_trabajo');
	
	campoTexto('nombre','Nombre',$datos,'span6');
	areaTexto('tareas','Tareas',$datos,'areaInforme');
	campoTexto('zonaTrabajo','Zona de trabajo',$datos);
	creaTablaEpis($datos['codigo']);
	echo '<div class="hide">';
	campoChecksClasificacion('checkClasificacionPuesto',$datos);
	echo '</div>';
	areaTexto('descripcion','Observaciones',$datos);
	campoOculto('','codigoCliente');
		
	cierraVentanaGestion('index.php',true);
}



function obtieneCodigoClientePuesto($datos){
	if(isset($_GET['codigoCliente'])){
		$res=$_GET['codigoCliente'];	
	}
	elseif($datos){
		$res=$datos['codigoCliente'];
	}
	else{
		$res='NULL';
	}

	return $res;
}

function imprimePuestos(){
	global $_CONFIG;

	conexionBD();

	$consulta=consultaBD("SELECT * FROM puestos_trabajo;");
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td>".$datos['nombre']."</td>
				<td>".$datos['zonaTrabajo']."</td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
	        </tr>";
	}
	cierraBD();
}

//Fin parte de gestión de puestos