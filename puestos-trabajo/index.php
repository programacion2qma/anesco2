<?php
  $seccionActiva=10;
  include_once('../cabecera.php');
  
  $res=operacionesPuestos();
  $estadisticas=estadisticasGenericas('puestos_trabajo',false);

  /*$puestos=consultaBD('SELECT * FROM puestos_trabajo ORDER BY nombre',true);
  while($puesto=mysql_fetch_assoc($puestos)){
    echo $puesto['nombre'].'<br/>';
    $nombre=strtolower($puesto['nombre']);
    $letra=strtoupper(substr($nombre, 0,1));
    $nombre=$letra.substr($nombre, 1);
    $sql='UPDATE puestos_trabajo SET nombre="'.$nombre.'" WHERE codigo='.$puesto['codigo'];
    echo $sql.'<br/>';
    $res=consultaBD($sql,true);
  }*/
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de gestión puestos:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-sitemap"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Puesto/s registrado/s</div>
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Puesto de trabajo</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                  <!--<a href="../empleados/index.php?codigo=<?=$cliente?>" class="shortcut noAjax"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label"> Volver</span> </a>
  				        <a href="<?php echo $_CONFIG['raiz']; ?>epis/index.php?codigoCliente=<?=$cliente?>" class="shortcut"><i class="shortcut-icon icon-shield"></i><span class="shortcut-label">EPIs</span> </a>-->
                  <a href="<?php echo $_CONFIG['raiz']; ?>puestos-trabajo/gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nuevo puesto</span> </a>
                  <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Puestos registrados</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Nombre</th>
                  <th> Zona de trabajo </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimePuestos();
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>