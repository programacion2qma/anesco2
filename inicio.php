<?php
    session_start();
    include_once('funciones.php');
    compruebaSesion();

    compruebaContratosVigor();
    $tipoUsuario=$_SESSION['tipoUsuario'];

    if($tipoUsuario=='EMPLEADO' && isset($_GET['ejercicio'])){
        header('Location: documentacion-empleado/?ejercicio='.$_GET['ejercicio']);
    }
    elseif($tipoUsuario=='EMPLEADO'){
        header('Location: documentacion-empleado/');
    }
    elseif($tipoUsuario=='CLIENTE'){
        include_once('inicioCliente.php');
    }
    else{
        include_once('inicioNormal.php');
    }