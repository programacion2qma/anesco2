<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas



function imprimeContratosSinPlanificacion(){
	global $_CONFIG;


	$consulta=consultaBD("SELECT (ofertas.total-ofertas.importe_iva) AS importe, clientes.EMPNOMBRE,clientes.EMPMARCA, clientes.codigo AS codigoCliente, contratos.*, 
						  MAX(contratos_renovaciones.fecha) AS renovacion, contratos.tecnico, ofertas.opcion, IFNULL(facturas.facturaCobrada,'NO') AS facturaCobrada, 
						  clientes.EMPID, formularioPRL.codigoUsuarioTecnico, contratoRevisado, contratos.fichero, facturas.codigo AS codigoFactura, suspendido, 
						  incrementoIPC, incrementoIpcAplicado, emitirPlanificacionSinPagar, clientes.EMPCP

						  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
						  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
						  LEFT JOIN contratos_renovaciones ON contratos.codigo=contratos_renovaciones.codigoContrato 
						  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
						  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO' AND facturas.eliminado='NO'
						  LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato

						  WHERE contratos.eliminado='NO' AND enVigor='SI' AND contratos.codigo NOT IN(SELECT codigoContrato FROM formularioPRL WHERE codigoContrato IS NOT NULL)
						  AND (contratos_en_facturas.codigo IS NULL OR facturas.eliminado='NO')						  

						  GROUP BY contratos.codigo;",true);


	while($datos=mysql_fetch_assoc($consulta)){

		$estado=obtieneEstadoContratoListado($datos);

		if($estado!='Pendiente cobrar'){
			echo "
			<tr>
				<td class='nowrap'>".$datos['EMPID']."</td>
				<td class='nowrap'>".formateaReferenciaContrato($datos,$datos)."</td>
				<td>".formateaFechaWeb($datos['fechaInicio'])."</td>
				<td>".formateaFechaWeb($datos['fechaFin'])."</td>
				<td>".$datos['EMPNOMBRE']."</td>
				<td>".$datos['EMPMARCA']."</td>
				<td>".$estado."</td>
				<td><a href='../contratos/gestion.php?codigo=".$datos['codigo']."' class='btn btn-small btn-propio noAjax' target='_blank'><i class='icon-external-link'></i> Ver </a></td>
			</tr>";
		}
	}

}