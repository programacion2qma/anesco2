<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión Clientes


function operacionesTarifas(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('tarifas');
	}
	elseif(isset($_POST['codigoInterno'])){
		$res=insertadatos('tarifas');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('tarifas');
	}

	mensajeResultado('codigoInterno',$res,'Tarifa');
    mensajeResultado('elimina',$res,'Oferta', true);
}


function gestionTarifas(){
	operacionesTarifas();
	
	abreVentanaGestionConBotones('Gestión de Tarifas','index.php');
	$datos=compruebaDatos('tarifas');
	
	abreColumnaCampos();
		if(!$datos){
			$numeroInterno = generaNumero();
		} else {
			$numeroInterno = $datos['codigoInterno'];
		}
		campoTexto('codigoInterno','Código tarifa',$numeroInterno,'input-small');
	cierraColumnaCampos();	

	abreColumnaCampos();
		campoTexto('nombre','Nombre',$datos);
		campoTexto('descripcion','Descripción',$datos);
	cierraColumnaCampos();


	echo "<br clear='all'><br/><br/>";
	abreColumnaCampos();
		for($i=1;$i<8;$i++){
			$nombre='trabajadores';
			if($i==1){
				$nombre='trabajador';
			}
			campoTextoSimbolo('trabajador'.$i,$i.' '.$nombre,'€',$datos,'input-mini pagination-right precio');
		}
	cierraColumnaCampos();

	abreColumnaCampos();
		for($i=8;$i<15;$i++){
			$nombre='trabajadores';
			campoTextoSimbolo('trabajador'.$i,$i.' '.$nombre,'€',$datos,'input-mini pagination-right precio');
		}
	cierraColumnaCampos();

	abreColumnaCampos();
		for($i=15;$i<21;$i++){
			$nombre='trabajadores';
			campoTextoSimbolo('trabajador'.$i,$i.' '.$nombre,'€',$datos,'input-mini pagination-right precio');
		}
		campoTextoSimbolo('sola','Solo esta tarifa','€',$datos,'input-mini pagination-right precio');
	cierraColumnaCampos();
	

	if($_SESSION['tipoUsuario'] == 'ADMIN'){	
		cierraVentanaGestion('index.php',true);
	} else {
		cierraVentanaGestion('index.php',true,false);
	}
}


//Fin parte de campos personalizados

function imprimeTarifas(){
	global $_CONFIG;

	conexionBD();
	$consulta=consultaBD("SELECT * FROM tarifas ORDER BY codigoInterno;");

	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td>".$datos['codigoInterno']."</td>
				<td>".$datos['nombre']."</td>
				<td>".$datos['descripcion']."</td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	cierraBD();
}

function generaNumero(){
	$consulta=consultaBD("SELECT MAX(codigoInterno) AS numero FROM tarifas", true, true);
	
	return $consulta['numero']+1;
}

function estadisticasTarifasRestrict(){
	$res=array();

	conexionBD();


	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM tarifas;",false, true);


	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

//Fin parte de gestión Clientes
