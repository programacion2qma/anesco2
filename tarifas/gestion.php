<?php
  $seccionActiva=1;
  include_once("../cabecera.php");
  gestionTarifas();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

		$('#trabajador1').focusout(function(){
			var valor = $('#trabajador1').val().replace(',','.');
			for(i=1;i<21;i++){
				var nuevoValor = parseFloat(valor*i);
				$('#trabajador'+i).val(nuevoValor);
			}
		});

		$('.precio').focusout(function(){
			var valor = $(this).val().replace(',','.');
			$(this).val(valor);
		});
	
	});
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>