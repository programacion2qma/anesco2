<?php
  session_start();
  include_once('funciones.php');
  $res=operacionesOfertas();

  $seccionActiva=1;
  include_once('../cabecera.php');
  
  $estadisticas=estadisticasOfertasRestrict('NO');
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de gestión ofertas:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-tags"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Oferta/s registrada/s</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de oferta</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="<?php echo $_CONFIG['raiz']; ?>ofertas/gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva oferta</span> </a>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-times-circle"></i><span class="shortcut-label">Eliminar oferta/s</span> </a>
                <a href="eliminadas.php" class="shortcut"><i class="shortcut-icon icon-trash-o"></i><span class="shortcut-label">Eliminadas</span> </a>

                <?php
                if($_SESSION['tipoUsuario']!='TECNICO'){?>
                  <a href="<?php echo $_CONFIG['raiz']; ?>ofertas/generaListado.php?aceptadas=NO" class="shortcut noAjax"><i class="shortcut-icon icon-download"></i><span class="shortcut-label">Listado Ofertas</span> </a><br />
                  <a href="<?php echo $_CONFIG['raiz']; ?>ofertas/ofertasPendientes.php" class="shortcut"><i class="shortcut-icon icon-flag"></i><span class="shortcut-label">Pendientes</span> </a>
                  <a href="<?php echo $_CONFIG['raiz']; ?>ofertas/ofertasAceptadas.php" class="shortcut"><i class="shortcut-icon icon-check-circle"></i><span class="shortcut-label">Aceptadas</span> </a>
                  <a href="<?php echo $_CONFIG['raiz']; ?>ofertas/ofertasRechazadas.php" class="shortcut"><i class="shortcut-icon icon-ban"></i><span class="shortcut-label">Rechazadas</span> </a>
                <?php } else {?>
                  <a href="<?php echo $_CONFIG['raiz']; ?>ofertas/generaListado.php?aceptadas=NO&tecnico" class="shortcut noAjax"><i class="shortcut-icon icon-download"></i><span class="shortcut-label">Listado Ofertas</span> </a>
                <?php } ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Ofertas registradas</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Fecha </th>
                  <th> ID </th>
                  <th> Razón Social </th>
                  <th> Nombre Comercial </th>
                  <th> Servicio contratado </th>
                  <th> Total </th>
                  <th> Estado </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeOfertas('NO','',true);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>


<!-- contenido --></div>

</script>

<?php include_once('../pie.php'); ?>