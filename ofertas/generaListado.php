<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('../../api/phpexcel/PHPExcel.php');
	require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("../documentos/plantillaOfertas.xlsx");
	

	listadoOfertas($objPHPExcel,$_GET['aceptadas']);//Llamada a la función que rellena el Excel

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('../documentos/Listado_Ofertas.xlsx');


	// Definir headers
	header("Content-Type: application/ms-xlsx");
	header("Content-Disposition: attachment; filename=Listado_Ofertas.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('../documentos/Listado_Ofertas.xlsx');
?>