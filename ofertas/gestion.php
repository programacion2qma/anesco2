<?php
  $seccionActiva=1;
  include_once("../cabecera.php");
  $j=gestionOfertas();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>js/filasTabla.js" type="text/javascript"></script>

<script src="../js/oyenteFormaPagoOfertasContratos.js" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $('#fechaInicioOferta').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
      var fecha=$(this).val().split('/').reverse().join('-');
      fecha = new Date(fecha);
      fecha.setDate(fecha.getDate() - 1);
      fecha.setDate(fecha.getDate() + 365);
      var dia=fecha.getDate()<10?'0'+fecha.getDate():fecha.getDate();
      var mes=(fecha.getMonth()+1)<10?'0'+(fecha.getMonth()+1):(fecha.getMonth()+1);
      var anio = fecha.getFullYear();
      $('#fechaFinOferta').val(dia+'/'+mes+'/'+anio);
      $('#fechaFinOferta').datepicker("update");
    });

    //obtenerEmpleados($('#codigoCliente'));
    $('#codigoCliente').change(function(){
        var num = obtenerEmpleados($(this),true)
        oyenteImporte(num,$('#codigoTarifa').val());
    });

    mostrarOtra($('input[name=opcion]:checked').val());
    $('input[name=opcion]').change(function(){
        oyenteImporte($('#numEmpleados').val(),$('#codigoTarifa').val());
        mostrarOtra($(this).val());
    });


    $('#codigoTarifa').change(function(){
        oyenteImporte($('#numEmpleados').val(),$('#codigoTarifa').val());
    });


    $('#subtotal,input[name=iva]').change(function(){
        if($('#subtotal').val()!=''){
            calculaIVA(formateaNumeroCalculo($('#subtotal').val()));
        }
    });

    mostrarRechazado($('input[name=rechazada]:checked').val());
    $('input[name=rechazada]').change(function(){
        mostrarRechazado($(this).val());
    });

    mostrarRecoMedicosEspecialidades($('input[name=opcion]:checked').val());
    //mostrarIva($('input[name=opcion]:checked').val());
    $('input[name=opcion]').change(function(){
        mostrarRecoMedicosEspecialidades($(this).val());
       // mostrarIva($(this).val());
    });

    $('input[name=tipoEmpleados]').change(function(){
      var num = obtenerEmpleados($('#codigoCliente'),true)
      oyenteImporte(num,$('#codigoTarifa').val());
    });

    /*$('#calcular').click(function(){
        if($('#subtotal').val()=='' && $('input[name=opcion]:checked').val()!=4 && $('#codigoTarifa').val()!='NULL'){
            oyenteImporte($('#numEmpleados').val(),$('#codigoTarifa').val());
        } 
        else {
            calculaIVA(formateaNumeroCalculo($('#subtotal').val()));
        }
    });

    $('.importes').focusout(function(){
        var id=obtieneFilaCampo($(this));
        var tipoUsuario = $('#tipoUsuario').val();
        var importe = $(this).val();
        if(tipoUsuario != 'ADMIN'){
            var empleados = obtenerEmpleados($('#codigoCliente'),true)
            if(empleados > 20){
                empleados='20';
            }
            var importeBase=$('#codPrecio'+id+'_'+empleados.trim()).text();
            var sola = $('#codPrecio'+id+'_sola').text();
            if(actividadesSeleccionadas==1 && sola!='0'){
                importeBase=sola;
            }
            if(importe < importeBase){
                $(this).val(importeBase);
                alert('Contacte con la Dirección técnica si desea modificar la tarifa por debajo del limite establecido');
            }
        }
    });*/


    $('#importeRM, #numRM').change(function(){
        calculaSubtotalRM();
    });

    $(':submit').unbind().click(function(e){
        e.preventDefault();
        validaDatosOferta();
    });
  });

function oyenteImporte(empleados,tarifa){
  if(codigoCliente != 'NULL'){
    var consulta=$.post('../listadoAjax.php?include=ofertas&funcion=obtieneImporte();',{'empleados':empleados,'tarifa':tarifa});

    consulta.done(function(respuesta){
      if(respuesta=='fallo'){
        alert('Ha habido un problema al obtener el importe de esta tarifa.\nInténtelo de nuevo en unos segundos.\nSi el   problema persiste, contacte con webmaster@qmaconsultores.com');
      }
      else{
        $('#subtotal').val(formateaNumeroWeb(respuesta));
        calculaIVA(respuesta);
      }
    });
  }
}

function calculaIVA(subtotal){
    var importe = 0;

    if(subtotal != ''){
        if($('input[name=iva]:checked').val() == 'SI'){
          importe=subtotal*0.21;
        }
    }

    $('#importe_iva').val(formateaNumeroWeb(importe));
    calculaTotal();    
}

function obtenerEmpleados(cliente,devolver=false){
  var cod = '#codCliente'+cliente.val();
  var tipo = $('input[name=tipoEmpleados]:checked').val();
  if(tipo=='ACTIVOS'){
    cod+='_activos';
  }
  var num = $(cod).text();
  $('#numEmpleados').val(num);
  if(devolver){
    return num;
  }
}

function mostrarOtra(val){
  if(val==4){
    $('#otras').removeClass('hide');
  } else {
    $('#otras').addClass('hide');
  }
}

function mostrarRecoMedicosEspecialidades(val){
  if(val==1 || val==2){
    $('#recoMedicos').removeClass('hide');
    $('#cajaEspecialidades').addClass('hide');
  }
  else if(val==3){
    $('#recoMedicos').addClass('hide');
    $('#cajaEspecialidades').removeClass('hide');
  }
  else{
    $('#recoMedicos').addClass('hide');
    $('#cajaEspecialidades').addClass('hide');
  }
}

function mostrarRechazado(val){
  if(val=='SI'){
    $('#rechazar').removeClass('hide');
  } else {
    $('#rechazar').addClass('hide');
  }
}

function calculaSubtotalRM(){
    var importeRM=formateaNumeroCalculo($('#importeRM').val());
    var numRM=formateaNumeroCalculo($('#numRM').val());

    var total=importeRM*numRM;

    $('#subtotalRM').val(formateaNumeroWeb(total));

    calculaTotal();
}

function calculaTotal(){
    var subtotalRM=formateaNumeroCalculo($('#subtotalRM').val());
    var subtotalRM=0;
    var subtotal=formateaNumeroCalculo($('#subtotal').val());
    var importeIva=formateaNumeroCalculo($('#importe_iva').val());

    var total=parseFloat(subtotalRM)+parseFloat(subtotal)+parseFloat(importeIva);

    $('#total').val(formateaNumeroWeb(total));
}

function mostrarIva(actividadContratada){
  if(actividadContratada==2){
    $('input[name=iva][value="NO"]').attr('checked',true);
    $('#cajaIva').addClass('hide');
    $('#subtotal').parent().parent().prev().text('Total:');
  }
  else{
    $('#cajaIva').removeClass('hide'); 
    $('#subtotal').parent().parent().prev().text('Subtotal:');
  }
}


function validaDatosOferta(){
    var fecha=$('#fecha').val();
    var total=$('#total').val();
    var codigoCliente=$('#codigoCliente').val();

    if(fecha=='' || total=='' || codigoCliente=='NULL'){
        alert('Los campos Fecha, Total y Cliente son obligatorios.');
    }
    else{
        var consulta=$.post('../listadoAjax.php?include=ofertas&funcion=compruebaDatosClienteOferta();',{'codigoCliente':codigoCliente});
        consulta.done(function(respuesta){
            if(respuesta=='ok'){
                $('form').submit();        
            }
            else{
                alert('Los siguientes campos de la ficha del cliente son necesarios para registrar la oferta: \n- Razón social\n- Nombre comercial\n- Actividad\n- Dirección centro de trabajo (al menos la localidad)\n- Nº de trabajadores\n- Nº de centros');
                window.open('/anesco2/clientes/gestion.php?codigo='+codigoCliente,'_blank');
            }
        });
    }
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>