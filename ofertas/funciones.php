<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión Ofertas


function operacionesOfertas(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaOferta();
	}
	elseif(isset($_POST['codigoInterno'])){
		$res=insertaOferta();
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadaOferta($_POST['elimina']);//elimina será 'SI' o 'NO'
	}

	mensajeResultado('codigoInterno',$res,'Oferta');
    mensajeResultado('elimina',$res,'Oferta', true);
}

function insertaOferta(){
	global $_CONFIG;

	formateaCamposOferta();

	$res=insertaDatos('ofertas');
	
	if($res){
		$datos=arrayFormulario();
		$codigoOferta=$res;

		conexionBD();

		$res=insertaActividades($datos,$codigoOferta);
		$res=$res && insertaDocumentacion($datos,$codigoOferta);
		$res=$res && insertaCentrosTrabajoOferta($datos,$codigoOferta);

		cierraBD();
	}
	
	if($_POST['mostrar']=='NO'){
		header('Location: '.$_CONFIG['raiz'].'contratos/gestion.php?codigoOferta='.$codigoOferta);
	}
	

	return $res;
}

function actualizaOferta(){
	formateaCamposOferta();

	$res=actualizaDatos('ofertas');

	if($res){
		$datos=arrayFormulario();
		$codigoOferta=$datos['codigo'];

		conexionBD();

		$res=insertaActividades($datos,$codigoOferta,true);
		$res=$res && insertaDocumentacion($datos,$codigoOferta);

		cierraBD();
	}

	return $res;
}

function revisaDocumentos($codigo){
	$res=true;
	$contrato=datosRegistro('contratos',$codigo,'codigoOferta');
	if($contrato){
		$documento=consultaBD('SELECT COUNT(codigo) AS total FROM documentosVS WHERE codigoContrato='.$contrato['codigo'],true,true);
		if($documento['total']>0){
			if(!in_array($_POST['opcion'], array(1,2))){
				$res=consultaBD('DELETE FROM documentosVS WHERE codigoContrato='.$contrato['codigo'],true,true);
			}
		}
	}
	return $res;
}


function formateaCamposOferta(){
	$datos=arrayFormulario();

	$_POST['subtotal']=formateaNumeroWeb($datos['subtotal'],true);
	$_POST['importe_iva']=formateaNumeroWeb($datos['importe_iva'],true);
	$_POST['importeRM']=formateaNumeroWeb($datos['importeRM'],true);
	$_POST['subtotalRM']=formateaNumeroWeb($datos['subtotalRM'],true);
	$_POST['importeRMExtra']=formateaNumeroWeb($datos['importeRMExtra'],true);
	$_POST['total']=formateaNumeroWeb($datos['total'],true);

	
	if(isset($_POST['especialidadTecnica'])){
		$especialidades='';

		foreach($datos['especialidadTecnica'] as $especialidad){
			$especialidades.=$especialidad.'&{}&';
		}

		$_POST['especialidadTecnica']=quitaUltimaComa($especialidades,4);
	}
	else{
		$_POST['especialidadTecnica']='';
	}
}


function cambiaEstadoEliminadaOferta($estado){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],'ofertas',false);
	}
	cierraBD();

	return $res;
}

function insertaActividades($datos,$codigoOferta,$actualizacion=false){
	$res=true;//Siempre que vayamos a usarlo con un AND, inicializarlo a true

	if($actualizacion){
		$res=consultaBD('DELETE FROM actividades_ofertas WHERE codigoOferta='.$codigoOferta);
	}

	$i=0;

	while(isset($datos['codigoActividad'.$i])){
		if($datos['actividad'.$i] == 'SI'){
			$res=$res && consultaBD("INSERT INTO actividades_ofertas VALUES(NULL, ".$codigoOferta.",".$datos['codigoActividad'.$i].",".$datos['importe'.$i].")");
		}

		$i++;
	}

	return $res;
}


function gestionOfertas(){
	operacionesOfertas();
	
	abreVentanaGestionOferta();
	$datos=compruebaDatos('ofertas');

	if(isset($_GET['desdeContrato'])){
		campoOculto('NO','mostrar');
	} else {
		campoOculto($datos,'mostrar','SI');
	}
	campoOculto($_SESSION['tipoUsuario'],'tipoUsuario');
	
	campoOculto($datos,'eliminado','NO');
	echo "<h3 class='apartadoFormulario'> Datos de oferta</h3>";
	abreColumnaCampos();
		campoFechaObligatoria('fecha','Fecha',$datos);
		if(!$datos){
			$numeroInterno = generaNumero();
			$codigoCliente=-1;
		} else {
			$numeroInterno = $datos['codigoInterno'];
			$codigoCliente=$datos['codigoCliente'];
		}
		campoSelectConsulta('codigoCliente','Cliente','SELECT codigo,EMPNOMBRE as texto FROM clientes WHERE eliminado="NO" OR codigo="'.$codigoCliente.'" ORDER BY EMPNOMBRE',$datos);
		campoRadio('tipoEmpleados','Empleados',$datos,'ACTIVOS',array('Todos','Solo de centros activos'),array('TODOS','ACTIVOS'));
		
		campoTexto('numEmpleados','Nº de empleados',$datos,'input-mini',true);
		numeroEmpleados();
		
		campoUsuarioOferta($datos);
	cierraColumnaCampos();	

	abreColumnaCampos();
		campoTexto('codigoInterno','Nº de oferta',$numeroInterno,'input-mini pagination-right',true);
		//campoSelect('fase','Fase',array('Primer Contacto','Presentación','Propuesta Económica','Aprobada y en espera de inicio','Ganada','Perdida'),array('contacto','presentacion','propuesta','aprobada','ganada','perdida'),$datos);
		campoRadio('rechazada','¿Rechazada?',$datos['rechazada'],'NO');
	echo '<div style="clear:both" id="rechazar" class="hide">';
		campoFecha('fechaRechazada','Fecha',$datos);	
		areaTexto('motivosRechazo','Motivos',$datos,'areaInforme');
	echo '</div>';
	
		campoFecha('fechaInicioOferta','Inicio servicios',$datos);
		if($datos){
			$fechaFin=$datos['fechaFinOferta'];
		} else {
			$fechaFin = date('Y-m-d');
			$fechaFin = strtotime ('-1 days',strtotime($fechaFin));
			$fechaFin = date('Y-m-d',$fechaFin);
			$fechaFin = strtotime ('+365 days',strtotime($fechaFin));
			$fechaFin = date('Y-m-d',$fechaFin);
		}
		campoFecha('fechaFinOferta','Fin servicios',$fechaFin);

		campoOculto('','fechaInicio');
		campoOculto('','fechaFin');
	cierraColumnaCampos();


	echo "<h3 class='apartadoFormulario sinFlotar'> Actividades contratadas</h3>";
	abreColumnaCampos();

	campoRadio('opcion','Contratatación',$datos,1,array('SPA 4 Especialidades','SPA Vigilancia de la salud','SPA Especialides técnicas','Otras actuaciones'),array(1,2,3,4),true);
	campoSelectConsulta('codigoTarifa','Tarifa','SELECT codigo,nombre AS texto FROM tarifas',$datos);
	campoTexto('otraTarifa','Otra tarifa',$datos);
	

	cierraColumnaCampos();
	abreColumnaCampos('span6');

	//campoCalcula('subtotal','Subtotal',formateaNumeroWeb($datos['subtotal']),'€','btn-propio','input-mini pagination-right');
	campoTextoSimbolo('subtotal','Subtotal','€',formateaNumeroWeb($datos['subtotal']));
	
	echo "<div id='cajaIva'>";
	campoRadio('iva','IVA (21%)',$datos['iva'],'SI');
	campoTextoSimbolo('importe_iva','Importe IVA','€',formateaNumeroWeb($datos['importe_iva']),'input-mini pagination-right',0,true);
	echo "</div>";

	cierraColumnaCampos();

	echo '<div style="clear:both" id="otras" class="hide">';
		$bloqueado=false;
		if($datos && $datos['aceptado']=='SI'){
			$bloqueado=true;
		}
		areaTexto('otraOpcion','Definir',$datos,'areaInforme',$bloqueado);
	echo '</div>';
	

	/*$tarifas=consultaBD('SELECT * FROM tarifas ORDER BY codigoInterno',true);
	$i=0;
	$contratadas=0;
	while($tarifa=mysql_fetch_assoc($tarifas)){
		$contratada='NO';
		$importe='';
		if($datos){
			$actividad=consultaBD('SELECT * FROM actividades_ofertas WHERE codigoOferta='.$datos['codigo'].' AND codigoActividad='.$tarifa['codigo'],true,true);
			if($actividad){
				$contratadas++;
				$contratada='SI';
				$importe=$actividad['importe'];;
			}
		}
		camposPreciosTrabajadores($i,$tarifa);
		campoOculto($tarifa['codigo'],'codigoActividad'.$i);
		campoDato('Actividad',$tarifa['nombre']);
		echo "<div class='contradada'>";
		campoRadio('actividad'.$i,'Contratada',$contratada);
		echo "</div>";
		campoTextoSimbolo('importe'.$i,'Importe','€',$importe,'input-mini pagination-right importes');
		$i++;
		echo "<h3 class='apartadoFormulario'></h3>";
	}*/

	echo '<div id="recoMedicos" class="hide sinFlotar">';
		echo "<h3 class='apartadoFormulario sinFlotar'> Reconocimientos médicos</h3>";
		abreColumnaCampos('span5');
		
		campoTextoSimbolo('importeRM','Precio RM incluido en contrato','€',formateaNumeroWeb($datos['importeRM']));
		campoTexto('numRM','Nº de RRMM incluidos',$datos,'input-mini pagination-right');
		campoTextoSimbolo('subtotalRM','Subtotal (exento IVA)','€',formateaNumeroWeb($datos['subtotalRM']),'input-mini pagination-right',0,true);

		cierraColumnaCampos();
		abreColumnaCampos();

		campoTextoSimbolo('importeRMExtra','Precio RM extra (fuera contrato)','€',formateaNumeroWeb($datos['importeRMExtra']));

		cierraColumnaCampos();
	echo '</div>';

	echo '<div id="cajaEspecialidades" class="hide">';
		echo "<h3 class='apartadoFormulario sinFlotar'> Especialidades técnicas</h3>";
		abreColumnaCampos('span5');
		
		campoSelectMultiple('especialidadTecnica','Especialidad técnica',array('Higiene Industrial','Seguridad en el Trabajo','Ergonomía y Psicosociología'),array('Higiene Industrial','Seguridad en el Trabajo','Ergonomía y Psicosociología'),$datos,'span6 show-tick selectpicker');

		cierraColumnaCampos();
	echo '</div>';

	echo "<h3 class='apartadoFormulario sinFlotar'> Datos de pago</h3>";
	abreColumnaCampos();
		
		campoFormaPago($datos,'codigoFormaPagoOferta');
		campoSelectCuentaPropia($datos,'codigoCuentaPropiaOferta');

		campoTextoSimbolo('total','Total','€',formateaNumeroWeb($datos['total']),'input-mini pagination-right obligatorio',0,true);
		campoOculto($datos,'aceptado','NO');
		areaTexto('observaciones','Observaciones',$datos,'areaInforme');
	cierraColumnaCampos();
	abreColumnaCampos('sinFlotar');
		echo "<h3 class='apartadoFormulario'>Documentación</h3>";
		tablaFicheros($datos);
	cierraColumnaCampos(true);
	
	
	cierraVentanaGestionOferta();

	return 0;
}

function campoUsuarioOferta($datos){
	if($datos){
		$codigoUsuario=$datos['codigoUsuario'];
	}
	else{
		$codigoUsuario=$_SESSION['codigoU'];
	}

	campoSelectConsulta('codigoUsuarioMostrar','Usuario',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE tipo NOT IN('CLIENTE','EMPLEADO');",$codigoUsuario,'selectpicker span3 show-tick',"data-live-search='true'",true);
	campoOculto($codigoUsuario,'codigoUsuario','NULL');
}

function abreVentanaGestionOferta(){
	if(isset($_GET['editable']) && $_GET['editable']=='NO'){
		abreVentanaGestionConBotones('Gestión de Ofertas','index.php','','icon-edit','',true,'noAjax','ofertasAceptadas.php',false);
	}
	elseif(isset($_GET['editable']) && $_GET['editable']=='ELIMINADA'){
		abreVentanaGestionConBotones('Gestión de Ofertas','index.php','','icon-edit','',true,'noAjax','eliminadas.php',false);
	}
	else{
		abreVentanaGestionConBotones('Gestión de Ofertas','index.php','','icon-edit','',true,'noAjax');
	}
}

function cierraVentanaGestionOferta(){
	if(isset($_GET['editable']) && $_GET['editable']=='NO'){
		cierraVentanaGestion('ofertasAceptadas.php',true,false);
	}
	elseif(isset($_GET['editable']) && $_GET['editable']=='ELIMINADA'){
		cierraVentanaGestion('eliminadas.php',true,false);
	}
	else{
		cierraVentanaGestion('index.php',true);
	}
}

//Parte de campos personalizados


//Fin parte de campos personalizados

function imprimeOfertas($eliminadas,$aceptadas='',$celdaEstado=false){
	if($aceptadas!=''){
		$aceptadas="AND aceptado='$aceptadas' AND ofertas.rechazada='NO'";
	}
	
	$where=defineWhereEjercicio('WHERE 1=1','ofertas.fecha');

	conexionBD();

	if($_SESSION['tipoUsuario'] == 'COLABORADOR'){
		$consulta=consultaBD("SELECT ofertas.*, clientes.EMPNOMBRE, clientes.EMPMARCA, ofertas.opcion FROM ofertas LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND codigoCliente IN (SELECT codigo FROM clientes WHERE EMPASESORIA=".$_SESSION['codigoU'].") AND mostrar='SI' AND ofertas.eliminado='$eliminadas' $aceptadas ORDER BY fecha;");
	}
	else if($_SESSION['tipoUsuario'] == 'TECNICO'){
		$consulta=consultaBD("SELECT ofertas.*, clientes.EMPNOMBRE, clientes.EMPMARCA, ofertas.opcion 
							  FROM ofertas LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo
							  LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta
							  LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato 
							  $where AND mostrar='SI' AND ofertas.eliminado='$eliminadas' 
							  AND (contratos.codigo IS NULL OR contratos.tecnico=".$_SESSION['codigoU']." OR formularioPRL.codigoUsuarioTecnico=".$_SESSION['codigoU']." OR ofertas.codigoUsuario=".$_SESSION['codigoU'].") 
							  GROUP BY ofertas.codigo ORDER BY fecha;");
	} 
	else{
		$consulta=consultaBD("SELECT ofertas.*, clientes.EMPNOMBRE, clientes.EMPMARCA, ofertas.opcion FROM ofertas LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND mostrar='SI' AND ofertas.eliminado='$eliminadas' $aceptadas ORDER BY fecha;");
	}
	
	while($datos=mysql_fetch_assoc($consulta)){
		$servicio=obtieneNombreServicioContratado($datos['opcion']);

		if($celdaEstado){
			$estado=obtieneEstadoOfertaListado($datos);

			echo "
			<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['codigoInterno']."</td>
				<td>".$datos['EMPNOMBRE']."</td>
				<td>".$datos['EMPMARCA']."</td>	
				<td>".$servicio."</td>
				<td class='nowrap'>".formateaNumeroWeb($datos['total']-$datos['importe_iva'])." €</td>
				<td>".$estado."</td>
				<td class='centro'>
					".crearBoton($datos,$eliminadas)."
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
		}
		else{
			echo "
			<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['codigoInterno']."</td>
				<td>".$datos['EMPNOMBRE']."</td>
				<td>".$datos['EMPMARCA']."</td>	
				<td>".$servicio."</td>
				<td class='nowrap'>".formateaNumeroWeb($datos['total']-$datos['importe_iva'])." €</td>
				<td class='centro'>
					".crearBoton($datos,$eliminadas)."
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
		}
	}
	cierraBD();
}

function imprimeOfertasRechazadas(){

	conexionBD();
	$where=defineWhereEjercicio('WHERE 1=1','ofertas.fecha');
	if($_SESSION['tipoUsuario'] == 'COLABORADOR'){
		$consulta=consultaBD("SELECT ofertas.*, clientes.EMPNOMBRE FROM ofertas INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND codigoCliente IN (SELECT codigo FROM clientes WHERE EMPASESORIA=".$_SESSION['codigoU'].") AND mostrar='SI' AND ofertas.eliminado='NO' AND ofertas.rechazada='SI' ORDER BY fecha;");
	} else {
		$consulta=consultaBD("SELECT ofertas.*, clientes.EMPNOMBRE FROM ofertas INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND mostrar='SI' AND ofertas.eliminado='NO' AND ofertas.rechazada='SI' ORDER BY fecha;");
	}
	$iconoC=array('SI'=>'<i style="color:#5DB521;font-size:24px;" class="icon-check"></i>','NO'=>'<i style="color:#FF0000;font-size:24px;" class="icon-close"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['codigoInterno']."</td>
				<td>".$datos['EMPNOMBRE']."</td>
				<td class='nowrap'>".formateaNumeroWeb($datos['total']-$datos['importe_iva'])." €</td>
				<td>".formateaFechaWeb($datos['fechaRechazada'])."</td>
				<td>".$datos['motivosRechazo']."</td>
				<td class='centro'>
					".crearBoton($datos,'SI')."
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	cierraBD();
}

function crearBoton($datos,$eliminadas){
	global $_CONFIG;
	if($eliminadas=='NO' && $datos['aceptado']=='SI'){//Aprobadas
		$res="		<div class='btn-group centro'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."ofertas/gestion.php?codigo=".$datos['codigo']."&editable=NO'><i class='icon-search-plus'></i> Detalles</i></a></li>";
		$contrato=consultaBD('SELECT * FROM contratos WHERE codigoOferta='.$datos['codigo'],false,true);
		if($contrato){
			$res.="			<li class='divider'></li>
						    <li><a href='".$_CONFIG['raiz']."contratos/gestion.php?codigo=".$contrato['codigo']."'><i class='icon-file-text-o'></i> Ver contrato</i></a></li>
						    <li class='divider'></li>
						    <li><a target='_blank' class='noAjax' href='".$_CONFIG['raiz']."ofertas/generaDocumento.php?codigo=".$datos['codigo']."'><i class='icon-cloud-download'></i> Descargar oferta</i></a></li>";
		} 
		else {
			$res.="			<li class='divider'></li>
						    <li><a href='".$_CONFIG['raiz']."contratos/gestion.php?codigoOferta=".$datos['codigo']."'><i class='icon-file-text-o'></i> Crear contrato</i></a></li>
						    <li class='divider'></li>
						    <li><a target='_blank' class='noAjax' href='".$_CONFIG['raiz']."ofertas/generaDocumento.php?codigo=".$datos['codigo']."'><i class='icon-cloud-download'></i> Descargar oferta</i></a></li>";
		} 			    

		$res.="				<li class='divider'></li>
						    <li><a target='_blank' class='noAjax' href='".$_CONFIG['raiz']."correos/gestion.php?codigoOferta=".$datos['codigo']."'><i class='icon-send'></i> Enviar oferta</i></a></li>
						</ul>
					</div>";
	} 
	elseif($eliminadas=='NO') {//Pendientes
		$res="	<div class='btn-group centro'>
					<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
				  	<ul class='dropdown-menu' role='menu'>
					    <li><a href='".$_CONFIG['raiz']."ofertas/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li>
						<li class='divider'></li>
					    <li><a href='".$_CONFIG['raiz']."contratos/gestion.php?codigoOferta=".$datos['codigo']."'><i class='icon-file-text-o'></i> Aceptar y crear contrato</i></a></li>
					    <li class='divider'></li>
					    <li><a target='_blank' class='noAjax' href='".$_CONFIG['raiz']."ofertas/generaDocumento.php?codigo=".$datos['codigo']."'><i class='icon-cloud-download'></i> Descargar oferta</i></a></li>
					    <li class='divider'></li>
						<li><a target='_blank' class='noAjax' href='".$_CONFIG['raiz']."correos/gestion.php?codigoOferta=".$datos['codigo']."'><i class='icon-send'></i> Enviar oferta</i></a></li>
					</ul>
				</div>";			    
	}
	elseif($eliminadas=='RECHAZADA'){//Rechazadas
		$res="	<div class='btn-group centro'>
					<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
				  	<ul class='dropdown-menu' role='menu'>
					    <li><a href='".$_CONFIG['raiz']."ofertas/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li>
					    <li class='divider'></li>
					    <li><a target='_blank' class='noAjax' href='".$_CONFIG['raiz']."ofertas/generaDocumento.php?codigo=".$datos['codigo']."'><i class='icon-cloud-download'></i> Descargar oferta</i></a></li>
					    <li class='divider'></li>
						<li><a target='_blank' class='noAjax' href='".$_CONFIG['raiz']."correos/gestion.php?codigoOferta=".$datos['codigo']."'><i class='icon-send'></i> Enviar oferta</i></a></li>
					</ul>
				</div>";	
	}
	else{//Eliminadas
		$res="	<div class='btn-group centro'>
					<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
				  	<ul class='dropdown-menu' role='menu'>
					    <li><a href='".$_CONFIG['raiz']."ofertas/gestion.php?codigo=".$datos['codigo']."&editable=ELIMINADA'><i class='icon-search-plus'></i> Detalles</i></a></li>
					    <li class='divider'></li>
					    <li><a target='_blank' class='noAjax' href='".$_CONFIG['raiz']."ofertas/generaDocumento.php?codigo=".$datos['codigo']."'><i class='icon-download'></i> Descargar oferta</i></a></li>
					</ul>
				</div>";	
	}

	return $res;
}

function generaNumero(){
	$consulta=consultaBD("SELECT MAX(codigoInterno) AS numero FROM ofertas WHERE YEAR(fecha)=".date('Y'), true, true);
	
	return $consulta['numero']+1;
}

function estadisticasOfertasRestrict($eliminadas,$aceptadas=''){
	$res=array();

	if($aceptadas!=''){
		$aceptadas="AND aceptado='$aceptadas' AND ofertas.rechazada='NO'";
	}

	$where=defineWhereEjercicio('WHERE 1=1','ofertas.fecha');

	conexionBD();
	
	if($_SESSION['tipoUsuario'] == 'COLABORADOR'){
		$consulta=consultaBD("SELECT COUNT(ofertas.codigo) AS total FROM ofertas INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND codigoCliente IN (SELECT codigo FROM clientes WHERE EMPASESORIA=".$_SESSION['codigoU'].") AND mostrar='SI' AND ofertas.eliminado='$eliminadas' $aceptadas",false,true);
	} else if($_SESSION['tipoUsuario'] == 'TECNICO'){
		$consulta=consultaBD("SELECT COUNT(DISTINCT ofertas.codigo) AS total FROM ofertas INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato ".$where." AND mostrar='SI' AND ofertas.eliminado='$eliminadas' AND aceptado='SI' AND (contratos.tecnico=".$_SESSION['codigoU']." || formularioPRL.codigoUsuarioTecnico=".$_SESSION['codigoU'].");",false, true);
	} else {
		$consulta=consultaBD("SELECT COUNT(ofertas.codigo) AS total FROM ofertas INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND mostrar='SI' AND ofertas.eliminado='$eliminadas' $aceptadas;",false, true);
	}

	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

function estadisticasOfertasRechazadas(){
	$res=array();

	conexionBD();
	$where=defineWhereEjercicio('WHERE 1=1','ofertas.fecha');
	if($_SESSION['tipoUsuario'] == 'COLABORADOR'){
		$consulta=consultaBD("SELECT COUNT(ofertas.codigo) AS total FROM ofertas INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND codigoCliente IN (SELECT codigo FROM clientes WHERE EMPASESORIA=".$_SESSION['codigoU'].") AND mostrar='SI' AND ofertas.eliminado='NO' AND ofertas.rechazada='SI'",false,true);
	} else {
		$consulta=consultaBD("SELECT COUNT(ofertas.codigo) AS total FROM ofertas INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND mostrar='SI' AND ofertas.eliminado='NO' AND ofertas.rechazada='SI';",false, true);
	}

	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

function numeroEmpleados(){
	$consulta=consultaBD("SELECT codigo, EMPNTRAB FROM clientes", true);
	while($datos=mysql_fetch_assoc($consulta)){
		divOculto($datos['EMPNTRAB'],'codCliente'.$datos['codigo']);
		$trabajadores=consultaBD('SELECT SUM(trabajadores) AS total FROM clientes_centros WHERE activo="SI" AND eliminado="NO" AND codigoCliente='.$datos['codigo'],true,true);
		divOculto($trabajadores['total'],'codCliente'.$datos['codigo'].'_activos');
	}
}

function camposPreciosTrabajadores($indice,$tarifa){
	for($i=1;$i<21;$i++){
		divOculto($tarifa['trabajador'.$i],'codPrecio'.$indice.'_'.$i);
	}
	divOculto($tarifa['sola'],'codPrecio'.$indice.'_sola');
}

function obtieneImporte(){
	$datos=arrayFormulario();
	if($datos['empleados']==''){
		$datos['empleados']=1;
	}

	if($datos['empleados']>20){
		$res=consultaBD('SELECT trabajador1 AS precio FROM tarifas WHERE codigo='.$datos['tarifa'],true,true);
		$res=$res['precio']*$datos['empleados'];
	} else {
		$res=consultaBD('SELECT trabajador'.$datos['empleados'].' AS precio FROM tarifas WHERE codigo='.$datos['tarifa'],true,true);
		$res=$res['precio'];
	}

	echo $res;
}


function listadoOfertas($objPHPExcel,$aceptadas=''){
	if($aceptadas!=''){
		$aceptadas="AND ofertas.aceptado='$aceptadas'";
	}

	conexionBD();
	if(isset($_GET['tecnico'])){
		$consulta=consultaBD("SELECT ofertas.* FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato WHERE ofertas.eliminado='NO' AND ofertas.aceptado='SI' AND (contratos.tecnico=".$_SESSION['codigoU']." || formularioPRL.codigoUsuarioTecnico=".$_SESSION['codigoU'].")  GROUP BY ofertas.codigo ORDER BY ofertas.codigo;");
	} else {
		$consulta=consultaBD("SELECT * FROM ofertas WHERE eliminado='NO' $aceptadas ORDER BY codigo;");
	}
	cierraBD();

	$i=4;
	while($datos=mysql_fetch_assoc($consulta)){
		$cliente=datosRegistro('clientes',$datos['codigoCliente']);

		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($datos['codigo']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($cliente['EMPNOMBRE']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($datos['fecha']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue($datos['subtotal']);
		$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue($datos['importe_iva']);
		$objPHPExcel->getActiveSheet()->getCell('G'.$i)->setValue($datos['total']);
		$objPHPExcel->getActiveSheet()->getCell('H'.$i)->setValue($datos['aceptado']);
		$i++;
	}
	foreach(range('B','H') as $columnID) {
    	$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
	}
}

function compruebaDatosClienteOferta(){
	$res='ok';
	$datos=arrayFormulario();
	extract($datos);

	$cliente=consultaBD("SELECT EMPNOMBRE, EMPACTIVIDAD, EMPLOC, EMPMARCA, EMPNTRAB, EMPCENTROS FROM clientes WHERE codigo='$codigoCliente';",true,true);

	foreach($cliente as $campo){
		if(trim($campo)==''){
			$res='error';
		}
	}

	echo $res;
}

function tablaFicheros($datos){
	echo"
	 		<center>
	 		
				<table class='table table-bordered mitadAncho' id='tablaFicheros'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Nombre </th>
							<th> Fichero </th>
							<!--th> Eliminar </th-->
                    	</tr>
                  	</thead>
                  	<tbody>";

					    $i=0;
						if($datos){
							$consulta=consultaBD("SELECT * FROM ofertas_ficheros WHERE codigoOferta=".$datos['codigo'],true);				  
							while($fichero=mysql_fetch_assoc($consulta)){
							 	echo "<tr>";
							 		campoOculto($fichero['codigo'],'codigoFichero'.$i);
									campoTextoTabla('nombreSubido'.$i,$fichero['nombre']);
									campoFichero('ficheroSubido'.$i,'',1,$fichero['fichero'],'../documentos/ofertas/','Ver/descargar');
								echo"
									</tr>";
								$i++;
							}
						}
						campoOculto($i,'ficherosTotales');
						$i=0;
						echo "<tr>";
							campoTextoTabla('nombreSubido0');
							campoFichero('ficheroSubido0','Documento',1);
							//echo "<td></td>";
						echo "</tr>";

						echo "
					</tbody>
	                </table>
	            

					<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaFicheros\");'><i class='icon-plus'></i> Añadir documento</button>
				</center><br />";
}


function insertaDocumentacion($datos,$codigoOferta){
	$res=true;

	$i=0;
	while(isset($datos['codigoFichero'.$i])){
		$res=$res && consultaBD('UPDATE ofertas_ficheros SET nombre="'.$datos['nombreSubido'.$i].'" WHERE codigo='.$datos['codigoFichero'.$i]);
		$i++;
	}

	$i=0;
	while(isset($datos['nombreSubido'.$i])){
		if($_FILES['ficheroSubido'.$i]['tmp_name'] != ''){
			$fichero=subeDocumento('ficheroSubido'.$i,time().$i,'../documentos/ofertas');

			$res=$res && consultaBD("INSERT INTO ofertas_ficheros VALUES(NULL, '$codigoOferta', '".$datos['nombreSubido'.$i]."', '".$fichero."');");
		}
		$i++;
	}
	
	return $res;
}


function insertaCentrosTrabajoOferta($datos,$codigoOferta){
	$res=true;

	$codigoCliente=$datos['codigoCliente'];
	$tipo=$datos['tipoEmpleados'];

	$whereActivos='';
	if($tipo=='ACTIVOS'){
		$whereActivos=" AND activo='SI'";
	}

	$consulta=consultaBD("SELECT codigo FROM clientes_centros WHERE codigoCliente='$codigoCliente' $whereActivos");
	while($centro=mysql_fetch_assoc($consulta)){

		$codigoCentro=$centro['codigo'];

		$res=$res && consultaBD("INSERT INTO centros_trabajo_en_ofertas VALUES(NULL,$codigoOferta,$codigoCentro);");

	}

	return $res;
}


function eliminaOferta(){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();

    for($i=0;isset($datos['codigo'.$i]);$i++){
		$imagenes=consultaBD("SELECT * FROM ofertas_ficheros WHERE codigoOferta='".$datos['codigo'.$i]."';");
		while($imagen=mysql_fetch_assoc($imagenes)){
        	if($imagen['fichero'] != '' && $imagen['fichero'] != 'NO'){
            	unlink('../documentos/ofertas/'.$imagen['fichero']);
        	}
    	}
        $consulta=consultaBD("DELETE FROM ofertas WHERE codigo='".$datos['codigo'.$i]."';");
        if(!$consulta){
            $res=false;
            echo mysql_error();
        }
    }
    cierraBD();

    return $res;
}


//Fin parte de gestión Ofertas