<?php
  $seccionActiva=5;
  include_once('../cabecera.php');
  
  $res=operacionesVisitas();
  $where='WHERE clientes.codigo IN (SELECT clientes.codigo FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE activo="SI" AND contratos.enVigor="SI" AND clientes.eliminado="NO")';
  $texto='';
  $boton="<a href='".$_CONFIG['raiz']."visitas_seguridad/index.php?baja' class='shortcut'><i class='shortcut-icon icon-arrow-down'></i><span class='shortcut-label'>De clientes de baja</span> </a>";
  if(isset($_GET['baja'])){
    $where='WHERE clientes.codigo NOT IN (SELECT clientes.codigo FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE activo="SI" AND contratos.enVigor="SI" AND clientes.eliminado="NO")';
    $texto=' de clientes de baja';
    $boton="<a href='".$_CONFIG['raiz']."visitas_seguridad/index.php?' class='shortcut'><i class='shortcut-icon icon-arrow-left'></i><span class='shortcut-label'>Volver</span> </a>";
  }
  $estadisticas=estadisticasVisitas($where);
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Resultados de las visitas de seguridad<?php echo $texto;?>:</h6>
                  <div id="big_stats" class="cf">
                  
                    <canvas id="pie-chart" class="chart-holder" height="250" width="538"></canvas>
                  
                    <div class="leyenda">
                      <span class="grafico grafico12"></span>SI: <span id="valor4"></span><br>
                      <span class="grafico grafico9"></span>NO: <span id="valor1"></span><br>
                      <span class="grafico grafico10"></span>NP: <span id="valor2"></span><br>
                    </div>
                    
                    <!-- .stat --> 
                  </div>
                </div>            
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Visitas de seguridad</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="<?php echo $_CONFIG['raiz']; ?>visitas_seguridad/gestion.php" class="shortcut"><i class="shortcut-icon icon-eye"></i><span class="shortcut-label">Nueva visita de seguridad</span> </a>
                <?php echo $boton;?>
                		<a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Visitas de seguridad<?php echo $texto;?></h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table id='tabla1' class="table table-striped table-bordered datatable tablaNo">
              <thead>
                <tr>
                  <th> Fecha </th>
                  <th> Cliente </th>
                  <th> Centro </th>
                  <th> Porcentaje SI </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todoNo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeVisitas($where);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
    </div>
        
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/chart.js" type="text/javascript"></script>
<script type="text/javascript">    
    <?php
      $datos=generaDatosGraficoVisitas($where);
    ?>
    var pieData = [
        {
            value: <?php echo $datos['SI']; ?>,
            color: "#B02B2C"
        },
        {
            value: <?php echo $datos['NO']; ?>,
            color: "#6BBA70"
        },
        {
            value: <?php echo $datos['NP']; ?>,
            color: "#356AA0"
        }
        
      ];

    var myPie = new Chart(document.getElementById("pie-chart").getContext("2d")).Pie(pieData);
    
    $("#valor4").text("<?php echo $datos['SI']; ?>");
    $("#valor1").text("<?php echo $datos['NO']; ?>");
    $("#valor2").text("<?php echo $datos['NP']; ?>");
    
</script>


<?php include_once('../pie.php'); ?>