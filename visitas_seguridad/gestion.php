<?php
  $seccionActiva=5;
  include_once("../cabecera.php");
  gestionVisitas();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src='../../api/js/firma/jquery.signaturepad.js'></script>
<script src='../../api/js/firma/assets/json2.min.js'></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});


		oyenteCentros($('#codigoCliente').val());
		$('#codigoCliente').change(function(){
			oyenteCentros($(this).val());
		});

		inicializaFirma();
	});

function oyenteCentros(codigoCliente){
	if(codigoCliente != 'NULL'){
		var consulta=$.post('obtieneCentros.php?',{'codigoCliente':codigoCliente});

		consulta.done(function(respuesta){
			if(respuesta=='fallo'){
				alert('Ha habido un problema al obtener los centros del cliente.\nInténtelo de nuevo en unos segundos.\nSi el 	problema persiste, contacte con webmaster@qmaconsultores.com');
			}
			else{
				$('#centro').html(respuesta);
				$('#centro').selectpicker('refresh');
			}
		});
	}
}

function inicializaFirma(){
	$('.output').each(function(){
		var id=$(this).attr('id');
		var firma = $(this).val().trim(); 
		if(firma==''){//inicialización
			$('.'+id).signaturePad({drawOnly:true,lineTop:'500',penColour:'000000',validateFields:false});
		}
		else{
			$('.'+id).signaturePad({drawOnly:true,lineTop:'500',penColour:'000000',validateFields:false}).regenerate(firma.toLowerCase());
		}
	});
}
	
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>