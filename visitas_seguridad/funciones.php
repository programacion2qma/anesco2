<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión Auditorias


function operacionesVisitas(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('visitas');
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=insertaDatos('visitas');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('visitas');
	}

	mensajeResultado('codigoCliente',$res,'Visita de seguridad');
    mensajeResultado('elimina',$res,'Visita de seguridad', true);
}


function gestionVisitas(){
	operacionesVisitas();
	
	abreVentanaGestionConBotones('Cuestionario de Visita de seguridad','index.php');
	$datos=compruebaDatos('visitas');
	creaPestaniasAuditoria();
		abrePestania(1,'active');
			campoSelectConsulta('codigoCliente','Cliente','SELECT codigo, CONCAT(EMPNOMBRE," (",EMPMARCA,")") as texto FROM clientes WHERE activo="SI"',$datos);
			campoSelectConsulta('centro','Centro supervisado','SELECT codigo, CONCAT(direccion," - ",localidad," CP: ",cp," (",provincia,")") AS texto FROM clientes_centros WHERE codigoCliente='.$datos['codigoCliente'],$datos,'span8');
			campoFecha('fecha','Fecha',$datos);
		cierraPestania();
		abrePestania(2);
			creaTablaVisitas();
            	creaEncabezadoTablaVisitas('ESPACIOS DE TRABAJO',4);
                      	creaPreguntaTablaVisitas($datos,'El suelo se mantiene limpio, uniforme y regular',1);
                      	creaPreguntaTablaVisitas($datos,'Están delimitadas y libres de obstáculos las zonas de paso',2);
                      	creaPreguntaTablaVisitas($datos,'Si existen aberturas, están protegidas',3);
                creaEncabezadoTablaVisitas('ORDEN Y LIMPIEZA',4);
                      	creaPreguntaTablaVisitas($datos,'Las zonas de paso, vías de circulación y salidas de evacuación permanecen libres de obstáculos',4);
                      	creaPreguntaTablaVisitas($datos,'Las estanterías son estables, resistentes y están arriostradas a la pared',5);
                      	creaPreguntaTablaVisitas($datos,'El almacenamiento de los materiales se realiza en un lugar destinado a ello',6);
                creaEncabezadoTablaVisitas('INCENDIOS Y EXPLOSIONES',5);
                      	creaPreguntaTablaVisitas($datos,'Los equipos de protección contra incendios están señalizados',7);
                      	creaPreguntaTablaVisitas($datos,'Existen salidas de emergencia señalizadas y libres de obstáculos',8);
                      	creaPreguntaTablaVisitas($datos,'Existe iluminación de emergencia ',9);
                      	creaPreguntaTablaVisitas($datos,'Los productos inflamables se almacenan en armarios o locales protegidos',10);
                creaEncabezadoTablaVisitas('INSTALACIÓN ELÉCTRICA',3);
                      	creaPreguntaTablaVisitas($datos,'Los cuadros eléctricos están debidamente señalizados y siempre cerrados',11);
                      	creaPreguntaTablaVisitas($datos,'La instalación dispone de protección contra sobreintensidades y cortocircuitos.',12);
                creaEncabezadoTablaVisitas('ESCALERAS FIJAS',3);
                      	creaPreguntaTablaVisitas($datos,'Las escaleras fijas disponen de barandilla, listón intermedio y/o rodapié. O de pasamanos en su lado cerrado',13);
                      	creaPreguntaTablaVisitas($datos,'Los peldaños son uniformes y antideslizantes.',14);
                creaEncabezadoTablaVisitas('PRODUCTOS QUÍMICOS',4);
                      	creaPreguntaTablaVisitas($datos,'Están almacenados un lugar específico para ello, lejos de fuentes de calor.',15);
                      	creaPreguntaTablaVisitas($datos,'Se dispone de las fichas de seguridad de los productos',16);
                      	creaPreguntaTablaVisitas($datos,'Se mantienen los envases originales de los productos correctamente etiquetados',17);
                creaEncabezadoTablaVisitas('ILUMINACIÓN',2);
                      	creaPreguntaTablaVisitas($datos,'La iluminación es adecuada en todos los lugares de trabajo y zonas de paso habitual',18);
                creaEncabezadoTablaVisitas('TEMPERATURA Y HUMEDAD',7);
                      	creaPreguntaTablaVisitas($datos,'La temperatura es adecuada: entre 17º-27º en trabajos sedentarios y entre 14º-25º para trabajos ligeros',19);
                      	creaTituloTablaVisitas('En caso de trabajos a la intemperie:');
                      	creaPreguntaTablaVisitas($datos,'frío, se dispone de equipos de protección adecuados',20);
                      	creaPreguntaTablaVisitas($datos,' calor, se dispone de agua para beber frecuentemente y de tiempos de descanso',21);
                      	creaTituloTablaVisitas('');
                      	creaPreguntaTablaVisitas($datos,'La humedad relativa es adecuada, entre 30%-70%.',22);
            cierraTablaVisitas();
		cierraPestania();
		abrePestania(3);
			campoFirma($datos,'firmaTecnico','Firma del técnico');
			campoFirma($datos,'firmaResponsable','Firma del Responsable de PRL de la Empresa');
		cierraPestania();
	
		cierraVentanaGestion('index.php', true);

}

function creaPestaniasAuditoria(){
	echo "<div class='tabbable tabs-lopd'>
	       	<ul class='nav nav-tabs tabs-small'>
		        <li class='active'><a href='#1' class='noAjax' data-toggle='tab'>Datos</a></li>
		        <li><a href='#2' class='noAjax' data-toggle='tab'>Cuestionario</a></li>
		        <li><a href='#3' class='noAjax' data-toggle='tab'>Firmas</a></li>
	       	</ul>

       <div class='tab-content'>";
}


//Parte de campos personalizados


//Fin parte de campos personalizados

function imprimeVisitas($where){
	global $_CONFIG;

	conexionBD();
	/*$where = '';
	if(!!$cliente){
		$where = 'AND codigoCliente='.$cliente;
	}
	if($_SESSION['tipoUsuario'] == 'ADMIN'){
		$sql="SELECT a.codigo AS codigo, a.fechaAuditoria AS fecha, c.razon_s AS cliente, a.auditor AS auditor FROM auditorias_internas a INNER JOIN clientes c ON a.codigoCliente=c.codigo WHERE finalizado LIKE '".$finalizado."' ".$where." ORDER BY a.fechaAuditoria;";
	} else {
		$director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],true, true);
		if($director['director'] == 'SI'){
			$sql="SELECT a.codigo AS codigo, a.fechaAuditoria AS fecha, c.razon_s AS cliente, a.auditor AS auditor FROM auditorias_internas a INNER JOIN clientes c ON a.codigoCliente=c.codigo WHERE finalizado LIKE '".$finalizado."' ".$where." AND c.empleado IN (SELECT codigo FROM usuarios WHERE tipo LIKE '".$director['tipo']."') ORDER BY a.fechaAuditoria;";
		} else {
			$sql="SELECT a.codigo AS codigo, a.fechaAuditoria AS fecha, c.razon_s AS cliente, a.auditor AS auditor FROM auditorias_internas a INNER JOIN clientes c ON a.codigoCliente=c.codigo WHERE finalizado LIKE '".$finalizado."' ".$where." AND c.empleado LIKE ".$_SESSION['codigoU']." ORDER BY a.fechaAuditoria;";
		}
	}*/
	$where=defineWhereEjercicio($where,'visitas.fecha');
	$consulta=consultaBD("SELECT visitas.*, EMPNOMBRE, EMPMARCA FROM visitas INNER JOIN clientes ON visitas.codigoCliente=clientes.codigo ".$where." ORDER BY visitas.fecha DESC");
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['EMPNOMBRE']." (".$datos['EMPNOMBRE'].")</td>
				<td>".$datos['centro']."</td>
				<td class='centro'>".obtenerPorcentaje($datos)." % </td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	cierraBD();
}

function obtenerPorcentaje($datos){
	$afirmativas=0;
	for($i=1;$i<=22;$i++){
		if($datos['pregunta'.$i] == 'SI'){
			$afirmativas++;
		}
	}
	return round(($afirmativas*100)/22,2);
}

function obtieneCentros(){
   $codigoCliente=$_POST['codigoCliente'];
   $res="";

   $consulta=consultaBD('SELECT codigo, CONCAT(direccion," - ",localidad," CP: ",cp," (",provincia,")") AS texto FROM clientes_centros WHERE codigoCliente='.$codigoCliente,true);
   while($centro=mysql_fetch_assoc($consulta)){
		$res.="<option value='".$centro['codigo']."'>".$centro['texto']."</option>";
   }
   if($res==''){
       $res="<option value='NULL'></option>";
   }
   echo $res;
}

function generaDatosGraficoVisitas($where){
	$datos=array('SI'=>0,'NO'=>0,'NP'=>0);
	
	conexionBD();
	$where=defineWhereEjercicio($where,'visitas.fecha');
	$consulta=consultaBD("SELECT * FROM visitas INNER JOIN clientes ON visitas.codigoCliente=clientes.codigo ".$where);
	while($visita=mysql_fetch_assoc($consulta)){
		for($i=1;$i<=22;$i++){
			$datos[$visita['pregunta'.$i]]++;
		}
	}
	cierraBD();

	return $datos;
} 

function estadisticasVisitas($where){
  $where=defineWhereEjercicio($where,'visitas.fecha');
  return consultaBD('SELECT COUNT(visitas.codigo) AS total FROM visitas INNER JOIN clientes ON visitas.codigoCliente=clientes.codigo '.$where,true,true);
}

//Fin parte de gestión Auditorías
