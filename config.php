<?php

// =========================================================== Parámetros de configuración ====================================================================================

//Inicialización del array de configuración
$_CONFIG=array();

//Parámetros de conexión a la BDD
$_CONFIG['servidorBD']='localhost';

//$_CONFIG['usuarioBD']='anesco2';

//$_CONFIG['claveBD']='Writemaster7';

$_CONFIG['usuarioBD']='root';

$_CONFIG['claveBD']='root';

$_CONFIG['nombreBD']='anesco2';


$_CONFIG['usuarioBDFormacion']='root';

$_CONFIG['claveBDFormacion']='root';

$_CONFIG['nombreBDFormacion']='anescoformacion';


$_CONFIG['historicoSQL']=true;

//Nombre de cookie de sesión
$_CONFIG['nombreCookie']='sesionAnesco';

//Perfiles de usuario
$_CONFIG['perfiles']=array('ADMIN'=>'Administrador','TECNICO'=>'Técnico','COMERCIAL'=>'Comercial','COLABORADOR'=>'colaborador','CLIENTE'=>'cliente','EMPLEADO'=>'Empleado','FACTURACION'=>'Facturación','MEDICO'=>'Médico de empresa','ENFERMERIA'=>'Enfermería','ADMINISTRACION'=>'Administrativo');
$_CONFIG['tiposUsuario']=array('ADMIN','TECNICO','COMERCIAL','COLABORADOR','CLIENTE','EMPLEADO','FACTURACION','MEDICO','ENFERMERIA','ADMINISTRACION');

//Parámetros de maquetación
$_CONFIG['textoAcceso']='Acceso Privado';

$_CONFIG['tituloGeneral']='Software PRL';

$_CONFIG['titulosSecciones']=array(
									'Inicio',
									'Clientes',
									'Documentos LOPD',
									'Tareas',
									'Satisfacción de clientes',
									'Incidencias',
									'Usuarios',
									'Incidencias de software',
									'Declaración de ficheros',
									'Auditorías',
									'Empleados',
									'Contratos',
									'Actividades',
									'Formas de pago',
									'Anesco',
									'Citas',
									'Correos',
									'Documentos internos'
							);//15

$_CONFIG['altLogo']='Software PRL';

$_CONFIG['tituloSoftware']='Software de Gestión PRL';

$_CONFIG['textoPie']='<a href="http://www.qmaconsultores.com" target="_blank">QMA Consultores</a>';

//Parámetros de estilos
$_CONFIG['colorPrincipal']='#61B4DD';

$_CONFIG['colorSecundario']='#075581';

$_CONFIG['colorPrincipalRGB']='rgba(0,159,147,0.2)';//126,178,22. Se puede usar la utilidad online http://www.javascripter.net/faq/hextorgb.htm para conversión desde hexadecimal

//Para enlaces/rutas

$_CONFIG['raiz']='/anesco2/';

$_CONFIG['enlaceCuestionario']='https://crmparapymes.com.es'.$_CONFIG['raiz'].'cuestionario-de-satisfaccion/';

//Para módulo de incidencias de software

$_CONFIG['codigoIncidenciasSoftware']='320';

$_CONFIG['nombreIncidenciasSoftware']='Anesco 2';