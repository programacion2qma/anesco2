<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión de asistencias


function operacionesAsistencias(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('asistencia_a_inspecciones');
	}
	elseif(isset($_POST['fecha'])){
		$res=insertaDatos('asistencia_a_inspecciones');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('asistencia_a_inspecciones');
	}

	mensajeResultado('fecha',$res,'Asistencia');
    mensajeResultado('elimina',$res,'Asistencia', true);
}


function gestionAsistencia(){
	operacionesAsistencias();
	
	abreVentanaGestionConBotones('Gestión de Asistencia a Inspección','index.php','span3');
	$datos=compruebaDatos('asistencia_a_inspecciones');
	
	campoFecha('fecha','Fecha',$datos);
	campoSelectConsulta('codigoCliente','Cliente',"SELECT codigo, CONCAT(EMPNOMBRE,' (',EMPMARCA,')') AS texto FROM clientes WHERE activo='SI';",$datos);

	cierraColumnaCampos();
	abreColumnaCampos('span3');

	campoSelectConsulta('codigoUsuario','Técnico asignado',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE tipo='TECNICO' ORDER BY nombre,apellidos;",$datos);
	$tipos=array('Desplazamiento a la empresa','Preparación a la oficina','Desplazamiento a Inspección');
	campoSelect('tipoAsistencia','Tipo de asistencia',$tipos,$tipos,$datos,'selectpicker span3 show-tick','');

	cierraColumnaCampos();
	abreColumnaCampos('span10');

	areaTexto('observaciones','Observaciones',$datos,'observacionesAsistencia');
	campoTexto('horasDedicadas','Horas dedicadas',$datos,'input-mini pagination-right');

	cierraVentanaGestion('index.php',true);
}


function imprimeAsistencias($where){
	global $_CONFIG;

	$where=defineWhereEjercicio($where,array('asistencia_a_inspecciones.fecha'));

	$consulta=consultaBD("SELECT asistencia_a_inspecciones.codigo, EMPNOMBRE AS empresa, 
						  tipoAsistencia, horasDedicadas, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS tecnico,
						  asistencia_a_inspecciones.fecha
						  FROM asistencia_a_inspecciones LEFT JOIN clientes ON asistencia_a_inspecciones.codigoCliente=clientes.codigo
						  LEFT JOIN usuarios ON asistencia_a_inspecciones.codigoUsuario=usuarios.codigo ".$where);

	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['empresa']."</td>
				<td>".$datos['tecnico']."</td>
				<td>".$datos['tipoAsistencia']."</td>
				<td>".$datos['horasDedicadas']."</td>
				<td class='centro'>
					<a href='".$_CONFIG['raiz']."asistencia/gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a> 
				</td>
				<td><input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'></td>

			</tr>";
	}

}


function estadisticasAsistencias($where){
	$res=consultaBD('SELECT COUNT(asistencia_a_inspecciones.codigo) AS total FROM asistencia_a_inspecciones LEFT JOIN clientes ON asistencia_a_inspecciones.codigoCliente=clientes.codigo '.$where,true,true);

	return $res;
}
//Fin parte de gestión de asistencias