<?php
  $seccionActiva=5;
  include_once("../cabecera.php");
  gestionAsistencia();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	});
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>