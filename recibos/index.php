<?php
  $seccionActiva=32;
  include_once('../cabecera.php');
  
  operacionesRecibos();
  $estadisticas=consultaBD("SELECT COUNT(vencimientos_facturas.codigo) AS total FROM vencimientos_facturas INNER JOIN facturas ON vencimientos_facturas.codigoFactura=facturas.codigo WHERE tipoFactura!='ABONO' AND vencimientos_facturas.eliminado='NO' AND facturas.eliminado='NO';",true,true);
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de recibos:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-ticket"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Recibo/s registrado/s</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Recibos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                  <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-times-circle"></i><span class="shortcut-label">Eliminar</span> </a>
                  <a href="eliminados.php" class="shortcut"><i class="shortcut-icon icon-trash-o"></i><span class="shortcut-label">Eliminados</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Recibos registrados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered tablaTextoPeque datatable" id="tablaRecibos">
                <thead>
                  <tr>
                    <th> Fecha </th>
                    <th> Factura </th>
                    <th> Cliente </th>
                    <th> Contrato </th>
                    <th> Importe </th>
                    <th> Devolución </th>
                    <th> Motivo </th>
                    <th> Remesa </th>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                      imprimeRecibos();
                    ?>
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>
<script type="text/javascript" src="../../api/js/filtroTabla.js"></script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>