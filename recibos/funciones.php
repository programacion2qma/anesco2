<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de recibos

function operacionesRecibos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$_POST['importe']=formateaNumeroWeb($_POST['importe'],true);
		$_POST['gastosDevolucion']=formateaNumeroWeb($_POST['gastosDevolucion'],true);
		$res=actualizaDatos('vencimientos_facturas');
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoVencimientos($_POST['elimina']);
	}

	mensajeResultado('codigo',$res,'Recibo', true);
    mensajeResultado('elimina',$res,'Recibo', true);
}


function cambiaEstadoEliminadoVencimientos($estado){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],'vencimientos_facturas',false);
	}
	cierraBD();

	return $res;
}

function imprimeRecibos($eliminados='NO'){
	global $_CONFIG;

	$operadorEliminado='AND';
	if($eliminados=='SI'){
		$operadorEliminado='OR';
	}
	
	$consulta=consultaBD("SELECT vencimientos_facturas.fechaVencimiento, vencimientos_facturas.codigo, EMPNOMBRE,
						  vencimientos_facturas.importe, fechaDevolucion, motivoDevolucion, facturas.activo, clientes.codigo AS codigoCliente, codigoRemesa,
						  remesas.fecha AS fechaRemesa, EMPCP, EMPID, contratos.fechaInicio, contratos.codigoInterno, contratos.codigo AS codigoContrato,
						  facturas.numero, series_facturas.serie

						  FROM vencimientos_facturas INNER JOIN facturas ON vencimientos_facturas.codigoFactura=facturas.codigo
						  LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
						  LEFT JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
						  LEFT JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
						  LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
						  LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
						  LEFT JOIN vencimientos_en_remesas ON vencimientos_facturas.codigo=vencimientos_en_remesas.codigoVencimiento AND codigoRemesa IS NOT NULL
						  LEFT JOIN remesas ON vencimientos_en_remesas.codigoRemesa=remesas.codigo
						  WHERE tipoFactura!='ABONO' AND (vencimientos_facturas.eliminado='$eliminados' $operadorEliminado facturas.eliminado='$eliminados');",true);
	
	while($datos=mysql_fetch_assoc($consulta)){
		$enlaceRemesa=obtieneEnlaceRemesaRecibo($datos['codigoRemesa'],$datos['fechaRemesa']);

		$factura=$datos['serie']."/".$datos['numero'];
		if($datos['numero']==0){
			$factura='PROFORMA';
		}

		echo "<tr>
				<td>".formateaFechaWeb($datos['fechaVencimiento'])."</td>
				<td>".$factura."</td>
				<td><a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."' class='noAjax' target='_blank'>".$datos['EMPNOMBRE']."</a></td>
				<td><a href='../contratos/gestion.php?codigo=".$datos['codigoContrato']."' class='noAjax' target='_blank'>".formateaReferenciaContrato($datos,$datos)."</td>
				<td><div class='pagination-right nowrap'>".formateaNumeroWeb($datos['importe'])." €</div></td>
				<td>".formateaFechaWeb($datos['fechaDevolucion'])."</td>
				<td>".$datos['motivoDevolucion']."</td>
				<td>".$enlaceRemesa."</td>
				<td class='centro'>
	                <a href='".$_CONFIG['raiz']."recibos/gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio noAjax'><i class='icon-search-plus'></i> Detalles</i></a>
	            </td>
	            <td class='centro'>
	                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	            </td>
            </tr>";
		
	}
}


function obtieneEnlaceRemesaRecibo($codigoRemesa,$fechaRemesa){
	$res='<div class="centro">-</div>';

	if($codigoRemesa!=NULL){
		$res="<div class='centro'><a href='../remesas/gestion.php?codigo=".$codigoRemesa."' class='noAjax' target='_blank'>".formateaFechaWeb($fechaRemesa)."</a></div>";
	}

	return $res;
}


function gestionRecibo(){
	operacionesRecibos();

	abreVentanaGestionConBotones('Gestión de recibos','index.php','span3');
	$datos=compruebaDatosRecibos();

	campoDato('Número recibo',$datos['codigo']);
	campoDato('Factura asociada',$datos['factura']);
	campoFormaPago($datos);
	campoFecha('fechaVencimiento','Fecha',$datos);
	campoTexto('concepto','Concepto',$datos);
	
	cierraColumnaCampos();
	abreColumnaCampos();

	campoTextoSimbolo('importe','Importe','€',formateaNumeroWeb($datos['importe']));
	campoSelectEstadoVencimiento($datos,'',0);
	campoFecha('fechaDevolucion','Fecha devolución',$datos);
	campoTextoSimbolo('gastosDevolucion','Gastos devolución','€',formateaNumeroWeb($datos['gastosDevolucion']));
	campoSelect('motivoDevolucion','Motivo devolución',array('','Adeudo duplicado, indebido, erróneo o faltan datos','Aplicación R.D. 338/90, sobre el NIF','Incorriente','No domiciliado o cuenta cancelada','Oficina domiciliataria inexistente','Por orden del cliente: disconformidad con el importe','Por orden del cliente: error o baja en la domiciliación'),array('','ADEUDO DUPLICADO, INDEBIDO, ERRÓNEO O FALTAN DATOS','APLICACIÓN R.D. 338/90, SOBRE EL NIF','INCORRIENTE','NO DOMICILIADO O CUENTA CANCELADA','OFICINA DOMICILIATARIA INEXISTENTE','POR ORDEN DEL CLIENTE: DISCONFORMIDAD CON EL IMPORTE','POR ORDEN DEL CLIENTE: ERROR O BAJA EN LA DOMICILIACIÓN'),$datos);

	cierraVentanaGestion('index.php',true);
}

function compruebaDatosRecibos(){
	$datos=compruebaDatos('vencimientos_facturas');
	if($datos){
		$consulta=consultaBD("SELECT facturas.codigo, CONCAT(serie,'-',numero,' del ',DATE_FORMAT(facturas.fecha,'%d/%m/%Y'),' para el cliente ',EMPNOMBRE) AS factura FROM facturas LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo WHERE facturas.codigo=".$datos['codigoFactura'],true,true);
		
		if($consulta['factura']==NULL){
			$datos['factura']="<a href='../proformas/gestion.php?codigo=".$consulta['codigo']."' class='noAjax' target='_blank'>PROFORMA</a>";
		}
		else{
			$datos['factura']="<a href='../facturas/gestion.php?codigo=".$consulta['codigo']."' class='noAjax' target='_blank'>".$consulta['factura']."</a>";
		}
		
	}

	return $datos;
}
//Fin parte de recibos