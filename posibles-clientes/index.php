<?php
  $seccionActiva=1;
  include_once('../cabecera.php');
  
  $res=operacionesClientes();
  $estadisticas=estadisticasClientesRestrict();

 /* $i=1;

  $campos=array("EMPID","EMPCIF","EMPNOMBRE","EMPMARCA","EMPNTRAB","EMPDIR","EMPLOC","EMPCP","EMPPROV","EMPDIR2","EMPLOC2","EMPCP2","EMPPC","EMPTEL1","EMPTEL2","EMPEMAIL","EMPPC2","EMPTEL3","EMPTEL4","EMPEMAIL2","EMPRESPNOMBRE","EMPRESPCARGO","EMPRESPDNI","EMPMUTUA","EMPSPA","EMPASESORIA","EMPRESPONCONTRATO","EMPFECHAFICHA","EMPACTIVIDAD","EMPRIESGO","EMPCNAE","EMPANEXO","EMPCENTRO1","EMPCENTRO2","EMPCENTRO3","EMPCENTRO4","EMPPDTERESP","EMPPPTADO","EMPCONTRATADO","EMPIBAN","EMPCLAVE
","EMPUSUIDMOD","EMPFHORAMOD","EMPPROV2","imagen","EMPSADMIN","EMPSTEC","EMPSPRL");
  $indiceCampo=0;

  $empresas=array();
  $indiceEmpresas=0;

  $boolEmpresa = true;
  $contadorEempresa=0;

  $file = fopen("../documentacion/listadoEmpresas.xml", "r") or exit("Unable to open file!");
  //Output a line of the file until the end is reached
  while(!feof($file))
  {
    $linea =  fgets($file);
    if($i>=93 && $boolEmpresa){
      $linea = str_replace('<Cell><Data ss:Type="String">', '', $linea);
      $linea = str_replace('<Cell><Data ss:Type="Number">', '', $linea);
      $linea = str_replace('</Data></Cell>', '', $linea);
      $empresas[$indiceEmpresas][$campos[$indiceCampo]] = $linea;

      //echo "empresas[".$indiceEmpresas."][".$campos[$indiceCampo]."] = ".$linea."<br/>";
   
      $indiceCampo++;
      if($indiceCampo>47){
        $indiceCampo=0;
        $indiceEmpresas++;
        $boolEmpresa = false;
      }
    } else if (!$boolEmpresa){
      $contadorEempresa++;
      if($contadorEempresa == 2){
        $contadorEempresa=0;
        $boolEmpresa=true;
      }
    }
    $i++;
  }
  fclose($file);

  for($i=0;$i<count($empresas);$i++){
    $fecha = fechaBD();
    $query ="INSERT INTO clientes VALUES(NULL,'".$fecha."'";

    for($j=0;$j<48;$j++){
      //print_r('>'.utf8_encode($empresas[$i][$campos[$j]]).'<<br/>');
  
      if($j==42){
        if($empresas[$i][$campos[$j]] != ''){
          $fechaApoyo=split('/', $empresas[$i][$campos[$j]]);
          $fechaApoyo[2] = substr($fechaApoyo[2], 0, 4);
          $empresas[$i][$campos[$j]]=trim($fechaApoyo[2]).'-'.trim($fechaApoyo[1]).'-'.trim($fechaApoyo[0]);
        }
      }

      //print_r('>'.utf8_encode($empresas[$i][$campos[$j]]).'<<br/>');
      if ($empresas[$i][$campos[$j]] != NULL){
        $query.= ", '".$empresas[$i][$campos[$j]]."'";
      } 
    }

    $query.= ",'NULL','NULL');<br/>";

    echo $query;
  }*/
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de gestión Clientes:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-question-circle"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Posibles Clientes registrados</div>
                     <!--div class="stat"> <i class="icon-send"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Enviados a la agencia</div-->
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Clientes</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="<?php echo $_CONFIG['raiz']; ?>posibles-clientes/gestion.php" class="shortcut noAjax"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nuevo posible cliente</span> </a>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Posibles Clientes registrados</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Nº de empresa </th>
                  <th> Estado de pago </th>
                  <th> Razón Social </th>
                  <th> Nombre comercial </th>
                  <th> Código postal </th>
                  <th> Estado </th>
                  <th> Vencimiento </th>
                  <th> Comercial </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeClientes();
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>