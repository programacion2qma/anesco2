<?php
  $seccionActiva=1;
  include_once("../cabecera.php");
  gestionClientes();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>

<script src='../../api/js/firma/jquery.signaturepad.js'></script>
<script src='../../api/js/firma/assets/json2.min.js'></script>

<script src="../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>/js/enlacesSoftwareExternos.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>/js/creaFormulario.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>/js/validador.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/campoLogo.js" type="text/javascript"></script>

<script src="../js/validacionClientes.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script type="text/javascript">
	
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
		$('.enlaceExternoMapa').unbind();
		$(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
		//$('#asignAtencion').css('display','none');
		
		$('#usuario,#clave').attr('autocomplete','off');

		$('#asignEmpleado input[type=radio]').change(function(){
			$('.asign').css('display','none');
			$('.asign select').attr("name","noname");
			$('#asign'+this.value).css('display','inline');	
			$('#asign'+this.value+' select').attr('name','empleado');	
		});
		
		$('#asignColaborador input[type=radio]').change(function(){
			if(this.value == 'SI'){
				$('#colaboradores').css('display','inline');
			} else {
				$('#colaboradores').css('display','none');
			}
		});
		
		$("#EMPCIF").change(function(){
    		var res=isValidCifNif($(this).val());
    		if(res){
    			$('#usuario').val($(this).val());
    			$(this).css('border','1px solid #cccccc');
  			} else {
      			$(this).css('border','1px solid red');
  			}
		});

		
		$("#nif").focusout(function(){
    		isValidNif($(this).val());
		}); 

		$('#telefono, #fax').attr('maxlength','9');

		$('.enlaceExternoMapa').click(function(e){
      		e.preventDefault();
      		accedeSoftwareExterno($(this).attr('href'));
    	});

    	/*direccionesCentros($('#EMPCENTROS').val());
    	$('#EMPCENTROS').change(function(){
    		direccionesCentros($(this).val());
    	});
    	$('.btnEliminarCentro').unbind();
    	$('.btnEliminarCentro').click(function(e){
    		e.preventDefault();
    		eliminarCentro('#'+$(this).attr('id').replace('eliminaCentro','divDireccion'))
    	});*/


    	$('#generarClave').click(function(){
    		var caracteres = '$+=?@_23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz';
    		var pass='';
    		for(i=0;i<8;i++){
    			var indice=Math.floor((Math.random() * caracteres.length-1) + 1);
    			pass+=caracteres[indice];
    		}
    		$('#clave').val(pass);
    	});

    	//segundo();   
	});

function inicializaFirma(selector){
	var firma=$('#'+selector).val().trim();

	if(firma==''){
		$('#contenedor-'+selector).signaturePad({drawOnly:true,lineTop:'500',penColour:'000000',validateFields:false,output:'#'+selector});
	}
	else{
		$('#contenedor-'+selector).signaturePad({drawOnly:true,lineTop:'500',penColour:'000000',validateFields:false,output:'#'+selector}).regenerate(firma);
	}
}

function cargaClick(i){
	$('#map'+i).text('');
	cargarMapa($('#direccion'+i).val(),$('#cp'+i).val());
}

function segundo(){
	var i=1;
	while($('#direccion'+i).length){
		if($('#direccion'+i).val() != '' && $('#cp'+i).val() != ''){
			cargarMapa($('#direccion'+i).val(),$('#cp'+i).val());
		}
		i++;
	}

}

function cargarMapa(direccion,cp){
	var address = direccion+' '+cp;
	// Creamos el Objeto Geocoder
	var geocoder = new google.maps.Geocoder();
	// Hacemos la petición indicando la dirección e invocamos la función
	// geocodeResult enviando todo el resultado obtenido
    if(address.length!=0){
	   		geocoder.geocode({ 'address': address}, geocodeResult);
    }
}

function geocodeResult(results, status) {
    // Verificamos el estatus
    if (status == 'OK') {
        // Si hay resultados encontrados, centramos y repintamos el mapa
        // esto para eliminar cualquier pin antes puesto
        var mapOptions = {
            center: results[0].geometry.location,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var divMapa;

        $('.map').each(function(){
        	if($(this).html()==''){
        		divMapa=$(this).get(0);
        		return false;
        	}
        });

        map = new google.maps.Map(divMapa, mapOptions);
        // fitBounds acercará el mapa con el zoom adecuado de acuerdo a lo buscado
        map.fitBounds(results[0].geometry.viewport);
        // Dibujamos un marcador con la ubicación del primer resultado obtenido
        var markerOptions = { position: results[0].geometry.location }
        var marker = new google.maps.Marker(markerOptions);

        marker.setMap(map);

    } else {
        // En caso de no haber resultados o que haya ocurrido un error
        // lanzamos un mensaje con el error
        if($('#codigo').length && status=='ZERO_RESULTS'){
        	status='El campo Dirección está vacío o Google Maps no localiza la dirección proporcionada';
        }
        alert("El mapa no se pudo cargar debido a: " + status);
    }
}

/*function direccionesCentros(numero){
	if(numero>0){
		for(var i=1;i<=numero;i++){
			var campo = '<div class="direcciones" style="border-bottom:1px solid #000;margin-bottom:20px;float:left;width:100%;">';
			campo+='<fieldset class="span3">';
			var name='direccion'+i;
			campo+='<div class="control-group"><label class="control-label" for="'+name+'">Dirección:</label><div class="controls"><input id="'+name+'" class="input-large" name="'+name+'" value="" type="text"></div></div>';
			name='localidad'+i;
			campo+='<div class="control-group"><label class="control-label" for="'+name+'">Localidad:</label><div class="controls"><input id="'+name+'" class="input-large" name="'+name+'" value="" type="text"></div></div>';
			campo+='</fieldset>';

			campo+='<fieldset class="span3">';
			name='cp'+i;
			campo+='<div class="control-group"><label class="control-label" for="'+name+'">CP:</label><div class="controls"><input id="'+name+'" class="input-mini" name="'+name+'" value="" type="text"></div></div>';
			name='provincia'+i;
			campo+='<div class="control-group"><label class="control-label" for="'+name+'">Provincia:</label><div class="controls"><input id="'+name+'" class="input-large" name="'+name+'" value="" type="text"></div></div>';
			campo+='</fieldset>';
			campo+="<fieldset class='sinFlotar'><button style='' type='button' class='btn btn-propio' onclick='cargaClick("+i+")'><i class='icon-google-plus'></i> Cargar mapa</button>";
			name='map'+i;
			campo+="<center><div id='"+name+"' class='map'></div></center>";
			campo+='</fieldset></div>';
			$('#divCentros').append(campo);
		}
	}else if(numero==''){
		var numDirecciones=$('.direcciones').length;

		for(var j=0;j<=numDirecciones;j++){
			$('#divCentros .direcciones').last().remove();
		}
	}
}*/

function direccionesCentros(numero){
	var i=$('.direcciones').length==0?1:$('.direcciones').length+1;
	while(numero > $('.direcciones').length){
		var campo = '<div id="divDireccion'+i+'" class="direcciones" style="border-bottom:1px solid #000;margin-bottom:20px;float:left;width:100%;">';
		var name='codigoCentro'+i;
		campo+='<fieldset class="span3"><input id="'+name+'" class="hide" name="'+name+'" value="" type="hidden">';
		var name='nombre'+i;
		campo+='<div class="control-group"><label class="control-label" for="'+name+'">Centro de trabajo:</label><div class="controls"><input id="'+name+'" class="obligatorio input-large" name="'+name+'" value="" type="text"></div></div>';
		campo+='</fieldset><fieldset class="span3">';
		var name='activo'+i;
		campo+='<div class="control-group"><label class="control-label" for="'+name+'">Activo:</label><div class="controls nowrap"><label class="radio inline"><input name="'+name+'" value="SI" checked="checked" type="radio">Si</label><label class="radio inline"><input name="'+name+'" value="NO" type="radio">No</label></div></div>';
		campo+='</fieldset><fieldset class="sinFlotar"></fieldset><fieldset class="span3">';
		var name='direccion'+i;
		campo+='<div class="control-group"><label class="control-label" for="'+name+'">Dirección:</label><div class="controls"><input id="'+name+'" class="obligatorio input-large" name="'+name+'" value="" type="text"></div></div>';
		name='localidad'+i;
		campo+='<div class="control-group"><label class="control-label" for="'+name+'">Localidad:</label><div class="controls"><input id="'+name+'" class="obligatorio input-large" name="'+name+'" value="" type="text"></div></div>';
		campo+='</fieldset>';

		campo+='<fieldset class="span3">';
		name='cp'+i;
		campo+='<div class="control-group"><label class="control-label" for="'+name+'">CP:</label><div class="controls"><input id="'+name+'" class="obligatorio input-mini" name="'+name+'" value="" type="text"></div></div>';
		name='provincia'+i;
		campo+='<div class="control-group"><label class="control-label" for="'+name+'">Provincia:</label><div class="controls"><input id="'+name+'" class="obligatorio input-large" name="'+name+'" value="" type="text"></div></div>';
		campo+="</fieldset><fieldset class='sinFlotar'></fieldset><fieldset class='span3'>";
		var name='trabajadores'+i;
		campo+='<div class="control-group"><label class="control-label" for="'+name+'">Nº de trabajadores:</label><div class="controls"><input id="'+name+'" class="input-mini pagination-right trabajadoresCentro" name="'+name+'" value="0" type="text"></div></div>';
		campo+="</fieldset><fieldset class='sinFlotar'><button style='' type='button' class='btn btn-propio' onclick='cargaClick("+numero+")'><i class='icon-google-plus'></i> Cargar mapa</button>";
		name='map'+i;
		campo+="<center><div id='"+name+"' class='map'></div></center>";
		campo+='<button id="eliminaCentro'+i+'" style="margin-bottom:10px;" class="btnEliminarCentro btn btn-propio"><i class="icon-trash"></i> Eliminar </button><br/>';
		campo+='</fieldset></div>';
		$('#divCentros').append(campo);

		$('#trabajadores'+i).change(function(){
			compruebaTrabajadores($(this));
		});

		i++;
	}
	if(numero < $('.direcciones').length){
		alert('El campo "Nº de centros de trabajo es menor" a los centros de trabajo insertados')
	}
	/*while(numero < $('.direcciones').length){
		$('#divCentros .direcciones').last().remove();
	}*/

	$('.btnEliminarCentro').unbind();
    $('.btnEliminarCentro').click(function(e){
    	e.preventDefault();
    	eliminarCentro('#'+$(this).attr('id').replace('eliminaCentro','divDireccion'))
    });
}

function eliminarCentro(id){
	$(id).remove();
	var num=$('#EMPCENTROS').val()-1<0?0:$('#EMPCENTROS').val()-1;
	$('#EMPCENTROS').val(num);
	var i=1;
	$(".direcciones").each(function( index ) {
  		var parts = this.id.match(/(\D+)(\d*)$/);
  		$(this).attr('id',(parts[1]+i));
  		$(this).find('input').attr("name", function(){
        	var parts = this.name.match(/(\D+)(\d*)$/);
            return parts[1] + i;
        }).attr("id", function(){
            return this.name;
       	});

       	$(this).find('.map').attr("id", function(){
        	var parts = this.id.match(/(\D+)(\d*)$/);
            return parts[1] + i;
        });

       	$(this).find('.btnEliminarCentro').attr("id", function(){
        	var parts = this.id.match(/(\D+)(\d*)$/);
            return parts[1] + i;
        });

        $(this).find('button:not(.btnEliminarCentro)').attr("onclick",'cargaClick('+i+')');
       	i++;
	});
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>