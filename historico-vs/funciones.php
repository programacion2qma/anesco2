<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales
//Inicio de funciones específicas


//Parte de gestión de reconocimientos médicos


function operacionesHistorico(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaHistorico();
	}
	elseif(isset($_POST['codigoEmpleado'])){
		$res=creaHistorico();
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoHistorico($_POST['elimina']);
	}

	mensajeResultado('codigoEmpleado',$res,'historico');
    mensajeResultado('elimina',$res,'historico', true);
}


function creaHistorico(){
	$res=insertaDatos('historicoVS');

	return $res;
}

function actualizaHistorico(){
	$res=actualizaDatos('historicoVS');

	return $res;
}



function cambiaEstadoEliminadoHistorico($estado){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],'historicoVS',false);
	}
	cierraBD();

	return $res;
}


function gestionHistorico(){
	operacionesHistorico();
	
	abreVentanaGestionConBotones('Gestión de historico de VS','index.php','span6','icon-edit','',true,'noAjax');
	$datos=compruebaDatosHistorico();

	campoFecha('fecha','Fecha',$datos);
	campoSelectConsulta('codigoCliente','Empresa','SELECT codigo, EMPNOMBRE as texto FROM clientes WHERE activo="SI" AND eliminado="NO"',$datos);
	campoOculto($datos['codigoEmpleado'],'codigoEmpleadoOculto');
	campoSelectConsulta('codigoEmpleado','Empleado','SELECT codigo, CONCAT(nombre," ",apellidos) as texto FROM clientes WHERE aprobado="SI" AND eliminado="NO" AND  codigoCliente='.$datos['codigoCliente'],$datos);
	campoOculto($datos,'eliminado','NO');
	campoDato('DNI',false,'dni');
	campoDato('Edad',false,'edad');
	campoDato('Puesto de trabajo',false,'puesto');
	campoSelect('estado','Estado',array('Apto','No apto','No apto temporal','Sin criterio de aptitud'),array(1,2,3,4),$datos);
	campoRadio('sexo','Sexo',$datos,'V',array('V','M'),array('V','M'));
	campoTexto('nlab','NLAB (Analítica laboratorio)',$datos);
	campoTexto('fact','FACT',$datos);
	areaTexto('observaciones','Observaciones',$datos,'areaInforme'); 


	cierraVentanaGestion('index.php',true);
}

function compruebaDatosHistorico(){
	if(isset($_REQUEST['codigo'])){//OJO: con $_REQUEST porque puede venir desde el listado ($_GET) o desde el alta
		$datos=consultaBD('SELECT historicoVS.* , clientes.codigo AS codigoCliente
			FROM historicoVS 
			INNER JOIN empleados ON historicoVS.codigoEmpleado=empleados.codigo
			INNER JOIN clientes ON empleados.codigoCliente=clientes.codigo
			WHERE historicoVS.codigo='.$_REQUEST['codigo'],true,true);
		campoOculto($datos);
	}
	else{
		$datos=false;
	}
	return $datos;
}

function imprimeHistorico($eliminados='NO'){
	global $_CONFIG;
	$codigoUsuario=$_SESSION['codigoU'];

	$consulta=consultaBD("SELECT historicoVS.*, clientes.EMPNOMBRE, CONCAT(empleados.nombre,' ',empleados.apellidos) AS empleado, empleados.dni, empleados.codigo AS codigoEmpleado, puestos_trabajo.nombre AS puesto
							FROM historicoVS
							INNER JOIN empleados ON historicoVS.codigoEmpleado=empleados.codigo
							INNER JOIN clientes ON empleados.codigoCliente=clientes.codigo
							LEFT JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo
						  WHERE historicoVS.eliminado='$eliminados';");
	$estados=array(1=>'Apto',2=>'No apto',3=>'No apto temporal',4=>'Sin acritud de aptitud');
	while($datos=mysql_fetch_assoc($consulta)){
		$edad=obtieneEdad($datos['codigoEmpleado'],true);

		echo "
			<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['EMPNOMBRE']."</td>
				<td>".$datos['empleado']."</td>
				<td>".$datos['dni']."</td>
				<td>".$edad."</td>
				<td>".$estados[$datos['estado']]."</td>
				<td>".$datos['sexo']."</td>
				<td>".$datos['puesto']."</td>
				<td>".$datos['nlab']."</td>
				<td>".$datos['fact']."</td>
				<td><a class='btn btn-propio' href='".$_CONFIG['raiz']."historico-vs/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	
}

function obtieneEmpleados(){
	$res=array();
	$res['select']='';
	$datos=arrayFormulario();
	$empleados=consultaBD('SELECT * FROM empleados WHERE aprobado="SI" AND eliminado="NO" AND codigoCliente='.$datos['codigoCliente'],true);
	while($empleado=mysql_fetch_assoc($empleados)){
		$res['select'].='<option value="'.$empleado['codigo'].'">'.$empleado['nombre'].' '.$empleado['apellidos'].'</option>';
	}
	echo json_encode($res);
}

function obtieneDatosEmpleado(){
	$res=array();

	$datos=arrayFormulario();
	extract($datos);

	conexionBD();

	$res=consultaBD("SELECT puestos_trabajo.nombre AS puesto, empleados.dni FROM puestos_trabajo INNER JOIN empleados ON puestos_trabajo.codigo=empleados.codigoPuestoTrabajo WHERE empleados.codigo=$codigoEmpleado",false,true);


	$res['edad']=obtieneEdad($codigoEmpleado,false);

	cierraBD();

	echo json_encode($res);
}

function obtieneEdad($codigoEmpleado,$conexion){
	$empleado=consultaBD("SELECT fechaNacimiento FROM empleados WHERE codigo=$codigoEmpleado",$conexion,true);	
	return calculaEdadEmpleado($empleado['fechaNacimiento']);
}


//Fin parte de gestión de reconocimientos médicos