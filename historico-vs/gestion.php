<?php
  $seccionActiva=3;
  include_once("../cabecera.php");
  gestionHistorico();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../../api/js/funciones.js" type="text/javascript"></script>
<script src="../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>
<script src="../js/campoFicheroAnalitica.js" type="text/javascript"></script>

<script type="text/javascript">	
$(document).ready(function(){
	$('.selectpicker').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

	oyenteCliente($('#codigoCliente option:selected').val());
	$('#codigoCliente').change(function(){
		oyenteCliente($(this).val());
	});

	$('#codigoEmpleado').change(function(){
		oyenteEmpleado($(this).val());
	});

	$('input[name=checkRecomendacion]').click(function(){
		oyenteCheckRecomendaciones($(this));
	});

	$('#obtenerDatosPorDefecto').click(function(){
		rellenaDatosPorDefecto();
	});
	
	$('#obtenerDatosReconocimientoAnterior').click(function(){
		obtieneDatosReconocimientoAnterior();
	});

	$('#talla,#peso').change(function(){
		calculaIMC();
	});

	$('.tip').each(function(){
        var aviso=$(this).text();
        var campo=$(this).prev().find('.group-span-filestyle');

        campo.attr('data-toggle','tooltip');
        campo.attr('title',aviso);
        campo.tooltip(); 
        $(this).remove();
    });


    $('input[name=cerrado]').change(function(){
    	oyenteCerrado();
    });

    oyenteBloqueado();
    $('input[name=bloqueado]').change(function(){
    	oyenteBloqueado();
    });

    oyenteConcurrencia();
});

function oyenteCliente(codigoCliente){
	if(codigoCliente=='NULL'){
		
	}
	else{
		var consulta=$.post('../listadoAjax.php?include=historico-vs&funcion=obtieneEmpleados();',{'codigoCliente':codigoCliente},
		function(respuesta){
			$('#codigoEmpleado').html(respuesta.select);
			$('#codigoEmpleado').val($('#codigoEmpleadoOculto').val());
			$('#codigoEmpleado').selectpicker('refresh');
			oyenteEmpleado($('#codigoEmpleado').val());
		},'json');
	}
}

function oyenteEmpleado(codigoEmpleado){
	if(codigoEmpleado==null){
		$('#puesto').text('');
		$('#edad').text('');
		$('#dni').text('');
	}
	else{
		var consulta=$.post('../listadoAjax.php?include=historico-vs/&funcion=obtieneDatosEmpleado();',{'codigoEmpleado':codigoEmpleado},
		function(respuesta){
			$('#puesto').text(respuesta.puesto);
			$('#edad').text(respuesta.edad);
			$('#dni').text(respuesta.dni);
		},'json');
	}
}

function compruebaOpcionCheckClasificacion(nombreCampo,valor){
	var checked=false;

	if(valor=='SI'){
		checked=true;
	}
	
	$('input[name='+nombreCampo+']').attr('checked',checked);
}

function oyenteCheckRecomendaciones(campo){
	var texto=campo.parent().text().trim()+' ';
	var recomendaciones=$('#recomendaciones').val();

	if(campo.attr('checked')){
		$('#recomendaciones').val(recomendaciones+texto);
	}
	else{
		recomendaciones=recomendaciones.replace(texto,'');

		$('#recomendaciones').val(recomendaciones);
	}
}


function rellenaDatosPorDefecto(){
	$('#periodicidad').val('doce');
	$('#riesgoPuesto').val('Caídas, movimientos repetidos, posturas forzadas, cargas, sobre esfuerzos, dermatitis, fatiga física, fatiga mental, fatiga postural.');
	
	$('input[name=checkVacunaciones0]').attr('checked',true);
	$('#ejercicio').val('Realiza ejercicio físico esporádico').selectpicker('refresh');
	$('#alimentacion').val('Su alimentación es equilibrada-variada').selectpicker('refresh');
	$('#calidadSuenio').val('Bien: 6 o más horas').selectpicker('refresh');
	$('#historiaActual').val('Actualmente se encuentra bien, me refiere desconocer limitación alguna que le dificulte la realización de su trabajo.');

	$('#medicacionHabitual').val('No realiza.');
	$('#trastornosCongenitos').val('No refiere.');
	$('#fracturas').val('No refiere fracturas con limitaciones para su actividad laboral.');
	$('#riesgosLaborales').val('Caídas, movimientos repetidos, posturas forzadas, cargas, sobre esfuerzos, dermatitis, fatiga física, fatiga mental, fatiga postural.');
	$('#antecedentesPersonales').val('Riesgo cardiovascular: No HTA, no diabetes, no dislipemia. \nEnf.Cardiorrespiratorios: No cardiopatía, ni broncopatías. \nEnf.Digestivo: No antecedentes digestivos ni hepatobiliares. \nEnf.Genitourinario: No antecedentes urinarios. \nEnf.Ap.locomotor y neurológico: No refiere. \nEnf.ORL: No refiere. \nEnf.Oftalmológico: No refiere. \nCirugía: No refiere.');
	$('#alergias').val('Alimentos: No conocidas. Medicamentos: No conocidas. Ambientales: No conocidas. Laborales: No conocidas.');
	$('#antecedentesFamiliares').val('No refiere.');
	$('#minusvaliaReconocida').val('No.');

	$('#inspeccionGeneral').val('Buen estado general. Consciente, orientado y colaborador.');
	$('#orl').val('Exploración faríngea normal.');
	$('#oftalmologia').val('Inspección ocular sin hallazgos específicos.');
	$('#exploracionNeurologica').val('Dentro de los límites de la normalidad; no problemas de equilibrio, coordinación, fuerza ni sensibilidad.');
	$('#pielMucosas').val('Normal coloración e hidratación.');
	$('#auscultacionPulmonar').val('Buen murmullo vesicular en ambos campos pulmonares.');
	$('#aparatoDigestivo').val('ABDOMEN: Blando y depresible. No dolor a la palpación profunda. No se evidencia masas ni megalias');
	$('#extremidades').val('Sin edemas ni signos de trombosis venosa profunda.');
	$('#auscultacionCardiaca').val('Corazón rítmico sin soplos ni extratonos.');
	$('#sistemaUrogenital').val('Puño percusión renal negativa.');
	$('#aparatoLocomotor').val('Sin hallazgos específicos. Columna vertebral sin manifestaciones de efectos limitativos en la actualidad.');


	$('input[name=checkElectrocardiograma][value=SI]').attr('checked',true);
	$('#electrocardiograma').val('Sin alteraciones en la repolarización.');

	$('input[name=checkHemograma][value=SI]').attr('checked',true);
	$('#hemograma').val('Parámetros dentro de la normalidad.');	
	$('input[name=checkBioquimica][value=SI]').attr('checked',true);
	$('#bioquimica').val('Parámetros dentro de la normalidad.');
	$('input[name=checkOrina][value=SI]').attr('checked',true);
	$('#orina').val('Parámetros dentro de la normalidad.');
	$('#analitica').val('Los valores encontrado en los análisis que se han efectuado están dentro de la normalidad.');


	$('input[name=checkDinamometria][value=SI]').attr('checked',true);
	$('#dinamometria').val('Normal.');	
	$('#rxTorax').val('Prueba no realizada, según protocolos médicos aplicados.');

	$('#recomendaciones').val('El resultado de su Tensión Arterial (TA) es:  óptima (clasificación de la hipertensión arterial de acuerdo al JNC-V).\nRecomendamos revisar la visión por lo menos una vez al año. ');
}

function calculaIMC(){
	var talla=formateaNumeroCalculoIMC($('#talla').val());
	var peso=formateaNumeroCalculoIMC($('#peso').val());

	if(talla!='' && peso!=''){
		talla=talla/100;
		var imc=peso/(Math.pow(talla,2));

		$('#imc').val(formateaNumeroWeb(imc));
	}
	else{
		$('#imc').val('');
	}
}

function formateaNumeroCalculoIMC(numero){
	var res=numero;

	if(isNaN(res)){
		res=formateaNumeroCalculo(res);
	}

	return res;
}


function obtieneDatosReconocimientoAnterior(){
	var codigoEmpleado=$('#codigoEmpleado').val();
	if(codigoEmpleado=='NULL'){
		alert('Por favor, seleccione primero un empleado.');
	}
	else{
		var consulta=$.post('../listadoAjax.php?include=reconocimientos-medicos&funcion=consultaReconocimientoMedicoAnteriorEmpleado();',{'codigoEmpleado':codigoEmpleado});
		consulta.done(function(respuesta){
			if(respuesta=='NO'){
				alert('No hay reconocimientos anteriores para el empleado seleccionado.');
			}
			else{
				var codigoContrato=$('#codigoContrato').val();
				creaFormulario('?',{'codigoReconocimientoAnterior':respuesta,'codigoContrato':codigoContrato},'post');
			}
		});
	}
}

function oyenteCerrado(){
	if($('input[name=cerrado]:checked').val()=='SI'){
		$('input[name=bloqueado][value=SI]').attr('checked',true);
	}
}

function oyenteBloqueado(){
	if($('input[name=bloqueado]:checked').val()=='SI' || $('input[name=bloqueado][type=hidden]').val()=='SI'){
		$(':submit,.widget-header .btn-propio').attr('disabled',true);
		$(':submit,.widget-header .btn-propio').addClass('hide');
	}
	else{
		$(':submit,.widget-header .btn-propio').attr('disabled',false);
		$(':submit,.widget-header .btn-propio').removeClass('hide');
	}
}

function oyenteConcurrencia(){
	if($('#codigo').val()!=undefined){
		$('a:not([data-toggle="tab"],[dropdown-toggle="dropdown"]),button:not(.selectpicker)').unbind().click(function(e){
			var codigoReconocimiento=$('#codigo').val();
			
			var edicion=$.post('../listadoAjax.php?include=reconocimientos-medicos&funcion=marcaReconocimientoMedicoComoEditable();',{'codigoReconocimiento':codigoReconocimiento});
		});
	}
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>