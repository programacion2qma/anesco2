<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión de estados de recibos


function operacionesEstadosRecibos(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('estados_recibos');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('estados_recibos');
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoItem($_POST['elimina'],'estados_recibos');
	}

	mensajeResultado('nombre',$res,'estado de recibo');
    mensajeResultado('elimina',$res,'estado de recibo', true);
}



function gestionEstadoRecibo(){
	operacionesEstadosRecibos();
	
	abreVentanaGestionConBotones('Gestión de estados de recibo','index.php','span3');
	$datos=compruebaDatos('estados_recibos');

	campoTexto('nombre','Nombre estado',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	areaTexto('observaciones','Observaciones',$datos);
	campoOculto($datos,'eliminado','NO');

	cierraVentanaGestion('index.php',true);
}



function imprimeEstadosRecibos($eliminado){
	global $_CONFIG;

	$consulta=consultaBD("SELECT * FROM estados_recibos WHERE eliminado='$eliminado';",true);

	while($datos=mysql_fetch_assoc($consulta)){

		echo "
		<tr>
			<td>".$datos['nombre']."</td>
			<td>".$datos['observaciones']."</td>
			<td class='centro'><a class='btn btn-propio' href='".$_CONFIG['raiz']."estados-recibos/gestion.php?codigo=".$datos['codigo']."'><i class='icon-edit'></i> Editar</i></a></td>
			<td class='centro'>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' />
        	</td>
		</tr>";
	}
}

//Fin parte de gestión de estados de recibos