<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales
include_once('../../api/js/firma/signature-to-image.php');
include_once('../Mobile_Detect.php');
//Inicio de funciones específicas


//Parte de gestión formas de pago


function operacionesFormasPago(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('formas_pago');
	}
	elseif(isset($_POST['forma'])){
		$res=insertaDatos('formas_pago');
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoItem($_POST['elimina'],'formas_pago');
	}

	mensajeResultado('forma',$res,'forma de pago');
    mensajeResultado('elimina',$res,'forma de pago', true);
}


function gestionFormaPago(){
	operacionesFormasPago();
	
	abreVentanaGestionConBotones('Gestión de formas de pago','index.php','span3');
	$datos=compruebaDatos('formas_pago');

	campoTexto('forma','Forma de pago',$datos);
	campoRadio('remesable','Remesable',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto('numPagos','Número de pagos',$datos,'input-mini pagination-right');
	campoRadio('mostrarCuentas','Selector cuentas bancarias',$datos);
	campoOculto($datos,'eliminado','NO');

	cierraVentanaGestion('index.php',true);
}



function imprimeFormasPago($eliminado){
	global $_CONFIG;

	$consulta=consultaBD("SELECT * FROM formas_pago WHERE eliminado='$eliminado';",true);

	while($datos=mysql_fetch_assoc($consulta)){

		echo "
		<tr>
			<td>".$datos['forma']."</td>
			<td>".$datos['numPagos']."</td>
			<td>".$datos['remesable']."</td>
			<td>".$datos['mostrarCuentas']."</td>
			<td class='centro'><a class='btn btn-propio' href='".$_CONFIG['raiz']."formas-pago/gestion.php?codigo=".$datos['codigo']."'><i class='icon-edit'></i> Editar</i></a></td>
			<td class='centro'>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' />
        	</td>
		</tr>";
	}
}

//Fin parte de gestión de formas de pago