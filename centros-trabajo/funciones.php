<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales
@include_once('../Mobile_Detect.php');

//Inicio de funciones específicas


//Parte de gestión Clientes


function operacionesCentros(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaCentro();
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaCentro();
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoCentros($_POST['elimina'],'clientes_centros');
	}

	mensajeResultado('nombre',$res,'Centro de trabajo');
    mensajeResultado('elimina',$res,'Centro de trabajo', true);
}

function insertaCentro(){
	$res=true;
	$res=insertaDatos('clientes_centros');

	return $res;
}

function actualizaCentro(){
	$res=true;
	$res=actualizaDatos('clientes_centros');
	return $res;
}


function gestionCentros(){
	operacionesCentros();
	
	abreVentanaGestionConBotones('Gestión de Centros','');
	if(isset($_POST['nombre'])){
		$datos=false;
	} else {
		$datos=compruebaDatos('clientes_centros');
	}
	$codigoCliente=obtieneCodigoClienteCentros($datos);
	
	campoOculto($codigoCliente,'codigoCliente');
	campoOculto($datos,'eliminado','NO');

	abreColumnaCampos();
		campoTexto('nombre','Centro de trabajo',$datos,'input-large');
		campoTexto('direccion','Dirección',$datos,'input-large');
		campoTexto('localidad','Localidad',$datos,'input-large');
		campoTexto('trabajadores','Nº de trabajadores',$datos,'input-mini');
	cierraColumnaCampos();	

	abreColumnaCampos();
		campoRadio('activo','Activo',$datos);
		campoTexto('cp','CP',$datos,'input-mini');
		campoTexto('provincia','Provincia',$datos,'input-large');
	cierraColumnaCampos();

	crearMapa($datos);
	
	cierraVentanaGestionCentros('index.php?codigoCliente='.$codigoCliente,true);
}

function cierraVentanaGestionCentros($destino,$columnas=false,$botonGuardar=true,$texto='Guardar',$icono='icon-check',$botonVolver=true,$textoVolver='Volver',$iconoVolver='icon-chevron-left',$salto=true){
	if($columnas){
		echo "		  </fieldset>
			  		  <fieldset class='sinFlotar'>";
	}
	if($salto){
		echo "			<br />";
	}


	echo "
						
	                    <div class='form-actions'>";
	if($botonVolver){
	    echo "            <a href='$destino' class='btn btn-default'><i class='$iconoVolver'></i> $textoVolver</a>";
	}

	if($botonGuardar){                      
	    echo "            <button id='salir' type='submit' class='btn btn-propio'><i class='$icono'></i> Guardar y salir</button>";
	    echo "            <button id='nuevo' type='submit' class='btn btn-propio'><i class='$icono'></i> Guardar e introducir nuevo</button>";
	}

	echo "
	                    </div> <!-- /form-actions -->
	                  </fieldset>
	                </form>
	                </div>


	            </div>
	            <!-- /widget-content --> 
	          </div>

	      </div>
	    </div>
	    <!-- /container --> 
	  </div>
	  <!-- /main-inner --> 
	</div>
	<!-- /main -->";
}

function crearMapa($datos){
	echo "<fieldset class='sinFlotar'>";
	$calle='';
	if($datos){
		$calle=$datos['direccion'].' '.$datos['cp'];
	}
	if(isset($calle)){
		echo "<button style='' type='button' class='btn btn-propio' onclick='cargaClick()'><i class='icon-google-plus'></i> Cargar mapa</button>";

		$detect = new Mobile_Detect();
		if( $detect->isAndroid() ) {
		// Android
			echo "<center><a href='geo:0,0?daddr=$calle' class='noAjax enlaceExternoMapa'><div id='map' class='map'></div></a></center>";	
		} elseif ( $detect->isIphone() ) {
		// iPhone
			echo "<center><a href='https://maps.apple.com/maps?saddr=Current%20Location&daddr=$calle' class='noAjax enlaceExternoMapa'><div id='map".$i."' class='map'></div></a></center>";
		} elseif ( $detect->isWindowsphone() ) {
		// Windows Phone
			echo "<center><a href='maps:$calle' class='noAjax enlaceExternoMapa'><div id='map' class='map'></div></a></center>";
		} else{
		// Por defecto
			echo "<center><a href='https://www.google.es/maps/place/Calle+$calle' class='noAjax enlaceExternoMapa'><div id='map' class='map'></div></a></center>";
		//$url = 'http://maps.google.com?daddr=Universidad+de+deusto+bilbao';
		}

		//echo "<center><a href='https://www.google.es/maps/place/Calle+$calle' class='noAjax enlaceExternoMapa'><div id='map' class='map'></div></a></center>";		
	}else{
		echo "<button style='' type='button' class='btn btn-propio' onclick='cargaClick()'><i class='icon-google-plus'></i> Cargar mapa</button>";
		echo "<center><div id='map' class='map'></div></center>";
	}
	echo '</fieldset>';
	echo "<br clear='all'>";
}


function obtieneCodigoClienteCentros($datos=false){
	if(isset($_GET['codigoCliente'])){
		$res=$_GET['codigoCliente'];	
	}
	elseif($datos){
		$res=$datos['codigoCliente'];
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=$_POST['codigoCliente'];
	}
	else{
		$res=$_SESSION['codigoCliente'];
	}
	$_SESSION['codigoCliente']=$res;

	return $res;
}



//Fin parte de campos personalizados


function imprimeCentros($cliente){
	global $_CONFIG;

	conexionBD();

	$consulta=consultaBD("SELECT * FROM clientes_centros WHERE eliminado='NO' AND codigoCliente=".$cliente." ORDER BY nombre;");
	$iconos=array(
					'SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>',
					'PARCIAL'=>'<i class="icon-adjust iconoFactura icon-warning-color"></i>',
					'NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td>".$datos['nombre']."</td>
				<td>".$datos['direccion']." - CP: ".$datos['cp'].", ".$datos['localidad']." (".$datos['provincia'].")</td>
				<td class='centro'>".$datos['trabajadores']."</td>
				<td class='centro'>".$iconos[$datos['activo']]."</td>";
		echo "	<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>";
		echo "<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>";

		echo "</tr>";
	}
	cierraBD();
}


function estadisticasCentrosRestrict($cliente){
	$res=array();
	$eliminado="eliminado='NO'";
	conexionBD();
	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM clientes_centros WHERE ".$eliminado." AND codigoCliente=".$cliente,false, true);

	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

function obtenerCampoCliente($cliente,$campo){
	$contrato=consultaBD('SELECT '.$campo.' FROM clientes WHERE codigo='.$cliente,true,true);
	return $contrato[$campo];
}

function cambiaEstadoEliminadoCentros($estado,$tabla){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();
    for($i=0;isset($datos['codigo'.$i]);$i++){
        $res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],$tabla,false);
    }
    cierraBD();

    return $res;
}

//Fin parte de gestión Clientes
