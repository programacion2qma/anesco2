<?php
  $seccionActiva=1;
  include_once('../cabecera.php');

  $res=operacionesCentros();
  $cliente=obtieneCodigoClienteCentros();
  $estadisticas=estadisticasCentrosRestrict($cliente);
  $centros=obtenerCampoCliente($cliente,'EMPCENTROS');
  $nombre=obtenerCampoCliente($cliente,'EMPNOMBRE');
  $activo=obtenerCampoCliente($cliente,'activo');
  $btnVolver='<a href="'.$_CONFIG['raiz'].'clientes/index.php?" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>';
  if($activo=='NO'){
    $btnVolver='<a href="'.$_CONFIG['raiz'].'posibles-clientes/index.php?" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>';
  }
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de Centros de trabajo de <?php echo $nombre; ?>:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-building"></i>  <span class="value"><?php echo $estadisticas['total'].' / '.$centros; ?></span><br />Centros registrados / Centros en ficha de cliente</div>
                     <!--div class="stat"> <i class="icon-send"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Enviados a la agencia</div-->
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Centros</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">

                <?php
                    echo $btnVolver; 
                    echo '
                          <a href="'.$_CONFIG['raiz'].'centros-trabajo/gestion.php?codigoCliente='.$cliente.'" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nuevo centro</span> </a>
                          <a href="javascript:void" id="eliminar" class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>';
                ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Centros registrados</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Centro de trabajo</th>
                  <th> Dirección </th>
                  <th> Trabajadores </th>
                  <th> Activo </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeCentros($cliente);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
  $('#importarDatos').click(function(){
    abreVentana('#cajaFichero');
  });
  $('#puestosEmpleados').click(function(){
    abreVentana('#cajaPuestos');
  });

  $('#cajaPuestos select').val(10);
  $('#cajaPuestos select').trigger("change");

  $('#importaFichero').attr('data-dismiss','modal')
  $('#importaFichero').click(function(){
    $('#cajaFichero form').submit();
  });
});

function abreVentana(id){
  $(id).modal({'show':true,'backdrop':'static','keyboard':false});
}
</script>
<!-- contenido --></div>

<?php include_once('../pie.php'); ?>