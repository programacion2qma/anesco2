<?php
  $seccionActiva=1;
  include_once("../cabecera.php");
  gestionCentros();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
		cargaClick();

		$('.form-horizontal button[type=submit]').unbind();
 		$('.form-horizontal button[type=submit]').click(function(e){
 			e.preventDefault();
 			if($(this).attr('id')=='salir'){
 				$('.form-horizontal').attr('action','index.php');
 			} else if($(this).attr('id')=='nuevo'){
 				$('.form-horizontal').attr('action','?');
 			}
			$('.form-horizontal').submit();
		});
	});

function cargaClick(){
	$('#map').text('');
	cargarMapa($('#direccion').val(),$('#cp').val());
}

function cargarMapa(direccion,cp){
	var address = direccion+' '+cp;
	// Creamos el Objeto Geocoder
	var geocoder = new google.maps.Geocoder();
	// Hacemos la petición indicando la dirección e invocamos la función
	// geocodeResult enviando todo el resultado obtenido
    if(address!=' '){
	   		geocoder.geocode({ 'address': address}, geocodeResult);
    }
}

function geocodeResult(results, status) {
    // Verificamos el estatus
    if (status == 'OK') {
        // Si hay resultados encontrados, centramos y repintamos el mapa
        // esto para eliminar cualquier pin antes puesto
        var mapOptions = {
            center: results[0].geometry.location,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var divMapa;

        $('.map').each(function(){
        	if($(this).html()==''){
        		divMapa=$(this).get(0);
        		return false;
        	}
        });

        map = new google.maps.Map(divMapa, mapOptions);
        // fitBounds acercará el mapa con el zoom adecuado de acuerdo a lo buscado
        map.fitBounds(results[0].geometry.viewport);
        // Dibujamos un marcador con la ubicación del primer resultado obtenido
        var markerOptions = { position: results[0].geometry.location }
        var marker = new google.maps.Marker(markerOptions);

        marker.setMap(map);

    } else {
        // En caso de no haber resultados o que haya ocurrido un error
        // lanzamos un mensaje con el error
        if($('#codigo').length && status=='ZERO_RESULTS'){
        	status='El campo Dirección está vacío o Google Maps no localiza la dirección proporcionada';
        }
        alert("El mapa no se pudo cargar debido a: " + status);
    }
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>