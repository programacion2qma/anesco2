<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales


//Parte de documentación de cliente

function seleccionClienteListado(){
	$tipo=obtieneTipoDocumentacionCliente();

	abreVentanaGestion('Documentación - Selección de cliente','listado.php?tipo='.$tipo,'','icon-cursor','',false,'noAjax');
	campoSelectConsulta('codigoCliente','Cliente',"SELECT codigoUsuario AS codigo, CONCAT(EMPNOMBRE,' (',EMPMARCA,')') as texto FROM clientes INNER JOIN usuarios_clientes ON clientes.codigo=usuarios_clientes.codigoCliente WHERE activo='SI' ORDER BY EMPNOMBRE");
	cierraVentanaGestion('',false,true,'Ver documentación','icon-chevron-right',false);
}


function obtieneTipoDocumentacionCliente(){
	$res='vs';

	if(isset($_GET['tipo'])){
		$res=$_GET['tipo'];
	}

	return $res;
}




//Fin parte de documentación de cliente