<?php
  $seccionActiva=14;
  include_once('../cabecera.php');

  $tipo=obtieneTipoDocumentacionCliente();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Documentación disponible para descarga</h3>
            
            <?php
            if($_SESSION['tipoUsuario']!='CLIENTE'){
              echo "
              <div class='pull-right'>
                <a href='index.php' class='btn btn-default btn-small'><i class='icon-chevron-left'></i> Volver</a>
              </div>";
            }
            ?>
            
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Documento </th>
                
                <?php
                if($_SESSION['tipoUsuario']!='CLIENTE'){
                  echo '
                  <th> Cliente </th>
                  <th> Usuario </th>';
                }
                elseif(isset($_GET['tipo']) && $_GET['tipo']=='vs'){
                  echo '
                  <th>Empleado</th>';
                }
                ?>

                  <th> Fecha </th>
                  <th class="centro"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeDocumentacionCliente($tipo);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).on('click','.generaContrato',function(){
    var codigoContrato=$(this).attr('contrato');
    creaFormulario('../contratos/generaDocumento.php',{'codigoContrato':codigoContrato,'vigenciaContrato':'anual','renovar':'SI'},'post','_blank');
});
</script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>