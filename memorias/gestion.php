<?php
  $seccionActiva=3;
  include_once("../cabecera.php");
  gestionMemorias();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-select.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $('.selectpicker').selectpicker();

    $('input[type=number]').change(function(){
      var i=obtieneFilaCampo($(this));
      var valor=$(this).val();
      var total=$('#meses'+i+' select').length;
      if(total<valor){
        for(var j=total+1;j<=valor;j++){
          var nombreCampo='mes'+i+'_'+j;
          var campo=creaCampoMeses(nombreCampo);
          $('#meses'+i).append(campo);
          $('select#'+nombreCampo).selectpicker();
        }
      } else {
        for(var j=total;j>valor;j--){
          var nombreCampo='mes'+i+'_'+j;
          $('select#'+nombreCampo).parent().parent().remove();
        }
      }
    });

    $('#codigoCliente').change(function(){
      obtenerMaquinaria($(this).val());
    });
  });

function creaCampoMeses(nombreCampo){
  var meses=new Array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
  var res="<div class='control-group'>";
  res+="<label class='control-label' for='"+nombreCampo+"'>Mes:</label>";
  res+="<div class='controls'>";
  res+="<select name='"+nombreCampo+"' class='selectpicker span2 show-tick' id='"+nombreCampo+"' data-live-search='true'>"
  for(var i=0;i<=12;i++){
    res+="<option value='"+i+"'>"+meses[i]+"</option>";
  }
  res+="</select>";
  res+="</div></div>";
  return res;
}

function obtenerMaquinaria(codigo){
  if(codigo!='NULL'){
    var consulta=$.post('../listadoAjax.php?include=memorias&funcion=obtenerMaquinaria();',{'codigo':codigo});
      consulta.done(function(respuesta){
        $('select.selectMaquinaria').html(respuesta).selectpicker('refresh');
      });
  }
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>