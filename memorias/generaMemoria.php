<?php
	session_start();
	include_once('funciones.php');
	compruebaSesion();
	require_once '../../api/phpword/PHPWord.php';


	$PHPWord=new PHPWord();

	$documento=$PHPWord->loadTemplate('../documentos/plantillaMemorias.docx');

	$datos=consultaBD('SELECT EMPNOMBRE
	FROM memorias
	INNER JOIN clientes ON memorias.codigoCliente=clientes.codigo
	WHERE memorias.codigo='.$_GET['codigo'],true,true);

	$documento->setValue("razonSocial",utf8_decode($datos['EMPNOMBRE']));
    
    $documento->save('../documentos/Memoria.docx');
		// Definir headers
	header("Content-Type: application/msword");
	header("Content-Disposition: attachment; filename=Memoria_".time().".docx");
	header("Content-Transfer-Encoding: binary");

		// Descargar archivo
	readfile('../documentos/Memoria.docx');