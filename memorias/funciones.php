<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de incidencias

function operacionesMemorias(){
	$res=true;

	if(isset($_POST['fechaCreacion'])){
		$res=gestionaMemorias();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('memorias');
	}

	mensajeResultado('codigoCliente',$res,'Memoria');
    mensajeResultado('elimina',$res,'Memoria', true);
}

function gestionaMemorias(){
	$res=true;
	fechaYHoraActualizacion();
	$datos=arrayFormulario();
	if(isset($_POST['codigo'])){
		$res=actualizaDatos('memorias');
		$codigo=$_POST['codigo'];
		conexionBD();
			$res=insertaAP($codigo,$datos);
			$res=insertaER($codigo,$datos);
			$res=insertaFormacion($codigo,$datos);
			$res=insertaFormacion2($codigo,$datos);
			$res=insertaSiniestralidad($codigo,$datos);
			$res=insertaEvaluaciones($codigo,$datos);
			$res=insertaMaquinarias($codigo,$datos);
		cierraBD();
	}
	elseif(isset($_POST['fechaCreacion'])){
		$res=insertaDatos('memorias');
		$codigo=$res;
		conexionBD();
			$res=insertaAP($codigo,$datos);
			$res=insertaER($codigo,$datos);
			$res=insertaFormacion($codigo,$datos);
			$res=insertaFormacion2($codigo,$datos);
			$res=insertaSiniestralidad($codigo,$datos);
			$res=insertaEvaluaciones($codigo,$datos);
			$res=insertaMaquinarias($codigo,$datos);
		cierraBD();
	}
	return $res;
}

function fechaYHoraActualizacion(){
	$_POST['fechaUltimaActualizacion']=fecha();
	$_POST['horaUltimaActualizacion']=hora();
}

function insertaAP($codigo,$datos){
	$res=true;
	
	$i=1;
	while(isset($datos['ap'.$i])){
		$existe=consultaBD('SELECT * FROM memorias_actividades_preventivas WHERE codigoMemoria='.$codigo.' AND numeroInterno='.$i,false,true);
		if($existe){
			$res=consultaBD('UPDATE memorias_actividades_preventivas SET valor="'.$datos['ap'.$i].'" WHERE codigo='.$existe['codigo']);
		} else {
			$res=consultaBD('INSERT INTO memorias_actividades_preventivas VALUES(NULL,'.$codigo.','.$i.',"'.$datos['ap'.$i].'");');
		}
		$i++;
	}
	return $res;
}

function insertaER($codigo,$datos){
	$res=true;
	
	$i=1;
	while(isset($datos['erUno'.$i])){
		$existe=consultaBD('SELECT * FROM memorias_evaluaciones_riesgos WHERE codigoMemoria='.$codigo.' AND numeroInterno='.$i,false,true);
		if($existe){
			$res=consultaBD('UPDATE memorias_evaluaciones_riesgos SET valorUno="'.$datos['erUno'.$i].'",valorDos="'.$datos['erDos'.$i].'",valorTres="'.$datos['erTres'.$i].'" WHERE codigo='.$existe['codigo']);
		} else {
			$res=consultaBD('INSERT INTO memorias_evaluaciones_riesgos VALUES(NULL,'.$codigo.','.$i.',"'.$datos['erUno'.$i].'","'.$datos['erDos'.$i].'","'.$datos['erTres'.$i].'");');
		}
		$i++;
	}
	return $res;
}

function insertaFormacion($codigo,$datos){
	$res=true;
	
	$i=1;
	while(isset($datos['f1Uno'.$i])){
		$existe=consultaBD('SELECT * FROM memorias_formacion_trabajadores WHERE codigoMemoria='.$codigo.' AND numeroInterno='.$i,false,true);
		if($existe){
			$res=consultaBD('UPDATE memorias_formacion_trabajadores SET valorUno="'.$datos['f1Uno'.$i].'",valorDos="'.$datos['f1Dos'.$i].'" WHERE codigo='.$existe['codigo']);
		} else {
			$res=consultaBD('INSERT INTO memorias_formacion_trabajadores VALUES(NULL,'.$codigo.','.$i.',"'.$datos['f1Uno'.$i].'","'.$datos['f1Dos'.$i].'");');
		}
		$i++;
	}
	return $res;
}

function insertaFormacion2($codigo,$datos){
	$res=true;
	
	$i=1;
	while(isset($datos['f2Uno'.$i])){
		$existe=consultaBD('SELECT * FROM memorias_formacion_trabajadores2 WHERE codigoMemoria='.$codigo.' AND numeroInterno='.$i,false,true);
		if($existe){
			$res=consultaBD('UPDATE memorias_formacion_trabajadores2 SET valorUno="'.$datos['f2Uno'.$i].'",valorDos="'.$datos['f2Dos'.$i].'",valorTres="'.$datos['f2Tres'.$i].'",valorCuatro="'.$datos['f2Cuatro'.$i].'",valorCinco="'.$datos['f2Cinco'.$i].'",valorSeis="'.$datos['f2Seis'.$i].'" WHERE codigo='.$existe['codigo']);
		} else {
			$res=consultaBD('INSERT INTO memorias_formacion_trabajadores2 VALUES(NULL,'.$codigo.','.$i.',"'.$datos['f2Uno'.$i].'","'.$datos['f2Dos'.$i].'","'.$datos['f2Tres'.$i].'","'.$datos['f2Cuatro'.$i].'","'.$datos['f2Cinco'.$i].'","'.$datos['f2Seis'.$i].'");');
		}
		$i++;
	}
	return $res;
}

function insertaSiniestralidad($codigo,$datos){
	$res=true;
	
	$i=1;
	while(isset($datos['sUno'.$i])){
		$existe=consultaBD('SELECT * FROM memorias_siniestralidad WHERE codigoMemoria='.$codigo.' AND numeroInterno='.$i,false,true);
		if($existe){
			$res=consultaBD('UPDATE memorias_siniestralidad SET valorUno="'.$datos['sUno'.$i].'",valorDos="'.$datos['sDos'.$i].'" WHERE codigo='.$existe['codigo']);
		} else {
			$res=consultaBD('INSERT INTO memorias_siniestralidad VALUES(NULL,'.$codigo.','.$i.',"'.$datos['sUno'.$i].'","'.$datos['sDos'.$i].'");');
		}
		$i++;
	}
	return $res;
}

function insertaEvaluaciones($codigo,$datos){
	$res=true;
	$res=consultaBD('DELETE FROM memorias_evaluaciones WHERE codigoMemoria='.$codigo,true);
	$i=1;
	while(isset($datos['anio'.$i])){
		$j=1;
		while(isset($datos['mes'.$i.'_'.$j])){
			$res=consultaBD('INSERT INTO memorias_evaluaciones VALUES(NULL,'.$codigo.',"'.$datos['anio'.$i].'","'.$datos['mes'.$i.'_'.$j].'");',true);
			$j++;
		}
		$i++;
	}
	return $res;
}

function insertaMaquinarias($codigo,$datos){
	$res=true;
	$res=consultaBD('DELETE FROM memorias_maquinarias WHERE codigoMemoria='.$codigo,true);
	$i=0;
	while(isset($datos['maquinaria'.$i])){
		if($datos['maquinaria'.$i]!='NULL'){
			$res=consultaBD('INSERT INTO memorias_maquinarias VALUES(NULL,'.$codigo.','.$datos['maquinaria'.$i].',"'.$datos['mesM'.$i].'","'.$datos['anioM'.$i].'");',true);
		}
		$i++;
	}
	return $res;
}



function gestionMemorias(){
	operacionesMemorias();
	
	abreVentanaGestionConBotones('Gestión de Memorias','index.php');
	$datos=compruebaDatos('memorias');
	$where='';

	campoFecha('fechaCreacion','Fecha creación',$datos);

	echo '<h3 class="apartadoFormulario">Actividades realizadas en el ejercicio</h3>';
	tablaActividadesPreventivas($datos);

	echo '<h3 class="apartadoFormulario">Evaluaciones de riesgos</h3>';
	tablaEvaluacionesRiesgos($datos);

	echo '<h3 class="apartadoFormulario">Formacion de los trabajadores en las empresas</h3>';
	tablaFormacion($datos);

	echo '<h3 class="apartadoFormulario">Formacion de los trabajadores (básico y emergencia)</h3>';
	tablaFormacion2($datos);

	echo '<h3 class="apartadoFormulario">Siniestralidad en la empresa</h3>';
	tablaSiniestralidad($datos);

	echo '<h3 class="apartadoFormulario">Evaluaciones de riesgos</h3>';
	$j=1;
	for($i=2015;$i<date('Y');$i++){
		abreColumnaCampos('span3 spanMemoria');
			campoTexto('anio'.$j,'Año',$i,'input-mini',true);
			$cantidad=0;
			if($datos){
				$consulta=consultaBD('SELECT COUNT(codigo) AS total FROM memorias_evaluaciones WHERE anio="'.$i.'" AND codigoMemoria='.$datos['codigo'],true,true);
				$cantidad=$consulta['total'];
			}
			campoNumero('cantidad'.$j,'Cantidad',$cantidad);
			echo '<div id="meses'.$j.'">';
			if($datos){
				$consulta=consultaBD('SELECT * FROM memorias_evaluaciones WHERE anio="'.$i.'" AND codigoMemoria='.$datos['codigo'],true);
				$k=1;
				while($item=mysql_fetch_assoc($consulta)){
					campoSelect('mes'.$j.'_'.$k,'Mes',array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'),array(0,1,2,3,4,5,6,7,8,9,10,11,12),$item['mes'],'selectpicker span2 show-tick');
					$k++;
				}
			}
			echo '</div>';
		cierraColumnaCampos();
		$j++;
	}
	echo '<br clear="all"><h3 class="apartadoFormulario">Evaluaciones de equipos y maquinarias</h3>';
	tablaMaquinaria($datos);
	cierraVentanaGestion('index.php',true);
}

function tablaActividadesPreventivas($datos){
	$campos=array(1=>'Diseño e implantación de planes de prevención según Ley 54/2003',2=>'Evaluaciones iniciales de riesgos',3=>'Revisión o actualización de evaluaciones de riesgos',4=>'Planificación de la actividad preventiva',5=>'Seguimiento de las actividades planificadas',6=>'Información de loas trabajadores',7=>'Formación de los trabajadores ( Exclusivamente la formación referida al
articulo 19 de la LPRL)',8=>'Realización de planes de emergencia',9=>'Investigación y análisis de Accidentes de trabajo',10=>'Investigación y análisis de Enfermedades Profesionales',11=>'Planificación de la Vigilancia individual de la salud',12=>'Planificación de la Vigilancia colectiva de la salud',13=>'Seguimiento de las actividades sanitarias planificadas');
	echo "
		<div class='table-responsive centro'>
			<table class='table table-striped tabla-simple mitadAncho'>
			  	<thead>
			    	<tr>
			            <th> ACTIVIDADES PREVENTIVAS </th>
						<th class='centro'> N.T.AF </th>
			    	</tr>
			  	</thead>
			  	<tbody>";
			  		conexionBD();
			  		foreach ($campos as $key => $value) {
			  			$item=consultaBD('SELECT * FROM memorias_actividades_preventivas WHERE codigoMemoria='.$datos['codigo'].' AND numeroInterno='.$key,false,true);
			  			echo '<tr>
			  				<td>'.$value.'</td>';
			  				campoTextoTabla('ap'.$key,$item['valor'],'input-mini pagination-right');
			  				$j=$i+1;
			  			echo '</tr>';
			  		}
			  		cierraBD();
	echo "		</tbody>
			</table> ";
	echo "
		</div><br/>";
}

function tablaEvaluacionesRiesgos($datos){
	$campos=array(1=>'De seguridad: lugares de trabajo (excepto condiciones ambientales)',2=>'De seguridad: máquinas, equipos e instalaciones',3=>'Higiénicos: agentes químicos',4=>'Higiénicos: agentes cancerígenos',5=>'Higiénicos: agentes biológicos',6=>'Higiénicos: ruido',7=>'Higiénicos: vibraciones',8=>'Higiénicos: iluminación',9=>'Higiénicos: estrés térmico',10=>'Higiénicos: otros',11=>'Ergonómicos: carga física',12=>'Psicosociales');
	echo "
		<div class='table-responsive centro'>
			<table class='table table-striped tabla-simple mitadAncho'>
			  	<thead>
			    	<tr>
			            <th> EVALUACIONES DE RIESGO </th>
						<th class='centro'> M.V.U </th>
						<th class='centro'> N.T. </th>
						<th class='centro'> N.H. </th>
			    	</tr>
			  	</thead>
			  	<tbody>";
			  		conexionBD();
			  		foreach ($campos as $key => $value) {
			  			$item=consultaBD('SELECT * FROM memorias_evaluaciones_riesgos WHERE codigoMemoria='.$datos['codigo'].' AND numeroInterno='.$key,false,true);
			  			echo '<tr>
			  				<td>'.$value.'</td>';
			  				campoTextoTabla('erUno'.$key,$item['valorUno'],'input-large');
			  				campoTextoTabla('erDos'.$key,$item['valorDos'],'input-mini pagination-right');
			  				campoTextoTabla('erTres'.$key,$item['valorTres'],'input-mini pagination-right');
			  				$j=$i+1;
			  			echo '</tr>';
			  		}
			  		cierraBD();
	echo "		</tbody>
			</table> ";
	echo "
		</div><br/>";
}

function tablaFormacion($datos){
	$campos=array(1=>'Formación de los trabajadores sobre los riesgos específicos de sus puestos de trabajo (art. 19 LPRL');
	echo "
		<div class='table-responsive centro'>
			<table class='table table-striped tabla-simple mitadAncho'>
			  	<thead>
			    	<tr>
			            <th> FORMACIÓN DE LOS TRABAJADORES </th>
						<th class='centro'> N.T. </th>
						<th class='centro'> N.H. </th>
			    	</tr>
			  	</thead>
			  	<tbody>";
			  		conexionBD();
			  		foreach ($campos as $key => $value) {
			  			$item=consultaBD('SELECT * FROM memorias_formacion_trabajadores WHERE codigoMemoria='.$datos['codigo'].' AND numeroInterno='.$key,false,true);
			  			echo '<tr>
			  				<td>'.$value.'</td>';
			  				campoTextoTabla('f1Uno'.$key,$item['valorUno'],'input-mini pagination-right');
			  				campoTextoTabla('f1Dos'.$key,$item['valorDos'],'input-mini pagination-right');
			  				$j=$i+1;
			  			echo '</tr>';
			  		}
			  		cierraBD();
	echo "		</tbody>
			</table> ";
	echo "
		</div><br/>";
}

function tablaFormacion2($datos){
	$campos=array(1=>'Formación de nivel básico (anexo IV R.D. 39/1997)',2=>'Formación para emergencias');
	echo "
		<div class='table-responsive centro'>
			<table class='table table-striped tabla-simple'>
			  	<thead>
			    	<tr>
			            <th rowspan='2'> FORMACIÓN DE LOS TRABAJADORES </th>
						<th rowspan='2' class='centro'> N.T. </th>
						<th colspan='5' class='centro'> NÚMERO DE ACTIVIDADES FORMATIVAS </th>
			    	</tr>
			    	<tr>
						<th class='centro'> TP </th>
						<th class='centro'> P </th>
						<th class='centro'> CD </th>
						<th class='centro'> CO </th>
						<th class='centro'> OTRAS </th>
			    	</tr>
			  	</thead>
			  	<tbody>";
			  		conexionBD();
			  		foreach ($campos as $key => $value) {
			  			$item=consultaBD('SELECT * FROM memorias_formacion_trabajadores2 WHERE codigoMemoria='.$datos['codigo'].' AND numeroInterno='.$key,false,true);
			  			echo '<tr>
			  				<td>'.$value.'</td>';
			  				campoTextoTabla('f2Uno'.$key,$item['valorUno'],'input-mini pagination-right');
			  				campoTextoTabla('f2Dos'.$key,$item['valorDos'],'input-mini pagination-right');
			  				campoTextoTabla('f2Tres'.$key,$item['valorTres'],'input-mini pagination-right');
			  				campoTextoTabla('f2Cuatro'.$key,$item['valorCuatro'],'input-mini pagination-right');
			  				campoTextoTabla('f2Cinco'.$key,$item['valorCinco'],'input-mini pagination-right');
			  				campoTextoTabla('f2Seis'.$key,$item['valorSeis'],'input-mini pagination-right');

			  				$j=$i+1;
			  			echo '</tr>';
			  		}
			  		cierraBD();
	echo "		</tbody>
			</table> ";
	echo "
		</div><br/>";
}

function tablaSiniestralidad($datos){
	$campos=array(1=>'Accidentes leves',2=>'Accidentes graves',3=>'Accidentes mortales',4=>'Enfermedades profesionales');
	$anio=date('Y');
	$anioAnt=intval($anio)-1;
	echo "
		<div class='table-responsive centro'>
			<table class='table table-striped tabla-simple mitadAncho'>
			  	<thead>
			    	<tr>
			            <th> SINIESTRALIDAD </th>
						<th class='centro'> ".$anioAnt." </th>
						<th class='centro'> ".$anio." </th>
			    	</tr>
			  	</thead>
			  	<tbody>";
			  		conexionBD();
			  		foreach ($campos as $key => $value) {
			  			$item=consultaBD('SELECT * FROM memorias_siniestralidad WHERE codigoMemoria='.$datos['codigo'].' AND numeroInterno='.$key,false,true);
			  			echo '<tr>
			  				<td>'.$value.'</td>';
			  				campoTextoTabla('sUno'.$key,$item['valorUno'],'input-mini pagination-right');
			  				campoTextoTabla('sDos'.$key,$item['valorDos'],'input-mini pagination-right');
			  				$j=$i+1;
			  			echo '</tr>';
			  		}
			  		cierraBD();
	echo "		</tbody>
			</table> ";
	echo "
		</div><br/>";
}

function tablaMaquinaria($datos){
	echo "
		<div class='table-responsive centro'>
			<table class='table table-striped tabla-simple mitadAncho' id='tablaMaquinarias'>
			  	<thead>
			    	<tr>
			            <th> Maquinaria </th>
						<th> Año </th>
						<th> Mes </th>
						<th> </th>
			    	</tr>
			  	</thead>
			  	<tbody>";
			  		$i=0;
			  		conexionBD();
			  		if($datos){
			  			$consulta=consultaBD("SELECT memorias_maquinarias.* FROM memorias_maquinarias INNER JOIN maquinaria_formulario_prl ON memorias_maquinarias.codigoMaquinaria=maquinaria_formulario_prl.codigo WHERE codigoMemoria=".$datos['codigo'].' ORDER BY maquinaria_formulario_prl.nombre ASC, anio ASC, mes ASC');
			  			while($item=mysql_fetch_assoc($consulta)){
			  				$item['maquinaria']=$item['codigoMaquinaria'];
			  				$item['anioM']=$item['anio'];
			  				$item['mesM']=$item['mes'];
			  				echo '<tr>';
			  				campoSelectMaquinaria('maquinaria',$i,$item,$datos);
			  				campoSelectAnio('anioM',$i,$item,$datos);
			  				campoSelectMes('mesM',$i,$item,$datos);
			  				$j=$i+1;
			  				echo "<td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td>";
			  				echo '</tr>';
			  				$i++;
			  			}
			  			if($i==0){
			  				echo '<tr>';
			  				campoSelectMaquinaria('maquinaria',$i,false,$datos);
			  				campoSelectAnio('anioM',$i,false,$datos);
			  				campoSelectMes('mesM',$i,false,$datos);
			  				$j=$i+1;
			  				echo "<td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td>";
			  				echo '</tr>';
			  			}
			  		} else {
			  			echo '<tr>';
			  			campoSelectMaquinaria('maquinaria',$i,false,false);
			  			campoSelectAnio('anioM',$i,false,false);
			  			campoSelectMes('mesM',$i,false,false);
			  			$j=$i+1;
			  			echo "<td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td>";
			  			echo '</tr>';
			  		}
			  		cierraBD();

	echo "		</tbody>
			</table>
			<br/>
				<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaMaquinarias\");'><i class='icon-plus'></i> Añadir</button> 
				<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaMaquinarias\");'><i class='icon-trash'></i> Eliminar</button> ";
	echo "
		</div>";
}

function campoSelectMaquinaria($nombreCampo,$i,$item,$datos){
	if($datos){
		campoSelectConsulta($nombreCampo.$i,'','SELECT m.codigo, m.nombre AS texto 
			FROM maquinaria_formulario_prl m 
			INNER JOIN formularioPRL ON m.codigoFormularioPRL=formularioPRL.codigo
			INNER JOIN contratos ON formularioPRL.codigoContrato=contratos.codigo
			INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
			WHERE codigoCliente='.$datos['codigoCliente'],$item[$nombreCampo],'selectpicker span4 show-tick selectMaquinaria',"data-live-search='true'",'',1);
	} else {
		campoSelect($nombreCampo.$i,'',array(''),array('NULL'),false,'selectpicker span3 show-tick selectMaquinaria',"data-live-search='true'",1);
	}
}

function campoSelectAnio($nombreCampo,$i,$item,$datos){
	$valores=array();
	for($j=2015;$j<date('Y');$j++){
		array_push($valores,$j);
	}
	campoSelect($nombreCampo.$i,'',$valores,$valores,$item[$nombreCampo],'selectpicker span2 show-tick',"data-live-search='true'",1);
}

function campoSelectMes($nombreCampo,$i,$item,$datos){
	campoSelect($nombreCampo.$i,'',array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'),array(0,1,2,3,4,5,6,7,8,9,10,11,12),$item[$nombreCampo],'selectpicker span2 show-tick',"data-live-search='true'",1);
}


function imprimeMemorias(){
	global $_CONFIG;
	$consulta=consultaBD("SELECT * FROM memorias ",true);
	while($datos=mysql_fetch_assoc($consulta)){
		echo "<tr>
				<td> ".formateaFechaWeb($datos['fechaCreacion'])." </td>
				<td> ".formateaFechaWeb($datos['fechaUltimaActualizacion'])."  ".formateaHoraWeb($datos['horaUltimaActualizacion'])."</td>
				<td class='centro'>
					<a href='".$_CONFIG['raiz']."/memorias/gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	  	</td>
			 </tr>";
			 /*botonAccionesConClase(array('Detalles','Descargar'),array('memorias/gestion.php?codigo='.$datos['codigo'],'memorias/generaMemoria.php?codigo='.$datos['codigo']),array("icon-search-plus",'icon-cloud-download'),array(0,0),true,'Acciones',array('','noAjax'))*/
	}
}

function obtenerMaquinaria(){
	$listado=consultaBD('SELECT m.codigo, m.nombre AS texto 
			FROM maquinaria_formulario_prl m 
			INNER JOIN formularioPRL ON m.codigoFormularioPRL=formularioPRL.codigo
			INNER JOIN contratos ON formularioPRL.codigoContrato=contratos.codigo
			INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
			WHERE codigoCliente='.$_POST['codigo'],true);
	$res='<option value="NULL"></option>';
	while($item=mysql_fetch_assoc($listado)){
		$res.='<option value="'.$item['codigo'].'">'.$item['texto'].'</option>';
	}
	echo $res;
}
