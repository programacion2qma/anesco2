<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión de epis


function operacionesEPIs(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('epis');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('epis');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaEmpleado('epis');
	}

	mensajeResultado('nombre',$res,'EPI');
    mensajeResultado('elimina',$res,'EPI', true);
}



function gestionPuesto(){
	operacionesEPIs();
	
	abreVentanaGestion('Gestión de EPIs','index.php');
	$datos=compruebaDatos('epis');
	
	$codigoCliente=obtieneCodigoClienteEPI($datos);
	$nombreCliente=obtieneNombreCliente($codigoCliente);

	campoDato('Cliente',$nombreCliente);
	campoTexto('nombre','Nombre',$datos,'span6');
	campoOculto($codigoCliente,'codigoCliente');
		
	cierraVentanaGestion('index.php?codigoCliente='.$codigoCliente);
}


function obtieneCodigoClienteEPI($datos){
	if(isset($_GET['codigoCliente'])){
		$res=$_GET['codigoCliente'];	
	}
	elseif($datos){
		$res=$datos['codigoCliente'];
	}
	else{
		$res='NULL';
	}

	return $res;
}

function imprimeEPIs($cliente){

	$consulta=consultaBD("SELECT * FROM epis WHERE codigoCliente=".$cliente.";",true);
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td>".$datos['nombre']."</td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-edit'></i> Editar</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
	        </tr>";
	}
}

//Fin parte de gestión de epis