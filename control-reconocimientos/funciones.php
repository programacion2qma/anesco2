<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de control de facturas de recos


function imprimeControl(){

	conexionBD();
	
	$consulta=consultaBD("SELECT contratos.codigo AS codigoContrato, contratos.codigoInterno, EMPID, contratos.fechaInicio, EMPCP, EMPNOMBRE, numRM 
						  FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
						  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
						  WHERE contratos.eliminado='NO' AND contratos.enVigor='SI' AND opcion IN(1,2);");

	while($datos=mysql_fetch_assoc($consulta)){
		
		if($datos['codigoContrato']!=NULL){
			$contrato="<a href='../contratos/gestion.php?codigo=".$datos['codigoContrato']."' class='noAjax' target='_blank'>".formateaReferenciaContrato($datos,$datos)."</a>";
		} 
		else {
			$contrato='';
		}

		$rm=consultaBD('SELECT COUNT(codigo) AS total FROM reconocimientos_medicos WHERE codigoContrato='.$datos['codigoContrato'],false,true);

		$fact=consultaBD('SELECT COUNT(reconocimientos_medicos_en_facturas.codigo) AS total 
						  FROM reconocimientos_medicos_en_facturas INNER JOIN facturas_reconocimientos_medicos ON reconocimientos_medicos_en_facturas.codigoFacturaReconocimientoMedico=facturas_reconocimientos_medicos.codigo 
						  WHERE codigoReconocimientoMedico IS NOT NULL AND codigoContrato='.$datos['codigoContrato'],false,true);

		$iconoRM='<i style="font-size:12px;" class="icon-check-circle iconoFactura icon-success"></i>';
		$iconoFact='<i style="font-size:12px;" class="icon-check-circle iconoFactura icon-success"></i>';

		if($rm['total']<$datos['numRM']){
			$iconoRM='<i style="font-size:12px;" class="icon-times-circle iconoFactura icon-danger"></i>';
		}
		if($rm['total']>$fact['total']){
			$iconoFact='<i style="font-size:12px;" class="icon-times-circle iconoFactura icon-danger"></i>';
		}

		echo "<tr>
				<td>".$contrato."</td>
				<td>".$datos['EMPNOMBRE']."</td>
				<td class='centro'>".$datos['numRM']."</td>
				<td class='centro'>".$rm['total']." ".$iconoRM."</td>
				<td class='centro'>".$fact['total']." ".$iconoFact."</td>
			</tr>";
	}

	cierraBD();
}



function estadisticasControl(){
	$res=array();

	conexionBD();


	$rm=consultaBD("SELECT COUNT(reconocimientos_medicos.codigo) AS total 
				    FROM reconocimientos_medicos INNER JOIN contratos ON reconocimientos_medicos.codigoContrato=contratos.codigo 
				    WHERE YEAR(contratos.fechaInicio)='".$_SESSION['ejercicio']."' AND contratos.eliminado='NO'",false,true);
	
	$res['ejecutados']=$rm['total'];

	$fact=consultaBD("SELECT COUNT(reconocimientos_medicos_en_facturas.codigo) AS total 
					  FROM reconocimientos_medicos_en_facturas 
					  INNER JOIN facturas_reconocimientos_medicos ON reconocimientos_medicos_en_facturas.codigoFacturaReconocimientoMedico=facturas_reconocimientos_medicos.codigo 
					  INNER JOIN contratos ON facturas_reconocimientos_medicos.codigoContrato=contratos.codigo
					  WHERE YEAR(contratos.fechaInicio)='".$_SESSION['ejercicio']."' AND contratos.eliminado='NO'",false,true);

	$res['facturados']=$fact['total'];

	cierraBD();

	return $res;
}

//Fin parte de control de facturas de recos