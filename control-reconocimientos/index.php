<?php
  $seccionActiva=30;
  include_once('../cabecera.php'); 

  $estadisticas=estadisticasControl();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas para el área de facturas de reconocimientos médicos:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-stethoscope"></i> <span class="value"> <?php echo $estadisticas['ejecutados']?></span><br />Ejecutados</div>
                     <div class="stat"> <i class="icon-eur"></i> <span class="value"> <?php echo $estadisticas['facturados']?></span><br />Facturados</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de control de reconocimientos médicos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                  <a href="../facturas-reconocimientos/" class="shortcut"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label">Volver</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

        <div class="span12">
          <div class="widget widget-table action-table">
              <div class="widget-header"> <i class="icon-list"></i>
                <h3>Control de reconocimientos médicos</h3>
              </div>
              <!-- /widget-header -->
              <div class="widget-content">
                <table class="table table-striped table-bordered datatable tablaTextoPeque" id="tablaFacturas">
                  <thead>
                    <tr>
                      <th> Contrato </th>
                      <th> Cliente </th>
                      <th> RM </th>
                      <th> Ejecutados </th>
                      <th> Facturados </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      imprimeControl();
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /widget-content-->
            </div>

        </div>
        
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/filtroTabla.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>