<?php
  $seccionActiva=5;
  include_once('../cabecera.php');
  preSeleccionEvaluacion();
?> 

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-select.js" type="text/javascript"></script>

<script src="../js/funcionesEvaluacion.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('select').selectpicker();

    oyenteTipoEvaluacion(false);
    oyenteTipoEvaluacion2(false);
    
    $('input[name=todo0]').change(function(){
        oyenteCheckTodo();
    });

    $('.checkTipo input[type=checkbox]').change(function(){
        oyenteCheckTipo($(this));
    });

    $('button[type=submit]').unbind();
    $('button[type=submit]').click(function(e){
        e.preventDefault();
        var continuar='SI';
        if($('#codigoCliente').val()=='NULL'){
            continuar='NO';
            alert("El campo 'Cliente' no puede estar vacío");
        } else if($('#tipoEvaluacion').val()==''){
            continuar='NO';
            alert("El campo 'Tipo' no puede estar vacío");
        } else if($('#tipoEvaluacion').val()=='LUGAR' && $('#codigoCentroTrabajo').val()=='NULL'){
            continuar='NO';
            alert("El campo 'Centro de trabajo' no puede estar vacío");
        } else if($('#tipoEvaluacion').val()=='PUESTOS' && $('#codigoPuestoTrabajo').val()=='NULL'){
            continuar='NO';
            alert("El campo 'Puesto de trabajo' no puede estar vacío");
        } else if($('#tipoEvaluacion').val()=='OTRAS' && $('#otros').val()==''){
            continuar='NO';
            alert("El campo 'Descripción otra' no puede estar vacío");
        } else if($('#tipo').val()==''){
            continuar='NO';
            alert("El campo 'Tipo de evaluación' no puede estar vacío");
        }

        if(continuar=='SI'){
            $('form').submit();
        }
    });
});

function oyenteCheckTodo(){
    if($('input[name=todo0]').prop('checked')){
        $('input[type=checkbox]').prop('checked',true);
    }
    else{
        $('input[type=checkbox]').prop('checked',false);   
    }
}

function oyenteCheckTipo(elem){
    var tipo=elem.attr('name').replace('check','.div');;
    if(elem.prop('checked')){
        $(tipo+' input[type=checkbox]').prop('checked',true);
    }
    else{
        $(tipo+' input[type=checkbox]').prop('checked',false);   
    }
}

</script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>