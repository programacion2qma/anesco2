<?php
  $seccionActiva=5;
  include_once("../cabecera.php");
  gestionEvaluacion();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>

<script src="../js/funcionesEvaluacion.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.tip').addClass('hide');

    oyenteTipoEvaluacion(false);
    oyenteTipoEvaluacion2(false);

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$('.selectpicker').selectpicker();

    $(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});

	$('.prioridad').prop('readonly', true);

    $('button[type=submit]').unbind();
    $('button[type=submit]').click(function(e){
        e.preventDefault();
        var continuar='SI';
        if($('#codigoCliente').val()=='NULL'){
            continuar='NO';
            alert("El campo 'Cliente' no puede estar vacío");
        } else if($('#tipoEvaluacion').val()==''){
            continuar='NO';
            alert("El campo 'Tipo' no puede estar vacío");
        } else if($('#tipoEvaluacion').val()=='LUGAR' && $('#codigoCentroTrabajo').val()=='NULL'){
            continuar='NO';
            alert("El campo 'Centro de trabajo' no puede estar vacío");
        } else if($('#tipoEvaluacion').val()=='PUESTOS' && $('#codigoPuestoTrabajo').val()=='NULL'){
            continuar='NO';
            alert("El campo 'Puesto de trabajo' no puede estar vacío");
        } else if($('#tipoEvaluacion').val()=='OTRAS' && $('#otros').val()==''){
            continuar='NO';
            alert("El campo 'Descripción otra' no puede estar vacío");
        } else if($('#tipo').val()==''){
            continuar='NO';
            alert("El campo 'Tipo de evaluación' no puede estar vacío");
        }

        if(continuar=='SI'){
            $('form').submit();
        }
    });
    
    $(document).on('change','.operadores',function() {
        var indice=$(this).attr('id').split('_');
        var riesgo=indice[1];
        indice='_'+indice[1]+'_'+indice[2];
        var total=0;
        if($('#probabilidad'+indice).val() != '0' && $('#consecuencias'+indice).val() != '0'){
            total = parseInt($('#probabilidad'+indice).val()) * parseInt($('#consecuencias'+indice).val());
        }
        var clasePanel='';
        var resultado = '';

        if(total==1){
            resultado = 'BAJA - RIESGO TRIVIAL';
            clasePanel='panel-default';
            $('#tablaGestiones'+indice+' .selectPlazo').val(13);
        }
        else if(total==2){
            resultado = 'MEDIA/BAJA - RIESGO TOLERABLE';
            clasePanel='panel-amarillo';
            $('#tablaGestiones'+indice+' .selectPlazo').val(12);
        }
        else if(total==3 || total==4){
            resultado = 'MEDIA/ALTA -  RIESGO MODERADO';
            clasePanel='panel-warning';
            $('#tablaGestiones'+indice+' .selectPlazo').val(6);
        }
        else if(total==6){
            resultado = 'ALTA - RIESGO IMPORTANTE';
            clasePanel='panel-danger';
            $('#tablaGestiones'+indice+' .selectPlazo').val(3);
        }
        else if(total>6){
            resultado = 'CARÁCTER INMEDIATO - RIESGO INTOLERABLE'; 
            clasePanel='panel-inverse';
            $('#tablaGestiones'+indice+' .selectPlazo').val(0);
        }
        $('#total'+indice).val(total);
        $('#prioridad'+indice).val(resultado);
        clasePanel=obtieneClasePanel(riesgo);
        cambiaColorPanel($(this),clasePanel);
    });

  $('.selectPlazo').change(function(){
        //oyentePlazos($(this));
    });

});

function obtieneClasePanel(riesgo){
    var max=0;
    var i=0;
    var clasePanel='';
    while($('#total_'+riesgo+'_bloque'+i).length){
        if($('#total_'+riesgo+'_bloque'+i).val()>max){
            max=$('#total_'+riesgo+'_bloque'+i).val();
        }
        i++;
    }
    if(max==1){
        clasePanel='panel-default';
    }
    else if(max==2){
        clasePanel='panel-amarillo';
    }
    else if(max==3 || max==4){
        clasePanel='panel-warning';
    }
    else if(max==6){
        clasePanel='panel-danger';
    }
    else if(max>6){
        clasePanel='panel-inverse';
    }
    return clasePanel;

}
function cambiaColorPanel(boton,nuevaClase){
    var panel=boton.parent().parent().parent().parent().parent().parent().parent().parent().parent().parent();//9 saltos hacia arriba, siguiendo el árbol DOM.
    var claseAnterior=panel.attr('class');
    panel.removeClass(claseAnterior).addClass('panel '+nuevaClase);
}

function oyentePlazos(elem){
    var id = elem.attr('name');
    id=id.split('_');
    id=id[2].replace('riesgo','');
    valor=$('#total'+id).val();
    if(valor == 1){
        elem.val(13);
    } else if(valor==2){
        elem.val(12);
    } else if(valor==3 || valor==4){
        elem.val(6);
    } else if(valor==6){
        elem.val(3);
    } else if(valor>6){
        elem.val(0);
    }
    $(elem).selectpicker('refresh');
}

function insertaFilaGestion(tabla){
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla+" > tbody > tr:last").clone();

    //Si existe un campo de descarga, creo un input file junto a él
    var nombreDescarga=$tr.find('.descargaFichero').attr('nombre');
    $tr.find('.descargaFichero').after("<input type='file' name='"+nombreDescarga+"' id='"+nombreDescarga+"' />");

    //Obtengo el atributo name para los inputs y selects
    $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        return obtenerName(this.name,true);
    }).attr("id", function(){//Hago lo mismo con los IDs
        return obtenerID(this.id,true);
    });

    $tr.find(".table").attr("id", function(){//Para el identificador de la subtabla padre
        parts=this.id.split('_');
        var parts1 = parts[0].match(/(\D+)(\d*)$/);
        return parts1[1] + ++parts1[2] +'_'+parts[1];
    });

    $tr.find(".btn-success").attr("onclick", function(){//Para los botones de añadir alumno
        parts=$(this).attr('onclick').split('_');
        var parts1 = parts[0].match(/(\D+)(\d*)$/);
        return "insertaFila2"+parts1[1] + ++parts1[2] +'_'+parts[1];
    });

    $tr.find(".btn-danger").attr("onclick", function(){//Para los botones de añadir alumno
        parts=$(this).attr('onclick').split('_');
        var parts1 = parts[0].match(/(\D+)(\d*)$/);
        return "eliminarFila2"+parts1[1] + ++parts1[2] +'_'+parts[1];
    });


    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker)").attr("id", function(){//Para los botones de cálculo
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value","");
    $tr.find("select").val("NULL");
    $tr.find('.bootstrap-select').remove();//Eliminación de los selectpicker
    $tr.find('.bootstrap-filestyle').remove();//Eliminación de los filestyle
    $tr.find('.descargaFichero').remove();//Eliminación del enlace de descarga, si lo hubiere
    $tr.find(".existeGestion").val("NO");
    //Añado la nueva fila a la tabla
    $('#'+tabla+" > tbody > tr:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $tr.find(".selectpicker").selectpicker('refresh');
    }
    if(typeof jQuery.fn.filestyle=='function'){//Misma operación para el filestyle
        $tr.find(':file').filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    }
    if(typeof jQuery.fn.rating=='function'){//Misma operación para el input-rating
        var campoRating=$tr.find('.rating');//La librería bootstrap-rating-input mete el input dentro de un div, por lo que primero rescato el input...
        $tr.find('.rating-input').replaceWith(campoRating);//... y sustituyo el div por él (si no crearía 2 filas de estrellas)
        campoRating.rating();//Inicializo la librería sobre el input
    }

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $($tr.find('.selectPlazo')).each(function(){
        oyentePlazos($(this));
    });
}

function eliminaFilaGestion(tabla){
  if($('#'+tabla).find("tbody tr").length>1){//Si la tabla tiene más de 1 fila...
    var filasSeleccionadas=0;
    $('input[name="filasTabla[]"]:checked').each(function() {
        var fila=$(this).val();
        $('#'+tabla+' tr').eq(fila).remove();
        filasSeleccionadas++;
    });

    if(filasSeleccionadas>0){
        //Renumeración de los índices de la tabla
        var numFilas=$('#'+tabla+' tr:not(:first)').length;
        for(i=0;i<numFilas;i++){
           $('#'+tabla+' tr:not(:first)').eq(i).find("input:not([type=checkbox],.input-block-level),select,textarea").attr("name", function(){
                //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
                var parts = this.id.match(/(\D+)(\d*)$/);
                //Creo un nombre nuevo incrementando el número de fila (++parts[2])
                return parts[1] + i;
                //Hago lo mismo con los IDs
            }).attr("id", function(){
                var parts = this.id.match(/(\D+)(\d*)$/);
                return parts[1] + i;
            });

            $('#'+tabla+' tr:not(:first)').eq(i).find("input[type=checkbox]").attr("value", function(){
                var j=i+1
                return j;
            });
        }
        //Fin renumeración
    }
    else{
        alert('Para eliminar un registro, debe seleccionarlo utilizando el último check que tiene a su derecha.');
    }
  }
  else{
    alert('La tabla debe de contar con al menos 1 fila.');
  }
}

function insertaFila2(tabla){
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla).find("tbody .trMedida:last").clone();
    //Obtengo el atributo name para los inputs y selects
   $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        return obtenerName(this.name);
    }).attr("id", function(){//Hago lo mismo con los IDs
        return obtenerID(this.id);
    });


    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker)").attr("id", function(){//Para los botones de cálculo
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value",""); //MODIFICACION OFICINA 12/06/2015
    $tr.find('.bootstrap-select').remove();

    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody .trMedida:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $('#'+tabla).find(".selectpicker").selectpicker('refresh');
    }
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    //oyentePlazos($tr.find('.selectPlazo'));
}

function eliminaFila2(tabla){
  if($('#'+tabla).find("tbody .trMedida").length>1){//Si la tabla tiene más de 1 fila...
    var filasSeleccionadas=0;
    $('input[name="filasTabla[]"]:checked').each(function() {
        var fila=$(this).val();
        $('#'+tabla+' .trMedida').eq(fila).remove();
        filasSeleccionadas++;
    });

    if(filasSeleccionadas>0){
        //Renumeración de los índices de la tabla
        var numFilas=$('#'+tabla+' .trMedida:not(:first)').length;
        for(i=0;i<numFilas;i++){
           $('#'+tabla+' .trMedida:not(:first)').eq(i).find("input:not([type=checkbox],.input-block-level),select,textarea").attr("name", function(){
                //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
                var parts = this.id.match(/(\D+)(\d*)$/);
                //Creo un nombre nuevo incrementando el número de fila (++parts[2])
                return parts[1] + i;
                //Hago lo mismo con los IDs
            }).attr("id", function(){
                var parts = this.id.match(/(\D+)(\d*)$/);
                return parts[1] + i;
            });

            $('#'+tabla+' .trMedida:not(:first)').eq(i).find("input[type=checkbox]").attr("value", function(){
                var j=i+1
                return j;
            });
        }
        //Fin renumeración
      }
  }
  else{
    alert('Para eliminar un registro, debe seleccionarlo utilizando el último check que tiene a su derecha.');
  }
}

function obtenerName(name,losDos=false){
  var parts = name.split('_');
  var parts1 = parts[0].match(/(\D+)(\d*)$/);
  var parts2 = parts[1].match(/(\D+)(\d*)$/);
  if(name.indexOf("_gestion")>0){
    if(name.indexOf("[]")>0){
      name = parts1[1] + ++parts1[2] +'_'+parts[1]+'_'+parts[2] + '[]';
    }else{
      if(losDos){
        name = parts[0] +'_'+parts2[1] + ++parts2[2] +'_'+parts[2];
      } else {
        name = parts1[1] + ++parts1[2] +'_'+parts[1]+'_'+parts[2];
      }
    }
  } else {
    if(name.indexOf("[]")>0){
      name = parts1[1] + ++parts1[2] +'_'+parts[1] + '[]';
    }else{
      name = parts1[1] + ++parts1[2] +'_'+parts[1];
    }
  }
  return name;
}

function obtenerID(id,losDos=false){
  var parts = id.split('_');
  var parts1 = parts[0].match(/(\D+)(\d*)$/);
  var parts2 = parts[1].match(/(\D+)(\d*)$/);
  if(id.indexOf("_gestion")>0){
    if(losDos){
      id = parts[0] +'_'+parts2[1] + ++parts2[2] +'_'+parts[2];
    } else {
      id = parts1[1] + ++parts1[2] +'_'+parts[1];
    }
  }
  return id;
}

function insertaFilaDescripcion(tabla){
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla).find("tbody .trDescripcion:last").clone();
    //Obtengo el atributo name para los inputs y selects
   $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        if(this.type=='file'){
            var nombre=this.name.split('_');
            var desc = nombre[1].match(/(\D+)(\d*)$/);
            desc = desc[1] + ++desc[2];
            nombre=nombre[0]+'_'+desc+'_'+nombre[2];
        } else {
            var nombre=obtenerName(this.name);
        }
        return nombre;
    }).attr("id", function(){//Hago lo mismo con los IDs
        return this.name;
    });


    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker)").attr("id", function(){//Para los botones de cálculo
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    var idTabla=$tr.find("table").attr("id");
    idTabla=idTabla.split('_');
    var principioTabla = idTabla[0].match(/(\D+)(\d*)$/);
    principioTabla = principioTabla[1] + ++principioTabla[2];
    idTabla=principioTabla+'_'+idTabla[1];
    $tr.find("table").attr("id",idTabla);
    $tr.find(".btn-success").attr("onclick",'insertaFilaDescripcionFicheros("'+idTabla+'");')
    $tr.find(".btn-danger").attr("onclick",'eliminaFilaDescripcionFicheros("'+idTabla+'");')

    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value",""); //MODIFICACION OFICINA 12/06/2015
    $tr.find('.bootstrap-select').remove();
    $tr.find('.bootstrap-filestyle').remove();

    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody .trDescripcion:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $('#'+tabla).find(".selectpicker").selectpicker('refresh');
    }
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    if(typeof jQuery.fn.filestyle=='function'){//Misma operación para el filestyle
        $tr.find(':file').filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    }
}

function eliminaFilaDescripcion(tabla){
  if($('#'+tabla).find(".trDescripcion").length>1){//Si la tabla tiene más de 1 fila...
    var filasSeleccionadas=0;
    $('input[name="filasTabla[]"]:checked').each(function() {
        var fila=$(this).val();
        $('#'+tabla).find(".trDescripcion").eq(fila).remove();
        filasSeleccionadas++;
    });

    if(filasSeleccionadas>0){
        //Renumeración de los índices de la tabla
        var numFilas=$('#'+tabla).find(".trDescripcion").length;
        for(i=0;i<numFilas;i++){
           $('#'+tabla).find(".trDescripcion").eq(i).find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
                var parts = this.id.split('_');
                var principio = parts[0].match(/(\D+)(\d*)$/);
                principio = principio[1] + i;
                return principio+'_'+parts[1];
            }).attr("id", function(){
                return this.name;
            });

            $('#'+tabla).find(".trDescripcion").eq(i).find("input[name='filasTabla[]']").attr("value", function(){
                var j=i
                return j;
            });
        }
        //Fin renumeración
    }
    else{
        alert('Para eliminar un registro, debe seleccionarlo utilizando el último check que tiene a su derecha.');
    }
  }
  else{
    alert('La tabla debe de contar con al menos 1 fila.');
  }
}

function insertaFilaDescripcionFicheros(tabla){
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla).find("tbody .trDescripcionFicheros:last").clone();
    //Obtengo el atributo name para los inputs y selects
   $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        parts = this.name.split('_');
        var principio = parts[0].match(/(\D+)(\d*)$/);
        principio = principio[1] + ++principio[2];
        return principio+'_'+parts[1]+'_'+parts[2];
    }).attr("id", function(){//Hago lo mismo con los IDs
        return this.name;
    });


    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker)").attr("id", function(){//Para los botones de cálculo
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value",""); //MODIFICACION OFICINA 12/06/2015
    $tr.find('.bootstrap-select').remove();
    $tr.find('.bootstrap-filestyle').remove();

    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody .trDescripcionFicheros:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $('#'+tabla).find(".selectpicker").selectpicker('refresh');
    }
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    if(typeof jQuery.fn.filestyle=='function'){//Misma operación para el filestyle
        $tr.find(':file').filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    }
}

function eliminaFilaDescripcionFicheros(tabla){
  if($('#'+tabla).find(".trDescripcionFicheros").length>1){//Si la tabla tiene más de 1 fila...
    var filasSeleccionadas=0;
    $('input[name="filasTabla[]"]:checked').each(function() {
        var fila=$(this).val();
        $('#'+tabla).find(".trDescripcionFicheros").eq(fila).remove();
        filasSeleccionadas++;
    });

    if(filasSeleccionadas>0){
        //Renumeración de los índices de la tabla
        var numFilas=$('#'+tabla).find(".trDescripcionFicheros").length;
        for(i=0;i<numFilas;i++){
           $('#'+tabla).find(".trDescripcionFicheros").eq(i).find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
                var parts = this.name.split('_');
                var principio = parts[0].match(/(\D+)(\d*)$/);
                principio = principio[1] + i;
                return principio+'_'+parts[1]+'_'+parts[2];
            }).attr("id", function(){
                return this.name;
            });

            $('#'+tabla).find(".trDescripcionFicheros").eq(i).find("input[name='filasTabla[]']").attr("value", function(){
                var j=i
                return j;
            });
        }
        //Fin renumeración
    }
    else{
        alert('Para eliminar un registro, debe seleccionarlo utilizando el último check que tiene a su derecha.');
    }
  }
  else{
    alert('La tabla debe de contar con al menos 1 fila.');
  }
}

function insertaFilaMedidas(tabla){
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla).find("tbody .trMedida:last").clone();
    //Obtengo el atributo name para los inputs y selects
   $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        if(this.type=='file'){
            var nombre=this.name.split('_');
            var desc = nombre[1].match(/(\D+)(\d*)$/);
            desc = desc[1] + ++desc[2];
            nombre=nombre[0]+'_'+desc+'_'+nombre[2]+'_'+nombre[3];
        } else {
            var nombre=obtenerName(this.name);
        }
        return nombre;
    }).attr("id", function(){//Hago lo mismo con los IDs
        return this.name;
    });


    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker)").attr("id", function(){//Para los botones de cálculo
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    var idTabla=$tr.find("table").attr("id");
    idTabla=idTabla.split('_');
    var principioTabla = idTabla[0].match(/(\D+)(\d*)$/);
    principioTabla = principioTabla[1] + ++principioTabla[2];
    idTabla=principioTabla+'_'+idTabla[1]+'_'+idTabla[2];
    $tr.find("table").attr("id",idTabla);
    $tr.find(".btn-success").attr("onclick",'insertaFilaMedidasFicheros("'+idTabla+'");')
    $tr.find(".btn-danger").attr("onclick",'eliminaFilaMedidasFicheros("'+idTabla+'");')

    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value",""); //MODIFICACION OFICINA 12/06/2015
    $tr.find('.bootstrap-select').remove();
    $tr.find('.bootstrap-filestyle').remove();

    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody .trMedida:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $('#'+tabla).find(".selectpicker").selectpicker('refresh');
    }
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    if(typeof jQuery.fn.filestyle=='function'){//Misma operación para el filestyle
        $tr.find(':file').filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    }
}

function insertaFilaMedidasFicheros(tabla){
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla).find("tbody .trDescripcionFicheros:last").clone();
    //Obtengo el atributo name para los inputs y selects
   $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        parts = this.name.split('_');
        var principio = parts[0].match(/(\D+)(\d*)$/);
        principio = principio[1] + ++principio[2];
        return principio+'_'+parts[1]+'_'+parts[2]+'_'+parts[3];
    }).attr("id", function(){//Hago lo mismo con los IDs
        return this.name;
    });


    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker)").attr("id", function(){//Para los botones de cálculo
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value",""); //MODIFICACION OFICINA 12/06/2015
    $tr.find('.bootstrap-select').remove();
    $tr.find('.bootstrap-filestyle').remove();

    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody .trDescripcionFicheros:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $('#'+tabla).find(".selectpicker").selectpicker('refresh');
    }
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    if(typeof jQuery.fn.filestyle=='function'){//Misma operación para el filestyle
        $tr.find(':file').filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    }
}

function eliminaFilaMedidasFicheros(tabla){
  if($('#'+tabla).find(".trDescripcionFicheros").length>1){//Si la tabla tiene más de 1 fila...
    var filasSeleccionadas=0;
    $('input[name="filasTabla[]"]:checked').each(function() {
        var fila=$(this).val();
        $('#'+tabla).find(".trDescripcionFicheros").eq(fila).remove();
        filasSeleccionadas++;
    });

    if(filasSeleccionadas>0){
        //Renumeración de los índices de la tabla
        var numFilas=$('#'+tabla).find(".trDescripcionFicheros").length;
        for(i=0;i<numFilas;i++){
           $('#'+tabla).find(".trDescripcionFicheros").eq(i).find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
                var parts = this.name.split('_');
                var principio = parts[0].match(/(\D+)(\d*)$/);
                principio = principio[1] + i;
                return principio+'_'+parts[1]+'_'+parts[2]+'_'+parts[3];
            }).attr("id", function(){
                return this.name;
            });

            $('#'+tabla).find(".trDescripcionFicheros").eq(i).find("input[name='filasTabla[]']").attr("value", function(){
                var j=i
                return j;
            });
        }
        //Fin renumeración
    }
    else{
        alert('Para eliminar un registro, debe seleccionarlo utilizando el último check que tiene a su derecha.');
    }
  }
  else{
    alert('La tabla debe de contar con al menos 1 fila.');
  }
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>