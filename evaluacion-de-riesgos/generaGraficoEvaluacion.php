<?php
  $seccionActiva=5;
  include_once("../cabecera.php");
  $totales=gestionGraficos();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/chart.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>js/Chart.HorizontalBar.js" type="text/javascript"></script>

<script type="text/javascript">
  <?php
    generaGraficosRiesgos($totales);
  ?>

  function convierteImagen(contador){
    var base64=document.getElementById("cajaGrafico"+contador).toDataURL();//Convierte el gráfico a imagen en base64
    var base64=base64.replace(/^data:image\/(png|jpg);base64,/, "");//Para quitar tipo en cabecera fichero
    $("#graficoBarra"+contador).val(base64);
  }
</script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>