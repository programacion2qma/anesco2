<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de evaluación de riesgos


function operacionesEvaluaciones(){
	$res=true;

	if(isset($_POST['codigoEvaluacion'])){
		$res=actualizaBloques();
	}
	elseif(isset($_POST['codigo'])){
		$res=actualizaEvaluacionRiesgo();
	}
	elseif(isset($_POST['fechaEvaluacion'])){
		$res=creaEvaluacionRiesgo();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaEvaluacion();
	}

	mensajeResultado('fechaEvaluacion',$res,'Evaluación');
    mensajeResultado('elimina',$res,'Evaluación', true);
}


function actualizaBloques(){
	$res=true;
	$codigoEvaluacion=$_POST['codigoEvaluacion'];
	$riesgos=$_POST['codigoRiesgo'];
	conexionBD();
	$notIn='(0';
	foreach ($riesgos as $value) {
		$riesgo=consultaBD('SELECT * FROM riesgos_evaluacion_general WHERE codigoRiesgo='.$value.' AND codigoEvaluacion='.$codigoEvaluacion,false,true);
		if($riesgo){
			$riesgo=$riesgo['codigo'];
		} else {
			$res=consultaBD('INSERT INTO riesgos_evaluacion_general VALUES(NULL,'.$value.','.$codigoEvaluacion.');');
			$riesgo=mysql_insert_id();
		}
		$notIn.=','.$riesgo;
		$cantidad=$_POST['cantidad'.$value]==0?1:$_POST['cantidad'.$value];
		$bloques=consultaBD('SELECT COUNT(codigo) AS total FROM riesgos_evaluacion_general_bloques WHERE codigoRiesgo='.$riesgo,false,true);
		$bloques=$bloques['total'];
		if($cantidad>$bloques){
			for($i=$bloques;$i<$cantidad;$i++){
				$res=consultaBD('INSERT INTO riesgos_evaluacion_general_bloques VALUES (NULL,'.$riesgo.',"","","","","","","","")');
			}
		} else if($cantidad<$bloques){
			$listadoBloques=consultaBD('SELECT codigo FROM riesgos_evaluacion_general_bloques WHERE codigoRiesgo='.$riesgo.' ORDER BY codigo DESC');
			while($cantidad<$bloques){
				$b=mysql_fetch_assoc($listadoBloques);
				$res=consultaBD('DELETE FROM riesgos_evaluacion_general_bloques WHERE codigo='.$b['codigo']);
				$bloques--;
			}
		}
	}
	$notIn.=')';
	$res=consultaBD('DELETE FROM riesgos_evaluacion_general WHERE codigo NOT IN '.$notIn.' AND codigoEvaluacion='.$codigoEvaluacion);
	cierraBD();
}


function gestionEvaluacion(){
	//Si hay que guardarlo en la misma página hay que tener cuidado con la eliminación de riesgos.
	operacionesEvaluaciones();

	if(isset($_POST['codigo'])){
		$_REQUEST['codigo'] = $_POST['codigo'];
	} else if(isset($_POST['codigoEvaluacion'])){
		$_REQUEST['codigo'] = $_POST['codigoEvaluacion'];
	}
	

	abreVentanaGestionConBotonesEvaluacion('Gestión de Evaluaciones de Riesgo','?','span3','icon-edit','',true,'noAjax','index.php',true,'Continuar','icon-chevron-right');

	$datos=compruebaDatosEvaluacion();
	campoOculto('','graficoBarraGlobal');
	campoSelectConsulta('codigoCliente','Cliente',"SELECT codigo, CONCAT(EMPNOMBRE,' (',EMPMARCA,')') AS texto FROM clientes WHERE activo='SI' ORDER BY EMPNOMBRE",$datos);
	campoSelect('tipoEvaluacion','Tipo',array('','Lugar de trabajo','Puestos','Otra'),array('','LUGAR','PUESTOS','OTRAS'),$datos,'selectpicker span3 show-tick','');
	campoFichero('ficheroEvaluacion','Documentación',0,$datos,'../documentos/riesgos/');
	cierraColumnaCampos();
	abreColumnaCampos('span6');
	
	campoFecha('fechaEvaluacion','Fecha de evaluación',$datos);
	campoCentroTrabajoPreseleccion($datos);
	campoPuestoTrabajoPreseleccion($datos);
	campoOtrosPreseleccion($datos);

	cierraColumnaCampos(true);

	abreColumnaCampos();

	campoSelect('tipo','Tipo de evaluación',array('','INICIAL','PERIÓDICA','OTROS'),array('','INICIAL','PERIODICA','OTROS'),$datos,'selectpicker span3 show-tick','');
	campoTexto('motivo','Motivo',$datos);

	cierraColumnaCampos();

	abreColumnaCampos();

	echo '<div id="divOtroTipo" class="hide">';
	campoTexto('otroTipo','Otro',$datos);
	echo '</div>';

	cierraColumnaCampos();

	echo '<br clear="all">';
	if(!$datos || !isset($datos['checkActivo'])){
		$datos['checkActivo']='SI';
	}
	campoCheckIndividual('checkActivo','Activo para el informe',$datos);
	campoCheckIndividual('checkVisible','Visible',$datos);
	abreColumnaCampos('sinFlotar');
	echo "<h3 class='apartadoFormulario'>Evaluación de Riesgo</h3>";
	cierraColumnaCampos();

	
	creaCuestionariosEvaluacionDelitos($datos);

	cierraVentanaGestion('index.php?codigoCliente='.$datos['codigoCliente'],true,true,'Guardar','icon-check');
}

function creaCuestionariosEvaluacionDelitos($datos){
	echo "<div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true'>";
	
	conexionBD();
	//$consulta=consultaBD("SELECT codigo, nombre, descripcion FROM riesgos ORDER BY nombre");

	$i=1;
	if(isset($_POST['codigoRiesgo']) && !isset($_POST['codigoEvaluacion'])){
		$listado = $_POST['codigoRiesgo'];
		$riesgos = array();
		foreach ($listado as $value){
			$riesgos[$value]=$_POST['cantidad'.$value];
			campoOculto('NO','existeRiesgo'.$i);
			campoOculto($value,'codigoRiesgo'.$i);
			$i++;
		}
	} else {
		$consulta = consultaBD("SELECT riesgos_evaluacion_general.codigo, riesgos_evaluacion_general.codigoRiesgo FROM riesgos_evaluacion_general INNER JOIN riesgos ON riesgos_evaluacion_general.codigoRiesgo=riesgos.codigo WHERE codigoEvaluacion=".$datos['codigo']." GROUP BY codigoRiesgo ORDER BY tipo ASC, codigoInterno");
		while($reg=mysql_fetch_assoc($consulta)){
			$riesgos[$i] = $reg['codigoRiesgo'];
			if(isset($_GET['duplicar'])){
				campoOculto('NO','existeRiesgo'.$i);
			} else {
				campoOculto($reg['codigo'],'existeRiesgo'.$i);
			}
			campoOculto($reg['codigoRiesgo'],'codigoRiesgo'.$i);
			$i++;
		}
	}
	$i=1;
	if($datos['codigo']!=false){	
		if(isset($riesgos)){
			$rep=array();
			foreach ($riesgos as $value){
				$riesgo = consultaBD("SELECT * FROM riesgos a WHERE a.codigo= ".$value,false,true);
				if(array_key_exists($value,$rep)){
					$rep[$value]++;
				} else {
					$rep[$value]=1;
				}
				creaTablaRiesgo($riesgo,$datos,$i,$rep[$value]);
				$i++;
			}
		}
	} else {
		foreach ($riesgos as $key => $value){
			$riesgo = consultaBD("SELECT * FROM riesgos a WHERE a.codigo= ".$key,false,true);
			creaTablaRiesgoSinDatos($riesgo,$i,$value);
			$i++;
		}
	}
	cierraBD();

	echo "</div>";
}

function creaTablaRiesgo($riesgo,$datos,$contador,$rep){
	$listado=obtieneDatosRiesgoEvaluacion($riesgo['codigo'],$datos['codigo'],$rep);
	$tipo=datosRegistro('riesgos_tipos',$riesgo['tipo']);
	$tipo=strtoupper(substr($tipo['nombre'], 0,3));
	$numero=$contador < 10 ? '0'.$contador:$contador;
	abreTablaRiesgo($contador,$tipo.$numero.' - '.$riesgo['nombre'],$datos,$riesgo['codigo']);
	$j=0;
	echo '<table id="tablaRiesgos'.$contador.'">
		<tbody>';
	while($datosRiesgo=mysql_fetch_assoc($listado)){
		lineaTablaRiesgo($datosRiesgo,$contador,$j);
		$j++;
	}
	echo "</tbody></table>
	<br/>
	<center>
		<button type='button' class='btn btn-small btn-success' onclick='insertaFilaEvaluacion(\"tablaRiesgos".$contador."\");'><i class='icon-plus'></i> Añadir bloque</button>
	</center>";
	cierraTablaDelito();
}

function creaTablaRiesgoSinDatos($riesgo,$contador,$bloques){
	$tipo=datosRegistro('riesgos_tipos',$riesgo['tipo']);
	$tipo=strtoupper(substr($tipo['nombre'], 0,3));
	$numero=$contador < 10 ? '0'.$contador:$contador;
	abreTablaRiesgo($contador,$tipo.$numero.' - '.$riesgo['nombre'],false,$riesgo['codigo']);
	echo '<table id="tablaRiesgos'.$contador.'">
		<tbody>';
	for($j=0;$j<$bloques;$j++){
		lineaTablaRiesgo(false,$contador,$j);
	}
	echo "</tbody></table>
	<br/>
	<center>
		<button type='button' class='btn btn-small btn-success' onclick='insertaFilaEvaluacion(\"tablaRiesgos".$contador."\");'><i class='icon-plus'></i> Añadir bloque</button>
	</center>";
	cierraTablaDelito();
}

function lineaTablaRiesgo($datosRiesgo,$contador,$j){
	echo '<tr><td style="border-bottom:2px solid #000;">';
	echo "<br/><button type='button' id='btnEliminar_riesgo".$contador."_bloque".$j."' class='btn btn-small btn-danger' style='float:right' onclick='eliminaFilaEvaluacion(\"tablaRiesgos".$contador."\",this);'><i class='icon-trash'></i> Eliminar bloque</button><br/>";
	if($datosRiesgo && !isset($_GET['duplicar'])){
		campoOculto($datosRiesgo['codigo'],'existeBloque'.$j.'_riesgo'.$contador);
	} else {
		campoOculto('NO','existeBloque'.$j.'_riesgo'.$contador);
	}
	creaTablaDescripciones($contador,$j,$datosRiesgo);
	abreColumnaCampos('colEvaluacionDelito');

		$descripcion = $datosRiesgo['descripcionRiesgo'];

		areaTexto('descripcionRiesgo_riesgo'.$contador.'_bloque'.$j,'Factor de riesgo',$descripcion);
		campoOculto('','graficoBarra_riesgo'.$contador.'_bloque'.$j);
	cierraColumnaCampos();

	abreColumnaCampos('colEvaluacionDelito');
		campoSelect('probabilidad_riesgo'.$contador.'_bloque'.$j,'Probabilidad',array('','BAJA','MEDIA','ALTA'),array('0','1','2','3'),$datosRiesgo['probabilidad'],'selectpicker span3 show-tick operadores');
		campoSelect('consecuencias_riesgo'.$contador.'_bloque'.$j,'Consecuencias',array('','LIGERAMENTE DAÑINO','DAÑINO','EXTREMADAMENTE DANIÑO'),array('0','1','2','3'),$datosRiesgo['consecuencias'],'selectpicker span3 show-tick operadores');
	cierraColumnaCampos();


	abreColumnaCampos('colEvaluacionDelito');
		campoOculto($datosRiesgo['total'],'total_riesgo'.$contador.'_bloque'.$j);
		campoTexto('prioridad_riesgo'.$contador.'_bloque'.$j,'Nivel de riesgo',strtoupper($datosRiesgo['prioridad']),'span4 prioridad');
	cierraColumnaCampos();

	echo "<h3 class='apartadoFormulario'>Medidas preventivas</h3>";

	creaTablaRecomendacionesRiesgo($contador,$j,$datosRiesgo);
	echo "</td></tr>";
}

function abreTablaRiesgo($codigo,$nombre,$datos,$codigoRiesgo){
	$clase=obtieneClasePanelDelito($datos,$codigoRiesgo);
	echo "<div class='panel $clase'>
            <div class='panel-heading' role='tab' id='cabecera$codigo'>
              <h3 class='panel-title'>
                <a data-toggle='collapse' data-parent='#accordion' href='#delito$codigo' aria-expanded='true' aria-controls='delito$codigo' class='noAjax'>
                  <i class='icon-angle-right'></i> $nombre
                </a>
              </h3>
            </div>
            <div id='delito$codigo' class='panel-collapse collapse' role='tabpanel' aria-labelledby='cabecera$codigo'>
                <div class='panel-body'>";
}

function cierraTablaDelito(){
	echo 	  	"</div>
            </div>
         </div>";
}

function creaTablaDescripciones($riesgo,$j,$datos=false){
	echo "
	<br />
	<table class='table table-striped table-bordered' id='tablaDescripciones_riesgo".$riesgo."'>
      <thead>
        <tr>
          <th> Descripción</th>
          <th> </th>
        </tr>
      </thead>
      <tbody>";

  		imprimeCamposTablaDescripciones($j,$riesgo,$datos);

    echo "
      </tbody>
    </table><br />";
}

function imprimeCamposTablaDescripciones($j,$riesgo,$datos=false){
	$textos='';
	$valores='';
	echo "<tr class='trDescripcion'>";
	areaTextoTabla('descripcion2_riesgo'.$riesgo.'_bloque'.$j,$datos['descripcion2'],'areaTextoTablaDescripciones');
	creaTablaDescripcionesFicheros($riesgo,$j,$datos);
	echo "</tr>";
}

function creaTablaDescripcionesFicheros($riesgo,$j,$datos=false){
	echo "
	<td>
	<table class='table table-striped table-bordered tablaImagenes' id='tablaDescripciones".$riesgo."_bloque".$j."'>
      <thead>
        <tr>
          <th> Imágenes </th>
          <th> </th>
        </tr>
      </thead>
      <tbody>";
  	
  		$i=0;
  		$k=0;
  		conexionBD();
  		if($datos!=false && !isset($_GET['duplicar'])){
  			$consulta=consultaBD("SELECT * FROM evaluacion_imagenes WHERE codigoDescripcion='".$datos['codigo']."'",true);
  			while($datosF=mysql_fetch_assoc($consulta)){
				imprimeCamposTablaDescripcionesFicheros($riesgo,$j,$i,$k,$datosF);
				$i++;
				$k++;
  			}
  		}
  		

  		imprimeCamposTablaDescripcionesFicheros($riesgo,$j,$i,$k);
  	
  		cierraBD();

    echo "
      </tbody>
    </table>
    <center>
		<button type='button' id='anadirMedida' class='gestionMedida btn btn-small btn-success' onclick='insertaFilaImagenes(\"tablaDescripciones".$riesgo."_bloque".$j."\");'><i class='icon-plus'></i> Añadir imagen</button>
		<button type='button' id='eliminarrMedida' class='gestionMedida btn btn-small btn-danger' onclick='eliminaFilaImagenes(\"tablaDescripciones".$riesgo."_bloque".$j."\");'><i class='icon-trash'></i> Eliminar imagen</button>
	</center></td>";
}


function imprimeCamposTablaDescripcionesFicheros($riesgo,$j=0,$i=0,$k=0,$datos=false){
	echo "<tr class='trDescripcionFicheros'>";
	$nombre='riesgo'.$riesgo.'_bloque'.$j.'_imagenDescripcion'.$i;
	if($datos!=false){
		$nombre='riesgo'.$riesgo.'_bloque'.$j.'_descargaDescripcion'.$i;
		campoOculto($datos['codigo'],'existeRiesgo'.$riesgo.'_bloque'.$j.'_descargaDescripcion'.$i);
	}
	campoFichero($nombre,'',1,$datos['ficheroImagen'],'../documentos/riesgos/','Descargar');
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$k'>
		 </td>
	</tr>";
}



function obtieneClasePanelDelito($datos,$codigoRiesgo){
	$res='';
	if($datos){
		$totalRiesgo=consultaBD('SELECT MAX(total) AS max FROM riesgos_evaluacion_general_bloques INNER JOIN riesgos_evaluacion_general ON riesgos_evaluacion_general_bloques.codigoRiesgo=riesgos_evaluacion_general.codigo WHERE riesgos_evaluacion_general.codigoRiesgo='.$codigoRiesgo.' AND codigoEvaluacion='.$datos['codigo'],false,true);
		$totalRiesgo=$totalRiesgo['max'];
		if($totalRiesgo==1){
        	$res='panel-default';
    	}
    	else if($totalRiesgo==2){
        	$res='panel-amarillo';
    	}
    	else if($totalRiesgo==3 || $totalRiesgo==4){
        	$res='panel-warning';
    	}
    	else if($totalRiesgo==6){
        	$res='panel-danger';
    	}
    	else if($totalRiesgo>6){
        	$res='panel-inverse';
    	}
    }

	return $res;
}

function obtieneDatosRiesgoEvaluacion($codigoRiesgo,$codigoEvaluacion,$rep){
	$riesgos = consultaBD("SELECT * FROM riesgos_evaluacion_general WHERE codigoRiesgo='$codigoRiesgo' AND codigoEvaluacion='$codigoEvaluacion';",false,true);
	$riesgos = consultaBD("SELECT * FROM riesgos_evaluacion_general_bloques WHERE codigoRiesgo=".$riesgos['codigo']);
	/*$riesgo=false;
	for($i=1;$i<=$rep;$i++){
		$riesgo=mysql_fetch_assoc($riesgos);
	}*/
	return $riesgos;
}

function imprimeEvaluacionesRiesgo($where){
	global $_CONFIG;
	$res="";

	$where=defineWhereEjercicio($where,array('fechaEvaluacion','informes.fecha'));

	$consulta=consultaBD("SELECT clientes.codigo, EMPNOMBRE, EMPMARCA, COUNT(evaluacion_general.codigo) AS numEvaluaciones, COUNT(informes.codigo) AS numInformes 
						  FROM evaluacion_general INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo 
						  LEFT JOIN informes ON evaluacion_general.codigo=informes.codigoEvaluacion
						  ".$where."
						  GROUP BY codigoCliente
						  ORDER BY fechaEvaluacion;",true);

	while($datos=mysql_fetch_assoc($consulta)){
		$res.="
			<tr>
				<td>".$datos['EMPNOMBRE']." (".$datos['EMPNOMBRE'].")</td>
				<td class='centro'>".$datos['numEvaluaciones']."</td>
				<td class='centro'>
					<a href='".$_CONFIG['raiz']."evaluacion-de-riesgos/index.php?codigoCliente=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Ver evaluaciones</i></a>
				</td>
				<td class='centro'>".$datos['numInformes']."</td>
				<td class='centro'>
					<a href='".$_CONFIG['raiz']."generacion-de-informes/index.php?codigoCliente=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Ver informes</i></a>
				</td>
			</tr>";
	}

	return $res;
}

function imprimeEvaluacionesRiesgoDeCliente($where){
	global $_CONFIG;
	$res='';
	
	$where=defineWhereEjercicio($where,'fechaEvaluacion');

	$consulta=consultaBD("SELECT evaluacion_general.codigo, EMPNOMBRE, fechaEvaluacion,tipoEvaluacion,puestos_trabajo.nombre AS puesto,checkActivo,otros, c1.direccion AS direccionEvaluacion, c2.direccion AS direccionPuesto, evaluacion_general.codigoPuestoTrabajo, evaluacion_general.codigoCentroTrabajo, evaluacion_general.otros, EMPMARCA, informes.codigo AS informe
						  FROM evaluacion_general INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo 
						  LEFT JOIN funciones_formulario_prl ON evaluacion_general.codigoPuestoTrabajo=funciones_formulario_prl.codigo
						  LEFT JOIN empleados ON funciones_formulario_prl.codigoEmpleado=empleados.codigo
						  LEFT JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo
						  LEFT JOIN clientes_centros c1 ON evaluacion_general.codigoCentroTrabajo=c1.codigo
						  LEFT JOIN clientes_centros c2 ON funciones_formulario_prl.codigoCentroTrabajo=c2.codigo
						  LEFT JOIN informes ON evaluacion_general.codigo=informes.codigoEvaluacion
						  ".$where."
						  ORDER BY fechaEvaluacion;",true);

	while($datos=mysql_fetch_assoc($consulta)){
		$notificacion='';
		if($datos['tipoEvaluacion']=='PUESTOS'){
			$tipo='Puesto ('.$datos['puesto'].' - '.$datos['direccionPuesto'].')';
			if($datos['puesto']==''){
				$notificacion='Falta el puesto de trabajo';
			}
		} else if($datos['tipoEvaluacion']=='OTRAS'){
			$tipo='Otra ('.$datos['otros'].')';
		} else if($datos['tipoEvaluacion']=='LUGAR'){
			$tipo='Lugar de trabajo - '.$datos['direccionEvaluacion'];
		}
		if($notificacion!=''){
			$notificacion='<i title="'.$notificacion.'" class="icon-exclamation-circle icon-danger plazoSuperado"></i>';
		}
		$informe='';
		if($datos['informe']!=NULL){
			$informe="<li class='divider'></li>
			<li><a href='".$_CONFIG['raiz']."generacion-de-informes/gestion.php?codigo=".$datos['informe']."'><i class='icon-paste'></i> Ver informe</i></a></li> ";
		}
		$res.="
			<tr>
				<td>".$datos['EMPNOMBRE']." (".$datos['EMPMARCA'].")</td>
				<td>".formateaFechaWeb($datos['fechaEvaluacion'])."</td>
				<td>".$tipo." ".$notificacion."</td>
				<td>".$datos['checkActivo']."</td>
				<td class='centro'>
					<div class='btn-group centro'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."evaluacion-de-riesgos/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
						    <li class='divider'></li>
						    <li><a href='".$_CONFIG['raiz']."evaluacion-de-riesgos/gestion.php?codigo=".$datos['codigo']."&duplicar'><i class='icon-files-o'></i> Duplicar</i></a></li> 
						    <li class='divider'></li>
						    <li><a class='noAjax' href='".$_CONFIG['raiz']."evaluacion-de-riesgos/generaPlanificacion.php?codigo=".$datos['codigo']."'><i class='icon-download'></i> Planificación<br/>preventiva</i></a></li> 
						    $informe
						</ul>
					</div>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}

	return $res;
}


function creaEvaluacionRiesgo(){
	$res=insertaDatos('evaluacion_general',time(),'../documentos/riesgos');
	$codigoEvaluacion=mysql_insert_id();
	if($res){
		$res=insertaRiesgosEvaluacion($codigoEvaluacion);
	}

	return $res;
}

function actualizaEvaluacionRiesgo(){
	$res=actualizaDatos('evaluacion_general','','../documentos/riesgos');
	if($res){
		insertaRiesgosEvaluacion($_POST['codigo']);
	}

	return $res;
}

function insertaRiesgosEvaluacion($codigoEvaluacion){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	$sqlFicheros='(0';
	for($i=1;isset($datos['codigoRiesgo'.$i]);$i++){
		if($datos['existeRiesgo'.$i] == '' || $datos['existeRiesgo'.$i] == 'NO'){
			$res=$res && consultaBD("INSERT INTO riesgos_evaluacion_general VALUES(NULL,'".$datos['codigoRiesgo'.$i]."','$codigoEvaluacion');");
			$codigoRiesgo=mysql_insert_id();
			$res=insertaBloques($datos,$i,$codigoRiesgo);
		} else {
			$res=$res && consultaBD("UPDATE riesgos_evaluacion_general SET codigoRiesgo='".$datos['codigoRiesgo'.$i]."' WHERE codigo=".$datos['existeRiesgo'.$i].";");
			$codigoRiesgo=$datos['existeRiesgo'.$i];
			$res=insertaBloques($datos,$i,$codigoRiesgo);
		}
		$sqlFicheros.=','.$codigoRiesgo;
	}
	$sqlFicheros.=')';
	$res=$res && consultaBD('DELETE FROM riesgos_evaluacion_general WHERE codigo NOT IN '.$sqlFicheros.' AND codigoEvaluacion='.$codigoEvaluacion);
	cierraBD();
	return $res;
}

function insertaBloques($datos,$i,$codigoRiesgo){
	$res=true;
	$notIn='(0';
	for($j=0;isset($datos['existeBloque'.$j.'_riesgo'.$i]);$j++){
		if($datos['existeBloque'.$j.'_riesgo'.$i]=='' || $datos['existeBloque'.$j.'_riesgo'.$i]=='NO'){
			$res=$res && consultaBD("INSERT INTO riesgos_evaluacion_general_bloques VALUES (NULL,".$codigoRiesgo.",'".$datos['descripcionRiesgo_riesgo'.$i.'_bloque'.$j]."','".$datos['probabilidad_riesgo'.$i.'_bloque'.$j]."','".$datos['consecuencias_riesgo'.$i.'_bloque'.$j]."','".$datos['prioridad_riesgo'.$i.'_bloque'.$j]."','".$datos['total_riesgo'.$i.'_bloque'.$j]."','".$datos['graficoBarra_riesgo'.$i.'_bloque'.$j]."','".$datos['descripcion2_riesgo'.$i.'_bloque'.$j]."','".$datos['medida_riesgo'.$i.'_bloque'.$j]."');");
			$codigoBloque=mysql_insert_id();
		} else {
			$res=$res && consultaBD("UPDATE riesgos_evaluacion_general_bloques SET descripcionRiesgo='".$datos['descripcionRiesgo_riesgo'.$i.'_bloque'.$j]."',probabilidad='".$datos['probabilidad_riesgo'.$i.'_bloque'.$j]."',consecuencias='".$datos['consecuencias_riesgo'.$i.'_bloque'.$j]."',prioridad='".$datos['prioridad_riesgo'.$i.'_bloque'.$j]."',total='".$datos['total_riesgo'.$i.'_bloque'.$j]."',graficoBarra='".$datos['graficoBarra_riesgo'.$i.'_bloque'.$j]."',descripcion2='".$datos['descripcion2_riesgo'.$i.'_bloque'.$j]."',medida='".$datos['medida_riesgo'.$i.'_bloque'.$j]."' WHERE codigo=".$datos['existeBloque'.$j.'_riesgo'.$i]);
			$codigoBloque=$datos['existeBloque'.$j.'_riesgo'.$i];
		}
		$res=$res && insertaImagenes($datos,$i,$j,$codigoBloque,'evaluacion_imagenes');
		$res=$res && insertaImagenes($datos,$i,$j,$codigoBloque,'evaluacion_medidas_imagenes');
		$notIn.=','.$codigoBloque;
	}
	$notIn.=')';
	$res=$res && consultaBD('DELETE FROM riesgos_evaluacion_general_bloques WHERE codigo NOT IN '.$notIn.' AND codigoRiesgo='.$codigoRiesgo);
	return $res;
}

function insertaImagenes($datos,$i,$j,$codigoBloque,$tabla){
	$res=true;
	if($tabla=='evaluacion_imagenes'){
		$campoImagen='Descripcion';
		$campoBD='codigoDescripcion';
	} else {
		$campoImagen='Medida';
		$campoBD='codigoMedida';
	}
	$res=true;
	$k=0;
	$notIn='(0';
	while(isset($_FILES['riesgo'.$i.'_bloque'.$j.'_imagen'.$campoImagen.$k]) || isset($datos['existeRiesgo'.$i.'_bloque'.$j.'_descarga'.$campoImagen.$k])){
		if(isset($datos['existeRiesgo'.$i.'_bloque'.$j.'_descarga'.$campoImagen.$k])){
			$codigo=$datos['existeRiesgo'.$i.'_bloque'.$j.'_descarga'.$campoImagen.$k];
			$notIn.=','.$codigo;
		} else {
			if($_FILES['riesgo'.$i.'_bloque'.$j.'_imagen'.$campoImagen.$k]['tmp_name']!=''){
				$fichero=subeDocumento('riesgo'.$i.'_bloque'.$j.'_imagen'.$campoImagen.$k,'','../documentos/riesgos');
				$res=consultaBD('INSERT INTO '.$tabla.' VALUES(NULL,'.$codigoBloque.',"'.$fichero.'");');
				$codigo=mysql_insert_id();
				$notIn.=','.$codigo;
			}
		}
		$k++;
	}
	$notIn.=')';
	$ficheros=consultaBD('SELECT * FROM '.$tabla.' WHERE codigo NOT IN '.$notIn.' AND '.$campoBD.'='.$codigoBloque,false);
	while($fichero=mysql_fetch_assoc($ficheros)){
		if($fichero['ficheroImagen']!='NO' && $fichero['ficheroImagen']!=''){
			unlink('../documentos/riesgos/'.$fichero['ficheroImagen']);
		}
		$res=consultaBD('DELETE FROM '.$tabla.' WHERE codigo='.$fichero['codigo']);
	}
	return $res;

}


function generaDatosGraficoEvaluaciones($where){
	$datos=array();

	conexionBD();
	$where=defineWhereEjercicio($where,'fechaEvaluacion');
	$consulta=consultaBD("SELECT COUNT(riesgos_evaluacion_general.codigo) AS total FROM riesgos_evaluacion_general_bloques INNER JOIN riesgos_evaluacion_general ON riesgos_evaluacion_general_bloques.codigoRiesgo=riesgos_evaluacion_general.codigo INNER JOIN evaluacion_general ON riesgos_evaluacion_general.codigoEvaluacion=evaluacion_general.codigo INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo ".$where." AND total=1;",false,true);
	$datos['trivial']=$consulta['total'];

	$consulta=consultaBD("SELECT COUNT(riesgos_evaluacion_general.codigo) AS total FROM riesgos_evaluacion_general_bloques INNER JOIN riesgos_evaluacion_general ON riesgos_evaluacion_general_bloques.codigoRiesgo=riesgos_evaluacion_general.codigo INNER JOIN evaluacion_general ON riesgos_evaluacion_general.codigoEvaluacion=evaluacion_general.codigo INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo ".$where." AND total=2;",false,true);
	$datos['tolerable']=$consulta['total'];

	$consulta=consultaBD("SELECT COUNT(riesgos_evaluacion_general.codigo) AS total FROM riesgos_evaluacion_general_bloques INNER JOIN riesgos_evaluacion_general ON riesgos_evaluacion_general_bloques.codigoRiesgo=riesgos_evaluacion_general.codigo INNER JOIN evaluacion_general ON riesgos_evaluacion_general.codigoEvaluacion=evaluacion_general.codigo INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo ".$where." AND (total=3 OR total=4);",false,true);
	$datos['moderado']=$consulta['total'];

	$consulta=consultaBD("SELECT COUNT(riesgos_evaluacion_general.codigo) AS total FROM riesgos_evaluacion_general_bloques INNER JOIN riesgos_evaluacion_general ON riesgos_evaluacion_general_bloques.codigoRiesgo=riesgos_evaluacion_general.codigo INNER JOIN evaluacion_general ON riesgos_evaluacion_general.codigoEvaluacion=evaluacion_general.codigo INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo ".$where." AND total=6;",false,true);
	$datos['importante']=$consulta['total'];

	$consulta=consultaBD("SELECT COUNT(riesgos_evaluacion_general.codigo) AS total FROM riesgos_evaluacion_general_bloques INNER JOIN riesgos_evaluacion_general ON riesgos_evaluacion_general_bloques.codigoRiesgo=riesgos_evaluacion_general.codigo INNER JOIN evaluacion_general ON riesgos_evaluacion_general.codigoEvaluacion=evaluacion_general.codigo INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo ".$where." AND total>6;",false,true);
	$datos['intolerable']=$consulta['total'];
	
	cierraBD();

	$datos=escalaGraficoBarras($datos);

	return $datos;
}

function camposFormularioGrafico(){
    $res=array();
    foreach($_POST as $nombre=>$valor){
        if(!is_array($valor) && substr_count($nombre,'fecha')!=1){
            $valor=addslashes($valor);
        }

        if(substr_count($nombre,'nombre')==0){//No transmito el nombre del delito, pues no es necesario...
        	campoOculto($valor,$nombre);
        }
        $res[$nombre]=$valor;
    }

    if(isset($_FILES['ficheroEvaluacion'])){
    	$res['ficheroEvaluacion'] = subeDocumento('ficheroEvaluacion',time(),'../documentos/riesgos');
    	campoOculto($res['ficheroEvaluacion'],'ficheroEvaluacion');
  	} 
  
  	/*for($i=0;isset($_POST['codigo'.$i]);$i++){
		for($j=0;isset($_POST['fechaGestion'.$j.'_riesgo'.$i]);$j++){
			if(isset($_FILES['documento'.$j.'_riesgo'.$i])){
    			$res['documento'.$j.'_riesgo'.$i] = subeDocumento('documento'.$j.'_riesgo'.$i,'','../documentos/riesgos');
    			campoOculto($res['documento'.$j.'_riesgo'.$i],'documento'.$j.'_riesgo'.$i);
  			}
		}
	}*/

	for($i=1;isset($_POST['codigoRiesgo'.$i]);$i++){
		for($j=0;isset($_POST['descripcionRiesgo_riesgo'.$i.'_bloque'.$j]);$j++){
			for($k=0;isset($_FILES['fichero'.$k.'_descripcion'.$j.'_riesgo'.$i]) || isset($_POST['descarga'.$k.'_descripcion'.$j.'_riesgo'.$i]);$k++){
				if(isset($_FILES['fichero'.$k.'_descripcion'.$j.'_riesgo'.$i])){
					if($_FILES['fichero'.$k.'_descripcion'.$j.'_riesgo'.$i]['tmp_name']!=''){
    					$res['fichero'.$k.'_descripcion'.$j.'_riesgo'.$i] = subeDocumento('fichero'.$k.'_descripcion'.$j.'_riesgo'.$i,time().'d'.$i.$j.$k,'../documentos/riesgos');
    					campoOculto($res['fichero'.$k.'_descripcion'.$j.'_riesgo'.$i],'fichero'.$k.'_descripcion'.$j.'_riesgo'.$i);
    				}
    			}
  			}
		}

		for($j=0;isset($_POST['recomendacion'.$j.'_gestion0_riesgo'.$i]);$j++){
			for($k=0;isset($_FILES['fichero'.$k.'_recomendacion'.$j.'_gestion0_riesgo'.$i]) || isset($_POST['descarga'.$k.'_recomendacion'.$j.'_gestion0_riesgo'.$i]);$k++){
				if(isset($_FILES['fichero'.$k.'_recomendacion'.$j.'_gestion0_riesgo'.$i])){
					if($_FILES['fichero'.$k.'_recomendacion'.$j.'_gestion0_riesgo'.$i]['tmp_name']!=''){
    					$res['fichero'.$k.'_recomendacion'.$j.'_gestion0_riesgo'.$i] = subeDocumento('fichero'.$k.'_recomendacion'.$j.'_gestion0_riesgo'.$i,time().'r'.$i.$j.$k,'../documentos/riesgos');
    					campoOculto($res['fichero'.$k.'_recomendacion'.$j.'_gestion0_riesgo'.$i],'fichero'.$k.'_recomendacion'.$j.'_gestion0_riesgo'.$i);
    				}
    			}
  			}
		}		
	}

    return $res;
}

function generaDatosGraficosEvaluacion(){
	$res=array();

	//Gráfico global
	echo "<h3 class='apartadoFormulario aliIzq'>Nivel global de riesgo</h3>
		  <div class='centro cajaGraficoRiesgo'><canvas id='cajaGraficoGlobal' class='chart-holder' width='900' height='800'></canvas></div>";

	campoOculto('','graficoBarraGlobal');
	//Fin gráfico global

	$datos=camposFormularioGrafico();
	
	for($i=1;isset($datos['nombre'.$i]);$i++){
		echo "<h3 class='apartadoFormulario aliIzq'>".$datos['nombre'.$i]."</h3>
			  <div class='centro cajaGraficoRiesgo'><canvas id='cajaGrafico$i' class='chart-holder' width='600' height='250'></canvas></div>";

		campoOculto('','graficoBarra'.$i);
		array_push($res,calculaTotalRiesgo($datos['total'.$i]));
	}

	return $res;
}

function calculaTotalRiesgo($total){
    if($total<0){
        $total=0;
    }
    else{
        $total=round(($total*100)/9);//El 9 se corresponde con el 100% (regla de tres)
    }

    return $total;
}

function gestionGraficos(){
	abreVentanaGestionConBotones('Gráficos de Riesgos','index.php');
	$totales=generaDatosGraficosEvaluacion();
	cierraVentanaGestionGrafico('javascript:history.back();');

	return $totales;
}

function generaGraficosRiesgos($totales){
	$valoresGraficoGlobal=array();
	$etiquetasGraficoGlobal=array();
	$datos=arrayFormulario();
	$i=1;

	foreach ($totales as $total){
		$nivel=defineResultadoEvaluacion($datos['total'.$i],false);
		$color=defineColorBarra($datos['total'.$i]);
		
		//Para gráfico global
		//$valoresGraficoGlobal.=$total.', ';
		array_push($valoresGraficoGlobal,$total);
		if(strlen($datos['nombre'.$i])>40){
			$datos['nombre'.$i]=substr($datos['nombre'.$i],0,40).'...';//Acorto el nombre del delito para mostrarlo en el gráfico global
		}
		//$etiquetasGraficoGlobal.="'".$datos['nombre'.$i]."', ";
		array_push($etiquetasGraficoGlobal,$datos['nombre'.$i]);
		//Fin para gráfico global

		echo "var opciones={
			    scaleOverride : true,
			    scaleSteps : 10,
			    scaleStepWidth : 10,
			    scaleStartValue : 0,
			    barValueSpacing:40,
			    barDatasetSpacing:20,
			    tooltipTemplate: \"<%if (label){%><%=label %>: <%}%><%= value + ' %' %>\",
			    onAnimationComplete:function(){convierteImagen($i)}
			  }

			  var datosGrafico = {
			  	labels: ['Prioridad: $nivel'],
			  	datasets: [{
			        data: [$total],
			        fillColor: '$color',
			        strokeColor: '$color',
			    }]
			  };

			  var grafico = new Chart(document.getElementById('cajaGrafico$i').getContext('2d')).HorizontalBar(datosGrafico,opciones);";
		$i++;
	}

	//Gráfico global
	//$valoresGraficoGlobal=quitaUltimaComa($valoresGraficoGlobal);
	//$etiquetasGraficoGlobal=quitaUltimaComa($etiquetasGraficoGlobal);
	$valoresGraficoGlobal=formateaValoresGraficoGlobal($valoresGraficoGlobal,false);//Porque el gráfico horizontal invierte los valores
	$etiquetasGraficoGlobal=formateaValoresGraficoGlobal($etiquetasGraficoGlobal);

	echo "var opciones={
	    scaleOverride : true,
	    scaleSteps : 10,
	    scaleStepWidth : 10,
	    scaleStartValue : 0,
	    tooltipTemplate: \"<%if (label){%><%=label %>: <%}%><%= value + ' %' %>\",
	    onAnimationComplete:function(){convierteImagen('Global')}
	  }

	  var datosGrafico = {
	  	labels:[$etiquetasGraficoGlobal],
	  	datasets: [{
	  		label: ['Evaluación de Riesgo'],
	        data: [$valoresGraficoGlobal],
	        fillColor: '#5c9cd0',
	        strokeColor: '#5c9cd0',
	    }]
	  };

	  var grafico = new Chart(document.getElementById('cajaGraficoGlobal').getContext('2d')).HorizontalBar(datosGrafico,opciones);";
}

function formateaValoresGraficoGlobal($valores,$comillas=true){
	$res='';

	$valores=array_reverse($valores);
	foreach($valores as $valor){
		if($comillas){
			$res.='"'.$valor.'", ';
		}
		else{
			$res.=$valor.', ';	
		}


	}

	return $res;
}

function defineResultadoEvaluacion($total,$color=false){
    $res='Sin evaluar';
    $clase='primary';

    if($total==1){
        $res='Baja - Riesgo Trivial';
        $clase='default';
    }
    else if($total==2){
        $res='Media/Baja - Riesgo Tolerable';
        $clase='amarillo';
    }
    else if($total==3 || $total==4){
        $res='Media/Alta - Riesgo Moderado';
        $clase='warning';
    }
    else if($total==6){
        $res='Alta - Riesgo Importante';
        $clase='danger';
    }
    else if($total>6){
        $res='Carácter Inmediato - Riesgo Intolerable';
        $clase='inverse';
    }


    if($color){
        $res="<span class='label label-$clase'>".$res.'</span>';
    }

    return $res;
}

function defineColorBarra($total){
	$color = '#5c9cd0';

	if($total==1){
        $color='#f9f9f9';
    }
    else if($total==2){
        $color='#fff584';
    }
    else if($total==3 || $total==4){
        $color='#ffbf42';
    }
    else if($total==6){
        $color='#c00011';
    }
    else if($total>6){
        $color='#000';
    }

	return $color;
}

function cierraVentanaGestionGrafico($destino,$columnas=false,$botonGuardar=true,$texto='Guardar',$icono='icon-check',$botonVolver=true,$textoVolver='Volver',$iconoVolver='icon-chevron-left',$salto=true){
	if($columnas){
		echo "		  </fieldset>
			  		  <fieldset class='sinFlotar'>";
	}
	if($salto){
		echo "			<br />";
	}


	echo "
						
	                    <div class='form-actions'>";
	if($botonVolver){
	    echo "            <a href='$destino' class='btn btn-default noAjax'><i class='$iconoVolver'></i> $textoVolver</a>";
	}

	if($botonGuardar){                      
	    echo "            <button type='submit' class='btn btn-propio'><i class='$icono'></i> $texto</button>";
	}

	echo "
	                    </div> <!-- /form-actions -->
	                  </fieldset>
	                </form>
	                </div>


	            </div>
	            <!-- /widget-content --> 
	          </div>

	      </div>
	    </div>
	    <!-- /container --> 
	  </div>
	  <!-- /main-inner --> 
	</div>
	<!-- /main -->";
}

function creaTablaGestiones($riesgo=0,$datos=false){
	echo"
	<table class='table table-striped table-bordered' class='tablaGestiones' id='tablaGestiones".$riesgo."'>
      <thead>
        <tr>
          <th> Medidas preventivas</th>
        </tr>
      </thead>
      <tbody>";
      $i=0;
      echo "<tr><td>";
      campoOculto('','idGestion'.$i.'_riesgo'.$riesgo);
      campoOculto('NO','existeGestion'.$i.'_riesgo'.$riesgo,'','hide existeGestion');
      creaTablaRecomendacionesRiesgo($riesgo,$i,$datos);
      echo"  	</td></tr>";
     echo"
      </tbody>
    </table><br />";
}

function creaTablaRecomendacionesRiesgo($riesgo,$j,$datos=false){
	echo"
	<table class='table table-striped table-bordered' class='tablaGestiones' id='tablaGestiones".$riesgo."'>
      <thead>
        <tr>
          <th> Medidas preventivas</th>
          <th> </th>
        </tr>
      </thead>
      <tbody>";
  		imprimeCamposTablaRecomendacionesRiesgo($riesgo,$j,$datos);
    echo "
      </tbody>
    </table><br />";
}

function imprimeCamposTablaRecomendacionesRiesgo($riesgo,$j,$datos=false){
	$textos='';
	$valores='';
	echo "<tr class='trMedida'>";
	areaTextoTabla('medida_riesgo'.$riesgo.'_bloque'.$j,$datos['medida'],'areaTextoTablaMedidas');
	creaTablaDescripcionesFicherosMedidas($riesgo,$j,$datos);
	echo "</tr>";
}

function creaTablaDescripcionesFicherosMedidas($riesgo,$j,$datos=false){
	echo "
	<td>
	<table class='table table-striped table-bordered tablaImagenes' id='tablaMedidas".$riesgo."_bloque".$j."'>
      <thead>
        <tr>
          <th> Imágenes </th>
          <th> </th>
        </tr>
      </thead>
      <tbody>";
  	
  		$i=0;
  		$k=0;

  		conexionBD();
  		if($datos!=false && !isset($_GET['duplicar'])){
  			$consulta=consultaBD("SELECT * FROM evaluacion_medidas_imagenes WHERE codigoMedida='".$datos['codigo']."'",true);
  			while($datosF=mysql_fetch_assoc($consulta)){
				imprimeCamposTablaDescripcionesFicherosMedidas($riesgo,$j,$i,$k,$datosF);
				$i++;
				$k++;
  			}
  		}
  		

  		imprimeCamposTablaDescripcionesFicherosMedidas($riesgo,$j,$i,$k);
  	
  		cierraBD();

    echo "
      </tbody>
    </table>
    <center>
		<button type='button' id='anadirMedida' class='gestionMedida btn btn-small btn-success' onclick='insertaFilaImagenes(\"tablaMedidas".$riesgo."_bloque".$j."\");'><i class='icon-plus'></i> Añadir imagen</button>
		<button type='button' id='eliminarrMedida' class='gestionMedida btn btn-small btn-danger' onclick='eliminaFilaImagenes(\"tablaMedidas".$riesgo."_bloque".$j."\");'><i class='icon-trash'></i> Eliminar imagen</button>
	</center></td>";
}

function imprimeCamposTablaDescripcionesFicherosMedidas($riesgo,$j,$i,$k,$datos=false){
	echo "<tr class='trDescripcionFicheros'>";
	$nombre='riesgo'.$riesgo.'_bloque'.$j.'_imagenMedida'.$i;
	if($datos!=false){
		$nombre='riesgo'.$riesgo.'_bloque'.$j.'_descargaMedida'.$i;
		campoOculto($datos['codigo'],'existeRiesgo'.$riesgo.'_bloque'.$j.'_descargaMedida'.$i);
	}
	campoFichero($nombre,'',1,$datos['ficheroImagen'],'../documentos/riesgos/','Descargar');
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$k'>
		 </td>
	</tr>";
}

function eliminaEvaluacion(){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();
    $ruta='../documentos/riesgos';
    for($i=0;isset($datos['codigo'.$i]);$i++){
        $imagen=consultaBD("SELECT * FROM evaluacion_general WHERE codigo='".$datos['codigo'.$i]."';",false, true);
        if($imagen['ficheroEvaluacion'] != '' && $imagen['ficheroEvaluacion'] != 'NO'){
            unlink($ruta."/".$imagen['ficheroEvaluacion']);
        }
        $gestiones=consultaBD("SELECT * FROM gestion_evaluacion_general WHERE codigoEvaluacion IN (SELECT codigo FROM riesgos_evaluacion_general WHERE codigoEvaluacion=".$datos['codigo'.$i].")",true);
        while($gestion=mysql_fetch_assoc($gestiones)){
        	if($gestion['documentacion'] != '' && $gestion['documentacion'] != 'NO'){
            	unlink($ruta."/".$gestion['documentacion']);
        	}
        }
        $res=consultaBD("DELETE FROM evaluacion_general WHERE codigo='".$datos['codigo'.$i]."';");
        if(!$res && $_CONFIG['usuarioBD']=='root'){
            echo mysql_error();
        }
    }
    
    cierraBD();

    return $res;
}


function preSeleccionEvaluacion(){
	abreVentanaGestionConBotones('Nueva evaluación de Riesgos','gestion.php','span3','icon-plus-circle','',false,'noAjax','index.php',true,'Continuar','icon-chevron-right');

	conexionBD();

	$datos=false;
	if(isset($_GET['codigo'])){
		campoOculto($_GET['codigo'],'codigoEvaluacion');
		$consulta=consultaBD("SELECT * FROM riesgos_evaluacion_general WHERE codigoEvaluacion=".$_GET['codigo']." GROUP BY codigoRiesgo");
		
		$i=0;
		$riesgos=array();
		$cantidades=array();
		while($reg=mysql_fetch_assoc($consulta)){
			$riesgos[$i] = $reg['codigoRiesgo'];
			$cantidad=consultaBD('SELECT COUNT(codigo) AS total FROM riesgos_evaluacion_general_bloques WHERE codigoRiesgo='.$reg['codigo'] ,true,true);
			$cantidades[$i]=$cantidad['total'];
			$i++;
		}
		$datos=compruebaDatos('evaluacion_general');
	}

	campoSelectConsulta('codigoCliente','Cliente',"SELECT codigo, CONCAT(EMPNOMBRE,' (',EMPMARCA,')') AS texto FROM clientes WHERE activo='SI' ORDER BY EMPNOMBRE",$datos);
	campoSelect('tipoEvaluacion','Tipo',array('','Lugar de trabajo','Puestos','Otra'),array('','LUGAR','PUESTOS','OTRAS'),$datos,'selectpicker span3 show-tick','');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha('fechaEvaluacionPreseleccion','Fecha de evaluación',$datos['fechaEvaluacion']);
	campoCentroTrabajoPreseleccion($datos);
	campoPuestoTrabajoPreseleccion($datos);
	campoOtrosPreseleccion($datos);

	cierraColumnaCampos(true);

	abreColumnaCampos();

	campoSelect('tipo','Tipo de evaluación',array('','INICIAL','PERIÓDICA','OTROS'),array('','INICIAL','PERIODICA','OTROS'),$datos,'selectpicker span3 show-tick','');
	campoTexto('motivo','Motivo',$datos);

	cierraColumnaCampos();

	abreColumnaCampos();

	echo '<div id="divOtroTipo" class="hide">';
	campoTexto('otroTipo','Otro',$datos);
	echo '</div>';

	cierraColumnaCampos();

	abreColumnaCampos('sinFlotar');

	echo "<div class='cajaPreSeleccionRiesgos'>";
			//<div class='etiquetaPreSeleccionRiesgos'>Seleccione los riesgos que desea evaluar:</div>";

	campoCheck('todo','Seleccione los riesgos a evaluar',array('todo0'=>'SI'),array('Todos'),array('SI'));

	$consulta=consultaBD("SELECT riesgos.codigo, riesgos.codigoInterno, riesgos.nombre, riesgos_tipos.nombre AS tipo FROM riesgos INNER JOIN riesgos_tipos ON riesgos.tipo=riesgos_tipos.codigo ORDER BY tipo ASC, codigoInterno ASC");

	cierraBD();

	$primero=true;
	$tipo='';
	$tipo2='';
	$i=1;
	while($riesgo=mysql_fetch_assoc($consulta)){
		$marcado = '';
		if($tipo != $riesgo['tipo']){
			$i=1;
			$tipo = $riesgo['tipo'];

			if(!$primero){
				echo "<div class='sinFlotar'></div><hr class='separadorRiesgos' /><p class='tituloBloqueRiesgos'>".$tipo."</p>";
			}
			else{
				echo "<p class='tituloBloqueRiesgos'>".$tipo."</p>";
				$primero=false;
			}
			$tipo2=explode(' ', $tipo);
			echo '<div class="checkTipo">';
				campoCheckIndividual('check'.$tipo2[0],'Marcar todos / Desmarcar todos','SI');
			echo '</div>';
		}
		$cantidad=1;
		if(isset($_GET['codigo'])){
			if(in_array($riesgo['codigo'], $riesgos)){
				$marcado = $riesgo['codigo'];
				$key = array_search($riesgo['codigo'], $riesgos);
				$cantidad = $cantidades[$key];
			}
		} 
		else {
			$marcado = $riesgo['codigo'];
		}

		abreColumnaCampos('span4 checkRiesgo');
		echo '<div class="div'.$tipo2[0].'">';
			campoCheckIndividual('codigoRiesgo[]',$i.' - '.$riesgo['nombre'],$marcado,$riesgo['codigo']);
			campoCantidad('cantidad'.$riesgo['codigo'],$cantidad);
		echo '</div>';
		cierraColumnaCampos();
		$i++;
	}

	echo "</div>";

	cierraVentanaGestion('index.php',true,true,'Continuar','icon-chevron-right');
}


function campoPuestoTrabajoPreseleccion($datos=false){
	echo "<div class='hide' id='cajaPuestos'>";
	
	if(!$datos || $datos['tipoEvaluacion']!='PUESTOS'){
		campoSelect('codigoPuestoTrabajo','Puesto de trabajo',array(''),array('NULL'));
	}
	elseif($datos['tipoEvaluacion']=='PUESTOS'){
		$consulta=consultaBD("SELECT funciones_formulario_prl.codigo, CONCAT(puestos_trabajo.nombre,' (',clientes_centros.direccion,')') AS nombre FROM funciones_formulario_prl INNER JOIN empleados ON funciones_formulario_prl.codigoEmpleado=empleados.codigo INNER JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo INNER JOIN clientes_centros ON funciones_formulario_prl.codigoCentroTrabajo=clientes_centros.codigo WHERE empleados.codigoCliente=".$datos['codigoCliente']." ORDER BY nombre;",true);
		$nombres=array('');
		$codigos=array('NULL');
		while($item=mysql_fetch_assoc($consulta)){
			if(!in_array($item['nombre'], $nombres)){
				array_push($nombres, $item['nombre']);
				array_push($codigos, $item['codigo']);
			}
		}
		campoSelect('codigoPuestoTrabajo','Puesto de trabajo',$nombres,$codigos,$datos);
	}


	echo "</div>";
}

function campoCentroTrabajoPreseleccion($datos=false){
	echo "<div class='hide' id='cajaCentros'>";
	
	if(!$datos || $datos['tipoEvaluacion']!='LUGAR'){
		campoSelect('codigoCentroTrabajo','Centro de trabajo',array(''),array('NULL'));
	}
	elseif($datos['tipoEvaluacion']=='LUGAR'){
		campoSelectConsulta('codigoCentroTrabajo','Centro de trabajo',"SELECT codigo, direccion AS texto FROM clientes_centros WHERE codigoCliente=".$datos['codigoCliente'],$datos);
	}


	echo "</div>";
}


/*function campoPuestoTrabajoPreseleccion($datos=false){
	echo "<div class='hide' id='cajaPuestos'>";
	
	if(!$datos || $datos['tipoEvaluacion']!='PUESTOS'){
		campoSelect('codigoPuestoTrabajo','Puesto de trabajo',array(''),array('NULL'));
	}
	elseif($datos['tipoEvaluacion']=='PUESTOS'){
		campoSelectConsulta('codigoPuestoTrabajo','Puesto de trabajo',"SELECT codigo, nombre AS texto FROM puestos_trabajo WHERE codigoCliente='".$datos['codigoCliente']."' AND codigo IN(SELECT codigoPuestoTrabajo FROM puestos_trabajo_formulario_prl WHERE codigoPuestoTrabajo IS NOT NULL AND codigoFormularioPRL IS NOT NULL GROUP BY codigoPuestoTrabajo) ORDER BY nombre;",$datos);
	}


	echo "</div>";
}*/

function obtienePuestosTrabajoCliente(){
	$res="<option value='NULL'></option>";

	$datos=arrayFormulario();
	extract($datos);

	$consulta=consultaBD("SELECT funciones_formulario_prl.codigo, CONCAT(puestos_trabajo.nombre,' (',clientes_centros.direccion,')') AS nombre FROM funciones_formulario_prl INNER JOIN empleados ON funciones_formulario_prl.codigoEmpleado=empleados.codigo INNER JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo INNER JOIN clientes_centros ON funciones_formulario_prl.codigoCentroTrabajo=clientes_centros.codigo WHERE empleados.codigoCliente=$codigoCliente ORDER BY nombre;",true);
	$puestos=array();
	while($datos=mysql_fetch_assoc($consulta)){
		if(!in_array($datos['nombre'], $puestos)){
			$res.="<option value='".$datos['codigo']."'>".$datos['nombre']."</option>";
			array_push($puestos, $datos['nombre']);
		}
	}

	echo $res;
}

function obtieneCentrosTrabajoCliente(){
	$res="<option value='NULL'></option>";

	$datos=arrayFormulario();
	extract($datos);

	$consulta=consultaBD("SELECT codigo, direccion FROM clientes_centros WHERE codigoCliente=".$codigoCliente,true);
	while($datos=mysql_fetch_assoc($consulta)){
		$res.="<option value='".$datos['codigo']."'>".$datos['direccion']."</option>";
	}

	echo $res;
}

/*function obtienePuestosTrabajoCliente(){
	$res="<option value='NULL'></option>";

	$datos=arrayFormulario();
	extract($datos);

	$consulta=consultaBD("SELECT codigo, nombre FROM puestos_trabajo WHERE codigoCliente='$codigoCliente' AND codigo IN(SELECT codigoPuestoTrabajo FROM puestos_trabajo_formulario_prl WHERE codigoPuestoTrabajo IS NOT NULL AND codigoFormularioPRL IS NOT NULL GROUP BY codigoPuestoTrabajo) ORDER BY nombre;",true);
	while($datos=mysql_fetch_assoc($consulta)){
		$res.="<option value='".$datos['codigo']."'>".$datos['nombre']."</option>";
	}

	echo $res;
}*/

function campoOtrosPreseleccion($datos=false){
	echo "<div class='hide' id='cajaOtros'>";
	campoTexto('otros','Descripción otra',$datos);
	echo "</div>";
}


function compruebaDatosEvaluacion(){
	if(isset($_REQUEST['codigo'])){
		if(isset($_GET['duplicar'])){
			$datos=consultaBD('SELECT * FROM evaluacion_general WHERE codigo='.$_REQUEST['codigo'],true,true);
			$datos['codigoCliente']=NULL;
			$datos['fechaEvaluacion']=fechaBD();
			$datos['graficoBarraGlobal']='';
			$datos['ficheroEvaluacion']='';
			$datos['tipoEvaluacion']='';
			$datos['codigoPuestoTrabajo']=NULL;
			$datos['checkActivo']='SI';
			$datos['checkVisible']='NO';
			$datos['tipo']='';
			$datos['otroTipo']='';
			$datos['motivo']='';
			$datos['codigoCentroTrabajo'];
		} else {
			$datos=compruebaDatos('evaluacion_general');
		}
	}
	else{
		$datos=arrayFormulario();
		$datos['codigo']=false;
		$datos['fechaEvaluacion']=$datos['fechaEvaluacionPreseleccion'];
		$datos['ficheroEvaluacion']='NO';
		$datos['checkVisible']='NO';
	}

	return $datos;
}

function generaPDF($codigo){
	global $_CONFIG;

	$datos=consultaBD("SELECT informes.codigo, clientes.codigo AS codigoCliente, clientes.EMPNOMBRE, ficheroLogo, formulario evaluacion_general.fechaEvaluacion AS fecha, clientes_centros.direccion AS direccionCentro
					   FROM informes
					   INNER JOIN evaluacion_general ON informes.codigoEvaluacion=evaluacion_general.codigo
					   INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo
					   INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
					   INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
					   LEFT JOIN clientes_centros ON evaluacion_general.codigoCentroTrabajo=clientes_centros.codigo
					   INNER JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
					   WHERE informes.codigo='$codigo';",true,true);
	$formulario=explode('&{}&',$datos['formulario']);
    if(count($formulario)>1){
        foreach($formulario as $pregunta){
            $partes=explode('=>',$pregunta);
            $datos[$partes[0]]=$partes[1];
        }
    }
    $logoCliente=$datos['EMPNOMBRE'];
	if($datos['ficheroLogo']!='' && $datos['ficheroLogo']!='NO'){
		$logoCliente="<img src='../clientes/imagenes/".$datos['ficheroLogo']."' />";
	} else {
		$logoCliente=$datos['EMPNOMBRE'];
	}
    $contenido = "
	<style type='text/css'>
	<!--
			html{
				margin:0px;
				padding:0px;
				width:100%;
			}
	        body{
	            font-size:12px;
	            font-family: Calibri;
	            font-weight: lighter;
	            line-height: 24px;
	            margin:0px;
				padding:0px;
				width:100%;

	        }

	        .portada{
	        	border:2px solid #02215c;
	        	height:95%;
	        	margin:20px;
	        }

	        .titlePortada{
	        	text-align:center;
	        	font-size:56px;
	        	color:#02215c;
	        	font-weight:bold;
	        	margin-top:240px;
	        	line-height:64px;
	        }

	        .imagenFondo{
	        	position:absolute;
	        	top:10%;
	        	z-index:1;
	        	left:-3%
	        }

	        .imagenFondo img{
	        	width:103%;
	        	position:absolute;
	        	display:inline-block;
	        }

	        .imagenFondo2{
	        	position:absolute;
	        	z-index:1;
	        	top:-133px;
	        	left:-30px;
	        }

	        .imagenFondo2 img{
	        	width:104%;
	        	height:100%;
	        	display:inline-block;
	        }

	        .nombreEmpresa{
	        	font-size:36px;
	        	color:#02215c;
	        	text-align:center;
	        	margin-top:250px;
	        	width:100%;
	        	line-height:48px;
	        }

	        .cabecera{
	        	border-top:1px solid #02215c;
	        	border-bottom:1px solid #02215c;
	        	padding:0px;
	        	margin-bottom:200px;
	        	width:100%;
	        }

	        .cabecera .a20{
	        	width:20%;
	        	text-align:center;
	        }

	        .cabecera .a60{
	        	width:60%;
	        	color:#02215c;
	        	text-align:center;
	        	font-weight:bold;
	        	font-size:18px;
	        }

	        .cabecera img{
	        	width:58%;
	        }

	        .cabecera .tituloCabecera{
	        	color:#02215c;
	        	text-align:center;
	        	font-weight:bold;
	        	margin-left:140px;
	        	font-size:18px;
	        	margin-top:-25px;
	        }

	        .cabecera .logoCliente{
	        	position:absolute;
	        	right:0px;
	        	top:0px;
	        	background:red;
	        	width:100px;
	        }

	        .cajaRevision{
	        	color:#02215c;
	        	margin-left:80px;
	        }

	        .cajaNumeroPagina{
	        	color:#02215c;
	        	position:absolute;
	        	right:80px;
	        	bottom:0px;	
	        }

	        .tituloIndice{
	        	text-align:center;
	        	font-size:20px;
	        	margin-top:20px;
	        	margin-bottom:20px;
	        	color:#02215c;
	        }

	        .indice{
	        	margin-left:80px;
	        }


	        .indice td{
	        	font-size:14px;
	        	color:#02215c;
	        	padding-top:35px;
	        }

	        .indice .texto{
	        	font-weight:bold;
	        }

	        .indice .sub{
	        	font-weight:normal;
	        	padding-left:10px;
	        }

	        .indice .pagina{
	        	font-weight:bold;
	        }

	        .container{
	        	width:75%;
	        	margin-left:90px;
	        }

	        .container2{
	        	width:95%;
	        	margin-left:20px;
	        	border:1px solid #02215c;
	        	padding:5px;
	        	margin-top:20px;
	        }

	        .tituloSeccion{
	        	font-size:14px;
	        	color:#02215c;
	        	font-weight:bold;
	        }

	        p, li {
	        	text-align:justify;
	        	font-size:11px;
	        	line-height:22px;
	        }

	        li{
	        	padding-bottom:18px;
	        }

	        .tituloIndice{
	        	text-align:center;
	        	font-size:20px;
	        	margin-top:20px;
	        	margin-bottom:20px;
	        	color:#02215c;
	        }

	        .tablaDatos{
	        	width:100%;
	        	border:1px solid #000;
	        	border-collapse:collapse;
	        }

	        .tablaDatos th,
	        .tablaDatos td{
	        	border:1px solid #000;
	        	font-size:11px;
	        	padding-top:3px;
	        	padding-bottom:3px;
	        	padding-left:8px;
	        }

	        .tablaDatos th{
	        	background:#02215c;
	        	font-weight:bold;
	        	color:#FFF;
	        }

	        .tablaDatos .a10{
	        	width:10%;
	        }

	        .tablaDatos .a15{
	        	width:15%;
	        }

	        .tablaDatos .a20{
	        	width:20%;
	        }

	        .tablaDatos .a25{
	        	width:25%;
	        }

	        .tablaDatos .a30{
	        	width:30%;
	        }

	        .tablaDatos .a33,
	        .noBorde .a33{
	        	width:33%;
	        }

	        .tablaDatos .a35{
	        	width:35%;
	        }

	        .tablaDatos .a65{
	        	width:65%;
	        }

	        .tablaDatos .a70{
	        	width:70%;
	        }

	        .tablaDatos .a85{
	        	width:85%;
	        }

	        .tablaDatos .a100{
	        	width:100%;
	        }

	        .tablaDatos .centro{
	        	text-align:center;
	        }

	        .desconocido{
	        	color:red;
	        	font-weight:bold;
	        }

	        .noBorde{
	        	width:100%;
	        	border:none;
	        }

	        .noBorde th,
	        .noBorde td{
	        	font-size:11px;
	        	padding-top:3px;
	        	padding-bottom:3px;
	        	padding-left:8px;
	        	font-weight:bold;
	        	height:15px;
	        	border:none;
	        	border-collapse:collapse;
	        }

	        .noBorde th{
	        	background:#02215c;
	        	font-weight:bold;
	        	color:#FFF;
	        }

	        .noBorde .tdGris{
	        	background:#BBB;
	        }

	        p.tituloIndice{
	        	text-align:center;
	        	font-size:20px;
	        	margin-top:40px;
	        	margin-bottom:20px;
	        	color:#02215c;
	        	font-weight:bold;
	        }

	        .organigrama{
	        	width:80%;
	        }
	-->
	</style>
	

	<page backbottom='0mm' backleft='0mm' backright='0mm' >
		<div class='imagenFondo'><img src='../img/fondoOferta.jpeg' alt='Anesco' /></div>
		<div class='portada'>
			<div class='titlePortada'>EVALUACIÓN DE<br/>RIESGOS</div>

			<div class='nombreEmpresa'>".$datos['pregunta0']."</div>
		</div>

	</page>";

$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		

		<p class='tituloIndice'>ÍNDICE</p>

		<table class='indice'>
			<tr>
				<td class='texto'>1. INTRODUCCIÓN</td>
				<td class='pagina'>5</td>
			</tr>
			<tr>
				<td class='texto sub'>1.1 IDENTIFICACIÓN DE LA EMPRESA</td>
				<td class='pagina sub'>6</td>
			</tr>
			<tr>
				<td class='texto sub'>1.2 DESCRIPCIÓN DE LOS CENTROS DE TRABAJO</td>
				<td class='pagina sub'>6</td>
			</tr>
			<tr>
				<td class='texto'>2. OBJETO</td>
				<td class='pagina'>7</td>
			</tr>
			<tr>
				<td class='texto'>3. ALCANCE</td>
				<td class='pagina'>7</td>
			</tr>
			<tr>
				<td class='texto'>4. POLÍTICA</td>
				<td class='pagina'>7</td>
			</tr>
			<tr>
				<td class='texto'>5. OBJETIVO Y METAS</td>
				<td class='pagina'>9</td>
			</tr>
			<tr>
				<td class='texto'>6. RECURSOS</td>
				<td class='pagina'>9</td>
			</tr>
			<tr>
				<td class='texto sub'>6.1 RECURSOS HUMANOS DE LOS QUE SE DISPONE</td>
				<td class='pagina sub'>10</td>
			</tr>
			<tr>
				<td class='texto sub'>6.2 RECURSOS MATERIALES Y TÉCNICOS DE LOS QUE SE DISPONE</td>
				<td class='pagina sub'>10</td>
			</tr>
			<tr>
				<td class='texto sub'>6.3 RECURSOS ECONÓMICOS DE LOS QUE SE DISPONE</td>
				<td class='pagina sub'>10</td>
			</tr>
			<tr>
				<td class='texto'>7. ORGANIZACIÓN PRODUCTIVA</td>
				<td class='pagina'>11</td>
			</tr>
			<tr>
				<td class='texto sub'>7.1 ORGANIGRAMA</td>
				<td class='pagina sub'>11</td>
			</tr>
			<tr>
				<td class='texto sub'>7.2 LISTADO DE ÁREAS DE ACTIVIDAD Y PROCESOS PRODUCTIVOS</td>
				<td class='pagina sub'>12</td>
			</tr>
		</table>

		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>";


	return $contenido;
}

function planificacionPreventiva($objPHPExcel){
	conexionBD();
	$datos=consultaBD('SELECT evaluacion_general.*, clientes.EMPNOMBRE, puestos_trabajo.nombre AS nombrePuesto, clientes_centros.direccion AS direccionCentro, clientes_centros.nombre AS nombreCentro 
		FROM evaluacion_general 
		INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo 
		LEFT JOIN clientes_centros ON evaluacion_general.codigoCentroTrabajo=clientes_centros.codigo
		LEFT JOIN puestos_trabajo ON evaluacion_general.codigoPuestoTrabajo=puestos_trabajo.codigo
		WHERE evaluacion_general.codigo='.$_GET['codigo'],false,true);
	$riesgos=consultaBD('SELECT riesgos_evaluacion_general.*, riesgos.nombre AS nombreRiesgo, codigoInterno  FROM riesgos_evaluacion_general INNER JOIN riesgos ON riesgos_evaluacion_general.codigoRiesgo=riesgos.codigo WHERE codigoEvaluacion='.$_GET['codigo']);
	cierraBD();
	$tipos=array('LUGAR'=>'CENTRO DE TRABAJO: ','PUESTO'=>'PUESTO DE TRABAJO: ','OTRAS'=>'');
	$prefijo='';
	if($datos['tipoEvaluacion']=='LUGAR'){
		if($datos['nombreCentro']==''){
			$tipo='CENTRO DE TRABAJO: '.$datos['direccionCentro'];
		} else {
			$tipo='CENTRO DE TRABAJO: '.$datos['nombreCentro'].' ('.$datos['direccionCentro'].')';
		}
		$prefijo='L ';
	} else if($datos['tipoEvaluacion']=='PUESTOS'){
		$tipo='PUESTO DE TRABAJO: '.$datos['nombrePuesto'];
		$prefijo='P.'.substr(strtoupper($datos['nombrePuesto']),0,3).' ';
	} else if($datos['tipoEvaluacion']=='OTRAS'){
		$tipo=$datos['otros'];
	}
	$objPHPExcel->getActiveSheet()->getCell('A2')->setValue('EMPRESA: '.$datos['EMPNOMBRE']);
	$objPHPExcel->getActiveSheet()->getCell('E2')->setValue('FECHA DE ELABORACION: '.formateaFechaWeb($datos['fechaEvaluacion']));
	$objPHPExcel->getActiveSheet()->getCell('A3')->setValue($tipo);
	$i=5;
	$n=1;
	while($riesgo=mysql_fetch_assoc($riesgos)){
		$primero=$i;
		$nT=$n<10?'0'.$n:$n;
		$prioridad=explode(' - ', $riesgo['prioridad']);
		$prioridad[0]=str_replace('/', ' - ', $prioridad[0]);
		$prioridad[1]=str_replace('RIESGO ', '', $prioridad[1]);
		$prioridad[1]=trim($prioridad[1]);
		$iniciales=array('TRIVIAL'=>'(T)','TOLERABLE'=>'(TO)','MODERADO'=>'(M)','IMPORTANTE'=>'(I)','INTOLERABLE'=>'(IN)');
		$estilo=array(
					'font' => array(
						'size'=>9
					)
				);
		$colores=array('TRIVIAL'=>'f9f9f9','TOLERABLE'=>'fff584','MODERADO'=>'ffbf42','IMPORTANTE'=>'c00011','INTOLERABLE'=>'000000');
		$coloresF=array('TRIVIAL'=>'000000','TOLERABLE'=>'000000','MODERADO'=>'000000','IMPORTANTE'=>'ffffff','INTOLERABLE'=>'ffffff');
		$estilo2=array(
					'font' => array(
						'color' => array('rgb' => $coloresF[$prioridad[1]]),
					),
        			'fill' => array(
            			'type' => PHPExcel_Style_Fill::FILL_SOLID,
            			'color' => array('rgb' => $colores[$prioridad[1]])
        			)
    			);
		$objPHPExcel->getActiveSheet()->getCell('A'.$i)->setValue($prefijo.$nT);
		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($riesgo['nombreRiesgo']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($prioridad[1].' '.$iniciales[$prioridad[1]]);
		if($estilo2!=''){
			$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($estilo2);
		}
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($prioridad[0]);
		if($estilo!=''){
			$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->applyFromArray($estilo);
		}
		$medidas=consultaBD('SELECT medidas_riesgos_evaluacion_general.* FROM medidas_riesgos_evaluacion_general INNER JOIN gestion_evaluacion_general ON medidas_riesgos_evaluacion_general.codigoGestion=gestion_evaluacion_general.codigo WHERE codigoEvaluacion='.$riesgo['codigo'],true);
		$hayMedida=false;
		while($medida=mysql_fetch_assoc($medidas)){
			$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue('');
			$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue($medida['recomendacion']);
			$hayMedida=true;
			if($estilo!=''){
				$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->applyFromArray($estilo);
			}
			$i++;
		}
		$n++;
		if(!$hayMedida){
			$i++;
		}
		$ultimo=$i-1;
		if($ultimo>$primero){
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$primero.':A'.$ultimo);
			$objPHPExcel->getActiveSheet()->mergeCells('B'.$primero.':B'.$ultimo);
			$objPHPExcel->getActiveSheet()->mergeCells('C'.$primero.':C'.$ultimo);
			$objPHPExcel->getActiveSheet()->mergeCells('D'.$primero.':D'.$ultimo);
		}
	}
	/*foreach(range('B','I') as $columnID) {
    	$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
	}*/
	foreach($objPHPExcel->getActiveSheet()->getRowDimensions() as $rd) { 
    	$rd->setRowHeight(-1); 
	}
}

function campoCantidad($nombreCampo,$valor='0',$clase='input-mini pagination-right',$min='',$max=''){
	$valor=compruebaValorCampo($valor,$nombreCampo);

	echo "
	<div class='control-group campoCantidad'>                     
      <div class='controls'>
      	<input type='number' name='$nombreCampo' id='$nombreCampo' class='$clase' value='$valor' $min $max />
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}

//Fin parte de evaluación de riesgos