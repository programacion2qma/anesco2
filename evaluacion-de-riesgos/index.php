<?php
  $seccionActiva=5;
  include_once('../cabecera.php');
  
  $res=operacionesEvaluaciones();

  //Gestión de filtros
  $where='WHERE clientes.codigo IN (SELECT clientes.codigo FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE activo="SI" AND contratos.enVigor="SI" AND clientes.eliminado="NO")';
  
  $texto='';
  $textoColumna='<th>Nº evaluaciones</th><th class="centro"></th><th>Nº informes</th><th class="centro"></th>';
  $boton="<a href='".$_CONFIG['raiz']."evaluacion-de-riesgos/index.php?baja' class='shortcut'><i class='shortcut-icon icon-arrow-down'></i><span class='shortcut-label'>De clientes de baja</span> </a>";
  

  if(isset($_GET['baja'])){
    $where='WHERE clientes.codigo NOT IN (SELECT clientes.codigo FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE activo="SI" AND contratos.enVigor="SI" AND clientes.eliminado="NO")';
    
    $texto=' de clientes de baja';
    $boton="<a href='".$_CONFIG['raiz']."evaluacion-de-riesgos/index.php?' class='shortcut'><i class='shortcut-icon icon-arrow-left'></i><span class='shortcut-label'>Volver</span> </a>";

    $listado=imprimeEvaluacionesRiesgo($where);
  }
  elseif(isset($_GET['codigoCliente'])){
    $codigoCliente=$_GET['codigoCliente'];

    $nombreCliente=obtieneNombreCliente($codigoCliente);
    $where='WHERE evaluacion_general.codigoCliente='.$codigoCliente;
    
    $texto=' para el cliente '.$nombreCliente;
    $textoColumna='<th>Fecha evaluación</th><th>Tipo</th><th>Activa para informe</th><th class="centro"></th><th><input type="checkbox" id="todo"></th>';
    $boton="<a href='".$_CONFIG['raiz']."evaluacion-de-riesgos/index.php' class='shortcut'><i class='shortcut-icon icon-arrow-left'></i><span class='shortcut-label'>Volver</span> </a>";

    $listado=imprimeEvaluacionesRiesgoDeCliente($where);
  }
  else{
    $listado=imprimeEvaluacionesRiesgo($where);
  }
  //Fin gestión de filtros

  $estadisticas=generaDatosGraficoEvaluaciones($where);
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema sobre evaluación de riesgos<?php echo $texto;?>:</h6>
                   <div id="big_stats" class="cf">
                    
                    <canvas id="graficoBarras" class="chart-holder" width="500" height="250"></canvas>
                    <br /><br />
                    <div class="leyenda2" id='leyenda'></div>
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Evaluaciones</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="<?php echo $_CONFIG['raiz']; ?>evaluacion-de-riesgos/preseleccion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva evaluación</span> </a>
                <a href="<?php echo $_CONFIG['raiz']; ?>generacion-de-informes/gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nuevo informe</span> </a>
                <a href="<?php echo $_CONFIG['raiz']; ?>riesgos/index.php" class="shortcut"><i class="shortcut-icon icon-cog"></i><span class="shortcut-label">Riesgos</span> </a>
                <?php echo $boton;
                if(isset($_GET['codigoCliente'])){?>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
                <?php } ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Evaluaciones de Riesgo registradas<?php echo $texto;?></h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Cliente </th>
                  <?=$textoColumna?>
                </tr>
              </thead>
              <tbody>

        				<?php
                  echo $listado;
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>


<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/chart.js" type="text/javascript"></script>
<script type="text/javascript">
  var datosGrafico = {
      labels: ["Distintos niveles de riesgo"],
      datasets: [
        {
            data: [<?php echo $estadisticas['trivial']; ?>],
            fillColor: "rgba(249,249,249,0.5)",
            strokeColor: "#eee",
            label: 'Trivial'
        },
        {
            data: [<?php echo $estadisticas['tolerable']; ?>],
            fillColor: "rgba(255,245,132,0.5)",
            strokeColor: "#fff584",
            label: 'Tolerable'
        },
        {
            data: [<?php echo $estadisticas['moderado']; ?>],
            fillColor: "rgba(255,191,66,0.5)",
            strokeColor: "#ffbf42",
            label: 'Moderado'
        },
        {
            data: [<?php echo $estadisticas['importante']; ?>],
            fillColor: "rgba(192,0,17,0.5)",
            strokeColor: "#c00011",
            label: 'Importante'
        },
        {
            data: [<?php echo $estadisticas['intolerable']; ?>],
            fillColor: "rgba(0,0,0,0.5)",
            strokeColor: "#000",
            label: 'Intolerable'
        }
      ]
    }

    var opciones={
      scaleOverride : true,
      scaleSteps : <?php echo $estadisticas['max']; ?>,
      scaleStepWidth : <?php echo $estadisticas['escala'] ?>,
      scaleStartValue : 0,
      barValueSpacing:40,
      barDatasetSpacing:20
    }

    var grafico = new Chart(document.getElementById("graficoBarras").getContext("2d")).Bar(datosGrafico,opciones);
</script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/eventoGrafico.js"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>