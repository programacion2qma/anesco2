<?php
  $seccionActiva=1;
  include_once('../cabecera.php');
  
  $res=operacionesClientes();
  $where='AND clientes.eliminado="NO"';
  $texto='';
  $boton="
  <a href='".$_CONFIG['raiz']."clientes/index.php?alta' class='shortcut'><i class='shortcut-icon icon-check'></i><span class='shortcut-label'>Clientes de alta</span> </a>
  <a href='".$_CONFIG['raiz']."clientes/index.php?baja' class='shortcut'><i class='shortcut-icon icon-arrow-down'></i><span class='shortcut-label'>Clientes de baja</span> </a>";

  if(isset($_GET['baja'])){
    $where='AND clientes.eliminado="NO" AND clientes.codigo NOT IN (SELECT clientes.codigo FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE activo="SI" AND contratos.enVigor="SI" AND clientes.eliminado="NO")';
    $texto=' de baja';
    $boton="<a href='".$_CONFIG['raiz']."clientes/index.php?' class='shortcut'><i class='shortcut-icon icon-arrow-left'></i><span class='shortcut-label'>Volver</span> </a>
            <a href='".$_CONFIG['raiz']."clientes/index.php?alta' class='shortcut'><i class='shortcut-icon icon-check'></i><span class='shortcut-label'>Clientes de alta</span> </a>";
  }
  elseif(isset($_GET['alta'])){
    $where="AND contratos.eliminado='NO' AND enVigor='SI' AND facturas.facturaCobrada='SI'";
    $texto=' de alta';
    $boton="<a href='".$_CONFIG['raiz']."clientes/index.php?' class='shortcut'><i class='shortcut-icon icon-arrow-left'></i><span class='shortcut-label'>Volver</span> </a>
            <a href='".$_CONFIG['raiz']."clientes/index.php?baja' class='shortcut'><i class='shortcut-icon icon-arrow-down'></i><span class='shortcut-label'>Clientes de baja</span> </a>"; 
  }

  $estadisticas=estadisticasClientesRestrict($where);

/*$i=0;
  $clientes=consultaBD('SELECT * FROM clientes',true);
  while($cliente=mysql_fetch_assoc($clientes)){
    $consulta=consultaBD("INSERT INTO usuarios(codigo,nombre,usuario,clave,tipo) VALUES (NULL,'".$cliente['EMPNOMBRE']."','usuario".$i."','pass".$i."','CLIENTE');");
    $i++;
    $codigoUsuario=mysql_insert_id();
    $consulta=consultaBD('INSERT INTO usuarios_clientes VALUES (NULL,'.$codigoUsuario.','.$cliente['codigo'].');');
  }*/
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de gestión Clientes:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-check-circle"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Clientes <?php echo $texto;?></div>
                     <!--div class="stat"> <i class="icon-send"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Enviados a la agencia</div-->
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Clientes</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <?php 

                echo $boton;
                $parametro='';
                if($_SESSION['tipoUsuario']=='TECNICO'){
                  $parametro='?tecnico='.$_SESSION['codigoU'];
                }?>
                <a href="<?php echo $_CONFIG['raiz'].'clientes/generaListado.php'.$parametro; ?>" class="shortcut noAjax"><i class="shortcut-icon icon-download"></i><span class="shortcut-label">Listado Cliente</span> </a>
                <br />
                <a href="<?php echo $_CONFIG['raiz'].'clientes/generaClientesContratos.php';?>" class="shortcut noAjax"><i class="shortcut-icon icon-download"></i><span class="shortcut-label">Listado Clientes-Contratos</span> </a>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-times-circle"></i><span class="shortcut-label">Eliminar Cliente</span> </a>
                <a href="eliminadas.php" class="shortcut"><i class="shortcut-icon icon-trash-o"></i><span class="shortcut-label">Eliminados</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Clientes registrados<?php echo $texto;?></h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Nº Cliente </th>
                  <th> CIF </th>
                  <th> Razón Social </th>
                  <th> Nombre comercial </th>
                  <th> C.P. </th>
                  <th> Email contacto </th>
                  <th> Teléfono contacto </th>
                  <th> Estado </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeClientes($where);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>