<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('../../api/phpexcel/PHPExcel.php');
	require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("../documentos/plantillaClientesContratos.xlsx");
	

	$i=3;
	$listado=consultaBD('SELECT EMPID, EMPNOMBRE, EMPCP, contratos.codigoInterno, contratos.fechaInicio, enVigor FROM clientes 
	INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
	INNER JOIN (SELECT * FROM contratos ORDER BY codigo DESC) AS contratos ON ofertas.codigo=contratos.codigoOferta
	WHERE clientes.eliminado="NO" GROUP BY clientes.codigo ORDER BY EMPID',true);
	while($datos=mysql_fetch_assoc($listado)){
		$contrato=formateaReferenciaContrato($datos,$datos);
		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($datos['EMPID']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($datos['EMPNOMBRE']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($contrato);		
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue(formateaFechaWeb($datos['fechaInicio']));
		$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue($datos['enVigor']);
		$i++;
	}
	foreach(range('B','F') as $columnID) {
    	$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
	}


	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('../documentos/Clientes_Contratos.xlsx');


	// Definir headers
	header("Content-Type: application/ms-xlsx");
	header("Content-Disposition: attachment; filename=Clientes_Contratos.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('../documentos/Clientes_Contratos.xlsx');
?>