<?php
session_start();
include_once('funciones.php');
compruebaSesion();

//Carga de PHP Excel (usa el de la API para ahorrar espacio en el servidor)
require_once('../../api/phpexcel/PHPExcel.php');
require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');
require_once('../../api/phpexcel/PHPExcel/IOFactory.php');

//Carga de la plantilla
$objReader=new PHPExcel_Reader_Excel2007();
$documento=$objReader->load("plantilla2.xlsx");

generaExcelClientes($documento);

// Definir headers
header("Content-Type: application/ms-xlsx");
header("Content-Disposition: attachment; filename=Clientes.xlsx");
header("Content-Transfer-Encoding: binary");

// Descargar archivo
readfile('Clientes.xlsx');


function generaExcelClientes($documento){
	$ejercicio=obtieneEjercicioParaWhere();

    $documento->setActiveSheetIndex(0);//Selección hoja
    $query=consultaBD('SELECT * FROM clientes',true);
    $fila=3;
    while($datos=mysql_fetch_assoc($query)){
        $documento->getActiveSheet()->SetCellValue('A'.$fila,$datos['codigo']);
        $documento->getActiveSheet()->SetCellValue('B'.$fila,$datos['EMPNOMBRE']);
        $documento->getActiveSheet()->SetCellValue('C'.$fila,$datos['EMPMARCA']);
        $centros=consultaBD('SELECT COUNT(codigo) AS total FROM clientes_centros WHERE codigoCliente='.$datos['codigo'],true,true);
        $documento->getActiveSheet()->SetCellValue('D'.$fila,$centros['total']);
        /*if(mysql_num_rows($centros)==0){
        	$fila++;	
        } else {
        	while($item=mysql_fetch_assoc($centros)){
        		$documento->getActiveSheet()->SetCellValue('E'.$fila,$item['codigo']);
        		$documento->getActiveSheet()->SetCellValue('F'.$fila,$item['nombre']);
        		$documento->getActiveSheet()->SetCellValue('G'.$fila,$item['direccion']);
        		$documento->getActiveSheet()->SetCellValue('H'.$fila,$item['cp']);
        		$documento->getActiveSheet()->SetCellValue('I'.$fila,$item['localidad']);
        		$documento->getActiveSheet()->SetCellValue('J'.$fila,$item['provincia']);
        		$documento->getActiveSheet()->SetCellValue('K'.$fila,$item['trabajadores']);
        		$documento->getActiveSheet()->SetCellValue('L'.$fila,$item['activo']);
        		$fila++;	
    		}
    	}
    	$documento->getActiveSheet()->getStyle('A'.$fila.':L'.$fila)->getFill()->applyFromArray(array(
        	'type' => PHPExcel_Style_Fill::FILL_SOLID,
        		'startcolor' => array(
             		'rgb' => 'BBBBBB'
        		)
    		));*/
    	$fila++;
    }
    	
    $columnas=array('A','B','C','E','F','G','H','I','J');
    for($i=0;$i<count($columnas);$i++){
        $documento->getActiveSheet()->getColumnDimension($columnas[$i])->setAutoSize(true);//Ajusta el ancho de la columna al contenido
    }

    $objWriter=new PHPExcel_Writer_Excel2007($documento);
    $objWriter->save('Clientes.xlsx');
}