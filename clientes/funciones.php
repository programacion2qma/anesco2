<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales
include_once('../../api/js/firma/signature-to-image.php');
include_once('../Mobile_Detect.php');
//Inicio de funciones específicas


//Parte de gestión Clientes


function operacionesClientes(){
	$res=false;

	if(isset($_GET['enviaCredenciales'])){
		//$res=enviaCredencialesCliente();
	}
	elseif(isset($_POST['codigo'])){
		$res=insertaClientes();
	}
	elseif(isset($_POST['EMPNOMBRE'])){
		$res=insertaClientes();
	}
	elseif(isset($_GET['codigoAlta'])){
		$res=bajaAltaCliente($_GET['codigoAlta'],'NO');
	}
	elseif(isset($_GET['codigoBaja'])){
		$res=bajaCliente($_GET['codigoBaja']);
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoItem($_POST['elimina'],'clientes');
		//$res=eliminaItem('clientes','codigoCliente');
	}

	mensajeResultado('EMPNOMBRE',$res,'Clientes');
    mensajeResultado('elimina',$res,'Clientes', true);
}

function bajaAltaCliente($codigo,$valor){
	$res=true;
	$res=consultaBD('UPDATE clientes SET baja="'.$valor.'" WHERE codigo='.$codigo,true);
}

function bajaCliente($codigo){
	$res=true;
	$res=consultaBD('UPDATE contratos SET enVigor="NO" WHERE codigoOferta IN (SELECT codigo FROM ofertas WHERE codigoCliente='.$codigo.');',true);
	return $res;

}

function insertaClientes(){
	creaIBAN();
	if(isset($_POST['codigo'])){
		$res=actualizaDatos('clientes',time(),'imagenes');
		$res=actualizaContraseña();
		//$res=$res && insertaCentros($_POST['codigo']);
		$res=$res && insertaDocumentacion($_POST['codigo']);
	}
	elseif(isset($_POST['EMPNOMBRE'])){
		$res=insertaDatos('clientes',time(),'imagenes');
		$codigoCliente=$res;
		$res=$res && insertaActuaciones($res);
		//$res=$res && insertaCentros($codigoCliente);
		$res=$res && insertaDocumentacion($codigoCliente);
	}
	
	return $res;
}


function actualizaContraseña(){
	$res=true;
	$usuario=consultaBD('SELECT usuarios.codigo FROM usuarios_clientes INNER JOIN usuarios ON usuarios_clientes.codigoUsuario=usuarios.codigo WHERE usuarios_clientes.codigoCliente='.$_POST['codigo'],true,true);
	$res=consultaBD('UPDATE usuarios SET usuario="'.$_POST['usuario'].'",clave="'.$_POST['clave'].'" WHERE codigo='.$usuario['codigo'],true);
	return $res;
}

function insertaActuaciones($cliente){
	$res=true;//Siempre que vayamos a usarlo con un AND, inicializarlo a true

	conexionBD();
	
	$res=$res && consultaBD("INSERT INTO actuaciones_cliente VALUES(NULL, '$cliente',false,false, false,false)");

	cierraBD();

	return $res;
}


function gestionClientes(){
	
	operacionesClientes();
	
	abreVentanaGestionConBotones('Gestión de Clientes','index.php','','icon-edit','',true,'noAjax');
		$datos=compruebaDatos('clientes');

		if($datos){
			creaPestaniasAPI(array('Datos','Gestiones','Certificados de formación','Documentación','Contratos en vigor','Aptos'));

			abrePestaniaAPI(1,true);
		}
	
		echo "<h3 class='apartadoFormulario'> Datos de identificación de la empresa</h3>";

		if($datos && !esSuperAdmin()){
			campoOculto('SI','bloqueado');
		}
	
		abreColumnaCampos();
			campoLogo('ficheroLogo','Logo',0,$datos,'imagenes/','Descargar');
		cierraColumnaCampos();
	
		abreColumnaCampos();
		cierraColumnaCampos(true);
	
		abreColumnaCampos();
			campoOculto(formateaFechaWeb($datos['fecha']),'fecha',fecha());
			//campoOculto($datos,'baja','NO');
			campoOculto($datos,'eliminado','NO');
		
			if(!$datos){
				$numeroCliente = generaNumero();
				$usuario=false;
			} else {
				$numeroCliente = $datos['EMPID'];
				$usuario=consultaBD('SELECT codigoUsuario,usuario,clave FROM usuarios_clientes INNER JOIN usuarios ON usuarios_clientes.codigoUsuario=usuarios.codigo WHERE usuarios_clientes.codigoCliente='.$datos['codigo'],true,true);
				campoOculto($usuario['codigoUsuario'],'codigoUsuario');
			}
		
			campoTexto('EMPID','Nº de cliente',$numeroCliente,'input-mini pagination-right obligatorio');
			campoTexto('EMPNOMBRE','Razón social',$datos,'input-large obligatorio');
			campoSelectConsulta('EMPACTIVIDAD','Actividad','SELECT codigo,nombre AS texto FROM actividades ORDER BY nombre',$datos,'selectpicker span3 show-tick obligatorio');
			campoTexto('EMPDIR','Domicilio social',$datos,'input-large obligatorio');
			campoTexto('EMPLOC','Localidad',$datos,'input-large obligatorio');
			campoTextoSimbolo('EMPTELPRINC','Teléfono de empresa','<i class="icon-phone"></i>',$datos,'input-small pagination-right obligatorio');
			campoOculto($datos,'activo','NO');
		cierraColumnaCampos();	

		abreColumnaCampos();
			campoTexto('EMPMARCA','Nombre comercial',$datos,'input-large obligatorio');
			campoTexto('EMPCIF','CIF/DNI',$datos,'input-small obligatorio');
			campoCNAE('EMPCNAE','CNAE',$datos,'selectpicker show-tick span3 obligatorio');
			campoTexto('EMPCP','Código Postal',$datos,'input-mini pagination-right obligatorio');
			campoTexto('EMPPROV','Provincia',$datos,'input-large obligatorio');
			campoTextoSimbolo('EMPEMAILPRINC','eMail de la empresa','<i class="icon-envelope"></i>',$datos,'input-large obligatorio');
		cierraColumnaCampos(true);

		abreColumnaCampos();
			areaTexto('otrasActividades','Otra actividades',$datos,'areaInforme');
		cierraColumnaCampos();
	
		echo "<br clear='all'>";
		echo "<h3 class='apartadoFormulario'> Datos representante legal</h3>";
	
		abreColumnaCampos();
			campoTexto('EMPRL','Nombre',$datos,'input-large obligatorio');
			campoTexto('EMPRLDNI','DNI',$datos,'input-small obligatorio');
			campoTextoSimbolo('EMPRLEMAIL','eMail','<i class="icon-envelope"></i>',$datos,'input-large');
		cierraColumnaCampos();
	
		abreColumnaCampos();
			campoTexto('EMPRLCARGO','Cargo',$datos,'input-large obligatorio');
			campoTextoSimbolo('EMPRLTEL','Teléfono','<i class="icon-phone"></i>',$datos,'input-small pagination-right');
		cierraColumnaCampos();

		echo "<br clear='all'>";
		echo "<h3 class='apartadoFormulario'> Datos técnicos</h3>";
	
		abreColumnaCampos('span6');
			campoTexto('EMPPC','Persona de contacto',$datos,'input-large obligatorio');
			campoTextoSimbolo('EMPPCEMAIL','eMail de persona de contacto','<i class="icon-envelope"></i>',$datos,'input-large');
			campoTexto('EMPNTRAB','Nº de trabajadores',$datos,'input-mini pagination-right obligatorio');
			campoTexto('EMPASESORIA','Asesoría',$datos,'input-large');
			campoIBAN('EMPIBAN','IBAN',$datos);
			campoTexto('bic','BIC',$datos,'input-small');
		cierraColumnaCampos();	

		abreColumnaCampos();
			campoTextoSimbolo('EMPPCTEL','Tlf. persona de contacto','<i class="icon-phone"></i>',$datos,'input-small pagination-right');
			campoNumero('EMPCENTROS','Nº de centros de trabajo',$datos,'input-mini pagination-right obligatorio');
			campoRadio('EMPANEXO','Anexo',$datos);
			campoTexto('EMPMUTUA','Mutua',$datos,'input-large');
			campoSelectConsulta('comercial','Dpto. Comercial','SELECT codigo, CONCAT(nombre, " ", apellidos) AS texto FROM usuarios WHERE tipo LIKE "COMERCIAL";',$datos,'selectpicker show-tick span3 obligatorio');
			campoTexto('usuario','Usuario',$usuario,'input-large obligatorio');
			campoClaveAleatoria('clave','Contraseña',$usuario,'btn-propio','input-mini obligatorio');
			botonEnvioCredenciales($datos);
		cierraColumnaCampos();	

		abreColumnaCampos();
			areaTexto('observaciones','Observaciones',$datos,'areaInforme');
		cierraColumnaCampos();

		/*echo '<br clear="all">';
		echo "<h3 class='apartadoFormulario'> Centros de trabajo</h3>";
		echo '<div id="divCentros">';
		if($datos){
			$centros=consultaBD('SELECT * FROM clientes_centros WHERE codigoCliente='.$datos['codigo'],true);
			$i=1;
			while($centro=mysql_fetch_assoc($centros)){
				echo '<div id="divDireccion'.$i.'" class="direcciones" style="border-bottom:1px solid #000;margin-bottom:20px;float:left;width:100%;">';
				abreColumnaCampos();
					campoOculto($centro['codigo'],'codigoCentro'.$i);
					campoTexto('nombre'.$i,'Centro de trabajo',$centro['nombre'],'input-large obligatorio');
				cierraColumnaCampos();
				abreColumnaCampos();
					campoRadio('activo'.$i,'Activo',$centro['activo'],'SI');
				cierraColumnaCampos(true);
				abreColumnaCampos();
					campoTexto('direccion'.$i,'Dirección',$centro['direccion'],'input-large obligatorio');
					campoTexto('localidad'.$i,'Localidad',$centro['localidad'],'input-large obligatorio');
				cierraColumnaCampos();
				abreColumnaCampos();
					campoTexto('cp'.$i,'CP',$centro['cp'],'input-mini obligatorio');
					campoTexto('provincia'.$i,'Provincia',$centro['provincia'],'input-large obligatorio');
				cierraColumnaCampos(true);
				abreColumnaCampos();
					campoTexto('trabajadores'.$i,'Nº de trabajadores',$centro['trabajadores'],'input-mini pagination-right trabajadoresCentro');
				cierraColumnaCampos(true);
				crearMapa($centro['direccion'],$centro['cp'],$i);
				echo '<button id="eliminaCentro'.$i.'" style="margin-bottom:10px;" class="btnEliminarCentro btn btn-propio"><i class="icon-trash"></i> Eliminar </button><br/>';
				echo '</div>';
				$i++;
			}
		}
		echo '</div>';*/

		abreColumnaCampos('sinFlotar');
			echo "<h3 class='apartadoFormulario'>Documentación</h3>";
			tablaFicheros($datos);
		cierraColumnaCampos(true);
	
		if($datos){
			pestaniasClientes($datos);
		}

		echo "<br clear='all'>";

	cierraVentanaGestion('index.php',true);
}


function crearMapa($direccion,$cp,$i){
	echo "<fieldset class='sinFlotar'>";

	$calle=$direccion.' '.$cp;
	if(isset($calle)){
		echo "<button style='' type='button' class='btn btn-propio' onclick='cargaClick(".$i.")'><i class='icon-google-plus'></i> Cargar mapa</button>";

		$detect = new Mobile_Detect();
		if( $detect->isAndroid() ) {
		// Android
			echo "<center><a href='geo:0,0?daddr=$calle' class='noAjax enlaceExternoMapa'><div id='map".$i."' class='map'></div></a></center>";	
		} elseif ( $detect->isIphone() ) {
		// iPhone
			echo "<center><a href='https://maps.apple.com/maps?saddr=Current%20Location&daddr=$calle' class='noAjax enlaceExternoMapa'><div id='map".$i."' class='map'></div></a></center>";
		} elseif ( $detect->isWindowsphone() ) {
		// Windows Phone
			echo "<center><a href='maps:$calle' class='noAjax enlaceExternoMapa'><div id='map".$i."' class='map'></div></a></center>";
		} else{
		// Por defecto
			echo "<center><a href='https://www.google.es/maps/place/Calle+$calle' class='noAjax enlaceExternoMapa'><div id='map".$i."' class='map'></div></a></center>";
		//$url = 'http://maps.google.com?daddr=Universidad+de+deusto+bilbao';
		}

		//echo "<center><a href='https://www.google.es/maps/place/Calle+$calle' class='noAjax enlaceExternoMapa'><div id='map' class='map'></div></a></center>";		
	}else{
		echo "<button style='' type='button' class='btn btn-propio' onclick='cargaClick(".$i.")'><i class='icon-google-plus'></i> Cargar mapa</button>";
		echo "<center><div id='map".$i."' class='map'></div></center>";
	}
	echo '</fieldset>';
	echo "<br clear='all'>";
}

//Parte de campos personalizados

function campoSelectProvincia($nombreCampo,$datos){
	$valores=array();
	$nombres=array();
	$provinciasAgencia=array('01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');
	
	foreach ($provinciasAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}

	campoSelect($nombreCampo,'Provincia',$nombres,$valores,$datos);
}

//Fin parte de campos personalizados

function imprimeClientes($where=''){
	global $_CONFIG;
	$estados=array('NO'=>'Alta','SI'=>'Baja');

	conexionBD();

	// if($_SESSION['tipoUsuario']=='TECNICO'){
	// 	$consulta=consultaBD("SELECT clientes.codigo, clientes.fecha, EMPID, EMPNOMBRE AS razonSocial, EMPMARCA AS nombreComercial, EMPCP AS cp, EMPEMAILPRINC AS email, EMPTELPRINC, EMPCIF
	// 		FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
	// 		INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
	// 		LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
	// 		LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
	// 		LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
	// 		WHERE clientes.activo='SI' ".$where."
	// 		AND (contratos.tecnico=".$_SESSION['codigoU']." OR formularioPRL.codigoUsuarioTecnico=".$_SESSION['codigoU'].") 
	// 		GROUP BY clientes.codigo ORDER BY EMPID;");
	// } 
	// else {
		$consulta=consultaBD("SELECT clientes.codigo, clientes.fecha, EMPID, EMPNOMBRE AS razonSocial, EMPMARCA AS nombreComercial, EMPCP AS cp, EMPEMAILPRINC AS email, EMPTELPRINC, EMPCIF
						  FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
						  LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
						  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
						  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
						  WHERE clientes.activo='SI' ".$where." GROUP BY clientes.codigo ORDER BY EMPID;");
	//}

	while($datos=mysql_fetch_assoc($consulta)){
		
		$contratoEnVigor=consultaBD("SELECT enVigor 
									 FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo 
									 LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
									 LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
									 WHERE contratos.eliminado='NO' AND ((enVigor='SI' AND facturas.facturaCobrada='SI') || emitirPlanificacionSinPagar='SI')
									 AND ofertas.codigoCliente='".$datos['codigo']."'
									 ORDER BY contratos.fechaInicio DESC",false,true);

		if($contratoEnVigor){
			$estado='Alta';
			$baja="<li class='divider'></li><li><a href='".$_CONFIG['raiz']."clientes/index.php?codigoBaja=".$datos['codigo']."&baja'><i class='icon-arrow-down'></i> Dar de baja</i></a></li>";
		} else {
			$estado='Baja';
			$baja="";
		}
		
		echo "
			<tr>
				<td>".$datos['EMPID']."</td>
				<td>".$datos['EMPCIF']."</td>
				<td>".$datos['razonSocial']."</td>
				<td>".$datos['nombreComercial']."</td>
				<td>".$datos['cp']."</td>
				<td><a href='mailto:".$datos['email']."' class='noAjax'>".$datos['email']."</a></td>
				<td><a href='tel:".str_replace(' ','',$datos['EMPTELPRINC'])."' class='noAjax'>".$datos['EMPTELPRINC']."</a></td>
				<td>".$estado."</td>
				<td class='centro'>
					<div class='btn-group centro'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."clientes/gestion.php?codigo=".$datos['codigo']."' class='noAjax'><i class='icon-search-plus'></i> Detalles</i></a></li> 
						    <li class='divider'></li>
						    <li><a href='".$_CONFIG['raiz']."centros-trabajo/index.php?codigoCliente=".$datos['codigo']."' class='noAjax'><i class='icon-building'></i> Centros<br/>de trabajo</i></a></li> 
						    ".$baja."
						</ul>
					</div>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	cierraBD();

	/*<li><a href='".$_CONFIG['raiz']."empleados/index.php?codigo=".$datos['codigo']."'><i class='icon-group'></i> Empleados</i></a></li>
	<li class='divider'></li>*/

}

function imprimeClientesEliminados($where=''){
	global $_CONFIG;

	conexionBD();
	$where=defineWhereEmpleado($where);
	$consulta=consultaBD("SELECT codigo, fecha, EMPID, EMPNOMBRE AS razonSocial, EMPMARCA AS nombreComercial, EMPCP AS cp, EMPEMAILPRINC AS email, EMPTELPRINC, EMPCIF 
						  FROM clientes WHERE eliminado='".$where."' ORDER BY EMPID;");

	while($datos=mysql_fetch_assoc($consulta)){
		
		echo "
			<tr>
				<td>".$datos['EMPID']."</td>
				<td>".$datos['EMPCIF']."</td>
				<td>".$datos['razonSocial']."</td>
				<td>".$datos['nombreComercial']."</td>
				<td>".$datos['cp']."</td>
				<td><a href='mailto:".$datos['email']."' class='noAjax'>".$datos['email']."</a></td>
				<td><a href='mailto:".str_replace(' ','',$datos['EMPTELPRINC'])."' class='noAjax'>".$datos['EMPTELPRINC']."</a></td>
				<td class='centro'>
					<div class='btn-group centro'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."clientes/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
						    <li class='divider'></li>
						    <li><a href='".$_CONFIG['raiz']."empleados/index.php?codigo=".$datos['codigo']."'><i class='icon-group'></i> Empleados</i></a></li>
						</ul>
					</div>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	cierraBD();
}

function generaNumero(){
	$consulta=consultaBD("SELECT MAX(EMPID) AS numero FROM clientes", true, true);
	
	return $consulta['numero']+1;
}

function estadisticasClientesRestrict($where=''){
	$res=array();

	conexionBD();

	// if($_SESSION['tipoUsuario']=='TECNICO'){
	// 	$consulta=consultaBD("SELECT COUNT(DISTINCT clientes.codigo) AS total
	// 		FROM clientes
	// 		INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
	// 		INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
	// 		LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
	// 		LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
	// 		LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
	// 		WHERE clientes.activo='SI' ".$where."
	// 		AND (contratos.tecnico=".$_SESSION['codigoU']." || formularioPRL.codigoUsuarioTecnico=".$_SESSION['codigoU'].")",false, true);
	// } 
	// else {
		$consulta=consultaBD("SELECT COUNT(DISTINCT clientes.codigo) AS total 
							  FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente 
							  LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
							  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
						  	  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo AND facturas.tipoFactura!='ABONO'
							  WHERE clientes.activo='SI' ".$where,false, true);
	//}

	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

function estadisticasClientesEliminados($where=''){
	$res=array();

	conexionBD();

	$where=defineWhereEmpleado($where);
	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM clientes WHERE eliminado='".$where."' ORDER BY EMPNOMBRE;",false, true);

	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

function imprimeEmpleadosCliente($codigo){
	global $_CONFIG;

	conexionBD();

	$consulta=consultaBD("SELECT * FROM empleados WHERE codigoCliente = ".$codigo." ORDER BY codigoInterno;");
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td class='centro'>".$datos['codigoInterno']."</td>
				<td>".$datos['nombre']."</td>
				<td>".$datos['apellidos']."</td>
				<td>".$datos['dni']."</td>
				<td><a href='tel:".$datos['telefono']."'>".formateaTelefono($datos['telefono'])."</a></td>
				<td><a href='mailto:".$datos['email']."'>".$datos['email']."</a></td>
			</tr>";
	}
	cierraBD();
}


function estadisticasEmpleadosRestrict($codigo){
	$res=array();

	conexionBD();
	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM empleados WHERE codigoCliente=".$codigo,false, true);

	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

function listadoClientes($objPHPExcel){
	conexionBD();
	if(isset($_GET['tecnico'])){
		$consulta=consultaBD("SELECT clientes.codigo, clientes.fecha, EMPID, EMPNOMBRE AS razon_s, EMPCP AS cp, EMPEMAILPRINC AS email, clientes.comercial FROM clientes 
			INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
			INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
			LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
			WHERE clientes.activo='SI' AND contratos.enVigor='SI' AND (contratos.tecnico=".$_SESSION['codigoU']." || formularioPRL.codigoUsuarioTecnico=".$_SESSION['codigoU'].") GROUP BY clientes.codigo ORDER BY EMPID;");
	} else {
		$consulta=consultaBD("SELECT clientes.codigo, clientes.fecha, EMPID, EMPNOMBRE AS razon_s, EMPCP AS cp, EMPEMAILPRINC AS email, comercial FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE activo='SI' AND enVigor='SI' ORDER BY EMPID;");
	}
	cierraBD();

	$i=4;
	while($datos=mysql_fetch_assoc($consulta)){
		$comercial=datosRegistro('usuarios',$datos['comercial']);
		$estadoFactura=estadoFactura($datos['codigo']);
		$vencimiento=estadoContrato($datos['codigo']);
		$planificacion=estadoPlanificacion($datos['codigo'],true);
		$estilo='';
		if($planificacion=='SI'){
			$planificacion='';
    		$estilo=array(
        			'fill' => array(
            			'type' => PHPExcel_Style_Fill::FILL_SOLID,
            			'color' => array('rgb' => 'FE0700')
        				)
    				);
		} else if($planificacion=='NO') {
			$planificacion='';
			$estilo=array(
        			'fill' => array(
            			'type' => PHPExcel_Style_Fill::FILL_SOLID,
            			'color' => array('rgb' => '009900')
        				)
    				);
		}

		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($datos['EMPID']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($estadoFactura);
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($datos['razon_s']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue($datos['cp']);
		$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue($planificacion);
		if($estilo!=''){
			$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($estilo);
		}
		$objPHPExcel->getActiveSheet()->getCell('G'.$i)->setValue($vencimiento['vencimiento']);
		$objPHPExcel->getActiveSheet()->getCell('H'.$i)->setValue($vencimiento['tecnico']);
		$objPHPExcel->getActiveSheet()->getCell('I'.$i)->setValue($comercial['nombre']." ".$comercial['apellidos']);
		$i++;
	}
	foreach(range('B','I') as $columnID) {
    	$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
	}
}

function tablaFicheros($datos){
	$th='';
	if($_SESSION['tipoUsuario']=='ADMIN'){
		echo "Para eliminar un documento, marca el último check y guarda el formulario";
		$th='<th> Eliminar </th>';
	}
	echo"
	 		<center>
				<table class='table table-bordered mitadAncho' id='tablaFicheros'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Denominación </th>
		                    <th> Código </th>
							<th> Fichero </th>
							<th> Pertenece </th>
							<th> Fecha </th>
							<th> Área </th>
							<th> Visible </th>
							$th
                    	</tr>
                  	</thead>
                  	<tbody>";

					    $i=0;
						if($datos){
							$consulta=consultaBD("SELECT * FROM documentos_clientes WHERE codigoCliente=".$datos['codigo'],true);				  
							while($fichero=mysql_fetch_assoc($consulta)){
							 	echo "<tr>";
							 		campoOculto($fichero['codigo'],'codigoFichero'.$i);
									campoTextoTabla('nombreSubido'.$i,$fichero['nombre']);
									campoTextoTabla('codigoDocumentoSubido'.$i,$fichero['codigoDocumento'],'input-small');
									campoFichero('ficheroSubido'.$i,'',1,$fichero['fichero'],'../documentos/planificacion/','Ver/descargar');
									campoSelect('perteneceSubido'.$i,'',array('Administración','Técnico','Vigilancia de la salud'),array('ADMON','TECNICO','VS'),$fichero['pertenece'],'selectpicker span2 show-tick','',1);
									campoFechaTabla('fechaSubido'.$i,$fichero['fecha']);
									campoTextoTabla('areaSubido'.$i,$fichero['area']);
									campoCheckTabla('visibleSubido'.$i,$fichero['visible']);
									if($_SESSION['tipoUsuario']=='ADMIN'){
										campoCheckTabla('eliminarDocumento'.$i,$fichero['codigo']);
									}
								echo"
									</tr>";
								$i++;
							}
						}
						campoOculto($i,'ficherosTotales');
						$i=0;
						echo "<tr>";
							campoTextoTabla('nombre0');
							campoTextoTabla('codigoDocumento0','','input-small');
							campoFichero('fichero0','Documento',1);
							campoSelect('pertenece0','',array('Administración','Técnico','Vigilancia de la salud'),array('ADMON','TECNICO','VS'),false,'selectpicker span2 show-tick','',1);
							campoFechaTabla('fecha0');
							campoTextoTabla('area0');
							campoCheckTabla('visible0');
							if($_SESSION['tipoUsuario']=='ADMIN'){
								echo '<td></td>';
							}
						echo "</tr>";

						echo "
					</tbody>
	                </table>
	            

					<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaFicheros\");'><i class='icon-plus'></i> Añadir documento</button>
				</center><br />";
}


function insertaDocumentacion($codigo){
	$res=true;

	conexionBD();
	
	$datos=arrayFormulario();
	$i=0;
	while(isset($datos['codigoFichero'.$i])){
		if(isset($datos['eliminarDocumento'.$i])){
			
			// 17/05/2023 cambiamos la función datos registro por una consulta para evitar el cierre de la conexion de la BD
			// $documento=datosRegistro('documentos_clientes',$datos['codigoFichero'.$i]);
			$documento = consultaBD("SELECT * FROM documentos_clientes WHERE codigo = ".$datos['codigoFichero'.$i].";", false, true);
			if($documento['fichero']!='' && $documento['fichero']!='NO'){
				unlink('../documentos/planificacion/'.$documento['fichero']);
				$res=$res && consultaBD('DELETE FROM documentos_clientes WHERE codigo='.$documento['codigo']);
			}
		} else {
			$datos['visibleSubido'.$i]=isset($datos['visibleSubido'.$i])?$datos['visibleSubido'.$i]:'NO';
			$res=$res && consultaBD('UPDATE documentos_clientes SET nombre="'.$datos['nombreSubido'.$i].'",pertenece="'.$datos['perteneceSubido'.$i].'",codigoDocumento="'.$datos['codigoDocumentoSubido'.$i].'",fecha="'.$datos['fechaSubido'.$i].'",area="'.$datos['areaSubido'.$i].'",visible="'.$datos['visibleSubido'.$i].'" WHERE codigo='.$datos['codigoFichero'.$i]);
		}
		$i++;
	}
	$i=0;
	while(isset($_FILES['fichero'.$i])){
		if($_FILES['fichero'.$i]['tmp_name'] != ''){
			$fichero=subeDocumento('fichero'.$i,time().$i,'../documentos/planificacion');
			$datos['visible'.$i]=isset($datos['visible'.$i])?$datos['visible'.$i]:'NO';
			$res=$res && consultaBD("INSERT INTO documentos_clientes VALUES(NULL, '$codigo', '".$datos['nombre'.$i]."', '".$fichero."', '".$datos['pertenece'.$i]."', '".$datos['codigoDocumento'.$i]."', '".$datos['fecha'.$i]."', '".$datos['area'.$i]."', '".$datos['visible'.$i]."',".$_SESSION['codigoU'].");");
		}
		$i++;
	}
	
	cierraBD();

	return $res;
}

//Fin parte de gestión Clientes
