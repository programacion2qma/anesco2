<?php
  $seccionActiva=17;
  include_once("../cabecera.php");
  gestionDocumento();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$('.selectpicker').selectpicker();
	$(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});

	$('#insertarEtiqueta').click(function(){
		$('#cajaGestion').modal({'show':true,'backdrop':'static','keyboard':false});
	});

	$('.eliminaEtiqueta').click(function(){
    	oyenteEliminar($(this));
  	});

  $('#enviar').click(function(){//Botón "Guardar" del modal de las etiquetas
    var codigoEtiqueta=$('#etiqueta').val();
    var etiqueta="<div id='divetiqueta"+codigoEtiqueta+"'>"+$('#etiqueta option:selected').attr('data-content')+"<a href='#' style='color:red;font-size:16px;margin-left:5px;' class='eliminaEtiqueta noAjax' etiqueta='"+codigoEtiqueta+"'><i class='icon-times'></i></a><br/>";
    $('#cajaGestion').modal('hide');
    
    $('#campoEtiquetas').prepend(etiqueta);
    var campoOculto="<input type='hidden' class='hide nuevaetiqueta"+codigoEtiqueta+"' name='nuevasEtiquetas[]' value='"+codigoEtiqueta+"' />";
    $('#campoEtiquetas').prepend(campoOculto);
    
    $('.eliminaEtiqueta').click(function(){
      oyenteEliminar($(this));
    });
  });
});

function oyenteEliminar(elem){
  var name='etiqueta'+elem.attr('etiqueta');
  if($('.'+name).length ){
    $('.'+name).attr('id','eliminaEtiqueta[]');
    $('.'+name).attr('name','eliminaEtiqueta[]');
  } else if($('.nueva'+name).length ){
    $('.nueva'+name).remove();
  }
  $('#div'+name).remove();
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>