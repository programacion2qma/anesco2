<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de documentos

function obtieneDatosEspacioTrabajo(){
	if(isset($_GET['codigoEspacioTrabajo'])){
		global $_CONFIG;

		$datos=consultaBD("SELECT nombre FROM espacios_trabajo WHERE codigo='".$_GET['codigoEspacioTrabajo']."';",true,true);
		$_SESSION['codigoEspacioTrabajo']=$_GET['codigoEspacioTrabajo'];
		$_SESSION['nombreEspacioTrabajo']="<a href='".$_CONFIG['raiz']."categorias/gestion.php?codigo=".$_GET['codigoEspacioTrabajo']."'>".$datos['nombre']."</a>";
	}

	$res=array('codigo'=>$_SESSION['codigoEspacioTrabajo'],'nombre'=>$_SESSION['nombreEspacioTrabajo']);
	return $res;
}

function estadisticasDocumentosEspacioTrabajo($codigoEspacioTrabajo){
	return consultaBD("SELECT COUNT(codigoDocumento) AS total FROM documentos_pertenecen_espacios_trabajo WHERE codigoEspacioTrabajo='".$codigoEspacioTrabajo."';",true,true);
}

function estadisticasGestorDocumental(){
	$res=array();

	conexionBD();
	
	$datos=consultaBD("SELECT COUNT(codigo) AS total FROM espaciosTrabajo;",false,true);
	$res['espaciosTrabajo']=$datos['total'];

	$datos=consultaBD("SELECT COUNT(codigo) AS total FROM documentos;",false,true);
	$res['documentos']=$datos['total'];
	
	cierraBD();

	return $res;
}

function operacionesDocumentos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDocumento();
	}
	elseif(isset($_POST['nombre'])){
		$res=creaDocumento();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatosConFichero('documentos','fichero','../documentos/documentos-internos/');
	}

	mensajeResultado('nombre',$res,'Documentos');
	mensajeResultado('elimina',$res,'Documentos', true);
}

function operacionesEnvio(){
	$res=true;

	if(isset($_POST['correo'])){
		$res=enviaDocumento();
	}

	mensajeResultado('correo',$res,'Envío');
	mensajeResultado('elimina',$res,'Envío', true);
}



function gestionDocumento(){
	$espacioTrabajo=obtieneDatosEspacioTrabajo();
	operacionesDocumentos();

	abreVentanaGestion('Gestión de documentos para la carpeta '.$espacioTrabajo['nombre'],'index.php','','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('documentos');

	campoTexto('nombre','Nombre',$datos);
	campoFichero('fichero','Fichero',0,$datos,'../documentos/documentos-internos/');
	campoFecha('fecha','Fecha de creación',$datos);
	areaTexto('descripcion','Descripción',$datos);

	campoEspaciosTrabajo($datos,$espacioTrabajo['codigo']);
	campoEtiquetas($datos);

	campoOculto(fecha(),'fechaModificacion');
	campoOculto($datos,'codigoUsuario',$_SESSION['codigoU']);

	cierraVentanaGestion('index.php',true);

	abreVentanaModal('etiquetas');
	campoSelectEtiquetas();
	cierraVentanaModal();
}


function gestionEnvioDocumento(){
	global $_CONFIG;

	$espacioTrabajo=obtieneDatosEspacioTrabajo();
	$fichero=obtieneDatosFichero();
	$firma=firmaEnvioDocumento();

	operacionesEnvio();

	abreVentanaGestion('Envíos del documento '.$fichero['nombre'].' de la carpeta '.$espacioTrabajo['nombre'],'?');
	campoTextoSimbolo('correo','Dirección de correo',"<i class='icon-envelope'></i>",'','span4');
	campoTexto('asunto','Asunto',$_CONFIG['tituloGeneral'].' - Envío de documentación','span5');
	areaTexto('mensaje','Mensaje',$firma,'areaTextoEnriquecido');

	campoOculto($fichero['fichero'],'fichero');
	campoOculto($fichero['codigo'],'codigoDocumento');
	cierraVentanaGestion('index.php',false);

	abreVentanaGestion('Envíos realizados','?');
	imprimeEnviosDocumento($fichero['codigo']);
	cierraVentanaGestion('index.php',false,false);
}


function creaDocumento(){
	$res=insertaDatos('documentos',time(),'../documentos/documentos-internos/');

	if($res){
		$codigoDocumento=$res;
		$res=asignaDocumentoEspaciosTrabajo($codigoDocumento);
		$res=$res && asignaEtiquetasDocumento($codigoDocumento);
	}

	return $res;
}

function actualizaDocumento(){
	$res=actualizaDatos('documentos',time(),'../documentos/documentos-internos/');

	if($res){
		$res=asignaDocumentoEspaciosTrabajo($_POST['codigo'],true);
		$res=$res && asignaEtiquetasDocumento($_POST['codigo']);
	}

	return $res;
}

function asignaDocumentoEspaciosTrabajo($codigoDocumento,$actualizacion=false){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	$espaciosTrabajo=consultaBD("SELECT COUNT(codigo) AS total FROM espacios_trabajo;",false,true);
	$numeroEspaciosTrabajo=$espaciosTrabajo['total'];

	if($actualizacion){
		$res=consultaBD("DELETE FROM documentos_pertenecen_espacios_trabajo WHERE codigoDocumento='$codigoDocumento';");
	}

	for($i=0;$i<$numeroEspaciosTrabajo;$i++){

		if(isset($datos['espacioTrabajo'.$i])){
			$res=$res && consultaBD("INSERT INTO documentos_pertenecen_espacios_trabajo VALUES('$codigoDocumento','".$datos['espacioTrabajo'.$i]."');",true);
		}

	}

	cierraBD();
	return $res;
}


function asignaEtiquetasDocumento($codigoDocumento){
  $res=true;

  if(isset($_POST['eliminaEtiqueta'])){
    $eliminaEtiquetas=$_POST['eliminaEtiqueta'];
    conexionBD();
    for($i=0;$i<count($eliminaEtiquetas);$i++){
      $res=$res && consultaBD("DELETE FROM etiquetas_documentos WHERE codigoDocumento=".$codigoDocumento." AND codigoEtiqueta=".$eliminaEtiquetas[$i]);
    }
    cierraBD(); 
  }
  
  if(isset($_POST['nuevasEtiquetas'])){
    $nuevasEtiquetas=$_POST['nuevasEtiquetas']; 
    $codigosEtiquetas=array();
    
    if(isset($_POST['codigoEtiquetas'])){
      $codigosEtiquetas=$_POST['codigoEtiquetas'];
    }

    conexionBD();
    for($i=0;$i<count($nuevasEtiquetas);$i++){
      if(!in_array($nuevasEtiquetas[$i],$codigosEtiquetas)){
        $res=$res && consultaBD("INSERT INTO etiquetas_documentos VALUES('$codigoDocumento','".$nuevasEtiquetas[$i]."')");
      }
    }
    cierraBD();
  }

  return $res;
}

function imprimeDocumentos($codigoEspacioTrabajo){
	global $_CONFIG;
	$extesiones=array('zip, rar, 7z'=>'icon-file-archive-o','mp3, 3gp, midi, aac, wav, wma'=>'icon-file-audio-o','php, html, css, js, java, c, cpp, py, xml, xhtml, htm'=>'icon-file-code-o','xls, xlsx, ods, ots'=>'icon-file-excel-o','png, jpg, jpeg, gif, bmp, psd'=>'icon-file-image-o','mp4, mkv, avi, wmm, flv, mov'=>'icon-file-movie-o','pdf'=>'icon-file-pdf-o','odp, ppt, pptx'=>'icon-file-powerpoint-o','doc, docx, odt, ott'=>'icon-file-word-o');

	conexionBD();
	$consulta=consultaBD("SELECT documentos.codigo, documentos.nombre, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS autor, fichero, fecha, fechaModificacion FROM (documentos INNER JOIN usuarios ON documentos.codigoUsuario=usuarios.codigo) INNER JOIN documentos_pertenecen_espacios_trabajo AS documentos_pertenecen_espaciosTrabajo ON documentos.codigo=documentos_pertenecen_espaciosTrabajo.codigoDocumento WHERE codigoEspacioTrabajo='$codigoEspacioTrabajo';");
	while($datos=mysql_fetch_assoc($consulta)){
		$icono=obtieneIconoDocumento($extesiones,$datos['fichero']);
		$etiquetas=obtieneEtiquetasDocumento($datos['codigo']);
		$botonesExtra=obtieneBotonesExtra($datos['codigo'],$datos['fichero']);

		echo "
			<tr>
				<td><a href='".$_CONFIG['raiz']."categorias/gestion.php?codigo=".$datos['codigo']."'><i class='".$icono."'></i> ".$datos['nombre']."</a></td>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['autor']."</td>
				<td>".formateaFechaWeb($datos['fechaModificacion'])."</td>
				<td> ".$etiquetas."</td>
				<td class='centro'>
					<div class='btn-group'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."categorias/gestion.php?codigo=".$datos['codigo']."'><i class='icon-edit'></i> Editar</i></a></li>
						    $botonesExtra
						</ul>
					</div>
				</a>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	cierraBD();
}

function obtieneBotonesExtra($codigo,$documento){
	global $_CONFIG;
	$res='';

	if($documento!='NO'){
		$res="<li class='divider'></li>
			  <li><a href='".$_CONFIG['raiz']."categorias/envio.php?codigoDocumento=".$codigo."'><i class='icon-send'></i> Enviar</i></a></li>
			  <li class='divider'></li>
			  <li><a href='".$_CONFIG['raiz']."documentos/documentos-internos/".$documento."' class='noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar fichero</i></a></li>";
	}

	return $res;
}

function obtieneIconoDocumento($extensiones,$nombre){
	$res='icon-file-text-o';
	$extensionFichero=explode('.',$nombre);
	$extensionFichero=end($extensionFichero);

	foreach($extensiones as $extension => $icono) {
		if(substr_count($extension,$extensionFichero)==1){
			$res=$icono;
		}
	}

	return $res;
}

function obtieneEtiquetasDocumento($codigoDocumento){
	$res="";

	$consulta=consultaBD("SELECT codigo, nombre, color FROM etiquetas INNER JOIN etiquetas_documentos AS etiquetasDocumentos ON etiquetas.codigo=etiquetasDocumentos.codigoEtiqueta WHERE codigoDocumento='$codigoDocumento'");
	while($datosE=mysql_fetch_assoc($consulta)){
		$res.="<span class='label' style='background-color:".$datosE['color'].";'><i class='icon-tag'></i> ".$datosE['nombre']."</span> ";
	}

	return $res;
}

function campoEspaciosTrabajo($datos,$valorPorDefecto){
	$valores=array();
	$valoresCampos=array();
	$textosCampos=array();

	conexionBD();

	$consulta=consultaBD("SELECT codigo, nombre FROM espacios_trabajo ORDER BY nombre;");
	while($datosC=mysql_fetch_assoc($consulta)){
		array_push($valoresCampos,$datosC['codigo']);
		array_push($textosCampos,$datosC['nombre']);
	}

	if(!isset($datos['codigo'])){
		array_push($valores,$valorPorDefecto);
	}
	else{
		$valores=consultaArray("SELECT codigoEspacioTrabajo FROM documentos_pertenecen_espacios_trabajo WHERE codigoDocumento='".$datos['codigo']."';");
	}

	cierraBD();

	campoCheckEspaciosTrabajo('espacioTrabajo','Carpeta/s',$valores,$textosCampos,$valoresCampos,true);
}


function campoEtiquetas($datos){
  echo "
  <div class='control-group'>                     
      <label class='control-label'>Etiquetas:</label>
      <div class='controls datoSinInput' id='campoEtiquetas'>";
      
    if(isset($datos['codigo'])){
      $consulta=consultaBD("SELECT codigo, nombre, color FROM etiquetas INNER JOIN etiquetas_documentos AS etiquetasDocumentos ON etiquetas.codigo=etiquetasDocumentos.codigoEtiqueta WHERE codigoDocumento='".$datos['codigo']."'",true);
      while($datosE=mysql_fetch_assoc($consulta)){
        campoOculto($datosE['codigo'],'codigoEtiquetas[]','','hide etiqueta'.$datosE['codigo']);
        echo "<div id='divetiqueta".$datosE['codigo']."'>";
        echo "<span class='label' style='background-color:".$datosE['color'].";'><i class='icon-tag'></i> ".$datosE['nombre']."</span><a href='#' style='color:red;font-size:16px;margin-left:5px;' class='eliminaEtiqueta noAjax' etiqueta='".$datosE['codigo']."'><i class='icon-times'></i></a><br/> ";
        echo "</div>";
      }
    }
    
    echo "
        <br />
        <button type='button' class='btn btn-default btn-small' id='insertarEtiqueta'><i class='icon-plus'></i> Añadir etiqueta</button> 
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}


function campoCheckEspaciosTrabajo($nombreCampo,$texto, $valor=array(), $textosCampos=array('Si','No'),$valoresCampos=array('SI','NO'),$salto=false){
	$nombre=$nombreCampo;

	echo "
	<div class='control-group'>                     
      <label class='control-label'>$texto:</label>
      <div class='controls'>";

    for($i=0;$i<count($textosCampos);$i++){
    	if(!is_array($nombreCampo)){
    		$nombre=$nombreCampo.$i;
    	}

    	echo "<label class='checkbox inline'>
    			<input type='checkbox' name='$nombre' value='".$valoresCampos[$i]."'";
    	if(is_array($valor) && in_array($valoresCampos[$i],$valor)){
    		echo " checked='checked'";
    	} 
    	echo ">".$textosCampos[$i]."</label>";
    	if($salto){
    		echo "<br />";
    	}
    }
    echo "
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}

function campoSelectEtiquetas(){
	echo "<div class='control-group'>                     
		      <label class='control-label' for='etiqueta'>Etiqueta:</label>
		      <div class='controls'>
        		<select name='etiqueta' id='etiqueta' class='selectpicker show-tick'>";

	    $consulta=consultaBD("SELECT * FROM etiquetas ORDER BY nombre;",true);
	    while($datos=mysql_fetch_assoc($consulta)){
			echo "<option value='".$datos['codigo']."' data-content='<span class=\"label\" style=\"background-color:".$datos['color'].";\"><i class=\"icon-tag\"></i> ".$datos['nombre']."</span> '></option>";
	    }
	
	echo "     	</select>
      		</div> <!-- /controls -->       
    	</div> <!-- /control-group -->";
}

function obtieneDatosFichero(){
	$datos=consultaBD("SELECT codigo, nombre, fichero FROM documentos WHERE codigo='".$_REQUEST['codigoDocumento']."';",true,true);
	return $datos;
}

function firmaEnvioDocumento(){
	return "
	<br /><br /><br />
	<img src='http://crmparapymes.com.es/sistemas-de-calidad-iso-9001-2015/img/logo.png' width='100px' /><br />
	<a href='http://www.qmaconsultores.com'>www.qmaconsultores.com</a><br />
	Av. San Francisco Javier, 9, 41018 Sevilla, España<br />
	Tel: (+34) 955 097 453 <br />
	<br />
	Aquest missatge pot contenir informació confidencial. Si vostè no n' és el destinatari, si us plau, esborri'l i faci'ns-ho saber immediatament, no el reenviï ni en copiï el contingut, ja que està sotmès a secret professional i la llei en reglamenta la divulgació. Si la seva empresa no permet rebre missatges d'aquesta mena, per favor, faci'ns-ho saber immediatament.<br />
	<br />
	Este mensaje puede contener información confidencial, sometida a secreto profesional o cuya divulgación esté prohibida por la ley. Si usted no es el destinatario del mensaje, por favor bórrelo y notifíquenoslo inmediatamente, no lo reenvíe ni copie su contenido. Si su empresa no permite la recepción de mensajes de este tipo, por favor háganoslo saber inmediatamente.<br />
	<br />
	Information contained in this message might be Privileged/Confidential and protected by a professional privilege or whose disclosure might be prohibited by Law. If you are not the addressee indicated in this message you may not copy or deliver this message to anyone. In such case, you should destroy this message, and notify us immediately. If your company does not allow the reception of this kind of messages, please let us know immediately.";
}


function imprimeEnviosDocumento($codigoDocumento){
	echo "<table class='table table-striped table-bordered datatable'>
              <thead>
                <tr>
                  <th> eMail </th>
                  <th> Fecha </th>
                  <th> Hora </th>
                  <th> Asunto </th>
                  <th> Mensaje </th>
                </tr>
              </thead>
              <tbody>";

              $consulta=consultaBD("SELECT fecha, hora, correo, asunto, mensaje FROM documentos_enviados WHERE codigoDocumento='$codigoDocumento';",true);
              while($datos=mysql_fetch_assoc($consulta)){
              	$mensaje=substr($datos['mensaje'], 0,strrpos($datos['mensaje'],'<img'));

              	echo "<tr>
              			<td> ".$datos['correo']." </td>
              			<td> ".formateaFechaWeb($datos['fecha'])." </td>
              			<td> ".formateaHoraWeb($datos['hora'])." </td>
              			<td> ".$datos['asunto']." </td>
              			<td> ".$mensaje." </td>
              		  </tr>";
              }

    echo "
              </tbody>
            </table>";
}

function enviaDocumento(){
	$res=true;
	$datos=arrayFormulario();


	$mensaje=$datos['mensaje'];
	$adjunto='../documentos/documentos-internos/'.$datos['fichero'];
	

	if (!enviaEmail($datos['correo'], $datos['asunto'], $mensaje ,$adjunto)){
		$res=false;
	}
	else{
		$fecha=date('Y')."-".date('m')."-".date('d');
		$hora=date('H').":".date('i').":00";

		$res=consultaBD("INSERT INTO documentos_enviados VALUES(NULL,'".$datos['codigoDocumento']."','$fecha', '$hora', '".$datos['correo']."','".$datos['asunto']."', '".$datos['mensaje']."');",true);
	}

	return $res;
}

//Fin parte de documentos