<?php
  $seccionActiva=17;
  include_once("../cabecera.php");
  gestionEnvioDocumento();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-wysihtml5.es-ES.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.selectpicker').selectpicker();
	$('#mensaje').wysihtml5({locale: "es-ES"});
});
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>