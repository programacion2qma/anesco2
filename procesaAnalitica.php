<?php
/*ini_set('display_errors','1');
error_reporting(E_ALL);*/

session_start();
include_once('funciones.php');
require_once('class.pdf2text.php');

$res=true;

$ficheros=scandir('documentos/analiticas/');

$ficheros=array_diff($ficheros,array('.','..','.DS_Store'));//Para quitar el . y .. del listado de ficheros

$lectorPDF=new PDF2Text();

conexionBD();

foreach($ficheros as $fichero){
	$res=$res && procesaFicheroAnalitica($lectorPDF,$fichero);
}

cierraBD();

if($res){
	echo "Todos los ficheros se han procesado correctamente :D";
}
else{
	echo "Algo ha fallado :(<br />";
	echo mysql_error();
}


function procesaFicheroAnalitica($lectorPDF,$fichero){
	$res=true;

	$codigoReconocimiento=obtieneCodigoReconocimiento($fichero);
	
	if($codigoReconocimiento!='NULL'){

		$lectorPDF->setFilename('documentos/analiticas/'.$fichero);
		$lectorPDF->decodePDF();
		$contenido=$lectorPDF->output();

		$contenido=procesaContenido($contenido);
		
		$datos=divideContenido($contenido);

		$res=rename('documentos/analiticas/'.$fichero,'documentos/analiticas-procesadas/'.$fichero);
		if($res){
			$res=consultaBD("UPDATE reconocimientos_medicos SET hemograma='".$datos['hemograma']."', bioquimica='".$datos['bioquimica']."', orina='".$datos['orina']."', ficheroAnalitica='$fichero' WHERE codigo='$codigoReconocimiento';");
		}
	}

	return $res;
}


function procesaContenido($contenido){
	$res='';
	$contenido=str_replace("\r\n",' ',$contenido);//Para quitar los saltos de línea y poder recorrer el PDF carácter a carácter
	$datos=array();

	//Extracción de datos del PDF
	$extrae=true;
	$valor='';
	for($i=0;$i<strlen($contenido);$i++){
		if($contenido[$i]=='('){
			$extrae=true;
		}
		elseif($contenido[$i]==')'){
			$extrae=false;
			array_push($datos,$valor);
			$valor='';
		}
		elseif($extrae){
			$valor.=utf8_encode($contenido[$i]);
		}
	}

	
	//Eliminación de posiciones no válidas (línea con datos del pie de página)
	$eliminacion=false;
	$tam=count($datos);
	for($i=18;$i<$tam;$i++){
		if(substr_count($datos[$i],'MUESTRA')>0){
			$eliminacion=true;
		}
		elseif($datos[$i]=='GLUCOSA BASAL'){
			$eliminacion=false;
		}

		if($eliminacion){
			unset($datos[$i]);
		}
	}

	$datos=array_values($datos);//Re-indexación del array, para que no se produzcan Undefined offset en el siguiente bucle for

	$palabrasObviar=array('Análisis','Resultado','Unidades','Val. Referencia');
	$encabezados=array('SERIE BLANCA','SERIE ROJA','SERIE PLAQUETARIA','ERITROSEDIMENTACIÓN','PERFIL SISTEMÁTICO DE ORINA','SEDIMENTO URINARIO');
	
	for($i=18;$i<count($datos);$i++){//Los datos empiezan en el índice 18
		if(substr_count($datos[$i],'Fecha informe')>0){
			
			$i=count($datos);

		}
		elseif(!in_array($datos[$i],$palabrasObviar) && substr_count($datos[$i],'Análisis')==0){//"Análisis" debe de contener un caracter inválido que impide la comparación directa con in_array
			
			$res.=$datos[$i].' ';

			if(substr_count($datos[$i],']')>0 || in_array($datos[$i],$encabezados)){
				$res.='\n';
			}
		}
	}

	$res=str_replace('\ AST \\','(AST)',$res);
	$res=str_replace('\ ALT \\','(ALT)',$res);

	$res=tabulaContenido($res);

	return $res;
}

function tabulaContenido($contenido){
	$nombresValores=array(
		'LEUCOCITOS',
		'CAYADOS %',
		'SEGMENTADOS %',
		'LINFOCITOS %',
		'MONOCITOS %',
		'EOSINOFILOS %',
		'BASOFILOS %',
		'HEMATIES',
		'HEMOGLOBINA CORPUSCULAR MEDIA',
		'HEMOGLOBINA',
		'HEMATOCRITO',
		'VOLUMEN CORPUSCULAR MEDIO',
		'CONC. HEMOGL. CORP. MEDIA',
		'PLAQUETAS',
		'VOLUMEN PLAQUETARIO MEDIO',
		'HORA',
		'INDICE DE KATZ',
		'GLUCOSA BASAL',
		'CREATININA',
		'ACIDO URICO',
		'COLESTEROL TOTAL',
		'HDL - COLESTEROL',
		'LDL - COLESTEROL',
		'TRIGLICERIDOS',
		'INDICE DE ATEROGENICIDAD LDL/HDL',
		'GOT (AST)',
		'GPT (ALT)',
		'pH',
		'DENSIDAD',
		'PROTEINURIA',
		'GLUCOSURIA',
		'CUERPOS CETONICOS',
		'UROBILINOGENO',
		'BILLIRRUBINURIA'
	);
	$nombresTabulados=$nombresValores;

	for($i=0;$i<count($nombresTabulados);$i++){

		if($nombresTabulados[$i]=='HORA'){//Excepción para casos de V.S.G., que al tener caracteres especiales hace cosas raras
			$nombresTabulados[$i]=str_pad($nombresTabulados[$i],30,' ');
		}
		elseif($nombresTabulados[$i]=='GOT (AST)' || $nombresTabulados[$i]=='GPT (ALT)'){
			$nombresTabulados[$i]=str_pad($nombresTabulados[$i],39,' ');
		}
		else{
			$nombresTabulados[$i]=str_pad($nombresTabulados[$i],40,' ');
		}

	}

	$res=str_replace($nombresValores,$nombresTabulados,$contenido);
	$res=str_replace('HEMOGLOBINA                              CORPUSCULAR MEDIA','HEMOGLOBINA CORPUSCULAR MEDIA',$res);//Porque como 'HEMOGLOBINA' aparece sola, le mete espacio a ese capo
	$res=str_replace('[','\t[',$res);

	//Saltos para apartados
	$encabezados=array(
		'SERIE BLANCA',
		'SERIE ROJA',
		'SERIE PLAQUETARIA',
		'ERITROSEDIMENTACIÓN',
		'SEDIMENTO URINARIO'
	);
	$encabezadosConSalto=$encabezados;

	for($i=0;$i<count($encabezadosConSalto);$i++){
		$encabezadosConSalto[$i]='\n'.$encabezadosConSalto[$i];
	}
	
	$res=str_replace($encabezados,$encabezadosConSalto,$res);

	return $res;
}


function obtieneCodigoReconocimiento($nombreFichero){
	$res='NULL';
	
	$datos=consultaBD("SELECT reconocimientos_medicos.codigo
		  			   FROM reconocimientos_medicos INNER JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
					   WHERE ficheroAnalitica='NO' AND reconocimientos_medicos.eliminado='NO' AND '$nombreFichero' LIKE CONCAT('%',REPLACE(REPLACE(dni,' ',''),'-',''),'%');",false,true);
	
	if($datos){
		$res=$datos['codigo'];
	}

	return $res;
}

function divideContenido($contenido){
	$res=array('hemograma'=>'','bioquimica'=>'','orina'=>'');

	//Hemograma
	$inicio=strpos($contenido,'SERIE BLANCA');
	$fin=strpos($contenido,'GLUCOSA BASAL')-$inicio;

	$res['hemograma']=substr($contenido,$inicio,$fin);

	//Bioquímica
	$inicio=strpos($contenido,'GLUCOSA BASAL');
	$fin=strpos($contenido,'PERFIL')-$inicio;

	$res['bioquimica']=substr($contenido,$inicio,$fin);

	//Orina
	$inicio=strpos($contenido,'PERFIL');

	$res['orina']=substr($contenido,$inicio);

	return $res;
}