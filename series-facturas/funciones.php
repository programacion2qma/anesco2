<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de series de facturas

function operacionesSeriesFacturas(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('series_facturas');
	}
	elseif(isset($_POST['serie'])){
		$res=insertaDatos('series_facturas');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('series_facturas');
	}

	mensajeResultado('serie',$res,'Serie');
    mensajeResultado('elimina',$res,'Serie', true);
}


function imprimeSeries(){
	$consulta=consultaBD("SELECT series_facturas.codigo, serie, observaciones, series_facturas.activo, razonSocial AS emisor 
						  FROM series_facturas LEFT JOIN emisores ON series_facturas.codigoEmisorSerie=emisores.codigo",true);

	while($datos=mysql_fetch_assoc($consulta)){
		echo "<tr>
				<td>".$datos['serie']."</td>
				<td>".$datos['observaciones']."</td>
				<td>".$datos['emisor']."</td>
				<td class='centro'><a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-edit'></i> Editar</a>
        		<td><input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' /></td>
        	</tr>";
	}
}


function gestionSerieFactura(){
	operacionesSeriesFacturas();

	abreVentanaGestionConBotones('Gestión de series de facturas','index.php','span3','icon-edit');
	$datos=compruebaDatos('series_facturas');

	campoTexto('serie','Serie (2 caracteres)',$datos,'input-mini');
	areaTexto('observaciones','Observaciones',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();
	
	campoSelectConsulta('codigoEmisorSerie','Emisor asociado',"SELECT codigo, razonSocial AS texto FROM emisores ORDER BY razonSocial",$datos);
	campoRadio('activo','Activo',$datos,'SI');

	cierraVentanaGestion('index.php',true);
}

//Fin parte de series de facturas