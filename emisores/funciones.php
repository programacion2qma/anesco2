<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de emisores

function operacionesEmisores(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('emisores',time(),'../documentos/emisores/');
	}
	elseif(isset($_POST['razonSocial'])){
		$res=insertaDatos('emisores',time(),'../documentos/emisores/');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('emisores');
	}

	mensajeResultado('razonSocial',$res,'Emisor');
    mensajeResultado('elimina',$res,'Emisor', true);
}


function imprimeEmisores(){
	$consulta=consultaBD("SELECT * FROM emisores",true);

	while($datos=mysql_fetch_assoc($consulta)){
		echo "<tr>
				<td>".$datos['codigo']."</td>
				<td>".$datos['razonSocial']."</td>
				<td>".$datos['cif']."</td>
				<td>".$datos['iban']."</td>
				<td>".$datos['prefijo']."</td>
        		<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
	        </tr>";
	}
}


function gestionEmisor(){
	operacionesEmisores();

	abreVentanaGestionConBotones('Gestión de Emisores','index.php','span3','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('emisores');

	campoTexto('razonSocial','Razón social',$datos);
	campoTexto('cif','CIF',$datos,'input-small validaCIF');
	campoTexto('domicilio','Domicilio',$datos,'span3');
	campoTexto('cp','Código postal',$datos,'input-mini');
	campoTexto('localidad','Localidad',$datos);
	campoTexto('provincia','Provincia',$datos);
	campoTexto('iban','IBAN',$datos,'input-large');
	campoTexto('prefijo','Prefijo',$datos,'input-mini');
	campoTexto('referenciaAcreedor','Código acreedor',$datos,'input-medium');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoLogo('ficheroLogo','Logo (máx. 100x100px)',0,$datos,'../documentos/emisores/','Ver');
	campoTextoSimbolo('telefono','Teléfono','<i class="icon-phone"></i>',$datos,'input-small');
	campoTextoSimbolo('fax','Fax','<i class="icon-fax"></i>',$datos,'input-small');
	campoTextoSimbolo('web','Web','<i class="icon-globe"></i>',$datos,'input-large');
	campoTextoSimbolo('email','eMail','<i class="icon-envelope"></i>',$datos,'input-large');
	areaTexto('registro','Datos registro',$datos);
	campoRadio('activo','Activo',$datos,'SI');

	cierraVentanaGestion('index.php',true);
}

//Fin parte de emisores