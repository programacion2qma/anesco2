<?php
  $seccionActiva=8;
  include_once('../cabecera.php');
  
  operacionesUsuarios();
  $estadisticas=estadisticasGenericas('usuarios');
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas de accesos al sistema durante el día de hoy:</h6>

                  <canvas id="area-chart" class="chart-holder" width="538" height="250"></canvas>

                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Usuarios</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nuevo usuario</span> </a>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Usuarios registrados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Nombre </th>
                    <th> Teléfono </th>
                    <th> eMail </th>
                    <th> Usuario </th>
                    <th> Tipo perfil </th>
                    <th class="centro"></th>
                    <th><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

          				<?php
          					imprimeUsuarios();
          				?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
		  


          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-sign-in"></i>
              <h3>Accesos al sistema</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th> Usuario </th>
                    <th> Fecha </th>
                    <th> Hora </th>
                    <th> Dirección IP </th>
                    <th> Navegador </th>
                    <th> Sistema Operativo </th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimeAccesos();

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>



      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.dataTables.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filtroTabla.js"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/chart.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>

<script type="text/javascript">

  <?php $datos=generaDatosGraficoAccesos(); ?>
  var datosGrafico = {
            labels: <?php echo $datos['etiquetas']; ?>,
            datasets: [
        {
            fillColor: "rgba(220,220,220,0.5)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            data: <?php echo $datos['accesos']; ?>
        }
      ]

  }

  var opciones={
    scaleOverride : true,
    scaleSteps : <?php echo $datos['max']; ?>,
    scaleStepWidth : 1, //Si el número de incidencias es menor que 10, la escala del gráfico va de 1 en 1, sino de 5 en 5.
    scaleStartValue : 0,
    barValueSpacing:60,
    barDatasetSpacing:20
  }

  var grafico = new Chart(document.getElementById("area-chart").getContext("2d")).Line(datosGrafico,opciones);
</script>

<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>