<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de usuarios

function operacionesUsuarios(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('usuarios',time(),'../documentos/usuarios');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('usuarios',time(),'../documentos/usuarios');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('usuarios');
	}

	mensajeResultado('nombre',$res,'Usuario');
    mensajeResultado('elimina',$res,'Usuario', true);
}


function imprimeUsuarios(){
	global $_CONFIG;
	$nombresTipo = array(''=>'','ADMIN'=>'Administrador','TECNICO'=>'Técnico', 'COMERCIAL'=>'Comercial','COLABORADOR'=>'Colaborador','FACTURACION'=>'Facturación','MEDICO'=>'Médico de empresa','ENFERMERIA'=>'Enfermería','ADMINISTRACION'=>'Administrativo');

	$consulta=consultaBD("SELECT codigo, nombre, apellidos, telefono, email, usuario, tipo FROM usuarios WHERE usuario!='soporte' AND clave!='Soporte15' AND tipo <> 'CLIENTE' AND tipo <> 'EMPLEADO';",true);
	while($datos=mysql_fetch_assoc($consulta)){
		echo "<tr>
				<td> ".$datos['apellidos'].", ".$datos['nombre']." </td>
				<td> ".formateaTelefono($datos['telefono'])." </td>
				<td> <a href='mailto:".$datos['email']."' class='noAjax'>".$datos['email']."</a></td>
				<td> ".$datos['usuario']."</td>
				<td> ".$nombresTipo[$datos['tipo']]."</td>
				<td class='centro'> <a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a> </td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	  	</td>
			 </tr>";
	}
}

function gestionUsuario(){
	operacionesUsuarios();

	abreVentanaGestionConBotones('Gestión de Usuarios','index.php','span3','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('usuarios');
	
	campoOculto($datos,'firmaCorreo','NULL');
	campoOculto($datos,'directorAsociado','NULL');
	campoOculto($datos,'teleconcertador','NULL');

	campoTexto('nombre','Nombre',$datos);
	campoTexto('apellidos','Apellidos',$datos);
	campoTexto('dni','DNI',$datos,'input-small nif');
	campoTextoSimbolo('telefono','Teléfono','<i class="icon-phone"></i>',$datos,'input-small pagination-right');
	campoTextoSimbolo('email','eMail','<i class="icon-envelope"></i>',$datos,'input-large');
	campoTexto('colorTareas','Color para tareas',$datos,'input-mini');
	campoTexto('usuario','Usuario',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	campoClave('clave','Contraseña',$datos);
	$nombresTipo = array('Administrador','Administrativo','Técnico', 'Comercial','Colaborador','Facturación','Médico de empresa','Enfermería');
	$valoresTipo = array('ADMIN','ADMINISTRACION','TECNICO','COMERCIAL','COLABORADOR','FACTURACION','MEDICO','ENFERMERIA');
	campoSelect('tipo','Perfil',$nombresTipo,$valoresTipo,$datos);
	echo '<div id="divAdmin" class="hide">';
	if(esSuperadmin()){
		campoRadio('superadmin','Administrador total',$datos);
	} else {
		campoOculto($datos,'superadmin','NO');
	}
	echo '</div>';
	campoRadio('director','Director',$datos,'NO',array('Si','No'),array('SI','NO'));
	campoRadio('habilitado','Habilitado',$datos,'NO',array('Si','No'),array('SI','NO'));

	echo "<div class='hide' id='cajaMedico'>";
			campoTexto('tipoEspecialista','Tipo de especialista',$datos);
			campoTexto('numeroColegiado','Número colegiado',$datos,'input-mini pagination-right');
			campoFichero('ficheroFirma','Firma (con sello)',0,$datos,'../documentos/usuarios/','Descargar',false);
	echo "</div>";

	campoOculto($datos,'sesion');
	cierraVentanaGestion('index.php',true);
}

//Fin parte de usuarios