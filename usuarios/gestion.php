<?php
  $seccionActiva=8;
  include_once("../cabecera.php");
  gestionUsuario();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-colorpicker.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
	$('.selectpicker').selectpicker();
	$('#colorTareas').colorpicker();
	
	$(".nif").focusout(function(){
		isValidNif($(this).val());
	});

	$('.form-horizontal button[type=submit]').unbind();
 	$('.form-horizontal button[type=submit]').click(function(e){
 		e.preventDefault();
 		if($('#usuario').val()=='' || $('#clave').val()==''){
 			alert('Por favor, introduzca unas credenciales para el usuario.');
 		} 
 		else {
 			var usuario=$('#usuario').val();
 			var codigoUsuario=$('#codigo').val();

			if(codigoUsuario==undefined){
				codigoUsuario=0;
			}

	    	var consulta=$.post('../listadoAjax.php?funcion=compruebaUsuario();',{'codigoUsuario':codigoUsuario,'usuario':usuario});
			consulta.done(function(respuesta){
				if(respuesta=='error'){
					alert('Nombre de usuario repetido. Por favor, elija otro nombre.');
					$('#usuario').focus();
				} 
				else {	
					$('.form-horizontal').submit();
				}
			});
		}
	});


	oyenteTipoPerfil();
	oyenteSuperAdmin();
	$('#tipo').change(function(){
		oyenteTipoPerfil();
		oyenteSuperAdmin();
	});

});

function oyenteTipoPerfil(){
	if($('#tipo').val()=='MEDICO' || $('#tipo').val()=='ADMIN'){
		$('#cajaMedico').removeClass('hide');
	}
	else{
		$('#cajaMedico').addClass('hide');
	}
}

function oyenteSuperAdmin(){
	if($('#tipo option:selected').val()=='ADMIN'){
		$('#divAdmin').removeClass('hide');
	} else {
		$('#divAdmin').addClass('hide');
	}	
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
