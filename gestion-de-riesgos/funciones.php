<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de evaluación de riesgos

function creaEstadisticasGestionRiesgos(){

		$res=estadisticasGenericas('gestion_evaluacion_general',false,'codigoEvaluacion IS NOT NULL');

	return $res;
}


function operacionesGestionRiesgos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaGestionRiesgo();
	}
	elseif(isset($_POST['codigoEvaluacion'])){
		$res=creaGestionRiesgo();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatosConFichero('gestion_evaluacion_general','documentacion','../documentos/riesgos/');
	}

	mensajeResultado('codigoEvaluacion',$res,'Gestión de riesgo');
    mensajeResultado('elimina',$res,'Gestión de riesgo', true);
}



function gestionRiesgo(){
	operacionesGestionRiesgos();
	
	$enlaceFormulario='generaGraficoGestion.php';
	$enlaceVolver='index.php';

	abreVentanaGestion('Gestión de Riesgos',$enlaceFormulario,'','icon-edit','',true,'noAjax');

	$datos=compruebaDatos('gestion_evaluacion_general');

	campoSelectConsulta('codigoEvaluacion','Evaluación de riesgo',"SELECT evaluacion_general.codigo, DATE_FORMAT(fechaEvaluacion,'%d/%m/%Y') AS texto FROM evaluacion_general",$datos,'selectpicker span6 show-tick');

	echo "<h3 class='apartadoFormulario'>Riesgos evaluados</h3>
			<div id='riesgosEvaluados'>
			
		  </div><br clear='all'><br/>
			";
	campoFecha('fecha','Fecha de gestión',$datos);
	
	$codigoGestion = $datos ? $datos['codigo'] : 0;
	
	$medidas = consultaBD("SELECT m.codigoRiesgo as codigoRiesgo FROM medidas_riesgos_evaluacion_general m WHERE codigoGestion=".$codigoGestion." LIMIT 1",true,true);
	//campoSelectConsulta('codigoRiesgo','Selecciona Riesgo',"SELECT codigo, nombre AS texto FROM riesgos ORDER BY nombre",$medidas['codigoRiesgo']);
	$riesgos = explode('&$&', $medidas['codigoRiesgo']);
	$nombres = '';
	$i=0;
	while(isset($riesgos[$i])){
		$riesgo = datosRegistro('riesgos',$riesgos[$i]);
		if($nombres == ''){
			$nombres = $riesgo['nombre'];
		} else {
			$nombres .=', '.$riesgo['nombre'];
		}
		$i++;
	}

	campoOculto($medidas['codigoRiesgo'],'codigoRiesgo');
	//campoTexto('nombreRiesgo','Riesgo seleccionado',$medidas['nombre'],'span6',true);
	areaTexto('nombreRiesgo','Riesgos seleccionados',$nombres,'areaRiesgos span6',true);

	creaTablaRecomendacionesRiesgo($datos);

	echo "<h3 class='apartadoFormulario'>Documentación</h3>";

	campoFichero('documento','Subir documentación','',$datos['documentacion'],'../documentos/riesgos/','Descarga fichero');

	cierraVentanaGestion($enlaceVolver,true);
}

function imprimeGestionesRiesgo($codigoCliente=false){
	global $_CONFIG;
	$where="";
	$enlaceGestion="";

	$consulta=consultaBD("SELECT gestion_evaluacion_general.codigo, gestion_evaluacion_general.codigo AS codigoGestion, fecha, fechaEvaluacion FROM evaluacion_general INNER JOIN gestion_evaluacion_general ON evaluacion_general.codigo=gestion_evaluacion_general.codigoEvaluacion;",true);
	while($datos=mysql_fetch_assoc($consulta)){
		$codigoRiesgo = consultaBD("SELECT codigoRiesgo FROM medidas_riesgos_evaluacion_general WHERE codigoGestion=".$datos['codigoGestion']." LIMIT 1",true,true);

		$riesgos = explode('&$&', $codigoRiesgo['codigoRiesgo']);
		$nombres = '';
		$i=0;
		while(isset($riesgos[$i])){
			$riesgo = datosRegistro('riesgos',$riesgos[$i]);
			if($nombres == ''){
				$nombres = $riesgo['nombre'];
			} else {
				$nombres .=', '.$riesgo['nombre'];
			}
			$i++;
		}
		echo "
			<tr>
				<td>".$nombres."</td>
				<td>".formateaFechaWeb($datos['fechaEvaluacion'])."</td>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td class='centro'>
					<a href='".$_CONFIG['raiz']."gestion-de-riesgos/gestion.php?codigo=".$datos['codigo'].$enlaceGestion."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
}


function creaTablaRecomendacionesRiesgo($datos=false){
	echo "
	<br />
	<h3 class='apartadoFormulario'>Medidas a implantar</h3>
	<table class='table table-striped table-bordered' id='tablaMedidas'>
      <thead>
        <tr>
          <th> Responsable</th>
          <th> Recomendación </th>
          <th> Plazo </th>
          <th> Ejecutada </th>
          <th> </th>
        </tr>
      </thead>
      <tbody>";
  	
  		$i=0;
  		$j=1;

  		conexionBD();
  		if($datos!=false){
  			$consulta=consultaBD("SELECT * FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='".$datos['codigo']."'",true);
  			while($datosG=mysql_fetch_assoc($consulta)){
				imprimeCamposTablaRecomendacionesRiesgo($i,$j,$datosG);
				$i++;
				$j++;
  			}
  		}
  		
  		if($i==0){
  			imprimeCamposTablaRecomendacionesRiesgo(0,1,false);
  		}
  		cierraBD();

    echo "
      </tbody>
    </table>
    <center>
		<button type='button' class='btn btn-small btn-success' onclick='insertaFila2(\"tablaMedidas\");'><i class='icon-plus'></i> Añadir medida</button> 
		<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila2(\"tablaMedidas\");'><i class='icon-trash'></i> Eliminar medida</button>
	</center><br />";
}

function imprimeCamposTablaRecomendacionesRiesgo($i,$j,$datos){
	$textos='';
	$valores='';
	$z=0;
	/*$consulta=consultaBD("SELECT codigo, funcion FROM puestosTrabajo ORDER BY funcion",true);
	while($datosPuestos=mysql_fetch_assoc($consulta)){
		$textos[$z] = $datosPuestos['funcion'];
		$valores[$z] = $datosPuestos['codigo'];
		$z++;
 	}
 	$z=0;
 	$consulta=consultaBD("SELECT codigo, CONCAT(nombre,' ',apellidos) AS nombrecompleto FROM personal ORDER BY nombre",true);
	while($datosPersona=mysql_fetch_assoc($consulta)){
		$textos1[$z] = $datosPersona['nombrecompleto'];
		$valores1[$z] = $datosPersona['codigo'];
		$z++;
 	}*/
	echo "<tr class='trMedida'>
			<td>
				<table class='centro sinBorde'>
					<tr>";
						campoTexto('area'.$i,'Areas',$datos['area']);
	echo "			</tr>
					<tr>";
						campoTexto('responsable'.$i,'Empleados',$datos['responsable']);
	echo "			</tr>
				</table>
			</td>";
	areaTextoTabla('recomendacion'.$i,$datos['recomendacion'],'areaTextoTablaMedidas');
	echo "<td>
				<table class='centro sinBorde'>
					<tr>";
						campoSelect('plazo'.$i,'',array('6 Meses','12 meses','18 meses','Otros'),array(6,12,18,1),$datos['plazo'],'selectpicker span2 show-tick selectPlazo','',1);
	echo "			</tr>
					<tr>";
	$class = $datos && $datos['plazo'] == 1 ? 'input-small textOtroPlazo':'input-small hidden';
						campoTextoTabla('otroPlazo'.$i,$datos['otroPlazo'],$class);
	echo "			</tr>
				</table>
			</td>";
	campoCheckTabla('checkEjecutada'.$i,$datos['checkEjecutada']);
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		 </td>
	</tr>";
}

function creaGestionRiesgo(){
	$res=insertaDatos('gestion_evaluacion_general');
	if($res){
		$res=insertaMedidasGestionRiesgo($res);
	}

	return $res;
}

function actualizaGestionRiesgo(){
	$res=actualizaDatos('gestion_evaluacion_general');
	if($res){
		$res=insertaMedidasGestionRiesgo($_POST['codigo'],true);
	}

	return $res;
}


function insertaMedidasGestionRiesgo($codigoGestion,$actualizacion=false){
	$res=true;
	$datos=arrayFormulario();

	if($actualizacion){
		$res=consultaBD("DELETE FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='$codigoGestion';");
	}

	for($i=0;isset($datos['area'.$i]);$i++){
		if(isset($datos['checkEjecutada'.$i])){
			$ejecutada = 'SI';
		} else {
			$ejecutada = 'NO';
		}
		$area = isset($datos['area'.$i]) ? $datos['area'.$i] : '';
		$responsable = isset($datos['responsable'.$i]) ? $datos['responsable'.$i] : '';
		$res=$res && consultaBD("INSERT INTO medidas_riesgos_evaluacion_general VALUES(NULL, '".$area."', '".$responsable."', '".$datos['recomendacion'.$i]."','".$datos['plazo'.$i]."','".$datos['otroPlazo'.$i]."','".$ejecutada."','$codigoGestion','".$datos['codigoRiesgo']."');");
	}

	return $res;
}


function creaEstadisticasGestionCliente($codigoCliente){
	return consultaBD("SELECT COUNT(gestion_evaluacion_general.codigo) AS total FROM gestion_evaluacion_general INNER JOIN evaluacion_general ON gestion_evaluacion_general.codigoEvaluacion=evaluacion_general.codigo WHERE codigoCliente='$codigoCliente';",true,true);
}


function camposFormularioGrafico(){
    foreach($_POST as $nombre=>$valor){
        if(!is_array($valor) && substr_count($nombre,'fecha')!=1){
            $valor=addslashes($valor);
        }
        if(is_array($valor)){
        	$select = $valor;
        	$valor = '';
			if(!empty($select)){
				for($j=0;$j<count($select);$j++){
					if($j > 0){
						$valor .= "&$&";
					}
					$valor .= $select[$j];
				}
			}
        }
        campoOculto($valor,$nombre);
    }
    
    campoOculto('','graficoCircular');
    campoOculto('','graficoBarra');
}

function generaDatosGraficoCircular($datos){
	$consulta = consultaBD("SELECT codigo FROM puestosTrabajo ORDER BY codigo",true);
	while($datosPuestos=mysql_fetch_assoc($consulta)){
		$res[$datosPuestos['codigo']]=0;
	} 
	$k=0;
	$responsables=array();
	for($i=0;isset($datos['area'.$i]);$i++){
		$select = $datos['area'.$i];
		if(!empty($select)){
			for($j=0;$j<count($select);$j++){         
				$res[$select[$j]]++;
			}
		}
	}

	return $res;
}

function generaDatosGraficoBarras($datos){
	$res=array('6'=>0,'12'=>0,'18'=>0,'1'=>0);

	for($i=0;isset($datos['plazo'.$i]);$i++){
		$res[$datos['plazo'.$i]]++;//El valor devuelto para el plazo (6,12 ó 18) sirve como índice para el array contador resultante.
	}

	$res['max']=max($res);

	if($res['max']<10){
		$res['escala']=1;
	}
	else{
		$res['escala']=5;
	}

	return $res;
}

function obtieneRiesgosEvaluados(){
	$datos=arrayFormulario();
	$riesgos;
	$i = 0;
	$consulta=consultaBD("SELECT re.codigoEvaluacion AS codigoEvaluacion, r.codigo AS codigoRiesgo, r.nombre AS nombre, re.prioridad FROM riesgos_evaluacion_general re INNER JOIN riesgos r ON re.codigoRiesgo=r.codigo  WHERE re.prioridad <> '' AND codigoEvaluacion=".$datos['codigo'],true);// 9 PARA PROBAR
	while($datos=mysql_fetch_assoc($consulta)){
		$riesgos[$i] = $datos;
		$i++;
		$gestiones = consultaBD("SELECT COUNT(codigo) AS codigo FROM medidas_riesgos_evaluacion_general WHERE codigoRiesgo LIKE '%".$datos['codigoRiesgo']."%' AND codigoGestion IN (SELECT codigo FROM gestion_evaluacion_general WHERE codigoEvaluacion=".$datos['codigoEvaluacion'].") ",true,true);
		$riesgos[$i] = $gestiones;
		$i++;
	}
	echo json_encode($riesgos);
}

function generaGraficoAlmacen($datosGraficoCircular){
$colores=array('#B02B2C','#ff8c00','#6BBA70','#428bca','#B59464','#8904B1','#B6BBB5','#E75E9E','#F3D700','#6CD09E','#4C43CA','#B66B64','#3F3F3F','#C3D9FF','#D01F3C','#CDEB8B');
$i=0;
$res='';
$consulta = consultaBD("SELECT codigo,funcion FROM puestosTrabajo ORDER BY funcion",true);
while($datos=mysql_fetch_assoc($consulta)){
       $res.="{value: ".$datosGraficoCircular[$datos['codigo']].", color:'".$colores[$i%16]."', label: '".$datos['funcion']."'},";
       $i++;
}
$res=substr_replace($res,'',strlen($res)-1,strlen($res));//Para quitar última coma
echo $res;
}

//Fin parte de evaluación de riesgos