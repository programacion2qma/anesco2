<?php
  $seccionActiva=2;
  include_once('../cabecera.php');
  if(isset($_FILES['documento'])){
    $_POST['documentacion'] = subeDocumento('documento','','../documentos/riesgos');
  }
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span12">
          <div class="widget widget-nopad sinMargenAb">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Gráfico de gestión</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Clasificación de gestiones según el área al que afecta y niveles de prioridad:</h6>
                   <div id="big_stats" class="cf">

                    <canvas id="graficoCircularCaja" class="chart-holder" width="500" height="250"></canvas>
                    <div id="leyendaCircular" class="leyenda3"></div>
                    
                    <canvas id="graficoBarraCaja" class="chart-holder" width="500" height="250"></canvas>
                    <div id="leyendaBarra" class="leyenda3 derecha"></div>
                </div>
                    <div id="formcontrols" class='tab-pane'>
                    <form action="index.php" class='form-horizontal' method="post">
                      <?php
                        camposFormularioGrafico();

                        $datos=arrayFormulario();
                        //$datosGraficoCircular=generaDatosGraficoCircular($datos);
                        $datosGraficoBarras=generaDatosGraficoBarras($datos);
                      ?>
                      
                      <br /><br />  <br /><br />  <br />  <br />                      
                      <div class='form-actions'>
                        <a href='index.php' class='btn btn-default'><i class='icon-remove'></i> Cancelar</a> 
                        <button type='submit' class='btn btn-propio'><i class='icon-check'></i> Guardar</button> 
                      </div>
                    </form>
                    </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/chart.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>js/Chart.HorizontalBar.js" type="text/javascript"></script>

<script type="text/javascript">
  //Gráfico circular
  var colores=["#ff2500","#c1d2eb","#5488cf","#f39140","#ffc900","#81b859"];
  /*var datosGrafico = [
       <?php generaGraficoAlmacen($datosGraficoCircular); ?>
     ];


  var opciones={
      legendTemplate : "<table class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><tr><td><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%> (<%=segments[i].value%>)<%}%></td></tr><%}%></table>",
      onAnimationComplete:function(){convierteImagen('graficoCircular')}
    }

  var grafico=new Chart(document.getElementById("graficoCircularCaja").getContext("2d")).Pie(datosGrafico,opciones);
  var leyenda=document.getElementById('leyendaCircular');
  leyenda.innerHTML=grafico.generateLegend();*/

  //Fin gráfico circular
  $datosGrafico = '';
  //Gráfico de barras
  var datosGrafico = {
      labels: ["Prioridad en la Ejecución"],
      datasets: [
        {
            data: [<?php echo $datosGraficoBarras['6']; ?>],
            fillColor: "#d81e00",
            strokeColor: "#d81e00",
            label: 'Alta'
        },
        {
            data: [<?php echo $datosGraficoBarras['12']; ?>],
            fillColor: "#ffdf78",
            strokeColor: "#ffdf78",
            label: 'Media'
        },
        {
            data: [<?php echo $datosGraficoBarras['18']; ?>],
            fillColor: "#b6d79f",
            strokeColor: "#b6d79f",
            label: 'Baja'
        },
        {
            data: [<?php echo $datosGraficoBarras['1']; ?>],
            fillColor: "#f39140",
            strokeColor: "#f39140",
            label: 'Otros plazos'
        }
      ]
    }

    var opciones={
      scaleOverride : true,
      scaleSteps : <?php echo $datosGraficoBarras['max']; ?>,
      scaleStepWidth : <?php echo $datosGraficoBarras['escala'] ?>,
      scaleStartValue : 0,
      barValueSpacing:40,
      barDatasetSpacing:20,
      legendTemplate : "<table class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i+=4){%><tr><td><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></td><td><span style=\"background-color:<%=datasets[i+1].fillColor%>\"></span><%if(datasets[i+1].label){%><%=datasets[i+1].label%><%}%></td><td><span style=\"background-color:<%=datasets[i+2].fillColor%>\"></span><%if(datasets[i+2].label){%><%=datasets[i+2].label%><%}%></td><td><span style=\"background-color:<%=datasets[i+3].fillColor%>\"></span><%if(datasets[i+3].label){%><%=datasets[i+3].label%><%}%></td></tr><%}%></table>",
      onAnimationComplete:function(){convierteImagen('graficoBarra')}
    }


    grafico = new Chart(document.getElementById("graficoBarraCaja").getContext("2d")).Bar(datosGrafico,opciones);
    leyenda=document.getElementById('leyendaBarra');
    leyenda.innerHTML=grafico.generateLegend();
    //Fin gráfico de barras


    function convierteImagen(id){
      var base64=document.getElementById(id+'Caja').toDataURL();//Convierte el gráfico a imagen en base64
      var base64=base64.replace(/^data:image\/(png|jpg);base64,/, "");//Para quitar tipo en cabecera fichero
      $('#'+id).val(base64);
    }
</script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>