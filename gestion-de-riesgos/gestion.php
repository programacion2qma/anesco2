<?php
  $seccionActiva=2;
  include_once("../cabecera.php");
  gestionRiesgo();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../../api/js/filasTabla.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$('.selectpicker').selectpicker();

    recogeRiesgos($('#codigoEvaluacion').val());
    $( "#codigoEvaluacion" ).change(function() {
        recogeRiesgos($(this).val());
    });

    $('.selectPlazo').change(function(){
        oyentePlazos($(this));
    });

});

function oyentePlazos(elem){
    var valor = elem.val();
    var fila = obtieneFilaCampo(elem);
    if(valor == 1){
        $("#otroPlazo"+fila).removeClass('hidden');
    } else {
        $("#otroPlazo"+fila).addClass('hidden');
    }
}

function insertaFila2(tabla){
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla).find("tbody .trMedida:last").clone();
    //Obtengo el atributo name para los inputs y selects
   $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
        var parts = this.name.match(/(\D+)(\d*)/);
        //Creo un nombre nuevo incrementando el número de fila (++parts[2])
        if(this.name.indexOf("[]")>0){
          return parts[1] + ++parts[2] + '[]';
        }else{
          return parts[1] + ++parts[2];
        }
        
    }).attr("id", function(){//Hago lo mismo con los IDs
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });


    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker)").attr("id", function(){//Para los botones de cálculo
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value",""); //MODIFICACION OFICINA 12/06/2015
    $tr.find('.bootstrap-select').remove();

    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody .trMedida:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $('#'+tabla).find(".selectpicker").selectpicker('refresh');
    }
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $('.selectPlazo').change(function(){
        oyentePlazos($(this));
    });
}

function eliminaFila2(tabla){
  if($('#'+tabla).find("tbody .trMedida").length>1){//Si la tabla tiene más de 1 fila...
    var filasSeleccionadas=0;
    $('input[name="filasTabla[]"]:checked').each(function() {
        var fila=$(this).val();
        $('#'+tabla+' .trMedida').eq(fila).remove();
        filasSeleccionadas++;
    });

    if(filasSeleccionadas>0){
        //Renumeración de los índices de la tabla
        var numFilas=$('#'+tabla+' .trMedida:not(:first)').length;
        for(i=0;i<numFilas;i++){
           $('#'+tabla+' .trMedida:not(:first)').eq(i).find("input:not([type=checkbox],.input-block-level),select,textarea").attr("name", function(){
                //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
                var parts = this.id.match(/(\D+)(\d*)$/);
                //Creo un nombre nuevo incrementando el número de fila (++parts[2])
                return parts[1] + i;
                //Hago lo mismo con los IDs
            }).attr("id", function(){
                var parts = this.id.match(/(\D+)(\d*)$/);
                return parts[1] + i;
            });

            $('#'+tabla+' .trMedida:not(:first)').eq(i).find("input[type=checkbox]").attr("value", function(){
                var j=i+1
                return j;
            });
        }
        //Fin renumeración
      }
  }
  else{
    alert('Para eliminar un registro, debe seleccionarlo utilizando el último check que tiene a su derecha.');
  }
}
    
    function recogeRiesgos(codigo){
        $.ajax({
            data: {"codigo":codigo},//Matriz $_POST: se recogera de la forma $_POST['codigo'] (y tendrá el valor 1)
            type: "POST",//Siempre asi
            dataType: "json",//Siempre asi
            url: "recogeRiesgos.php",//URL destino (que solo debe hacer echo del resultado)
        }).done(function(datos,textStatus,jqXHR){
            var i = 0;
            var table = document.getElementById("riesgosEvaluados");
            $('#riesgosEvaluados').empty()
            while(i<datos.length){
                riesgo = datos[i].codigoRiesgo;
                if(datos[i].prioridad == 'TOLERABLE'){
                    var clase = "riesgo-success";
                } else if(datos[i].prioridad == 'SIGNIFICATIVO'){
                    var clase = "riesgo-warning";
                } else if(datos[i].prioridad == 'INTOLERABLE'){
                    var clase = "riesgo-danger";
                }
                var texto = datos[i].nombre;
                
                i++;
                var gestion = "<i style='font-size:15px;' class='icon-check-circle'></i>";
                if (datos[i].codigo == 0){
                    gestion = "<i style='font-size:15px;' class='icon-times-circle'></i>";
                }
                
                var input = document.createElement("div");
                    input.type = "text";
                    input.className = clase;
                    input.style.height = "25px";
                    input.style.width = "250px";
                    input.style.border = "1px";
                    input.style.margin = "5px";
                    input.style.float = 'left';
                    input.style.color = "#FFF";
                    input.style.fontWeight = "bold";
                    input.style.padding = "5px";
                    input.style.cursor = "pointer";
                    input.setAttribute("codigoRiesgo", riesgo);
                    input.setAttribute("nombreRiesgo", texto);
                    input.innerHTML = texto + "  " + gestion;
                table.appendChild(input);
                i++;
                $(input).click(function() {
                    alert("Riesgo seleccionado");
                    codigos = $("#codigoRiesgo").val() == '' ? '' : $("#codigoRiesgo").val() + '&$&';
                    codigos = codigos+ $(this).attr("codigoRiesgo");
                    $("#codigoRiesgo").val(codigos);
                    nombres = $("#nombreRiesgo").val() == '' ? '' : $("#nombreRiesgo").val() + ', ';
                    nombres = nombres+ $(this).attr("nombreRiesgo");
                    $("#nombreRiesgo").val(nombres);
                });    
            }
            var leyenda = document.createElement("div");
                leyenda.className = "leyenda_riesgos";
                leyenda.style.marginTop = "25px";
                leyenda.style.marginBottom = "25px";
                leyenda.style.marginLeft = "8px";
                leyenda.style.float = 'left';
                leyenda.style.clear = 'both';
                leyenda.innerHTML = "<b>Niveles de riesgo:</b> <span class='evaluacion riesgo-success'>TTT</span>Tolerable <span class='evaluacion riesgo-warning'>TTT</span>Significativo <span class='evaluacion riesgo-danger'>TTT</span>Intolerable - <b>Gestión:</b> <i style='font-size:15px;' class='icon-check-circle'></i> Riesgo gestionado <i style='font-size:15px;' class='icon-times-circle'></i> Riesgo no gestionado";
            table.appendChild(leyenda);    
        })

    }
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>