<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de profesionales externos


function operacionesProfesionalesExternos(){
	$res=false;

	if(isset($_POST['codigo'])){
		$_POST['precio']=formateaNumeroWeb($_POST['precio'],true);
		$res=actualizaDatos('profesionales_externos');
	}
	elseif(isset($_POST['nombre'])){
		$_POST['precio']=formateaNumeroWeb($_POST['precio'],true);
		$res=insertaDatos('profesionales_externos');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('profesionales_externos');
	}

	mensajeResultado('nombre',$res,'profesional externo');
    mensajeResultado('elimina',$res,'profesional externo', true);
}



function gestionProfesionalExterno(){
	operacionesProfesionalesExternos();
	
	abreVentanaGestionConBotones('Gestión de profesional externo','index.php','span3');
	$datos=compruebaDatos('profesionales_externos');
	
	campoTexto('nombre','Nombre',$datos);
	campoTexto('apellidos','Apellidos',$datos);
	campoTexto('clinica','Clínica',$datos);
	campoTextoSimbolo('precio','Precio','€',formateaNumeroWeb($datos['precio']));

	cierraColumnaCampos();
	abreColumnaCampos();

	areaTexto('observaciones','Observaciones',$datos);
	
	cierraVentanaGestion('index.php',true);
}

function imprimeProfesionalesExternos(){

	$consulta=consultaBD("SELECT * FROM profesionales_externos;",true);
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
		<tr>
			<td>".$datos['nombre']." ".$datos['apellidos']."</td>
			<td>".$datos['clinica']."</td>
			<td>".formateaNumeroWeb($datos['precio'])." €</td>
			<td class='centro'>
				<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-edit'></i> Editar</i></a>
			</td>
			<td>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	</td>
        </tr>";
	}
}

//Fin parte de profesionales externos