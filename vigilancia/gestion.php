<?php
  $seccionActiva=3;
  include_once("../cabecera.php");
  gestionVigilancia();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src='../../api/js/firma/jquery.signaturepad.js'></script>
<script src='../../api/js/firma/assets/json2.min.js'></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

		inicializaFirma();
		recogeEmpleados($('#codigoCliente option:selected').val(),$('#codigoEmpleadoSelect').val());
		$('#codigoCliente').change(function(){
			var codigo=$(this).val();
			var select=$('#codigoEmpleadoSelect').val();
			recogeEmpleados(codigo,select);
		})

	});
	

function inicializaFirma(){
	$('.output').each(function(){
		var id=$(this).attr('id');
		var firma = $(this).val().trim(); 
		if(firma==''){//inicialización
			$('.'+id).signaturePad({drawOnly:true,lineTop:'500',penColour:'000000',validateFields:false});
		}
		else{
			$('.'+id).signaturePad({drawOnly:true,lineTop:'500',penColour:'000000',validateFields:false}).regenerate(firma.toLowerCase());
		}
	});
}

function recogeEmpleados(codigo,select){
		var parametros = {
				"codigo" : codigo,
				"select"  : select
        };
		$.ajax({
			 type: "POST",
			 url: "../listadoAjax.php?include=vigilancia&funcion=recogeEmpleados();",
			 data: parametros,
			  beforeSend: function () {
				  
			  },
			 success: function(response){
				   $("#codigoEmpleado").html(response);
				   $('#codigoEmpleado').selectpicker('refresh');
			 }
		});
}



</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>