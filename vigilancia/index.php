<?php
  $seccionActiva=3;
  include_once('../cabecera.php');
  
  $res=operacionesVigilancia();
  $where='WHERE clientes.codigo IN (SELECT clientes.codigo FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE activo="SI" AND contratos.enVigor="SI" AND clientes.eliminado="NO")';
  $texto='';
  $boton="<a href='".$_CONFIG['raiz']."vigilancia/index.php?baja' class='shortcut'><i class='shortcut-icon icon-arrow-down'></i><span class='shortcut-label'>De clientes de baja</span> </a>";
  if(isset($_GET['baja'])){
    $where='WHERE clientes.codigo NOT IN (SELECT clientes.codigo FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE activo="SI" AND contratos.enVigor="SI" AND clientes.eliminado="NO")';
    $texto=' de clientes de baja';
    $boton="<a href='".$_CONFIG['raiz']."vigilancia/index.php?' class='shortcut'><i class='shortcut-icon icon-arrow-left'></i><span class='shortcut-label'>Volver</span> </a>";
  }
  $estadisticas=estadisticasVigilancia($where);
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

         <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de gestión Vigilancia de seguridad:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-heartbeat"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Reconocimientos médicos registrados<?php echo $texto;?></div>
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Reconocimientos médicos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
              <?php if($_SESSION['tipoUsuario']=='ADMIN'){?>
				        <a href="<?php echo $_CONFIG['raiz']; ?>vigilancia/gestion.php" class="shortcut"><i class="shortcut-icon icon-heartbeat"></i><span class="shortcut-label">Nuevo reconocimiento</span> </a>
                <?php echo $boton;?>
                		<a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              <?php } ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Reconocimientos médicos registrados<?php echo $texto;?></h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table id='tabla1' class="table table-striped table-bordered datatable tablaNo">
              <thead>
                <tr>
                  <th> Nº Reconocimiento </th>
                  <th> Nombre y apellidos </th>
                  <th> Edad </th>
                  <th> Fecha </th>
                  <th class="centro"></th>
                  <?php if($_SESSION['tipoUsuario']=='ADMIN'){?>
                  <th><input type='checkbox' id="todoNo"></th>
                  <?php } ?>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeVigilancia($where);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
    </div>
        
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/chart.js" type="text/javascript"></script>
<script type="text/javascript">    

    
</script>


<?php include_once('../pie.php'); ?>