<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión Auditorias


function operacionesVigilancia(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('vigilancia','','../documentacion/fotos');
	}
	elseif(isset($_POST['codigoInterno'])){
		$res=insertaDatos('vigilancia','','../documentacion/fotos');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDocumentosAPI('vigilancia',array('ficheroAptitud'),'../documentacion/fotos/');
	}

	mensajeResultado('codigoInterno',$res,'Trabajador');
    mensajeResultado('elimina',$res,'Trabajador', true);
}


function gestionVigilancia(){
	operacionesVigilancia();
	
	abreVentanaGestionConBotones('Gestión de trabajadores','index.php','','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('vigilancia');
	creaPestaniasAuditoria();
		abrePestania(1,'active');
			abreColumnaCampos();
				$codigoInterno = $datos ? $datos['codigoInterno']:generaNumero();
				campoTexto('codigoInterno','Número de reconocimiento',$codigoInterno,'input-mini');
			cierraColumnaCampos();

			abreColumnaCampos();
				campoSelectConsulta('codigoCliente','Cliente','SELECT codigo,EMPNOMBRE as texto FROM clientes ORDER BY EMPNOMBRE',$datos);
				campoOculto($datos['codigoEmpleado'],'codigoEmpleadoSelect','NULL');
				campoSelectConsulta('codigoEmpleado','Empleado',array(),array());
				campoFecha('fecha','Fecha',$datos);
			cierraColumnaCampos();
		cierraPestania();
		
		abrePestania(2);
			campoSelect('aptitud','Aptitud',array('Si','No'),array('SI','NO'),$datos,'selectpicker span1 show-tick');
			campoFichero('ficheroAptitud','Documento',0,$datos,'../documentacion/fotos/');
		cierraPestania();

		abrePestania(3);
			abreColumnaCampos();
				campoTexto('exploracion','Exploración',$datos);
				campoTexto('frecuenciaRespiratoria','Frecuencia respiratoria',$datos,'input-mini');
				campoTexto('peso','Peso',$datos,'input-mini');
				campoTexto('imc','IMC',$datos,'input-mini');
				areaTexto('exploracionCabeza','Exploración de cabeza y cuello',$datos);
				areaTexto('alteracionesSensitivas','Alteraciones sensitivas',$datos);
				areaTexto('alteracionesEquilibrio','Alteraciones del equilibrio',$datos);
				areaTexto('alteracionesReflejos','Alteraciones de los reflejos tendinosos',$datos);
			cierraColumnaCampos();

			abreColumnaCampos();
				campoTexto('constitucion','Consitución',$datos);
				campoTexto('frecuenciaCardiaca','Frecuencia cardiaca',$datos,'input-mini');
				campoTexto('talla','Talla',$datos,'input-mini');
				campoTexto('PA','P-A MAX/MIN',$datos,'input-mini');
				areaTexto('alteracionesMotoras','Alteraciones motoras',$datos);
				areaTexto('alteracionesMarcha','Alteraciones de la marcha',$datos);
				areaTexto('dismetrias','Dismetrías',$datos);
				areaTexto('orientacionTemporespacial','Orientación Temporespacial',$datos);
			cierraColumnaCampos();
		cierraPestania();

		abrePestania(4);
			abreColumnaCampos();
				echo "<b><u>Análisis de sangre</u></b><br/>";
				campoTexto('hemograma','Hemograma completo',$datos);
				campoTexto('leucocitos','Leucocitos y fórmula leucocitaria',$datos);
				campoTexto('plaquetas','Plaquetas',$datos);
				campoTexto('velocidad','Velocidad de sedimentación',$datos);
				campoTexto('bioquimica','Bioquímica',$datos);
				campoTexto('perfiles','Perfiles',$datos);
			cierraColumnaCampos();

			abreColumnaCampos();
				echo "<b><u>Análisis de orina</u></b><br/>";
				campoTexto('densidad','Densidad',$datos);
				campoTexto('ph','Ph',$datos);
				campoTexto('anormales','Anormales',$datos);
				campoTexto('sedimento','Sedimento',$datos);
			cierraColumnaCampos();
		cierraPestania();

		abrePestania(5);
			abreColumnaCampos();
				echo "<b><u>Lejos</u></b><br/>";
				campoSelect('lejosDerecho','Derecho',array('Si','No'),array('SI','NO'),$datos,'selectpicker span1 show-tick');
				campoSelect('lejosIzquierdo','Izquierdo',array('Si','No'),array('SI','NO'),$datos,'selectpicker span1 show-tick');
				campoSelect('lejosBinocular','Binocular',array('Si','No'),array('SI','NO'),$datos,'selectpicker span1 show-tick');
				campoSelect('lejosCorreccion','Correción',array('Si','No'),array('SI','NO'),$datos,'selectpicker span1 show-tick');
				campoSelect('lejosColores','Colores',array('Si','No'),array('SI','NO'),$datos,'selectpicker span1 show-tick');
			cierraColumnaCampos();

			abreColumnaCampos();
				echo "<b><u>Cerca</u></b><br/>";
				campoSelect('cercaDerecho','Derecho',array('Si','No'),array('SI','NO'),$datos,'selectpicker span1 show-tick');
				campoSelect('cercaIzquierdo','Izquierdo',array('Si','No'),array('SI','NO'),$datos,'selectpicker span1 show-tick');
				campoSelect('cercaBinocular','Binocular',array('Si','No'),array('SI','NO'),$datos,'selectpicker span1 show-tick');
				campoSelect('cercaCorreccion','Correción',array('Si','No'),array('SI','NO'),$datos,'selectpicker span1 show-tick');
				campoSelect('cercaColores','Colores',array('Si','No'),array('SI','NO'),$datos,'selectpicker span1 show-tick');
			cierraColumnaCampos();
		cierraPestania();

		abrePestania(6);
			abreColumnaCampos();
				echo "<b><u>Patrón</u></b><br/>";
				campoTexto('cvf','CVF',$datos);
				campoTexto('fev1','FEV1',$datos);
				campoTexto('tiffenau','Tiffenau',$datos);
				campoTexto('restrictivo','Restrictivo',$datos);
				campoTexto('obstructivo','Obstructivo',$datos);
				campoTexto('mixto','Mixto',$datos);
				campoTexto('normal','Normal',$datos);
			cierraColumnaCampos();
		cierraPestania();

		abrePestania(7);
			abreColumnaCampos();
				areaTexto('holterIA','Holter I.A.',$datos);
				areaTexto('holterECG','Holter E.C.G.',$datos);
				areaTexto('ECGEsfuerzo','E.C.G. de esfuerzo',$datos);
			cierraColumnaCampos();
		cierraPestania();

		abrePestania(8);
			abreColumnaCampos();
				campoSelect('revision','Tipo de reconocimiento periodicidad grado de aptitud relacion con el riesgo',array('Apto','No apto'),array('SI','NO'),$datos,'selectpicker span2 show-tick');
				campoFecha('fechaRevision','Fecha',$datos);
				campoTexto('nombreMedico','Nombre del médico',$datos);
			cierraColumnaCampos();
			echo "<br clear='all'>";
			campoFirma($datos,'firmaDoctor','Firma');
		cierraPestania();
	
		cierraVentanaGestion('index.php', true);

}

function creaPestaniasAuditoria(){
	echo "<div class='tabbable tabs-lopd'>
	       	<ul class='nav nav-tabs tabs-small'>
		        <li class='active'><a href='#1' class='noAjax' data-toggle='tab'>Datos Generales</a></li>
		        <li><a href='#2' class='noAjax' data-toggle='tab'>Aptitud</a></li>
		        <li><a href='#3' class='noAjax' data-toggle='tab'>Exploración</a></li>
		        <li><a href='#4' class='noAjax' data-toggle='tab'>Analítica</a></li>
		        <li><a href='#5' class='noAjax' data-toggle='tab'>Visión</a></li>
		        <li><a href='#6' class='noAjax' data-toggle='tab'>Espirometría</a></li>
		        <li><a href='#7' class='noAjax' data-toggle='tab'>Electro EGC</a></li>
		        <li><a href='#8' class='noAjax' data-toggle='tab'>Firma Doctor</a></li>
	       	</ul>

       <div class='tab-content'>";
}


//Parte de campos personalizados


//Fin parte de campos personalizados

function imprimeVigilancia($where){
	global $_CONFIG;

	conexionBD();
	if($_SESSION['tipoUsuario']=='CLIENTE'){
		$cliente=obtenerCodigoCliente(false);
		$where=' AND vigilancia.codigoCliente='.$cliente;
	} else if($_SESSION['tipoUsuario']=='EMPLEADO') {
		$empleado=obtenerCodigoEmpleado(false);
		$where=' AND vigilancia.codigoEmpleado='.$empleado;
	}
	$where=defineWhereEjercicio($where,'vigilancia.fecha');
	$consulta=consultaBD("SELECT vigilancia.codigo,vigilancia.codigoInterno, empleados.nombre, empleados.apellidos, empleados.edad, vigilancia.fecha, vigilancia.codigoCliente, vigilancia.codigoEmpleado,baja FROM vigilancia INNER JOIN empleados ON vigilancia.codigoEmpleado=empleados.codigo INNER JOIN clientes ON vigilancia.codigoCliente=clientes.codigo ".$where." ORDER BY vigilancia.codigoInterno ASC");
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td>".$datos['codigoInterno']."</td>
				<td>".$datos['nombre']." ".$datos['apellidos']."</td>
				<td>".$datos['edad']."</td>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td class='centro'>
					".crearBotones($datos['codigo'])."
				</td>";
				if($_SESSION['tipoUsuario']=='ADMIN'){
				echo "<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>";
	        	}
		echo"	</tr>";
	}
	cierraBD();
}

function crearBotones($codigo){
	global $_CONFIG;
	$res='';
	if($_SESSION['tipoUsuario']=='ADMIN'){
		$res="<div class='btn-group centro'>
				<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
				<ul class='dropdown-menu' role='menu'>
					<li><a href='".$_CONFIG['raiz']."vigilancia/gestion.php?codigo=".$codigo."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
					<li class='divider'></li>
					<li><a onClick='alert(\"Funcion aún no desarrollada\")' class='noAjax' target='_blank'><i class='icon-cloud-download'></i> Carta de aptitud</i></a></li>
					<li class='divider'></li>
					<li><a onClick='alert(\"Funcion aún no desarrollada\")' class='noAjax' target='_blank'><i class='icon-cloud-download'></i> Informe médico</i></a></li>
				</ul>
			</div>";
	}
	else if($_SESSION['tipoUsuario']=='CLIENTE') {
		$res="<a onClick='alert(\"Funcion aún no desarrollada\")' class='btn btn-propio noAjax' target='_blank'><i class='icon-cloud-download'></i> Carta de aptitud</i></a>";
	} else if($_SESSION['tipoUsuario']=='EMPLEADO') {
		$res="<a onClick='alert(\"Funcion aún no desarrollada\")' class='btn btn-propio noAjax' target='_blank'><i class='icon-cloud-download'></i> Informe médico</i></a>";
	}	
	return $res;
}

function generaNumero(){
	$consulta=consultaBD('SELECT MAX(codigoInterno) AS codigo FROM vigilancia WHERE YEAR(fecha)='.date('Y'),true,true);
	return $consulta['codigo']+1;
}

function recogeEmpleados(){
	$res='';
	$datos=arrayFormulario();
	$empleados=consultaBD('SELECT * FROM empleados WHERE aprobado="SI" AND eliminado="NO" codigoCliente='.$datos['codigo'],true);
	while($empleado=mysql_fetch_assoc($empleados)){
		$res.='<option value="'.$empleado['codigo'].'"';
		if($empleado['codigo']==$datos['select']){
			$res.=' selected ';
		}
		$res.='>'.$empleado['nombre'].' '.$empleado['apellidos'].'</option>';
	}
	echo $res;
}

function estadisticasVigilancia($where){
  if($_SESSION['tipoUsuario']=='CLIENTE'){
    $cliente=obtenerCodigoCliente(true);
    $where.=' AND codigoCliente='.$cliente;
  } else if($_SESSION['tipoUsuario']=='EMPLEADO') {
    $empleado=obtenerCodigoEmpleado(true);
    $where.=' AND codigoEmpleado='.$empleado;
  }
  $where=defineWhereEjercicio($where,'vigilancia.fecha');
  return consultaBD('SELECT COUNT(vigilancia.codigo) AS total FROM vigilancia INNER JOIN clientes ON vigilancia.codigoCliente=clientes.codigo '.$where,true,true);
}

//Fin parte de gestión Auditorías
