<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales


//Parte de documentación de empleado


function imprimeDocumentacionEmpleado(){
	$codigoUsuario=$_SESSION['codigoU'];
	$usuarioFormacion=consultaBD('SELECT usuarioFormacion FROM usuarios_sincronizados WHERE usuarioPRL='.$codigoUsuario,false,true);
	if($usuarioFormacion){
		$usuarioFormacion=$usuarioFormacion['usuarioFormacion'];
	} else {
		$usuarioFormacion=0;
	}
	
	$whereEjercicio='';
	$ejercicio=obtieneEjercicioFiltro();
	if($ejercicio!='Todos'){
		$whereEjercicio="AND YEAR(fecha)='$ejercicio'";
	}

	conexionBD();

	$consulta=consultaBD("SELECT MD5(reconocimientos_medicos.codigo) AS codigo, fecha, apto, ficheroAnalitica
						  FROM reconocimientos_medicos INNER JOIN usuarios_empleados ON reconocimientos_medicos.codigoEmpleado=usuarios_empleados.codigoEmpleado
						  WHERE usuarios_empleados.codigoUsuario='$codigoUsuario' AND reconocimientoVisible='SI' AND reconocimientos_medicos.eliminado='NO' $whereEjercicio;");

	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td> Informe de reconocimiento médico </td>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td class='centro'>
					<a href='../reconocimientos-medicos/generaReconocimiento.php?encp=".$datos['codigo']."' class='btn btn-propio noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar</i></a>
				</td>
			</tr>";

		$ficheroAnalitica=$datos['ficheroAnalitica'];
		if($ficheroAnalitica!='NO' && $ficheroAnalitica!=''){
			echo "
			<tr>
				<td> Analítica </td>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td class='centro'>
					<a href='../documentos/analiticas-procesadas/".$datos['ficheroAnalitica']."' class='btn btn-propio noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar</i></a>
				</td>
			</tr>";
		}
	}

	$consulta=consultaBD("SELECT comentarios_analiticas.comentario, comentarios_analiticas.ficheroAdjunto, reconocimientos_medicos.fecha
						  FROM reconocimientos_medicos INNER JOIN usuarios_empleados ON reconocimientos_medicos.codigoEmpleado=usuarios_empleados.codigoEmpleado
						  INNER JOIN comentarios_analiticas_reconocimientos_medicos ON reconocimientos_medicos.codigo=comentarios_analiticas_reconocimientos_medicos.codigoReconocimiento
						  INNER JOIN comentarios_analiticas ON comentarios_analiticas_reconocimientos_medicos.codigoComentarioAnalitica=comentarios_analiticas.codigo
						  WHERE usuarios_empleados.codigoUsuario='$codigoUsuario' AND reconocimientoVisible='SI' AND ficheroAdjunto NOT IN('','NO') AND checkAdjunto='SI' $whereEjercicio;");

	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td> Anexo de recomendaciones sobre ".$datos['comentario']." </td>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td class='centro'>
					<a href='../documentos/comentarios-analiticas/".$datos['ficheroAdjunto']."' class='btn btn-propio noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar</i></a>
				</td>
			</tr>";
	}

	imprimeDocumentosPRL($codigoUsuario,'documentos_info');
	imprimeDocumentosPRL($codigoUsuario,'documentos_plan_prev');

	cierraBD();

	if($usuarioFormacion!=0){
		conexionBDFormacion();
			$certificado=consultaBD('SELECT codigo, fechaCertificado FROM puestos_empleados WHERE firma!="" AND codigoEmpleado='.$usuarioFormacion,false,true);
			if($certificado){
				echo "
					<tr>
						<td> Certificado de formación </td>
						<td>".formateaFechaWeb($certificado['fechaCertificado'])."</td>
						<td class='centro'>
							<a href='../empleados/generaCertificado.php?empleado=".$certificado['codigo']."' class='btn btn-propio noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar</i></a>
						</td>
					</tr>";

			}
		cierraBD();
	}
}


function imprimeDocumentosPRL($codigoUsuario,$tabla){
	$whereEjercicio='';
	$ejercicio=obtieneEjercicioFiltro();
	if($ejercicio!='Todos'){
		$whereEjercicio="AND YEAR(formularioPRL.fecha)";
	}

	$consulta=consultaBD("SELECT $tabla.nombre, $tabla.fichero, formularioPRL.fecha
						  FROM $tabla INNER JOIN formularioPRL ON $tabla.codigoFormulario=formularioPRL.codigo
						  INNER JOIN contratos ON formularioPRL.codigoContrato=contratos.codigo
						  INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
						  INNER JOIN empleados ON ofertas.codigoCliente=empleados.codigoCliente
						  INNER JOIN usuarios_empleados ON empleados.codigo=usuarios_empleados.codigoEmpleado
						  WHERE usuarios_empleados.codigoUsuario='$codigoUsuario' $whereEjercicio;");

	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td>".$datos['nombre']."</td>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td class='centro'>
					<a href='../documentos/planificacion/".$datos['fichero']."' class='btn btn-propio noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar</i></a>
				</td>
			</tr>";
	}
}

//Fin parte de documentación de empleado