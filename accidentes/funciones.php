<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión Auditorias


function operacionesAccidentes(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaAccidente();
	}
	elseif(isset($_POST['fechaComunicacion'])){
		$res=insertaAccidente();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('accidentes');
	}

	mensajeResultado('fechaComunicacion',$res,'Accidente');
    mensajeResultado('elimina',$res,'Accidente', true);
}


function insertaAccidente(){
	$res=insertaDatos('accidentes');

	if($res){
		$codigoAccidente=$res;
		$datos=arrayFormulario();

		conexionBD();

		$res=insertaMedidasCorrectorasAccidente($datos,$codigoAccidente);
		$res=$res && insertaFotosAccidente($datos,$codigoAccidente);

		cierraBD();
	}

	return $res;
}


function actualizaAccidente(){
	$res=actualizaDatos('accidentes');

	if($res){
		$datos=arrayFormulario();

		conexionBD();

		$res=insertaMedidasCorrectorasAccidente($datos,$datos['codigo'],true);
		$res=$res && insertaFotosAccidente($datos,$datos['codigo'],true);

		cierraBD();
	}

	return $res;
}


function insertaMedidasCorrectorasAccidente($datos,$codigoAccidente,$actualizacion=false){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM medidas_correctoras_accidentes WHERE codigoAccidente='$codigoAccidente';");
	}

	for($i=0;isset($datos['medidas'.$i]);$i++){
		$medidas=$datos['medidas'.$i];
		$plazo=$datos['plazo'.$i];
		$verificacion=$datos['verificacion'.$i];

		$res=$res && consultaBD("INSERT INTO medidas_correctoras_accidentes VALUES(NULL,'$medidas','$plazo','$verificacion','$codigoAccidente');");
	}

	return $res;
}

function insertaFotosAccidente($datos,$codigoAccidente,$actualizacion=false){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM fotos_accidentes WHERE codigoAccidente='$codigoAccidente';");
	}

	for($i=0;isset($datos["fichero$i"]) || compruebaCampoFoto("fichero$i");$i++){
		if(compruebaCampoFoto("fichero$i")){//Inserción de nuevo documento (por $_FILES)
			$fichero=subeDocumento("fichero$i",time().$i,'../documentos/accidentes/');
		}
		else{//Inserción de documento existente (por $_POST)
			$fichero=$datos["fichero$i"];
		}

		$res=$res && consultaBD("INSERT INTO fotos_accidentes VALUES(NULL,'$fichero',$codigoAccidente)");
	}

	return $res;
}


function compruebaCampoFoto($indice){
	return isset($_FILES[$indice]) && $_FILES[$indice]['name']!='';
}

function gestionAccidente(){
	operacionesAccidentes();
	
	abreVentanaGestionConBotones('Gestión de Accidentes de Trabajo','index.php','span2','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('accidentes');
	
	// $trabajador=false;
	// if($datos){
	// 	$trabajador=obtieneDatosAccidentados($datos['codigoEmpleado'],false);
	// }

	/*campoRadio('tipo','Tipo de parte',$datos,'ACCIDENTE',array('Accidente de trabajo con baja','Recaída por un accidente de trabajo con con baja anterior'),array('ACCIDENTE','RECAIDA'),true);
	echo '<h3 class="apartadoFormulario">Datos del trabajador accidentado</h3>';
	abreColumnaCampos();*/
		campoSelectEmpleadoAccidente($datos);
		//campoDato('Sexo',$trabajador['sexo'],'campoSexo');
		//campoDato('Número de aficiliación a la Seguridad Social','','campoNaf');
	/*cierraColumnaCampos();
	abreColumnaCampos();
	cierraColumnaCampos(true);*/
	
	campoFecha('fechaComunicacion','Fecha comunicación',$datos);
	campoSelect('comunicacion','Medio comunicación',array('Correo electrónico','Teléfono','Otro'),array('Correo electrónico','Teléfono','Otro'),$datos);
	campoTextoOtraComunicacion($datos);
	campoFecha('fechaInvestigacion','Fecha investigación',$datos);
	campoSelectConsulta('codigoUsuarioTecnico','Técnico encargado',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE tipo='TECNICO' ORDER BY nombre,apellidos;",$datos);
	campoFecha('fechaAccidente','Fecha accidente',$datos);
	campoHora('horaAccidente','Hora',$datos);
	campoTexto('puesto','Puesto',$datos);
	campoTexto('testigo','Testigo',$datos);
	campoSelect('gradoLesion','Grado lesión',array('','Leve','Grave','Muy grave','Mortal'),array('','Leve','Grave','Muy grave','Mortal'),$datos);
	campoFecha('fechaRecepcionInforme','Redacción',$datos);
	echo '<div style="margin-left:220px;">';
	campoCheckIndividual('checkVisible','Visible',$datos);
	echo '</div>';

	cierraColumnaCampos();
	abreColumnaCampos('columnaDescripcionAccidente');
	
	areaTexto('descripcion','Descripción',$datos,'areaTextoAccidente');
	areaTexto('causas','Causas',$datos,'areaTextoAccidente');
	areaTexto('secuelas','Secuelas',$datos,'areaTextoAccidente');

	cierraColumnaCampos();
	abreColumnaCampos('sinFlotar');
	campoCodigoINSST($datos);
	creaTablaMedidasCorrectoras($datos);
	creaTablaFotos($datos);

	cierraVentanaGestion('index.php');
}

function campoSelectEmpleadoAccidente($datos){
	if(!$datos){

		$query="SELECT clientes.codigo, CONCAT(EMPID,' - ',EMPNOMBRE,' (',EMPMARCA,')') AS texto
				FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente 
				LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
				WHERE activo='SI' AND contratos.enVigor='SI' AND clientes.eliminado='NO'
				ORDER BY EMPNOMBRE";

		campoSelectConsulta('codigoCliente','Empresa',$query);
		campoSelect('codigoEmpleado','Empleado',array(''),array('NULL'));
	}
	else{
		conexionBD();

		$empleado=consultaBD("SELECT codigoCliente FROM empleados WHERE codigo='".$datos['codigoEmpleado']."';",false,true);
		
		$query="SELECT clientes.codigo, CONCAT(EMPID,' - ',EMPNOMBRE,' (',EMPMARCA,')') AS texto
				FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente 
				LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
				WHERE (activo='SI' AND contratos.enVigor='SI' AND clientes.eliminado='NO') OR clientes.codigo='".$empleado['codigoCliente']."'
				ORDER BY EMPNOMBRE";

		campoSelectConsulta('codigoCliente','Empresa',$query,$empleado,'selectpicker span3 show-tick','data-live-search="true"','',0,false);
		
		campoSelectConsulta('codigoEmpleado','Empleado',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM empleados WHERE aprobado='SI' AND eliminado='NO' AND codigoCliente='".$empleado['codigoCliente']."' ORDER BY nombre, apellidos;",$datos,'selectpicker span3 show-tick','data-live-search="true"','',0,false);

		cierraBD();
	}
}

function obtieneDatosAccidentados($codigo=false,$ajax=true){
	if(!$codigo){
		$codigo=$_POST['codigo'];
	}
	$res=consultaBD('SELECT empleados.* FROM empleados WHERE empleados.codigo='.$codigo,true,true);
	$sexo=array('V'=>'Varón','M'=>'Mujer');
	$res['sexo']=$sexo[$res['sexoEmpleado']];
	if($ajax){
		echo json_encode($res);
	} else {
		return $res;
	}
}

function campoTextoOtraComunicacion($datos){
	echo "<div class='hide' id='cajaOtraComunicacion'>";
	campoTexto('textoComunicacion','Descripción medio',$datos);
	echo "</div>";
}



function creaTablaMedidasCorrectoras($datos){
    echo "
    <h3 class='apartadoFormulario'>Causas y  Medidas correctoras / preventivas a aplicar</h3>
    <div class='centro'>
        <table class='table table-striped tabla-simple mitadAncho' id='tablaMedidas'>
          <thead>
            <tr>
              <th> Medidas a Adoptar </th>
              <th> Plazo Ejecución </th>
              <th> Verificación </th>
              <th> </th>
            </tr>
          </thead>
          <tbody>";
        
            $i=0;

            if($datos){
                $consulta=consultaBD("SELECT * FROM medidas_correctoras_accidentes WHERE codigoAccidente='".$datos['codigo']."'", true);

                while($medida=mysql_fetch_assoc($consulta)){
                    imprimeLineaTablaMedidasCorrectoras($i,$medida);
                    $i++;
                }
            }
            
            if($i==0){
                imprimeLineaTablaMedidasCorrectoras(0,false);
            }
      
    echo "</tbody>
        </table>
        <br />
        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaMedidas\");'><i class='icon-plus'></i> Añadir medida</button> 
        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaMedidas\");'><i class='icon-trash'></i> Eliminar medida</button>
    </div>
    <br />";
}

function imprimeLineaTablaMedidasCorrectoras($i,$datos){
    $j=$i+1;

    echo "<tr>";
	        areaTextoTabla('medidas'.$i,$datos['medidas']);
	        campoTextoTabla('plazo'.$i,$datos['plazo']);
	        areaTextoTabla('verificacion'.$i,$datos['verificacion']);
    echo "	<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    	  </tr>";
}


function creaTablaFotos($datos){
    echo "
    <h3 class='apartadoFormulario'>Fotografías relacionadas con el accidente</h3>
    <div class='centro'>
        <table class='table table-striped tabla-simple mitadAncho' id='tablaFotos'>
          <thead>
            <tr>
              <th> Foto </th>
              <th> </th>
            </tr>
          </thead>
          <tbody>";
        
            $i=0;

            if($datos){
                $consulta=consultaBD("SELECT * FROM fotos_accidentes WHERE codigoAccidente='".$datos['codigo']."'",true);

                while($medida=mysql_fetch_assoc($consulta)){
                    imprimeLineaTablaFotos($i,$medida);
                    $i++;
                }
            }
            
            if($i==0){
                imprimeLineaTablaFotos(0,false);
            }
      
    echo "</tbody>
        </table>
        <br />
        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaFotos\");'><i class='icon-plus'></i> Añadir foto</button> 
        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaFotos\");'><i class='icon-trash'></i> Eliminar foto</button>
    </div>
    <br />";
}

function imprimeLineaTablaFotos($i,$datos){
    $j=$i+1;

    echo "<tr class='centro'>";
	        campoFichero('fichero'.$i,'',1,$datos['fichero'],'../documentos/accidentes/');
    echo "	<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    	  </tr>";
}



function imprimeAccidentes($where){
	global $_CONFIG;

	$where=defineWhereEjercicio($where,array('fechaComunicacion','fechaInvestigacion','fechaAccidente','fechaRecepcionInforme'));
	$consulta=consultaBD("SELECT accidentes.codigo, EMPNOMBRE AS empresa, CONCAT(empleados.nombre,' ',empleados.apellidos) AS empleado, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS tecnico,
						  fechaComunicacion, fechaInvestigacion,fechaAccidente,fechaRecepcionInforme, codigoINSST
						  FROM accidentes LEFT JOIN empleados ON accidentes.codigoEmpleado=empleados.codigo
						  LEFT JOIN clientes ON empleados.codigoCliente=clientes.codigo
						  LEFT JOIN usuarios ON accidentes.codigoUsuarioTecnico=usuarios.codigo ".$where, true);

	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td>".$datos['codigoINSST']."</td>
				<td>".$datos['empresa']."</td>
				<td>".$datos['empleado']."</td>
				<td>".formateaFechaWeb($datos['fechaComunicacion'])."</td>
				<td>".$datos['tecnico']."</td>
				<td>".formateaFechaWeb($datos['fechaInvestigacion'])."</td>
				<td class='centro'>
					<div class='btn-group centro'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."accidentes/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li>
						    <li class='divider'></li>
						    <li><a target='_blank' class='noAjax' href='".$_CONFIG['raiz']."accidentes/generaDocumento.php?codigo=".$datos['codigo']."'><i class='icon-download'></i> Generar informe</i></a></li>
						</ul>
					</div>
				</td>
				<td><input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'></td>

			</tr>";
	}

}

function estadisticasAccidentes($where){
	$res=array();
	$res=consultaBD('SELECT COUNT(accidentes.codigo) AS total FROM accidentes LEFT JOIN empleados ON accidentes.codigoEmpleado=empleados.codigo LEFT JOIN clientes ON empleados.codigoCliente=clientes.codigo '.$where,true,true);
	return $res;
}

function generaPDFInforme($codigo,$md5=false){
	
	if($md5){
		$datos=consultaBD("SELECT * FROM accidentes WHERE MD5(codigo)='$codigo';",true,true);
	}
	else{
		$datos=datosRegistro('accidentes',$codigo);
	}


	$meses=array(''=>'','0'=>'','01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
	$fechaComunicacion=explode('-', $datos['fechaComunicacion']);
	$fechaInvestigacion=explode('-', $datos['fechaInvestigacion']);
	$fechaAccidente=explode('-', $datos['fechaAccidente']);
	$medio=$datos['comunicacion'];
	if($medio=='Otro'){
		$medio=$datos['textoComunicacion'];
	}
	$empleado=datosRegistro('empleados',$datos['codigoEmpleado']);
	$empresa=datosRegistro('clientes',$empleado['codigoCliente']);
	$puesto=datosRegistro('puestos_trabajo',$empleado['codigoPuestoTrabajo']);
	$tecnico=datosRegistro('usuarios',$datos['codigoUsuarioTecnico']);
	
	$firmasCodigo=array(145,146,150,1558,152,872);
    $firmas=array(145=>'firmaAdan.png',146=>'firmaAngeles.png',150=>'firmaMarta.png',1558=>'firmaChesco.png',152=>'firmaPatricio.jpg',872=>'firmaPatricio.jpg');
    if(in_array($tecnico['codigo'], $firmasCodigo)){
        $firma='<img style="width:250px" src="../img/'.$firmas[$tecnico['codigo']].'" />';
    }  else {
        $firma='';
    }

	$grado=array('Leve'=>"<img style='width:27px;height:27px' src='../img/cuadoNO.png' />",'Grave'=>"<img style='width:27px;height:27px' src='../img/cuadoNO.png' />",'Muy grave'=>"<img style='width:27px;height:27px' src='../img/cuadoNO.png' />",'Mortal'=>"<img style='width:27px;height:27px' src='../img/cuadoNO.png' />");
	$grado[$datos['gradoLesion']]="<img style='width:27px;height:27px' src='../img/cuadoSI.png' />";
    $contenido = "
	<style type='text/css'>
	<!--
	        body{
	            font-size:12px;
	            font-family: helvetica;
	            font-weight: lighter;
	            line-height: 24px;
	        }

	        .container{
	            background: url(../img/fondoOferta2.png) center center no-repeat;
	            margin-top:0px;
	            height:100%;
	        }

	        .container2{
	            margin-top:0px;
	            height:100%;
	        }

	        .content{
	            margin-top:20px;
	            width:70%;
	            text-align:justify;
	            margin-left:110px;
	        }

	        .cabecera{
	        	margin-top:20px;
	        	border-bottom:1px solid #075581;
	        	border-top:1px solid #075581;
	    	}
	        table .a15 img{
	        	width:100%;
	        }

	        h1{
	        	font-size:28px;
	        	text-align:center;
	        	margin-top:30px;
	        	color:#075581;
	        }

	        h2{
	        	font-size:14px;
	        	text-align:left;
	        	color:#000;
	        	padding-left:20px;
	        	font-weight:bold;
	        }

			table{
				width:100%;
			}	    

	        table .a15{
	        	width:15%;
	        }

	        table .a70{
	        	width:70%;
	        }

	        .tabla1{
	        	border:1px solid #000;
	        	border-collapse:collapse;
	        }

	        .tabla1 td, .tabla1 th{
	        	border:1px solid #000;
	        	padding:5px;
	        }

	        .tabla1 th{
	        	background:#012653;
	        	color:#FFF;
	        	font-weight:bold;
	        	width:45%;
	        	text-align:left;
	        }

	        .tabla1 td{
	        	width:55%;
	        	text-align:left;
	        }

	        .tabla2{
	        	border:1px solid #000;
	        	border-collapse:collapse;
	        }

	        .tabla2 td, .tabla2 th{
	        	border:1px solid #000;
	        	padding:5px;
	        }

	        .tabla2 th{
	        	background:#012653;
	        	color:#FFF;
	        	font-weight:bold;
	        	text-align:center;
	        }

	        .tabla2 td{
	        	text-align:center;
	        }

	        .tabla2 .a70{
	        	width:60%;
	        }


	        .tabla2 .a20{
	        	width:20%;
	        }

	        .pie{
	        	width:70%;
	        	text-align:center;
	        	color:#075581;
	        	margin-left:110px;
	        }

	        .tabla3{
	        	border:1px solid #000;
	        	border-collapse:collapse;
	        }

	        .tabla3 td{
	        	border:1px solid #000;
	        	padding:10px;
	        	background:#CCC;
	        	font-weight:bold;
	        	font-size:14px;
	        	color:#012653;
	        	width:25%;
	        	text-align:center
	        }

	        .tabla4{
	        	border-collapse:collapse;
	        }

	        .tabla4 td, .tabla4 th{
	        	padding:5px;
	        }

	        .tabla4 th{
	        	color:#012653;
	        	border-top:1px solid #012653;
	        	border-bottom:1px solid #012653;;
	        	font-weight:bold;
	        	width:33%;
	        	text-align:left;
	        }

	        .tabla4 td{
	        	width:33%;
	        	text-align:left;
	        	font-weight:bold;
	        	color:#012653;
	        }

	        .tablaFotos{
	        	width:100%;
	        }

	        .tablaFotos td{
	        	width:50%;
	        }

	        .tablaFotos td img{
	        	width:100%;
	        }
		}
	-->
	</style>
	<page footer='page'>
		<div class='container'>
			<div class='cabecera'>
				<table>
					<tr>
						<td class='a15'><img src='../img/logoOferta.png' /></td>
						<td class='a70'><h1>INFORME DE INVESTIGACIÓN DE ACCIDENTES</h1></td>
						<td class='a15' style='text-align:right;line-height:25px;padding-top:5px;'>FO 06.21<br/>
									REVISIÓN: 00<br/>
									FECHA: ".formateaFechaWeb($datos['fechaInvestigacion'])."<br/>
									PÁGINA 1 DE 4</td>
					</tr>
				</table>
			</div>
			<div class='content'>
				Estimados Sres.<br/><br/>

				El pasado día ".$fechaComunicacion[2]." de ".$meses[$fechaComunicacion[1]]." de ".$fechaComunicacion[0]." nos fue comunicado por ".$medio." el accidente laboral sufrido por el empleado/a D./doña ".$empleado['nombre']." ".$empleado['apellidos'].", DNI ".$empleado['dni'].", en fecha ".$fechaAccidente[2]." de ".$meses[$fechaAccidente[1]]." de ".$fechaAccidente[0].". <br/><br/>

				En base al mismo, el día ".$fechaInvestigacion[2]." de ".$meses[$fechaInvestigacion[1]]." de ".$fechaInvestigacion[0].", el Técnico ".$tecnico['nombre']." ".$tecnico['apellidos']." realizó la oportuna investigación y toma de declaración al/a la accidentado/a y a los testigos declarados en dicha comunicación, fruto de la cual se expone el siguiente informe.<br/><br/>

				<h2>1. IDENTIFICACIÓN</h2>
				<table class='tabla1'>
					<tr>
						<th>EMPRESA</th>
						<td>".$empresa['EMPNOMBRE']."</td>
					</tr>
					<tr>
						<th>NOMBRE DEL ACCIDENTADO/A</th>
						<td>".$empleado['nombre']." ".$empleado['apellidos']."</td>
					</tr>
					<tr>
						<th>FECHA DEL ACCIDENTE</th>
						<td>".formateaFechaWeb($datos['fechaAccidente'])."</td>
					</tr>
					<tr>
						<th>HORA DEL ACCIDENTE</th>
						<td>".$datos['horaAccidente']."</td>
					</tr>
					<tr>
						<th>PUESTO DE TRABAJO</th>
						<td>".$datos['puesto']."</td>
					</tr>
					<tr>
						<th>TESTIGO</th>
						<td>".$datos['testigo']."</td>
					</tr>
				</table>
				<h2>2. DESCRIPCION DEL ACCIDENTE</h2>
				<br>".$datos['descripcion']."
				</div>
		</div>
			<page_footer>
				<div class='pie'>
					<b>ANESCO SALUD Y PREVENCIÓN, S.L.</b><br/>
					C/ Murillo, 1 2ª planta. Sevilla. 41001.  CIF: B-90.072.240<br/>
					Telf.: 954109393.  Móvil: 627199152; <u>info@anescoprl.es</u> ; <u>anesco@anescoprl.es</u>
				</div>
			</page_footer>
	</page>
		<page footer='page'>
		<div class='container'>
			<div class='cabecera'>
				<table>
					<tr>
						<td class='a15'><img src='../img/logoOferta.png' /></td>
						<td class='a70'><h1>INFORME DE INVESTIGACIÓN DE ACCIDENTES</h1></td>
						<td class='a15' style='text-align:right;line-height:25px;padding-top:5px;'>FO 06.21<br/>
									REVISIÓN: 00<br/>
									FECHA: ".formateaFechaWeb($datos['fechaInvestigacion'])."<br/>
									PÁGINA 2 DE 4</td>
					</tr>
				</table>
			</div>
			<div class='content'>
				<h2>3. CAUSAS DEL ACCIDENTE</h2>
				<br>".$datos['causas']."<br/><br/>
				<h2>4. MEDIDAS CORRECTORAS / PREVENTIVAS A APLICAR:</h2>
				<table class='tabla2'>
					<tr>
						<th class='a70'>MEDIDAS A ADOPTAR</th>
						<th class='a10'>PLAZO<br/>EJECUCIÓN</th>
						<th class='a20'>VERIFICACIÓN</th>
					</tr>";
				$medidas=consultaBD('SELECT * FROM medidas_correctoras_accidentes WHERE codigoAccidente='.$datos['codigo'],true);
				while($medida=mysql_fetch_assoc($medidas)){
					$contenido.="<tr>
									<td class='a70'>".$medida['medidas']."</td>
									<td class='a20'>".$medida['plazo']."</td>
									<td class='a20'>".$medida['verificacion']."</td>
								</tr>";
				}
$contenido.="	</table>
				</div>
		</div>
			<page_footer>
			<div class='pie'>
				<b>ANESCO SALUD Y PREVENCIÓN, S.L.</b><br/>
				C/ Murillo, 1 2ª planta. Sevilla. 41001.  CIF: B-90.072.240<br/>
				Telf.: 954109393.  Móvil: 627199152; <u>info@anescoprl.es</u> ; <u>anesco@anescoprl.es</u>
				</div>
			</page_footer>
	</page>
	<page footer='page'>
		<div class='container2'>
			<div class='cabecera'>
				<table>
					<tr>
						<td class='a15'><img src='../img/logoOferta.png' /></td>
						<td class='a70'><h1>INFORME DE INVESTIGACIÓN DE ACCIDENTES</h1></td>
						<td class='a15' style='text-align:right;line-height:25px;padding-top:5px;'>FO 06.21<br/>
									REVISIÓN: 00<br/>
									FECHA: ".formateaFechaWeb($datos['fechaInvestigacion'])."<br/>
									PÁGINA 3 DE 4</td>
					</tr>
				</table>
			</div>
			<div class='content'>
				<h2>5. CONCLUSION</h2>
				<table class='tabla3'>
					<tr>
						<td colspan='4'>GRADO DE LESIÓN</td>
					</tr>
					<tr>
						<td style='padding-left:0px;padding-right:0px;text-align:center'>".$grado['Leve']." LEVE</td>
						<td style='padding-left:0px;padding-right:0px;text-align:center'>".$grado['Grave']." GRAVE</td>
						<td style='padding-left:0px;padding-right:0px;text-align:center'>".$grado['Muy grave']." MUY GRAVE</td>
						<td style='padding-left:0px;padding-right:0px;text-align:center'>".$grado['Mortal']." MORTAL</td>
					</tr>
				</table>
				<h2>6. REDACCIÓN Y RECEPCIÓN</h2>
				<table class='tabla4'>
					<tr>
						<th>Elaborado por:</th>
						<th>Dirección de la empresa</th>
						<th>Delegado de prevención</th>
					</tr>
					<tr style='background:#CEE1F0;'>
						<td>".$tecnico['nombre']." ".$tecnico['apellidos']."</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td colspan='3'>Técnico Superior en Prevención de Riesgos Laborales</td>
					</tr>
					<tr>
						<td colspan='3'>".$firma."</td>
					</tr>
					<tr style='background:#CEE1F0;'>
						<td>Fecha:</td>
						<td>Fecha:</td>
						<td>Fecha:</td>
					</tr>
					<tr>
						<td colspan='3'>ANESCO SALUD Y PREVENCIÓN, S.L.</td>
					</tr>
				</table>
				</div>
		</div>
			<page_footer>
				<div class='pie'>
					<b>ANESCO SALUD Y PREVENCIÓN, S.L.</b><br/>
					C/ Murillo, 1 2ª planta. Sevilla. 41001.  CIF: B-90.072.240<br/>
					Telf.: 954109393.  Móvil: 627199152; <u>info@anescoprl.es</u> ; <u>anesco@anescoprl.es</u>
				</div>
			</page_footer>
	</page>
	<page footer='page'>
		<div class='container2'>
			<div class='cabecera'>
				<table>
					<tr>
						<td class='a15'><img src='../img/logoOferta.png' /></td>
						<td class='a70'><h1>INFORME DE INVESTIGACIÓN DE ACCIDENTES</h1></td>
						<td class='a15' style='text-align:right;line-height:25px;padding-top:5px;'>FO 06.21<br/>
									REVISIÓN: 00<br/>
									FECHA: ".formateaFechaWeb($datos['fechaInvestigacion'])."<br/>
									PÁGINA 4 DE 4</td>
					</tr>
				</table>
			</div>
			<div class='content'>
				<h2>7. FOTOGRAFÍAS RELACIONADAS CON EL ACCIDENTE</h2>
				<table class='tablaFotos'>";
				$primer=true;
				$imagenes=consultaBD('SELECT * FROM fotos_accidentes WHERE codigoAccidente='.$datos['codigo'],true);
				while($imagen=mysql_fetch_assoc($imagenes)){
					if($primer){
						$contenido.='<tr><td><img src="../documentos/accidentes/'.$imagen['fichero'].'" /></td>';
						$primer=false;
					} else {
						$contenido.='<td><img src="../documentos/accidentes/'.$imagen['fichero'].'" /></td></tr>';
						$primer=true;
					}
				}
				if(!$primer){
					$contenido.='<td></td></tr>';
				}
$contenido.="</table>			
			</div>
		</div>
			<page_footer>
				<div class='pie'>
					<b>ANESCO SALUD Y PREVENCIÓN, S.L.</b><br/>
					C/ Murillo, 1 2ª planta. Sevilla. 41001.  CIF: B-90.072.240<br/>
					Telf.: 954109393.  Móvil: 627199152; <u>info@anescoprl.es</u> ; <u>anesco@anescoprl.es</u>
				</div>
			</page_footer>
	</page>";

	return $contenido;
}

function campoCodigoINSST($datos,$nombreCampo='codigoINSST',$texto='Código causa'){
	$nombres=array('');
	$valores=array('');
	$listado=listadoCausas();
	foreach ($listado as $key => $value) {
		array_push($valores,$key);
		array_push($nombres,$key.' - '.$value);
	}
	campoSelect($nombreCampo,$texto,$nombres,$valores,$datos,'selectpicker span7 show-tick');
}

function listadoCausas(){
	return array(
		11=>'Configuración de los espacios de trabajo',
		12=>'Orden y limpieza',
		13=>'Agentes físicos en el ambiente',
		2=>'INSTALACIONES DE SERVICIO O PROTECCIÓN',
		21=>'Diseño, construcción, ubicación, montaje, mante­ nimiento, reparación y limpieza de instalaciones de servicio o protección',
		22=>'Elementos y dispositivos de protección de instala­ ciones de servicio o protección',
		23=>'Señalización e información de instalaciones de servicio o protección',
		3=>'MÁQUINAS',
		31=>'Diseño, construcción, ubicación, montaje, mante­ nimiento, reparación y limpieza de máquinas',
		32=>'Elementos y dispositivos de protección de máquinas',
		33=>'Señalización e información de máquinas',
		4=>'OTROS EQUIPOS DE TRABAJO',
		41=>'Diseño, construcción, ubicación, montaje y limpie­ za de otros equipos de trabajo',
		42=>'Elementos y dispositivos de protección de otros equipos de trabajo',
		43=>'Señalización e información de otros equipos de trabajo',
		5=>'MATERIALES Y AGENTES CONTAMINANTES',
		51=>'Manipulación y almacenamiento de materiales',
		52=>'Productos químicos (sustancias o preparados)',
		53=>'Agentes biológicos y seres vivos',
		6=>'ORGANIZACIÓN DEL TRABAJO',
		61=>'Método de trabajo',
		62=>'Realización de las tareas',
		63=>'Formación, información, instrucciones y señaliza­ ción sobre la tarea',
		64=>'Selección y utilización de equipos y materiales',
		7=>'GESTIÓN DE LA PREVENCIÓN',
		71=>'Gestión de la prevención',
		72=>'Actividades preventivas',
		8=>'FACTORES PERSONALES/INDIVIDUALES',
		81=>'Factores de comportamiento',
		82=>'Factores intrínsecos, de salud o capacidades',
		9=>'OTROS',
		91=>'Otras causas',
		92=>'Hechos no causales',
		11=>'CONFIGURACIÓN DE LOS ESPACIOS DE TRABAJO',
		1101=>'Deficiente diseño ergonómico del puesto de trabajo.',
		1102=>'Superficies de trabajo habitualmente inestables, incluidos barcos.',
		1103=>'Espacio insuficiente en lugares de trabajo o en las zonas de tránsito.',
		1104=>'Ausencia/deficiencia de protecciones colectivas frente a caídas de personas.',
		1105=>'Aberturas y huecos desprotegidos.',
		1106=>'Falta de seguridad estructural o estabilidad de pa­ ramentos, etc.',
		1107=>'Ausencia de señalización de límite de sobrecarga de uso de las superficies de trabajo.',
		1108=>'Falta/deficiencia de entibación o taludes inadecua­ dos.',
		1109=>'Dificultad/deficiencia en el acceso al puesto de trabajo.',
		1110=>'Pavimento deficiente o inadecuado (discontinuo, resbaladizo, inestable, con pendiente excesiva, etc.).',
		1111=>'Escaleras fijas, de servicio o escala inseguras por falta de anchura, peldaño desigual, huella insufi­ciente etc o en mal estado.',
		1112=>'Vías de evacuación o salida no señalizadas o seña­ lizadas de forma insuficiente o incorrecta.',
		1113=>'Ausencia de vías de evacuación o insuficientes en número, mal dimensionadas, obstruidas o incorrec­tamente distribuidas.',
		1114=>'Inexistencia, insuficiencia o ineficacia de sectori­ zación o aislamiento de áreas de riesgos, como por ejemplo zonificación de atmósferas explosivas, espacios confinados, zonas extremadamente ca­ lientes o frías etc.',
		1115=>'Deficiencia/ausencia de señalización u otro tipo de elementos necesarios para la delimitación de la zona de trabajo (ej.: maniobras o trabajos próximos a instalaciones de a.t., área de obra, movimientos de vehículos, etc.).',
		1116=>'Vías de circulación deficientes (insuficentes, mal di­ mensionadas o con faltas de separación entre ellas).',
		1117=>'Diseño incorrecto de ventanas (sistema de cierre inseguro, falta de previsión de situaciones de lim­ pieza y mantenimiento).',
		1118=>'Diseño incorrecto de puertas y pontones (siste­ ma de cierre inseguro, sin parada de emergen­ cia, falta de previsión situaciones de limpieza y mantenimiento).',
		1199=>'Otras causas relativas a la configuración de los es­ pacios de trabajo.',
		12=>'ORDEN Y LIMPIEZA',
		1201=>'Orden y limpieza deficientes.',
		1202=>'Ausencia o deficiencia de medios para drenaje de líquidos.',
		1203=>'No delimitación entre las zonas de trabajo o tránsito y las de almacenamiento o no respetar las zonas establecidas.',
		1299=>'Otras causas relativas al orden y limpieza.',
		13=>'AGENTES FÍSICOS EN EL AMBIENTE',
		1301=>'Causas relativas al nivel de ruido ambiental.',
		1302=>'Causas relativas a vibraciones.',
		1303=>'Causas relativas a radiaciones ionizantes.',
		1304=>'Causas relativas a radiaciones no ionizantes.',
		1305=>'Iluminación insuficiente o inapropiada.',
		1306=>'Deslumbramientos.',
		1307=>'Causas relativas a la temperatura y condiciones termo higrométricas.',
		1308=>'Causas relativas a los aspectos meteorológicos.',
		1309=>'Ausencia/deficiencia de protecciones para evitar la generación y propagación de agentes físicos.',
		1399=>'Otras causas relativas a los agentes físicos en el ambiente.',
		1999=>'Otras causas relativas a las condiciones de los es­ pacios de trabajo.',
		21=>'DISEÑO, CONSTRUCCIÓN, UBICACIÓN, MONTAJE, MANTENIMIENTO, REPARACIÓN Y LIMPIEZA DE INSTALACIONES DE SERVICIO O PROTECCIÓN',
		2101=>'Corte no debidamente previsto de suministro ener­ gético o de servicios (agua, electricidad, aire com­ primido, gases, etc.).',
		2102=>'No uso de muy baja tensión de funcionamiento (compatible con el MI BT 036) estando establecido su uso obligatorio.',
		2103=>'Defectos en el diseño, construcción, montaje, man­ tenimiento o uso de instalaciones eléctricas (ubi­ cación de celdas de alta tensión, transformadores, aparallaje o instalaciones de baja tensión).',
		2104=>'Ausencia/deficiencia de sistemas para evitar la ge­ neración de electricidad estática.',
		2105=>'Sistemas de detección de incendios-transmisión de alarma inexistentes, insuficientes o ineficaces.',
		2106=>'Almacenamiento de Sustancias y/o Preparados in­ flamables (“Clasificados como Clases A y B en la MIE-APQ 1”) en coexistencia con focos de ignición de distinta etiología (eléctricos, térmicos, mecáni­ cos...), sin el control preciso.',
		2107=>'Inexistencia de instalación anti-explosiva en at­ mósferas potencialmente explosivas (aplicable a equipos eléctricos, instrumentos neumáticos e hi­ dráulicos).',
		2108=>'Dificultad para efectuar un adecuado manteni­ miento o limpieza de instalaciones de servicio o protección.',
		2109=>'Instalación de servicio o protección en mal estado por otra causa diferente de las anteriores.',
		2110=>'Ausencia de cubeto de retención de una Instalación de almacenamiento de productos químicos.',
		2111=>'Tanques o depósitos inadecuados (por diseño, construcción, ubicación, mantenimiento, uso, etc) por ejemplo no estancos o fabricados con mate­ rial inadecuado para la naturaleza de la sustancia almacenada.',
		2112=>'Sistemas inadecuados de conducción de sustan­ cias o preparados (por diseño, construcción, ubi­cación, mantenimiento, uso, etc) por ejemplo no estancos, fabricados con material inadecuado para la naturaleza de la sustancia transportado.',
		2199=>'Otras causas relativas al diseño, construcción, montaje, mantenimiento, reparación y limpieza de instalaciones de servicio y protección',
		22=>'ELEMENTOS Y DISPOSITIVOS DE PROTECCIÓN DE INSTALACIONES DE SERVI­ CIO O PROTECCIÓN',
		2201=>'Ausencia/deficiencia de medios para evitar los con­ tactos eléctricos directos (defectos en el aislamiento de las partes activas, barreras o envolventes inexis­ tentes o ineficaces, ausencia o deficiencia en los medios para obstaculizar el paso o incumplimiento en cuanto a las distancias que conforman el volumen de accesibilidad).',
		2202=>'Deficiencias de aislamiento o inadecuado grado de protección (i.p.) en conductores, tomas de corriente, aparatos o conexiones eléctricas defectuosas de la instalación.',
		2203=>'Ausencia o no funcionamiento de elementos cons­ tituyentes del sistema de prevención contra contac­ tos indirectos (interruptores diferenciales por ser inadecuados o haber sido “puenteados”, puesta a tierra, etc.).',
		2204=>'Ausencia o no funcionamiento de dispositivos para la eliminación de la electricidad estática.',
		2205=>'Imposibilidad de corte omnipolar simultáneo en Ins­ talaciones eléctricas.',
		2206=>'Ausencia de protección contra sobre-intensidad, sobrecarga y cortocircuito en instalaciones eléc­ tricas.',
		2207=>'Ausencia de protección contra sobrepresión(válvulas de presión,venteos, discos de rotura, válvulas de descarga, etc.).',
		2208=>'Inexistencia, insuficiencia o ineficacia de medios de extinción.',
		2209=>'Generación de atmósferas peligrosas por deficien­ cias de ventilación, natural o forzada.',
		2210=>'Dispositivos de enclavamiento violados (puentea­ dos, anulados, etc.).',
		2299=>'Otras causas relativas los elementos y dispositivos de protección de instalaciones de servicio y pro­ tección.',
		23=>'SEÑALIZACIÓN E INFORMACIÓN DE INSTALACIONES DE SERVICIO O PROTECCIÓN',
		2301=>'Defectos o insuficiencias en la identificación de con­ ductores activos y de protección.',
		2302=>'Conducciones de fluidos peligrosos deficiente­ mente señalizadas (inflamables, tóxicos, corrosi­ vos, etc.).',
		2303=>'Medios de lucha contra incendios no señalizados o señalizados incorrectamente (pulsadores de alar­ ma, extintores, bie ́s,etc.).',
		2399=>'Otras causas relativas a la señalización e infor­ mación de protección de instalaciones de servicio y protección',
		2999=>'Otras causas relativas a las instalaciones de ser­ vicio y protección',
		31=>'DISEÑO, CONSTRUCCIÓN, UBICACIÓN, MONTAJE, MANTENIMIENTO, REPARACIÓN Y LIMPIEZA DE MÁQUINAS',
		3101=>'Diseño incorrecto de la máquina o componente que hace que no se cumplan los principios de la preven­ ción intrínseca y/o de la ergonomía.',
		3102=>'Modificaciones realizadas en la máquina que dan lugar a situaciones de riesgo no previstas por el fabricante.',
		3103=>'Deficiente ubicación de la máquina.',
		3104=>'Defectos de estabilidad en equipos, máquinas o sus componentes.',
		3105=>'Resistencia mecánica insuficiente de la máquina.',
		3106=>'Diseño incorrecto de la máquina frente a presión interna o temperatura o agresión química.',
		3107=>'Focos de ignición no controlados (por causa mecá­ nica, eléctrica, térmica o química).',
		3108=>'Fallos en el sistema neumático, hidráulico o eléc­ trico.',
		3109=>'Órganos de accionamiento inseguros (incorrecto diseño, no visibles o identificables, que posibilitan arranques intempestivos, imposibilitan la detención de partes móviles, variación incontrolada de velo­ cidad, mal funcionamiento del modo manual, etc.).',
		3110=>'Falta de dispositivos de parada, puesta en marcha y control en el punto de operación (en este epígrafe no se contempla la parada de emergencia).',
		3111=>'Accesibilidad a órganos de la máquina peligrosos (atrapantes, cortantes, punzantes, o con posibilidad de ocasionar un contacto eléctrico).',
		3112=>'Accesibilidad o falta de medios de aislamiento a zo­ nas de la máquina en las que puede haber sustan­ cias peligrosas por pérdidas, fugas etc. o a zonas extremadamente calientes/frías.',
		3113=>'Deficiencia de los medios de acceso al puesto de trabajo o de conducción de la maquina.',
		3114=>'Visibilidad insuficiente en el puesto de conducción de la máquina ya sea por un mal diseño o por no disponer de de dispositivos auxiliares que mejoren la visibilidad cuando el campo de visión no es di­ recto (espejos, cámaras de T.V.).',
		3115=>'Defectos o ausencia en el sistema de dirección de la máquina automotriz, en el mecanismo de embra­ gue, en el sistema de cambio de velocidades o en el sistema de frenos.',
		3116=>'Dificultad para efectuar un adecuado manteni­ miento, reglaje o limpieza (accesibilidad a partes internas, dificultad de manipulación, dificultad de supervisión, ausencia de medios de diagnóstico).',
		3117=>'Ausencia/deficiencia de elementos de montaje de máquinas.',
		3118=>'Deficiencia en el dispositivo de enganche/desen­ ganche entre maquinas.',
		3119=>'Categoria insuficiente del dispositivo de mando o de protección.',
		3199=>'Otras causas relativas al diseño, ubicación, cons­ truccion, montaje, mantenimiento reparación y lim­ pieza de máquinas.',
		32=>'ELEMENTOS Y DISPOSITIVOS DE PROTEC­ CIÓN DE MÁQUINAS',
		3201=>'Ausencia y/o deficiencia de resguardos y de dispo­ sitivos de protección (nota: el fallo puede consistir en la inexistencia de resguardos o de dispositivos de protección, en su mala instalación, en su apli­cación en lugar de otros más adecuados al riesgo que quieren evitar).',
		3202=>'Parada de emergencia inexistente, ineficaz o no accesible.',
		3203=>'Ausencia/deficiencia de protecciones colectivas frente a caídas de personas y objetos desde má­quinas.',
		3204=>'Ausencia/deficiencia de protecciones antivuelco en máquinas automotrices (r.o.p.s.).',
		3205=>'Ausencia/deficiencia de estructura de protección contra caída de materiales (f.o.p.s.).',
		3206=>'Ausencia de medios técnicos para la consignación (imposibilidad de puesta en marcha) de la máquina o vehículo.',
		3207=>'Ausencia de dispositivos que eviten que los trabaja­ dores no autorizados utilicen los equipos de trabajo.',
		3208=>'Ausencia/deficiencia o falta de uso de dispositivos que mantengan a los conductores o a los trabajado­ res transportados en su posición correcta durante el desplazamiento.',
		3299=>'Otras causas relativas a los elementos y dispositi­ vos de protección de máquinas.',
		33=>'SEÑALIZACIÓN E INFORMACIÓN DE MÁQUINAS',
		3301=>'Ausencia de alarmas (puesta en marcha de máqui­nas peligrosas o marcha atrás de vehículos, etc.).',
		3302=>'Deficiencia/ausencia del manual de instrucciones de máquinas.',
		3399=>'Otras causas relativas a la señalización e informa­ ción de máquinas.',
		3999=>'Otras causas relativas a las máquinas.',
		41=>'DISEÑO, CONSTRUCCIÓN, UBICACIÓN, MON­ TAJE Y LIMPIEZA DE OTROS EQUIPOS DE TRABAJO',
		4101=>'Diseño incorrecto del equipo que hace que no se cumplan los principios de la prevención intrínseca y/o de la ergonomía.',
		4102=>'Deficiente ubicación, defectos de estabilidad en equipos de trabajo debido a una incorrecta cons­ trucción, montaje y mantenimiento.',
		4103=>'Resistencia mecánica insuficiente del equipo de trabajo.',
		4104=>'Diseño incorrecto del equipo de trabajo frente a presión interna o temperatura o agresión química.',
		4105=>'Partes del equipo accesibles peligrosas (atrapan­ tes, cortantes, punzantes,etc.).',
		4106=>'Escalera de mano insegura (material no resistente, apoyos inadecuados,etc.).',
		4199=>'Otras causas relativas al diseño, ubicación, cons­ truccion, montaje, y limpieza de otros equipos de trabajo.',
		42=>'ELEMENTOS Y DISPOSITIVOS DEPROTECCIÓN DE OTROS EQUIPOS DE TRABAJO',
		4201=>'Ausencia/deficiencia de elementos de seguridad en los medios de elevación de cargas (pestillos de seguridad en ganchos, etc).',
		4202=>'Ausencia de elementos de protección de herramien­ tas (protectores de empuñaduras, etc).',
		4203=>'Ausencia/deficiencia de protecciones colectivas frente a caídas de personas y objetos desde equi­ pos de trabajo.',
		4299=>'Otras causas relativas a los elementos y dispositi­ vos de protección de otros equipos de trabajo',
		43=>'SEÑALIZACIÓN E INFORMACIÓN DE OTROS EQUIPOS DE TRABAJO',
		4301=>'Deficiencia/ausencia del manual de instrucciones o señalización (nota: el fallo puede consistir en la inexistencia del manual de instrucciones o en la in­ suficiente o ininteligible información contenida en él o en la utilización de un idioma incomprensible.',
		4302=>'Falta de señalización e información acerca de la carga máxima en eslingas y otros accesorios de elevación.',
		4399=>'Otras causas relativas a la señalización e informa­ción de otros equipos de trabajo',
		4999=>'Otras causas relativas a otros equipos de trabajo',
		51=>'MANIPULACIÓN Y ALMACENAMIENTO DE MATERIALES',
		5101=>'Defectos en los materiales utilizados de forma ge­ neral, excluídos los que conforman una máquina, instalación y equipo.',
		5102=>'Materiales muy pesados, voluminosos, de gran superficie, inestables o con aristas/perfiles cor­ tantes, en relación con los medios utilizados en su manejo.',
		5103=>'No mecanización o automatización de las operacio­ nes de carga/descarga.',
		5104=>'Deficiente sistema de almacenamiento, empaque­ tado, paletizado, apilamiento, etc.',
		5105=>'Zonas de almacenamiento inadecuadas o no pre­ vistas.',
		5106=>'Falta de planificación y/o vigilancia en operaciones de levantamiento de cargas.',
		5199=>'Otras causas relativas a la manipulación y almace­ namiento de materiales',
		52=>'PRODUCTOS QUÍMICOS (sustancias o prepara­ dos)',
		5201=>'Inhalación, ingestión o contacto con productos quí­ micos (sustancias o preparados) presentes en el puesto de trabajo.',
		5202=>'Ausencia o deficiencia en los procedimientos de manipulado o almacenamiento de productos quí­ micos (sustancias o preparados).',
		5203=>'Presencia de productos químicos (sustancias o preparados) en el ambiente (tóxicos, irritantes, in­ flamables, etc...) en cualquier estado (polvos, va­ pores, gases, etc.), cuyo control o eliminación no está garantizado.',
		5204=>'Productos químicos (sustancias o preparados) ca­ paces de producir reacciones peligrosas (exotér­ micas, tóxicas, etc) cuyo control o eliminación no está garantizado.',
		5205=>'Productos químicos (sustancias o preparados) infla­ mable o explosiva, en cualquier estado físico, cuyo control o eliminación no está garantizado.',
		5206=>'Posibilidad de contacto o mezcla de productos quí­ micos (sustancias o preparados) incompatibles o que pueden generar una reacción con desprendi­miento de productos tóxicos, corrosivos y/o calor.',
		5207=>'Fugas o derrames de productos químicos (sustan­ cias o preparados) (durante su fabricación, trans­ porte, almacenamiento y manipulación).',
		5208=>'Deficiente envasado y etiquetado de los productos químicos (sustancias o preparados) utilizados en caso de trasvase en la propia empresa',
		5209=>'Deficiente envasado y etiquetado de los productos químicos (sustancias o preparados) utilizados (ex­ cluido los trasvasados en la propia empresa).',
		5210=>'Ausencia/deficiencia de protecciones para evitar la generación y propagación de agentes químicos (estará incluida la ausencia/deficiencia de disposi­ tivos de encapsulamiento de la fuente y en general de aquellos que eviten o minimizen la liberación de agentes).',
		5299=>'Otras causas relativas a los productos químicos',
		53=>'AGENTES BIOLÓGICOS Y SERES VIVOS',
		5301=>'Contacto o presencia de animales.',
		5302=>'Agentes biológicos o seres vivos susceptibles de originar cualquier tipo de infección, alergia o toxici­ dad, cuyo control o eliminación no está garantizado.',
		5303=>'Ausencia/deficiencia de protecciones para evitar la generación y propagación de agentes biológicos (estará incluida la ausencia/deficiencia de disposi­ tivos de encapsulamiento de la fuente y en general de aquellos que eviten o minimizen la liberación de agentes).',
		5304=>'Deficiencia de medidas de higiene personal ade­cuadas para la prevención de enfermedades infec­ciosas.',
		5399=>'Otras causas relativas a los agentes biologicos y seres vivos',
		5999=>'Otras causas relativas a materiales y agentes con­ taminantes',
		61=>'MÉTODO DE TRABAJO',
		6101=>'Método de trabajo inexistente.',
		6102=>'Método de trabajo inadecuado.',
		6103=>'Diseño inadecuado del trabajo o tarea.',
		6104=>'Apremio de tiempo o ritmo de trabajo elevado.',
		6105=>'Trabajo monótono o rutinario, sin medidas para evi­tar su efecto nocivo.',
		6106=>'Trabajos solitarios sin las medidas de prevención adecuadas.',
		6107=>'Sobrecarga de la máquina o equipo (respecto a sus características técnicas).',
		6108=>'Sobrecarga del trabajador (fatiga física o mental).',
		6109=>'Existencia de interferencias o falta de coordinación entre trabajadores que realizan la misma o distintas tareas.',
		6110=>'Ausencia de vigilancia, control y dirección por per­ sona competente.',
		6111=>'No organizar el trabajo teniendo en cuenta las con­ diciones meteorológicas adversas.',
		6199=>'Otras causas relativas a los métodos de trabajo',
		62=>'REALIZACIÓN DE LAS TAREAS',
		6201=>'Operación inhabitual para el operario que la realiza, sea ordinaria o esporádica.',
		6202=>'Operación destinada a evitar averías o incidentes o a recuperar incidentes.',
		6203=>'Operación extraordinaria realizada en caso de inci­ dentes, accidentes o emergencias.',
		6299=>'Otras causas relativas a la organización de las tareas',
		63=>'FORMACIÓN, INFORMACIÓN, INSTRUCCIONES Y SEÑALIZACIÓN SOBRE LA TAREA',
		6301=>'Deficiencias en el sistema de comunicación a nivel horizontal o vertical, incluyendo la incomprensión del idioma.',
		6302=>'Instrucciones inexistentes.',
		6303=>'Instrucciones respecto a la tarea confusas, contra­ dictorias o insuficientes.',
		6304=>'Formación/información inadecuada o inexistente sobre la tarea.',
		6305=>'Procedimientos inexistentes o insuficientes para formar o informar a los trabajadores acerca de la utilización o manipulación de maquinaria, equipos, productos, materias primas y útiles de trabajo.',
		6306=>'Deficiencia/ausencia de información o señalización visual o acústica obligatoria o necesaria, incluyendo la utilización de un idioma incomprensible para el trabajador.',
		6307=>'Falta de señalista en caso necesario para organizar la circulación de personas y/o vehículos, así como el manejo de cargas.',
		6308=>'Deficiencia/ausencia de señalización de "prohibi­do maniobra" en los órganos de accionamiento, en caso de trabajos a efectuar sin tensión, sin presión o sin otras energías.',
		6399=>'Otras causas relativas a la formación, información, instrucciones y señalización sobre la tarea.',
		64=>'SELECCIÓN Y UTILIZACIÓN DE EQUIPOS Y MATERIALES',
		6401=>'No poner a disposición de los trabajadores las má­ quinas, equipos y medios auxiliares necesarios o adecuados.',
		6402=>'Selección de máquinas no adecuadas al trabajo a realizar.',
		6403=>'Selección de útiles, herramientas y medios auxilia­ res no adecuados al trabajo a realizar.',
		6404=>'Selección de materiales no adecuados al trabajo a realizar.',
		6405=>'Utilización de la máquina de manera no prevista por el fabricante.',
		6406=>'Utilización de útiles, herramientas y medios auxilia­ res de manera no prevista por el fabricante.',
		6407=>'Utilización de materiales en general de manera no prevista por el fabricante.',
		6408=>'No comprobación del estado de las máquinas, he­ rramientas, equipos o medios auxiliares antes de su utilización.',
		6409=>'Ausencia de medios organizativos o procedimientos para la consignación de máquinas, instalaciones y lugares de trabajo.',
		6499=>'Otras causas relativas a la selección y utilización de equipos y materiales.',
		6999=>'Otras causas relativas a la organización del trabajo.',
		71=>'GESTIÓN DE LA PREVENCIÓN',
		7101=>'Inexistencia o insuficiencia de un procedimiento que regule la realización de las actividades dirigidas a la identificación y evaluación de riesgos, incluidas las referidas a los estudios requeridos en las Obras de Construcción.',
		7102=>'Inexistencia o deficiencia de un procedimiento que regule la planificación de la implantación de las medidas preventivas propuestas, incluidas las referidas a los planes de seguridad en las Obras de Construcción.',
		7103=>'Procedimientos inexistentes o insuficientes para formar o informar a los trabajadores de los riesgos y las medidas preventivas.',
		7104=>'Deficiencias en la organización de los recursos obligatorios (organización preventiva) para la rea­ lización de las actividades preventivas exigidas por la normativa.',
		7105=>'Procedimientos inexistentes, insuficientes o defi­cientes. para la coordinación de actividades reali­ zadas por varias empresas.',
		7106=>'Inadecuada política de compras desde el punto de vista de la prevención.',
		7107=>'Sistema inadecuado de asignación de tareas por otras razones que no sean la falta de cualificación o experiencia.',
		7108=>'No apreciar las características de los trabajadores para la realización de la tarea o en función de los riesgos.',
		7199=>'Otras causas relativas a la gestión de la prevención.',
		72=>'ACTIVIDADES PREVENTIVAS',
		7201=>'No identificación del/los riesgos que han materiali­ zado el accidente.',
		7202=>'Medidas preventivas propuestas en la planificación derivada de la evaluación de riesgos insuficientes o inadecuadas, incluidas las referidas al Plan de Seguridad y Salud en el Trabajo en Obras de Cons­ trucción.',
		7203=>'No ejecución de medidas preventivas propuestas en la planificación derivada de la evaluación de riesgos.',
		7204=>'Falta de control del cumplimiento del Plan de segu­ ridad y salud en Construcción.',
		7205=>'Mantenimiento preventivo inexistente o inadecuado o falta de realización de las revisiones periódicas obligatorias.',
		7206=>'Formación/información inadecuada, inexistente so­ bre riesgos o medidas preventivas.',
		7207=>'Inexistencia o inadecuación de plan y/o medidas de emergencia.',
		7208=>'No poner a disposición de los trabajadores las pren­ das o equipos de protección necesarios o ser estos inadecuados o mal mantenidos, o no supervisar su correcta utilización.',
		7209=>'Falta de presencia de los recursos preventivos re­queridos.',
		7210=>'Vigilancia de la salud inadecuada a los riesgos del puesto de trabajo.',
		7211=>'Ausencia/deficiencias de permisos y/o procedimien­ tos de trabajo en intervenciones peligrosas (por ejemplo soldaduras en zonas de riesgo, trabajos en tensión, espacios confinados etc.).',
		7212=>'Asignación de tarea a un trabajador con falta de cualificación o experiencia.',
		7299=>'Otras causas relativas a las actividades preventi­vas.',
		7999=>'Otras causas relativas a la gestión de la prevención.',
		81=>'FACTORES DE COMPORTAMIENTO',
		8101=>'Realización de tareas no asignadas.',
		8102=>'Incumplimiento de procedimientos e instrucciones de trabajo.',
		8103=>'Incumplimiento de normas de seguridad estable­ cidas.',
		8104=>'Uso indebido de materiales, herramientas o útiles de trabajo, puestos a disposición por la empresa.',
		8105=>'Uso indebido o no utilización de medios auxiliares de trabajo o de seguridad puestos a disposición por la empresa y de uso obligatorio (empujadores, dis­ tanciadores, etc.).',
		8106=>'No utilización de equipos de protección individual puestos a disposición por la empresa y de uso obli­gatorio.',
		8107=>'Retirada o anulación de protecciones o dispositivos de seguridad.',
		8108=>'Permanencia de algún trabajador dentro de una zona peligrosa o indebida.',
		8109=>'Adopción de una postura inadecuada en el puesto de trabajo.',
		8199=>'Otras causas relativas a los factores de comporta­ miento.',
		82=>'FACTORES INTRINSECOS, DE SALUD O CAPACIDADES',
		8201=>'Incapacidad física o mental para la realización nor­ mal del trabajo.',
		8202=>'Deficiente asimilación de órdenes recibidas.',
		8203=>'Falta de cualificación y/o experiencia para la tarea realizada achacable al trabajador.',
		8299=>'Otras causas relativas a los factores intrinsecos, de salud o capacidades',
		8999=>'Otras causas relativas a los factores personales- individuales',
		91=>'OTRAS CAUSAS',
		9199=>'Otras causas.',
		92=>'HECHOS NO CAUSALES',
		9299=>'Hechos no causales.'
	);
}
//Fin parte de gestión Auditorías
