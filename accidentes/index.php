<?php
  $seccionActiva=5;
  include_once('../cabecera.php');
  
  $res=operacionesAccidentes();
  $where='WHERE clientes.codigo IN (SELECT clientes.codigo FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE activo="SI" AND contratos.enVigor="SI" AND clientes.eliminado="NO")';
  $texto='';
  $boton="<a href='".$_CONFIG['raiz']."accidentes/index.php?baja' class='shortcut'><i class='shortcut-icon icon-arrow-down'></i><span class='shortcut-label'>De clientes de baja</span> </a>";
  
  if(isset($_GET['baja'])){
    $where='WHERE clientes.codigo NOT IN (SELECT clientes.codigo FROM clientes LEFT JOIN ofertas ON clientes.codigo=ofertas.codigoCliente LEFT JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE activo="SI" AND contratos.enVigor="SI" AND clientes.eliminado="NO")';
    $texto=' de clientes de baja';
    $boton="<a href='".$_CONFIG['raiz']."accidentes/index.php?' class='shortcut'><i class='shortcut-icon icon-arrow-left'></i><span class='shortcut-label'>Volver</span> </a>";
  }
  
  $where=defineWhereEjercicio($where,array('fechaComunicacion','fechaInvestigacion','fechaAccidente','fechaRecepcionInforme'));
  $estadisticas=estadisticasAccidentes($where);
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

         <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de Accidentes Laborales:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-ambulance"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Accidente/s registrado/s<?php echo $texto;?></div>
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Accidentes de Trabajo</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="<?php echo $_CONFIG['raiz']; ?>accidentes/gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nuevo registro</span> </a>
                <?php echo $boton;?>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Accidentes de Trabajo registrados<?php echo $texto;?></h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table id='tabla1' class="table table-striped table-bordered datatable tablaNo">
              <thead>
                <tr>
                  <th> Causa </th>
                  <th> Empresa </th>
                  <th> Empleado </th>
                  <th> F. comunicación </th>
                  <th> Técnico encargado </th>
                  <th> F. investigación </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeAccidentes($where);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
    </div>
        
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/chart.js" type="text/javascript"></script>
<script type="text/javascript">    

    
</script>

</div><!-- Contenido! -->
<?php include_once('../pie.php'); ?>