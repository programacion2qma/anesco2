<?php
  $seccionActiva=5;
  include_once("../cabecera.php");
  gestionAccidente();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
		$(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});

		$('#codigoCliente').change(function(){
			cargaEmpleadosCliente($(this).val());
		});

		oyenteComunicacion();
		$('#comunicacion').change(function(){
			oyenteComunicacion();
		});
	});

	function cargaEmpleadosCliente(codigoCliente){
		var consulta=$.post('../listadoAjax.php?include=accidentes&funcion=obtieneEmpleadosClienteParaAjax();',{'codigoCliente':codigoCliente});
		consulta.done(function(respuesta){
			$('#codigoEmpleado').html(respuesta).selectpicker('refresh');
		});
	}


	function oyenteComunicacion(){
		if($('#comunicacion').val()=='Otro'){
			$('#cajaOtraComunicacion').removeClass('hide');
		}
		else{
			$('#cajaOtraComunicacion').addClass('hide');
		}
	}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>