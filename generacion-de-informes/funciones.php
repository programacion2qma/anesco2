<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de generación de informes


function operacionesInformes(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('informes');
	}
	elseif(isset($_POST['codigoEvaluacion'])){
		$res=insertaDatos('informes');
	}
	elseif(isset($_GET['codigoVisible'])){
		$res=enviarDocumentacion();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('informes');
	}

	mensajeResultado('codigoEvaluacion',$res,'Informe');
    mensajeResultado('elimina',$res,'Informe', true);
}

function enviarDocumentacion(){
	$res=true;
	$datos=consultaBD('SELECT informes.codigo, clientes.codigo AS codigoCliente, evaluacion_general.codigo AS codigoEvaluacion, clientes.EMPEMAILPRINC
	FROM informes 
	INNER JOIN evaluacion_general ON informes.codigoEvaluacion=evaluacion_general.codigo
	INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo
	WHERE informes.codigo='.$_GET['codigoVisible'],true,true);

	$formularioPRL=consultaBD('SELECT formularioPRL.codigo, formulario FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta INNER JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato WHERE ofertas.codigoCliente='.$datos['codigoCliente'],true,true);

	$ficheros=array();
	$codigoEvaluaciones=array();

	require_once('../../api/html2pdf/html2pdf.class.php');
	require_once('../../api/phpexcel/PHPExcel.php');
	require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');
	
	$contenido=generaPDF($datos['codigo']);
    $html2pdf=new HTML2PDF('P','A4','es');
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->WriteHTML($contenido);
    $html2pdf->Output('../documentos/Evaluación_de_riesgos_empresariales.pdf','f');
    array_push($ficheros,'../documentos/Evaluación_de_riesgos_empresariales.pdf');

    $contenido=generaPlanPrevencion($formularioPRL['codigo']);
    $html2pdf=new HTML2PDF('P','A4','es');
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->WriteHTML($contenido);
    $html2pdf->createIndex('Indice', 18, 10, false, true, 2);
    $html2pdf->Output('../documentos/Plan_de_prevencion.pdf','f');
    array_push($ficheros,'../documentos/Plan_de_prevencion.pdf');

	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("../documentos/plantillaPlanificacionPreventiva.xlsx");
	planificacionPreventiva($objPHPExcel,$datos['codigoEvaluacion']);
	array_push($codigoEvaluaciones, $datos['codigoEvaluacion']);
	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('../documentos/Planificacion_Preventiva_1.xlsx');
	array_push($ficheros, '../documentos/Planificacion_Preventiva_1.xlsx');
	$evaluaciones=consultaBD('SELECT * FROM evaluacion_general WHERE tipoEvaluacion="PUESTOS" AND codigoCliente='.$datos['codigoCliente']);
	$i=2;
	while($item=mysql_fetch_assoc($evaluaciones)){
		$objReader = new PHPExcel_Reader_Excel2007();
		$objPHPExcel = $objReader->load("../documentos/plantillaPlanificacionPreventiva.xlsx");
		planificacionPreventiva($objPHPExcel,$item['codigo']);
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('../documentos/Planificacion_Preventiva_'.$i.'.xlsx');
		array_push($ficheros, '../documentos/Planificacion_Preventiva_'.$i.'.xlsx');
		array_push($codigoEvaluaciones, $item['codigo']);
		$i++;
	}
	
    $adjunto=generaZip($ficheros,true);

    $mensaje='Ya está disponible la Evaluación de riesgos. Ademas le adjunto en este correo la documentación';
    $destinatarios=$datos['EMPEMAILPRINC'];
    $asunto='Disponibilidad de Evaluación y envío de documentación';

    $codigoU=$_SESSION['codigoU'];
    $consulta=consultaBD("SELECT email FROM usuarios WHERE codigo='$codigoU';",true,true); 

    if (!enviaEmail($destinatarios, $asunto, $mensaje ,$adjunto)){
        $res=false;
    }
    else{
        $fecha=date('Y')."-".date('m')."-".date('d');
        $hora=date('H').":".date('i').":00";

        conexionBD();
        consultaBD("INSERT INTO correos VALUES(NULL,'".$destinatarios."', '$fecha', '$hora', '".$asunto."', '".$mensajeBD."', '$adjunto', '$codigoU','USUARIO',NULL,'');");
        foreach ($codigoEvaluaciones as $key => $value) {
        	consultaBD("UPDATE evaluacion_general SET checkVisible='SI' WHERE codigo=".$value);
    	}
        cierraBD();
    }
	return $res;
}


function gestionInforme(){
	operacionesInformes();
	
	abreVentanaGestionConBotones('Gestión de Informes ','?','','icon-edit');

	$datos=compruebaDatos('informes');
	$sql="SELECT evaluacion_general.codigo, CONCAT(EMPNOMBRE,' - ' ,DATE_FORMAT(fechaEvaluacion,'%d/%m/%Y'),' - ',
	IF(tipoEvaluacion='LUGAR',
	CONCAT('LUGAR DE TRABAJO (',IFNULL(clientes_centros.direccion,''),')'),
	IF(tipoEvaluacion='PUESTOS',
	CONCAT('PUESTO DE TRABAJO (',IFNULL(CONCAT(puestos_trabajo.nombre,' - ',c2.direccion),''),')'),
	evaluacion_general.otros))) AS texto 
	FROM evaluacion_general 
	LEFT JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo 
	LEFT JOIN clientes_centros ON evaluacion_general.codigoCentroTrabajo=clientes_centros.codigo
	LEFT JOIN funciones_formulario_prl ON evaluacion_general.codigoPuestoTrabajo=funciones_formulario_prl.codigo
	LEFT JOIN empleados ON funciones_formulario_prl.codigoEmpleado=empleados.codigo
	LEFT JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo
	LEFT JOIN clientes_centros c2 ON funciones_formulario_prl.codigoCentroTrabajo=c2.codigo
	WHERE tipoEvaluacion!='OTROS' ORDER BY fechaEvaluacion DESC";
	campoSelectConsulta('codigoEvaluacion','Evaluación',$sql,$datos,'selectpicker span8 show-tick');

	campoFecha('fecha','Fecha de creación',$datos);
	areaTexto('historico','Histórico',$datos);
	campoOculto($datos,'codigoUsuarioInforme',$_SESSION['codigoU']);

	if($datos){
		$cliente=consultaBD('SELECT codigoCliente FROM evaluacion_general WHERE codigo='.$datos['codigoEvaluacion'],true,true);
		$enlace='index.php?codigoCliente='.$cliente['codigoCliente'];
	} else {
		$enlace='../evaluacion-de-riesgos/index.php';
	}
	cierraVentanaGestion($enlace);
}

function imprimeInformes($where){
	global $_CONFIG;
	$consulta=consultaBD("SELECT informes.codigo, informes.fecha AS fechaInforme, evaluacion_general.fechaEvaluacion AS fechaEvaluacion, EMPNOMBRE, EMPMARCA, clientes.codigo AS codigoCliente, 
						  checkVisible, evaluacion_general.tipoEvaluacion, clientes_centros.direccion AS nombreCentro, puestos_trabajo.nombre AS nombrePuesto, c2.direccion AS direccionPuesto, evaluacion_general.otros 
						  FROM informes INNER JOIN evaluacion_general ON informes.codigoEvaluacion=evaluacion_general.codigo 
						  INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo 
						  LEFT JOIN clientes_centros ON evaluacion_general.codigoCentroTrabajo=clientes_centros.codigo
						  LEFT JOIN funciones_formulario_prl ON evaluacion_general.codigoPuestoTrabajo=funciones_formulario_prl.codigo
						  LEFT JOIN empleados ON funciones_formulario_prl.codigoEmpleado=empleados.codigo
						  LEFT JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo 
						  LEFT JOIN clientes_centros c2 ON funciones_formulario_prl.codigoCentroTrabajo=c2.codigo
	 					  ".$where,true);
	
	while($datos=mysql_fetch_assoc($consulta)){
		/*$enlace="<li><a href='".$_CONFIG['raiz']."generacion-de-informes/generaInforme.php?codigo=".$datos['codigo']."' class='noAjax'><i class='icon-cloud-download'></i> Descarga de informe</i></a></li>";*/

		$tipos=array('LUGAR'=>'Lugar de trabajo ('.$datos['nombreCentro'].')','PUESTOS'=>'Puesto de trabajo ('.$datos['nombrePuesto'].' - '.$datos['direccionPuesto'].')','OTRAS'=>$datos['otros']);
		$formularioPRL=consultaBD('SELECT formularioPRL.codigo, formulario FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta INNER JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato WHERE formularioPRL.fecha < "'.$datos['fechaEvaluacion'].'" AND ofertas.codigoCliente='.$datos['codigoCliente'].' ORDER BY formularioPRL.fecha DESC',true,true);
		if($formularioPRL && $formularioPRL['formulario']!=''){
			$enlace="<li><a target='_blank' href='".$_CONFIG['raiz']."generacion-de-informes/generaPDF.php?codigo=".$datos['codigo']."' class='noAjax'><i class='icon-download'></i> Informe</i></a></li>
				<li class='divider'></li>
				<li><a class='noAjax' target='_blank' href='".$_CONFIG['raiz']."planificacion/generaPlanPrevencion.php?codigo=".$formularioPRL['codigo']."'><i class='icon-download'></i> Plan de prevención</i></a></li>";
		} else {
			$enlace="<li><a onClick='alert(\"Este cliente aún no tiene hecha la toma de datos necesaria para generar el informe\")' class='noAjax' target='_blank'><i class='icon-download'></i> Informe</i></a></li>
				<li class='divider'></li>
				<li><a onClick='alert(\"Este cliente aún no tiene hecha la toma de datos necesaria para generar el plan de prevención\")' class='noAjax' target='_blank'><i class='icon-download'></i> Plan de prevención</i></a></li>";
		}
		$enlace.="<li class='divider'></li>
				<li><a class='noAjax' target='_blank' href='".$_CONFIG['raiz']."generacion-de-informes/generaPlanificacion.php?codigo=".$datos['codigo']."'><i class='icon-download'></i> Planificación<br/>preventiva</i></a></li> ";
		if($datos['checkVisible']=='NO'){
			if($formularioPRL && $formularioPRL['formulario']!=''){
				$enlace.="<li class='divider'></li>
					<li><a class='noAjax' href='".$_CONFIG['raiz']."generacion-de-informes/index.php?codigoVisible=".$datos['codigo']."'><i class='icon-check'></i> Visible</i></a></li> ";
			} else {
				$enlace.="<li class='divider'></li>
					<li><a class='noAjax' onClick='alert(\"Este cliente aún no tiene hecha la toma de datos necesaria para ponerlo como visible\")'><i class='icon-check'></i> Visible</i></a></li> ";
			}
		}
		echo "
			<tr>
				<td>".$datos['EMPNOMBRE']." (".$datos['EMPMARCA'].")</td>
				<td>".$tipos[$datos['tipoEvaluacion']]."</td>
				<td>".formateaFechaWeb($datos['fechaInforme'])."</td>
				<td>".formateaFechaWeb($datos['fechaEvaluacion'])."</td>
				<td class='centro'>
					<div class='btn-group centro'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."generacion-de-informes/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
						    <li class='divider'></li>
						    ".$enlace."
						</ul>
					</div>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
}



function generaDocumentosInformes($PHPWord,$codigoInforme){
	conexionBD();
	/*$datos=consultaBD("SELECT gestion_evaluacion_general.codigo AS codigoGestion, codigoEvaluacion, textoConclusiones, gestion_evaluacion_general.graficoBarra AS graficoBarraGestion, graficoCircular, comentariosDocumentacion1, comentariosDocumentacion2, comentariosDocumentacion3, comentariosDocumentacion4, comentariosDocumentacion5, comentariosDocumentacion6 FROM gestion_evaluacion_general INNER JOIN informes ON gestion_evaluacion_general.codigo=informes.codigoGestion WHERE informes.codigo='$codigoInforme';",false,true);
	$recomendaciones=obtieneCalculoRecomendacionesGestion($datos['codigoGestion']);
	$graficosDelitos=obtieneGraficosDelitos($datos['codigoEvaluacion']);
	$tablasRecomendaciones=obtieneTablasRecomendaciones($datos['codigoGestion'],$graficosDelitos);
	$datosFE=consultaBD("SELECT informes.fecha, razonSocial, nombreComercial, graficoArania, formularios_evaluacion.fecha AS fechaEvaluacion, datosRegistro, numEscritura, fechaEscritura, domicilio FROM ((informes INNER JOIN formularios_evaluacion ON informes.codigoFormularioEvaluacion=formularios_evaluacion.codigo) INNER JOIN clientes ON formularios_evaluacion.codigoCliente=clientes.codigo) INNER JOIN fe1 ON formularios_evaluacion.codigo=fe1.codigoFormularioEvaluacion WHERE informes.codigo='$codigoInforme';",false,true);
	cierraBD();
	
	//Formateo de datos
	$arrayFecha=explode('-',$datosFE['fecha']);
	$diaInforme=$arrayFecha[2];
	$mesInforme=obtieneMes($arrayFecha[1]);
	$anioInforme=$arrayFecha[0];

	$arrayFecha=explode('-',$datosFE['fechaEvaluacion']);
	$diaEvaluacion=$arrayFecha[2];
	$mesEvaluacion=obtieneMes($arrayFecha[1]);
	$anioEvaluacion=$arrayFecha[0];

	$razonSocial=preg_replace('/[&|\?]/','',$datosFE['razonSocial']);//Pare evitar errores en la generación del WORD
	$nombreComercial=preg_replace('/[&|\?]/','',$datosFE['nombreComercial']);*/

	//Fichero de Recomendaciones
	$documento=$PHPWord->loadTemplate('../documentos/recomendaciones/plantilla.docx');

	$documento->setValue("dia",utf8_decode($diaInforme));
	$documento->setValue("mes",utf8_decode($mesInforme));
	$documento->setValue("anio",utf8_decode($anioInforme));
	$documento->setValue("nombreCliente",utf8_decode($razonSocial));

	$documento->setValue("numMedidas",$recomendaciones['numMedidas']);
	$documento->setValue("numPreven",$recomendaciones['numPreven']);
	$documento->setValue("numMitiga",$recomendaciones['numMitiga']);
	$documento->setValue("numSeis",$recomendaciones['numSeis']);
	$documento->setValue("numDoce",$recomendaciones['numDoce']);
	$documento->setValue("numDieciocho",$recomendaciones['numDieciocho']);

	for($i=0;$i<6;$i++){
		$documento->setValue($i,$recomendaciones['grafico'.$i]);		
	}

	$documento->setValue("recomendaciones",$tablasRecomendaciones);

	//Gráfico circular
	$imagen=base64_decode($datos['graficoCircular']);
	file_put_contents('../documentos/recomendaciones/image6.png',$imagen);
	$documento->replaceImage('../documentos/recomendaciones/','image6.png');
	//Gráfico de barra
	$imagen=base64_decode($datos['graficoBarraGestion']);
	file_put_contents('../documentos/recomendaciones/image7.png',$imagen);
	$documento->replaceImage('../documentos/recomendaciones/','image7.png');


	$documento->save('../documentos/recomendaciones/Fichero_Recomendaciones.docx');

	//Fichero de Conclusiones
	$documento=$PHPWord->loadTemplate('../documentos/conclusiones/plantilla.docx');

	$documento->setValue("razonSocial",$razonSocial);
	$documento->setValue("nombreComercial",$nombreComercial);
	$documento->setValue("diaInforme",$diaInforme);
	$documento->setValue("mesInforme",utf8_decode($mesInforme));
	$documento->setValue("anioInforme",$anioInforme);
	$documento->setValue("diaFormulario",$diaEvaluacion);
	$documento->setValue("mesFormulario",utf8_decode($mesEvaluacion));
	$documento->setValue("anioFormulario",$anioEvaluacion);
	$documento->setValue("datosRegistro",utf8_decode($datosFE['datosRegistro']));
	$documento->setValue("numEscritura",utf8_decode($datosFE['numEscritura']));
	$documento->setValue("fechaEscritura",formateaFechaWeb($datosFE['fechaEscritura']));
	$documento->setValue("direccionCliente",utf8_decode($datosFE['domicilio']));
	for($i=1;$i<7;$i++){
		$documento->setValue("comentariosDocumentacion$i",utf8_decode($datos["comentariosDocumentacion$i"]));
	}


	$documento->setValue("conclusionesGestion",utf8_decode($datos['textoConclusiones']));

	//Gráficos delitos
	insertaGraficosDelitosDocumento($graficosDelitos,$documento);


	//Gráfico de araña
	$imagen=base64_decode($datosFE['graficoArania']);
	file_put_contents('../documentos/conclusiones/image3.png',$imagen);

	$documento->replaceImage('../documentos/conclusiones/','image3.png');

	$documento->save('../documentos/conclusiones/Fichero_Conclusiones.docx');

	//Fichero ZIP
	$zip = new ZipArchive();

	$nombreZip='Informes.zip';
	$fichero='../documentos/'.$nombreZip;
	
	if($zip->open($fichero,ZIPARCHIVE::CREATE)===true) {
		$zip->addFile('../documentos/recomendaciones/Fichero_Recomendaciones.docx','Fichero_Recomendaciones.docx');
		$zip->addFile('../documentos/conclusiones/Fichero_Conclusiones.docx','Fichero_Conclusiones.docx');

		if(!$zip->close()){
			echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
			echo $zip->getStatusString().'<br />';
		}
	}

	return $nombreZip;
}

/*function generaInformes($PHPWord,$codigoInforme){
	global $_CONFIG;
	$documento=$PHPWord->loadTemplate('../documentos/personal/plantillaInformes.docx');

	$documento->replaceImage('../img/','image8.png');

	$informe=datosRegistro('informes',$codigoInforme);

	$documento->setValue("fechaInforme",utf8_decode(formateaFechaWeb($informe['fecha'])));
	//Formateo de datos
	$arrayFecha=explode('-',$informe['fecha']);
	$diaInforme=$arrayFecha[2];
	$mesInforme=obtieneMes($arrayFecha[1]);
	$anioInforme=$arrayFecha[0];
	$documento->setValue("dia",utf8_decode($diaInforme));
	$documento->setValue("mes",utf8_decode($mesInforme));
	$documento->setValue("anio",utf8_decode($anioInforme));

	$documento->setValue("nombreEmpresa",utf8_decode($_CONFIG['tituloGeneral']));

	$datos=consultaBD("SELECT gestion_evaluacion_general.codigo AS codigoGestion, informes.codigoEvaluacion, gestion_evaluacion_general.graficoBarra AS graficoBarraGestion, graficoCircular FROM gestion_evaluacion_general INNER JOIN informes ON gestion_evaluacion_general.codigo=informes.codigoGestion WHERE informes.codigo='$codigoInforme';",false,true);
	$recomendaciones=obtieneCalculoRecomendacionesGestion($datos['codigoGestion']);
	$documento->setValue("numMedidas",$recomendaciones['numMedidas']);
	$documento->setValue("numPreven",$recomendaciones['numPreven']);
	$documento->setValue("numMitiga",$recomendaciones['numMitiga']);
	$documento->setValue("numSeis",$recomendaciones['numSeis']);
	$documento->setValue("numDoce",$recomendaciones['numDoce']);
	$documento->setValue("numDieciocho",$recomendaciones['numDieciocho']);

	$graficosRiesgos=obtieneGraficosRiesgos($datos['codigoEvaluacion']);
	$tablasRecomendaciones=obtieneTablasRecomendaciones($datos['codigoGestion'],$graficosRiesgos);
	$documento->setValue("recomendaciones",$tablasRecomendaciones);

	for($i=0;$i<6;$i++){
		$documento->setValue($i,$recomendaciones['grafico'.$i]);		
	}

	$imagen=base64_decode($datos['graficoCircular']);
	file_put_contents('../documentos/conclusiones/image6.png',$imagen);
	$documento->replaceImage('../documentos/conclusiones/','image6.png');
	//Gráfico de barra
	$imagen=base64_decode($datos['graficoBarraGestion']);
	file_put_contents('../documentos/conclusiones/image7.png',$imagen);
	$documento->replaceImage('../documentos/conclusiones/','image7.png');

	$documento->setValue("nombreAbogado",utf8_decode('Nombre Apellidos Abogado'));
	$documento->setValue("nombreAsesorJuridico",utf8_decode('Nombre Apellidos Asesor'));

	$documento->save('../documentos/personal/Fichero_Recomendaciones.docx');

	return "Fichero_Recomendaciones.docx";
}*/

function generaInforme($PHPWord,$codigoInforme){
	global $_CONFIG;
	$documento=$PHPWord->loadTemplate('../documentos/informes/plantilla.docx');

	$datos=consultaBD("SELECT EMPNOMBRE, EMPDIR, informes.fecha, formulario, informes.codigoEvaluacion, clientes.codigo AS codigoCliente, clientes.EMPRL AS representante, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS tecnico, usuarios.codigo AS codigoTecnico
					   FROM informes INNER JOIN evaluacion_general ON informes.codigoEvaluacion=evaluacion_general.codigo
					   INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo 
					   INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
					   INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
					   LEFT JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
					   LEFT JOIN usuarios ON contratos.tecnico=usuarios.codigo
					   WHERE tipoEvaluacion='LUGAR' AND informes.codigo='$codigoInforme' ORDER BY ofertas.fecha DESC;",true,true);

	$tomaDatos=formateaPreguntasFormulario($datos['formulario']);


	$documento->setValue("empresa",utf8_decode(strtoupper($datos['EMPNOMBRE'])));
	$documento->setValue("direccion",utf8_decode($tomaDatos['pregunta2']));
	$documento->setValue("fecha",formateaFechaWeb($datos['fecha']));
	$documento->setValue("centroTrabajo",utf8_decode($tomaDatos['pregunta6']));

	$caracteristicasCentro=$tomaDatos['pregunta23'].' planta(s), que conforma(n) una superficie total de '.$tomaDatos['pregunta21'].' metros cuadrados';
	$documento->setValue("caracteristicasCentro",utf8_decode($caracteristicasCentro));
	if($tomaDatos['pregunta49']=='SI'){
		copy('../documentos/planificacion/'.$datos['ficheroPlano'],'../documentos/planificacion/image2.png');
		$documento->replaceImage('../documentos/planificacion/','image2.png');
	}

	$firmasCodigo=array(145,146,150);
	$firmas=array(145=>'firmaAdan.png',146=>'firmaAngeles.png',150=>'firmaMarta.png');
	if(in_array($datos['codigoTecnico'], $firmasCodigo)){
		copy('../img/'.$firmas[$datos['codigoTecnico']],'../img/image4.png');
		$documento->replaceImage('../img/','image4.png');
	} 

	$instalaciones=nl2br($tomaDatos['pregunta51']);
	$instalaciones=str_replace('<br />','<w:br />',$instalaciones);
	$documento->setValue("instalaciones",utf8_decode($instalaciones));
	$documento->setValue("tablaTareas",utf8_decode(generaTablaTareas($datos['codigoCliente'])));
	$documento->setValue("tablaTrabajadores",utf8_decode(generaTablaTrabajadores($datos['codigoCliente'])));
	$documento->setValue("tablasRiesgos",utf8_decode(generaTablasRiesgos($datos['codigoEvaluacion'])));
	$documento->setValue("representante",utf8_decode($datos['representante']));
	$documento->setValue("tecnico",utf8_decode($datos['tecnico']));
	$documento->setValue("puestos",utf8_decode(generaPuestos($datos['codigoCliente'])));
	/*$informe=datosRegistro('informes',$codigoInforme);
	$evaluacion=datosRegistro('evaluacion_general',$informe['codigoEvaluacion']);
	$gestiones=consultaBD("SELECT * FROM gestion_evaluacion_general WHERE codigoEvaluacion=".$informe['codigoEvaluacion'],true);

	$documento->setValue("fecha",formateaFechaWeb($informe['fecha']));
	$documento->setValue("cliente",utf8_decode($_CONFIG['tituloGeneral']));
	$documento->setValue("evaluacion",utf8_decode(tablaEvaluacion($evaluacion)));
	$documento->setValue("tablaDos",utf8_decode(tablaSegunda($gestiones)));
	//$documento->setValue("anio",utf8_decode($anioInforme));

	$consultaHistorico=consultaBD("SELECT fecha, historico FROM informes WHERE codigoEvaluacion='".$informe['codigoEvaluacion']."' ORDER BY fecha;",true);
	$i=1;
	while($datosHistorico=mysql_fetch_assoc($consultaHistorico)){
		$documento->setValue("r".$i,utf8_decode($i));
		$documento->setValue("f".$i,utf8_decode(formateaFechaWeb($datosHistorico['fecha'])));
		$documento->setValue("h".$i,utf8_decode($datosHistorico['historico']));
		$i++;
	}
	if($i<14){
		while($i<=14){
			$documento->setValue("r".$i,utf8_decode(''));
			$documento->setValue("f".$i,utf8_decode(''));
			$documento->setValue("h".$i,utf8_decode(''));
			$i++;
		}
	}

	$documento->replaceImage('../img/','image1.png');
	if (file_exists('../img/graficos/image5.png')) {
    	unlink('../img/graficos/image5.png');
	}
	$Base64Img = base64_decode($evaluacion['graficoBarraGlobal']);
	file_put_contents('../img/graficos/image5.png', $Base64Img); 
	$documento->replaceImage('../img/graficos/','image5.png');*/

	$documento->save('../documentos/informes/Informe_Evaluacion_Riesgos.docx');

	return "Informe_Evaluacion_Riesgos.docx";
}

function generaTablaTareas($codigoCliente){
	$entra=false;
	$res='<w:p w14:paraId="0A1BD1C5" w14:textId="77777777" w:rsidR="007055EF" w:rsidRPr="0080542A" w:rsidRDefault="007055EF" w:rsidP="007055EF"><w:pPr><w:pStyle w:val="Encabezado"/><w:tabs><w:tab w:val="left" w:pos="0"/></w:tabs><w:ind w:right="584"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblInd w:w="108" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="2552"/><w:gridCol w:w="4262"/><w:gridCol w:w="1798"/></w:tblGrid><w:tr w:rsidR="007055EF" w:rsidRPr="0080542A" w14:paraId="64BC715F" w14:textId="77777777" w:rsidTr="00117BB0"><w:tc><w:tcPr><w:tcW w:w="2552" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="684B1A0D" w14:textId="77777777" w:rsidR="007055EF" w:rsidRPr="00EA2D9C" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00EA2D9C"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>PUESTO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4262" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="24335A5D" w14:textId="77777777" w:rsidR="007055EF" w:rsidRPr="00EA2D9C" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00EA2D9C"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>TAREAS</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1798" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="611E5C01" w14:textId="77777777" w:rsidR="007055EF" w:rsidRPr="00EA2D9C" w:rsidRDefault="00B13A94" w:rsidP="00117BB0"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00EA2D9C"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>ZONA DE TRABAJO</w:t></w:r></w:p></w:tc></w:tr>';
	$puestos=consultaBD('SELECT * FROM puestos_trabajo ORDER BY nombre',true);
	while($puesto=mysql_fetch_assoc($puestos)){
	$entra=true;
	$res.='<w:tr w:rsidR="007055EF" w:rsidRPr="0080542A" w14:paraId="778B9D9C" w14:textId="77777777" w:rsidTr="00117BB0"><w:tc><w:tcPr><w:tcW w:w="2552" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="2C562FA4" w14:textId="5CDDE213" w:rsidR="007055EF" w:rsidRPr="0080542A" w:rsidRDefault="00BE7633" w:rsidP="00117BB0"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr><w:t>'.$puesto['nombre'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4262" w:type="dxa"/></w:tcPr><w:p w14:paraId="016E7029" w14:textId="4736EA57" w:rsidR="007055EF" w:rsidRPr="0080542A" w:rsidRDefault="00BE7633" w:rsidP="00117BB0"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Calibri" w:hAnsi="Calibri"/></w:rPr><w:t>'.$puesto['tareas'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1798" w:type="dxa"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="04D1969C" w14:textId="20C8D39D" w:rsidR="007055EF" w:rsidRPr="0080542A" w:rsidRDefault="00BE7633" w:rsidP="00117BB0"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr><w:t>'.$puesto['zonaTrabajo'].'</w:t></w:r></w:p></w:tc></w:tr>';
	}
	if(!$entra){
		$res.='<w:tr w:rsidR="007055EF" w:rsidRPr="0080542A" w14:paraId="778B9D9C" w14:textId="77777777" w:rsidTr="00117BB0"><w:tc><w:tcPr><w:tcW w:w="2552" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="2C562FA4" w14:textId="5CDDE213" w:rsidR="007055EF" w:rsidRPr="0080542A" w:rsidRDefault="00BE7633" w:rsidP="00117BB0"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr><w:t></w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4262" w:type="dxa"/></w:tcPr><w:p w14:paraId="016E7029" w14:textId="4736EA57" w:rsidR="007055EF" w:rsidRPr="0080542A" w:rsidRDefault="00BE7633" w:rsidP="00117BB0"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Calibri" w:hAnsi="Calibri"/></w:rPr><w:t></w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1798" w:type="dxa"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="04D1969C" w14:textId="20C8D39D" w:rsidR="007055EF" w:rsidRPr="0080542A" w:rsidRDefault="00BE7633" w:rsidP="00117BB0"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr><w:t></w:t></w:r></w:p></w:tc></w:tr>';
	}
	$res.='</w:tbl>';

	return $res;
}

function generaTablaTrabajadores($codigoCliente){
	$entra=false;
	$res='<w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblInd w:w="108" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="5245"/><w:gridCol w:w="3367"/></w:tblGrid><w:tr w:rsidR="007055EF" w:rsidRPr="0080542A" w14:paraId="49B6851C" w14:textId="77777777" w:rsidTr="00117BB0"><w:tc><w:tcPr><w:tcW w:w="5245" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="66209B44" w14:textId="77777777" w:rsidR="007055EF" w:rsidRPr="00EA2D9C" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00EA2D9C"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t xml:space="preserve">NOMBRE </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3367" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0BF178DE" w14:textId="77777777" w:rsidR="007055EF" w:rsidRPr="00EA2D9C" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00EA2D9C"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>PUESTO DE TRABAJO</w:t></w:r></w:p></w:tc></w:tr>';
	$personas=consultaBD('SELECT empleados.nombre, empleados.apellidos, puestos_trabajo.nombre AS puesto FROM empleados INNER JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo WHERE empleados.codigoCliente='.$codigoCliente.' ORDER BY empleados.nombre',true);
	while($persona=mysql_fetch_assoc($personas)){
	$entra=true;
	$res.='<w:tr w:rsidR="007055EF" w:rsidRPr="0080542A" w14:paraId="4CE4FE91" w14:textId="77777777" w:rsidTr="00117BB0"><w:tc><w:tcPr><w:tcW w:w="5245" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7A1D604E" w14:textId="4E7E8C33" w:rsidR="007055EF" w:rsidRPr="0080542A" w:rsidRDefault="00683D96" w:rsidP="00117BB0"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr><w:t>'.$persona['nombre'].' '.$persona['apellidos'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3367" w:type="dxa"/></w:tcPr><w:p w14:paraId="54048812" w14:textId="67D565FA" w:rsidR="007055EF" w:rsidRPr="00863A3A" w:rsidRDefault="00683D96" w:rsidP="00117BB0"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr><w:t>'.$persona['puesto'].'</w:t></w:r><w:bookmarkStart w:id="6" w:name="_GoBack"/><w:bookmarkEnd w:id="6"/></w:p></w:tc></w:tr>';
	}
	if(!$entra){
		$res.='<w:tr w:rsidR="007055EF" w:rsidRPr="0080542A" w14:paraId="4CE4FE91" w14:textId="77777777" w:rsidTr="00117BB0"><w:tc><w:tcPr><w:tcW w:w="5245" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7A1D604E" w14:textId="4E7E8C33" w:rsidR="007055EF" w:rsidRPr="0080542A" w:rsidRDefault="00683D96" w:rsidP="00117BB0"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr><w:t></w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3367" w:type="dxa"/></w:tcPr><w:p w14:paraId="54048812" w14:textId="67D565FA" w:rsidR="007055EF" w:rsidRPr="00863A3A" w:rsidRDefault="00683D96" w:rsidP="00117BB0"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr><w:t></w:t></w:r><w:bookmarkStart w:id="6" w:name="_GoBack"/><w:bookmarkEnd w:id="6"/></w:p></w:tc></w:tr>';
	}
	$res.='</w:tbl>';

	return $res;
}

function generaTablasRiesgos($codigoEvaluacion,$puesto=false){
	$res='';
	$riesgos=consultaBD('SELECT riesgos.codigoInterno, riesgos.nombre AS nombreRiesgo, riesgos_evaluacion_general.descripcionRiesgo AS descripcion, riesgos_evaluacion_general.probabilidad, riesgos_evaluacion_general.consecuencias,riesgos_evaluacion_general.prioridad, gestion_evaluacion_general.codigo AS codigoGestion FROM riesgos_evaluacion_general INNER JOIN riesgos ON riesgos_evaluacion_general.codigoRiesgo=riesgos.codigo INNER JOIN gestion_evaluacion_general ON riesgos_evaluacion_general.codigo=gestion_evaluacion_general.codigoEvaluacion WHERE riesgos_evaluacion_general.codigoEvaluacion='.$codigoEvaluacion,true);
	$probabilidades=array(''=>'',1=>'Baja',2=>'Media',3=>'Alta');
	$consecuencias=array(''=>'',1=>'Ligeramente dañino',2=>'Dañino',3=>'Extremadamente dañino');
	$i=1;
	while($riesgo=mysql_fetch_assoc($riesgos)){
	$textoMedidas='';
	$medidas=consultaBD('SELECT * FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='.$riesgo['codigoGestion'],true);
	while($medida=mysql_fetch_assoc($medidas)){
		if(!$puesto){
			$codigoInterno=$i<10 ? 'L 0'.$i : 'L '.$i;
			$tituloCodigo='RIESGO L XX';
		} else {
			$codigoInterno=$i<10 ? 'P.'.$puesto.' 0'.$i : 'P.'.$puesto.' '.$i;
			$tituloCodigo='RIESGO P.XXX XX';
		}
		$i++;
		if($medida['area']!='' || $medida['responsable']!='' || $medida['recomendacion']!=''){
			if($medida['area']!=''){
				$textoMedidas.=' - Area: '.$medida['area']; 
			}
			if($medida['responsable']!=''){
				$textoMedidas.=' - Responsable: '.$medida['responsable']; 
			}
			if($medida['area']!=''){
				$textoMedidas.=' - Recomendación: '.$medida['recomendacion']; 
			}

			if($medida['plazo']==1){
				$textoMedidas.=' - Plazo: '.$medida['otroPlazo'];
			} else {
				$textoMedidas.=' - Plazo: '.$medida['plazo'].' meses';
			}
			$textoMedidas.='<w:br/>';
		}
	}
	$res.='<w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="000000"/><w:left w:val="single" w:sz="4" w:space="0" w:color="000000"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/><w:right w:val="single" w:sz="4" w:space="0" w:color="000000"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="000000"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="000000"/></w:tblBorders><w:tblLayout w:type="fixed"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="2835"/><w:gridCol w:w="108"/><w:gridCol w:w="2835"/><w:gridCol w:w="2942"/></w:tblGrid><w:tr w:rsidR="007055EF" w:rsidRPr="00095485" w14:paraId="30426CB1" w14:textId="77777777" w:rsidTr="00117BB0"><w:tc><w:tcPr><w:tcW w:w="2835" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="7452EE4C" w14:textId="77777777" w:rsidR="007055EF" w:rsidRPr="00696D2D" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00696D2D"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.$tituloCodigo.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="5885" w:type="dxa"/><w:gridSpan w:val="3"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="2C8CCA87" w14:textId="77777777" w:rsidR="007055EF" w:rsidRPr="00696D2D" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00696D2D"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>FACTOR DE RIESGO</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="007055EF" w:rsidRPr="00095485" w14:paraId="08158284" w14:textId="77777777" w:rsidTr="00117BB0"><w:tc><w:tcPr><w:tcW w:w="2835" w:type="dxa"/></w:tcPr><w:p w14:paraId="7F509F5C" w14:textId="0D1D128A" w:rsidR="007055EF" w:rsidRPr="00095485" w:rsidRDefault="007C57B5" w:rsidP="00117BB0"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr><w:t>'.$codigoInterno.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="5885" w:type="dxa"/><w:gridSpan w:val="3"/></w:tcPr><w:p w14:paraId="52FE7D30" w14:textId="3B899A5E" w:rsidR="007055EF" w:rsidRPr="00095485" w:rsidRDefault="005C1DA7" w:rsidP="00117BB0"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr><w:t>'.$riesgo['nombreRiesgo'].'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="007055EF" w:rsidRPr="00095485" w14:paraId="01C0DADA" w14:textId="77777777" w:rsidTr="00117BB0"><w:tc><w:tcPr><w:tcW w:w="8720" w:type="dxa"/><w:gridSpan w:val="4"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="3C3CC0E5" w14:textId="77777777" w:rsidR="007055EF" w:rsidRPr="00696D2D" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00696D2D"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>DESCRIPCIÓN</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="007055EF" w:rsidRPr="00095485" w14:paraId="40B5AC67" w14:textId="77777777" w:rsidTr="00117BB0"><w:tc><w:tcPr><w:tcW w:w="8720" w:type="dxa"/><w:gridSpan w:val="4"/></w:tcPr><w:p w14:paraId="5E987F83" w14:textId="77777777" w:rsidR="007055EF" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="1B60A5C7" w14:textId="77777777" w:rsidR="007055EF" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.$riesgo['descripcion'].'</w:t></w:r></w:p><w:p w14:paraId="70F34BF3" w14:textId="77777777" w:rsidR="007055EF" w:rsidRDefault="007055EF" w:rsidP="005C1DA7"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr></w:p></w:tc></w:tr><w:tr w:rsidR="007055EF" w:rsidRPr="00095485" w14:paraId="691B4EEC" w14:textId="77777777" w:rsidTr="00117BB0"><w:tc><w:tcPr><w:tcW w:w="8720" w:type="dxa"/><w:gridSpan w:val="4"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="14A3C077" w14:textId="77777777" w:rsidR="007055EF" w:rsidRPr="00696D2D" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00696D2D"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>EVALUACIÓN</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="007055EF" w:rsidRPr="00095485" w14:paraId="79215117" w14:textId="77777777" w:rsidTr="00117BB0"><w:tc><w:tcPr><w:tcW w:w="2943" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="57AFAEB1" w14:textId="77777777" w:rsidR="007055EF" w:rsidRPr="00696D2D" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00696D2D"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>PROBABILIDAD</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2835" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="09CD98EA" w14:textId="77777777" w:rsidR="007055EF" w:rsidRPr="00696D2D" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00696D2D"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>CONSECUENCIAS</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2942" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="2F700FF7" w14:textId="77777777" w:rsidR="007055EF" w:rsidRPr="00696D2D" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00696D2D"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>NIVEL DE RIESGO</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="007055EF" w:rsidRPr="00095485" w14:paraId="13DE215F" w14:textId="77777777" w:rsidTr="00117BB0"><w:tc><w:tcPr><w:tcW w:w="2943" w:type="dxa"/><w:gridSpan w:val="2"/></w:tcPr><w:p w14:paraId="0E30E1CF" w14:textId="39C650F4" w:rsidR="007055EF" w:rsidRPr="00095485" w:rsidRDefault="00DB67BF" w:rsidP="00117BB0"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr><w:t>'.$probabilidades[$riesgo['probabilidad']].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2835" w:type="dxa"/></w:tcPr><w:p w14:paraId="04537ED2" w14:textId="289D79A0" w:rsidR="007055EF" w:rsidRPr="00095485" w:rsidRDefault="00DB67BF" w:rsidP="00117BB0"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr><w:t>'.$consecuencias[$riesgo['consecuencias']].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2942" w:type="dxa"/></w:tcPr><w:p w14:paraId="1234EFBF" w14:textId="3C4FBC14" w:rsidR="007055EF" w:rsidRPr="00095485" w:rsidRDefault="00DB67BF" w:rsidP="00117BB0"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr><w:t>'.$riesgo['prioridad'].'</w:t></w:r><w:bookmarkStart w:id="6" w:name="_GoBack"/><w:bookmarkEnd w:id="6"/></w:p></w:tc></w:tr><w:tr w:rsidR="007055EF" w:rsidRPr="00095485" w14:paraId="29969EF5" w14:textId="77777777" w:rsidTr="00117BB0"><w:tc><w:tcPr><w:tcW w:w="8720" w:type="dxa"/><w:gridSpan w:val="4"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="4BB2E8B7" w14:textId="77777777" w:rsidR="007055EF" w:rsidRPr="00696D2D" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00696D2D"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t xml:space="preserve">PROPUESTA DE MEDIDAS PREVENTIVAS </w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="007055EF" w:rsidRPr="00095485" w14:paraId="6D6F5835" w14:textId="77777777" w:rsidTr="00117BB0"><w:tc><w:tcPr><w:tcW w:w="8720" w:type="dxa"/><w:gridSpan w:val="4"/></w:tcPr><w:p w14:paraId="4BBCC954" w14:textId="77777777" w:rsidR="007055EF" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="24846BD7" w14:textId="77777777" w:rsidR="007055EF" w:rsidRDefault="007055EF" w:rsidP="009E64F4"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:ind w:left="720"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr><w:bookmarkStart w:id="7" w:name="_GoBack"/><w:bookmarkEnd w:id="7"/><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t xml:space="preserve">- </w:t></w:r><w:proofErr w:type="spellStart"/><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.$textoMedidas.'</w:t></w:r></w:p><w:p w14:paraId="4711A037" w14:textId="77777777" w:rsidR="007055EF" w:rsidRDefault="007055EF" w:rsidP="00117BB0"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr></w:p></w:tc></w:tr></w:tbl><w:p></w:p>';
	}
	return $res;
}

function generaPuestos($codigoCliente){
	$res='';
	$evaluaciones=consultaBD('SELECT evaluacion_general.codigo AS codigoEvaluacion, puestos_trabajo.nombre AS puesto, puestos_trabajo.tareas, puestos_trabajo.zonaTrabajo, clientes.EMPNOMBRE, puestos_trabajo.codigo AS codigoPuesto FROM evaluacion_general INNER JOIN puestos_trabajo ON evaluacion_general.codigoPuestoTrabajo=puestos_trabajo.codigo INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo WHERE tipoEvaluacion="PUESTOS" AND checkActivo="SI" AND evaluacion_general.codigoCliente='.$codigoCliente,true);
	$i=1;
	while($evaluacion=mysql_fetch_assoc($evaluaciones)){
		//Titulo de la evaluacion del puesto
		$res.='<w:p w14:paraId="242E53F3" w14:textId="326D705D" w:rsidR="00965AF9" w:rsidRPr="00965AF9" w:rsidRDefault="00965AF9" w:rsidP="00965AF9"><w:pPr><w:ind w:left="360"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr></w:pPr><w:r w:rsidRPr="00965AF9"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t>7.'.$i.'. EVALUACIÓN DE RIESGO DEL PUESTO: '.strtoupper($evaluacion['puesto']).'</w:t></w:r></w:p>';
		$res.='<w:p w14:paraId="483D303E" w14:textId="77777777" w:rsidR="00445292" w:rsidRDefault="00445292" w:rsidP="00EE6FC3"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:color w:val="FF0000"/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr></w:pPr></w:p>';
		//Cuadro del puesto
		$res.='<w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblInd w:w="108" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="1985"/><w:gridCol w:w="3685"/><w:gridCol w:w="2942"/></w:tblGrid><w:tr w:rsidR="00EE6FC3" w:rsidRPr="0080542A" w14:paraId="025D5794" w14:textId="77777777" w:rsidTr="00B13A94"><w:tc><w:tcPr><w:tcW w:w="1985" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="6E709F86" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="00EA2D9C" w:rsidRDefault="00EE6FC3" w:rsidP="00CE1C26"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00EA2D9C"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>PUESTO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3685" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="71A4D22F" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="00EA2D9C" w:rsidRDefault="00EE6FC3" w:rsidP="00CE1C26"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00EA2D9C"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>TAREAS</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2942" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="5B2054AD" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="00EA2D9C" w:rsidRDefault="00B13A94" w:rsidP="00CE1C26"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/></w:rPr></w:pPr><w:r w:rsidRPr="00EA2D9C"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>ZONA DE TRABAJO</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00EE6FC3" w:rsidRPr="0080542A" w14:paraId="3D0E4426" w14:textId="77777777" w:rsidTr="00B13A94"><w:tc><w:tcPr><w:tcW w:w="1985" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="51F57D5D" w14:textId="6E68250C" w:rsidR="00EE6FC3" w:rsidRPr="0080542A" w:rsidRDefault="00FA0D3B" w:rsidP="00CE1C26"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr><w:t>'.$evaluacion['puesto'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3685" w:type="dxa"/></w:tcPr><w:p w14:paraId="51D74642" w14:textId="2C6230C0" w:rsidR="00EE6FC3" w:rsidRPr="0080542A" w:rsidRDefault="00FA0D3B" w:rsidP="00CE1C26"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Calibri" w:hAnsi="Calibri"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Calibri" w:hAnsi="Calibri"/></w:rPr><w:t>'.$evaluacion['tareas'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2942" w:type="dxa"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4AAF2AEB" w14:textId="4DF50519" w:rsidR="00EE6FC3" w:rsidRPr="0080542A" w:rsidRDefault="00FA0D3B" w:rsidP="00CE1C26"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/></w:rPr><w:t>'.$evaluacion['zonaTrabajo'].'</w:t></w:r><w:bookmarkStart w:id="7" w:name="_GoBack"/><w:bookmarkEnd w:id="7"/></w:p></w:tc></w:tr></w:tbl>';
		//Sección tareas
		$res.='<w:p w14:paraId="67594A48" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="0080542A" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:pStyle w:val="texto"/><w:spacing w:before="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Arial"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:p w14:paraId="67839C3F" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="00186143" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:spacing w:line="276" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t>Son</w:t></w:r><w:r w:rsidRPr="00F01E4E"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t xml:space="preserve"> la</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t>s</w:t></w:r><w:r w:rsidRPr="00F01E4E"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t xml:space="preserve"> persona</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t>s</w:t></w:r><w:r w:rsidRPr="00F01E4E"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t xml:space="preserve"> responsable</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t>s</w:t></w:r><w:r w:rsidRPr="00F01E4E"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t xml:space="preserve"> de </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t>...</w:t></w:r></w:p><w:p w14:paraId="3F0E12E6" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:pStyle w:val="Ttulo7"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Arial"/><w:b w:val="0"/><w:i/><w:iCs/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr></w:pPr></w:p><w:p w14:paraId="19F7DE9E" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="00787B75" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:pStyle w:val="Ttulo7"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Arial"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr></w:pPr><w:r w:rsidRPr="00787B75"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Arial"/><w:b w:val="0"/><w:iCs/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t>'.$evaluacion['tareas'].'</w:t></w:r></w:p><w:p w14:paraId="55241DCD" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="00863A3A" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr></w:pPr></w:p><w:p w14:paraId="65982979" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="00863A3A" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:pStyle w:val="texto"/><w:spacing w:before="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Arial"/><w:bCs/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Arial"/><w:bCs/><w:szCs w:val="22"/></w:rPr><w:t>Entre las tareas que se realizan se pueden destacar:</w:t></w:r></w:p><w:p w14:paraId="197BE54B" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="00AA4E51" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:pStyle w:val="texto"/><w:tabs><w:tab w:val="left" w:pos="1080"/><w:tab w:val="left" w:pos="4680"/></w:tabs><w:spacing w:before="0" w:line="240" w:lineRule="auto"/><w:ind w:firstLine="720"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Arial"/><w:bCs/><w:color w:val="FF0000"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:p w14:paraId="41183C06" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRDefault="00EE6FC3" w:rsidP="00E847A6"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="15"/></w:numPr><w:tabs><w:tab w:val="left" w:pos="709"/></w:tabs><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t>TAREAS.</w:t></w:r></w:p><w:p w14:paraId="59C37A7C" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="00DE1789" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:tabs><w:tab w:val="left" w:pos="709"/></w:tabs><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr></w:pPr></w:p><w:p w14:paraId="3B3ECBC0" w14:textId="77777777" w:rsidR="00664168" w:rsidRDefault="00664168" w:rsidP="00C82705"><w:pPr><w:tabs><w:tab w:val="left" w:pos="709"/></w:tabs><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t>El médico del trabajo, del departamento de vigilancia de la salud, será quien decida los protocolos médicos a aplicar</w:t></w:r><w:r w:rsidR="00445292"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t xml:space="preserve"> a los trabajadores con este puesto</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t>, en función de la presente evaluación de riesgos.</w:t></w:r></w:p>';
		//Título Equipos de trabajo
		$res.='<w:p w14:paraId="731B6362" w14:textId="77777777" w:rsidR="00664168" w:rsidRDefault="00664168" w:rsidP="00EE6FC3"><w:pPr><w:tabs><w:tab w:val="left" w:pos="709"/></w:tabs><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p><w:p w14:paraId="7CC0903F" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="0080542A" w:rsidRDefault="00630389" w:rsidP="00EE6FC3"><w:pPr><w:tabs><w:tab w:val="left" w:pos="709"/></w:tabs><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:noProof/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:pict w14:anchorId="1D176152"><v:shape id="_x0000_s1037" type="#_x0000_t202" style="position:absolute;margin-left:-1.95pt;margin-top:19.45pt;width:438.75pt;height:25.05pt;z-index:-251644928;mso-height-percent:200;mso-height-percent:200;mso-width-relative:margin;mso-height-relative:margin" wrapcoords="-41 -745 -41 20855 21641 20855 21641 -745 -41 -745" fillcolor="#b8cce4" strokecolor="white"><v:textbox style="mso-next-textbox:#_x0000_s1037;mso-fit-shape-to-text:t"><w:txbxContent><w:p w14:paraId="3BA8CA7F" w14:textId="77777777" w:rsidR="007C57B5" w:rsidRPr="00EA2D9C" w:rsidRDefault="007C57B5" w:rsidP="00EE6FC3"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="002060"/><w:sz w:val="28"/></w:rPr></w:pPr><w:r w:rsidRPr="00EA2D9C"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="002060"/><w:sz w:val="28"/></w:rPr><w:t>EQUIPOS DE TRABAJO</w:t></w:r></w:p></w:txbxContent></v:textbox><w10:wrap type="through"/></v:shape></w:pict></w:r></w:p>';
		//Texto Equipos de trabajo
		$res.='<w:p w14:paraId="026AC597" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:pStyle w:val="Encabezado"/><w:tabs><w:tab w:val="left" w:pos="0"/></w:tabs><w:ind w:right="584"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:p w14:paraId="552AD957" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="00186143" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:pStyle w:val="Encabezado"/><w:tabs><w:tab w:val="left" w:pos="0"/></w:tabs><w:ind w:right="584"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r w:rsidRPr="00186143"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>Maquinaria</w:t></w:r><w:r w:rsidRPr="00186143"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>:</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t xml:space="preserve"> XXX, etc.</w:t></w:r></w:p><w:p w14:paraId="67E0D43E" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="00186143" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:pStyle w:val="Encabezado"/><w:tabs><w:tab w:val="left" w:pos="0"/></w:tabs><w:ind w:right="584"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r w:rsidRPr="00186143"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p><w:p w14:paraId="373B461C" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:pStyle w:val="Encabezado"/><w:tabs><w:tab w:val="left" w:pos="0"/></w:tabs><w:ind w:right="584"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r w:rsidRPr="00186143"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>Herramientas y utillaje:</w:t></w:r><w:r w:rsidRPr="00186143"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>XXX, etc.</w:t></w:r></w:p><w:p w14:paraId="77FD761D" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="00186143" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:pStyle w:val="Encabezado"/><w:tabs><w:tab w:val="left" w:pos="0"/></w:tabs><w:ind w:right="584"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:p w14:paraId="51A91D7C" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="00186143" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:pStyle w:val="Encabezado"/><w:tabs><w:tab w:val="left" w:pos="0"/></w:tabs><w:ind w:right="584"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r w:rsidRPr="00186143"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>Otro equipamiento:</w:t></w:r><w:r w:rsidRPr="00186143"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>XXX, etc.</w:t></w:r></w:p><w:p w14:paraId="5FDD0E10" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="00186143" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:pStyle w:val="Encabezado"/><w:tabs><w:tab w:val="left" w:pos="0"/></w:tabs><w:ind w:right="584"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:p w14:paraId="3E716ADB" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="00186143" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:pStyle w:val="Encabezado"/><w:tabs><w:tab w:val="left" w:pos="0"/></w:tabs><w:ind w:right="584"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r w:rsidRPr="00186143"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:b/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>Material de consumo:</w:t></w:r><w:r w:rsidRPr="00186143"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>XXX, etc.</w:t></w:r></w:p><w:p w14:paraId="201D34BE" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:spacing w:line="276" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:p w14:paraId="1B7D270E" w14:textId="77777777" w:rsidR="00266ADA" w:rsidRDefault="00266ADA" w:rsidP="00266ADA"><w:pPr><w:pStyle w:val="Encabezado"/><w:tabs><w:tab w:val="left" w:pos="0"/></w:tabs><w:ind w:right="584"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>La maquinaria y los equipos de trabajo de la empresa están recogidos en un listado ANEXO a la presente evaluación de riesgos (proporcionado por la empresa).</w:t></w:r></w:p>';
		//Titulo y texto Productos y sustancias
		$res.='<w:p w14:paraId="2B50093F" w14:textId="77777777" w:rsidR="00266ADA" w:rsidRDefault="00266ADA" w:rsidP="00EE6FC3"><w:pPr><w:spacing w:line="276" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:p w14:paraId="387F8277" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRDefault="00630389" w:rsidP="00EE6FC3"><w:pPr><w:spacing w:line="276" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Calibri" w:hAnsi="Calibri"/><w:noProof/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:pict w14:anchorId="24FD5279"><v:shape id="_x0000_s1038" type="#_x0000_t202" style="position:absolute;margin-left:-1.95pt;margin-top:21.15pt;width:438.75pt;height:25.05pt;z-index:-251643904;mso-height-percent:200;mso-height-percent:200;mso-width-relative:margin;mso-height-relative:margin" wrapcoords="-41 -745 -41 20855 21641 20855 21641 -745 -41 -745" fillcolor="#b8cce4" strokecolor="white"><v:textbox style="mso-next-textbox:#_x0000_s1038;mso-fit-shape-to-text:t"><w:txbxContent><w:p w14:paraId="5C42E60A" w14:textId="77777777" w:rsidR="007C57B5" w:rsidRPr="00EA2D9C" w:rsidRDefault="007C57B5" w:rsidP="00EE6FC3"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="002060"/><w:sz w:val="28"/></w:rPr></w:pPr><w:r w:rsidRPr="00EA2D9C"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="002060"/><w:sz w:val="28"/></w:rPr><w:t>PRODUCTOS O SUSTANCIAS</w:t></w:r></w:p></w:txbxContent></v:textbox><w10:wrap type="through"/></v:shape></w:pict></w:r></w:p><w:p w14:paraId="0EBCDB23" w14:textId="339C24FA" w:rsidR="009020F9" w:rsidRPr="00EF3BB7" w:rsidRDefault="009020F9" w:rsidP="009020F9"><w:pPr><w:spacing w:line="276" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t xml:space="preserve">Se adjunta a la presente evaluación un anexo con un listado de las sustancias empleadas. Esta información es facilitada por </w:t></w:r><w:r w:rsidR="00DE6783"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Calibri" w:hAnsi="Calibri"/><w:color w:val="002060"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.strtoupper($evaluacion['EMPNOMBRE']).'</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>.</w:t></w:r></w:p>';
		//Titulo EPIS
		$res.='<w:p w14:paraId="7E2F8143" w14:textId="77777777" w:rsidR="00EF3BB7" w:rsidRDefault="00EF3BB7" w:rsidP="00EE6FC3"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:p w14:paraId="7F7EE657" w14:textId="77777777" w:rsidR="000443C7" w:rsidRDefault="000443C7" w:rsidP="00EE6FC3"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:p w14:paraId="42579350" w14:textId="77777777" w:rsidR="000443C7" w:rsidRDefault="000443C7" w:rsidP="00EE6FC3"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:p w14:paraId="34BB39B5" w14:textId="77777777" w:rsidR="000443C7" w:rsidRDefault="000443C7" w:rsidP="00EE6FC3"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:p w14:paraId="0DA19FD6" w14:textId="77777777" w:rsidR="000443C7" w:rsidRPr="000443C7" w:rsidRDefault="00630389" w:rsidP="000443C7"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:noProof/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:pict w14:anchorId="2551A19C"><v:shape id="Cuadro_x0020_de_x0020_texto_x0020_36" o:spid="_x0000_s1045" type="#_x0000_t202" style="position:absolute;margin-left:.45pt;margin-top:4.3pt;width:438.75pt;height:25.05pt;z-index:-251640832;visibility:visible;mso-wrap-style:square;mso-width-percent:0;mso-height-percent:200;mso-wrap-distance-left:9pt;mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;mso-wrap-distance-bottom:0;mso-position-horizontal-relative:margin;mso-position-vertical-relative:text;mso-width-percent:0;mso-height-percent:200;mso-width-relative:margin;mso-height-relative:margin;v-text-anchor:top" wrapcoords="-37 -655 -37 21600 21637 21600 21637 -655 -37 -655" o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF&#xA;90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA&#xA;0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD&#xA;OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893&#xA;SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y&#xA;JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl&#xA;bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR&#xA;JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY&#xA;22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i&#xA;OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA&#xA;IQAvK+ErNQIAAGEEAAAOAAAAZHJzL2Uyb0RvYy54bWysVNtu2zAMfR+wfxD0vjpOky416hRt2gwD&#xA;ugvQ7QMUSY6FyaJGKbG7ry8lp212wR6G+UEgReqQPCR9cTl0lu01BgOu5uXJhDPtJCjjtjX/+mX9&#xA;ZsFZiMIpYcHpmj/owC+Xr19d9L7SU2jBKo2MQFyoel/zNkZfFUWQre5EOAGvHRkbwE5EUnFbKBQ9&#xA;oXe2mE4mZ0UPqDyC1CHQ7c1o5MuM3zRaxk9NE3RktuaUW8wn5nOTzmJ5IaotCt8aeUhD/EMWnTCO&#xA;gj5D3Ygo2A7Nb1CdkQgBmngioSugaYzUuQaqppz8Us19K7zOtRA5wT/TFP4frPy4/4zMqJqfnnHm&#xA;REc9Wu2EQmBKs6iHCIwsRFPvQ0Xe957843ANA7U7lxz8HchvgTlYtcJt9RUi9K0WitIs08vi6OmI&#xA;ExLIpv8AisKJXYQMNDTYJQ6JFUbo1K6H5xZRIkzS5Xz+dlpO55xJsp2Wi/J0nkOI6um1xxDfaehY&#xA;EmqONAIZXezvQkzZiOrJJQULYI1aG2uzgtvNyiLbCxqX68VqdTs7oP/kZh3ra34+pzz+DrHO358g&#xA;OhNp7q3par6YpC85iSrRdutUlqMwdpQpZesOPCbqRhLjsBly58r8OJG8AfVAzCKMc057SUIL+IOz&#xA;nma85uH7TqDmzL531J3zcjZLS5GVGTFLCh5bNscW4SRB1TxyNoqrOC7SzqPZthTpaR6uqKNrk8l+&#xA;yeqQP81x7sFh59KiHOvZ6+XPsHwEAAD//wMAUEsDBBQABgAIAAAAIQA3z1Ro4QAAAAwBAAAPAAAA&#xA;ZHJzL2Rvd25yZXYueG1sTI/BTsMwEETvSPyDtUjcqJ2QtijEqSogUignClKv23hJArEd2W6b/j3u&#xA;CY6jfZp5W6wmPbAjOd9bIyGZCWBkGqt600r4/KjuHoD5gEbhYA1JOJOHVXl9VWCu7Mm803EbWhZL&#xA;jM9RQhfCmHPum440+pkdycTbl3UaQ4yu5crhKZbrgadCLLjG3sSFDkd66qj52R60hHWd4Xflnl/T&#xA;8FZXdTi/7DY7IeXtzbR+BBZoCn8wXPSjOpTRaW8PRnk2xLwUSUQlpPMkA3YhRLZcANtLuBfJHHhZ&#xA;8P9PlL8AAAD//wMAUEsBAi0AFAAGAAgAAAAhALaDOJL+AAAA4QEAABMAAAAAAAAAAAAAAAAAAAAA&#xA;AFtDb250ZW50X1R5cGVzXS54bWxQSwECLQAUAAYACAAAACEAOP0h/9YAAACUAQAACwAAAAAAAAAA&#xA;AAAAAAAvAQAAX3JlbHMvLnJlbHNQSwECLQAUAAYACAAAACEALyvhKzUCAABhBAAADgAAAAAAAAAA&#xA;AAAAAAAuAgAAZHJzL2Uyb0RvYy54bWxQSwECLQAUAAYACAAAACEAN89UaOEAAAAMAQAADwAAAAAA&#xA;AAAAAAAAAACPBAAAZHJzL2Rvd25yZXYueG1sUEsFBgAAAAAEAAQA8wAAAJ0FAAAAAA==&#xA;" fillcolor="#b8cce4" strokecolor="white"><v:textbox style="mso-fit-shape-to-text:t"><w:txbxContent><w:p w14:paraId="02E21077" w14:textId="77777777" w:rsidR="007C57B5" w:rsidRPr="0096731A" w:rsidRDefault="007C57B5" w:rsidP="000443C7"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="002060"/><w:sz w:val="28"/></w:rPr></w:pPr><w:r w:rsidRPr="0096731A"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="002060"/><w:sz w:val="28"/></w:rPr><w:t>EQUIPOS DE PROTECCIÓN INDIVIDUAL (EPI)</w:t></w:r></w:p></w:txbxContent></v:textbox><w10:wrap type="through" anchorx="margin"/></v:shape></w:pict></w:r></w:p>';
		//Texto EPIS
		$res.=generaEpis($evaluacion['codigoPuesto']);
		//Titulo y texto evaluaciones
		$res.='<w:p w14:paraId="3C871196" w14:textId="77777777" w:rsidR="00A520D1" w:rsidRDefault="00A520D1" w:rsidP="00412B97"><w:pPr><w:tabs><w:tab w:val="left" w:pos="709"/></w:tabs><w:ind w:left="709"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr></w:pPr></w:p><w:p w14:paraId="13103E17" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRPr="0080542A" w:rsidRDefault="00630389" w:rsidP="00EE6FC3"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:noProof/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:pict w14:anchorId="47B1933F"><v:shape id="_x0000_s1040" type="#_x0000_t202" style="position:absolute;margin-left:-2.45pt;margin-top:15pt;width:438.75pt;height:25.05pt;z-index:-251641856;mso-height-percent:200;mso-height-percent:200;mso-width-relative:margin;mso-height-relative:margin" wrapcoords="-41 -745 -41 20855 21641 20855 21641 -745 -41 -745" fillcolor="#b8cce4" strokecolor="white"><v:textbox style="mso-next-textbox:#_x0000_s1040;mso-fit-shape-to-text:t"><w:txbxContent><w:p w14:paraId="14AA215B" w14:textId="77777777" w:rsidR="007C57B5" w:rsidRPr="00EA2D9C" w:rsidRDefault="007C57B5" w:rsidP="00EE6FC3"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="002060"/><w:sz w:val="28"/></w:rPr></w:pPr><w:r w:rsidRPr="00EA2D9C"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="002060"/><w:sz w:val="28"/></w:rPr><w:t>EVALUACIÓN DE RIESGOS Y PROPUESTA DE MEDIDAS PREVENTIVAS</w:t></w:r></w:p></w:txbxContent></v:textbox><w10:wrap type="through"/></v:shape></w:pict></w:r></w:p><w:p w14:paraId="712AB9FD" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:p w14:paraId="0F2E239D" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>Los riesgos del puesto de XXX se designarán con un número precedido de las letras "P.'.strtoupper($evaluacion['puesto']).'".</w:t></w:r></w:p><w:p w14:paraId="7818E585" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:p w14:paraId="6E2A14F4" w14:textId="77777777" w:rsidR="00EE6FC3" w:rsidRDefault="00EE6FC3" w:rsidP="00EE6FC3"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p>';
		$res.=generaTablasRiesgos($evaluacion['codigoEvaluacion'],strtoupper($evaluacion['puesto']));
		$i++;
	}
	return $res;
}

function generaEpis($codigoPuesto){
	$res='';
	$epis=consultaBD('SELECT epis.nombre FROM epis_de_puesto_trabajo INNER JOIN epis ON epis_de_puesto_trabajo.codigoEPI=epis.codigo WHERE codigoPuestoTrabajo='.$codigoPuesto.' ORDER BY nombre',true);
	while($epi=mysql_fetch_assoc($epis)){
	$entra=true;
	$res.='<w:p w14:paraId="0C7D885F" w14:textId="77777777" w:rsidR="000443C7" w:rsidRPr="000443C7" w:rsidRDefault="000443C7" w:rsidP="00E847A6"><w:pPr><w:numPr><w:ilvl w:val="2"/><w:numId w:val="13"/></w:numPr><w:tabs><w:tab w:val="left" w:pos="709"/></w:tabs><w:ind w:left="709" w:hanging="180"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr></w:pPr><w:r w:rsidRPr="000443C7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="es-ES_tradnl"/></w:rPr><w:t>'.$epi['nombre'].'.</w:t></w:r></w:p>';
	}

	return $res;
}

function generaGráficos($gestion){
	if (file_exists('../img/graficos/image5.png')) {
    	unlink('../img/graficos/image5.png');
	}
	if (file_exists('../img/graficos/image6.png')) {
    	unlink('../img/graficos/image6.png');
	}
	$evaluacion = $gestion['codigoEvaluacion'];
	$riesgos = consultaBD("SELECT * FROM medidas_riesgos_evaluacion_general WHERE codigoGestion=".$gestion['codigo']." LIMIT 1",true,true);
	$riesgos = explode('&$&', $riesgos['codigoRiesgo']);
	$i=0;
	$imagen=5;
	while(isset($riesgos[$i])){
		$riesgo = consultaBD("SELECT * FROM riesgos_evaluacion_general WHERE codigoEvaluacion=".$evaluacion." AND codigoRiesgo=".$riesgos[$i],true, true);
		$Base64Img = base64_decode($riesgo['graficoBarra']);
		//escribimos la información obtenida en un archivo llamado 
		//unodepiera.png para que se cree la imagen correctamente
		file_put_contents('../img/graficos/image'.$imagen.'.png', $Base64Img); 
		$i++;
		$imagen++;
	}
	return $i;
}

function tablaEvaluacion($evaluacion){
	$res = cabeceraTablaUno($evaluacion['fechaEvaluacion']);
		$verde='468847';
		$naranja='F89406';
		$rojo='FF0000';
		$probabilidad=array('0'=>'','1'=>'Remota','2'=>'Inusual','3'=>'Ocasional','4'=>'Frecuente');
		$consecuencias=array('0'=>'','1'=>'Moderada','2'=>'Relevante','3'=>'Grave','4'=>'Catastrófica');
		$consulta=consultaBD("SELECT * FROM riesgos_evaluacion_general INNER JOIN riesgos ON riesgos_evaluacion_general.codigoRiesgo=riesgos.codigo WHERE codigoEvaluacion=".$evaluacion['codigo']." ORDER BY codigoProceso, actividad",true);
		$actividad = '1';
		while($riesgo=mysql_fetch_assoc($consulta)){
			$datosRiesgo=datosRegistro('riesgos',$riesgo['codigoRiesgo']);
			if($actividad == '1'){
				$actividad = $datosRiesgo['actividad'];
			} else if($actividad != $datosRiesgo['actividad']){
				$actividad = $datosRiesgo['actividad'];
				$res.=creaSeparador();
			}
			$colorEnviar='';
			$total='';
			if($riesgo['total']>6){
				$colorEnviar=$rojo;
				$total='INTOLERABLE';
			}elseif($riesgo['total']>=4 && $riesgo['total']<=6){	
				$colorEnviar=$naranja;
				$total='SIGNIFICATIVO';
			}else{
				$colorEnviar=$verde;
				$total='TOLERABLE';
			}
			$res.=creaLineaTablaUno($datosRiesgo['nombre'],$datosRiesgo['descripcion'],$probabilidad[$riesgo['probabilidad']],$consecuencias[$riesgo['consecuencias']],$colorEnviar,$total,$datosRiesgo['codigoProceso'],$datosRiesgo['actividad']);
		}
	$res.='</w:tbl>';

	return $res;
}

function tablaSegunda($gestiones){
	$res = '';
	$codigoGestion=0;
	while($gestion=mysql_fetch_assoc($gestiones)){
	if($codigoGestion == 0){
		$res .= cabeceraTablaDos($gestion['fecha']);
		$codigoGestion = $gestion['codigo'];
	} else if($codigoGestion != $gestion['codigo']){
		$res.='</w:tbl><w:p w14:paraId="0C51166C" w14:textId="77777777" w:rsidR="007E0610" w:rsidRDefault="007E0610" w:rsidP="007E0610"><w:pPr><w:pStyle w:val="ListParagraph"/><w:ind w:left="360"/><w:rPr><w:sz w:val="24"/></w:rPr></w:pPr><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p><w:p w14:paraId="4B162B5F" w14:textId="26DA1971" w:rsidR="00D23046" w:rsidRDefault="00D23046"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/></w:pPr><w:r><w:br w:type="page"/></w:r></w:p><w:p w14:paraId="180ECA09" w14:textId="14BEAB53" w:rsidR="00D26F43" w:rsidRDefault="00D26F43"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/></w:pPr></w:p><w:p w14:paraId="71BF410D" w14:textId="77777777" w:rsidR="00D26F43" w:rsidRDefault="00D26F43"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/></w:pPr></w:p>';
		$res.= cabeceraTablaDos($gestion['fecha']);
		$codigoGestion = $gestion['codigo'];
	}
		$verde='468847';
		$naranja='F89406';
		$rojo='FF0000';
		$plazos=array('6'=>'6 meses','12'=>'12 meses','18'=>'18 meses','1'=>'Otros');
		$evaluacion=datosRegistro('evaluacion_general',$gestion['codigoEvaluacion']);
		$consulta=consultaBD("SELECT * FROM medidas_riesgos_evaluacion_general WHERE codigoGestion=".$gestion['codigo'],true);
		while($riesgo=mysql_fetch_assoc($consulta)){
			$responsables='';
			if($riesgo['responsable'] != '' ){
				$listado = explode('&$&', $riesgo['responsable']);
				$i=0;
				while(isset($listado[$i])){
					$responsable=datosRegistro('personal',$listado[$i]);
					if($i == 0){
						$responsables .= $responsable['nombre'].' '.$responsable['apellidos'];
					} else {
						$responsables .= ','. $responsable['nombre'].' '.$responsable['apellidos'];
					}
					$i++;
				}
			} else {
				$listado = explode('&$&', $riesgo['area']);
				$i=0;
				while(isset($listado[$i])){
					$responsable=datosRegistro('puestosTrabajo',$listado[$i]);
					if($i == 0){
						$responsables .= $responsable['funcion'];
					} else {
						$responsables .= ','. $responsable['funcion'];
					}
					$i++;
				}
			}
			$riesgosExplode=explode('&$&',$riesgo['codigoRiesgo']);
			$riesgoDatos=consultaBD("SELECT total FROM riesgos_evaluacion_general WHERE codigoEvaluacion='".$evaluacion['codigo']."';",true,true);
			foreach($riesgosExplode AS $valor){
				$datosRiesgo=datosRegistro('riesgos',$valor);
				$colorEnviar='';
				$total='';
				if($riesgoDatos['total']>6){
					$colorEnviar=$rojo;
					$total='INTOLERABLE';
				}elseif($riesgoDatos['total']>=4 && $riesgoDatos['total']<=6){	
					$colorEnviar=$naranja;
					$total='SIGNIFICATIVO';
				}else{
					$colorEnviar=$verde;
					$total='TOLERABLE';
				}
				$res.=creaLineaTablaDos($datosRiesgo['nombre'],$riesgo['recomendacion'],$plazos[$riesgo['plazo']],$responsables,$riesgo['checkEjecutada'],$colorEnviar,$total);
			}	
		}
	}
	$res.='</w:tbl>';
	return $res;
}

function obtieneGraficosRiesgos($codigoEvaluacion){
	$res=array();

	$consulta=consultaBD("SELECT riesgos.codigo, descripcion, graficoBarra, nombre FROM riesgos_evaluacion_general INNER JOIN riesgos ON riesgos_evaluacion_general.codigoRiesgo=riesgos.codigo WHERE codigoEvaluacion='$codigoEvaluacion' ORDER BY nombre;");
	$i=5;//El contador empieza en 5 porque dentro de la estructura interna del Word, la primera imagen a sustituir por un gráfico se llama "image5".
	$j=1;//El contador j sirve para la numeración de los gráficos en el informe de Conclusiones (8.1, 8.2, etc) y para la correlación con el informe de Recomendaciones
	while($datos=mysql_fetch_assoc($consulta)){
		array_push($res,creaImagenGraficoRiesgoDelito($datos,$i,$j));
		$i++;
		$j++;
	}

	//Parte de gráfico global
	$datos=consultaBD("SELECT graficoBarraGlobal FROM evaluacion_general WHERE codigo='$codigoEvaluacion';",false,true);
	$imagen=base64_decode($datos['graficoBarraGlobal']);
	file_put_contents("../documentos/conclusiones/image4.png",$imagen);
	$res['graficoGlobal']='image4.png';
	//Fin parte gráfico global

	return $res;
}

/*
	La siguiente función crea la imagen a partir de la información almacenada en la BDD, 
	y devuelve un array con la forma: numero - delito - grafico - descripcion
	
	Los índices numero, delito, grafico y descripcion se usan para insertar los gráficos en el documento.
	Los índices código y numero se usan para la correlación de las recomendaciones y los delitos en el Informe de Recomendaciones.
*/
function creaImagenGraficoRiesgoDelito($datos,$i,$j){
	$imagen=base64_decode($datos['graficoBarra']);
	file_put_contents("../documentos/conclusiones/image$i.png",$imagen);

	return array('codigoRiesgo'=>$datos['codigo'],'numero'=>"8.$j",'nombre'=>utf8_decode($datos['nombre']),'imagen'=>"image$i.png",'descripcion'=>utf8_decode($datos['descripcion']));
}


function insertaGraficosDelitosDocumento($graficosDelitos,$documento){	
	$i=1;
	foreach ($graficosDelitos as $grafico){
		if($grafico=='image4.png'){//Gráfico global
			$documento->replaceImage('../documentos/conclusiones/',$grafico);
		}
		elseif($grafico['imagen']!='image34.png'){
			$documento->setValue("tituloGrafico$i","8.$i ".$grafico['nombreDelito']);
			$documento->replaceImage('../documentos/conclusiones/',$grafico['imagen']);
			$documento->setValue("descripcionGrafico$i",$grafico['descripcionRiesgo']);
			$documento->setValue("departamentosGrafico$i",$grafico['departamentosGrafico']);
			$i++;
		}
	}
}

function obtieneCalculoRecomendacionesGestion($codigoGestion){
	$res=array('numMedidas'=>0,'numPreven'=>0,'numMitiga'=>0,'numSeis','numDoce'=>0,'numDieciocho'=>0,'grafico0'=>0,'grafico1'=>0,'grafico2'=>0,'grafico3'=>0,'grafico4'=>0,'grafico5'=>0);
	
	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='$codigoGestion';",false,true);
	$res['numMedidas']=$consulta['total'];

	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='$codigoGestion' AND area IN(0,1,2,3,4);",false,true);
	$res['numPreven']=$consulta['total'];

	$res['numMitiga']=$res['numMedidas']-$res['numPreven'];//Así me ahorro una consulta

	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='$codigoGestion' AND plazo='6';",false,true);
	$res['numSeis']=$consulta['total'];

	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='$codigoGestion' AND plazo='12';",false,true);
	$res['numDoce']=$consulta['total'];

	$res['numDieciocho']=$res['numMedidas']-$res['numSeis']-$res['numDoce'];//Y así me ahorro otra consulta :3

	//Para la leyenda del gráfico circular dentro del Word
	for($i=0;$i<6;$i++){
		$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='$codigoGestion' AND area IN($i);",false,true);
		$res['grafico'.$i]=$consulta['total'];
	}

	return $res;
}

function obtieneMes($num){
	$num--;
	$meses=array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	return $meses[(int)$num];//Hago el casting a int para quitar el 0 de la izquierda (los meses vienen: 01, 02...).
}

function obtieneTablasRecomendaciones($codigoGestion,$datosDelitos){//El sentido del parámetro $graficosDelitos viene explicado en el comentario de la funcón creaImagenGraficoRiesgoDelito
	$res='';
	$imagenes=array('6'=>'rId11','12'=>'rId12','18'=>'rId13');//Este array tiene como claves el número de meses que tiene de plazo cada recomendación para ser implantada, y como valor el ID interno que le asigna Word a cada una de las 3 imágenes en forma de señal con exclamación (roja, amarilla y verde respectivamente).
	$consulta=consultaBD("SELECT medidas_riesgos_evaluacion_general.*, descripcionRiesgo, medidas_riesgos_evaluacion_general.codigoRiesgo, riesgos.nombre AS riesgo, area, responsable FROM ((gestion_evaluacion_general INNER JOIN medidas_riesgos_evaluacion_general ON gestion_evaluacion_general.codigo=medidas_riesgos_evaluacion_general.codigoGestion) INNER JOIN riesgos_evaluacion_general ON gestion_evaluacion_general.codigoEvaluacion=riesgos_evaluacion_general.codigoEvaluacion) INNER JOIN riesgos ON riesgos_evaluacion_general.codigoRiesgo=riesgos.codigo WHERE codigoGestion='$codigoGestion' GROUP BY medidas_riesgos_evaluacion_general.codigo;");


	while($datos=mysql_fetch_assoc($consulta)){
		$res.=creaTablaRecomendaciones($datos,$imagenes[$datos['plazo']],$datosDelitos);
	}



	return $res;
}

function creaTablaRecomendaciones($datos,$imagen,$datosDelitos){
	$numero=obtieneNumeroCorrelacionDelito($datos['codigoRiesgo'],$datosDelitos);
	$area = consultaBD("SELECT funcion FROM puestosTrabajo WHERE codigo=".$datos['area'],true,true);

	return '<w:tbl><w:tblPr><w:tblStyle w:val="Tablaconcuadrcula"/><w:tblW w:w="9634" w:type="dxa"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="1685"/><w:gridCol w:w="1267"/><w:gridCol w:w="3797"/><w:gridCol w:w="1751"/><w:gridCol w:w="1134"/></w:tblGrid>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="535"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t xml:space="preserve">'.utf8_decode('Riesgo').'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="5121" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode('Descripción').'</w:t></w:r></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidTr="00F825E7"><w:trPr><w:trHeight w:val="70"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($datos['riesgo']).'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="5121" w:type="dxa"/><w:gridSpan w:val="2"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($datos['descripcionRiesgo']).'</w:t></w:r></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidTr="00F825E7"><w:trPr><w:trHeight w:val="290"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1278" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="3843" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1134" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00DD7126" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="996"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>Area</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="7956" w:type="dxa"/><w:gridSpan w:val="4"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($area['funcion']).'</w:t></w:r></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00DD7126" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="1123"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>Responsable</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="7956" w:type="dxa"/><w:gridSpan w:val="4"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($datos['responsable']).'</w:t></w:r></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00DD7126" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="1376"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode('Recomendación').'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="7956" w:type="dxa"/><w:gridSpan w:val="4"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($datos['recomendacion']).'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00DD7126" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="1376"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode('Prioridad').'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1134" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="center"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-ES" w:eastAsia="es-ES"/></w:rPr><w:drawing><wp:inline distT="0" distB="0" distL="0" distR="0" wp14:anchorId="6F693A28" wp14:editId="32E90C64"><wp:extent cx="409575" cy="409575"/><wp:effectExtent l="0" t="0" r="9525" b="9525"/><wp:docPr id="2" name="Picture 2"/><wp:cNvGraphicFramePr><a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" noChangeAspect="1"/></wp:cNvGraphicFramePr><a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"><a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:nvPicPr><pic:cNvPr id="1" name="200px-Gtk-dialog-warning.svg_[1].png"/><pic:cNvPicPr/></pic:nvPicPr><pic:blipFill><a:blip r:embed="'.$imagen.'" cstate="print"><a:extLst><a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}"><a14:useLocalDpi xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" val="0"/></a:ext></a:extLst></a:blip><a:stretch><a:fillRect/></a:stretch></pic:blipFill><pic:spPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="409575" cy="409575"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom></pic:spPr></pic:pic></a:graphicData></a:graphic></wp:inline></w:drawing></w:r></w:p><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
				</w:tr>
			</w:tbl><w:p w14:paraId="287010F2" w14:textId="77777777" w:rsidR="00E70E2C" w:rsidRDefault="00E70E2C"/><w:p w14:paraId="287010F2" w14:textId="77777777" w:rsidR="00E70E2C" w:rsidRDefault="00E70E2C"/>';

			
}

function creaTablaRecomendaciones2($datos,$imagen,$datosDelitos){
	$numero=obtieneNumeroCorrelacionDelito($datos['codigoRiesgo'],$datosDelitos);

	return '<w:tbl><w:tblPr><w:tblStyle w:val="Tablaconcuadrcula"/><w:tblW w:w="9634" w:type="dxa"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="1685"/><w:gridCol w:w="1267"/><w:gridCol w:w="3797"/><w:gridCol w:w="1751"/><w:gridCol w:w="1134"/></w:tblGrid>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="535"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t xml:space="preserve">'.utf8_decode('Correlación').'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="5121" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode('Descripción').'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>Responsabilidad Penal (Si/No)</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1134" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>Prioridad</w:t></w:r></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidTr="00F825E7"><w:trPr><w:trHeight w:val="70"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.$numero.'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="5121" w:type="dxa"/><w:gridSpan w:val="2"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($datos['descripcionRiesgo']).'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t></w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1134" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="center"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-ES" w:eastAsia="es-ES"/></w:rPr><w:drawing><wp:inline distT="0" distB="0" distL="0" distR="0" wp14:anchorId="6F693A28" wp14:editId="32E90C64"><wp:extent cx="409575" cy="409575"/><wp:effectExtent l="0" t="0" r="9525" b="9525"/><wp:docPr id="2" name="Picture 2"/><wp:cNvGraphicFramePr><a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" noChangeAspect="1"/></wp:cNvGraphicFramePr><a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"><a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:nvPicPr><pic:cNvPr id="1" name="200px-Gtk-dialog-warning.svg_[1].png"/><pic:cNvPicPr/></pic:nvPicPr><pic:blipFill><a:blip r:embed="'.$imagen.'" cstate="print"><a:extLst><a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}"><a14:useLocalDpi xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" val="0"/></a:ext></a:extLst></a:blip><a:stretch><a:fillRect/></a:stretch></pic:blipFill><pic:spPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="409575" cy="409575"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom></pic:spPr></pic:pic></a:graphicData></a:graphic></wp:inline></w:drawing></w:r></w:p><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidTr="00F825E7"><w:trPr><w:trHeight w:val="290"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1278" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="3843" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1134" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00DD7126" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="996"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>Riesgo</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="7956" w:type="dxa"/><w:gridSpan w:val="4"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($datos['riesgo']).'</w:t></w:r></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00DD7126" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="1123"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode('Consecuencia Jurídica').'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="7956" w:type="dxa"/><w:gridSpan w:val="4"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t></w:t></w:r></w:p><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00DD7126" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="1376"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode('Recomendación').'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="7956" w:type="dxa"/><w:gridSpan w:val="4"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($datos['recomendacion']).'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
				</w:tr>
			</w:tbl><w:p w14:paraId="287010F2" w14:textId="77777777" w:rsidR="00E70E2C" w:rsidRDefault="00E70E2C"/><w:p w14:paraId="287010F2" w14:textId="77777777" w:rsidR="00E70E2C" w:rsidRDefault="00E70E2C"/>';
}

function obtieneNumeroCorrelacionDelito($codigoDelito,$datosDelitos){
	$res='';
	foreach ($datosDelitos as $delito) {
		if(isset($delito['codigoDelito']) && $delito['codigoDelito']==$codigoDelito){
			$res=$delito['numero'];
		}
	}

	return $res;
}

function obtieneGestiones(){
	$res='';
	$datos=arrayFormulario();
	$res="<option value='NULL'></option>";
		
	$consulta=consultaBD("SELECT DISTINCT g.codigo, DATE_FORMAT(fecha,'%d/%m/%Y') AS fecha FROM gestion_evaluacion_general g INNER JOIN medidas_riesgos_evaluacion_general m ON g.codigo=m.codigoGestion WHERE g.codigoEvaluacion=".$datos['codigoEvaluacion'],true);
	
	while($datos=mysql_fetch_assoc($consulta)){
		$res.="<option value='".$datos['codigo']."'>".$nombresRiesgos." - ".$datos['fecha']."</option>";
	}

	echo $res;
}

function cabeceraTablaUno($fechaEvaluacion){
	return '<w:tbl>
	<w:tblPr>
		<w:tblW w:w="13990" w:type="dxa"/>
		<w:tblInd w:w="40" w:type="dxa"/>
		<w:tblLayout w:type="fixed"/>
		<w:tblCellMar>
			<w:left w:w="70" w:type="dxa"/>
			<w:right w:w="70" w:type="dxa"/>
		</w:tblCellMar>
		<w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/>
	</w:tblPr>
	<w:tblGrid>
		<w:gridCol w:w="1207"/>
		<w:gridCol w:w="2653"/>
		<w:gridCol w:w="2974"/>
		<w:gridCol w:w="1717"/>
		<w:gridCol w:w="2961"/>
		<w:gridCol w:w="1651"/>
		<w:gridCol w:w="827"/>
	</w:tblGrid>
	<w:tr w:rsidR="008A343D" w:rsidRPr="00C52182" w:rsidTr="00AA4A38">
		<w:trPr>
			<w:gridAfter w:val="2"/>
			<w:wAfter w:w="2478" w:type="dxa"/>
			<w:trHeight w:val="386"/>
		</w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1207" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="5627" w:type="dxa"/>
				<w:gridSpan w:val="2"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>RIESGOS EMPRESARIALES</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1717" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2961" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="8" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
		</w:tc>
	</w:tr>
	<w:tr w:rsidR="008A343D" w:rsidRPr="00C52182" w:rsidTr="00AA4A38">
		<w:trPr>
			<w:trHeight w:val="372"/>
		</w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1207" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="8" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>PROCESO</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2653" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="8" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>ACTIVIDAD</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2974" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="8" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>RIESGO</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="4678" w:type="dxa"/>
				<w:gridSpan w:val="2"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="8" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>DESCRIPCIÓN</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1651" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="4" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>VALORACIÓN</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1827" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="8" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>PUNT.</w:t>
				</w:r>
			</w:p>
		</w:tc>
	</w:tr>
	<w:tr w:rsidR="008A343D" w:rsidRPr="00C52182" w:rsidTr="00AA4A38">
		<w:trPr>
			<w:trHeight w:val="264"/>
		</w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1207" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2653" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="right"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t xml:space="preserve">Fecha Valoración: </w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2974" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="006831E1" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="006831E1">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.formateaFechaWeb($fechaEvaluacion).'</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="4678" w:type="dxa"/>
				<w:gridSpan w:val="2"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1651" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="827" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="4" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
		</w:tc>
	</w:tr>';
}

function creaLineaTablaUno($riesgo, $descripcion, $probabilidad, $consecuencia, $color, $total, $proceso, $actividad){
	$proceso=datosRegistro('indicadores',$proceso);
	$proceso=$proceso['nombre'];
	return '<w:tr w:rsidR="008A343D" w:rsidRPr="00C52182" w:rsidTr="00AA4A38">
		<w:trPr>
			<w:trHeight w:val="255"/>
		</w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1207" w:type="dxa"/>
				<w:vMerge w:val="restart"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.$proceso.'</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2653" w:type="dxa"/>
				<w:vMerge w:val="restart"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.$actividad.'</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2974" w:type="dxa"/>
				<w:vMerge w:val="restart"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.$riesgo.'</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="4678" w:type="dxa"/>
				<w:gridSpan w:val="2"/>
				<w:vMerge w:val="restart"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.$descripcion.'</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1651" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>Probabilidad</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="827" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="right"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.$probabilidad.'</w:t>
				</w:r>
			</w:p>
		</w:tc>
	</w:tr>
	<w:tr w:rsidR="008A343D" w:rsidRPr="00C52182" w:rsidTr="00AA4A38">
		<w:trPr>
			<w:trHeight w:val="255"/>
		</w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1207" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2653" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2974" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="4678" w:type="dxa"/>
				<w:gridSpan w:val="2"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1651" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>Consecuencia</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="827" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="right"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:proofErr w:type="spellStart"/>
				<w:r>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.$consecuencia.'</w:t>
				</w:r>
				<w:proofErr w:type="spellEnd"/>
			</w:p>
		</w:tc>
	</w:tr>
	<w:tr w:rsidR="008A343D" w:rsidRPr="00C52182" w:rsidTr="00AA4A38">
		<w:trPr>
			<w:trHeight w:val="255"/>
		</w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1207" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2653" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2974" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="4678" w:type="dxa"/>
				<w:gridSpan w:val="2"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1651" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="827" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="right"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
	</w:tr>
	<w:tr w:rsidR="008A343D" w:rsidRPr="00C52182" w:rsidTr="00AA4A38">
		<w:trPr>
			<w:trHeight w:val="191"/>
		</w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1207" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2653" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2974" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="4678" w:type="dxa"/>
				<w:gridSpan w:val="2"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1651" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="right"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>TOTAL</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="827" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="000000" w:fill="'.$color.'"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="right"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.$total.'</w:t>
				</w:r>
				<w:bookmarkStart w:id="0" w:name="_GoBack"/>
				<w:bookmarkEnd w:id="0"/>
			</w:p>
		</w:tc>
	</w:tr>';
}


function cabeceraTablaDos($fechaEvaluacion){
	return '<w:tbl>
			<w:tblPr>
				<w:tblW w:w="14053" w:type="dxa"/>
				<w:tblBorders>
					<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
					<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
					<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
					<w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
					<w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
				</w:tblBorders>
				<w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/>
			</w:tblPr>
			<w:tblGrid>
				<w:gridCol w:w="2286"/>
				<w:gridCol w:w="1914"/>
				<w:gridCol w:w="3388"/>
				<w:gridCol w:w="944"/>
				<w:gridCol w:w="271"/>
				<w:gridCol w:w="1289"/>
				<w:gridCol w:w="1523"/>
				<w:gridCol w:w="2438"/>
			</w:tblGrid>
			<w:tr w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidTr="00886D27">
				<w:trPr>
					<w:trHeight w:val="626"/>
				</w:trPr>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="7588" w:type="dxa"/>
						<w:gridSpan w:val="3"/>
						<w:tcBorders>
							<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						</w:tcBorders>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
							</w:rPr>
							<w:t>MEDIDAS PARA LA GESTIÓN DE RIESGOS</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="1215" w:type="dxa"/>
						<w:gridSpan w:val="2"/>
						<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:sz w:val="28"/>
							</w:rPr>
						</w:pPr>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="5250" w:type="dxa"/>
						<w:gridSpan w:val="3"/>
						<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
								<w:sz w:val="28"/>
							</w:rPr>
							<w:t>FECHA</w:t>
						</w:r>
						<w:r w:rsidRPr="006831E1">
							<w:rPr>
								<w:b/>
								<w:sz w:val="28"/>
							</w:rPr>
							<w:t>: '.formateaFechaWeb($fechaEvaluacion).'</w:t>
						</w:r>
					</w:p>
				</w:tc>
			</w:tr>
			<w:tr w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidTr="00886D27">
				<w:trPr>
					<w:trHeight w:val="412"/>
				</w:trPr>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="2286" w:type="dxa"/>
						<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
							<w:t>RIESGO</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="1914" w:type="dxa"/>
						<w:tcBorders>
							<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						</w:tcBorders>
						<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
							<w:t>NIVEL DE RIESGO</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="4332" w:type="dxa"/>
						<w:gridSpan w:val="2"/>
						<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
							<w:t>RECOMENDACIÓN</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="1560" w:type="dxa"/>
						<w:gridSpan w:val="2"/>
						<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
							<w:t>PLAZO</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="1523" w:type="dxa"/>
						<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
							<w:t>RESPONSABLE</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="2438" w:type="dxa"/>
						<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
							<w:t>EJECUTADA</w:t>
						</w:r>
					</w:p>
				</w:tc>
			</w:tr>';
}

function creaLineaTablaDos($riesgo,$recomendacion,$plazo,$responsable,$ejecutada,$color,$total){
	return '<w:tr w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidTr="0056060A">
				<w:trPr>
					<w:trHeight w:val="1956"/>
				</w:trPr>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="2286" w:type="dxa"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="0048154A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
							</w:rPr>
						</w:pPr>
						<w:proofErr w:type="spellStart"/>
						<w:r>
							<w:rPr>
								<w:b/>
							</w:rPr>
							<w:t>'.$riesgo.'</w:t>
						</w:r>
						<w:proofErr w:type="spellEnd"/>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="1914" w:type="dxa"/>
						<w:shd w:val="clear" w:color="auto" w:fill="'.$color.'"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="0048154A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
						</w:pPr>
						<w:r>
							<w:t>'.$total.'</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="4332" w:type="dxa"/>
						<w:gridSpan w:val="2"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="0048154A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
						</w:pPr>
						<w:r>
							<w:t>'.$recomendacion.'</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="1560" w:type="dxa"/>
						<w:gridSpan w:val="2"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="0048154A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
						</w:pPr>
						<w:proofErr w:type="spellStart"/>
						<w:r>
							<w:t>'.$plazo.'</w:t>
						</w:r>
						<w:proofErr w:type="spellEnd"/>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="1523" w:type="dxa"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="0048154A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
						</w:pPr>
						<w:r>
							<w:t>'.$responsable.'</w:t>
						</w:r>
						<w:bookmarkStart w:id="0" w:name="_GoBack"/>
						<w:bookmarkEnd w:id="0"/>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="2438" w:type="dxa"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="0048154A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
						</w:pPr>
						<w:r>
							<w:t>'.$ejecutada.'</w:t>
						</w:r>
					</w:p>
				</w:tc>
			</w:tr>';
}

function creaSeparador(){
	return '<w:tr w:rsidR="00AD20F0" w:rsidRPr="00E1407E" w14:paraId="29C97D53" w14:textId="77777777" w:rsidTr="00AD20F0"><w:trPr><w:trHeight w:val="255"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="3604" w:type="dxa"/><w:gridSpan w:val="2"/><w:tcBorders><w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9" w:themeFill="background1" w:themeFillShade="D9"/><w:noWrap/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="2204E78D" w14:textId="6C401450" w:rsidR="00AD20F0" w:rsidRDefault="0023752D" w:rsidP="00A80799"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t></w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2771" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9" w:themeFill="background1" w:themeFillShade="D9"/><w:noWrap/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="3133E3BD" w14:textId="77777777" w:rsidR="00AD20F0" w:rsidRDefault="00AD20F0" w:rsidP="00A80799"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4364" w:type="dxa"/><w:gridSpan w:val="2"/><w:tcBorders><w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9" w:themeFill="background1" w:themeFillShade="D9"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="2EB4A97F" w14:textId="77777777" w:rsidR="00AD20F0" w:rsidRDefault="00AD20F0" w:rsidP="00EE7A63"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1544" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:left w:val="nil"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9" w:themeFill="background1" w:themeFillShade="D9"/><w:noWrap/><w:vAlign w:val="bottom"/></w:tcPr><w:p w14:paraId="63D72CD3" w14:textId="77777777" w:rsidR="00AD20F0" w:rsidRPr="00E1407E" w:rsidRDefault="00AD20F0" w:rsidP="00EE7A63"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1707" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:left w:val="nil"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9" w:themeFill="background1" w:themeFillShade="D9"/><w:noWrap/><w:vAlign w:val="bottom"/></w:tcPr><w:p w14:paraId="50DF83A0" w14:textId="2A19F898" w:rsidR="00AD20F0" w:rsidRDefault="0023752D" w:rsidP="00EE7A63"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="right"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t></w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p></w:tc></w:tr>';
}

/*
function generaPDF($codigo){
	global $_CONFIG;

	$datos=consultaBD("SELECT informes.fecha, formulario, informes.codigoEvaluacion, imagen
					   FROM informes INNER JOIN evaluacion_general ON informes.codigoEvaluacion=evaluacion_general.codigo
					   INNER JOIN formularioPRL ON evaluacion_general.codigoCliente=formularioPRL.codigoCliente
					   INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo
					   WHERE informes.codigo='$codigo' ORDER BY formularioPRL.fecha DESC;",true,true);

	$formulario=formateaPreguntasFormulario($datos['formulario']);
	$logoCliente='';
	if($datos['imagen']!='' && $datos['imagen']!='NO'){
		$logoCliente="<img src='../clientes/imagenes/".$datos['imagen']."' class='logoCliente' />";
	}

    $contenido = "
	<style type='text/css'>
	<!--
			html{
				margin:0px;
				padding:0px;
				width:100%;
			}
	        body{
	            font-size:12px;
	            font-family: Arial;
	            font-weight: lighter;
	            line-height: 24px;
	            margin:0px;
				padding:0px;
				width:100%;

	        }

	        li{
	        	padding-bottom:10px;
	        	line-height:20px;
	        }

	        .header{
	        	padding-left:50px;
	        	height:50px;
	        	float:left;
	        }

	        .container{
	        	width:100%;
	        	float:left;
	        }

	        .content{
	        	width:80%;
	        	margin-left:60px;
	        }

	        table{
	        	border:1px solid #000;
	        	border-collapse: collapse;
	        	width:100%;
	        }

	        table td,
	        table th{
	        	border:1px solid #000;
	        	font-size: 12px;
	        	padding:5px;
	        	width:100%;
	        	text-align:center;
	        }

	        .col2 td,
	        .col2 th{
	        	width:50%;
	        }

	        .col3 td,
	        .col3 th{
	        	width:33.3%;
	        }

	        .col4 td,
	        .col4 th{
	        	width:25%;
	        }

	        .col5 td,
	        .col5 th{
	        	width:20%;
	        }

	        table th{
	        	background:#999999;
	        }

	        .blue th{
	        	background:rgb(211,222,238);
	        }

	        .conclusion td{
	        	text-align:left;
	        	font-weight:bold;
	        }

	        .conclusion .impar{
	        	background:rgb(211,222,238);
	        }

	        hr{
	        	color:rgb(211,222,238);
	        }
	        
	        .title{
	        	font-size:14px;
	        	font-weight:bold;
	        	width:100%;
	        	color:#02215c;
	        }

	        .subtitle{
	        	margin-left:15px;
	        }

	        .ficheros{
	        	width:100%;
	      		line-height:18px;
	        	border:0px !important;
	        }

	        .ficheros td{
	        	border:0px;
	        	border-bottom:1 dashed #000;
	        	padding:5px 0px;
	        	text-align:left;
	        }

	        .ficheros .tituloFichero{
	        	width:30%;
	        }

	        .ficheros .descFichero{
	        	width:70%;
	        }

	        .calificacion{
	        	width:25px;
	        	height:22px;
	        	background:rgb(11,89,179);
	        	border:1px solid #000;
	        	padding:12px;
	        	margin-top:5px; 
	        	margin-left:350px; 
	        	margin-bottom:5px; 
	        	margin-right:0px;
	        	position:relative;
	        }

	        .calificacion .inner{
	        	background:rgb(211,222,238);
	        	position:absolute;
	        	top:10px;
	        	width:30px;
	        	left:10px;
	        	border:1px solid #000;
	        	text-align:center;
	        	font-size:24px;
	        }

	        .cajaPie{
	            width:100%;
	            height:auto;
	            position:relative;
	            margin-top:50px;
	            color:#333;
	            padding:10px;
	            padding-top:0px;
	            font-size:10px;
	            text-align:center;
	        }

	        .cajaPiePortada{
	            width:100%;
	            height:auto;
	            position:relative;
	            color:#333;
	            padding:10px;
	            padding-top:0px;
	            font-size:10px;
	            text-align:right;
	            margin-bottom:20px;
	        }

	        .cajaPie .left{
	        	text-align:left;
	        }

	        .titlePortada{
	        	text-align:center;
	        	font-size:48px;
	        	color:#02215c;
	        	font-weight:bold;
	        	margin-top:240px;
	        	line-height:64px;
	        }

	        .textPortada{
	        	margin-top:0px;
	        	font-size:26px;
	        	color:#0C519A;
	        	z-index:10;
	        	position:absolute;
	        	top:100px;
	        	left:50px;
	        	width:580px;
	        	text-align:center;
	        }

	        .razonPortada{
	        	font-size:26px;
	        	z-index:10;
	        	position:absolute;
	        	top:350px;
	        	left:50px;
	        	margin:0px;
	        	width:580px;
	        }	  	       

	        .fondoPortada{
	        	z-index:1;
	        	width:100%;
	        	margin-top:-15px;
	        }

	        .fechaPortada{
	        	margin-top:50px;
	        	text-align:center;
	        } 

	        .certificado{
	        	line-height:20px;
	        }

	        .firma{
	        	float:right;
	        }

	        ol.indice{
	        	margin-top:20px;
	        	margin-left:100px;
	        }

	        ol.indice li{
	        	padding: 20px;
	        	color:#02215c;
	        }

	        table.historico .a15{
	        	width:15%;
	        	text-align:center;
	        }

	        table.historico .a70{
	        	width:70%;
	        	text-align:center;
	        }

	        table.historico th{
	        	background:rgb(92,147,101);
	        	color:#FFF;
	        }

	        table.historico .even td{
	        	background:rgb(229,239,217);
	        }

	        table .a10{
	        	width:10%;
	        }

	        table .a15{
	        	width:15%;
	        }

	        table .a20{
	        	width:20%;
	        }

	        table .a25{
	        	width:25%;
	        }

	        table .a30{
	        	width:30%;
	        }

	        table .a40{
	        	width:40%;
	        }

	        table .a70{
	        	width:70%;
	        }

	        table .a75{
	        	width:75%;
	        }

	        table .a80{
	        	width:80%;
	        }

	        table .a100{
	        	width:100%;
	        	background:rgb(216,216,216);
	        }

	        table.evaluacion .primeraCabecera .uno{
	        	background:transparent;
	        	text-align:left;
	        }

	        table.evaluacion .primeraCabecera .dos{
	        	background:transparent;
	        	border-top:0px;
	        	border-right:0px;
	        }

	        table.evaluacion .segundaCabecera th{
	        	background:rgb(58,102,102);;
	        	color:#FFF;
	        	font-weight:bold;
	        }

	        table.evaluacion .terceraCabecera .uno{
	        	background:rgb(95,151,104);
	     		text-align:right;
	        }

	        table.evaluacion .terceraCabecera .dos{
	        	background:rgb(95,151,104);
	     		text-align:left;
	        }

	        table.evaluacion .right{
	        	text-align:right;
	        }

	        table .naranja{
	        	background:#F89406;
	        }

	        table .rojo{
	        	background:#FF0000;
	        	color:#fff;
	        }

	        table .amarillo{
	        	background:#fff584;
	        }

	        table .blanco{
	        	background:#f9f9f9;
	        }

	        table .negro{
	        	background:#000;
	        	color:#fff;
	        }

	        table.gestion .primeraCabecera .uno{
	        	background:transparent;
	        	text-align:center;
	        }	

	        table.gestion .primeraCabecera .dos{
	        	background:rgb(95,151,104);
	        	text-align:center;
	        }

	        table.gestion .segundaCabecera th{
	        	background:rgb(58,102,102);;
	        	color:#FFF;
	        	font-weight:bold;
	        }		

	        img.graficoBarra{
	        	height:70%;
	        }

	        .imagenFondo{
	        	position:absolute;
	        	top:0;
	        	z-index:1;
	        }

	        .imagenFondo img{
	        	width:100%;
	        	display:inline-block;
	        }

	        .nombreEmpresa{
	        	font-size:36px;
	        	color:#02215c;
	        	text-align:center;
	        	margin-top:50px;
	        	width:100%;
	        	line-height:48px;
	        }

	        .tituloIndice{
	        	text-align:center;
	        	font-size:16px;
	        	color:#02215c;
	        }

	        .cabecera{
	        	border-top:1px solid #02215c;
	        	border-bottom:1px solid #02215c;
	        	padding:10px;
	        	margin-bottom:200px;
	        }

	        .cabecera img{
	        	height:50px;
	        }

	        .cabecera .tituloCabecera{
	        	color:#02215c;
	        	text-align:center;
	        	width:50%;
	        	font-weight:bold;
	        	margin-left:100px;
	        	margin-top:-10px;
	        }

	        .cabecera .logoCliente{
	        	position:absolute;
	        	left:600px;
	        	top:30px;
	        	margin-bottom:-40px;
	        }

	        .cajaRevision{
	        	color:#02215c;
	        	margin-left:80px;
	        }

	        .cajaNumeroPagina{
	        	color:#02215c;
	        	position:absolute;
	        	right:80px;
	        	bottom:0px;	
	        }

	        .imagenFondo2{
	        	position:absolute;
	        	top:0;
	        	z-index:1;
	        }

	        .imagenFondo2 img{
	        	width:100%;
	        	display:inline-block;
	        }

	        .justificado{
	        	text-align:justify;
	        	line-height:18px;
	        }

	        .justificado strong{
	        	color:#02215c;
	        }

	        .listaCuadros{
	        	list-style-type: square;
	        }

	        .listaCuadros li{
	        	padding:10px;
	        	text-align:justify;
	        }

	        .tituloCentrado{
	        	text-align:center;
	        	color:#02215c;
	        	font-weight:bold;
	        }

	        .listaLetras{
	        	list-style-type: lower-alpha;
	        	margin-top:-10px;
	        }
	-->
	</style>
	

	<page backbottom='27mm' backleft='0mm' backright='0mm' >
		<div class='imagenFondo'><img src='../img/fondoOferta.jpeg' alt='Anesco' /></div>
		<div class='titlePortada'>EVALUACIÓN DE<br/>RIESGOS</div>

		<div class='nombreEmpresa'>".$formulario['pregunta0']."</div>

		<div class='nombreEmpresa'>".$formulario['pregunta6']."</div>

	</page>";

	$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm'>
		<page_header>
			<div class='cabecera'>
				<img src='../img/logoOferta.png' />
				<span class='tituloCabecera'>EVALUACIÓN DE RIESGOS</span>
				".$logoCliente."
			</div>
		</page_header>
		

		<p class='tituloIndice'>ÍNDICE</p>

		<ol class='indice'>
			<li>INTRODUCCIÓN</li>
			<li>OBJETO</li>
			<li>ÁMBITO DE APLICACIÓN</li>
			<li>DEFINICIONES</li>
			<li>DESARROLLO: MÉTODO DE EVALUACIÓN</li>
			<li>EVALUACIÓN DE RIESGOS DEL CENTRO DE TRABAJO</li>
			<li>(FALTAN LOS ANEXOS POR PUESTO EVALUADO)</li>
		</ol>

		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>";



	$contenido.="
	<page backtop='27mm' backleft='20mm' backright='20mm'>
		<page_header>
			<div class='cabecera'>
				<img src='../img/logoOferta.png' />
				<span class='tituloCabecera'>EVALUACIÓN DE RIESGOS</span>
				".$logoCliente."
			</div>
		</page_header>
		

		<div class='title'>1. INTRODUCCIÓN</div>

		<p class='justificado'>
			La evaluación de riesgos es el elemento central del sistema de gestión de la prevención, ya que a partir de la misma se han de configurar las diferentes actividades de planificación del control de los riesgos, así como la totalidad de elementos de gestión del propio sistema preventivo (formación, información, equipos de protección individual, etc.).
			<br /><br />
			El verdadero significado de toda evaluación está en ser el medio de reflexión necesario para poder realizar cualquier trabajo de la mejor manera posible sin afrontar riesgos innecesarios y poder controlar mejor aquellos que no hayan podido ser eliminados.
			<br /><br />
			El método de evaluación deberá proporcionar confianza sobre sus resultados. Incluirá la realización de las mediciones, análisis o ensayos que se consideren necesarios, salvo que se trate de operaciones, actividades o procesos en los que la directa apreciación profesional permita llegar a una conclusión sin necesidad de recurrir a aquellos. Si existe una normativa específica de aplicación en el caso de algún factor de riesgo, el procedimiento de evaluación deberá ajustarse a lo establecido legalmente.
			<br /><br />
			Las evaluaciones de riesgos de los puestos de trabajo deberán ser periódicas y revisarse cuando cambien las condiciones de trabajo (equipos, sustancias químicas, nuevas tecnologías, nuevos procedimientos, nuevas instalaciones, etc.), cuando se detecten daños para la salud en ese puesto (accidentes o enfermedades), cuando las medidas preventivas se vean que no son efectivas o cuando legalmente esté establecido.
			<br /><br />
			La Ley de Prevención de Riesgos Laborales en su artículo 23.1 a) y el R.D. 39/1997, exigen al empresario documentar la evaluación de riesgos y conservarla a disposición de la autoridad laboral.
			<br /><br />
			Además, deberá registrarse toda la documentación del proceso: identificación del puesto, relación de trabajadores en ese puesto, riesgos existentes o potenciales, resultado de la evaluación, medidas preventivas procedentes, criterios y procedimientos de evaluación empleados y/o exigibles, técnico de prevención que ha hecho la evaluación, etc. Y esta documentación estará a disposición de la autoridad laboral si así lo requiere.
		</p>

		<br /><br />

		<div class='title'>2. OBJETO</div>

		<p class='justificado'>
			El objeto de este procedimiento es establecer las pautas para la evaluación de riesgos existentes en la empresa.
			<br /><br />
			La Evaluación de Riesgos para la Seguridad y Salud de los Trabajadores, constituye la herramienta que permitirá al empresario desarrollar la actividad adecuada a los riesgos existentes (Art. 16.1 Ley 31 / 1995 de prevención de riesgos laborales).
			<br /><br />
			Su objetivo es determinar las posibles fuentes de daño para la seguridad y salud de los trabajadores en función de las instalaciones y equipos existentes y de las tareas que éstos deban desempeñar y la evaluación de dichos riesgos.
		</p>

		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>";




	$contenido.="
	<page backtop='27mm' backleft='20mm' backright='20mm'>
		<page_header>
			<div class='cabecera'>
				<img src='../img/logoOferta.png' />
				<span class='tituloCabecera'>EVALUACIÓN DE RIESGOS</span>
				".$logoCliente."
			</div>
		</page_header>
		

		<div class='title'>3. ÁMBITO DE APLICACIÓN</div>

		<p class='justificado'>
			Este Procedimiento se aplica a todos los puestos de trabajo, equipos e instalaciones propias de la empresa.
			<br /><br />
			Los resultados recogidos en la evaluación están basados en la información proporcionada por la propia empresa y en la obtenida en la visita nuestros técnicos al centro de trabajo principal e información de los trabajos que se realizan en las distintas obras que realiza esta empresa.
			<br /><br />
			Tienen valor en tanto persistan las condiciones de trabajo y el estado de instalaciones y equipos existente actualmente.
			<br /><br />
			La presente evaluación no abarca trabajos que sean objeto de estudios específicos (tales como obras de construcción, montaje o desmontaje de elementos prefabricados, derribos, etc.).
			<br /><br />
			La evaluación será actualizada cuando cambien las condiciones de trabajo y, en todo caso, se someterá a consideración y se revisará, si fuera necesario, con ocasión de los daños para la salud que se puedan producir (Art. 16.1 Ley 31 / 1995 de prevención de riesgos laborales).
		</p>

		<br /><br />

		<div class='title'>4. DEFINICIONES</div>

		<p class='justificado'>
			<strong>Prevención</strong>: Conjunto de actividades o medidas adoptadas o previstas en todas las fases de actividad de la empresa con el fin de evitar o disminuir los riesgos derivados del trabajo. 
			<br /><br />
			<strong>Evaluación de riesgos laborales</strong>: Es el proceso dirigido a estimar la magnitud de aquellos riesgos que no hayan podido evitarse, obteniendo la información necesaria para que el empresario esté en condiciones de tomar una decisión apropiada sobre la necesidad de adoptar medidas preventivas y, en tal caso, sobre el tipo de medidas que deben adoptarse. 
			<br /><br />
			Para dicha estimación, se valorarán conjuntamente la probabilidad de que se produzca el daño, la severidad del mismo y el tiempo de exposición al riesgo:


			<ul class='listaCuadros'>
				<li>Probabilidad: Posibilidad o frecuencia de que se materialice un peligro.</li>
				<li>Severidad: Consecuencia en términos de gravedad que normalmente se espera cuando se materializa un peligro.</li>
			</ul>


			Cuando de la evaluación realizada resulte necesaria la adopción de medidas preventivas, deberán ponerse claramente de manifiesto las situaciones en que sea necesario:

			<ul class='listaCuadros'>
				<li>Eliminar o reducir el riesgo, mediante medidas de prevención en el origen, organizativas, de protección colectiva, de protección individual, o de formación e información a los trabajadores.</li>
				<li>Controlar periódicamente las condiciones, la organización y los métodos de trabajo y el estado de salud de los trabajadores.</li>
			</ul>
		</p>

		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>";




	$contenido.="
	<page backtop='27mm' backleft='20mm' backright='20mm'>
		<page_header>
			<div class='cabecera'>
				<img src='../img/logoOferta.png' />
				<span class='tituloCabecera'>EVALUACIÓN DE RIESGOS</span>
				".$logoCliente."
			</div>
		</page_header>


		<p class='justificado'>
			<strong>Condición de trabajo</strong>: Cualquier característica del mismo que pueda tener una influencia significativa en la generación de riesgos para la seguridad y la salud del trabajador.
			<br /><br />
			<strong>Daños derivados del trabajo</strong>: enfermedades, patologías o lesiones sufridas con motivo u ocasión del trabajo.
			<br /><br />
			<strong>Equipo de Protección Individual (EPI)</strong>: Cualquier equipo destinado a ser llevado o sujetado por el trabajador para que le proteja de uno o varios riesgos que puedan amenazar su seguridad o su salud en el trabajo, así como cualquier complemento o accesorio destinado a tal fin.
			<br /><br />
			<strong>Equipo de trabajo</strong>: cualquier máquina, aparato, instrumento o instalación utilizada en el trabajo.
		</p>

		<br /><br />
		
		<div class='title'>5. DESARROLLO: MÉTODO DE EVALUACIÓN</div>
		<br /><br />
		<div class='tituloCentrado'>ESTRUCTURA</div>
		<br /><br />
		Las partes que constituyen una evaluación son:

		<ul class='listaCuadros'>
			<li>DEFINICIÓN DEL PUESTO, CENTRO O EQUIPO. Apartado donde se define y describe cómo y en qué consiste el puesto, el centro o el equipo de trabajo evaluado.</li>
			<li>TAREAS. Se desarrollan las tareas que quedan abarcadas en el puesto, que se realizan en el centro o que se llevan a cabo con el equipo de trabajo.</li>
			<li>EQUIPOS DE TRABAJO. Indicación de los equipos de trabajo referidos en la evaluación o bien los existentes en el centro de trabajo o utilizados en el puesto de trabajo.</li>
			<li>PRODUCTOS O SUSTANCIAS. Indicación de los productos químicos existentes en el centro de trabajo, utilizados en el puesto de trabajo o manipulados durante el uso de un equipo de trabajo.</li>
			<li>FORMACIÓN E INFORMACIÓN. Apartado en el que se indica qué formación o contenido de la misma debe poseer un trabajador para poder desarrollar sus tareas en un puesto, equipo o centro de trabajo objeto de dicha evaluación.</li>
			<li>EVALUACIÓN DE RIESGOS Y PROPUESTA DE MEDIDAS PREVENTIVAS. Propia evaluación en sí, identificando, valorando y priorizando los riesgos presentes en un puesto, equipo o centro de trabajo.</li>
			<li>ELABORADO, REVISADO Y CONFORMADO. Aprobación del documento por las partes afectadas.</li>
		</ul>


		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>";





	$contenido.="
	<page backtop='27mm' backleft='20mm' backright='20mm'>
		<page_header>
			<div class='cabecera'>
				<img src='../img/logoOferta.png' />
				<span class='tituloCabecera'>EVALUACIÓN DE RIESGOS</span>
				".$logoCliente."
			</div>
		</page_header>

		<div class='tituloCentrado'>DESCRIPCIÓN DEL MÉTODO DE EVALUACIÓN GENERAL DE RIESGOS</div>
				
		<p class='justificado'>
			<strong>GENERALIDADES:</strong>
			
			<br /><br />
			
			Cualquier riesgo se puede evaluar mediante un método general de evaluación como el que se expone en este apartado.
			
			<br /><br />
			
			<strong>ETAPAS DEL PROCESO</strong>
			
			<br /><br />
			
			Un proceso general de evaluación de riesgos se compone de las siguientes etapas:
			
			<br /><br />
			
			&nbsp;&nbsp;&nbsp; <strong>1. CLASIFICACIÓN DE LAS ACTIVIDADES</strong>

			<br /><br />

			Un paso preliminar a la evaluación de riesgos es preparar una lista de actividades de trabajo, agrupándolas en forma racional y manejable. Una posible forma de clasificar las actividades de trabajo es la siguiente:
			
			<ol class='listaLetras'>
				<li>Áreas externas a las instalaciones de la empresa.</li>
				<li>Etapas en el proceso de producción o en el suministro de un servicio.</li>
				<li>Trabajos planificados y de mantenimiento.</li>
				<li>Tareas definidas, por ejemplo: conductores de carretillas elevadoras.</li>
			</ol>

			Para cada actividad de trabajo puede ser preciso obtener información, entre otros, sobre los siguientes aspectos:

			<ol class='listaLetras'>
				<li>Tareas a realizar. Su duración y frecuencia.</li>
				<li>Lugares donde se realiza el trabajo.</li>
				<li>Quien realiza el trabajo, tanto permanente como ocasional.</li>
				<li>Otras personas que puedan ser afectadas por las actividades de trabajo (por ejemplo: visitantes, subcontratistas, público).</li>
				<li>Formación que han recibido los trabajadores sobre la ejecución de sus tareas.</li>
				<li>Procedimientos escritos de trabajo, y/o permisos de trabajo.</li>
				<li>Instalaciones, maquinaria y equipos utilizados.</li>
				<li>Herramientas manuales movidas a motor utilizados.</li>
				<li>Instrucciones de fabricantes y suministradores para el funcionamiento y mantenimiento de planta, maquinaria y equipos.</li>
				<li>Tamaño, forma, carácter de la superficie y peso de los materiales a manejar.</li>
				<li>Distancia y altura a las que han de moverse de forma manual los materiales.</li>
				<li>Energías utilizadas (por ejemplo: aire comprimido).</li>
				<li>Sustancias y productos utilizados y generados en el trabajo.</li>
				<li>Estado físico de las sustancias utilizadas (humos, gases, vapores, líquidos, polvo, sólidos).</li>
				<li>Contenido y recomendaciones del etiquetado de las sustancias utilizadas.</li>
				<li>Requisitos de la legislación vigente sobre la forma de hacer el trabajo, instalaciones, maquinaria y sustancias utilizadas.</li>
				<li> Medidas de control existentes.</li>
			</ol>
		</p>


		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>";









	$contenido.="<page footer='page' backbottom='27mm' backleft='0mm' backright='0mm' >
	<img class='fondoPortada' src='../img/fondoInforme.png' alt='QMA Consultores'>
	<p class='fechaPortada'>INFORME GESTIÓN DEL RIESGO: ".formateaFechaWeb($datos['fecha'])."</p>
	<p class='title'>1. HISTÓRICO DE REVISIONES</p>
	<p>Todas las revisiones relativas al presente informe se registrarán en la siguiente tabla, con su fecha y causas</p>
	".obtieneTablaHistorico($datos['codigoEvaluacion'])."
	</page>";

	$contenido.="<page footer='page' backbottom='27mm' backleft='0mm' backright='0mm' >
	<img class='fondoPortada' src='../img/fondoInforme.png' alt='QMA Consultores'>
	<p class='fechaPortada'>INFORME GESTIÓN DEL RIESGO: ".formateaFechaWeb($datos['fecha'])."</p>
	<p class='title'>2. OBJETO</p>
	<p>Establecer la sistemática a seguir para identificar, evaluar y registrar los riesgos a los que <b>".$_CONFIG['tituloGeneral']."</b> está expuesto y que generan incertidumbre para la consecución de nuestros objetivos, para determinar aquellos que tienen o pueden tener efectos significativos en los objetivos de <b>".$_CONFIG['tituloGeneral']."</b> y establecer medidas para su control.<br/><br/>
		La identificación y evaluación de riesgo permite:
	</p>
	<ul>
		<li>Asegurar que los riesgos empresariales son identificados.</li>
		<li>Establecer medidas de control, al menos, sobre aquellos que supongan un riesgo intolerable para <b>".$_CONFIG['tituloGeneral']."</b> a corto plazo.</li>
		<li>Definir las pautas de actuación ante situaciones de materialización de los riesgos evaluados.</li>
	</ul>
	
	<p class='title'>3 ALCANCE</p>
	<p>Este procedimiento aplica a las actividades actuales y futuras desarrolladas por <b><b>".$_CONFIG['tituloGeneral']."</b></b> (directamente o a través de subcontratas relacionadas con actividades y servicios dentro del ALCANCE DEL SISTEMA.)
	</p>
	
	<p class='title'>4. GENERAL</p>
	<p class='title subtitle'>4.1. DEFINICIONES</p>
	<p><b>Organización</b>: Persona o grupo de personas que tiene sus propias funciones con responsabilidades, autoridades y relaciones para lograr sus objetivos.<br/>
Nota 1. El concepto de organización incluye, entre otros, un trabajador independiente, compañía, corporación, firma, empresa, autoridad, sociedad, asociación, organización benéfica o institución, o una parte o combinación de éstas, ya estén constituidas o no, públicas o privadas.<br/>
Nota 2. Este término constituye uno de los términos comunes y definiciones esenciales para las normas de sistemas de gestión que se proporcionan en el Anexo SL del Suplemento ISO consolidado de la Parte 1 de las Directivas ISO/IEC. La definición original se ha modificado añadiendo la nota 1 a la entrada.<br/><br/>

<b>Contexto de la organización</b>: Combinación de cuestiones internas y externas que pueden tener un efecto en el enfoque de la organización para el desarrollo y logro de sus objetivos.<br/>
Nota 1 a la entrada Los objetivos de la organización pueden estar relacionados con sus productos y servicios, inversiones y comportamiento hacia sus partes interesadas.<br/>
Nota 2 a la entrada El concepto de contexto de la organización se aplica por igual tanto a organizaciones sin fines de lucro o de servicio público como a aquellas que buscan beneficios con frecuencia.<br/>
Nota 3 a la entrada En inglés, este concepto con frecuencia se denomina mediante otros términos, tales como “entorno empresarial”, “entorno de la organización” o “ecosistema de una organización”.<br/>
Nota 4 a la entrada: Entender la infraestructura puede ayudar a definir el contexto de la organización.<br/><br/>

<b>Parte interesada</b>: Persona u organización que puede afectar, verse afectada o percibirse como afectada por una decisión o actividad.<br/>
EJEMPLO Clientes, propietarios, personas de una organización, proveedores, banca, legisladores, sindicatos, socios o sociedad en general que puede incluir competidores o grupos de presión con intereses opuestos.<br/><br/>

Nota 1 a la entrada Este término constituye uno de los términos comunes y definiciones esenciales para las normas de sistemas de gestión que se proporcionan en el Anexo SL del Suplemento ISO consolidado de la Parte 1 de las Directivas ISO/IEC. La definición original se ha modificado añadiendo el ejemplo.<br/><br/></p>
</page>";
$contenido.="<page footer='page' backbottom='27mm' backleft='0mm' backright='0mm' >
	<img class='fondoPortada' src='../img/fondoInforme.png' alt='QMA Consultores'>
	<p class='fechaPortada'>INFORME GESTIÓN DEL RIESGO: ".formateaFechaWeb($datos['fecha'])."</p>
<p>
<b>Objetivo</b>: Resultado a lograr.<br/>
Nota 1. Un objetivo puede ser estratégico, táctico u operativo.<br/>
Nota 2. Los objetivos pueden referirse a diferentes disciplinas (tales como objetivos financieros, de salud y seguridad y ambientales) y se pueden aplicar en diferentes niveles [como estratégicos, para toda la organización (3.2.1), para el proyecto (3.4.2), el producto (3.7.6) y el proceso (3.4.1)].<br/>
Nota 3. Un objetivo se puede expresar de otras maneras, por ejemplo, como un resultado previsto, un propósito, un criterio operativo, un objetivo de la calidad (3.7.2), o mediante el uso de términos con un significado similar (por ejemplo, fin o meta).<br/>
Nota 4. En el contexto de sistemas de gestión de la calidad (3.5.4), la organización (3.2.1) establece los objetivos de la calidad (3.7.2), de forma coherente con la política de la calidad (3.5.9), para lograr resultados específicos.<br/><br/>

<b>Riesgo</b>: Efecto de la incertidumbre.<br/>
Nota 1. Un efecto es una desviación de lo esperado, ya sea positivo o negativo.<br/>
Nota 2. Incertidumbre es el estado, incluso parcial, de deficiencia de información (3.8.2) relacionada con la comprensión o conocimiento de un evento, su consecuencia o su probabilidad.<br/>
Nota 3. Con frecuencia el riesgo se caracteriza por referencia a eventos potenciales (según se define en la Guía ISO 73:2009, 3.5.1.3) y consecuencias (según se define en la Guía ISO 73:2009, 3.6.1.3), o a una combinación de éstos.<br/>
Nota 4. Con frecuencia el riesgo se expresa en términos de una combinación de las consecuencias de un evento (incluidos cambios en las circunstancias) y la probabilidad (según se define en la Guía ISO 73:2009, 3.6.1.1) asociada de que ocurra.<br/> 
Nota 5. La palabra “riesgo” algunas veces se utiliza cuando sólo existe la posibilidad de consecuencias negativas.<br/><br/>
	</p>
	<p class='title subtitle'>4.2. NORMA DE REFERENCIA</p>
	<p>Este informe es en base a la implantación revisada y finalizada de la Norma UNE EN ISO 9001:2015, así como a la Norma ISO 9000:2015.</p>

	<p class='title'>5. PRODECIMIENTO</p>
	<p class='title subtitle'>5.1. IDENTIFICACIÓN Y EVAUACIÓN DE RIESGOS EMPRESARIALES</p>
	<p>El presente informe es el resultado de la aplicación de la IT – 01, EVALUACIÓN DEL RIESGO, que incluye los criterios para la identificación y evaluación de los Riesgos incluidos en el presente informe.</p>

	<p class='title subtitle'>5.2. GESTIÓN DEL RIESGO EMPRESARIAL</p>
	<p>Este informe no es sino el punto de partida para que, una vez identificados los riesgos a los que nuestra compañía está expuesta y evaluados los efectos de su materialización en base a la probabilidad de ocurrencia, las medidas incluidas en el ANEXO II sean puestas en marchas con las prioridades establecidas en cada una de ellas, con el objetivo de minimizar los efectos en caso de materialización y/o, en su caso, llegar a eliminarlos.</p>
	</page>";

	
	$contenido.="<page footer='page' backbottom='27mm' backleft='0mm' backright='0mm' orientation='L'>
	<img class='fondoPortada' src='../img/fondoInforme.png' alt='QMA Consultores'>
	<p class='fechaPortada'>INFORME GESTIÓN DEL RIESGO: ".formateaFechaWeb($datos['fecha'])."</p>
	<p class='title'>6. ANEXO I: EVALUACIÓN DE RIESGOS</p>
	".obtieneTablaEvaluacion($datos['codigoEvaluacion'])."
	</page>";

	$contenido.="<page footer='page' backbottom='27mm' backleft='0mm' backright='0mm' orientation='L'>
	<img class='fondoPortada' src='../img/fondoInforme.png' alt='QMA Consultores'>
	<p class='fechaPortada'>INFORME GESTIÓN DEL RIESGO: ".formateaFechaWeb($datos['fecha'])."</p>
	<p class='title'>7. ANEXO II: PROGRAMA DE GESTIÓN DE RIESGOS</p>
	".obtieneTablaGestion($datos['codigoEvaluacion'],$datos['fecha'])."
	</page>";

	$evaluacion=datosRegistro('evaluacion_general',$datos['codigoEvaluacion']);
	$Base64Img = base64_decode($evaluacion['graficoBarraGlobal']);
	file_put_contents('../img/graficos/image5.png', $Base64Img); 
	$contenido.="<p class='title'>8. ANEXO III: ANÁLISIS GRÁFICO DE LA EXPOSICIÓN AL RIESGO</p>
	<img class='graficoBarra' src='../img/graficos/image5.png'>";

	return $contenido;
}

function obtieneTablaHistorico($evaluacion){
	$tabla="<table class='historico'>
		<tr>
			<th class='a15'>REVISIÓN</th>
			<th class='a15'>FECHA</th>
			<th class='a70'>HISTÓRICO</th>
		</tr>";
	$clase='even';
	$informes=consultaBD("SELECT * FROM informes WHERE codigoEvaluacion=".$evaluacion." ORDER BY fecha",true);
	$i=1;
	while($datos=mysql_fetch_assoc($informes)){
		$tabla.="
		<tr class='".$clase."'>
			<td class='a15'>".$i."</td>
			<td class='a15'>".formateaFechaWeb($datos['fecha'])."</td>
			<td class='a70'>".$datos['historico']."</td>
		</tr>";
		$i++;
		$clase=$clase=='even'?'odd':'even';
	}
	$tabla.='</table>';
	return $tabla;
}

function obtieneTablaEvaluacion($evaluacion){
	$evaluacion=datosRegistro('evaluacion_general',$evaluacion);

	$probabilidad=array('0'=>'','1'=>'Baja','2'=>'Media','3'=>'Alta');
	$consecuencias=array('0'=>'','1'=>'Ligeramente dañino','2'=>'Dañino','3'=>'Extremadamente dañino');
	$tabla="<table class='evaluacion'>
		<tr class='primeraCabecera'>
			<th class='a80 uno' colspan='4'>RIESGOS EMPRESARIALES</th>
			<th class='a20 dos' colspan='2'></th>
		</tr>
		<tr class='segundaCabecera'>
			<th class='a10' >TIPO</th>
			<th class='a15' >CÓDIGO</th>
			<th class='a15' >RIESGO</th>
			<th class='a40' >DESCRIPCIÓN</th>
			<th class='a10' >VALORACIÓN</th>
			<th class='a10' >PUNT.</th>
		</tr>
		<tr class='terceraCabecera'>
			<th class='a25 uno' colspan='2'>Fecha Valoración:</th>
			<th class='a75 dos' colspan='4'>".formateaFechaWeb($evaluacion['fechaEvaluacion'])."</th>
		</tr>";
	$riesgos=consultaBD('SELECT riesgos_evaluacion_general.*, riesgos.tipo, riesgos.codigoInterno, riesgos.nombre AS tipo FROM riesgos_evaluacion_general INNER JOIN riesgos ON riesgos_evaluacion_general.codigoRiesgo=riesgos.codigo  WHERE codigoEvaluacion='.$evaluacion['codigo'].' ORDER BY riesgos.tipo ASC, riesgos.codigoInterno ASC',true);
	$codigoProceso='';
	$first=true;
	while($datos=mysql_fetch_assoc($riesgos)){
		$total="";
		
		if($datos['total']==1){
	        $total="<td class='a10 blanco right'>Riesgo Trivial</td>";
	    }
	    else if($datos['total']==2){
	        $total="<td class='a10 amarillo right'>Riesgo Tolerable</td>";
	    }
	    else if($datos['total']==3 || $datos['total']==4){
	        $total="<td class='a10 naranja right'>Riesgo Moderado</td>";
	    }
	    else if($datos['total']==6){
	        $total="<td class='a10 rojo right'>Riesgo Importante</td>";
	    }
	    else if($datos['total']>6){
	        $total="<td class='a10 negro right'>Riesgo Intolerable</td>";
	    }


		$riesgo=datosRegistro('riesgos',$datos['codigoRiesgo']);
		if($riesgo['tipo'] != $codigoProceso && !$first){
			$tabla.="<tr><td colspan='6' class='a100'></td></tr>";
		}
		$codigoProceso=$riesgo['tipo'];
		$tabla.="<tr>
			<td class='a10' rowspan='4'>".$riesgo['tipo']."</td>
			<td class='a15' rowspan='4'>".$riesgo['codigoInterno']."</td>
			<td class='a15' rowspan='4'>".$riesgo['nombre']."</td>
			<td class='a40' rowspan='4'>".$datos['descripcionRiesgo']."</td>
			<td class='a10'>Probabilidad</td>
			<td class='a10'>".$probabilidad[$datos['probabilidad']]."</td>
		</tr>
		<tr>
			<td class='a10'>Consecuencia</td>
			<td class='a10'>".$consecuencias[$datos['consecuencias']]."</td>
		</tr>
		<tr>
			<td class='a10'></td>
			<td class='a10'></td>
		</tr>
		<tr>
			<td class='a10 right'>TOTAL</td>
			".$total."
		</tr>";
		$first=false;
	}
	$tabla.="</table>";
	return $tabla;
}

function obtieneTablaGestion($codigoEvaluacion,$fechaEvaluacion){
	$tabla='';
	$pie="</page><page footer='page' backbottom='0mm' backleft='0mm' backright='0mm' orientation='L'>
	<img class='fondoPortada' src='../img/fondoInforme.png' alt='QMA Consultores'>
	<p class='fechaPortada'>INFORME GESTIÓN DEL RIESGO: ".formateaFechaWeb($fechaEvaluacion)."</p>";

	$riesgosEvaluacion=consultaBD("SELECT * FROM riesgos_evaluacion_general WHERE codigoEvaluacion=".$codigoEvaluacion,true);
	while($riesgoEvaluacion=mysql_fetch_assoc($riesgosEvaluacion)){
	$gestiones=consultaBD("SELECT * FROM gestion_evaluacion_general WHERE codigoEvaluacion=".$riesgoEvaluacion['codigo']." ORDER BY fecha",true);
	$j=1;
	while($gestion=mysql_fetch_assoc($gestiones)){
		$tabla.=creaCabeceraTablaGestion($gestion['fecha']);
		$medidas=consultaBD("SELECT * FROM medidas_riesgos_evaluacion_general WHERE codigoGestion=".$gestion['codigo'],true);
		$i=0;
		$listadoRiesgos=array();
		while($medida=mysql_fetch_assoc($medidas)){
			$nombreRiesgo=datosRegistro('riesgos',$riesgoEvaluacion['codigoRiesgo']);
			$tipo=$nombreRiesgo['tipo'];
			$listadoRiesgos[$i]=array();
			$listadoRiesgos[$i]['nombre']=$nombreRiesgo['nombre'];
			$listadoRiesgos[$i]['recomendacion']=$medida['recomendacion'];
			$listadoRiesgos[$i]['total']=$riesgoEvaluacion['total'];
			$listadoRiesgos[$i]['medida']=$medida['recomendacion'];
			$listadoRiesgos[$i]['plazo']=$medida['plazo'] == 1 ? $medida['otroPlazo'] : $medida['plazo'].' meses';
			$listadoRiesgos[$i]['responsable']=$medida['responsable'];
			$listadoRiesgos[$i]['area']=$medida['area'];
			$listadoRiesgos[$i]['checkEjecutada']=$medida['checkEjecutada'];
			$listadoRiesgos[$i]['tipo']=$tipo;
			$listadoRiesgos[$i]['codigoInterno']=$nombreRiesgo['codigoInterno'];
			$i++;
	
		}
		usort($listadoRiesgos, "compararProcesos");
		foreach ($listadoRiesgos as $riesgo) {
			$lineaTotal="<td class='a15 right'></td>";
			if($riesgo['total']==1){
		        $lineaTotal="<td class='a15 blanco right'>Trivial</td>";
		    }
		    else if($riesgo['total']==2){
		        $lineaTotal="<td class='a15 amarillo right'>Tolerable</td>";
		    }
		    else if($riesgo['total']==3 || $riesgo['total']==4){
		        $lineaTotal="<td class='a15 naranja right'>Moderado</td>";
		    }
		    else if($riesgo['total']==6){
		        $lineaTotal="<td class='a15 rojo right'>Importante</td>";
		    }
		    else if($riesgo['total']>6){
		        $lineaTotal="<td class='a15 negro right'>Intolerable</td>";
		    }


			$plazo = $riesgo['plazo'];
			$listaResponsables=$riesgo['responsable'] == ''?$riesgo['area']:$riesgo['responsable'];
			$tabla.="
				<tr>
					<td class='a15'>".$riesgo['codigoInterno']." - ".$riesgo['nombre']."</td>
						".$lineaTotal."
					<td class='a40'>".$riesgo['recomendacion']."</td>
					<td class='a10'>".$plazo."</td>
					<td class='a10'>".$listaResponsables."</td>
					<td class='a10'>".$riesgo['checkEjecutada']."</td>
				</tr>";	
		}
		$tabla.='</table><br/>';
		if($j%2==0){
			$tabla.=$pie;
		}
		$j++;
	}
	}
	if($j%2==0){
		$tabla.=$pie;
	}
	return $tabla;
}

function creaCabeceraTablaGestion($fecha){
	$cabeceraTabla="<table class='gestion'>
		<tr class='primeraCabecera'>
			<th class='a70 uno' colspan='3'>MEDIDAS PARA LA GESTIÓN DE RIESGOS</th>
			<th class='a30 dos' colspan='3'>FECHA: ".formateaFechaWeb($fecha)."</th>
		</tr>
		<tr class='segundaCabecera'>
			<th class='a15'>RIESGO</th>
			<th class='a15'>NIVEL DE RIESGO</th>
			<th class='a40'>RECOMENDACIÓN</th>
			<th class='a10'>PLAZO</th>
			<th class='a10'>RESPONSABLE</th>
			<th class='a10'>EJECUTADA</th>
		</tr>";
	return $cabeceraTabla;
}*/

function compararProcesos($a, $b){
	$res=0;
	$i=0;
	$campos=array('codigoInterno');
	while($res==0 && $i<1){
    	$res=strcmp($a[$campos[$i]], $b[$campos[$i]]);
    	$i++;
	}
    return $res;
}

function estadisticasInformes($where){
	$res=array();
	$res=consultaBD('SELECT COUNT(informes.codigo) as total FROM informes INNER JOIN evaluacion_general ON informes.codigoEvaluacion=evaluacion_general.codigo INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo WHERE '.$where,true,true);
	return $res;
}


function formateaPreguntasFormulario($formulario){
	$res=array();

	$formulario=explode('&{}&',$formulario);

    if(count($formulario)>1){
        foreach($formulario as $pregunta){
            $partes=explode('=>',$pregunta);

            $res[$partes[0]]=$partes[1];
        }
    } 
    else{
    	for($i=0;$i<=100;$i++){
			$res['pregunta'.$i]='';
        }
    }

    return $res;
}

function generaPDF($codigo){
	global $_CONFIG;

	$datos=consultaBD("SELECT informes.codigo, clientes.codigo AS codigoCliente, clientes.EMPNOMBRE, ficheroLogo, formulario, evaluacion_general.fechaEvaluacion AS fecha, clientes_centros.direccion AS direccionCentro, clientes_centros.nombre AS nombreCentro, evaluacion_general.tipo, evaluacion_general.otroTipo, evaluacion_general.motivo, clientes_centros.cp AS cpCentro, clientes_centros.codigo AS codigoCentro, formularioPRL.codigo AS codigoFormulario, evaluacion_general.codigo AS codigoEvaluacion, evaluacion_general.tipoEvaluacion, puestos_trabajo.nombre AS nombrePuesto,  evaluacion_general.codigoPuestoTrabajo, formularioPRL.codigoUsuarioTecnico, puestos_trabajo.codigo AS codigoPuesto, funciones_formulario_prl.codigoCentroTrabajo AS centroPuesto, evaluacion_general.otros
					   FROM informes
					   INNER JOIN evaluacion_general ON informes.codigoEvaluacion=evaluacion_general.codigo
					   INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo
					   INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
					   INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
					   INNER JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
					   LEFT JOIN clientes_centros ON evaluacion_general.codigoCentroTrabajo=clientes_centros.codigo
					   LEFT JOIN funciones_formulario_prl ON evaluacion_general.codigoPuestoTrabajo=funciones_formulario_prl.codigo
					   LEFT JOIN empleados ON funciones_formulario_prl.codigoEmpleado=empleados.codigo
					   LEFT JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo
					   WHERE formularioPRL.fecha<evaluacion_general.fechaEvaluacion AND informes.codigo='$codigo' ORDER BY formularioPRL.fecha DESC;",true,true);
	if($datos['tipoEvaluacion']=='LUGAR'){
		$evaluacionesPuestos=consultaBD('SELECT evaluacion_general.*, puestos_trabajo.codigo AS codigoPuesto, funciones_formulario_prl.codigoCentroTrabajo FROM evaluacion_general INNER JOIN funciones_formulario_prl ON evaluacion_general.codigoPuestoTrabajo=funciones_formulario_prl.codigo INNER JOIN empleados ON funciones_formulario_prl.codigoEmpleado=empleados.codigo INNER JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo WHERE tipoEvaluacion="PUESTOS" AND funciones_formulario_prl.codigoCentroTrabajo='.$datos['codigoCentro'],true);
		$evaluacionesPuestosIndice=consultaBD('SELECT puestos_trabajo.nombre FROM evaluacion_general INNER JOIN funciones_formulario_prl ON evaluacion_general.codigoPuestoTrabajo=funciones_formulario_prl.codigo INNER JOIN empleados ON funciones_formulario_prl.codigoEmpleado=empleados.codigo INNER JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo WHERE tipoEvaluacion="PUESTOS" AND funciones_formulario_prl.codigoCentroTrabajo='.$datos['codigoCentro'],true);
		$nombrePrincipal=$datos['nombreCentro'];
		if($datos['direccionCentro']!='' && trim($datos['direccionCentro'])!=trim($datos['nombreCentro'])){
			if($nombrePrincipal==''){
				$nombrePrincipal=$datos['direccionCentro'];
			} else {
				$nombrePrincipal.='<br/>'.$datos['direccionCentro'];
			}
		}
		$direccion = $datos['direccionCentro'].', '.$datos['cpCentro'];

		$geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($direccion).'&sensor=false');
		$geo = json_decode($geo, true);
		if ($geo['status'] == 'OK') {
			$lat = $geo['results'][0]['geometry']['location']['lat'];
			$lon = $geo['results'][0]['geometry']['location']['lng'];
		} else {
			$lat = 0;
			$lon = 0;
		}
		if($lat!=0 && $lon!=0){
			$mapa='<img src="https://maps.googleapis.com/maps/api/staticmap?center='.$lat.','.$lon.'&markers=color:red%7Clabel:C%7C'.$lat.','.$lon.'&zoom=12&size=600x200"/>';
		} else {
			$mapa='';
		}

		$empleados=consultaBD('SELECT CONCAT(empleados.nombre," ",apellidos) AS nombre, puestos_trabajo.nombre AS puesto, puestos_trabajo.codigo AS codigoPuesto FROM empleados LEFT JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo INNER JOIN funciones_formulario_prl ON empleados.codigo=funciones_formulario_prl.codigoEmpleado  WHERE codigoCentroTrabajo='.$datos['codigoCentro'],true);
	} else if($datos['tipoEvaluacion']=='PUESTOS'){
		$nombrePrincipal=$datos['nombrePuesto'];
		$mapa='';

		$empleados=false;
	} else {
		$nombrePrincipal=$datos['otros'];
		$mapa='';
	}
	$formulario=explode('&{}&',$datos['formulario']);
    if(count($formulario)>1){
        foreach($formulario as $pregunta){
            $partes=explode('=>',$pregunta);
            $datos[$partes[0]]=$partes[1];
        }
    }
    $logoCliente=$datos['EMPNOMBRE'];
	if($datos['ficheroLogo']!='' && $datos['ficheroLogo']!='NO'){
		$divCliente="<div class='logoPortada'><img src='../clientes/imagenes/".$datos['ficheroLogo']."' /></div>";
		$logoCliente="<img src='../clientes/imagenes/".$datos['ficheroLogo']."' />";
	} else {
		$divCliente="<div class='logoPortadaSin'></div>";
	}

	$tipoEvaluacion=$datos['tipo']=='OTROS'?$datos['otroTipo']:$datos['tipo'];
	$mapa='';
	$usuario=datosRegistro('usuarios',$datos['codigoUsuarioTecnico']);
	$codigoFirmas=array(145,146,150);
	$firmas=array(145=>'firmaAdan.png',146=>'firmaAngeles.png',150=>'firmaMarta.png');
	if($usuario && in_array($usuario['codigo'], $codigoFirmas)){
		$firma='<img src="../img/'.$firmas[$usuario['codigo']].'"">';
	} else {
		$firma='';
	}
    $contenido = "
	<style type='text/css'>
	<!--
			html{
				margin:0px;
				padding:0px;
				width:100%;
			}
	        body{
	            font-size:12px;
	            font-family: Calibri;
	            font-weight: lighter;
	            line-height: 24px;
	            margin:0px;
				padding:0px;
				width:100%;

	        }

	        .portada{
	        	border:2px solid #02215c;
	        	height:95%;
	        	margin:20px;
	        }

	        .titlePortada{
	        	text-align:center;
	        	font-size:56px;
	        	color:#02215c;
	        	font-weight:bold;
	        	margin-top:20px;
	        	line-height:64px;
	        }

	        .imagenFondo{
	        	position:absolute;
	        	top:10%;
	        	z-index:1;
	        	left:-3%
	        }

	        .imagenFondo img{
	        	width:103%;
	        	position:absolute;
	        	display:inline-block;
	        }

	        .imagenFondo2{
	        	position:absolute;
	        	z-index:1;
	        	top:-133px;
	        	left:-30px;
	        }

	        .imagenFondo2 img{
	        	width:104%;
	        	height:100%;
	        	display:inline-block;
	        }

	        .imagenFondo3{
	        	position:absolute;
	        	z-index:1;
	        	top:-133px;
	        	left:-205px;
	        }

	        .imagenFondo3 img{
	        	width:104%;
	        	height:100%;
	        	width:800px;
	        	display:inline-block;
	        }

	        .nombreEmpresa{
	        	font-size:36px;
	        	color:#02215c;
	        	text-align:center;
	        	margin-top:20px;
	        	width:100%;
	        	line-height:48px;
	        }

	        .tablaFirmas{
	        	border:1px solid #000;
	        	border-collapse:collapse;
	        	font-weight:bold;
	        	margin-top:20px;
	        	width:80%;
	        	margin-left:80px;
	        }

	        .tablaFirmas th,
	        .tablaFirmas td{
	        	width:33%;
	        }

	        .tablaFirmas .primeraFila{
	        	color:#02205C;
	        	border-top:1px solid #02205C;
	        	border-bottom:1px solid #02205C;
	        }

	        .tablaFirmas .segundaFila{
	        	background:rgb(188,204,226);
	        }
	        .tablaFirmas .ultimaFila{
	        	border-top:1px solid #02205C;
	        	border-bottom:1px solid #02205C;
	        }

	        .tablaFirmas td img{
	        	width:100%;
	        }

	        .cabecera{
	        	border-top:1px solid #02215c;
	        	border-bottom:1px solid #02215c;
	        	padding:0px;
	        	margin-bottom:200px;
	        	width:100%;
	        }

	        .cabecera .a20{
	        	width:20%;
	        	text-align:center;
	        }

	        .cabecera .a60{
	        	width:60%;
	        	color:#02215c;
	        	text-align:center;
	        	font-weight:bold;
	        	font-size:18px;
	        }

	        .cabecera img{
	        	width:58%;
	        }

	        .cabecera .tituloCabecera{
	        	color:#02215c;
	        	text-align:center;
	        	font-weight:bold;
	        	margin-left:140px;
	        	font-size:18px;
	        	margin-top:-25px;
	        }

	        .cabecera .logoCliente{
	        	position:absolute;
	        	right:0px;
	        	top:0px;
	        	background:red;
	        	width:100px;
	        }

	        .cajaRevision{
	        	color:#02215c;
	        	margin-left:80px;
	        }

	        .cajaNumeroPagina{
	        	color:#02215c;
	        	position:absolute;
	        	right:80px;
	        	bottom:0px;	
	        }

	        .tituloIndice{
	        	text-align:center;
	        	font-size:20px;
	        	margin-top:20px;
	        	margin-bottom:10px;
	        	color:#02215c;
	        }

	        .indice{
	        	margin-left:80px;
	        	width:100%;
	        }


	        .indice td{
	        	font-size:14px;
	        	color:#02215c;
	        	padding-top:35px;
	        }

	        .indice .texto{
	        	width:75%;
	        }

	        .indice .sub{
	        	font-weight:normal;
	        	padding-left:10px;
	        }

	        .indice .pagina{
	        	font-weight:bold;
	        	width:5%;
	        }

	        .container{
	        	width:75%;
	        	margin-left:90px;
	        }

	        .container2{
	        	width:95%;
	        	margin-left:20px;
	        	border:1px solid #02215c;
	        	padding:5px;
	        	margin-top:20px;
	        }

	        .tituloSeccion{
	        	font-size:16px;
	        	margin-top:20px;
	        	margin-bottom:20px;
	        	color:#02215c;
	        }

	        .tituloSeccionOtro{
	        	margin-left:0px !important;
	        	padding-left:0px !important;
	        	text-align:left !important;
	        }

	        p, li {
	        	text-align:justify;
	        	font-size:11px;
	        	line-height:18px;
	        }

	        li{
	        	padding-bottom:14px;
	        }

	        .tituloIndice{
	        	text-align:center;
	        	font-size:20px;
	        	margin-top:20px;
	        	margin-bottom:20px;
	        	color:#02215c;
	        }

	        .tablaDatos{
	        	width:100%;
	        	border:1px solid #000;
	        	border-collapse:collapse;
	        }

	        .tablaDatos th,
	        .tablaDatos td{
	        	border:1px solid #000;
	        	font-size:11px;
	        	padding-top:3px;
	        	padding-bottom:3px;
	        	padding-left:8px;
	        }

	        .tablaDatos th{
	        	background:#02215c;
	        	font-weight:bold;
	        	color:#FFF;
	        }

	        .tablaDatos .a10{
	        	width:10%;
	        }

	        .tablaDatos .a15{
	        	width:15%;
	        }

	        .tablaDatos .a20{
	        	width:20%;
	        }

	        .tablaDatos .a25{
	        	width:25%;
	        }

	        .tablaDatos .a30{
	        	width:30%;
	        }

	        .tablaDatos .a33,
	        .noBorde .a33{
	        	width:33%;
	        }

	        .tablaDatos .a35{
	        	width:35%;
	        }

	        .tablaDatos .a65{
	        	width:65%;
	        }

	        .tablaDatos .a70{
	        	width:70%;
	        }

	        .tablaDatos .a85{
	        	width:85%;
	        }

	        .tablaDatos .a100{
	        	width:100%;
	        }

	        .tablaDatos .centro{
	        	text-align:center;
	        }

	        .desconocido{
	        	color:red;
	        	font-weight:bold;
	        }

	        .noBorde{
	        	width:100%;
	        	border:none;
	        }

	        .noBorde th,
	        .noBorde td{
	        	font-size:11px;
	        	padding-top:3px;
	        	padding-bottom:3px;
	        	padding-left:8px;
	        	font-weight:bold;
	        	height:15px;
	        	border:none;
	        	border-collapse:collapse;
	        }

	        .noBorde th{
	        	background:#02215c;
	        	font-weight:bold;
	        	color:#FFF;
	        }

	        .noBorde .tdGris{
	        	background:#BBB;
	        }

	        p.tituloIndice{
	        	text-align:center;
	        	font-size:20px;
	        	margin-top:40px;
	        	margin-bottom:20px;
	        	color:#02215c;
	        	font-weight:bold;
	        }

	        .organigrama{
	        	width:80%;
	        }

	        .logoPortada{
	        	width:100%;
	        	text-align:center;
	        	margin-top:30px;
	        	height:400px;
	        }

	        .logoPortada img{
	        	width:39%;
	        }

	         .logoPortadaSin{
	        	width:100%;
	        	text-align:center;
	        	margin-top:30px;
	        	height:200px;
	        }

	        ul{
	        	padding-bottom:0px;
	        }

	        .centrado{
	        	text-align:center;
	        	color:#02215c;
	        	font-weight:bold;
	        }

	        .nivelesRiesgo, .nivelesRiesgo2, .nivelesRiesgo3{
	        	border-collapse:collapse;
	        }
	        .nivelesRiesgo td{
	        	border:1px solid #000;
	        	text-align:center;
	        	padding:3px;
	        	vertical-align:middle;
	        	height:40px;
	        	width:22%;
	        }

	        .nivelesRiesgo2 td, .nivelesRiesgo3 td{
	        	border:1px solid #000;
	        	padding:7px;
	        	vertical-align:middle;
	        }

	        .nivelesRiesgo2 th, .nivelesRiesgo3 th{
	        	border:1px solid #000;
	        	padding:7px;
	        	vertical-align:middle;
	        	text-align:center;
	        	background:#DDD;
	        }

	        .a12{
	        	width:12%;
	        }

	        .a20{
	        	width:20%;
	        }

	        .a34{
	        	width:34%;
	        }

	        .a60{
	        	width:60%;
	        }

	        .a66{
	        	width:66%;
	        }

	        .conFondo{
	        	background:#DDD;
	        }

	        .panel-default{
	        	background:#f9f9f9;
	        }

	        .panel-amarillo{
	        	background: #fff584;
	        }

	        .panel-warning{
	        	background: #ffbf42;
	        }

	        .panel-danger{
	        	background: #c00011;
	        	color:#FFF;
	        }

	        .panel-inverse{
	        	background: #000;
	        	color:#FFF;
	        }

	        .noBordeLeft{
	        	border-left:0px;
	        }

	        .noBordeTop{
	        	border-top:0px;
	        }

	        .tablaDatos{
	        	border:1px solid #000;
	        	width:100%;
	        	border-collapse:collapse;
	        }

	        .tablaDatos th, .tablaDatos td{
	        	border:1px solid #000;
	        	padding:3px;
	        }

	        .tablaDatos th{
	        	font-weight:bold;
	        	background:rgb(188,204,226);
	        	color:#000;
	        }

	        .a20{
	        	width:20%;
	        }

	        .a30{
	        	width:30%;
	        }

	        .a33{
	        	width:33%;
	        }

	        .a35{
	        	width:35%;
	        }

	        .a50{
	        	width:50%;
	        }

	        .a65{
	        	width:65%;
	        }

	        .a66{
	        	width:66%;
	        }

	        .a70{
	        	width:70%;
	        }

	        .centro{
	        	text-align:center;
	        }

	        .tituloConRelleno{
	        	font-weight:bold;
	        	background:rgb(188,204,226);
	        	width:100%;
	        	padding:5px;
	        	font-size:18px;
	        }

	        .panel-default{
  				background-color: #f9f9f9;
	        }

	        .panel-amarillo{
  				background-color: #fff584;
	        }

	        .panel-warning{
  				background-color: #ffbf42;
	        }

	        .panel-danger{
  				background-color: #c00011;
	        }

	        .panel-inverse{
	        	color: #FFF;
  				background-color: #000;
	        }

	        .imagenRiesgo{
	        	width:50%;
	        	border:0px;
	        }

	        .imagenRiesgo2{
	        	width:100%;
	        	border:0px;
	        	text-align:center;
	        }

	        .imagenRiesgo img{
	        	height:150px;
	        }

	        .imagenRiesgo2 img{
	        	height:150px;
	        }

	        .noBordeBottom{
	        	border-bottom:0px !important;
	        }

	        .noBordeTopBottom{
	        	border-top:0px !important;
	        	border-bottom:0px !important;
	        }

	        .rojo{
	        	color:red;
	        	font-weight:bold;
	        }
	-->
	</style>
	

	<page backbottom='0mm' backleft='0mm' backright='0mm'>
		<div class='imagenFondo'><img src='../img/fondoOferta.jpeg' alt='Anesco' /></div>
		<div class='portada'>
			<div class='titlePortada'>EVALUACIÓN DE<br/>RIESGOS</div>
			".$divCliente."
			<div class='nombreEmpresa'>".$datos['pregunta0']."<br/>".$nombrePrincipal."</div>
			<table class='tablaFirmas'>
				<tr>
					<td class='primeraFila'>Elaborado por:</td>
					<td class='primeraFila'>Revisión:</td>
					<td class='primeraFila'>Aprobado:</td>
				</tr>
				<tr>
					<td class='segundaFila'>".$usuario['nombre']." ".$usuario['apellidos']."<br/>".$firma."</td>
					<td class='segundaFila'></td>
					<td class='segundaFila'></td>
				</tr>
				<tr>
					<td>Técnico Superior en Prevención de Riesgos Laborales</td>
					<td></td>
					<td>Representante Legal</td>
				</tr>
				<tr>
					<td class='segundaFila'>Fecha: ".formateaFechaWeb($datos['fecha'])."</td>
					<td class='segundaFila'>Fecha:</td>
					<td class='segundaFila'>Fecha:</td>
				</tr>
				<tr>
					<td class='ultimaFila'>ANESCO SALUD Y PREVENCIÓN, S.L.</td>
					<td class='ultimaFila'></td>
					<td class='ultimaFila'></td>
				</tr>
			</table>
		</div>

	</page>";

$contenido.="
	<page backtop='30mm' backleft='50mm' backright='50mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo3'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
	</page>";


$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<bookmark title='1. INTRODUCCIÓN' level='0' ></bookmark>
			<h1 class='tituloSeccion'>1. INTRODUCCIÓN</h1>
			<p>La evaluación de riesgos es el elemento central del sistema de gestión de la prevención, ya que a partir de la misma se han de configurar las diferentes actividades de planificación del control de los riesgos, así como la totalidad de elementos de gestión del propio sistema preventivo (formación, información, equipos de protección individual, etc.).</p>
			<p>El verdadero significado de toda evaluación está en ser el medio de reflexión necesario para poder realizar cualquier trabajo de la mejor manera posible sin afrontar riesgos innecesarios y poder controlar mejor aquellos que no hayan podido ser eliminados.</p>
			<p>El método de evaluación deberá proporcionar confianza sobre sus resultados. Incluirá la realización de las mediciones, análisis o ensayos que se consideren necesarios, salvo que se trate de operaciones, actividades o procesos en los que la directa apreciación profesional permita llegar a una conclusión sin necesidad de recurrir a aquellos. Si existe una normativa específica de aplicación en el caso de algún factor de riesgo, el procedimiento de evaluación deberá ajustarse a lo establecido legalmente.</p>
			<p>Las evaluaciones de riesgos de los puestos de trabajo deberán ser periódicas y revisarse cuando cambien las condiciones de trabajo (equipos, sustancias químicas, nuevas tecnologías, nuevos procedimientos, nuevas instalaciones, etc.), cuando se detecten daños para la salud en ese puesto (accidentes o enfermedades), cuando las medidas preventivas se vean que no son efectivas o cuando legalmente esté establecido.</p>
			<p>La Ley de Prevención de Riesgos Laborales en su artículo 23.1 a) y el R.D. 39/1997, exigen al empresario documentar la evaluación de riesgos y conservarla a disposición de la autoridad laboral.</p>
			<p>Además, deberá registrarse toda la documentación del proceso: identificación del puesto, relación de trabajadores en ese puesto, riesgos existentes o potenciales, resultado de la evaluación, medidas preventivas procedentes, criterios y procedimientos de evaluación empleados y/o exigibles, técnico de prevención que ha hecho la evaluación, etc. Y esta documentación estará a disposición de la autoridad laboral si así lo requiere.</p>
			<bookmark title='2. OBJETO' level='0' ></bookmark>
			<h1 class='tituloSeccion'>2. OBJETO</h1>
			<p>El objeto de este procedimiento es establecer las pautas para la evaluación de riesgos existentes en la empresa.</p>
			<p>La Evaluación de Riesgos para la Seguridad y Salud de los Trabajadores, constituye la herramienta que permitirá al empresario desarrollar la actividad adecuada a los riesgos existentes (Art. 16.1 Ley 31 / 1995 de prevención de riesgos laborales).</p>
			<p>Su objetivo es determinar las posibles fuentes de daño para la seguridad y salud de los trabajadores en función de las instalaciones y equipos existentes y de las tareas que éstos deban desempeñar y la evaluación de dichos riesgos.</p>
		</div>
	</page>";

$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<bookmark title='3. ÁMBITO DE APLICACIÓN' level='0' ></bookmark>
			<h1 class='tituloSeccion'>3. ÁMBITO DE APLICACIÓN</h1>
			<p>Este Procedimiento se aplica a todos los puestos de trabajo, equipos e instalaciones propias de la empresa.</p>
			<p>Los resultados recogidos en la evaluación están basados en la información proporcionada por la propia empresa y en la obtenida en la visita nuestros técnicos al centro de trabajo principal e información de los trabajos que se realizan en las distintas obras que realiza esta empresa.</p>
			<p>Tienen valor en tanto persistan las condiciones de trabajo y el estado de instalaciones y equipos existente actualmente.</p>
			<p>La presente evaluación no abarca trabajos que sean objeto de estudios específicos (tales como obras de construcción, montaje o desmontaje de elementos prefabricados, derribos, etc.).</p>
			<p>La evaluación será actualizada cuando cambien las condiciones de trabajo y, en todo caso, se someterá a consideración y se revisará, si fuera necesario, con ocasión de los daños para la salud que se puedan producir (Art. 16.1 Ley 31 / 1995 de prevención de riesgos laborales).</p>
			<bookmark title='4. DEFINICIONES' level='0' ></bookmark>
			<h1 class='tituloSeccion'>4. DEFINICIONES</h1>
			<p><b>Prevención:</b> Conjunto de actividades o medidas adoptadas o previstas en todas las fases de actividad de la empresa con el fin de evitar o disminuir los riesgos derivados del trabajo. </p>
			<p><b>Evaluación de riesgos laborales:</b> Es el proceso dirigido a estimar la magnitud de aquellos riesgos que no hayan podido evitarse, obteniendo la información necesaria para que el empresario esté en condiciones de tomar una decisión apropiada sobre la necesidad de adoptar medidas preventivas y, en tal caso, sobre el tipo de medidas que deben adoptarse. </p>
			<p>Para dicha estimación, se valorarán conjuntamente la probabilidad de que se produzca el daño, la severidad del mismo y el tiempo de exposición al riesgo:</p>
			<ul style='list-style-type:square;'>
				<li>Probabilidad: Posibilidad o frecuencia de que se materialice un peligro. </li>
				<li>Severidad: Consecuencia en términos de gravedad que normalmente se espera cuando se materializa un peligro.</li>
			</ul>
			<p>Para dicha estimación, se valorarán conjuntamente la probabilidad de que se produzca el daño, la severidad del mismo y el tiempo de exposición al riesgo:</p>
			<ul style='list-style-type:square;'>
				<li>Eliminar o reducir el riesgo, mediante medidas de prevención en el origen, organizativas, de protección colectiva, de protección individual, o de formación e información a los trabajadores.</li>
				<li>Controlar periódicamente las condiciones, la organización y los métodos de trabajo y el estado de salud de los trabajadores.</li>
			</ul>
		</div>
	</page>";

$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<p><b>Condición de trabajo:</b> Cualquier característica del mismo que pueda tener una influencia significativa en la generación de riesgos para la seguridad y la salud del trabajador.</p>
			<p><b>Daños derivados del trabajo:</b> enfermedades, patologías o lesiones sufridas con motivo u ocasión del trabajo.</p>
			<p><b>Equipo de Protección Individual (EPI):</b> Cualquier equipo destinado a ser llevado o sujetado por el trabajador para que le proteja de uno o varios riesgos que puedan amenazar su seguridad o su salud en el trabajo, así como cualquier complemento o accesorio destinado a tal fin.</p>
			<p><b>Equipo de trabajo:</b> cualquier máquina, aparato, instrumento o instalación utilizada en el trabajo.</p>
			<bookmark title='5. DESARROLLO: MÉTODO DE EVALUACIÓN' level='0' ></bookmark>
			<h1 class='tituloSeccion'>5. DESARROLLO: MÉTODO DE EVALUACIÓN</h1>
			<p class='centrado'>ESTRUCTURA</p>
			<p>Las partes que constituyen una evaluación son:</p>
			<ul style='list-style-type:square;'>
				<li>DEFINICIÓN DEL PUESTO, CENTRO O EQUIPO. Apartado donde se define y describe cómo y en qué consiste el puesto, el centro o el equipo de trabajo evaluado.</li>
				<li>TAREAS. Se desarrollan las tareas que quedan abarcadas en el puesto, que se realizan en el centro o que se llevan a cabo con el equipo de trabajo.</li>
				<li>RELACIÓN DE TRABAJADORES</li>
				<li>FORMACIÓN E INFORMACIÓN. Apartado en el que se indica qué formación o contenido de la misma debe poseer un trabajador para poder desarrollar sus tareas en un puesto, equipo o centro de trabajo objeto de dicha evaluación.</li>
				<li>PRODUCTOS O SUSTANCIAS. Indicación de los productos químicos existentes en el centro de trabajo, utilizados en el puesto de trabajo o manipulados durante el uso de un equipo de trabajo.</li>
				<li>EQUIPOS DE TRABAJO. Indicación de los equipos de trabajo referidos en la evaluación o bien los existentes en el centro de trabajo o utilizados en el puesto de trabajo.</li>
				<li>EQUIPOS DE PROTECCIÓN INDIVIDUAL. Indicación de los equipos de protección que deberán utilizar los trabajadores en  function de last areas que realicen en sus puestos de trabajo.</li>
				<li>EVALUACIÓN DE RIESGOS Y PROPUESTA DE MEDIDAS PREVENTIVAS. Propia evaluación en sí, identificando, valorando y priorizando los riesgos presentes en un puesto, equipo o centro de trabajo.</li>
				<li>ELABORADO, REVISADO Y CONFORMADO. Aprobación del documento por las partes afectadas. </li>
			</ul>
			<p class='centrado'>DESCRIPCIÓN DEL MÉTODO DE EVALUACIÓN GENERAL DE RIESGOS</p>
			<p>GENERALIDADES:</p>
			<p>Cualquier riesgo se puede evaluar mediante un método general de evaluación como el que se expone en este apartado.</p>
			<p>ETAPAS DEL PROCESO:<br/>
			Un proceso general de evaluación de riesgos se compone de las siguientes etapas:</p>
		</div>

	</page>";

$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<ul style='list-style-type:none'>
				<li>1. CLASIFICACIÓN DE LAS ACTIVIDADES</li>
			</ul>
			<p style='margin-top:-10px;'>Un paso preliminar a la evaluación de riesgos es preparar una lista de actividades de trabajo, agrupándolas en forma racional y manejable. Una posible forma de clasificar las actividades de trabajo es la siguiente:</p>
			<p>a. Áreas externas a las instalaciones de la empresa.<br/>
			b. Etapas en el proceso de producción o en el suministro de un servicio.<br/>
			c. Trabajos planificados y de mantenimiento.<br/>
			d. Tareas definidas, por ejemplo: conductores de carretillas elevadoras.</p>
			<p>Para cada actividad de trabajo puede ser preciso obtener información, entre otros, sobre los siguientes aspectos:</p>
			<p>a. Tareas a realizar. Su duración y frecuencia.<br/>
			b. Lugares donde se realiza el trabajo.<br/>
			c. Quien realiza el trabajo, tanto permanente como ocasional.<br/>
			d. Otras personas que puedan ser afectadas por las actividades de trabajo (por ejemplo: visitantes, subcontratistas, público).<br/>
			e. Formación que han recibido los trabajadores sobre la ejecución de sus tareas.<br/>
			f. Procedimientos escritos de trabajo, y/o permisos de trabajo.<br/>
			g. Instalaciones, maquinaria y equipos utilizados.<br/>
			h. Herramientas manuales movidas a motor utilizados.<br/>
			i. Instrucciones de fabricantes y suministradores para el funcionamiento y mantenimiento de planta, maquinaria y equipos.<br/>
			j. Tamaño, forma, carácter de la superficie y peso de los materiales a manejar.<br/>
			k. Distancia y altura a las que han de moverse de forma manual los materiales.<br/>
			l. Energías utilizadas (por ejemplo: aire comprimido).<br/>
			m. Sustancias y productos utilizados y generados en el trabajo.<br/>
			n. Estado físico de las sustancias utilizadas (humos, gases, vapores, líquidos, polvo, sólidos).<br/>
			o. Contenido y recomendaciones del etiquetado de las sustancias utilizadas.<br/>
			p. Requisitos de la legislación vigente sobre la forma de hacer el trabajo, instalaciones, maquinaria y sustancias utilizadas.<br/>
			q. Medidas de control existentes.<br/>
			r. Datos reactivos de actuación en prevención de riesgos laborales: incidentes, accidentes, enfermedades laborales derivadas de la actividad que se desarrolla, de los equipos y de las sustancias utilizadas. Debe buscarse información dentro y fuera de la organización.<br/>
			s. Datos de evaluaciones de riesgos existentes, relativos a la actividad desarrollada.<br/>
			t. Organización del trabajo.</p>
			<ul style='list-style-type:none;'>
				<li>2. ANÁLISIS DE LOS RIESGOS, IDENTIFICACIÓN</li>
			</ul>
			<p style='margin-top:-10px;'>Para llevar a cabo la identificación de los riesgos hay que preguntarse tres cosas:</p>
			<p>a. ¿Existe una fuente de daño?<br/>
			b. ¿Quién (o qué) puede ser dañado?<br/>
			c. ¿Cómo puede ocurrir el daño?</p>
			<p>Con el fin de ayudar en el proceso de identificación de peligros, es útil categorizarlos en distintas formas, por ejemplo, por temas: mecánicos, eléctricos, radiaciones, sustancias, incendios, explosiones, etc.</p>
			<p>Complementariamente se puede desarrollar una lista de preguntas, tales como: durante las actividades de trabajo, ¿existen los siguientes riesgos?</p>
		</div>

	</page>";

$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<p>a. Golpes y cortes.<br/>
			b. Caídas al mismo nivel.<br/>
			c. Caídas de personas a distinto nivel.<br/>
			d. Caídas de herramientas, materiales, etc., desde altura.<br/>
			e. Espacio inadecuado.<br/>
			f. Riesgos asociados con manejo manual de cargas.<br/>
			g. Riesgos en las instalaciones y en las máquinas asociados con el montaje, la consignación, la operación, el mantenimiento, la modificación, la reparación y el desmontaje.<br/>
			h. Riesgos de los vehículos, tanto en el transporte interno como el transporte por carretera.<br/>
			i. Incendios y explosiones.<br/>
			j. Sustancias que pueden inhalarse.<br/>
			k. Sustancias o agentes que pueden dañar los ojos.<br/>
			l. Sustancias que pueden causar daño por el contacto o la absorción por la piel.<br/>
			m. Sustancias que pueden causar daños al ser ingeridas.<br/>
			n. Energías peligrosas (por ejemplo: electricidad, radiaciones, ruido y vibraciones).<br/>
			o. Trastornos musculoesqueléticos derivados de movimientos repetitivos.<br/>
			p. Ambiente térmico inadecuado.<br/>
			q. Condiciones de iluminación inadecuadas.<br/>
			r. Barandillas inadecuadas en escaleras.</p>
			<ul style='list-style-type:none;'>
				<li>3. ESTIMACIÓN DEL RIESGO</li>
			</ul>
			<p>Para cada peligro detectado debe estimarse el riesgo, determinando la potencial severidad del daño (consecuencias) y la probabilidad de que ocurra el hecho.</p>
			<ul style='list-style-type:none;'>
				<li>3.1. SEVERIDAD</li>
			</ul>
			<p>Para determinar la potencial severidad del daño, debe considerarse:</p>
			<p>a. Partes del cuerpo que se verán afectadas<br/>
			b. Naturaleza del daño, graduándolo desde ligeramente dañino a extremadamente dañino.</p>
			<p>Ejemplos de LIGERAMENTE DAÑINO:</p>
			<p>Daños superficiales (cortes y magulladuras pequeñas, irritación de los ojos por polvo), molestias e irritación, dolor de cabeza, disconfort.</p>
			<p>Ejemplos de DAÑINO:</p>
			<p>Laceraciones, quemaduras, conmociones, torceduras importantes, fracturas menores, sordera, dermatitis, asma, trastornos musculoesqueléticos (en adelante TME), enfermedad que conduce a una incapacidad menor.</p>
			<p>Ejemplos de EXTREMADAMENTE DAÑINO:</p>
			<p>Amputaciones, fracturas mayores, intoxicaciones, lesiones múltiples, lesiones fatales, cáncer y otras enfermedades crónicas que acorten severamente la vida.</p>
		</div>

	</page>";

$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<ul style='list-style-type:none;'>
				<li>3.2. PROBABILIDAD DE QUE OCURRA</li>
			</ul>
			<p>La probabilidad de que ocurra el daño se puede graduar, desde baja hasta alta, con el siguiente criterio:</p>
			<p>Probabilidad ALTA: El daño ocurrirá siempre o casi siempre.</p>
			<p>Probabilidad MEDIA: El daño ocurrirá en algunas ocasiones.</p>
			<p>Probabilidad BAJA: El daño ocurrirá raras veces.</p>
			<p>A la hora de establecer la probabilidad de daño, se debe considerar si las medidas de control ya implantadas son adecuadas. Los requisitos legales y los códigos de buena práctica para medidas específicas de control, también juegan un papel importante. </p>
			<p>Además de la información sobre las actividades de trabajo, se debe considerar lo siguiente:</p>
			<ul style='list-style-type:lower-alpha;'>
				<li>Trabajadores especialmente sensibles a determinados riesgos (características personales o estado biológico).</li>
				<li>Frecuencia de exposición al peligro.</li>
				<li>Fallos en el servicio. Por ejemplo: electricidad y agua.</li>
				<li>Fallos en los componentes de las instalaciones y de las máquinas, así como en los dispositivos de protección.</li>
				<li>Exposición a los elementos.</li>
				<li>Protección suministrada por los EPI y tiempo de utilización de estos equipos.</li>
				<li>Actos inseguros de las personas (errores no intencionados y violaciones intencionadas de los procedimientos):</li>
			</ul>
			<p>El cuadro siguiente da un método simple para estimar los niveles de riesgo de acuerdo a su probabilidad estimada y a sus consecuencias esperadas.</p>
			<p class='centrado'>NIVELES DE RIESGO</p>
			<table class='nivelesRiesgo'>
				<tr>
					<td colspan='2' rowspan='2' class='a34 noBordeLeft noBordeTop'></td>
					<td colspan='3' class='a66 conFondo'><b>CONSECUENCIAS</b></td>
				</tr>
				<tr>
					<td class='noBordeLeft conFondo'>Ligeramente<br/>Daniño<br/><b>LD</b></td>
					<td class='conFondo'>Daniño<br/><b>LD</b></td>
					<td class='conFondo'>Extremadamente<br/>Daniño<br/><b>ED</b></td>
				</tr>
				<tr>
					<td rowspan='3' class='conFondo'><b>PROBABILIDAD</b></td>
					<td class='a12 conFondo'>Baja<br/><b>B</b></td>
					<td class='panel-default'>Riesgo Trivial<br/><b>T</b></td>
					<td class='panel-amarillo'>Riesgo Tolerable<br/><b>TO</b></td>
					<td class='panel-warning'>Riesgo Moderado<br/><b>MO</b></td>
				</tr>
				<tr>
					<td class='a12 noBordeLeft conFondo'>Media<br/>M</td>
					<td class='panel-amarillo'>Riesgo Tolerable<br/><b>TO</b></td>
					<td class='panel-warning'>Riesgo Moderado<br/><b>MO</b></td>
					<td class='panel-danger'>Riesgo Importante<br/><b>RI</b></td>
				</tr>
				<tr>
					<td class='a12 noBordeLeft conFondo'>Alta<br/>A</td>
					<td class='panel-warning'>Riesgo Moderado<br/><b>MO</b></td>
					<td class='panel-danger'>Riesgo Importante<br/><b>RI</b></td>
					<td class='panel-inverse'>Riesgo Intolerable<br/><b>IN</b></td>
				</tr>
			</table>
		</div>

	</page>";

$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<ul style='list-style-type:none;'>
				<li>3.3. VALORACIÓN DEL RIESGO</li>
			</ul>
			<p>Los niveles de riesgos indicados en el cuadro anterior forman la base para decidir si se requiere mejorar los controles existentes o implantar unos nuevos, así como la temporización de las acciones. </p>
			<p>En la siguiente tabla se muestra un criterio sugerido como punto de partida para la toma de decisión. </p>
			<p>La tabla también indica que los esfuerzos precisos para el control de los riesgos y la urgencia con la que deben adoptarse las medidas de control deben ser proporcionales al riesgo.</p><br/><br/>
			<table class='nivelesRiesgo2'>
				<tr>
					<th class='a34'><b>RIESGO</b></th>
					<th class='a66'><b>ACCIÓN Y TEMPORIZACIÓN</b></th>
				</tr>
				<tr>
					<td class='a34 panel-default'>Trivial <b>(T)</b></td>
					<td class='a66 panel-default'>No se requiere acción específica.</td>
				</tr>
				<tr>
					<td class='a34 panel-amarillo'>Tolerable <b>(TO)</b></td>
					<td class='a66 panel-amarillo'>No se necesita mejorar la acción preventiva. Sin embargo, se deben considerar soluciones más rentables o mejoras que no supongan una carga económica importante.<br/>Se requieren comprobaciones periódicas para asegurar que se mantiene la eficacia de las medidas de control.</td>
				</tr>
				<tr>
					<td class='a34 panel-warning'>Moderado <b>(M)</b></td>
					<td class='a66 panel-warning'>Se deben hacer esfuerzos para reducir el riesgo, determinando las inversiones precisas. Las medidas para reducir el riesgo deben implantarse en un período determinado.<br/>Cuando el riesgo moderado está asociado con consecuencias extremadamente dañinas, se precisará una acción posterior para establecer, con más precisión, la probabilidad de daño como base para determinar la necesidad de mejora de las medidas de control.</td>
				</tr>
				<tr>
					<td class='a34 panel-danger'>Importante <b>(I)</b></td>
					<td class='a66 panel-danger'>No debe comenzarse el trabajo hasta que se haya reducido el riesgo. Puede que se precisen recursos considerables para controlar el riesgo. Cuando el riesgo corresponda a un trabajo que se está realizando, debe remediarse el problema en un tiempo inferior al de los riesgos moderados.</td>
				</tr>
				<tr>
					<td class='a34 panel-inverse'>Intolerable <b>(IN)</b></td>
					<td class='a66 panel-inverse'>No debe comenzar ni continuar el trabajo hasta que se reduzca el riesgo. Si no es posible reducir el riesgo, incluso con recursos ilimitados, debe prohibirse el trabajo.</td>
				</tr>
			</table>
		</div>

	</page>";

$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<ul style='list-style-type:none;'>
				<li>3.4. PRIORIZACIÓN</li>
			</ul>
			<p>Para cada factor de riesgo se deberán desarrollar una serie de medidas para controlar dicho riesgo. </p>
			<p>La priorización consiste en determinar una serie de valores para que sean tenidos en cuenta a la hora de planificar dichas medidas. </p>
			<p>Por ello tenemos los siguientes valores en cuanto a prioridad:</p>
			<table class='nivelesRiesgo3'>
				<tr>
					<th class='a20'><b>RIESGO</b></th>
					<th class='a20'><b>PRIORIDAD</b></th>
					<th class='a60'><b>TEMPORIZACIÓN</b></th>
				</tr>
				<tr>
					<td class='a20 panel-default'>Trivial <b>(T)</b></td>
					<td class='a20 panel-default'>Tendrá una prioridad BAJA</td>
					<td class='a60 panel-default'>Las medidas propuestas pueden tener un largo plazo de implantación (EN 12 MESES O MÁS) o bien sólo son necesarias medidas para controlar el mantenimiento de la situación.</td>
				</tr>
				<tr>
					<td class='a20 panel-amarillo'>Tolerable <b>(TO)</b></td>
					<td class='a20 panel-amarillo'>Tendrá una prioridad MEDIA/BAJA</td>
					<td class='a60 panel-amarillo'>Se requieren comprobaciones periódicas para asegurar que se mantiene la eficacia de las medidas de control.<br/>Las medidas propuestas pueden tener un largo plazo de implantación de HASTA 12 MESES.</td>
				</tr>
				<tr>
					<td class='a20 panel-warning'>Moderado <b>(M)</b></td>
					<td class='a20 panel-warning'>Tendrá una prioridad MEDIA/ALTA</td>
					<td class='a60 panel-warning'>Las medidas propuestas deben tener un medio plazo de implantación de HASTA 6 MESES.</td>
				</tr>
				<tr>
					<td class='a20 panel-danger'>Importante <b>(I)</b></td>
					<td class='a20 panel-danger'>Tendrá una prioridad ALTA </td>
					<td class='a60 panel-danger'>Las medidas propuestas deben tener un corto plazo de implantación de HASTA 3 MESES.</td>
				</tr>
				<tr>
					<td class='a20 panel-inverse'>Intolerable <b>(IN)</b></td>
					<td class='a20 panel-inverse'>Carácter INMEDIATO</td>
					<td class='a60 panel-inverse'>Debe tratarse INMEDIATAMENTE.</td>
				</tr>
			</table>
			<ul style='list-style-type:none;'>
				<li>4. PREPARACIÓN DE UN PLAN DE CONTROL DE RIESGOS</li>
			</ul>
			<p>El resultado de una evaluación de riesgos debe servir para hacer un inventario de acciones, con el fin de diseñar, mantener o mejorar los controles de riesgos. </p>
			<p>Es necesario contar con un buen procedimiento para planificar la implantación de las medidas de control que sean precisas después de la evaluación de riesgos.</p>
			<p>Los métodos de control deben escogerse teniendo en cuenta los siguientes principios:</p>
			<p>a. Combatir los riesgos en su origen<br/>
			b. Adaptar el trabajo a la persona, en particular en lo que respecta a la concepción de los puestos de trabajo, así como a la elección de los equipos y métodos de trabajo y de producción, con miras, en particular a atenuar el trabajo monótono y repetitivo y a reducir los efectos del mismo en la salud.<br/>
			c. Tener en cuenta la evolución de la técnica.<br/>
			d. Sustituir lo peligroso por lo que entrañe poco o ningún peligro<br/>
			e. Adoptar las medidas que antepongan la protección colectiva a la individual.<br/>
			f. Dar las debidas instrucciones a los trabajadores.</p>
		</div>

	</page>";

$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<ul style='list-style-type:none;'>
				<li>5. REVISIÓN DE LA EVALUACIÓN</li>
			</ul>
			<p>La evaluación de riesgos debe ser, en general, un proceso continuo. Por lo tanto, la adecuación de las medidas de control debe estar sujeta a una revisión continua y modificarse si es preciso. </p>
			<p>De igual forma, si cambian las condiciones de trabajo, y con ello varían los peligros y los riesgos, habrá de revisarse la evaluación de riesgos.</p>
			<p>La evaluación inicial deberá revisarse cuando así lo establezca una disposición específica. </p>
			<p>En todo caso, se deberá revisar la evaluación correspondiente a aquellos puestos de trabajo afectados cuando se hayan detectado daños a la salud de los trabajadores o se haya apreciado a través de los controles periódicos, incluidos los relativos a la vigilancia de la salud, que las actividades de prevención pueden ser inadecuadas o insuficientes. Para ello se tendrán en cuenta los resultados de:</p>
			<p>a. La investigación sobre las causas de los daños para la salud que se hayan producido.<br/>
			b. Las actividades para la reducción de los riesgos.<br/>
			c. Las actividades para el control de los riesgos.<br/>
			d. El análisis de la situación epidemiológica según los datos aportados por el sistema de información sanitaria u otras fuentes disponibles.</p>
			<p>Sin perjuicio de lo señalado en el apartado anterior, deberá revisarse igualmente la evaluación inicial con la periodicidad que se acuerde entre la empresa y los representantes de los trabajadores, teniendo en cuenta, en particular, el deterioro por el transcurso del tiempo de los elementos que integran el proceso productivo.</p>
			<ul style='list-style-type:none;'>
				<li>5.1. EVALUACIÓN PERIÓDICA</li>
			</ul>
			<p>A partir de la evaluación inicial, deberán volver a evaluarse los puestos de trabajo que puedan verse afectados por:</p>
			<p>a. La elección de equipos de trabajo, sustancias o preparados químicos, la introducción de nuevas tecnologías o la modificación en el acondicionamiento de los lugares de trabajo.<br/>
			b. El cambio en las condiciones de trabajo.<br/>
			c. La incorporación de un trabajador cuyas características personales o estado biológico conocido lo hagan especialmente sensible a las condiciones del puesto.</p>
			<ul style='list-style-type:none;'>
				<li>6. ÁMBITO DE VALIDEZ</li>
			</ul>
			<p>Los resultados que se desprenden de una evaluación de riesgos apuntan a la identificación, estimación y valoración de los tipos de riesgos existentes en la empresa, en el momento de la evaluación, para la seguridad y salud de los trabajadores en los lugares y puestos de trabajo, sin perjuicio de las evaluaciones específicas que deban realizarse por razones legales o de especial complejidad para obtener mayor precisión y confianza en los resultados.</p>
			<p>El empresario debe tener en consideración, que la presente evaluación inicial corresponde a las informaciones facilitadas y observaciones realizadas sobre las condiciones existentes durante el periodo de visitas efectuadas al efecto, y que requerirá del empresario completar, revisar o actualizar cuando existan nuevos datos o un cambio de las mencionadas condiciones de trabajo y en particular según los criterios siguiente:</p>
		</div>

	</page>";

$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<ol>
				<li>Las situaciones que no se contemplan en esta evaluación, debido a su complejidad o la imposibilidad de analizar directamente (salvo que hayan sido expresamente informadas por la empresa o sus representantes):
					<ul style='list-style-type:square;'>
						<li>Riesgos que puedan producirse en instalaciones sujetas a Reglamentación específica de Industria, por la no observancia de las revisiones exigibles o por el no cumplimento de las exigencias técnicas en la ejecución de las instalaciones.</li>
						<li>Riesgos en maquinaria o equipos de trabajo que no hayan sido informados por el fabricante o distribuidor oficial de los mismos y que por su naturaleza no sean posibles detectar en la observación directa del puesto de trabajo (fallos de diseño, defectos ocultos, inobservancia de las normas exigibles en el diseño y construcción de los mismos, etc.).</li>
						<li>Riesgos excepcionales no previsibles que puedan producirse por paradas de emergencia o no planificadas de instalaciones, maquinarias o equipos.</li>
						<li>Riesgos producidos por paradas totales de las instalaciones, para reformas, mantenimiento puesta a punto y ajustes de la instalación, salvo que dichas actividades se realicen de forma habitual y planificada.</li>
						<li>Riesgos producidos como consecuencia de imprudencia del usuario.</li>
						<li>Riesgos producidos por falta de experiencia o formación para el trabajo.</li>
						<li>En general todos aquellos riesgos derivados de situaciones que no hayan sido informadas por la empresa, trabajadores o sus representantes y que por su naturaleza no sea factible observar o identificar en el proceso de tomas de datos.</li>
					</ul>
				</li>
				<li>La evaluación debe ser actualizada:
					<ul style='list-style-type:square;'>
						<li>Cuando por razón de los resultados de las evaluaciones específicas sea procedente actualizar dicha evaluación.</li>
						<li>Cuando se hayan aplicado o hecho efectivas las medidas contempladas en la planificación de la actividad preventiva para el control de los riesgos.</li>
					</ul>
				</li>
				<li>Revisión de la evaluación (de acuerdo con los criterios legales):
					<ul style='list-style-type:square;'>
						<li>Cuando así los establezca una disposición específica (RSP art. 6-1).</li>
						<li>Cuando se elijan nuevos equipos de trabajo, sustancias o preparados químicos, se introduzcan nuevas tecnologías o se modifique el acondicionamiento de los lugares de trabajo (LPRL art. 16-1) (RSP art. 4-2a).</li>
						<li>Cuando existan cambios en las con (L.P.R.L. art. 16-1) (R.S.P. art. 4-2a).</li>
						<li>Condiciones de trabajo por modificación del proceso, etc. (LPRL art. 16-1) (RSP art. 4-2b).</li>
						<li>Por la incorporación de un trabajador menor de 18 años o cuyas características o estado biológico le hagan especialmente sensibles a determinados riesgos (LPRL art. 27-1 y art. 25-2) (RSP art. 4-2c).</li>
						<li>Cuando en caso de maternidad y periodo de lactancia, no se hubiese contemplado esta situación específica en la evaluación inicial (LPRL art. 26.1 y 3) (RSP art. 4-2c).</li>
						<li>Cuando en los controles periódicos de las condiciones de seguridad se haya detectado que las actividades preventivas son insuficientes o inadecuadas (LPRL art. 16) (RSP art. 6-1).</li>
						<li>Cuando en los controles periódicos de la vigilancia de la salud se haya detectado que las actividades preventivas son insuficientes o inadecuadas (LPRL art. 16-3) (RSP art. 6-1).</li>
						<li>Cuando se produzcan daños para la salud. (LPRL art. 16-1) RSP art. 6-1).</li>
					</ul>
				</li>
			</ol>
		</div>

	</page>";

$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<ol style='list-style-type:none;'>
				<li>
					<ul style='list-style-type:square;'>
						<li>Cuando exista una situación epidemiológica según datos aportados por las autoridades sanitarias u otras fuentes. (RSP art.6-1).</li>
						<li>Cuando se acuerde con los representantes de los trabajadores teniendo en cuenta el deterioro a lo largo del tiempo de los medios empleados en el proceso productivo. (RSP art.6-2).</li>
					</ul>
				</li>
			</ol>
			<ul style='list-style-type:none;'>
				<li>7. PLANIFICACIÓN DE LA ACTIVIDAD PREVENTIVA</li>
			</ul>
			<p>Conforme a los art. 8 y 9 de RD 39/97 de 17 de enero, cuando el resultado de la evaluación de riesgos pusiera de manifiesto situaciones de riesgo, el empresario planificará la actividad preventiva que proceda con objeto de eliminar o controlar y reducir dichos riesgos, conforme a un orden de prioridades en función de su magnitud y número de trabajadores expuestos a los mismos.</p>
			<p>OBJETIVO Y CONTENIDO DE LA PLANIFICACIÓN:</p>
			<p>El fin de la Planificación es implantar acciones necesarias para un eficaz control de los riesgos.</p>
			<p>Deberá establecer:</p>
			<p>a. Cómo y cuándo hacerla y quien debe hacerla.<br/>
			b. Objetivo y metas a conseguir.<br/>
			c. Asignación de prioridades y plazos para los objetivos y metas establecidos.<br/>
			d. Asignación de recursos y medidas.<br/>
			e. Seguimiento periódico de la consecución de objetivos.</p>
			<p>Para controlar el cumplimiento de los objetivos marcados en la evaluación de riesgos y planificación anual se establece el modelo de control, en el cual se recoge para cada peligro las acciones preventivas acordadas y se refleja un plazo en función de la prioridad de dicho riesgo así como la designación de un responsable de ejecución, el coste y la fecha de finalización de las mismas, una vez realizadas en su totalidad, consignando además su coste al objeto de recoger los datos precisos para poder realizar la memoria anual del Servicio de Prevención.</p>
			<p>Una vez finalizadas las medidas contempladas en la evaluación de riesgos deberá evaluarse su eficacia, comprobándose si se han logrado o no los objetivos previstos. Se reflejarán las observaciones sobre la eficacia de las medidas implantadas por si fuese preciso acometer otras al no haberse controlado aún el riesgo adecuadamente.</p>
			<p>Se designará, por tanto, a la persona encargada de comprobar que todas las medias establecidas propuestas en el apartado anterior se han llevado a cabo dentro de los plazos estipulados.</p>
			<p>En el modelo de control y seguimiento de las medidas preventivas deberá quedar constancia escrita (mediante firma y fecha) de:</p>
			<ul style='list-style-type:square;'>
				<li>La implantación de las medidas propuestas.</li>
				<li>La comprobación de dicha implantación.</li>
				<li>El presupuesto asignado o dedicado a tales medidas.</li>
			</ul>
			<p>El plan de actuación debe revisarse antes de su implantación, considerando lo siguiente:</p>
		</div>
	</page>";
if($datos['tipoEvaluacion']=='LUGAR'){
$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<p>a. Si los nuevos sistemas de control de riesgos conducirán a niveles de riesgo aceptables.<br/>
			b. Si los nuevos sistemas de control han generado nuevos peligros.<br/>
			c. La opinión de los trabajadores afectados sobre la necesidad y la operatividad de las nuevas medidas de control.</p>
			<bookmark title='6. EVALUACIÓN DE RIESGOS DEL CENTRO DE TRABAJO' level='0' ></bookmark>
			<h1 class='tituloSeccion'>6. EVALUACIÓN DE RIESGOS DEL CENTRO DE TRABAJO</h1>
			<table class='tablaDatos'>
				<tr>
					<th class='a35'>CENTRO DE TRABAJO</th>
					<td class='a65'>".$datos['EMPNOMBRE']."</td>
				</tr>
				<tr>
					<th class='a35'>DIRECCIÓN</th>
					<td class='a65'>".$datos['direccionCentro']."</td>
				</tr>
				<tr>
					<th class='a35'>TIPO DE EVALUACIÓN</th>
					<td class='a65'>".$tipoEvaluacion."</td>
				</tr>
				<tr>
					<th class='a35'>FECHA DE ELABORACIÓN</th>
					<td class='a65'>".formateaFechaWeb($datos['fecha'])."</td>
				</tr>
				<tr>
					<th class='a35'>MOTIVO</th>
					<td class='a65'>".$datos['motivo']."</td>
				</tr>
				<tr>
					<th class='a35'>MÉTODO DE EVALUACIÓN</th>
					<td class='a65'>Procedimiento de evaluación de riesgos</td>
				</tr>
			</table>
			<br/>
			<div class='tituloConRelleno'>DESCRIPCIÓN DEL CENTRO</div>
			<p>".$datos['pregunta'.$datos['codigoCentro'].'56']."</p>
			".$mapa."
			<p>Instalaciones</p>
			<p>Entre las instalaciones con la que cuenta el centro podemos destacar:</p>
			<table style='width:100%;'>";
			$listas=array();
			$listas[0]="<tr><td style='width:50%;'><ul style='list-style-type=square;'>";
			$listas[1]="<td style='width:50%;'><ul style='list-style-type=square;>";
			$iL=0;
			$instalaciones=consultaBD('SELECT nombre FROM instalaciones_formulario_prl INNER JOIN instalaciones ON instalaciones_formulario_prl.codigoInstalacion=instalaciones.codigo WHERE codigoFormularioPRL='.$datos['codigoFormulario'],true);
			while($item=mysql_fetch_assoc($instalaciones)){
				$listas[$iL].='<li>'.$item['nombre'].'</li>';
				$iL=$iL==0?1:0;
			}
			$listas[0].="</ul></td>";
			$listas[1].="</ul></td></tr></table>";
$contenido.=$listas[0].$listas[1]."			
		</div>
	</page>";

$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<div class='tituloConRelleno'>TAREAS</div><br/><br/>
			<table class='tablaDatos'>
				<tr>
					<th class='a30'>PUESTO</th>
					<th class='a50'>TAREAS</th>
					<th class='a20'>ZONA DE<br/>TRABAJO</th>
				</tr>";
			$puestos=consultaBD('SELECT puestos_trabajo.nombre, GROUP_CONCAT(DISTINCT IF(area="",NULL,area) SEPARATOR ", ") AS area, tareas, GROUP_CONCAT(DISTINCT IF(funciones="",NULL,funciones) SEPARATOR ", ") AS funciones FROM empleados LEFT JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo INNER JOIN funciones_formulario_prl ON empleados.codigo=funciones_formulario_prl.codigoEmpleado  WHERE codigoCentroTrabajo='.$datos['codigoCentro'].' GROUP BY puestos_trabajo.codigo' ,true);
			while($item=mysql_fetch_assoc($puestos)){
				$contenido.='<tr>';
				$contenido.='<td class="a30">'.$item['nombre'].'</td>';
				$contenido.='<td class="a50">'.$item['funciones'].'</td>';
				$contenido.='<td class="a20">'.$item['area'].'</td>';
				$contenido.='</tr>';
			}
$contenido.="		
		</table>
		<br/><br/>
		<div class='tituloConRelleno'>RELACIÓN DE TRABAJADORES</div><br/><br/>
		<table class='tablaDatos'>
				<tr>
					<th class='a70'>NOMBRE</th>
					<th class='a30'>PUESTO DE TRABAJO</th>
				</tr>";
				$epis=array();
				while($empleado=mysql_fetch_assoc($empleados)){
					$contenido.="
					<tr>
						<td class='a70'>".$empleado['nombre']."</td>
						<td class='a30'>".$empleado['puesto']."</td>
					</tr>";
					$listado=consultaBD('SELECT nombre FROM epis INNER JOIN epis_de_puesto_trabajo ON epis.codigo=epis_de_puesto_trabajo.codigoEPI WHERE codigoPuestoTrabajo='.$empleado['codigoPuesto'],true);
					while($item=mysql_fetch_assoc($listado)){
						if(!in_array($item['nombre'], $epis)){
							array_push($epis, $item['nombre']);
						}
					}
				}
$contenido.="
		</table>";
$contenido.="
		</div>
	</page>";
$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<div class='tituloConRelleno'>FORMACIÓN E INFORMACIÓN</div><br/><br/>
			<p>Para el acceso al centro de trabajo, debe informarse acerca de los riesgos existentes en el mismo, así como de las medidas establecidas para eliminarlos o minimizarlos (este documento), y de las medidas de emergencias y primeros auxilios según artículos 19 y 20 de la Ley de Prevención de Riesgos Laborales.</p>
			<div class='tituloConRelleno'>PRODUCTOS O SUSTANCIAS</div><br/><br/>";
			if(isset($datos['pregunta'.$datos['codigoCentro'].'57']) && $datos['pregunta'.$datos['codigoCentro'].'57']!=''){
				$productos=str_replace('font-family:Wingdings;', '', $datos['pregunta'.$datos['codigoCentro'].'57']);
			} else {
				$productos='DESCRIBIR SOMERAMENTE EN LA TOMA DE DATOS DEL CLIENTE QUÉ TIPO DE PRODUCTOS SON O PARA QUÉ SON UTILIZADOS. POR EJEMPLO: <br/>
					Los productos químicos utilizados en las distintas operaciones llevadas a cabo en el taller son, básicamente, aceites lubricantes , disolventes, pinturas,…..<br/>
					Esporádicamente, se utilizan otros productos como………';
			}
$contenido.="
			<p>".$productos."</p>
			<p>Se adjunta a la presente evaluación un anexo con un listado de las sustancias empleadas. Esta información es facilitada por ".$datos['pregunta0']."</p>
			<p>Se deben etiquetar, almacenar y utilizar los productos químicos según el Real Decreto 374/2001, de 6 de abril, sobre la protección de la salud y seguridad de los trabajadores contra los riesgos relacionados con los agentes químicos durante el trabajo, el Reglamento (CE) nº 1272/2008 del Parlamento Europeo y del Consejo, de 16 de diciembre de 2008, sobre clasificación, etiquetado y envasado de sustancias y mezclas (CLP), el Reglamento 1907/2006 del Parlamento Europeo y del Consejo, de 18 de diciembre de 2006, relativo al registro, la evaluación, la autorización y la restricción de las sustancias y preparados químicos (REACH) y el Real Decreto 379/2001, de 6 de abril, por el que se aprueba el Reglamento de almacenamiento de productos químicos (RAPQ) y sus instrucciones técnicas complementarias (ITC MIE APQ) y las Fichas de Datos de Seguridad de cada producto.</p>
			<p>Además, se recuerda que, de conformidad con lo establecido en el título IV del Reglamento (CE) 1907/ 2006 relativo al registro, la evaluación, la autorización y la restricción de las sustancias y preparados químicos (Reglamento REACH), el proveedor de una sustancia o mezcla peligrosa debe facilitar al destinatario del mismo una Ficha de Datos de Seguridad (FDS). Por tanto, se recomienda que cada vez que se adquiera una sustancia peligrosa se solicite al suministrador la FDS del producto, dejando ésta siempre accesible a los trabajadores que utilicen dicha sustancia y conociendo éstos el contenido de la misma.</p>
			<div class='tituloConRelleno'>EQUIPOS DE TRABAJO / MÁQUINAS</div><br/><br/>
			<p>Se entiende por equipo de trabajo cualquier máquina aparato, instrumento o instalación utilizado en el trabajo. La utilización de dichos equipos corresponde a cualquier actividad: esto es, puesta en marcha, detención, empleo, transporte, reparación, transformación, mantenimiento, conservación y limpieza.</p>
			<p>Una vez definido que es un equipo de trabajo , indicamos a continuación , una relación una de las máquinas y herramientas.</p>
			<p>Las clasificamos en dos grupos: por un lado las correspondientes a maquinarias fijas y por otro las máquinas manuales o herramientas.</p>
			</div>
		</page>";

$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		<div class='container'>
			<p><b>Maquinaria fija</b></p>
			<p>Son aquellas máquinas puestas a disposición de los trabajadores para el desarrollo de sus tareas y que requieren un correcto conocimiento de su funcionamiento por partes de los mismos.	</p>
			<table style='width:100%;'>";
			$listas=array();
			$listas[0]="<tr><td style='width:50%;'><ul>";
			$listas[1]="<td style='width:50%;'><ul>";
			$iL=0;	
			$maquinaria=consultaBD('SELECT * FROM maquinaria_formulario_prl WHERE codigoFormularioPRL='.$datos['codigoFormulario'].' AND codigoCentroTrabajo='.$datos['codigoCentro'].' AND tipo="FIJA"',true);
			while($item=mysql_fetch_assoc($maquinaria)){
				$listas[$iL].='<li>'.$item['nombre'].'</li>';
				$iL=$iL==0?1:0;
			}
			$listas[0].="</ul></td>";
			$listas[1].="</ul></td></tr></table>";
			$datos['pregunta4']=datosRegistro('actividades',$datos['pregunta4']);
$contenido.=$listas[0].$listas[1]."		
			<p><b>Herramientas y útiles</b></p>
			<p>Se utiliza todo tipo de herramientas necesarias para llevar a cabo las labores propias de un/a ".$datos['pregunta4']['nombre'].". Normalmente están distribuidas en diferentes cuadros y mesas de trabajo	</p>
			<ul>";
			$maquinaria=consultaBD('SELECT * FROM maquinaria_formulario_prl WHERE codigoFormularioPRL='.$datos['codigoFormulario'].' AND codigoCentroTrabajo='.$datos['codigoCentro'].' AND tipo="MANUAL"',true);
			while($item=mysql_fetch_assoc($maquinaria)){
				$contenido.='<li>'.$item['nombre'].'</li>';
			}
$contenido.="</ul>		
			<div style='border:1px solid #000;padding:3px;'><p>En el caso de existir máquinas sin marcado CE, el trabajador designado o el recurso preventivo velará por el cumplimiento de las medidas preventivas previstas en el presente documento al efecto.</p></div>
			<p>Se anexa  a la presente evaluación una descripción de los equipos de trabajo / maquinaria utilizados, según la información facilitada por ".$datos['pregunta0']." y los datos recabados durante la visita.</p>
			<p>El empresario adoptará las medidas necesarias para que los equipos de trabajo que se pongan a disposición de los trabajadores sean adecuados al trabajo que deba realizarse y convenientemente adaptados al mismo, de forma que garanticen la seguridad y salud de los trabajadores al utilizar dichos equipos de trabajo.</p>
			<p>Por norma general, todos los equipos de trabajo / maquinaria que se utilicen en ".$datos['pregunta0']." deberán cumplir los siguientes requisitos:</p>";
$contenido.="
		</div>
	</page>";

$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<ul style='list-style-type:square;'>
				<li>MÁQUINAS COMERCIALIZADAS Y/O PUESTAS EN SERVICIO A PARTIR DEL 1 DE ENERO DE 1995 (Fecha de aplicación obligatoria del Real Decreto 1435/1992 ):
					<ul style='list-style-type:none;'>
						<li>- Deben ir provistas del “marcado CE”.</li>
						<li>- Deben disponer de la declaración “CE” de conformidad, redactada en castellano, que deberá comprender, entre otras cosas: el nombre y la dirección del fabricante o de su representante legalmente establecido en la Comunidad; descripción de la máquina y todas las disposiciones pertinentes a las que se ajuste la máquina.</li>
						<li>- Cada máquina debe llevar un manual de instrucciones redactado, como mínimo, en castellano, en el que se indique, entre otras cosas: la instalación, la puesta en servicio, la utilización, el mantenimiento, etc.</li>
					</ul>
				</li>
				<li>MÁQUINAS EXISTENTES EN LA EMPRESA CON ANTERIORIDAD AL 27 DE AGOSTO DE 1997 (Fecha de entrada en vigor del Real Decreto 1215/1997):
					<ol>
						<li>Si las máquinas fueron adquiridas con posterioridad al 1 de enero de 1995, el usuario está obligado a garantizar, a través de un mantenimiento adecuado, que las prestaciones iniciales de la máquina en materia de seguridad se conservan a lo largo de la vida de la misma.</li>
						<li>Si las máquinas fueron adquiridas con anterioridad al 1 de enero de 1995, con carácter general, no irán con el “marcado CE”, ni acompañadas de la declaración “CE” de conformidad ni del manual de instrucciones, aunque es posible que algunas máquinas comercializadas a partir del 1 de enero de 1993 ya dispusieran de estos requisitos. En estas máquinas se deben identificar y evaluar los posibles riesgos existentes e implantar las medidas oportunas que, como mínimo, se ajustarán a los requisitos del Anexo I del citado Real Decreto.</li>
					</ol>
				</li>
			</ul>
			<div class='tituloConRelleno'>EQUIPOS DE PROTECCIÓN INDIVIDUAL (EPI)</div><br/><br/>
			<ul>";
			foreach ($epis as $key => $value) {
				$contenido.='<li>'.$value.'</li>';
			}
$contenido.="</ul>
			<p>Los vehículos, entre otros requisitos, deberán tener pasada la correspondiente Inspección Técnica del Vehículo (ITV).</p>
			</div>
		</page>";
$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<p><u><b>EXPOSICIÓN A AGENTES FÍSICOS</b></u></p>
			<p><b>RUIDO:</b> ".nl2br($datos['pregunta'.$datos['codigoCentro'].'58'])."</p>
			<p><b>VIBRACIONES:</b> ".nl2br($datos['pregunta'.$datos['codigoCentro'].'61'])."</p>
			<p><b>RADIACIONES:</b> ".nl2br($datos['pregunta'.$datos['codigoCentro'].'59'])."</p>
			<p><b>TEMPERATURA, HUMEDAD Y VELOCIDAD DEL AIRE:</b> ".nl2br($datos['pregunta'.$datos['codigoCentro'].'62'])."</p>
			<p><b>LUZ:</b> ".nl2br($datos['pregunta'.$datos['codigoCentro'].'60'])."</p>
			<p><u><b>EXPOSICIÓN A AGENTES BIOLÓGICOS:</b></u> ".nl2br($datos['pregunta'.$datos['codigoCentro'].'63'])."</p>
			<p><u><b>EXPOSICIÓN A AGENTES QUÍMICOS</b></u></p>
			<p>".nl2br($datos['pregunta'.$datos['codigoCentro'].'64'])."</p>
		</div>
	</page>";

} else if($datos['tipoEvaluacion']=='PUESTOS'){
$puesto=consultaBD('SELECT puestos_trabajo.codigo, puestos_trabajo.nombre, puestos_trabajo.tareas, GROUP_CONCAT(funciones_formulario_prl.funciones SEPARATOR ", ") AS funciones, GROUP_CONCAT(funciones_formulario_prl.area SEPARATOR ", ") AS area, descripcion, GROUP_CONCAT(productos SEPARATOR ", ") AS productos, GROUP_CONCAT(CONCAT(empleados.nombre," ",empleados.apellidos) SEPARATOR ", ") AS empleados, GROUP_CONCAT(maquinaria SEPARATOR ", ") AS maquinaria, GROUP_CONCAT(herramientas SEPARATOR ", ") AS herramientas, GROUP_CONCAT(medio SEPARATOR ", ") AS medio FROM funciones_formulario_prl INNER JOIN empleados ON funciones_formulario_prl.codigoEmpleado=empleados.codigo INNER JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo WHERE funciones_formulario_prl.codigoCentroTrabajo='.$datos['centroPuesto'].' AND puestos_trabajo.codigo='.$datos['codigoPuesto'],true,true);

$contenido.="
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
			<bookmark title='6. EVALUACIÓN DE RIESGOS DEL PUESTO: ".$puesto['nombre']."' level='0' ></bookmark>
			<h1 class='tituloSeccion'>6. EVALUACIÓN DE RIESGOS DEL PUESTO: ".$puesto['nombre']."</h1>";
$referencia='P.'.substr(strtoupper($puesto['nombre']),0,3);
$epis=consultaBD('SELECT nombre FROM epis_de_puesto_trabajo INNER JOIN epis ON epis_de_puesto_trabajo.codigoEPI=epis.codigo WHERE codigoPuestoTrabajo='.$datos['codigoPuesto'],true);
$entra=false;
$listadoEpis='';
while($epi=mysql_fetch_assoc($epis)){
	$entra=true;
	$listadoEpis.=$epi['nombre'].' ';
}
if(!$entra){
	$listadoEpis='No procede';
}

$sensibles=consultaBD('SELECT GROUP_CONCAT(CONCAT(empleados.nombre," ",empleados.apellidos) SEPARATOR ", ") AS empleados FROM empleados INNER JOIN empleados_sensibles_formulario_prl ON empleados.codigo=empleados_sensibles_formulario_prl.codigoEmpleado WHERE empleados_sensibles_formulario_prl.codigoCentroTrabajo='.$datos['centroPuesto'].' AND empleados.codigoPuestoTrabajo='.$datos['codigoPuesto'],true,true);
$contenido.="
			<table class='tablaDatos'>
				<tr>
					<th class='a30'>DENOMINACIÓN DEL PUESTO</th>
					<td class='a70'>".$puesto['nombre']."</td>
				</tr>
				<tr>
					<th class='a30'>AREAS O PROCESOS:</th>
					<td class='a70'>".$puesto['area']."</td>
				</tr>
				<tr>
					<th class='a30'>DESCRIPCIÓN:</th>
					<td class='a70'>".nl2br($puesto['tareas'])."</td>
				</tr>
				<tr>
					<th class='a30'>TRABAJADORES QUE HABITUALMENTE LO DESEMPEÑAN</th>
					<td class='a70'>".$puesto['empleados']."</td>
				</tr>
				<tr>
					<th class='a30'>TRABAJADORES SENSIBLES:</th>
					<td class='a70'>".$sensibles['empleados']."</td>
				</tr>
				<tr>
					<th class='a30'>FUNCIONES/TAREAS</th>
					<td class='a70'>".$puesto['funciones']."</td>
				</tr>
				<tr>
					<th class='a30'>MAQUINARIA Y EQUIPOS USADOS</th>
					<td class='a70'>".$puesto['maquinaria']."</td>
				</tr>
				<tr>
					<th class='a30'>EPIs</th>
					<td class='a70'>".$listadoEpis."</td>
				</tr>
				<tr>
					<th class='a30'>HERRAMIENTAS</th>
					<td class='a70'>".$puesto['herramientas']."</td>
				</tr>
				<tr>
					<th class='a30'>MEDIOS MECANICOS DE CARGA</th>
					<td class='a70'>".$puesto['medio']."</td>
				</tr>
				<tr>
					<th class='a30'>SUSTANCIAS Y PRODUCTOS</th>
					<td class='a70'>".$puesto['productos']."</td>
				</tr>
				<tr>
					<th class='a30'>OBSERVACIONES</th>
					<td class='a70'>".$puesto['descripcion']."</td>
				</tr>
			</table>
			</div>
	</page>";	
} else {
	$contenido.="
	<page backtop='30mm' backleft='24mm' backright='20mm' backbottom='0mm' footer='page' backimg='../img/fondoOfertaBN.jpg'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div>
			<bookmark title='6. EVALUACIÓN DE RIESGOS DE ".$datos['otros']."' level='0' ></bookmark>
			<h1 class='tituloSeccion tituloSeccionOtro'>6. EVALUACIÓN DE RIESGOS DE ".$datos['otros']."</h1>
		</div>";

}

if($datos['tipoEvaluacion']=='LUGAR'){
$contenido.="
	<page backtop='30mm' backleft='24mm' backright='20mm' backbottom='0mm' footer='page' backimg='../img/fondoOfertaBN.jpg'>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container3'>
			<div class='tituloConRelleno'>EVALUACIÓN DE RIESGOS Y PROPUESTA DE MEDIDAS PREVENTIVAS</div><br/><br/>
			<p>Los riesgos del centro de trabajo (riesgos de lugar) se designarán con un número precedido de la letra L.</p>";
		$contenido.=listadoRiesgos($datos['codigoEvaluacion'],'L',$logoCliente,$datos['fecha']);
$contenido.="	
		</div></page>";
$numSec=7;
while($evaluacion=mysql_fetch_assoc($evaluacionesPuestos)){
//echo $evaluacion['codigo'].'<br/>';
$puesto=consultaBD('SELECT puestos_trabajo.nombre, puestos_trabajo.codigo, funciones_formulario_prl.codigoCentroTrabajo, tareas, GROUP_CONCAT(DISTINCT IF(funciones="",NULL,funciones) SEPARATOR ", ") AS funciones, GROUP_CONCAT(DISTINCT IF(area="",NULL,area) SEPARATOR ", ") AS area, descripcion, GROUP_CONCAT(DISTINCT IF(productos="",NULL,productos) SEPARATOR ", ") AS productos, GROUP_CONCAT(CONCAT(empleados.nombre," ",empleados.apellidos) SEPARATOR ", ") AS empleados, GROUP_CONCAT(DISTINCT IF(maquinaria="",NULL,maquinaria) SEPARATOR ", ") AS maquinaria, GROUP_CONCAT(DISTINCT IF(herramientas="",NULL,herramientas) SEPARATOR ", ") AS herramientas, GROUP_CONCAT(DISTINCT IF(medio="",NULL,medio) SEPARATOR ", ") AS medio FROM puestos_trabajo INNER JOIN empleados ON puestos_trabajo.codigo=empleados.codigoPuestoTrabajo INNER JOIN funciones_formulario_prl ON empleados.codigo=funciones_formulario_prl.codigoEmpleado WHERE funciones_formulario_prl.codigoCentroTrabajo='.$evaluacion['codigoCentroTrabajo'].' AND empleados.codigoPuestoTrabajo='.$evaluacion['codigoPuesto'],true,true);

//if($numSec>7){
	$contenido.="<page backtop='30mm' backleft='24mm' backright='20mm' backbottom='0mm' footer='page' backimg='../img/fondoOfertaBN.jpg'>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>";
//}
$contenido.="
	
		
		<div class='container3'>
			<bookmark title='".$numSec.". EVALUACIÓN DE RIESGOS DEL PUESTO: ".$puesto['nombre']."' level='0' ></bookmark>
			<h1 class='tituloSeccion'>".$numSec.". EVALUACIÓN DE RIESGOS DEL PUESTO: ".$puesto['nombre']."</h1>";
$referencia='P.'.substr(strtoupper($puesto['nombre']),0,3);
$epis=consultaBD('SELECT nombre FROM epis_de_puesto_trabajo INNER JOIN epis ON epis_de_puesto_trabajo.codigoEPI=epis.codigo WHERE codigoPuestoTrabajo='.$puesto['codigo'],true);
$entra=false;
$listadoEpis='';
while($epi=mysql_fetch_assoc($epis)){
	$entra=true;
	$listadoEpis.=$epi['nombre'].' ';
}
if(!$entra){
	$listadoEpis='No procede';
}

$sensibles=consultaBD('SELECT GROUP_CONCAT(CONCAT(empleados.nombre," ",empleados.apellidos) SEPARATOR ", ") AS empleados FROM empleados INNER JOIN empleados_sensibles_formulario_prl ON empleados.codigo=empleados_sensibles_formulario_prl.codigoEmpleado WHERE empleados_sensibles_formulario_prl.codigoCentroTrabajo='.$evaluacion['codigoCentroTrabajo'].' AND empleados.codigoPuestoTrabajo='.$evaluacion['codigoPuesto'],true,true);
$contenido.="
			<table class='tablaDatos'>
				<tr>
					<th class='a30'>DENOMINACIÓN DEL PUESTO</th>
					<td class='a70'>".$puesto['nombre']."</td>
				</tr>
				<tr>
					<th class='a30'>AREAS O PROCESOS:</th>
					<td class='a70'>".$puesto['area']."</td>
				</tr>
				<tr>
					<th class='a30'>DESCRIPCIÓN:</th>
					<td class='a70'>".nl2br($puesto['tareas'])."</td>
				</tr>
				<tr>
					<th class='a30'>TRABAJADORES QUE HABITUALMENTE LO DESEMPEÑAN</th>
					<td class='a70'>".$puesto['empleados']."</td>
				</tr>
				<tr>
					<th class='a30'>TRABAJADORES SENSIBLES:</th>
					<td class='a70'>".$sensibles['empleados']."</td>
				</tr>
				<tr>
					<th class='a30'>FUNCIONES/TAREAS</th>
					<td class='a70'>".$puesto['funciones']."</td>
				</tr>
				<tr>
					<th class='a30'>MAQUINARIA Y EQUIPOS USADOS</th>
					<td class='a70'>".$puesto['maquinaria']."</td>
				</tr>
				<tr>
					<th class='a30'>EPIs</th>
					<td class='a70'>".$listadoEpis."</td>
				</tr>
				<tr>
					<th class='a30'>HERRAMIENTAS</th>
					<td class='a70'>".$puesto['herramientas']."</td>
				</tr>
				<tr>
					<th class='a30'>MEDIOS MECANICOS DE CARGA</th>
					<td class='a70'>".$puesto['medio']."</td>
				</tr>
				<tr>
					<th class='a30'>SUSTANCIAS Y PRODUCTOS</th>
					<td class='a70'>".$puesto['productos']."</td>
				</tr>
				<tr>
					<th class='a30'>OBSERVACIONES</th>
					<td class='a70'>".$puesto['descripcion']."</td>
				</tr>
			</table>
			<br/>
			<div class='tituloConRelleno'>EVALUACIÓN DE RIESGOS Y PROPUESTA DE MEDIDAS PREVENTIVAS</div>
			<p>Los riesgos del puesto de ".$puesto['nombre']." se designarán con un número precedido de las letras \"".$referencia."\".</p>
	</div>
	</page>";
	$numSec++;

$contenido.="
	<page backtop='30mm' backleft='24mm' backright='20mm' backbottom='0mm' footer='page' backimg='../img/fondoOfertaBN.jpg'>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container3'>";
		$contenido.=listadoRiesgos($evaluacion['codigo'],$referencia,$logoCliente,$datos['fecha']);
$contenido.='
		</div>
	</page>';
}//CIERRE DE WHILE DE PUESTOS

	/*if($numSec==7){
		//$contenido.='</page>';
	}*/
} else if($datos['tipoEvaluacion']=='PUESTOS'){ //ELSE POR SI LA EVALUACIÓN ES DE PUESTOS

$contenido.="
	<page backtop='30mm' backleft='24mm' backright='20mm' backbottom='0mm' footer='page' backimg='../img/fondoOfertaBN.jpg'>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>EVALUACIÓN DE RIESGOS</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container3'>
		<div class='tituloConRelleno'>EVALUACIÓN DE RIESGOS Y PROPUESTA DE MEDIDAS PREVENTIVAS</div>
			<p>Los riesgos del puesto de ".$puesto['nombre']." se designarán con un número precedido de las letras \"".$referencia."\".</p>";
		$contenido.=listadoRiesgos($datos['codigoEvaluacion'],$referencia,$logoCliente,$datos['fecha']);
$contenido.='	
		</div>
	</page>';

} else { 
$referencia='O.'.substr(strtoupper($datos['otros']),0,3);
$contenido.="
		<div class='container3'>
		<div class='tituloConRelleno'>EVALUACIÓN DE RIESGOS Y PROPUESTA DE MEDIDAS PREVENTIVAS</div>
			<p>Los riesgos de esta evaluación se designarán con un número precedido de las letras \"".$referencia."\".</p>";
		$contenido.=listadoRiesgos($datos['codigoEvaluacion'],$referencia,$logoCliente,$datos['fecha']);
$contenido.='	
		</div>
	</page>';
}

$contenido=substr($contenido, 0,-13);
/*$contenido.='<br/><br/><h1>INFORME ELABORADO POR '.$usuario['nombre']." ".$usuario['apellidos'].' EL '.formateaFechaWeb($datos['fecha']).'</h1></div></page>';*/
$contenido.='</div></page>';
	return $contenido;
}

function planificacionPreventiva($objPHPExcel,$codigo){
	conexionBD();
	$datos=consultaBD('SELECT evaluacion_general.*, clientes.EMPNOMBRE, puestos_trabajo.nombre AS nombrePuesto, clientes_centros.direccion AS direccionCentro, clientes_centros.nombre AS nombreCentro 
		FROM evaluacion_general 
		INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo 
		LEFT JOIN clientes_centros ON evaluacion_general.codigoCentroTrabajo=clientes_centros.codigo
		LEFT JOIN puestos_trabajo ON evaluacion_general.codigoPuestoTrabajo=puestos_trabajo.codigo
		WHERE evaluacion_general.codigo='.$codigo,false,true);
	$riesgos=consultaBD('SELECT riesgos_evaluacion_general.*, riesgos.nombre AS nombreRiesgo, codigoInterno  FROM riesgos_evaluacion_general INNER JOIN riesgos ON riesgos_evaluacion_general.codigoRiesgo=riesgos.codigo WHERE codigoEvaluacion='.$codigo);
	cierraBD();
	$tipos=array('LUGAR'=>'CENTRO DE TRABAJO: ','PUESTO'=>'PUESTO DE TRABAJO: ','OTRAS'=>'');
	$prefijo='';
	if($datos['tipoEvaluacion']=='LUGAR'){
		if($datos['nombreCentro']==''){
			$tipo='CENTRO DE TRABAJO: '.$datos['direccionCentro'];
		} else {
			$tipo='CENTRO DE TRABAJO: '.$datos['nombreCentro'].' ('.$datos['direccionCentro'].')';
		}
		$prefijo='L ';
	} else if($datos['tipoEvaluacion']=='PUESTOS'){
		$tipo='PUESTO DE TRABAJO: '.$datos['nombrePuesto'];
		$prefijo='P.'.substr(strtoupper($datos['nombrePuesto']),0,3).' ';
	} else if($datos['tipoEvaluacion']=='OTRAS'){
		$tipo=$datos['otros'];
	}
	$objPHPExcel->getActiveSheet()->getCell('A2')->setValue('EMPRESA: '.$datos['EMPNOMBRE']);
	$objPHPExcel->getActiveSheet()->getCell('E2')->setValue('FECHA DE ELABORACION: '.formateaFechaWeb($datos['fechaEvaluacion']));
	$objPHPExcel->getActiveSheet()->getCell('A3')->setValue($tipo);
	$i=5;
	$n=1;
	while($riesgo=mysql_fetch_assoc($riesgos)){
		$primero=$i;
		$nT=$n<10?'0'.$n:$n;
		if($riesgo['prioridad']==''){
			$prioridad[0]='';
			$prioridad[1]='';
		} else {
			$prioridad=explode(' - ', $riesgo['prioridad']);
			$prioridad[0]=str_replace('/', ' - ', $prioridad[0]);
			$prioridad[1]=str_replace('RIESGO ', '', $prioridad[1]);
			$prioridad[1]=trim($prioridad[1]);
		}
		$iniciales=array(''=>'','TRIVIAL'=>'(T)','TOLERABLE'=>'(TO)','MODERADO'=>'(M)','IMPORTANTE'=>'(I)','INTOLERABLE'=>'(IN)');
		$estilo=array(
					'font' => array(
						'size'=>9
					)
				);
		$colores=array(''=>'','TRIVIAL'=>'f9f9f9','TOLERABLE'=>'fff584','MODERADO'=>'ffbf42','IMPORTANTE'=>'c00011','INTOLERABLE'=>'000000');
		$coloresF=array(''=>'','TRIVIAL'=>'000000','TOLERABLE'=>'000000','MODERADO'=>'000000','IMPORTANTE'=>'ffffff','INTOLERABLE'=>'ffffff');
		$estilo2=array(
					'font' => array(
						'color' => array('rgb' => $coloresF[$prioridad[1]]),
					),
        			'fill' => array(
            			'type' => PHPExcel_Style_Fill::FILL_SOLID,
            			'color' => array('rgb' => $colores[$prioridad[1]])
        			)
    			);
		$objPHPExcel->getActiveSheet()->getCell('A'.$i)->setValue($prefijo.$nT);
		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($riesgo['nombreRiesgo']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($prioridad[1].' '.$iniciales[$prioridad[1]]);
		if($estilo2!=''){
			$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($estilo2);
		}
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($prioridad[0]);
		if($estilo!=''){
			$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->applyFromArray($estilo);
		}
		$medidas=consultaBD('SELECT medidas_riesgos_evaluacion_general.* FROM medidas_riesgos_evaluacion_general INNER JOIN gestion_evaluacion_general ON medidas_riesgos_evaluacion_general.codigoGestion=gestion_evaluacion_general.codigo WHERE codigoEvaluacion='.$riesgo['codigo'],true);
		$hayMedida=false;
		while($medida=mysql_fetch_assoc($medidas)){
			$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue('');
			$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue($medida['recomendacion']);
			$hayMedida=true;
			if($estilo!=''){
				$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->applyFromArray($estilo);
			}
			$i++;
		}
		$n++;
		if(!$hayMedida){
			$i++;
		}
		$ultimo=$i-1;
		if($ultimo>$primero){
			$objPHPExcel->getActiveSheet()->mergeCells('A'.$primero.':A'.$ultimo);
			$objPHPExcel->getActiveSheet()->mergeCells('B'.$primero.':B'.$ultimo);
			$objPHPExcel->getActiveSheet()->mergeCells('C'.$primero.':C'.$ultimo);
			$objPHPExcel->getActiveSheet()->mergeCells('D'.$primero.':D'.$ultimo);
		}
	}
	/*foreach(range('B','I') as $columnID) {
    	$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
	}*/
	foreach($objPHPExcel->getActiveSheet()->getRowDimensions() as $rd) { 
    	$rd->setRowHeight(-1); 
	}
}


function generaPlanPrevencion($codigo){
	global $_CONFIG;

	$datos=consultaBD("SELECT formularioPRL.codigo, formularioPRL.fecha, clientes.codigo AS codigoCliente, formularioPRL.formulario, clientes.EMPPC, clientes.EMPANEXO, clientes.EMPRL, clientes.EMPRLDNI, ofertas.opcion, ofertas.especialidadTecnica, ofertas.otraOpcion, clientes.EMPNOMBRE, ficheroLogo, ficheroOrganigrama, codigoUsuarioTecnico
					   FROM formularioPRL 
					   INNER JOIN contratos ON formularioPRL.codigoContrato=contratos.codigo
					   INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
					   INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
					   LEFT JOIN actividades ON clientes.EMPACTIVIDAD=actividades.codigo
					   WHERE formularioPRL.codigo='$codigo';",true,true);
	$datos['especialidadTecnica']=explode('&{}&', $datos['especialidadTecnica']);
	$arrayCNAE=arrayCNAE();
	$direccionesCentros='';
	$centros=consultaBD('SELECT * FROM clientes_centros WHERE codigoCliente='.$datos['codigoCliente'],true);
	while($centro=mysql_fetch_assoc($centros)){
		if($direccionesCentros==''){
			$direccionesCentros=$centro['direccion'];
		} else {
			$direccionesCentros.=', '.$centro['direccion'];
		}
	}
	$centros=consultaBD('SELECT * FROM clientes_centros WHERE codigoCliente='.$datos['codigoCliente'],true);
	$formulario=explode('&{}&',$datos['formulario']);
    if(count($formulario)>1){
        foreach($formulario as $pregunta){
            $partes=explode('=>',$pregunta);
            $datos[$partes[0]]=$partes[1];
        }
    }
    $actividad=datosRegistro('actividades',$datos['pregunta4']);
    $puestos=consultaBD('SELECT nombre, seccionPuestoTrabajo AS zonaTrabajo, tareas  FROM puestos_trabajo_formulario_prl INNER JOIN puestos_trabajo ON puestos_trabajo_formulario_prl.codigoPuestoTrabajo=puestos_trabajo.codigo WHERE codigoFormularioPRL='.$datos['codigo'].' GROUP BY puestos_trabajo.codigo ORDER BY nombre',true);
    $logoCliente=$datos['EMPNOMBRE'];
	if($datos['ficheroLogo']!='' && $datos['ficheroLogo']!='NO'){
		$logoCliente="<img src='../clientes/imagenes/".$datos['ficheroLogo']."' />";
	} else {
		$logoCliente=$datos['EMPNOMBRE'];
	}

	if($datos['ficheroOrganigrama']!='' && $datos['ficheroOrganigrama']!='NO'){
		$organigrama="<img class='organigrama' src='ficheros/".$datos['ficheroOrganigrama']."' />";
	} else {
		$organigrama="<img class='organigrama' src='../img/organigramaPRL.png' alt='Organigrama' />";
	}

	$organo='No dispone de órgano de representación';
	if($datos['pregunta17']=='SI'){
		$organo='Dispone de órgano de representación';
	}
	$tecnico=datosRegistro('usuarios',$datos['codigoUsuarioTecnico']);
	$firmasCodigo=array(145,146,150);
	$firmas=array(145=>'firmaAdan.png',146=>'firmaAngeles.png',150=>'firmaMarta.png');
	if(in_array($tecnico['codigo'], $firmasCodigo)){
		$firma='<img style="width:150px" src="../img/'.$firmas[$tecnico['codigo']].'" />';
	}  else {
		$firma='';
	}
	$datos['fecha']=date('Y-m-d');
    $contenido = "
	<style type='text/css'>
	<!--
			html{
				margin:0px;
				padding:0px;
				width:100%;
			}
	        body{
	            font-size:12px;
	            font-family: Calibri;
	            font-weight: lighter;
	            line-height: 24px;
	            margin:0px;
				padding:0px;
				width:100%;

	        }

	        .portada{
	        	border:2px solid #02215c;
	        	height:95%;
	        	margin:20px;
	        }

	        .titlePortada{
	        	text-align:center;
	        	font-size:56px;
	        	color:#02215c;
	        	font-weight:bold;
	        	margin-top:240px;
	        	line-height:64px;
	        }

	        .imagenFondo{
	        	position:absolute;
	        	top:10%;
	        	z-index:1;
	        	left:-3%
	        }

	        .imagenFondo img{
	        	width:103%;
	        	position:absolute;
	        	display:inline-block;
	        }

	        .imagenFondo2{
	        	position:absolute;
	        	z-index:1;
	        	top:-133px;
	        	left:-30px;
	        }

	        .imagenFondo2 img{
	        	width:104%;
	        	height:100%;
	        	display:inline-block;
	        }

	       .imagenFondo3{
	        	position:absolute;
	        	z-index:1;
	        	top:-133px;
	        	left:-205px;
	        }

	        .imagenFondo3 img{
	        	width:104%;
	        	height:100%;
	        	width:800px;
	        	display:inline-block;
	        }

	        .nombreEmpresa{
	        	font-size:36px;
	        	color:#02215c;
	        	text-align:center;
	        	margin-top:250px;
	        	width:100%;
	        	line-height:48px;
	        }

	        .cabecera{
	        	border-top:1px solid #02215c;
	        	border-bottom:1px solid #02215c;
	        	padding:0px;
	        	margin-bottom:200px;
	        	width:100%;
	        }

	        .cabecera .a20{
	        	width:20%;
	        	text-align:center;
	        }

	        .cabecera .a60{
	        	width:60%;
	        	color:#02215c;
	        	text-align:center;
	        	font-weight:bold;
	        	font-size:18px;
	        }

	        .cabecera img{
	        	width:58%;
	        }

	        .cabecera .tituloCabecera{
	        	color:#02215c;
	        	text-align:center;
	        	font-weight:bold;
	        	margin-left:140px;
	        	font-size:18px;
	        	margin-top:-25px;
	        }

	        .cabecera .logoCliente{
	        	position:absolute;
	        	right:0px;
	        	top:0px;
	        	background:red;
	        	width:100px;
	        }

	        .cajaRevision{
	        	color:#02215c;
	        	margin-left:80px;
	        }

	        .cajaNumeroPagina{
	        	color:#02215c;
	        	position:absolute;
	        	right:80px;
	        	bottom:0px;	
	        }

	        .tituloIndice{
	        	text-align:center;
	        	font-size:20px;
	        	margin-top:20px;
	        	margin-bottom:20px;
	        	color:#02215c;
	        }

	        .indice{
	        	margin-left:80px;
	        }


	        .indice td{
	        	font-size:14px;
	        	color:#02215c;
	        	padding-top:35px;
	        }

	        .indice .texto{
	        	font-weight:bold;
	        }

	        .indice .sub{
	        	font-weight:normal;
	        	padding-left:10px;
	        }

	        .indice .pagina{
	        	font-weight:bold;
	        }

	        .container{
	        	width:75%;
	        	margin-left:90px;
	        }

	        .container2{
	        	width:95%;
	        	margin-left:20px;
	        	border:1px solid #02215c;
	        	padding:5px;
	        	margin-top:20px;
	        }

	        .tituloSeccion{
	        	font-size:14px;
	        	color:#02215c;
	        	font-weight:bold;
	        }

	        p, li {
	        	text-align:justify;
	        	font-size:11px;
	        	line-height:22px;
	        }

	        li{
	        	padding-bottom:18px;
	        }

	        .tituloIndice{
	        	text-align:center;
	        	font-size:20px;
	        	margin-top:20px;
	        	margin-bottom:20px;
	        	color:#02215c;
	        }

	        .tablaDatos{
	        	width:100%;
	        	border:1px solid #000;
	        	border-collapse:collapse;
	        }

	        .tablaDatos th,
	        .tablaDatos td{
	        	border:1px solid #000;
	        	font-size:11px;
	        	padding-top:3px;
	        	padding-bottom:3px;
	        	padding-left:8px;
	        }

	        .tablaDatos th{
	        	background:#02215c;
	        	font-weight:bold;
	        	color:#FFF;
	        }

	        .tablaDatos .a10{
	        	width:10%;
	        }

	        .tablaDatos .a15{
	        	width:15%;
	        }

	        .tablaDatos .a20{
	        	width:20%;
	        }

	        .tablaDatos .a25{
	        	width:25%;
	        }

	        .tablaDatos .a30{
	        	width:30%;
	        }

	        .tablaDatos .a33,
	        .noBorde .a33{
	        	width:33%;
	        }

	        .tablaDatos .a35{
	        	width:35%;
	        }

	        .tablaDatos .a65{
	        	width:65%;
	        }

	        .tablaDatos .a70{
	        	width:70%;
	        }

	        .tablaDatos .a85{
	        	width:85%;
	        }

	        .tablaDatos .a100{
	        	width:100%;
	        }

	        .tablaDatos .centro{
	        	text-align:center;
	        }

	        .desconocido{
	        	color:red;
	        	font-weight:bold;
	        }

	        .noBorde{
	        	width:100%;
	        	border:none;
	        }

	        .noBorde th,
	        .noBorde td{
	        	font-size:11px;
	        	padding-top:3px;
	        	padding-bottom:3px;
	        	padding-left:8px;
	        	font-weight:bold;
	        	height:15px;
	        	border:none;
	        	border-collapse:collapse;
	        }

	        .noBorde th{
	        	background:#02215c;
	        	font-weight:bold;
	        	color:#FFF;
	        }

	        .noBorde .tdGris{
	        	background:#BBB;
	        }

	        p.tituloIndice{
	        	text-align:center;
	        	font-size:20px;
	        	margin-top:40px;
	        	margin-bottom:20px;
	        	color:#02215c;
	        	font-weight:bold;
	        }

	        .organigrama{
	        	width:80%;
	        }
	-->
	</style>
	

	<page backbottom='0mm' backleft='0mm' backright='0mm' >
		<div class='imagenFondo'><img src='../img/fondoOferta.jpeg' alt='Anesco' /></div>
		<div class='portada'>
			<div class='titlePortada'>PLAN DE<br/>PREVENCIÓN</div>

			<div class='nombreEmpresa'>".$datos['pregunta0']."</div>
			<div class='container'>
			<table class='noBorde'>
				<tr>	
					<th class='a33'>ELABORADO POR:</th>
					<th class='a33'>REVISIÓN:</th>
					<th class='a33'>APROBADO:</th>
				</tr>
				<tr>
					<td class='a33'>".$tecnico['nombre']." ".$tecnico['apellidos']."</td>
					<td class='a33'></td>
					<td class='a33'></td>
				</tr>
				<tr>
					<td class='a33'>".$firma."</td>
					<td class='a33'></td>
					<td class='a33'></td>
				</tr>
				<tr>
					<td class='a33 tdGris'>TÉCNICO SUPERIOR EN PREVENCIÓN DE RIESGOS LABORALES</td>
					<td class='a33 tdGris'></td>
					<td class='a33 tdGris'>REPRESENTANTE LEGAL:</td>
				</tr>
				<tr>
					<td class='a33 tdGris'></td>
					<td class='a33 tdGris'></td>
					<td class='a33 tdGris'></td>
				</tr>
				<tr>
					<td class='a33 tdGris'>FECHA: ".formateaFechaWeb($datos['fecha'])."</td>
					<td class='a33 tdGris'>FECHA</td>
					<td class='a33 tdGris'>FECHA</td>
				</tr>
				<tr>
					<td class='a33 tdGris'></td>
					<td class='a33 tdGris'></td>
					<td class='a33 tdGris'></td>
				</tr>
				<tr>
					<td class='a33'>ANESCO SALUD Y PREVENCIÓN, S.L.</td>
					<td class='a33'></td>
					<td class='a33'></td>
				</tr>
			</table>
			</div>
		</div>

	</page>";

$contenido.="
	<page backtop='30mm' backleft='50mm' backright='50mm' backbottom='0mm'>
	<div class='imagenFondo3'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
		<bookmark title='1. INTRODUCCIÓN' level='0' ></bookmark>
		<h1 class='tituloSeccion'>1. INTRODUCCIÓN</h1>
		<p>La <b>Ley 31/1995</b>, de 8 de noviembre, de Prevención de Riesgos Laborales y el <b>Real Decreto 39/1997</b>, de 17 de enero, Reglamento de los Servicios de Prevención constituyen los pilares fundamentales dentro del marco normativo que regula actualmente la seguridad y la salud en el trabajo. La aplicación de la Ley de Prevención de Riesgos laborales persigue no sólo la ordenación de las obligaciones y responsabilidades de los actores inmediatamente relacionados con el hecho laboral, sino fomentar una nueva <b>cultura de la prevención.</b><br/><br/>

La entrada en vigor de la <b>Ley 54/2003</b>, de 12 de diciembre, de reforma del marco normativo de la prevención de riesgos laborales así como el <b>RD 604/2006</b>, de 19 de mayo, por el que se modifican el RD 39/1997 y el RD 1627/1997, de 24 de octubre, por el que se establecen las disposiciones mínimas de seguridad y salud en las obras de construcción, da un nuevo enfoque con objeto de combatir de manera efectiva la siniestralidad laboral y fomentar una auténtica cultura de la prevención de los riesgos en el trabajo, que asegure el cumplimiento efectivo y real de las obligaciones. <br/><br/>

A través de la implantación y aplicación de un <b>Plan de Prevención de Riesgos Laborales</b>, los empresarios desarrollaran la <b>integración de la actividad preventiva</b> en el sistema de gestión de su empresa, comprendiendo tanto los niveles jerárquicos como el conjunto de todas las actividades
De este modo, el objeto de este informe es servir de herramienta a través de la cual la empresa pueda integrar la Prevención en el Sistema General de Gestión en todos los niveles jerárquicos de la empresa, con el fin de asegurar una protección eficaz del trabajador frente a los riesgos laborales presentes en el desarrollo de su trabajo.<br/><br/>

En este sentido el Plan de prevención de riesgos laborales debe ser aprobado por la dirección de la empresa, asumido por toda su estructura organizativa, en particular por todos sus niveles jerárquicos, y conocido por todos sus trabajadores.<br/><br/>

Los trabajadores y sus representantes deberán contribuir a la integración de la prevención de riesgos laborales y colaborar en la adopción y el cumplimiento de las medidas preventivas a través de los mecanismos de que Ley de Prevención de Riesgos Laborales recoge al efecto.<br/><br/>

Dicho Plan habrá de reflejarse en un documento que se conservará a disposición de la autoridad laboral, de las autoridades sanitarias y de los representantes de los trabajadores.
</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
		<bookmark title='1.1 IDENTIFICACIÓN DE LA EMPRESA' level='1' ></bookmark>
		<h1 class='tituloSeccion'>1.1 IDENTIFICACIÓN DE LA EMPRESA:</h1>
		
		<table class='tablaDatos'>
			<tr>
				<th class='a35'>NOMBRE EMPRESA</th>
				<td class='a65'>".$datos['pregunta0']."</td>
			</tr>
			<tr>
				<th class='a35'>CIF</th>
				<td class='a65'>".$datos['pregunta1']."</td>
			</tr>
			<tr>
				<th class='a35'>ACTIVIDAD</th>
				<td class='a65'>".$actividad['nombre']."</td>
			</tr>
			<tr>
				<th class='a35'>CNAE</th>
				<td class='a65'>".$datos['pregunta5']."</td>
			</tr>
			<tr>
				<th class='a35'>Incluida Anexo I</th>
				<td class='a65'>".ucwords(strtolower($datos['EMPANEXO']))."</td>
			</tr>
			<tr>
				<th class='a35'>DOMICILIO FISCAL</th>
				<td class='a65'>".$datos['pregunta2']."</td>
			</tr>
			<tr>
				<th class='a35'>DOMICILIO DE LOS CENTROS</th>
				<td class='a65'>".$direccionesCentros."</td>
			</tr>
			<tr>
				<th class='a35'>RESPONSABLE</th>
				<td class='a65'>".$datos['EMPPC']."</td>
			</tr>
			<tr>
				<th class='a35'>Nº TRABAJADORES</th>
				<td class='a65'>".$datos['pregunta12']."</td>
			</tr>
		</table>
		<br/><br/>
		<table class='tablaDatos' style='text-align:justify;'>
			<tr>
				<th colspan='3' class='a100 centro'>PUESTO DE TRABAJO</th>
			</tr>
			<tr>
				<th class='a33 centro'>PUESTO</th>
				<th class='a33 centro'>SECCIÓN/ÁREA</th>
				<th class='a33 centro'>FUNCIONES</th>
			</tr>
		";
		while($puesto=mysql_fetch_assoc($puestos)){
			$contenido.="<tr>
							<td class='a33'>".$puesto['nombre']."</td>
							<td class='a33'>".$puesto['zonaTrabajo']."</td>
							<td class='a33' style='padding-right:5px;'>".nl2br($puesto['tareas'])."</td>
						</tr>";
		}
		$contenido.="
		</table>
		<br/><br/>
		<bookmark title='1.2 DESCRIPCIÓN DE LOS CENTROS DE TRABAJOS' level='1' ></bookmark>
		<h1 class='tituloSeccion'>1.2 DESCRIPCIÓN DE LOS CENTROS DE TRABAJOS</h1><p>
		";
		while($centro=mysql_fetch_assoc($centros)){
		$contenido.="<b>".$centro['direccion']."</b><br/>
		".$datos['pregunta'.$centro['codigo'].'56']."<br/>";
		}
$contenido.="
</p>


		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
		<bookmark title='2. OBJETO' level='0' ></bookmark>
		<h1 class='tituloSeccion'>2. OBJETO</h1>
		<p>Es objeto del presente documento es diseñar el Plan de Prevención de Riesgos Laborales relativo a la empresa <b>".$datos['pregunta0']."</b> en el que se definen la estructura, las responsabilidades, las funciones, las prácticas, los procedimientos, los procesos y los recursos necesarios para conseguir la integración de la prevención de riesgos laborales en el sistema de gestión de la empresa.<br/><br/>

		El presente informe se realiza a instancias de la empresa, de conformidad con el contrato de concierto con ANESCO SALUD Y PREVENCIÓN, S.L., y para el debido cumplimiento de las exigencias de la legislación vigente, en especial, el artículo 16 de la Ley 31/95 de Prevención de Riesgos Laborales.</p>
		<bookmark title='3. ALCANCE' level='0' ></bookmark>
		<h1 class='tituloSeccion'>3. ALCANCE</h1>
		<p>El alcance del presente documento es de aplicación a todas las actividades desarrolladas por <b>".$datos['pregunta0']."</b>.</p>

		<bookmark title='4. POLÍTICA' level='0' ></bookmark>
		<h1 class='tituloSeccion'>4. POLÍTICA</h1>
		<p>La Política en materia de prevención tiene por objeto la promoción de la mejora de las condiciones de trabajo dirigida a elevar el nivel de protección de la seguridad y salud de los trabajadores.</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container2'>
		<p>La Dirección de  <b>".$datos['pregunta0']."</b>, SL tomando como base los mismos fundamentos que rigen la normativa en Prevención de Riesgos Laborales, y con el objeto, tanto de desarrollar una labor eficaz de promoción de la Seguridad y Salud de los trabajadores, como de diseñar la estrategia de la efectiva implantación de la Prevención en todos los niveles jerárquicos ha desarrollado la Política de Prevención de Riesgos Laborales que será aplicable a todos sus trabajadores e instalaciones, comprometiéndose al cumplimiento de los siguientes principios:<br/><br/>

			1º La vida, integridad física y salud de los trabajadores son derechos cuya protección ha de ser una constante del quehacer cotidiano para todos los que trabajamos en <b>".$datos['pregunta0']."</b> y especialmente del de aquellos que, en uno u otro nivel y en uno u otro puesto de trabajo, ejercen funciones de mando.<br/><br/>

			2º Debido a que consideramos que las personas constituyen el activo más importante de nuestra empresa, esta Dirección quiere establecer una política preventiva que vaya hacia un modelo de prevención científica, integral, integrada y participativa.<br/><br/>

			3º Basándonos en el principio de que todos los accidentes, incidentes y enfermedades laborales pueden y deben ser evitados, la empresa se compromete a alcanzar un alto nivel de seguridad y salud en el trabajo, no limitándose solamente a cumplir con la legislación vigente en la materia, sino llevando a cabo acciones que eleven el grado de protección de los trabajadores marcado por la ley si ello fuera necesario.<br/><br/>

			4º La línea de mando asumirá y potenciará la integración de la seguridad en el proceso de producción, estableciendo como principio básico que la mejor productividad se consigue con la mayor seguridad, pues no se debe olvidar que la conservación de los recursos materiales y humanos constituye un elemento fundamental para disminuir los costes.<br/><br/>

			5º En aras a promover una conducta segura en las actividades desarrolladas, se aportará a los trabajadores toda la información existente sobre los riesgos inherentes a su trabajo, así como la formación necesaria sobre los medios y medidas a adoptar para su correcta prevención.<br/><br/>

			6º De igual manera, se promoverá la participación de todos los trabajadores en las cuestiones relacionadas con la prevención de riesgos en el trabajo, por ser ellos los que conocen con mayor profundidad los pormenores de las tareas que realizan, y por lo tanto son los más indicados para aportar ideas sobre la manera más segura de llevarlas a cabo.  <br/><br/>

			7º Para lograr una eficaz implantación de la política de prevención de riesgos laborales en <b>".$datos['pregunta0']."</b>, SL, se asignarán los recursos necesarios y se planificará de manera adecuada la utilización de los mismos.<br/><br/>
			Finalmente, es compromiso firme de esta empresa el integrar la prevención en la estructura organizativa de la empresa, a fin de lograr que la prevención no sea ajena a la organización productiva, pretendiendo así más que el mero cumplimiento de determinados requisitos de carácter básicamente documental.<br/><br/>
			En _________________________________, a _____de ___________________de 20__
		</p>
		</div>
		<div class='container'>
		<p>NOTA: SE DISPONDRÁN COPIAS DE ESTE DOCUMENTO EN LOS TABLONES DE INFORMACIÓN DE TRABAJADORES Y CLIENTES</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
		<bookmark title='5. OBJETIVOS Y METAS' level='0' ></bookmark>
		<h1 class='tituloSeccion'>5. OBJETIVOS Y METAS</h1>
		<p>Los Objetivos se establecen sobre la base de su pertinencia con las propias metas de la empresa, respetándose las expectativas y necesidades actuales de sus clientes, así como los aspectos medioambientales y de seguridad y salud en el trabajo. La comprensión de estos objetivos por los diferentes estamentos y niveles de la empresa es asegurada mediante una información adecuada y una comunicación constante entre ellos.<br/><br/>

			Los objetivos preventivos que pretende alcanzar <b>".$datos['pregunta0']."</b> a tenor de los principios recogidos en la Política Preventiva son los siguientes:</p>
		<ul>
			<li>Cumplir con los principios esenciales indicados en la política preventiva de la entidad.</li>
			<li>Asegurar el cumplimiento de la normativa de aplicación. </li>
			<li>Facilitar los medios humanos y materiales necesarios para el desarrollo de las acciones establecidas para el alcance de los objetivos.</li>
			<li>Definir las funciones y responsabilidades de cada nivel jerárquico a fin de que se cumplan los objetivos.</li>
			<li>Asumir un compromiso participativo en las diferentes actuaciones preventivas.</li>
			<li>Suprimir todo riesgo eliminable.</li>
			<li>Minimizar las consecuencias y probabilidad de ocurrencia de aquellos riesgos que no pueden ser eliminados.</li>
			<li>Salud y bienestar: Mantener unos altos niveles de salud y bienestar en el trabajo.</li>
		</ul>

		<bookmark title='6. RECURSOS' level='0' ></bookmark>
		<h1 class='tituloSeccion'>6. RECURSOS</h1>
		<p>Para alcanzar dichos objetivos <b>".$datos['pregunta0']."</b> pone a disposición de las personas encargadas de gestionar la prevención de riesgos laborales en la empresa los recursos humanos, técnicos, materiales y económicos que se indican en este apartado.<br/><br/>
		Además, la asignación de los recursos humanos, técnicos, materiales y económicos será objeto de cuantificación y designación en el <b>documento de planificación de la actividad preventiva</b>, que debe planificarse para un periodo determinado, estableciéndose las fases y prioridades de su desarrollo en función de la magnitud de los riesgos. En ella se identificará la actividad adoptada, el plazo para llevarla a cabo, la designación de responsables y los recursos humanos y materiales para su ejecución.<br/><br/> 

La Planificación de la actividad preventiva estará contenida en un <b>DOCUMENTO ANEXO</b> a este Plan.
</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
		<bookmark title='6.1 RECURSOS HUMANOS DE LOS QUE SE DISPONE' level='1' ></bookmark>
		<h1 class='tituloSeccion'>6.1 RECURSOS HUMANOS DE LOS QUE SE DISPONE</h1>
		<table class='tablaDatos'>
			<tr>
				<th class='a25'>RECURSO</th>
				<th class='a25'>IDENTIFICACIÓN</th>
				<th class='a25'>TIPO</th>
				<th class='a25'>PUESTO</th>
			</tr>
			<tr>
				<td class='a25'>Enlace con el Servicio de Prevención</td>
				<td class='a25'></td>
				<td class='a25'>Recurso propio</td>
				<td class='a25'>Interlocutor en Prevención de Riesgos Laborales con SPA</td>
			</tr>
		</table>
		<bookmark title='6.2 RECURSOS MATERIALES Y TÉCNICOS DE LOS QUE SE DISPONE' level='1' ></bookmark>
		<h1 class='tituloSeccion'>6.2 RECURSOS MATERIALES Y TÉCNICOS DE LOS QUE SE DISPONE</h1>
		<table class='tablaDatos'>
			<tr>
				<th class='a65'>RECURSO</th>
				<th class='a35'>TIPO</th>
			</tr>
			<tr>
				<td class='a65'>Botiquín/es MUTUA</td>
				<td class='a35'>Material</td>
			</tr>
			<tr>
				<td class='a65'>Equipos de Protección Individual</td>
				<td class='a35'>Material</td>
			</tr>
			<tr>
				<td class='a65'>Otros</td>
				<td class='a35'></td>
			</tr>
		</table>
		<bookmark title='6.3 RECURSOS ECONÓMICOS DE LOS QUE SE DISPONE' level='1' ></bookmark>
		<h1 class='tituloSeccion'>6.3 RECURSOS ECONÓMICOS DE LOS QUE SE DISPONE</h1>
		<table class='tablaDatos'>
			<tr>
				<th class='a15'>RECURSO:</th>
				<th class='a85'>COMPRA Y ADQUISICIÓN DE EQUIPOS DE PROTECCIÓN INDIVIDUAL</th>
			</tr>
			<tr>
				<th class='a15'>TIPO:</th>
				<th class='a85'>OBLIGATORIO</th>
			</tr>
			<tr>
				<td colspan='2' class='a100'>Descripción del recurso económico:<br/><br/>
Compra de aquellos EPI necesarios y establecidos por la Evaluación de Riesgos para la realización de las tareas y para ser entregados a los trabajadores:</td>
			</tr>
			<tr>
				<td colspan='2' class='a100 centro'>Importe asignado:</td>
			</tr>
		</table>

		<table class='tablaDatos'>
			<tr>
				<th class='a15'>RECURSO:</th>
				<th class='a85'>LUCHA CONTRA INCENDIOS</th>
			</tr>
			<tr>
				<th class='a15'>TIPO:</th>
				<th class='a85'>OBLIGATORIO</th>
			</tr>
			<tr>
				<td colspan='2' class='a100'>Descripción del recurso económico:<br/><br/>
Contrato de mantenimiento de equipos contra incendios y/o revisiones periódicas de dichos equipos.
Extintor/es polvo ABC en el centro de trabajo.
</td>
			</tr>
			<tr>
				<td colspan='2' class='a100 centro'>Importe asignado:</td>
			</tr>
		</table>

		<table class='tablaDatos'>
			<tr>
				<th class='a15'>RECURSO:</th>
				<th class='a85'>PARTIDA PARA PREVENCIÓN OBLIGATORIA</th>
			</tr>
			<tr>
				<th class='a15'>TIPO:</th>
				<th class='a85'>OBLIGATORIO</th>
			</tr>
			<tr>
				<td colspan='2' class='a100'>Descripción del recurso económico:<br/><br/>
Se incluyen las actividades derivadas del Concierto con el Servicio de Prevención Ajeno</td>
			</tr>
			<tr>
				<td colspan='2' class='a100 centro'>Importe asignado:</td>
			</tr>
		</table>

		<table class='tablaDatos'>
			<tr>
				<th class='a15'>RECURSO:</th>
				<th class='a85'>SEÑALIZACIÓN</th>
			</tr>
			<tr>
				<th class='a15'>TIPO:</th>
				<th class='a85'>OBLIGATORIO</th>
			</tr>
			<tr>
				<td colspan='2' class='a100'>Descripción del recurso económico:<br/><br/>
Compra de señalizaciones varias en materia de prevención</td>
			</tr>
			<tr>
				<td colspan='2' class='a100 centro'>Importe asignado:</td>
			</tr>
		</table>

		<table class='tablaDatos'>
			<tr>
				<th class='a15'>RECURSO:</th>
				<th class='a85'>VIGILANCIA DE LA SALUD</th>
			</tr>
			<tr>
				<th class='a15'>TIPO:</th>
				<th class='a85'>OBLIGATORIO</th>
			</tr>
			<tr>
				<td colspan='2' class='a100'>Descripción del recurso económico:<br/><br/>
Vigilancia de la Salud de los trabajadores: Reconocimientos médicos iniciales y periódicos, Medicina del Trabajo, etc.</td>
			</tr>
			<tr>
				<td colspan='2' class='a100 centro'>Importe asignado:</td>
			</tr>
		</table>
		<p><b>IMPORTE ASIGNADO TOTAL:</b></p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
		<bookmark title='7. ORGANIZACIÓN PRODUCTIVA' level='0' ></bookmark>
		<h1 class='tituloSeccion'>7. ORGANIZACIÓN PRODUCTIVA</h1>
		<bookmark title='7.1 ORGANIGRAMA' level='1' ></bookmark>
		<h1 class='tituloSeccion'>7.1 ORGANIGRAMA</h1>
		".$organigrama."
		
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
		<bookmark title='7.2 LISTADO DE ÁREAS DE ACTIVIDAD Y PROCESOS PRODUCTIVOS' level='1' ></bookmark>
		<h1 class='tituloSeccion'>7.2 LISTADO DE ÁREAS DE ACTIVIDAD Y PROCESOS PRODUCTIVOS</h1>
		<p><b>".$datos['pregunta0']."</b> desempeña su actividad en <b>".$actividad['nombre']."</b>.<br/>Los procesos más habituales a desarrollar serán:</p>
		<ul style='list-style-type:square;'>";
		$procesos=consultaBD('SELECT * FROM actividades_procesos WHERE codigoActividad='.$actividad['codigo'],true);
		while($proceso=mysql_fetch_assoc($procesos)){
			$contenido.="<li>".$proceso['nombre'].".</li>";
		}
		$contenido.="
		</ul>
		<bookmark title='8. ORGANIZACIÓN PREVENTIVA' level='0' ></bookmark>
		<h1 class='tituloSeccion'>8. ORGANIZACIÓN PREVENTIVA</h1>
		<p>En la Organización Preventiva de la empresa se incluye la estructura de la empresa con los recursos humanos que dispone para la gestión de la actividad preventiva, incluyendo las funciones de estos recursos.</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
		<bookmark title='8.1 ESTRUCTURA PREVENTIVA' level='1' ></bookmark>
		<h1 class='tituloSeccion'>8.1 ESTRUCTURA PREVENTIVA</h1>
		<img src='../img/estructura_preventiva.png' alt='Estructura preventiva' />
		<bookmark title='8.2 MODALIDAD PREVENTIVAS' level='1' ></bookmark>
		<h1 class='tituloSeccion'>8.2. MODALIDAD PREVENTIVA</h1>
		<p>La Empresa ha optado por la modalidad preventiva de concertación de la actividad preventiva con el Servicio de Prevención Ajeno <b>ANESCO SALUD Y PREVENCIÓN</b> en las especialidades de: </p>
		<ul style='list-style-type:square;'>";
		if($datos['opcion']==1 || ($datos['opcion']==3 && in_array('Seguridad en el Trabajo', $datos['especialidadTecnica']))){
			$contenido.="<li>Seguridad en el Trabajo.</li>";
		}
		if($datos['opcion']==1 || ($datos['opcion']==3 && in_array('Higiene Industrial', $datos['especialidadTecnica']))){
			$contenido.="<li>Higiene Industrial.</li>";
		}
		if($datos['opcion']==1 || ($datos['opcion']==3 && in_array('Ergonomía y Psicosociología', $datos['especialidadTecnica']))){
			$contenido.="<li>Ergonomía y Psicosociología Aplicada.</li>";
		}
		if($datos['opcion']==1 || $datos['opcion']==2){
			$contenido.="<li>Vigilancia de la Salud.</li>";
		}
		if($datos['opcion']==4){
			$contenido.="<li>".$datos['otraOpcion'].".</li>";
		}
		$contenido.="
		</ul>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
		<p>Como complemento al modelo de organización de la prevención en la empresa y con el fin de favorecer la integración de la actividad preventiva en el seno de la misma, se dispone de las figuras que se indican a continuación:</p>
		<ul style='list-style-type:square;'>
			<li>Interlocutor con el Servicio de Prevención Ajeno.</li>
			<li>Servicio de Prevención Ajeno.</li>
		</ul>
		<p>Centrados a partir de la política de prevención adoptada existe un <b>modelo bidireccional de comunicación, coordinación y actuación</b> en todos los niveles de la organización</p>
		<img src='../img/modelo_bidireccional.png' alt='Modelo bidireccional' />
		<p>Los distintos cauces de comunicación entre los responsables y miembros de cada uno de los departamentos enumerados en el organigrama arriba indicado quedarán reflejados en los procedimientos de gestión e instrucciones operativas, de manera que se asegure una comunicación adecuada, en reuniones, envíos de escritos, correos electrónicos, comunicación telefónica, etc.<br/><br/>

		La comunicación entre <b>".$datos['pregunta0']."</b> y <b>ANESCO SALUD Y PREVENCIÓN</b> se hará por los cauces habituales de comunicación telefónica, correo electrónico, etc. cuyos números han sido facilitados por el Servicio de Prevención. <b>Comunicación SPA Ajeno.</b></p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container'>
		<bookmark title='8.3 ÓRGANOS DE REPRESENTACIÓN' level='1' ></bookmark>
		<h1 class='tituloSeccion'>8.3. ÓRGANOS DE REPRESENTACIÓN</h1>
		".$organo."
		<bookmark title='9. FUNCIONES Y RESPONSABILIDADES' level='0' ></bookmark>
		<h1 class='tituloSeccion'>9. FUNCIONES Y RESPONSABILIDADES</h1>
		<p>En función del organigrama descrito en la página anterior, describimos a continuación las funciones de cada puesto, así como la responsabilidad en materia preventiva, en lo referente a la ejecución y/o control del plan de prevención, así como de todas las acciones preventivas a realizar. <br/><br/>

			En cualquier caso, queda entendido que las responsabilidades descritas no sustituyen, sino que complementan, a las descritas en la normativa interna de seguridad y salud, ya que las mismas deben ser cumplidas por todos y cada uno de los operarios de la empresa, independientemente de su puesto.</p>
		<table class='tablaDatos'>
			<tr>
				<th class='a20 centro'>FUNCIÓN</th>
				<th class='a35 centro'>NOMBRE Y APELLIDOS</th>
				<th class='a15 centro'>DNI</th>
				<th class='a15 centro'>FECHA</th>
				<th class='a15 centro'>FIRMA</th>
			</tr>
			<tr>
				<td class='a20 centro'>GERENTE</td>
				<td class='a35 centro'>".$datos['EMPRL']."</td>
				<td class='a15 centro'>".$datos['EMPRLDNI']."</td>
				<td class='a15 centro'></td>
				<td class='a15 centro'></td>
			</tr>
			<tr>
				<td class='a20 centro'>INTERLOCUTOR<br/>CON EL SPA</td>
				<td class='a35 centro'>".$datos['pregunta18']."</td>
				<td class='a15 centro'>".$datos['pregunta19']."</td>
				<td class='a15 centro'></td>
				<td class='a15 centro'></td>
			</tr>
			";
		$empleados=consultaBD('SELECT * FROM funciones_formulario_prl WHERE codigoFormularioPRL='.$datos['codigo'],true);
		while($empleado=mysql_fetch_assoc($empleados)){
			if($empleados['funciones']!='' || $empleados['nombre']!='' || $empleados['dni']!=''){
				$contenido.="<tr>
					<td class='a20 centro'>".$empleado['funciones']."</td>
					<td class='a35 centro'>".$empleado['nombre']."</td>
					<td class='a15 centro'>".$empleado['dni']."</td>
					<td class='a15 centro'>".formateaFechaWeb($empleado['fecha'])."</td>
					<td class='a15 centro'></td>
				</tr>";
			}
		}
		$contenido.="
		</table>
		<bookmark title='9.1 FUNCIONES Y/O RESPONSABILIDADES DE LA DIRECCIÓN DE LA EMPRESA' level='1' ></bookmark>
		<h1 class='tituloSeccion'>9.1. FUNCIONES Y/O RESPONSABILIDADES DE LA DIRECCIÓN DE LA EMPRESA</h1>
		<p>
		Tareas relacionadas con la administración y dirección de la organización. <br/>
		<b>Responsabilidades:</b><br/>
		Tiene la responsabilidad de garantizar la seguridad y salud de los trabajadores a su cargo, para ello impondrá las medidas necesarias:<br/>
		a)	Determinar una política preventiva y transmitirla a la organización.<br/>
		b)	Determinar los objetivos y metas a alcanzar.<br/>
		c)	Definir las funciones y responsabilidades de cada nivel jerárquico a fin de que se cumplan dichos objetivos.<br/>
		d)	Asegurar el cumplimiento de los preceptos contemplados en la normativa de aplicación.<br/>
		e)	Fijar y documentar los objetivos y metas esperados a tenor de la política preventiva.<br/>
		f)	Establecer una modalidad organizativa de la prevención.<br/>
		g)	Liderar el desarrollo y mejora continua del sistema de gestión de la prevención de riesgos laborales establecido.<br/>
		h)	Facilitar los medios humanos y materiales necesarios para el desarrollo de las acciones establecidas para el alcance de los objetivos.<br/>
		i)	Asumir un compromiso participativo en diferentes actuaciones preventivas, para demostrar su liderazgo en el sistema de gestión preventiva.<br/>
		j)	Adoptar las acciones correctoras y preventivas necesarias para corregir las posibles desviaciones que se detecten en el Plan de Prevención.<br/>
		k)	Asegurar que la organización disponga de la formación necesaria para desarrollar las funciones y responsabilidades establecidas.<br/>
		l)	Designar a uno o varios trabajadores para la asunción del S.G.P.R.L., que coordinen el sistema, controlen su evolución y le mantengan informado.<br/>
		m)	Establecer las competencias de cada nivel organizativo para el desarrollo de las actividades preventivas definidas en los procedimientos.<br/>
		</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		<div class='container'>
		<p>
		n)	Asignar los recursos necesarios, tanto humanos como materiales, para conseguir los objetivos establecidos.<br/>
		o)	Integrar los aspectos relativos al S.G.P.R.L. en el sistema general de gestión de la entidad.<br/>
		p)	Participar de forma “proactiva” en el desarrollo de la actividad preventiva que se desarrolla, a nivel de los lugares de trabajo, para poder estimular comportamientos eficientes, detectar deficiencias y demostrar interés por su solución. <br/>
		q)	Realizar periódicamente análisis de la eficacia del sistema de gestión y en su caso establecer las medidas de carácter general que se requieran para adaptarlo a los principios marcados en la política preventiva.<br/>
		r)	Favorecer la consulta y participación de los trabajadores conforme a los principios indicados en la normativa de aplicación.<br/>
		s)	Todas aquellas otras funciones y responsabilidades definidas en el Sistema de Gestión Integrado de la empresa y en la Ley de PRL. <br/>
		</p>
		<bookmark title='9.2 FUNCIONES Y/O RESPONSABILIDADES INTERLOCUTOR CON EL SERVICIO DE PREVENCIÓN AJENO' level='1' ></bookmark>
		<h1 class='tituloSeccion'>9.2. FUNCIONES Y/O RESPONSABILIDADES INTERLOCUTOR CON EL SERVICIO DE PREVENCIÓN AJENO</h1>
		<p>
		Velar por fiel cumplimiento de la normativa de seguridad y salud de los trabajadores. Hacer oír las peticiones de los trabajadores y velar por las mejoras dentro de la política de seguridad y salud de la empresa. Responsabilidades:<br/>
		Como <b>INTERLOCUTOR CON EL SPA AJENO</b> deberá:<br/>
		a)	Coordinarse con los Servicios de Prevención Ajenos, asegurando su adecuada actuación. <br/>
		b)	Participar en la realización y revisión de la evaluación de riesgos cuando sea necesario. <br/>
		c)	Gestionar la compra de los equipos de protección individual <br/>
		d)	Revisar el cumplimiento de los equipos de trabajo de los requisitos establecidos en el Real Decreto 1215/97. <br/>
		e)	Mantener al día un listado de los equipos de trabajo y su estado con respecto a la legislación vigente. <br/>
		f)	Establecer los controles activos para el aseguramiento de las medidas de seguridad y salud, desarrollando la supervisión y control que se considere oportuna por parte del Servicio. <br/>
		g)	Establecer los equipos e instalaciones sometidos a requisitos de compra en base a requisitos de seguridad y salud. <br/>
		h)	Revisar el cumplimiento de las normas de seguridad de los equipos de trabajo antes de su puesta en marcha tras su instalación, reparación o haberse visto involucrado en un accidente. <br/>
		i)	Controlar la legislación aplicable en materia de prevención, definiendo los requisitos de aplicación. <br/>
		j)	Elaboración y mantenimiento del Plan de Autoprotección. <br/>
		k)	Convocar a los trabajadores a los reconocimientos médicos, comunicando a las unidades aquellas personas que debiendo someterse a un reconocimiento médico específico no lo han llevado a cabo en el plazo establecido. <br/>
		l)	Supervisión de las condiciones de trabajo, asegurando que cumplen con los requisitos definidos en la evaluación de riesgos. <br/>
		m)	Asegurarse, mediante revisión, que todas las instrucciones y métodos de trabajo incluyen las normas de seguridad necesarias. <br/>
		n)	Retener la aprobación de instrucciones y métodos de trabajo no incluyan las medidas de seguridad necesarias o que las incluidas no sean adecuadas. <br/>
		o)	Retener la aprobación de instrucciones y métodos de trabajo no incluya actividades no evaluadas hasta su evaluación y definición de las medidas de seguridad necesarias. <br/>
		</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		<div class='container'>
		
		<p>
		p)	Establecimiento de las no conformidades que se consideren oportunas para el aseguramiento de los requisitos establecidos. <br/>
		q)	Elaboración, gestión y mantenimiento del Sistema de Gestión de la Prevención. <br/>
		r)	Coordinar los aspectos de seguridad y salud con las contratas y subcontratas. <br/>
		s)	Asistir a las reuniones del Comité de Seguridad y Salud. <br/>
		t)	Participar en el diseño, aplicación y coordinación en el cumplimiento de los Planes y Programas de prevención. <br/>
		u)	Diseño e impartición de la formación en materia de prevención. <br/>
		v)	Gestión de la formación impartida por entidades externas. <br/>
		w)	Todas aquellas otras funciones y responsabilidades definidas en el Sistema de Gestión Integrado de la empresa y en la Ley de PRL. <br/><br/>

		Como <b>RESPONSABLE DE COMPRAS</b> deberá:<br/>
		a) Controlará el cumplimiento de los requisitos de seguridad en todas las compras de máquinas, equipos, herramientas, productos, etc.<br/>
		b) Identificará a los proveedores que cumplen con los requisitos básicos.<br/><br/>	

		Como <b>RESPONSABLE CONTRATACIÓN</b> deberá:<br/>
		a) Recabar del personal a contratar su formación e información previa el puesto a que se destina y su aptitud médica para el mismo.<br/>
		b) Controlará el cumplimiento de los requisitos de seguridad previo a la contratación de las empresas y trabajos.<br/><br/>

		Como <b>COORDINADOR DE ACTIVIDADES EMPRESARIALES</b> deberá:<br/>
		a)	Colaborar con las personas responsables de la ejecución del contrato en el envío, a las contratas, de los formularios correspondientes sobre cumplimiento de la normativa en materia de prevención. <br/>
		b)	Revisar la documentación aportada por las contratas y registrarla. <br/>
		c)	Colaborar, cuando se precise, con los técnicos del Servicio de Prevención, en la evaluación de los riesgos asociados a las áreas donde se van a realizar las tareas por las contratas y en el establecimiento de las medidas preventivas a adoptar. <br/>
		d)	Enviar a las contratas la información sobre los riesgos de las áreas donde van a prestarse o ejecutarse las tareas, así como las medidas preventivas a adoptar y las medidas en caso de emergencia. <br/>
		e)	Podrá realizar controles periódicos para comprobar el cumplimiento por parte de las contratas de la normativa de prevención de riesgos y de las disposiciones del presente procedimiento. <br/>
		f)	Informar a las personas responsables de la ejecución de los contratos sobre los riesgos generados por las empresas contratadas y que pueden afectar a los trabajadores a su cargo, así como sobre las medidas preventivas que deben adoptar para evitar dichos riesgos. <br/>
		g)	Proponer medidas a adoptar en caso de incumplimientos en materia de prevención cometidos por las contratas. <br/>
		h)	Todas aquellas otras funciones y responsabilidades definidas en el Sistema de Gestión Integrado de la empresa y en la Ley de PRL. <br/>

		</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		<div class='container'>
		<bookmark title='9.3 FUNCIONES Y/O RESPONSABILIDADES TODOS LOS TRABAJADORES' level='1' ></bookmark>
		<h1 class='tituloSeccion'>9.3. FUNCIONES Y/O RESPONSABILIDADES TODOS LOS TRABAJADORES</h1>
		<p>
		<b>Responsabilidades</b><br/>
		a)	Cumplir con las normas establecidas en la empresa, las instrucciones recibidas de los superiores jerárquicos y las señales existentes, así como seguir la política de prevención. Preguntar al personal responsable en caso de dudas acerca del contenido o forma de aplicación de las normas e instrucciones, o sobre cualquier duda relativa al modo de desempeñar su trabajo.<br/>
		b)	Adoptar todas las medidas de prevención propias de la profesión u oficio desempeñado.<br/>
		c)	Informar inmediatamente al superior jerárquico directo y al personal con funciones específicas en prevención sobre cualquier condición o práctica que pueda suponer un peligro para la seguridad y salud de los trabajadores, así como notificar la ocurrencia de accidentes o incidentes potencialmente peligrosos.<br/>
		d)	Utilizar los equipos adecuados al trabajo que se realiza teniendo en cuenta el riesgo existente, usarlos de forma segura y mantenerlos en buen estado de conservación.<br/>
		e)	Utilizar y ajustar, alterar o reparar el equipo sólo si se está autorizado.<br/>
		f)	No anular y utilizar correctamente los equipos y dispositivos de seguridad, en particular los de protección individual.<br/>
		g)	Mantener las áreas de trabajo limpias y ordenadas. Eliminar lo innecesario y clasificar lo útil.<br/>
		h)	Cooperar con la empresa en todas aquellas actividades destinadas a la prevención de riesgos laborales.<br/>
		i)	Cooperar en las labores de extinción de incendios, evacuación en caso de emergencia y salvamento de las víctimas en caso de accidente.<br/>
		j)	Todas aquellas otras funciones y responsabilidades definidas en el Sistema de Gestión Integrado de la empresa y en la Ley de PRL.</p>
		<bookmark title='9.4 FUNCIONES Y/O RESPONSABILIDADES DEL SERVICIO DE PREVENCIÓN AJENO' level='1' ></bookmark>
		<h1 class='tituloSeccion'>9.4. FUNCIONES Y/O RESPONSABILIDADES DEL SERVICIO DE PREVENCION AJENO:</h1>
		<p>
		a)	El diseño, aplicación y coordinación del Plan de Prevención de Riesgos Laborales que permita la integración de la prevención en la empresa. <br/>
		b)	La evaluación de los factores de riesgo que pueden afectar a la seguridad y salud de los trabajadores. <br/>
		c)	La planificación de la actividad preventiva, y la determinación de las prioridades en adopción de las medidas preventivas adecuadas y la vigilancia de su eficacia. <br/>
		d)	La información y formación de los trabajadores. <br/>
		e)	Los planes de emergencia. <br/>
		f)	El desarrollo de la normativa interna de aplicación necesaria para que la empresa lleve a cabo la Gestión de la Prevención de Riesgos Laborales.<br/>
		g)	El desarrollo de las actividades de vigilancia de la salud de los trabajadores en relación con los riesgos derivados del trabajo. <br/>
		h)	Todas aquellas otras funciones y responsabilidades definidas en el Sistema de Gestión Integrado de la empresa y en la Ley de PRL. <br/></p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		<div class='container'>
		<bookmark title='10. EVALUACIÓN DE RIESGOS' level='0' ></bookmark>
		<h1 class='tituloSeccion'>10. EVALUACIÓN DE RIESGOS</h1>
		<p>
		Por su importancia en la correcta gestión de la prevención, el RD 39/1997 de RSP, obliga al empresario a realizar una evaluación de riesgos, en concreto, en su artículo 3.2 a). Por otra parte, el artículo 15 de la LPRL, incluye la evaluación de riesgos como uno de los “principios de la actividad preventiva”.<br/><br/>

		El RSP, en su artículo 3, define la evaluación de riesgos, como el proceso dirigido a estimar la magnitud de aquellos riesgos QUE NO HAYAN PODIDO EVITARSE, obteniendo la información necesaria para que el empresario esté en condiciones de tomar una decisión apropiada sobre la necesidad de adoptar medidas preventivas y, en tal caso, sobre el tipo de medidas que deben adoptarse.<br/><br/>

		La Evaluación de Riesgos pretende poner de manifiesto las posibles situaciones de riesgo, para aplicar una acción correctora y eliminar o minimizar el riesgo en su máxima medida.<br/><br/>
		La <b>metodología de evaluación</b> se ha basado en las indicaciones que se establecen: </p>
		<ul class='list-style-type:square;'>
			<li>La Ley 31/1995 de Prevención de Riesgos Laborales (modificada por la Ley 54/2003)</li>
			<li>El R.D. 39/1997 por el que se aprueba el Reglamento de los Servicios de Prevención.</li>
			<li>Disposiciones y Reales Decretos que complementan y desarrollan las normas anteriormente citadas que establecen las condiciones mínimas de seguridad y salud a cumplir por el empresario.</li>
			<li>Criterio general de evaluación basado en la Guía del Instituto Nacional de Seguridad e Higiene en el Trabajo: “Evaluación de Riesgos Laborales”.</li>
		</ul>
		<p>
		La evaluación se dividirá en dos partes:<br/>
		A) CONDICIONES GENERALES DE SEGURIDAD Y SALUD<br/>
		B) EVALUACIÓN ESPECÍFICA DE RIESGOS POR PUESTOS<br/><br/>

		En la evaluación se contemplarán todos los puestos de la empresa y considerará, en su caso, la necesidad de asegurar la protección de los trabajadores especialmente sensibles a determinados riesgos (por sus características personales, estado biológico o discapacidad física, psíquica o sensorial).<br/><br/>
		La evaluación inicial de riesgos deberá ser revisada, según el art. 4.2 del RD 39/97, con ocasión de que los puestos de trabajo puedan verse afectados por:
		</p>
		<ul style='list-style-type:none;'>
			<li>I. Incorporación de “algo nuevo” (art. 4.2.a RD 39/97): la elección de equipos de trabajo, de sustancias o preparados químicos, de introducción de nuevas tecnologías o la modificación en el acondicionamiento de los lugares de trabajo.</li>
			<li>II. El cambio en las condiciones de trabajo (art. 4.2.b RD 39/97).</li>
		</ul>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		<div class='container'>
		<ol style='list-style-type:none;'>
			<li>III. La incorporación de un trabajador cuyas características personales o estado biológico conocido lo hagan especialmente sensible a las condiciones del puesto (art. 4.2.c RD 39/97)</li>
			<li>IV. El acuerdo o convenio entre las partes (art. 6.2 RD 39/97)</li>
			<li>V. Por el análisis de la situación epidemiológica (art. 6.1 d RD 39/97)</li>
			<li>VI. Por la investigación de las causas de los accidentes y sus daños (art. 6.1 a RD 39/97)</li>
			<li>VII. Detección de daños para la Salud en determinados puestos o inadecuación o insuficiencia de medidas preventivas (art. 6.1 RD 39/97)</li>
			<li>VIII. Por la acción o ejecución de las actividades para la reducción del riesgo (art. 6.1 c RD 39/97)</li>
			<li>IX. Por la acción o ejecución de actividades para el control del riesgo (art. 6.1 c RD 39/97)</li>
			<li>X. Cuando así lo establezca una disposición específica o normativa de aplicación (art. 6.1 RD 39/97)</li>
			<li>XI. El paso o transcurrir del tiempo (art. 6.2 RD 39/97)</li>
		</ol>
		<p>
		Las evaluaciones iniciales y/o periódicas realizadas para la empresa <b>".$datos['pregunta0']."</b> estarán contenidas en este Plan como documentos anexos.
		</p>
		<bookmark title='11. INFORMACIÓN, CONSULTA Y PARTICIPACIÓN' level='0' ></bookmark>
		<h1 class='tituloSeccion'>11. INFORMACIÓN, CONSULTA Y PARTICIPACIÓN</h1>
		<p>A fin de dar cumplimiento al deber de protección establecido en la presente Ley, art. 18, <b>".$datos['pregunta0']."</b> tiene implantado el procedimiento comunicación, por el que se definen las actuaciones a realizar para que los trabajadores reciban todas las informaciones necesarias, en relación con:
		</p>
		<ul style='list-style-type:square;'>
			<li>Los riesgos para la seguridad y la salud de los trabajadores en el trabajo, tanto aquellos que afecten a la empresa en su conjunto como a cada tipo de puesto de trabajo o función.</li>
			<li>Las medidas y actividades de protección y prevención aplicables a los riesgos señalados en el apartado anterior.</li>
			<li>Las medidas adoptadas de conformidad con lo dispuesto en el artículo 20 de la Ley 31/95 de Prevención de Riesgos Laborales.</li>
		</ul>
		<p>
		Asimismo, <b>".$datos['pregunta0']."</b> actualmente no cuenta con representantes de los trabajadores, la información a que se refiere el presente apartado se facilitará por el empresario a los trabajadores a través del Interlocutor de prevención de riesgos laborales; no obstante, a cada trabajador se le informa de los riesgos específicos que afecten a su puesto de trabajo o función y de las medidas de protección y prevención aplicables a dichos riesgos.<br/><br/>

<b>".$datos['pregunta0']."</b> consulta a los trabajadores, y permitir su participación, en el marco de todas las cuestiones que afecten a la seguridad y a la salud en el trabajo, de conformidad con lo dispuesto en el capítulo V de la Ley de Prevención de Riesgos Laborales.<br/>
<b>Procedimiento de Información, Consulta y Participación de los trabajadores (apartado 11)</b>

		</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		<div class='container'>
		<bookmark title='12. FORMACIÓN' level='0' ></bookmark>
		<h1 class='tituloSeccion'>12. FORMACIÓN</h1>
		<p>En cumplimiento del art. 19 LPRL y del deber de protección, <b>".$datos['pregunta0']."</b> garantiza que cada trabajador reciba una formación teórica y práctica, suficiente y adecuada, en materia preventiva, tanto en el momento de su contratación, cualquiera que sea la modalidad o duración de ésta, como cuando se produzcan cambios en las funciones que desempeñe o se introduzcan nuevas tecnologías o cambios en los equipos de trabajo, tal y como recoge en su procedimiento.<br/><b>Procedimiento de Formación</b>.<br/><br/>

			La formación está centrada específicamente en el puesto de trabajo o función de cada trabajador, adaptándose a la evolución de los riesgos y a la aparición de otros nuevos y repetirse periódicamente, si fuera necesario.<br/><br/>
			La información y los planes de formación de los trabajadores estarán contenidos en el documento de Planificación.
		</p>
		<bookmark title='13. EQUIPOS DE PROTECCIÓN INDIVIDUAL' level='0' ></bookmark>
		<h1 class='tituloSeccion'>13. EQUIPOS DE PROTECCIÓN INDIVIDUAL</h1>
		<p>Según el Real Decreto 773/1997, 30 de mayo, sobre disposiciones mínimas de seguridad y salud relativas a la utilización por los trabajadores de equipos de protección individual se entenderá por Equipo de Protección Individual (EPI) cualquier equipo destinado a ser llevado o sujetado por el trabajador para que le proteja de uno o varios riesgos que puedan amenazar su seguridad o su salud, así como cualquier complemento o accesorio destinado a tal fin.<br/><br>

		Este mismo Real Decreto, en su artículo 3, recoge que el empresario estará obligado a:<br/>
		<i>“a) Determinar los puestos de trabajo en los que deba recurrirse a la protección individual conforme a lo establecido en el artículo 4 y precisar, para cada uno de estos puestos, el riesgo o riesgos frente a los que debe ofrecerse protección, las partes del cuerpo a proteger y el tipo de equipo o equipos de protección individual que deberán utilizarse.<br/>
		b) Elegir los equipos de protección individual conforme a lo dispuesto en los artículos 5 y 6 de este Real Decreto, manteniendo disponible en la empresa o centro de trabajo la información pertinente a este respecto y facilitando información sobre cada equipo.<br/>
		c) Proporcionar gratuitamente a los trabajadores los equipos de protección individual que deban utilizar, reponiéndolos cuando resulte necesario.<br/>
		d) Velar por que la utilización de los equipos se realice conforme a lo dispuesto en el artículo 7 del presente Real Decreto.<br/>
		e) Asegurar que el mantenimiento de los equipos se realice conforme a lo dispuesto en el artículo 7 del presente Real Decreto”.<br/>
		<b>Procedimiento de Equipos de Protección Individual (EPI)</b></i>.
		</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		<div class='container'>
		<bookmark title='14. COORDINACIÓN DE ACTIVIDADES EMPRESARIALES' level='0' ></bookmark>
		<h1 class='tituloSeccion'>14. COORDINACIÓN DE ACTIVIDADES EMPRESARIALES</h1>
		<bookmark title='14.1 ÁMBITO Y OBJETO DE APLICACIÓN' level='1' ></bookmark>
		<h1 class='tituloSeccion'>14.1. ÁMBITO Y OBJETO DE APLICACIÓN</h1>
		<p><b>La Ley 31/1995 de Prevención de Riesgos Laborales establece en su artículo 24 la necesidad de llevar a cabo una</b> Coordinación de actividades empresariales. Concretamente dice:<br/><br/>

		<i>“1. Cuando en un mismo centro de trabajo desarrollen actividades trabajadores de dos o más empresas, éstas deberán cooperar en la aplicación de la normativa sobre prevención de riesgos laborales. A tal fin, establecerán los medios de coordinación que sean necesarios en cuanto a la protección y prevención de riesgos laborales y la información sobre los mismos a sus respectivos trabajadores, en los términos previstos en el apartado 1 del artículo 18 de esta Ley.<br/>
			2. El empresario titular del centro de trabajo adoptará las medidas necesarias para que aquellos otros empresarios que desarrollen actividades en su centro de trabajo reciban la información y las instrucciones adecuadas, en relación con los riesgos existentes en el centro de trabajo y con las medidas de protección y 	prevención correspondientes, así como sobre las medidas de emergencia a aplicar, para su traslado a sus respectivos trabajadores.<br/>
			3. Las empresas que contraten o subcontraten con otras la realización de obras o servicios correspondientes a la propia actividad de aquéllas y que se desarrollen en sus propios centros de trabajo deberán vigilar el cumplimiento por dichos contratistas y subcontratistas de la normativa de prevención de riesgos laborales.<br/>
			4. Las obligaciones consignadas en el último párrafo del apartado 1 del artículo 41 de esta Ley serán también de aplicación, respecto de las operaciones contratadas, en los supuestos en que los trabajadores de la empresa contratista o subcontratista no presten servicios en los centros de trabajo de la empresa principal, siempre que tales trabajadores deban operar con maquinaria, equipos, productos, materias primas o útiles 	proporcionados por la empresa principal.<br/>
			5. Los deberes de cooperación y de información e instrucción recogidos en los apartados 1 y 2 serán de aplicación respecto de los trabajadores autónomos que desarrollen actividades en dichos centros de trabajo.“<br/>
			<b>Procedimiento de Coordinación de Actividades</b></i>.
		</p>
		<bookmark title='14.2 CONTRATAS, SUBCONTRATAS, TRABAJADORES, AUTONÓNOMOS' level='1' ></bookmark>
		<h1 class='tituloSeccion'>14.2. CONTRATAS, SUBCONTRATAS, TRABAJADORES AUTONÓNOMOS</h1>
		<p>Con la entrada en vigor del nuevo R.D. 171/2004, de 30 de enero, por el que se desarrolla el artículo 24 de la Ley 31/1995, de Prevención de Riesgos Laborales, en materia de coordinación de actividades empresariales, se impulsa aún más la necesidad de COORDINACIÓN EN MATERIA PREVENTIVA ENTRE EMPRESAS.<br/><br/>

		Así, tenemos:<br/>
		Cuando <b>".$datos['pregunta0']."</b> esté desarrollando actividades en un mismo centro de trabajo donde desarrollen actividades trabajadores de dos o más empresas, las empresas deberán cooperar en la aplicación de la normativa sobre prevención de riesgos laborales. A tal fin, establecerán los medios de coordinación que sean necesarios en cuanto a la protección y prevención de riesgos laborales y la información sobre los mismos a sus respectivos trabajadores, en los términos previstos en el apartado 1 del artículo 18 de la LPRL.<br/><br/>
		</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		<div class='container'>
		<p>
		Éstas deberán cooperar en la aplicación de la normativa sobre prevención de riesgos laborales. Apartado 1, artículo 24 de la Ley 31/1995, de 8 de noviembre, de Prevención de Riesgos Laborales.  Además, se tendrán en cuenta las obligaciones previstas en el Real Decreto 171/2004, de 30 de enero, por el que se desarrolla el artículo 24 de la Ley 31/1995 anteriormente citada, en materia de coordinación de actividades empresariales.<br/><br/> 
		Este R.D. se presenta con la obligatoriedad de informarse recíprocamente sobre los riesgos específicos de las actividades que desarrollen las empresas que desarrollen sus actividades en un mismo centro de trabajo. Así mismo, dicha información deberá ser suficiente y tendrá que proporcionarse antes del inicio de las actividades.<br/><br/>

		Además, el empresario principal debe vigilar el cumplimiento de la normativa de prevención de riesgos laborales por parte de las empresas contratistas y subcontratistas de obras y servicios correspondientes a su propia actividad y que desarrollen en su propio centro de trabajo (Art. 10. R.D. 171/2004), por lo que debe demandar a las empresas concurrentes la siguiente documentación:<br/><br/>
		</p>
		<ol>
			<li>Indicación de la modalidad de organización preventiva que tiene su empresa en cumplimiento del Art. 10 del R.D. 39/1.997. </li>
			<li>Evaluación de riesgos, correspondiente a cada puesto de trabajo que ha de intervenir en el centro de trabajo.</li>
			<li>Acreditación de los trabajadores que se encuentren en el centro de trabajo y corriente de pago en la Seguridad Social (TC1 y TC2). </li>
			<li>Certificado de aptitud del trabajador para el puesto que va a desarrollar (vigilancia de la salud, Art. 22 Ley 31/1.995). </li>
			<li>Certificado de haber recibido la formación e información preventiva preceptiva (Art. 18 y 19 Ley 31/1.995). </li>
			<li>Nombramiento de un trabajador como coordinador de la seguridad y salud en el centro de trabajo.</li>
			<li>Determinación del modelo preventivo.</li>
			<li>Justificante de entrega de Equipos de Protección Individual (EPI) firmados por el trabajador. </li>
		</ol>
		<p><b>DOCUMENTACIÓN EN CASO DE ÁPORTAR MAQUINARIA Y/O MEDIOS AUXILIARES (PROPIOS, SUBCONTRATADOS O ALQUILADOS) </b>
		</p>
		<ol>
			<li>Relación de máquinas y/o medios auxiliares que tienen previsto utilizar en el centro de trabajo. </li>
			<li>Relación del personal autorizado y con formación (especial y/o adecuada) para el uso de las máquinas y/o medios auxiliares descritos en el apartado anterior.</li>
			<li>Certificado de características y certificado de conformidad de los equipos de trabajo de acuerdo con los requisitos del R.D. 1215/97.</li>
		</ol>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		<div class='container'>
		<bookmark title='14.3 RECURSOS PREVENTIVOS' level='1' ></bookmark>
		<h1 class='tituloSeccion'>14.3. RECURSOS PREVENTIVOS</h1>
		<p>Respecto a los denominados Recursos Preventivos, el <b>Artículo 32 bis. (Introducido por el artículo 4.3. de la Ley 54/2003)</b> establece la necesidad de presencia de recursos preventivos en las empresas. Dicho artículo establece:<br/><br/>

		<i>“1. La presencia en el centro de trabajo de los recursos preventivos, cualquiera que sea la modalidad de organización de dichos recursos, será necesaria en los siguientes casos:<br/>
			* A completar por el técnico en función del organigrama de la empresa.<br/>
			a) Cuando los riesgos puedan verse agravados o modificados en el desarrollo del proceso o la actividad, por la concurrencia de operaciones diversas que se desarrollan sucesiva o simultáneamente y que hagan 	preciso el control de la correcta aplicación de 	los métodos de trabajo.<br/>
			b) Cuando se realicen actividades o procesos que reglamentariamente sean considerados como peligrosos o con riesgos especiales.<br/>
			c) Cuando la necesidad de dicha presencia sea requerida por la Inspección de Trabajo y Seguridad Social, si las circunstancias del caso así lo exigieran debido a las condiciones de trabajo detectadas.<br/><br/>

			2. Se consideran recursos preventivos, a los que el empresario podrá asignar la presencia, los siguientes:<br/>
			a) Uno o varios trabajadores designados de la empresa.<br/>
			b) Uno o varios miembros del servicio de prevención propio de la empresa.<br/
			c) Uno o varios miembros del o los servicios de prevención ajenos concertados por la empresa.<br/>
			Cuando la presencia sea realizada por diferentes recursos preventivos éstos deberán colaborar entre sí.<br/><br/>

			3. Los recursos preventivos a que se refiere el apartado anterior deberán tener la capacidad suficiente, 	disponer de los medios necesarios y ser suficientes en número para vigilar el cumplimiento de las actividades 	preventivas, debiendo permanecer en el centro de trabajo durante el tiempo en que se mantenga la situación 	que determine su presencia.<br/><br/>

			4. No obstante lo señalado en los apartados anteriores, el empresario podrá asignar la presencia de forma 	expresa a uno o varios trabajadores de la empresa que, sin formar parte del servicio de prevención propio ni 	ser trabajadores designados, reúnan los conocimientos, la calificación y la experiencia necesarios en las 	actividades o procesos a que se refiere el apartado 1 y cuenten con la formación preventiva correspondiente, 	como mínimo, a las funciones del nivel básico. En este supuesto, tales trabajadores deberán mantener la necesaria colaboración con los recursos preventivos del empresario”.
		</i>.
		</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		<div class='container'>
		<bookmark title='14.4 MEDIOS DE COORDINACIÓN' level='1' ></bookmark>
		<h1 class='tituloSeccion'>14.4. MEDIOS DE COORDINACIÓN</h1>
		<p>El Capítulo V, del R.D. 171/2004, establece la necesidad de llevar a cabo medios de coordinación cuando existan en un mismo centro de trabajo dos o más empresas concurrentes.  A continuación se exponen los medios de coordinación propuestos por la normativa vigente:
		</p>
		<ul style='list-style-type:square;'>
			<li>Intercambio de información.</li>
			<li>Reuniones periódicas.</li>
			<li>Reuniones con los delegados de prevención de las subcontratas.</li>
			<li>Impartición de instrucciones sobre normas de seguridad a llevar a cabo en la empresa.</li>
			<li>Establecimiento conjunto de medidas específicas de prevención de los riesgos existentes en el centro de trabajo.</li>
			<li>Presencia de los recursos preventivos de las empresas concurrentes.</li>
			<li>La designación de un encargado de la coordinación de las actividades preventivas.</li>
		</ul>
		<bookmark title='15. CONTRATACIÓN DE TRABAJADORES DE UNA EMPRESA DE TRABAJO TEMPORAL' level='0' ></bookmark>
		<h1 style='margin-top:0px' class='tituloSeccion'>15. CONTRATACIÓN DE TRABAJADORES DE UNA EMPRESA DE TRABAJO TEMPORAL</h1>
		<p>Cuando la empresa decida la contratación de los servicios de personal perteneciente a una empresa de trabajo temporal seguirá las siguientes normas para dar cumplimiento tanto al Art. 28 de la Ley 31/95 de Prevención de Riesgos Laborales (Obligaciones de la empresa usuaria), como al R. D 216/99 Sobre empresas de trabajo temporal:<br/><br/>

			Antes del inicio del trabajo se informará a la empresa de trabajo temporal de la información acerca del puesto de trabajo a desempeñar, características, cualificaciones del trabajador necesarias, así como cualquier riesgo y equipos de protección necesario. Recordando a la ETT su obligación de trasladar dicha información al trabajador, así como facilitarle la formación necesaria.<br/><br/>

			Asimismo, la empresa está obligada a exigir a la ETT, (Art. 4 R. D. 216/1999) la información necesaria para asegurarse de que el trabajador puesto a su disposición reúne las siguientes condiciones:<br/><br/>

			<b>a.</b>	Ha sido considerado apto a través de un adecuado reconocimiento de su estado de salud para la realización de los servicios que deba prestar en las condiciones en que hayan de ser efectuados, de conformidad con lo dispuesto en el Art. 22 de la Ley de Prevención de Riesgos Laborales y en el Art. 37.3del Reglamento de los Servicios de Prevención.<br/><br/>

			<b>b.</b>	Posee las Cualificaciones y capacidades requeridas para el desempeño de las tareas que se le encomienden en las condiciones en que vayan a efectuarse y cuenta con la formación necesaria, todo ello en relación con la prevención de los riesgos a los que pueda estar expuesto, en los términos previstos en el Art. 19 de la Ley de Prevención de Riesgos Laborales y en sus disposiciones de desarrollo.<br/><br/>
		</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>
	
	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		<div class='container'>
		<p>
			<b>c.</b>	Ha recibido las informaciones relativas a las características propias del puesto de trabajo y de las tareas a desarrollar, a las Cualificaciones y aptitudes requeridas y a los resultados de la evaluación de riesgos, conforme la información suministrada por la empresa usuaria.<br/>
			Otras obligaciones de la empresa usuaria respecto los trabajadores a incorporarse de la ETT, son: (Art. 4 R. D. 216/99).<br/><br/>

			<img src='../img/checkPP.png'> La empresa usuaria no permitirá el inicio de la prestación de servicios en la misma de un trabajador puesto a su disposición hasta que no tenga constancia del cumplimiento de las obligaciones del apartado anterior.<br/><br/>

			<img src='../img/checkPP.png'> La empresa usuaria informará a los delegados de prevención o, en su defecto, a los representantes legales de sus trabajadores, de la incorporación de todo trabajador puesto a disposición por una empresa de trabajo temporal, especificando el puesto de trabajo a desarrollar, sus riesgos y medidas preventivas y la información y formación recibidas por el trabajador. El trabajador podrá dirigirse a estos representantes en el ejercicio de sus derechos reconocidos en el presente Real Decreto y, en general, en el conjunto de la legislación sobre prevención de riesgos laborales.<br/><br/>

			La información a la que se refiere el párrafo anterior será igualmente facilitada por la empresa usuaria a su servicio de prevención o, en su caso, a los trabajadores designados para el desarrollo de las actividades preventivas.<br/><br/>

			En los siguientes documentos se describe la información a suministrar, así como un documento de entrega de equipos de protección:<br/><br/>
		</p>
		<bookmark title='16. CONTRATACIÓN DE TRABAJADORES ESPECIALMENTE SENSIBLES' level='0' ></bookmark>
		<h1 class='tituloSeccion'>16. CONTRATACIÓN DE TRABAJADORES ESPECIALMENTE SENSIBLES</h1>
		<p>Atendiendo al Art. 25 de la Ley de Prevención de Riesgos Laborales, referente a la protección de trabajadores 	especialmente sensibles a determinados riesgos, el empresario  tendrá en cuenta en las evaluaciones de riesgos a aquellos trabajadores que, por sus características personales o estado biológico conocido, incluidos 	aquellos que tengan reconocida la situación de discapacidad física, psíquica o sensorial, sean especialmente sensibles a los riesgos derivados del trabajo, con objeto de adoptar las medidas preventivas y de protección necesarias.<br/><br/>

		A estos efectos deberá comunicar a ANESCO la existencia de dichos trabajadores para poder asesorar al empresario sobre la forma de proteger adecuadamente a dichos trabajadores. Dicha comunicación a ANESCO competerá al Departamento de Prevención de la Empresa, y se realizará a través de un comunicado por escrito que contenga los nombres y apellidos del trabajador sensible, categoría profesional, y puesto que desempeña, desplazándose un técnico de ANESCO para analizar las funciones que desempeñe y recogerlas a través de anexo a la presente evaluación con las medidas de prevención y protección necesarias.<br/>
		<b>Procedimiento de Protección de Trabajadora/es especialmente sensibles</b>
		</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		<div class='container'>
		<bookmark title='17. PROTECCIÓN DE MENORES' level='0' ></bookmark>
		<h1 class='tituloSeccion'>17. PROTECCIÓN DE MENORES</h1>
		<p>El empresario, antes de la incorporación al trabajo de jóvenes menores de 18 años y, previamente a una modificación de sus condiciones de trabajo, efectuará una evaluación de los puestos de trabajo a desempeñar 	en cualquier actividad susceptible de presentar riesgos que puedan poner en peligro la seguridad o salud de 	estos trabajadores (Artículo 27 de la Ley 31/1995). A estos efectos deberá comunicar a ANESCO la existencia de dichos trabajadores para poder asesorar al empresario sobre la forma de proteger adecuadamente a 	dichos trabajadores. Dicha comunicación a ANESCO competerá al Departamento de Prevención de la 	Empresa, y se realizará a través de un comunicado por escrito que contenga los nombres y apellidos del 	trabajador, edad, categoría profesional, y puesto que vaya a desempeñar, desplazándose un técnico de ANESCO para analizar las funciones que desempeñe, naturaleza, grado y duración de la exposición a riesgos en su puesto de trabajo. Se realizará evaluación específica por el Dpto. técnico de ANESCO y la empresa informará directamente al trabajador y a sus padres o tutores de las conclusiones de la misma, con los riesgos y medidas preventivas a adoptar, para el conocimiento por éstos. <br/>
			<b>Procedimiento de protección a los menores</b>
		</p>
		<bookmark title='18. PROTECCIÓN A LA MATERNIDAD' level='0' ></bookmark>
		<h1 style='margin-top:0px' class='tituloSeccion'>18. PROTECCIÓN A LA MATERNIDAD</h1>
		<p>Si los resultados de la evaluación revelasen un riesgo para la seguridad y salud o una posible repercusión sobre el embarazo o la lactancia de las trabajadoras embarazadas, el empresario adoptará las medidas necesarias para evitar la exposición a dicho riesgo según contempla el artículo 26 de la Ley de Prevención de Riesgos Laborales, debiendo comunicar a ANESCO dichas circunstancias para poder asesorar al empresario sobre la forma de proteger adecuadamente a dichos trabajadores. Dicha comunicación la realizará el Departamento de Prevención de la Empresa y posterior visita a través del técnico para analizar las funciones que desempeñe	y recogerlas a través de anexo a la presente evaluación con las medidas de prevención y protección necesarias 	en su caso o, si estas no fueran posibles, establecimiento de reunión entre ANESCO, Dpto. Laboral o RR.HH 	de la empresa y dirección de ésta, para el análisis de nuevo puesto de trabajo que no contemple riesgos para 	la trabajadora o el feto o, en última instancia, la suspensión del contrato de trabajo conforme al sistema General de la Seguridad Social para dichas situaciones, previo informe de ANESCO.<br/><br/>

			El empresario adoptará las medidas necesarias para evitar la exposición de la trabajadora afectada a riesgos laborales durante su jornada, a través de una adaptación de las condiciones o del tiempo de trabajo. <br/><br/>

			Cuando la adaptación de las condiciones o del tiempo de trabajo no resultase posible o, a pesar de tal adaptación, las condiciones de un puesto de trabajo pudieran influir negativamente en la salud de la trabajadora embarazada o del feto, y así lo certifique el médico que en el régimen de la Seguridad Social aplicable asista facultativamente a la trabajadora, ésta deberá desempeñar un puesto de trabajo o función diferente y compatible con su estado. <br/><br/>

			El empresario deberá determinar, previa consulta con los representantes de los trabajadores, la relación de los puestos de trabajo exentos de riesgos a estos efectos. El cambio de puesto o función se llevará a cabo de conformidad con las reglas y criterios que se apliquen en los supuestos de movilidad funcional y tendrá efectos hasta el momento en que el estado de salud de la trabajadora permita su reincorporación al anterior puesto. <br/><br/>
		</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		<div class='container'>
		<p>
			En el supuesto de que no existiese puesto de trabajo o función compatible, la trabajadora podrá ser destinada a un puesto no correspondiente a su grupo o a categoría equivalente, si bien conservará el derecho al conjunto 	de retribuciones de su puesto de origen.<br/><br/>

			Lo dispuesto anteriormente también se aplicará durante el período de lactancia, si las condiciones de trabajo pudieran influir negativamente en la salud de la mujer o del hijo y así lo certificase el médico que, en el régimen de Seguridad Social aplicable, asista facultativamente a la trabajadora. <br/><br/>

			Las trabajadoras embarazadas tendrán derecho a ausentarse del trabajo, con derecho a remuneración, para la realización de exámenes prenatales y técnicas de preparación al parto, previo aviso al empresario y 	justificación de la necesidad de su realización dentro de la jornada de trabajo.<br/>
			<b>Procedimiento de protección a la maternidad</b>
		</p>
		<bookmark title='19. SEGUIMIENTO Y MEDICIÓN' level='0' ></bookmark>
		<h1 class='tituloSeccion'>19. SEGUIMIENTO Y MEDICIÓN</h1>
		<p>
		La evaluación periódica de los objetivos marcados y del sistema preventivo y del conjunto de elementos fundamentales que lo componen es una actividad ineludible que ha de permitir su mejora continua y garantizar, tanto el cumplimiento de las exigencias reglamentarias en materia de gestión preventiva, como la eficacia del propio sistema.<br/><br/>
		La realización de auditorías del Plan de Prevención, con la consiguiente valoración del grado de cumplimiento de la implantación, estará incluida en el documento de Planificación de la Actividad Preventiva.<br/><br/>

		Las condiciones de seguridad de los lugares, instalaciones, equipos y ambiente de trabajo deben controlarse periódicamente y deben someterse aún mantenimiento adecuado para evitar el deterioro de las mismas a lo largo del tiempo. Las actividades específicas de mantenimiento y control necesarias a tal efecto están establecidas en función de los resultados de la evaluación de riesgos, aunque en algunos casos dichas actividades se concretan y vienen directamente impuestas por una normativa específica.
		</p>
		<bookmark title='20. MEDIDAS DE EMERGENCIA' level='0' ></bookmark>
		<h1 class='tituloSeccion'>20. MEDIDAS DE EMERGENCIA</h1>
		<p>
		<b>".$datos['pregunta0']."</b> en aplicación del art. 20 de la LPRL, y teniendo en cuenta su tamaño y actividad, así como la posible presencia de personas ajenas a la misma, ha analizado las posibles situaciones de emergencia y adoptado las medidas necesarias en materia de primeros auxilios, lucha contra incendios y evacuación de los trabajadores, designando para ello al personal encargado de poner en práctica estas medidas y comprobando periódicamente, en su caso, su correcto funcionamiento. El citado personal dispone de la formación necesaria, suficiente en número y dispone del material adecuado, en función de las circunstancias antes señaladas.<br/>
			<b>Procedimiento del Plan de Emergencia.</b>
		</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>

	<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm'>
	<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
		<page_header>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		<div class='container'>
		<bookmark title='21. DAÑOS A LA SALUD: ACCIDENTES DE TRABAJO Y ENFERMEDADES PROFESIONALES' level='0' ></bookmark>
		<h1 class='tituloSeccion'>21. DAÑOS A LA SALUD: ACCIDENTES DE TRABAJO Y ENFERMEDADES PROFESIONALES</h1>
		<p>
		Cuando se produzca un daño para la salud de los trabajadores o cuando, con ocasión de la vigilancia de la salud de los mismos aparezcan indicios de que las medidas de prevención resultan insuficientes, el empresario llevará a cabo una investigación al respecto, al objeto de detectar las causas de los mismos.<br/><br/>

		En relación con dichos daños, deben comunicarse a la autoridad competente los datos e informaciones indicados en la normativa específica aplicable (partes de accidentes de trabajo a través del Sistema DELTA y de enfermedades profesionales a través del sistema CEPROSS). Asimismo, se comunicará a la Autoridad Sanitaria las posibles sospechas de enfermedades profesionales<br/>
			<b>Procedimiento de Actuación en caso de Accidente de Trabajo.</b>

		</p>
		<bookmark title='22. VIGILANCIA DE LA SALUD' level='0' ></bookmark>
		<h1 class='tituloSeccion'>22. VIGILANCIA DE LA SALUD</h1>
		<p>
		<b>".$datos['pregunta0']."</b> garantiza a los trabajadores a su servicio la vigilancia periódica de su estado de salud en función de los riesgos inherentes al trabajo.<br/><br/>
			Esta vigilancia se lleva a cabo cuando el trabajador preste su consentimiento. De este carácter voluntario sólo se exceptuarán, previo informe de los representantes de los trabajadores, los supuestos en los que la realización de los reconocimientos sea imprescindible para evaluar los efectos de las condiciones de trabajo sobre la salud de los trabajadores o para verificar si el estado de salud del trabajador puede constituir un peligro para el mismo, para los demás trabajadores o para otras personas relacionadas con la empresa o cuando así esté establecido en una disposición legal en relación con la protección de riesgos específicos y actividades de especial peligrosidad.<br/>
			Los resultados de la vigilancia a que se refiere el apartado anterior serán comunicados a los trabajadores afectados.<br/><br/>

			El acceso a la información médica de carácter personal se limitará al personal médico y a las autoridades sanitarias que lleven a cabo la vigilancia de la salud de los trabajadores, sin que pueda facilitarse a <b>".$datos['pregunta0']."</b> o a otras personas sin consentimiento expreso del trabajador.<br/><br/>

			No obstante lo anterior, <b>".$datos['pregunta0']."</b> y las personas u órganos con responsabilidades en materia de prevención serán informados de las conclusiones que se deriven de los reconocimientos efectuados en relación con la aptitud del trabajador para el desempeño del puesto de trabajo o con la necesidad de introducir o mejorar las medidas de protección y prevención, a fin de que puedan desarrollar correctamente su funciones en materia preventiva.<br/><br/>

			Las medidas de vigilancia y control de la salud de los trabajadores se llevarán a cabo por personal sanitario con competencia técnica, formación y capacidad acreditada.<br/>
			<b>Procedimiento de Vigilancia de la Salud</b><br/>.

		</p>
		</div>
		<page_footer>
	    	<div class='cajaRevision'>
	    		REVISIÓN: 00 - ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    	<div class='cajaNumeroPagina'>
	    		PÁGINA [[page_cu]] DE [[page_nb]]
	    	</div>
	  </page_footer>
	</page>";


	return $contenido;
}

function listadoRiesgos($codigoEvaluacion,$ref,$logoCliente,$fecha){
	$contenido='';
	$riesgos=consultaBD('SELECT nombre, riesgos_evaluacion_general.codigo FROM riesgos_evaluacion_general INNER JOIN riesgos ON riesgos_evaluacion_general.codigoRiesgo=riesgos.codigo WHERE codigoEvaluacion='.$codigoEvaluacion,true);
	$i=1;
	$probabilidades=array(1=>'BAJA',2=>'MEDIA',3=>'ALTA',''=>'');
	$consecuencias=array(1=>'LIGERAMENTE DAÑINO',2=>'DAÑINO',3=>'EXTREMADAMENTE DAÑINO',''=>'');
	$clases=array('TRIVIAL'=>'panel-default','Riesgo Trivial'=>'panel-default','TOLERABLE'=>'panel-amarillo','MODERADO'=>'panel-warning','Riesgo Moderado'=>'panel-warning','IMPORTANTE'=>'panel-danger','INTOLERABLE'=>'panel-inverse');
	$primero=true;
	while($riesgo=mysql_fetch_assoc($riesgos)){
		$num=$i<10?'0'.$i:$i;
	if(!$primero){
		/*$contenido.="	
				</div>
			</page>
			<page backtop='30mm' backleft='0mm' backright='0mm' backbottom='0mm' footer='page'>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<page_header>
				<table class='cabecera'>
					<tr>
						<td class='a20'><img src='../img/logoOferta.png' /></td>
						<td class='a60'>EVALUACIÓN DE RIESGOS</td>
						<td class='a20'>".$logoCliente."</td>
					</tr>
				</table>
			</page_header>
		
			<div class='container'>";*/
		$contenido.='<br/><br/>';
	}
	$contenido.="
		<table class='tablaDatos'>
			<tr>
				<th colspan='3' class='a100'>RIESGO ".$ref." ".$num."</th>
			</tr>
			<tr>
				<td colspan='3' class='a100'>".$riesgo['nombre']."</td>
			</tr>";
	$bloques=consultaBD('SELECT * FROM riesgos_evaluacion_general_bloques WHERE codigoRiesgo='.$riesgo['codigo'],true);
	while($bloque=mysql_fetch_assoc($bloques)){
		if($bloque['prioridad']==''){
			$prioridad='SIN EVALUAR';
			$clase='';
		} else {
			$prioridad=explode('-', $bloque['prioridad']);
			$prioridad=trim(str_replace('RIESGO ', '', $prioridad[1]));
			$clase=$clases[$prioridad];
		}
		$contenido.="
			<tr>
				<th colspan='3' class='a100' style='background:black;'></th>
			</tr>
			<tr>
				<th colspan='3' class='a100'>FACTOR DE RIESGOS</th>
			</tr>
			<tr>
				<td colspan='3' class='a100'>".$bloque['descripcionRiesgo']."</td>
			</tr>
			<tr>
				<th colspan='3' class='a100 centro'>DESCRIPCIÓN</th>
			</tr>";
		$contenido.="<tr><td colspan='3' class='a100 noBordeTop'>".nl2br($bloque['descripcion2'])."</td></tr>";
			$imagenes=consultaBD('SELECT ficheroImagen FROM evaluacion_imagenes WHERE codigoDescripcion='.$bloque['codigo'],true);
			$ultimo=consultaBD('SELECT COUNT(codigo) AS total FROM evaluacion_imagenes WHERE codigoDescripcion='.$bloque['codigo'],true,true);
			$nuevo=true;
			$num=1;
			while($imagen=mysql_fetch_assoc($imagenes)){
				$img="../documentos/riesgos/".$imagen['ficheroImagen'];
				$img="<img src='".$img."'>";
				if($nuevo){
					$claseFoto='imagenRiesgo';
					if($num==$ultimo['total']){
						$claseFoto='imagenRiesgo2';	
					}
					$contenido.="<tr><td colspan='3' class='a100 noBordeTopBottom'>";
					$contenido.="<table style='width:100%;border:0px;'><tr>";
					$contenido.="<td class='".$claseFoto."'>";
					$contenido.=$img;
					$contenido.="</td>";
					$nuevo=false;
				} else {
					$contenido.="<td class='imagenRiesgo'>";
					$contenido.=$img;
					$contenido.="</td>";
					$contenido.="</tr></table></td></tr>";
					$nuevo=true;
				}
				$num++;
			}
			if(!$nuevo){
				$contenido.="</tr></table></td></tr>";
			}
		$contenido.="
				<tr>
					<th colspan='3' class='a100 centro'>EVALUACIÓN</th>
				</tr>
				<tr>
					<th class='a33 centro'>PROBABILIDAD</th>
					<th class='a33 centro'>CONSECUENCIAS</th>
					<th class='a33 centro'>NIVEL DE RIESGO</th>
				</tr>
				<tr>
					<td class='a33 centro'>".$probabilidades[$bloque['probabilidad']]."</td>
					<td class='a33 centro'>".$consecuencias[$bloque['consecuencias']]."</td>
					<td class='a33 centro ".$clase."'>".$prioridad."</td>
				</tr>
				<tr>
					<th colspan='3' class='a100 centro'>PROPUESTA DE MEDIDAS PREVENTIVAS </th>
				</tr>";
				$n=1;
				$imagenes=consultaBD('SELECT ficheroImagen FROM evaluacion_medidas_imagenes WHERE codigoMedida='.$bloque['codigo'],true);
				if(mysql_num_rows($imagenes)>0){
				$claseTD='noBordeTopBottom';
				} else {
				$claseTD='';
				}
				$contenido.="<tr><td colspan='3' class='a100 ".$claseTD."'>".nl2br($bloque['medida'])."</td></tr>";
				$ultimo=consultaBD('SELECT COUNT(codigo) AS total FROM evaluacion_medidas_imagenes WHERE codigoMedida='.$bloque['codigo'],true,true);
				$nuevo=true;
				$num=1;
				while($imagen=mysql_fetch_assoc($imagenes)){
				if($nuevo){
					$claseFoto='imagenRiesgo';
					if($num==$ultimo['total']){
						$claseFoto='imagenRiesgo2';	
					}
					$contenido.="<tr><td colspan='3' class='a100 noBordeTopBottom'>";
					$contenido.="<table style='width:100%;border:0px;'><tr>";
					$contenido.="<td class='".$claseFoto."'>";
					$contenido.="<img src='../documentos/riesgos/".$imagen['ficheroImagen']."'>";
					$contenido.="</td>";
					$nuevo=false;
				} else {
					$contenido.="<td class='imagenRiesgo'>";
					$contenido.="<img src='../documentos/riesgos/".$imagen['ficheroImagen']."'>";
					$contenido.="</td>";
					$contenido.="</tr></table></td></tr>";
					$nuevo=true;
					}
					$num++;
				}
				if(!$nuevo){
					$contenido.="</tr></table></td></tr>";
				}
				$n++;
		}
		$contenido.="</table>";
		$i++;
		$primero=false;

	}
	return $contenido;
}

//Fin parte de generación de informes