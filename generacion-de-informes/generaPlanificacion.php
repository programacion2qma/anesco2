<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('../../api/phpexcel/PHPExcel.php');
	require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');

	$ficheros=array();
	$evaluacion=consultaBD('SELECT evaluacion_general.codigo, evaluacion_general.codigoCliente FROM evaluacion_general INNER JOIN informes ON evaluacion_general.codigo=informes.codigoEvaluacion WHERE informes.codigo='.$_GET['codigo'],true,true);
	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("../documentos/plantillaPlanificacionPreventiva.xlsx");
	planificacionPreventiva($objPHPExcel,$evaluacion['codigo']);
	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('../documentos/Planificacion_Preventiva_1.xlsx');
	array_push($ficheros, '../documentos/Planificacion_Preventiva_1.xlsx');

	$evaluaciones=consultaBD('SELECT * FROM evaluacion_general WHERE tipoEvaluacion="PUESTOS" AND codigoCliente='.$evaluacion['codigoCliente']);
	$i=2;
	while($item=mysql_fetch_assoc($evaluaciones)){
		$objReader = new PHPExcel_Reader_Excel2007();
		$objPHPExcel = $objReader->load("../documentos/plantillaPlanificacionPreventiva.xlsx");
		planificacionPreventiva($objPHPExcel,$item['codigo']);
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('../documentos/Planificacion_Preventiva_'.$i.'.xlsx');
		array_push($ficheros, '../documentos/Planificacion_Preventiva_'.$i.'.xlsx');
		$i++;
	}

	generaZip($ficheros);

?>