<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión de protocolos


function operacionesProtocolos(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('protocolos');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('protocolos');
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminado('protocolos',$_POST['elimina']);
	}

	mensajeResultado('nombre',$res,'EPI');
    mensajeResultado('elimina',$res,'EPI', true);
}



function gestionProtocolo(){
	operacionesProtocolos();
	
	abreVentanaGestionConBotones('Gestión de protocolos','index.php');
	$datos=compruebaDatos('protocolos');
	
	campoTexto('nombre','Nombre',$datos,'span6');	
	campoOculto($datos,'eliminado','NO');

	cierraVentanaGestion('index.php');
}


function imprimeProtocolos($eliminado='NO'){

	$consulta=consultaBD("SELECT * FROM protocolos WHERE eliminado='$eliminado';",true);
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td>".$datos['nombre']."</td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-edit'></i> Editar</i></a>
				</td>
				<td class='centro'>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
	        </tr>";
	}
}

//Fin parte de gestión de protocolos