<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('../../api/phpexcel/PHPExcel.php');
	require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("../documentacion/prl.xlsx");
for($i=2;$i<395;$i++){
	$codigoInterno=$objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue();

	$codigoCliente=$objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue();

	$fecha=fechaBD();

	$visitada=comprobarMarcado($objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue());
	$numVisitas=numeroVisitas($objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue());

	$planPrev=comprobarMarcado($objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue());

	$er=comprobarMarcado($objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue());

	$pap=comprobarMarcado($objPHPExcel->getActiveSheet()->getCell('G'.$i)->getValue());

	$info=comprobarMarcado($objPHPExcel->getActiveSheet()->getCell('H'.$i)->getValue());

	$cert=comprobarMarcado($objPHPExcel->getActiveSheet()->getCell('I'.$i)->getValue());

	$memoria=comprobarMarcado($objPHPExcel->getActiveSheet()->getCell('J'.$i)->getValue());

	$vs=ucwords(strtolower($objPHPExcel->getActiveSheet()->getCell('K'.$i)->getValue()));

	$observaciones=$objPHPExcel->getActiveSheet()->getCell('M'.$i)->getValue();

	$mesVencimiento=$objPHPExcel->getActiveSheet()->getCell('N'.$i)->getValue();
	$meses=array(''=>'','1'=>'Enero','2'=>'Febrero','3'=>'Marzo','4'=>'Abril','5'=>'Mayo','6'=>'Junio','7'=>'Julio','8'=>'Agosto','9'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
	$mesVencimiento=$meses[$mesVencimiento];


	if($codigoCliente!=''){
		echo "INSERT INTO formularioPRL VALUES(NULL,'".$codigoInterno."',".$codigoCliente.",'".$fecha."','".$visitada."','".$fecha."',".$numVisitas.",'".$planPrev."','".$fecha."','".$er."','".$fecha."','".$pap."','".$fecha."','".$info."','".$fecha."','".$cert."','".$fecha."','".$memoria."','".$fecha."','".$vs."','','".$observaciones."','".$mesVencimiento."','');<br/>";
	}
}



function comprobarMarcado($valor){
	$res = 'NO';
	if($valor != '' && $valor != 'N/A'){
		$res = 'SI';
	}

	return $res;
}

function numeroVisitas($valor){
	$valor=str_replace(' ', '', $valor);
	return strlen($valor);
}
?>