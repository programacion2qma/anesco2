<?php
    session_start();
    include_once("funciones.php");
    compruebaSesion();

    $nombreFichero=generaXmlMemoria($_GET['codigo']);

    // Definir headers
    header("Content-Type: text/xml");
    header('Content-Disposition: attachment; filename="'.$nombreFichero.'"');
    header("Content-Transfer-Encoding: binary");

    // Descargar archivo
    readfile('../documentacion/memoria/memoria-serpa.xml');

    cierraBD();
function generaXmlMemoria($codigo){
    conexionBD();
    $datos=consultaBD("SELECT YEAR(f.fecha) AS anio, f.codigoInterno, cl.EMPCIF, o.codigoCliente, f.codigoInterno, o.opcion, f.formacionPresencial, f.formacionDistancia, f.formacionOnline, f.accidentesLeves AS leves, f.accidentesGraves AS graves, f.accidentesMortales AS mortales
        FROM formularioPRL f
        INNER JOIN contratos c ON f.codigoContrato=c.codigo 
        INNER JOIN ofertas o ON c.codigoOferta=o.codigo
        INNER JOIN clientes cl ON o.codigoCliente=cl.codigo
        WHERE f.codigo=".$codigo,false,true);
    $centros=consultaBD("SELECT COUNT(codigo) AS nCentros, SUM(trabajadores) AS nTrabajadores FROM clientes_centros WHERE codigoCliente=".$datos['codigoCliente'],false,true);

    cierraBD();

    $xml=file_get_contents('../documentacion/memoria/plantillaMemoria.xml');
    $xml=str_replace('${anio}',$datos['anio'], $xml);
    $xml=str_replace('${id}',$datos['codigoInterno'], $xml);
    $xml=str_replace('${tipo}',$datos['opcion'], $xml);
    $xml=str_replace('${cif}',$datos['EMPCIF'], $xml);
    $xml=str_replace('${centros}',$centros['nCentros'], $xml);
    $xml=str_replace('${trabajadores}',$centros['nTrabajadores'], $xml);

    $otros=intval($centros['nTrabajadores'])-intval($datos['formacionPresencial'])-intval($datos['formacionDistancia'])-intval($datos['formacionOnline']);
    $xml=str_replace('${presencial}',$datos['formacionPresencial'], $xml);
    $xml=str_replace('${distancia}',$datos['formacionDistancia'], $xml);
    $xml=str_replace('${online}',$datos['formacionOnline'], $xml);
    $xml=str_replace('${otros}',$otros, $xml);

    $incidentes=intval($datos['leves'])+intval($datos['graves'])+intval($datos['mortales']);
    $xml=str_replace('${leves}',$datos['leves'], $xml);
    $xml=str_replace('${graves}',$datos['graves'], $xml);
    $xml=str_replace('${mortales}',$datos['mortales'], $xml);
    $xml=str_replace('${incidentes}',$incidentes, $xml);



    file_put_contents("../documentacion/memoria/memoria-serpa.xml",$xml);

    //unset($grupos);
    //unset($xml);//Borro las variables para liberar memoria.

    return 'memoria-serpa_'.$datos['EMPCIF'].'.xml';
}

/*function generaXmlMemoria(){
    $where='WHERE ((enVigor="SI" AND contratos.eliminado="NO") OR (enVigor="NO" AND contratos.fechaInicio>CURDATE()) OR codigoContrato IS NULL)';
    $whereEjercicio=defineWhereEjercicio('',array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaMail'));
    $where.=str_replace('WHERE',' AND (',$whereEjercicio).' OR (visitada="NO" OR planPrev="NO" OR pap="NO" OR info="NO" OR memoria="NO" OR vs="NO")';

    if($_SESSION['ejercicio']!='' && $_SESSION['ejercicio']!='Todos'){
        $where.='AND YEAR(f.fecha)<='.$_SESSION['ejercicio'].')';
    }

    if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FACTURACION'){
        $where.=' AND codigoUsuarioTecnico='.$_SESSION['codigoU'];
    }
    conexionBD();
    $consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, visitada, planPrev, pap, info, f.horasPrevistas, memoria, fechaVisita, fechaPlanPrev, fechaPap, fechaInfo,  
                          fechaMemoria, vs, mesVS, anioVS, codigoUsuarioTecnico, CONCAT(EMPNOMBRE,' / ',EMPMARCA) AS cliente,f.codigoContrato, ofertas.codigoCliente, formulario, opcion, 
                          suspendido, (ofertas.total-ofertas.importe_iva) AS importe, incrementoIPC, incrementoIpcAplicado
                          FROM formularioPRL f 
                          LEFT JOIN contratos ON f.codigoContrato=contratos.codigo 
                          LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo 
                          LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
                          ".$where." AND f.eliminado='NO' AND f.aprobado='SI' AND f.visible='SI';");
    $i=0;
    $nCentros=0;
    $nTrabajadores=0;
    $datos=array(
    'planPrev'=>array('texto'=>'Plan de prevención','total'=>0),
    'pap'=>array('texto'=>'Evaluación de riesgos/Planificación preventiva','total'=>0),
    'info'=>array('texto'=>'Información/Formación de los trabajadores','total'=>0),
    'memoria'=>array('texto'=>'Memoria','total'=>0),
    'vs'=>array('texto'=>'Reconocimientos médicos','total'=>0),
    );
    while($item=mysql_fetch_assoc($consulta)){
        $referencia='';
        if($item['codigoContrato']!=NULL){
            $contrato=consultaBD('SELECT * FROM contratos WHERE codigo="'.$item['codigoContrato'].'";',false,true);
            $cliente=consultaBD('SELECT * FROM clientes WHERE codigo="'.$item['codigoCliente'].'";',false,true);
            $referencia=formateaReferenciaContrato($cliente,$contrato);
        }
        $centros=consultaBD('SELECT trabajadores FROM clientes_centros WHERE codigoCliente='.$item['codigoCliente']);
        while($c=mysql_fetch_assoc($centros)){
            $nCentros++;
            $nTrabajadores=$nTrabajadores+$c['trabajadores'];
        }
        //echo $i.' - '.$referencia.'<br/>';
        $i++;
        foreach ($datos as $key => $value) {
            if($item[$key]=='SI'){
                $datos[$key]['total']++;
            }
        }
    }

    foreach ($datos as $key => $value) {
        //echo $value['texto'].': '.$value['total'].'<br/>';
    }

    cierraBD();

    $xml=file_get_contents('../documentacion/memoria/plantillaMemoria.xml');
    $xml=str_replace('${anio}',$_SESSION['ejercicio'], $xml);
    $xml=str_replace('${centros}',$nCentros, $xml);
    $xml=str_replace('${trabajadores}',$nTrabajadores, $xml);


    file_put_contents("../documentacion/memoria/memoria-serpa.xml",$xml);

    //unset($grupos);
    //unset($xml);//Borro las variables para liberar memoria.

    return 'memoria-serpa.xml';
}*/
?>