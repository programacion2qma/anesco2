<?php
session_start();
include_once('funciones.php');
compruebaSesion();

//Carga de PHP Excel (usa el de la API para ahorrar espacio en el servidor)
require_once('../../api/phpexcel/PHPExcel.php');
require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');
require_once('../../api/phpexcel/PHPExcel/IOFactory.php');

//Carga de la plantilla
$objReader=new PHPExcel_Reader_Excel2007();
$documento=$objReader->load("../documentacion/memoria/plantillaMemoria.xlsx");
conexionBD();
if(isset($_GET['codigo'])){
    $i=3;
    $nombre=generaExcel($documento,$_GET['codigo'],$i);
} else {
    $where='WHERE ((enVigor="SI" AND contratos.eliminado="NO") OR (enVigor="NO" AND contratos.fechaInicio>CURDATE()) OR codigoContrato IS NULL)';
    $whereEjercicio=defineWhereEjercicio('',array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaMail'));
    $where.=str_replace('WHERE',' AND (',$whereEjercicio).' OR (visitada="NO" OR planPrev="NO" OR pap="NO" OR info="NO" OR memoria="NO" OR vs="NO")';

    if($_SESSION['ejercicio']!='' && $_SESSION['ejercicio']!='Todos'){
        $where.='AND YEAR(f.fecha)<='.$_SESSION['ejercicio'].')';
    }

    if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FACTURACION'){
        $where.=' AND codigoUsuarioTecnico='.$_SESSION['codigoU'];
    }
    $consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, visitada, planPrev, pap, info, f.horasPrevistas, memoria, fechaVisita, fechaPlanPrev, fechaPap, fechaInfo,  
                          fechaMemoria, vs, mesVS, anioVS, codigoUsuarioTecnico, CONCAT(EMPNOMBRE,' / ',EMPMARCA) AS cliente,f.codigoContrato, ofertas.codigoCliente, formulario, opcion, 
                          suspendido, (ofertas.total-ofertas.importe_iva) AS importe, incrementoIPC, incrementoIpcAplicado
                          FROM formularioPRL f 
                          LEFT JOIN contratos ON f.codigoContrato=contratos.codigo 
                          LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo 
                          LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
                          ".$where." AND f.eliminado='NO' AND f.aprobado='SI' AND f.visible='SI';");
    $i=3;
    while($item=mysql_fetch_assoc($consulta)){
        $nombre=generaExcel($documento,$item['codigo'],$i);
        $i++;
    }
    $nombre='';
}
cierraBD();
$objWriter=new PHPExcel_Writer_Excel2007($documento);
$objWriter->save('../documentacion/memoria/memoria_serpa.xlsx');

$nombre='memoria_serpa'.$nombre.'.xlsx';

// Definir headers
header("Content-Type: application/ms-xlsx");
header("Content-Disposition: attachment; filename=".$nombre);
header("Content-Transfer-Encoding: binary");

// Descargar archivo
readfile('../documentacion/memoria/memoria_serpa.xlsx');


function generaExcel($documento,$codigo,$i){
    $documento->setActiveSheetIndex(0);//Selección hoja
    $datos=consultaBD("SELECT YEAR(f.fecha) AS anio, f.codigoInterno, cl.EMPCIF, o.codigoCliente, f.codigoInterno, o.opcion, f.formacionPresencial, f.formacionDistancia, f.formacionOnline, f.accidentesLeves AS leves, f.accidentesGraves AS graves, f.accidentesMortales AS mortales, cl.EMPNOMBRE, cl.EMPID, f.planPrev, f.pap, f.info
        FROM formularioPRL f
        INNER JOIN contratos c ON f.codigoContrato=c.codigo 
        INNER JOIN ofertas o ON c.codigoOferta=o.codigo
        INNER JOIN clientes cl ON o.codigoCliente=cl.codigo
        WHERE f.codigo=".$codigo,false,true);
    $centros=consultaBD("SELECT COUNT(codigo) AS nCentros, SUM(trabajadores) AS nTrabajadores FROM clientes_centros WHERE codigoCliente=".$datos['codigoCliente'],false,true);

    
    $documento->getActiveSheet()->SetCellValue('A'.$i,$datos['EMPCIF']);
    $documento->getActiveSheet()->SetCellValue('B'.$i,$datos['EMPID']);
    $documento->getActiveSheet()->SetCellValue('C'.$i,$datos['EMPNOMBRE']);

    $documento->getActiveSheet()->SetCellValue('AF'.$i,$datos['leves']);
    $documento->getActiveSheet()->SetCellValue('AG'.$i,$datos['graves']);
    $documento->getActiveSheet()->SetCellValue('AH'.$i,$datos['mortales']);

    $campos=array('planPrev'=>'G','pap'=>'E','info'=>'I','info'=>'J');
    foreach ($campos as $key => $c) {
        if($datos[$key]=='SI'){
            $documento->getActiveSheet()->SetCellValue($c.$i,'1');
        }
    }
    	
    $columnas=array('A','B','C');
    for($i=0;$i<count($columnas);$i++){
        $documento->getActiveSheet()->getColumnDimension($columnas[$i])->setAutoSize(true);//Ajusta el ancho de la columna al contenido
    }

    

    return '_'.$datos['EMPCIF'];
}