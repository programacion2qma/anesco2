<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión de Formularios PRL


function operacionesFormularioPRL(){
	$res=false;
		
	if(isset($_POST['codigo'])){
		$res=actualizaPlanificacion();
	}
	elseif(isset($_POST['codigoContrato'])){
		$res=insertaPlanificacion();
	}
	elseif(isset($_POST['codigoPRL'])){
		$res=insertaFormulario();
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoPLanificacion($_POST['elimina']);
		//$res=eliminaPLanificacion();
	}
	
	mensajeResultadoAdicional('fecha',$res,'Formulario');
	mensajeResultadoAdicional('codigoPRL',$res,'Formulario');
    mensajeResultado('elimina',$res,'Formulario', true);
}

function insertaPlanificacion(){
	definirPOST();
	$res=insertaDatos('formularioPRL');
	
	if($res){
		$codigo=$res;
		$res=insertaVisitas($codigo);
		//$res=insertaDocumentacion($codigo,'documentos_plan_prev','plan');
		//$res=$res && insertaDocumentacion($codigo,'documentos_info','info');
	}

	return $res;
}

function actualizaPlanificacion(){
	definirPOST();
	$res=actualizaDatos('formularioPRL');

	if($res){
		//eliminarFicheros('documentos_plan_prev','plan');
		//eliminarFicheros('documentos_info','info');

		$codigo=$_POST['codigo'];
		$res=compruebaFormularioRenovado($codigo);
		$res=insertaVisitas($codigo);
		//$res=insertaDocumentacion($codigo,'documentos_plan_prev','plan');
		//$res=$res && insertaDocumentacion($codigo,'documentos_info','info');
	}

	return $res;
}


function insertaFormulario(){
	$res=true;
	$datos=arrayFormulario();
	
	$formulario=obtienePreguntasDatosGenerales($datos);

	conexionBD();
	
	foreach($datos['codigosCentros'] as $codigoCentro){

		$formulario.=obtienePreguntasCentro($datos,$codigoCentro);

		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'extintores_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','numero','kg','tipo','eficacia','senializado','revisado','ubicacionExtintor'),'numero',$codigoCentro);
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'bie_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','numeroBIE','medidaBIE','presion','senializado','revisado','ubicacion'),'numeroBIE',$codigoCentro);
		
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'botiquines_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','ubicacionBotiquines','senializadoBotiquines','numeroBotiquines','revisadoBotiquines'),'ubicacionBotiquines',$codigoCentro);
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'locales_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','ubicacionLocales','senializadoLocales','numeroLocales','revisadoLocales'),'ubicacionLocales',$codigoCentro);
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'medios_humanos_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','ubicacionMedios','numeroMedios','revisadoMedios','senializadoMedios'),'ubicacionMedios',$codigoCentro);
		
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'materias_primas_formulario_prl',$datos['codigoPRL'],array('materiaPrima','lugar','cantidad','codigoFormularioPRL'),'materiaPrima',$codigoCentro);
		
		//$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'puestos_trabajo_formulario_prl',$datos['codigoPRL'],array('codigoPuestoTrabajoSI','codigoFormularioPRL','sensibleSI'),'codigoPuestoTrabajoSI',$codigoCentro);
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'empleados_sensibles_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','codigoEmpleado'),'codigoEmpleado',$codigoCentro);

		/*$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'puestos_trabajo_formulario_prl',$datos['codigoPRL'],array('codigoPuestoTrabajoNO','codigoFormularioPRL','sensibleNO','seccionPuestoTrabajoNO'),'codigoPuestoTrabajoNO',$codigoCentro);*/
		$res=$res && insertaDatosTablaAsociadaFormularioPRL($datos,'instalaciones_formulario_prl',$datos['codigoPRL'],array('codigoFormularioPRL','codigoInstalacion'),'codigoInstalacion',$codigoCentro);

		$res=$res && insertaFuncionesResponsabilidades($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaMaquinaria($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaHigienicos($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaIluminacion($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaGas($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaEvacuacion($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaRecursosHumanos($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaRecursosTecnicos($datos,$datos['codigoPRL'],$codigoCentro);
		$res=$res && insertaRecursosEconomicos($datos,$datos['codigoPRL'],$codigoCentro);
	}

	$formulario=quitaUltimaComa($formulario,4);//Para quitar el último separador

	$res = consultaBD("UPDATE formularioPRL SET formulario = '".$formulario."' WHERE codigo=".$datos['codigoPRL']);
	$fichero='';
	if(isset($_FILES['ficheroOrganigrama']) && $_FILES['ficheroOrganigrama']['name']==''){
		$fichero='NO';
	} elseif(isset($_FILES['ficheroOrganigrama']) && $_FILES['ficheroOrganigrama']['name']!=''){
		$fichero=subeDocumento('ficheroOrganigrama','org_'.time(),'ficheros');
	}
	if($fichero!=''){
		$res = consultaBD("UPDATE formularioPRL SET ficheroOrganigrama = '".$fichero."' WHERE codigo=".$datos['codigoPRL']);
	}
	$fichero='';
	if(isset($_FILES['ficheroEstructura']) && $_FILES['ficheroEstructura']['name']==''){
		$fichero='NO';
	} elseif(isset($_FILES['ficheroEstructura']) && $_FILES['ficheroEstructura']['name']!=''){
		$fichero=subeDocumento('ficheroEstructura','est_'.time(),'ficheros');
	}
	if($fichero!=''){
		$res = consultaBD("UPDATE formularioPRL SET ficheroEstructura = '".$fichero."' WHERE codigo=".$datos['codigoPRL']);
	}

	cierraBD();

	$res=$res && compruebaCambioDatosContrato();
	
	return $res;
}

function obtienePreguntasDatosGenerales($datos){
	$res='';
	
	for($i=0;$i<22;$i++){

		$res.="pregunta$i=>".$datos['pregunta'.$i]."&{}&";
	}

	return $res;
}


function obtienePreguntasCentro($datos,$codigoCentro){
	$res='';
	
	for($i=17;$i<=70;$i++){

		if($i!=55){
			$res.="pregunta$codigoCentro$i=>".$datos['pregunta'.$codigoCentro.$i];
		}
		else{//El índice 55 corresponde al fichero con los planos
			$res.=subeFicheroPlano('pregunta'.$codigoCentro.$i);
		}

		$res.="&{}&";
	}

	return $res;
}


function cambiaEstadoEliminadoPLanificacion($estado){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],'formularioPRL',false);
	}
	cierraBD();

	return $res;
}

function subeFicheroPlano($indice){
	$res="$indice=>NO";

	if(isset($_FILES[$indice])){
		$fichero=subeDocumento($indice,time(),'../documentos/planificacion');
		$res="$indice=>$fichero";
	}

	return $res;
}

function insertaDatosTablaAsociadaFormularioPRL($datos,$tabla,$codigoPRL,$campos,$campoCondicion,$codigoCentro,$borrado=true){
	$res=true;

	if($borrado){
		$res=consultaBD("DELETE FROM $tabla WHERE codigoFormularioPRL='$codigoPRL' AND codigoCentroTrabajo='$codigoCentro';");
	}
	

	for($i=0;isset($datos[$campoCondicion.$codigoCentro.'_'.$i]);$i++){
		$valores='';

		for($j=0;$j<count($campos);$j++){
			if($campos[$j]=='codigoFormularioPRL'){
				$valores.="'".$codigoPRL."', ";
			}
			elseif($campos[$j]=='sensibleSI'){
				$valores.="'SI', ";
			}
			elseif($campos[$j]=='sensibleNO'){
				$valores.="'NO', ";
			}
			elseif(substr_count($campos[$j],'codigo')>0){
				$valores.=$datos[$campos[$j].$codigoCentro.'_'.$i].", ";//Si es un campo código, no pone las comillas!! ('NULL' da error)
			}
			else{
				$valores.="'".$datos[$campos[$j].$codigoCentro.'_'.$i]."', ";
			}
		}

		$valores.="'".$codigoCentro."'";

		$res=$res && consultaBD("INSERT INTO $tabla VALUES(NULL,$valores);");
	}

	return $res;
}

function insertaFuncionesResponsabilidades($datos,$codigoPRL,$codigoCentro){
	$res=true;

	$codigos='(0';
	for($i=0;isset($datos['codigoEmpleadoFunciones'.$codigoCentro.'_'.$i]);$i++){
		if(isset($datos['existeEmpleadoFunciones'.$codigoCentro.'_'.$i])){
			$res=consultaBD('UPDATE funciones_formulario_prl SET funciones="'.$datos['funcionesFunciones'.$codigoCentro.'_'.$i].'",codigoEmpleado='.$datos['codigoEmpleadoFunciones'.$codigoCentro.'_'.$i].', area="'.$datos['areaFunciones'.$codigoCentro.'_'.$i].'", productos="'.$datos['productosFunciones'.$codigoCentro.'_'.$i].'", maquinaria="'.$datos['maquinariaFunciones'.$codigoCentro.'_'.$i].'", herramientas="'.$datos['herramientasFunciones'.$codigoCentro.'_'.$i].'", medio="'.$datos['medioFunciones'.$codigoCentro.'_'.$i].'" WHERE codigo='.$datos['existeEmpleadoFunciones'.$codigoCentro.'_'.$i]);
			$codigos.=','.$datos['existeEmpleadoFunciones'.$codigoCentro.'_'.$i];
		} else {
			$res=consultaBD('INSERT INTO funciones_formulario_prl VALUES(NULL,'.$codigoPRL.',"'.$datos['funcionesFunciones'.$codigoCentro.'_'.$i].'",'.$datos['codigoEmpleadoFunciones'.$codigoCentro.'_'.$i].',"'.$datos['areaFunciones'.$codigoCentro.'_'.$i].'",'.$codigoCentro.',"'.$datos['productosFunciones'.$codigoCentro.'_'.$i].'","'.$datos['maquinariaFunciones'.$codigoCentro.'_'.$i].'","'.$datos['herramientasFunciones'.$codigoCentro.'_'.$i].'","'.$datos['medioFunciones'.$codigoCentro.'_'.$i].'")');
			$codigos.=','.mysql_insert_id();
		}
	}
	$codigos.=')';
	$res=consultaBD('DELETE FROM funciones_formulario_prl WHERE codigo NOT IN '.$codigos.' AND codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	return $res;
}

function insertaMaquinaria($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM maquinaria_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$tipos=array('FIJA','MANUAL');
	foreach ($tipos as $tipo) {
		$i=0;
		while(isset($datos['maquinaria'.$tipo.$codigoCentro.'_'.$i])){
			if($datos['maquinaria'.$tipo.$codigoCentro.'_'.$i]!=''){
				$res=consultaBD('INSERT INTO maquinaria_formulario_prl VALUES(NULL,'.$codigoPRL.',"'.$datos['maquinaria'.$tipo.$codigoCentro.'_'.$i].'","'.$tipo.'",'.$codigoCentro.')');
			}
			$i++;
		}
		
	}

	return $res;
}

function insertaHigienicos($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM higienicos_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$i=0;
	while(isset($datos['nombreHigienico'.$codigoCentro.'_'.$i])){
		if($datos['nombreHigienico'.$codigoCentro.'_'.$i]!=''){
			$res=consultaBD('INSERT INTO higienicos_formulario_prl VALUES(NULL,'.$codigoPRL.','.$codigoCentro.',"'.$datos['nombreHigienico'.$codigoCentro.'_'.$i].'","'.$datos['descripcionHigienico'.$codigoCentro.'_'.$i].'")');
		}
		$i++;
	}
	return $res;
}

function insertaIluminacion($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM iluminacion_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$i=0;
	while(isset($datos['nombreIluminacion'.$codigoCentro.'_'.$i])){
		if($datos['nombreIluminacion'.$codigoCentro.'_'.$i]!=''){
			$res=consultaBD('INSERT INTO iluminacion_formulario_prl VALUES(NULL,'.$codigoPRL.','.$codigoCentro.',"'.$datos['nombreIluminacion'.$codigoCentro.'_'.$i].'","'.$datos['descripcionIluminacion'.$codigoCentro.'_'.$i].'")');
		}
		$i++;
	}
	return $res;
}

function insertaGas($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM gas_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$i=0;
	while(isset($datos['zonaGas'.$codigoCentro.'_'.$i])){
		if($datos['zonaGas'.$codigoCentro.'_'.$i]!=''){
			$res=consultaBD('INSERT INTO gas_formulario_prl VALUES(NULL,'.$codigoPRL.','.$codigoCentro.',"'.$datos['zonaGas'.$codigoCentro.'_'.$i].'","'.$datos['sueloGas'.$codigoCentro.'_'.$i].'","'.$datos['m2Gas'.$codigoCentro.'_'.$i].'","'.$datos['m3Gas'.$codigoCentro.'_'.$i].'")');
		}
		$i++;
	}
	return $res;
}

function insertaEvacuacion($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM evacuacion_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$i=0;
	while(isset($datos['tipoEvacuacion'.$codigoCentro.'_'.$i])){
		if($datos['tipoEvacuacion'.$codigoCentro.'_'.$i]!='0'){
			$res=consultaBD('INSERT INTO evacuacion_formulario_prl VALUES(NULL,'.$codigoPRL.','.$codigoCentro.',"'.$datos['tipoEvacuacion'.$codigoCentro.'_'.$i].'","'.$datos['zonaEvacuacion'.$codigoCentro.'_'.$i].'","'.$datos['descripcionEvacuacion'.$codigoCentro.'_'.$i].'")');
		}
		$i++;
	}
	return $res;
}

function insertaRecursosHumanos($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM recursos_humanos_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$i=0;
	while(isset($datos['recursoRecursosH'.$codigoCentro.'_'.$i])){
		if($datos['recursoRecursosH'.$codigoCentro.'_'.$i]!='0'){
			$res=consultaBD('INSERT INTO recursos_humanos_formulario_prl VALUES(NULL,'.$codigoPRL.','.$codigoCentro.',"'.$datos['recursoRecursosH'.$codigoCentro.'_'.$i].'","'.$datos['identificacionRecursosH'.$codigoCentro.'_'.$i].'","'.$datos['tipoRecursosH'.$codigoCentro.'_'.$i].'","'.$datos['puestoRecursosH'.$codigoCentro.'_'.$i].'")');
		}
		$i++;
	}
	return $res;
}

function insertaRecursosTecnicos($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM recursos_tecnicos_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$i=0;
	while(isset($datos['recursoRecursosT'.$codigoCentro.'_'.$i])){
		if($datos['recursoRecursosT'.$codigoCentro.'_'.$i]!='0'){
			$res=consultaBD('INSERT INTO recursos_tecnicos_formulario_prl VALUES(NULL,'.$codigoPRL.','.$codigoCentro.',"'.$datos['recursoRecursosT'.$codigoCentro.'_'.$i].'","'.$datos['tipoRecursosT'.$codigoCentro.'_'.$i].'")');
		}
		$i++;
	}
	return $res;
}

function insertaRecursosEconomicos($datos,$codigoPRL,$codigoCentro){
	$res=true;
	$res=consultaBD('DELETE FROM recursos_economicos_formulario_prl WHERE codigoFormularioPRL='.$codigoPRL.' AND codigoCentroTrabajo='.$codigoCentro);
	$i=0;
	while(isset($datos['recursoRecursosE'.$codigoCentro.'_'.$i])){
		if($datos['recursoRecursosE'.$codigoCentro.'_'.$i]!='0'){
			$$datos['importeRecursosE'.$codigoCentro.'_'.$i]=str_replace(',','.', $datos['importeRecursosE'.$codigoCentro.'_'.$i]);
			$res=consultaBD('INSERT INTO recursos_economicos_formulario_prl VALUES(NULL,'.$codigoPRL.','.$codigoCentro.',"'.$datos['recursoRecursosE'.$codigoCentro.'_'.$i].'","'.$datos['tipoRecursosE'.$codigoCentro.'_'.$i].'","'.$datos['descripcionRecursosE'.$codigoCentro.'_'.$i].'","'.$datos['importeRecursosE'.$codigoCentro.'_'.$i].'")');
		}
		$i++;
	}
	return $res;
}


/*function insertaEmpleadosPRL($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM empleados_prl WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['nombreEmpleadoPRL'.$i])){
		if($datos['nombreEmpleadoPRL'.$i] != ''){
		$res = $res && consultaBD("INSERT INTO empleados_prl VALUES (NULL,'".$codigo."','".$datos['nombreEmpleadoPRL'.$i]."',
			'".$datos['dniEmpleadoPRL'.$i]."','".$datos['puestoEmpleadoPRL'.$i]."','".$datos['funcionesEmpleadoPRL'.$i]."','".$datos['salenEmpleadoPRL'.$i]."',
			'".$datos['vehiculoEmpleadoPRL'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function insertaCentrosPRL($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM centros_prl WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['nombreCentroPRL'.$i])){
		if($datos['nombreCentroPRL'.$i] != ''){
		$res = $res && consultaBD("INSERT INTO centros_prl VALUES (NULL,'".$codigo."','".$datos['nombreCentroPRL'.$i]."',
			'".$datos['delegadoCentroPRL'.$i]."','".$datos['comiteCentroPRL'.$i]."','".$datos['trabajadorCentroPRL'.$i]."','".$datos['consultaCentroPRL'.$i]."');");
		}

		$i++;
	}

	return $res;
}*/

function eliminarFicheros($tabla,$prefijo){
	$res = true;

	for($i=0;$i<$_POST['ficherosTotales_'.$prefijo];$i++){
		if(isset($_POST['eliminaFichero_'.$prefijo.$i])){
			$codigo=$_POST['eliminaFichero_'.$prefijo.$i];
			$fichero=datosRegistro($tabla,$codigo);
			$res=consultaBD('DELETE FROM '.$tabla.' WHERE codigo='.$codigo,true);
			unlink('ficheros/'.$fichero['fichero']);
		}
	}

	return $res;
}

function insertaDocumentacion($codigo,$tabla,$prefijo){
	$res=true;//Siempre que vayamos a usarlo con un AND, inicializarlo a true

	conexionBD();
	
	$datos=arrayFormulario();
	$i=0;
	while(isset($datos['codigoFichero_'.$prefijo.$i])){
		$res=$res && consultaBD('UPDATE '.$tabla.' SET nombre="'.$datos['nombreSubido_'.$prefijo.$i].'" WHERE codigo='.$datos['codigoFichero_'.$prefijo.$i]);
		$i++;
	}
	$i=0;
	while(isset($_FILES['fichero_'.$prefijo.$i])){
		if($_FILES['fichero_'.$prefijo.$i]['tmp_name'] != ''){
			$fichero=subeDocumento('fichero_'.$prefijo.$i,time(),'../documentos/planificacion');

			$res=$res && consultaBD("INSERT INTO ".$tabla." VALUES(NULL, '$codigo', '".$datos['nombre_'.$prefijo.$i]."', '".$fichero."');");
		}
		$i++;
	}
	
	cierraBD();

	return $res;
}


function gestionFormularioPRL(){
	$mesesNombres=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	operacionesFormularioPRL();
	
	if($_SESSION['tipoUsuario']!='FACTURACION'){
		abreVentanaGestionConBotones('Gestión de planificaciones de PRL','index.php','','icon-edit','',true,'noAjax');
	}
	else{
		abreVentanaGestionConBotones('Gestión de planificaciones de PRL','index.php','','icon-edit','',true,'noAjax','index.php',false);
	}

	$datos=compruebaDatos('formularioPRL');

	abreColumnaCampos();
		//campoFecha('fecha','Fecha',$datos);
		campoOculto(formateaFechaWeb($datos['fecha']),'fecha',fecha());

		if(!$datos){
			$numero = generaNumero();
		} else {
			$numero = $datos['codigoInterno'];
		}
		campoTexto('codigoInterno','Num',$numero,'input-mini pagination-right');
		campoOculto($datos,'formulario');
		campoOculto($datos,'ficheroOrganigrama');
		campoOculto($datos,'ficheroEstructura');
		campoOculto($datos,'eliminado','NO');
		$where=defineWhereEmpleado('WHERE activo="SI" AND enVigor="SI" AND opcion<>"2"');
		campoSelectContrato($datos);
		campoUsuarioTecnicoPlanificacion($datos);
		campoNumero('horasPrevistas','Horas previstas',$datos['horasPrevistas']);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadio('aprobado','Aprobado',$datos);
		campoFechaBlanco('fechaAprobada','Fecha aprobado',$datos);
	cierraColumnaCampos(true);
	
	
	echo "<h3 class='apartadoFormulario'>Visitas</h3>";
	abreColumnaCampos();
		echo '<div class="hide">';
		campoRadio('visitada','Visitada',$datos);
		campoFechaBlanco('fechaVisita','Fecha prevista',$datos);
		campoFechaBlanco('fechaVisitaReal','Fecha real',$datos);
		campoNumero('numVisitas','Número de visitas',$i);
		echo '</div>';
		$i=creaTablaVisitasPlanificacion($datos);
	cierraColumnaCampos();
	abreColumnaCampos();

	campoDato('Número de visitas',$i);

	cierraColumnaCampos(true);

	


	echo "<h3 class='apartadoFormulario'>Plan de prevención</h3>";

	abreColumnaCampos();
		
	campoRadio('planPrev','Plan prevención',$datos);
	cierraColumnaCampos(true);
	echo '<div id="div_planPrev">';
		abreColumnaCampos();
			campoFechaBlanco('fechaPlanPrev','Fecha prevista',$datos);
			echo '<div style="margin-left:220px;">';
			campoCheckIndividual('checkVisiblePlanPrev','Visible',$datos);
			echo '</div>';
		cierraColumnaCampos();
		abreColumnaCampos();
			campoFechaBlanco('fechaPlanPrevReal','Fecha real',$datos);
			campoNumero('horasPlanPrev','Horas',$datos);
		cierraColumnaCampos(true);
	echo '</div>';
	echo "<h3 class='apartadoFormulario'>Evaluación de Riesgos/Planificación Preventiva</h3>";
	abreColumnaCampos();
		campoRadio('pap','Evaluación de riesgos \\ Planificación preventiva',$datos);
	cierraColumnaCampos(true);
	echo '<div id="div_pap">';
		abreColumnaCampos();
			campoFechaBlanco('fechaPap','Fecha prevista',$datos);
		cierraColumnaCampos();
		abreColumnaCampos();
			campoFechaBlanco('fechaPapReal','Fecha real',$datos);
			campoNumero('horasPap','Horas',$datos);
		cierraColumnaCampos(true);
	echo '</div>';
	echo "<h3 class='apartadoFormulario'>Información/Formación a los Trabajadores</h3>";
	abreColumnaCampos();
		campoRadio('info','Información a los trabajadores \\ Certificados de formación',$datos);
	cierraColumnaCampos(true);
	echo '<div id="div_info">';
		abreColumnaCampos();
			campoFechaBlanco('fechaInfo','Fecha prevista',$datos);
		cierraColumnaCampos();
		abreColumnaCampos();
			campoFechaBlanco('fechaInfoReal','Fecha real',$datos);
			campoNumero('formacionPresencial','Nº trab. formación presencial',$datos);
			campoNumero('formacionDistancia','Nº trab. formación a distancia',$datos);
			campoNumero('formacionOnline','Nº trab. formación online',$datos);
		cierraColumnaCampos(true);
	echo '</div>';
	echo "<h3 class='apartadoFormulario'>Accidentes de trabajo</h3>";
	abreColumnaCampos();
		campoRadio('accidentes','Accidentes de trabajo',$datos);
	cierraColumnaCampos(true);
	echo '<div id="div_accidentes">';
		abreColumnaCampos();
			campoFechaBlanco('fechaAccidentes','Fecha prevista',$datos);
		cierraColumnaCampos();
		abreColumnaCampos();
			campoFechaBlanco('fechaAccidentesReal','Fecha real',$datos);
			campoNumero('accidentesLeves','Leves',$datos);
			campoNumero('accidentesGraves','Graves',$datos);
			campoNumero('accidentesMortales','Mortales',$datos);
		cierraColumnaCampos(true);


	/*abreColumnaCampos('sinFlotar');
		echo "<h3 class='apartadoFormulario'>Documentos</h3>";
		tablaFicheros($datos,2);
	cierraColumnaCampos(true);*/
	echo '</div>';
	echo "<h3 class='apartadoFormulario'>Memoria</h3>";
	abreColumnaCampos();
		campoRadio('memoria','Memoria',$datos);
	cierraColumnaCampos(true);
	echo '<div id="div_memoria">';
		abreColumnaCampos();
			campoFechaBlanco('fechaMemoria','Fecha prevista',$datos);
		cierraColumnaCampos();
		abreColumnaCampos();	
			campoFechaBlanco('fechaMemoriaReal','Fecha real',$datos);
		cierraColumnaCampos(true);
	echo '</div>';
	echo "<h3 class='apartadoFormulario'>Reconocimiento médicos</h3>";
	abreColumnaCampos();
		campoRadio('vs','F. prevista para  Rec. Médicos',$datos);
	cierraColumnaCampos(true);
	echo '<div id="div_vs">';
		abreColumnaCampos();
			campoMesPlanificacion('mesVS',$datos);
			campoAnioPlanificacion('anioVS',$datos);
			campoFechaBlanco('fechaVsReal','Fecha real',$datos);
			$tiempo='00:00';
			$tareas=consultaBD('SELECT tiempo FROM tareas_parte_diario WHERE codigoContrato='.$datos['codigoContrato'],true);
			while($tarea=mysql_fetch_assoc($tareas)){
				$tiempo=sumaTiempo($tiempo,$tarea['tiempo']);
			}
			campoTexto('totalHoras','Total horas dedicadas',$tiempo,'input-small',true);
		cierraColumnaCampos();
		abreColumnaCampos();
			//campoRadio('corrientePago','Esta al corriente de pago',$datos);
			campoTexto('mail','Mail informativo, formacion y vigilancia de la salud',$datos);
			campoFechaBlanco('fechaMail','Fecha email',$datos);
			areaTexto('observaciones','Observaciones',$datos,'areaInforme');
			campoSelect('mesVencimiento','Mes vencimiento',$mesesNombres,$mesesNombres,$datos,'selectpicker span2 show-tick');
			cierraColumnaCampos(true);
	echo '</div>';


	/*abreColumnaCampos('sinFlotar');
		echo "<h3 class='apartadoFormulario'>Documentación</h3>";
		tablaFicheros($datos);
	cierraColumnaCampos(true);*/

	campoOculto($datos,'ficheroOrganigrama','NO');
	campoOculto($datos,'ficheroEstructura','NO');

	if($_SESSION['tipoUsuario']!='FACTURACION'){
		cierraVentanaGestion('index.php',true);
	}
	else{
		cierraVentanaGestion('index.php',true,false);
	}
	
}


function campoHorasReales($nombreCampo,$valor=false){
	/*$datos=array();
	if(!$valor || ($valor && $valor[$nombreCampo]=='')){
		$datos[0]='';
		$datos[1]='';
	} 
	else {
		$datos=explode(':',$valor[$nombreCampo]);	
		if(!isset($datos[1])){
			$datos[1]='';		
		}
	}
	campoNumero($nombreCampo,'Horas dedicadas',formateaHoraWeb($datos[$nombreCampo]),'input-mini pagination-right horas',0);
	campoNumero($nombreCampo.'_minutos','Minutos',$datos[1],'input-mini pagination-right minutos',0);*/

	campoNumero($nombreCampo,'Horas dedicadas',$valor);
    
}

function definirPOST(){
	/*$campos=array('horasVisita','horasPlanPrev','horasPap','horasInfo','horasMemoria','horasVs');
	foreach ($campos as $campo) {
		$_POST[$campo]=$_POST[$campo.'_horas'].':'.$_POST[$campo.'_minutos'];
	}*/
}

function completarDatosGeneralesPRL($formulario,$cliente){
	$cliente = datosRegistro('clientes',$cliente);

	$formulario['pregunta3'] = $formulario['pregunta3'] == '' ? $cliente['EMPNOMBRE'] : $formulario['pregunta3']; 
	$formulario['pregunta4'] = $formulario['pregunta4'] == '' ? $cliente['EMPDIR'] : $formulario['pregunta4']; 
	$formulario['pregunta5'] = $formulario['pregunta5'] == '' ? $cliente['EMPLOC'] : $formulario['pregunta5']; 
	$formulario['pregunta6'] = $formulario['pregunta6'] == '' ? $cliente['EMPTEL1'] : $formulario['pregunta6']; 
	$formulario['pregunta12'] = $formulario['pregunta12'] == '' ? $cliente['EMPEMAIL'] : $formulario['pregunta12']; 
	$formulario['pregunta9'] = $formulario['pregunta9'] == '' ? $cliente['EMPCIF'] : $formulario['pregunta9']; 
	$formulario['pregunta10'] = $formulario['pregunta10'] == '' ? $cliente['EMPCP'] : $formulario['pregunta10']; 
	$formulario['pregunta11'] = $formulario['pregunta11'] == '' ? $cliente['EMPPROV'] : $formulario['pregunta11']; 
	$formulario['pregunta13'] = $formulario['pregunta13'] == '' ? $cliente['EMPACTIVIDAD'] : $formulario['pregunta13']; 

	return $formulario;
}


function campoSelectContrato($datos){
	$contratos=consultaBD('SELECT * FROM contratos',true);	
	$nombres=array();
	$valores=array();
	while($contrato=mysql_fetch_assoc($contratos)){
		$cliente=consultaBD('SELECT clientes.* FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente WHERE ofertas.codigo='.$contrato['codigoOferta'],true,true);
		$referencia=formateaReferenciaContrato($cliente,$contrato);
		array_push($nombres, 'Nº: '.$referencia.' - '.$cliente['EMPMARCA']);
		array_push($valores, $contrato['codigo']);
	}
	campoSelect('codigoContrato','Contrato',$nombres,$valores,$datos);
}

//Parte de Toma de Datos

function tomaDatos(){
	abreVentanaGestionConBotones('Toma de datos PRL','index.php','','icon-edit','',true,'noAjax');
	
	$codigosCentros=creaPestaniasFormularioPRL();
	$datos=compruebaDatosTomaDatos($codigosCentros);

	abrePestaniaAPI(1,true);
	camposDatosGenerales($datos);
	campoFichero('ficheroOrganigrama','Organigrama',0,$datos,'ficheros/','Descargar');
	campoFichero('ficheroEstructura','Estructura preventiva',0,$datos,'ficheros/','Descargar');
	abreColumnaCampos();
		areaTextoFormulario(20,'Descripción de la empresa',$datos,'areaInforme areaDescripcion'); 
	cierraColumnaCampos();
	cierraPestaniaAPI();

	$i=2;
	foreach ($codigosCentros as $codigoCentro) {
		abrePestaniaAPI($i);
		
		campoOculto($codigoCentro,'codigosCentros[]');

		camposDescripcionCentro($datos,$codigoCentro);
		camposFuncionesResponsabilidades($datos,$codigoCentro);
		camposTrabajadoresSensibles($datos,$codigoCentro);
		camposMaquinarias($datos,$codigoCentro);
		camposAgentes($datos,$codigoCentro);
		camposEmergencia($datos,$codigoCentro);
		camposMateriasPrimas($datos,$codigoCentro);
		camposCentro($datos,$codigoCentro);
		camposHigienicos($datos,$codigoCentro);
		camposIluminacion($datos,$codigoCentro);
		camposInstalaciones($datos,$codigoCentro);
		camposAlarmas($datos,$codigoCentro);
		camposIncendio($datos,$codigoCentro);
		camposEvacuacion($datos,$codigoCentro);
		camposPrimerosAuxilios($datos,$codigoCentro);
		camposVestuarios($datos,$codigoCentro);
		camposRecursos($datos,$codigoCentro);
		//camposPuestosTrabajos($datos,$codigoCentro);
		camposObservaciones($datos,$codigoCentro);
		campoFirma($datos,'pregunta'.$codigoCentro.'54','Firma del acompañante');

		cierraPestaniaAPI();

		$i++;
	}

	cierraPestaniasAPI();

	cierraVentanaGestion('index.php',true);
}

function compruebaDatosTomaDatos($codigosCentros){
	$codigoPRL=$_GET['codigoPRL'];

	campoOculto($codigoPRL,'codigoPRL');

	$datos=datosRegistro('formularioPRL',$codigoPRL);

	$formulario=explode('&{}&',$datos['formulario']);

    if(count($formulario)>1){

        foreach($formulario as $pregunta){
            $partes=explode('=>',$pregunta);

            $datos[$partes[0]]=$partes[1];
        }
    } 
    else {

    	$cliente=consultaBD("SELECT EMPNOMBRE, ofertas.numEmpleados, EMPCIF, EMPDIR, EMPCP, EMPTELPRINC, EMPACTIVIDAD, EMPCNAE, EMPMUTUA
    						 FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='".$datos['codigoContrato']."';",true,true);


    	$datos['pregunta0']=$cliente['EMPNOMBRE'];
    	$datos['pregunta1']=$cliente['EMPCIF'];
    	$datos['pregunta2']=$cliente['EMPDIR'];
    	$datos['pregunta3']=$cliente['EMPCP'];
    	$datos['pregunta4']=$cliente['EMPACTIVIDAD'];
    	$datos['pregunta5']=$cliente['EMPCNAE'];
    	//$datos['pregunta6']=formateaCentrosTrabajoTomaDatos($cliente);
    	$datos['pregunta6']='';
    	$datos['pregunta7']=$cliente['EMPTELPRINC'];
    	$datos['pregunta8']='';
    	$datos['pregunta9']='';
    	$datos['pregunta10']='';
    	$datos['pregunta11']='';
    	$datos['pregunta12']=$cliente['numEmpleados'];
    	$datos['pregunta13']='';
    	$datos['pregunta14']='';
    	$datos['pregunta15']=$cliente['EMPMUTUA'];
    	$datos['pregunta16']='';

    	foreach ($codigosCentros as $codigoCentro){
    		for($i=17;$i<=55;$i++){//El índice 54 es la firma del técnico y el 55 es el fichero con los planos
				$datos['pregunta'.$codigoCentro.$i]='';
	        }
    	}

    }

    return $datos;
}

function obtenerCentrosTrabajo($cliente){
	$cliente=consultaBD('SELECT clientes.codigo FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='.$cliente,true,true);
	$res=array();
	$res['valores']=array();
	$res['nombres']=array();
	$centros=consultaBD('SELECT * FROM clientes_centros WHERE codigoCliente="'.$cliente['codigo'].'";',true);
	while($centro=mysql_fetch_assoc($centros)){
		array_push($res['valores'], $centro['codigo']);
		array_push($res['nombres'], $centro['direccion'].', '.$centro['localidad'].' - CP: '.$centro['cp'].' ('.$centro['provincia'].')');
	}
	return $res;
}

function camposDescripcionCentro($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Descripción del centro</h3>";
	areaTextoFormulario($codigoCentro.'56','Descripción del centro',$datos,'areaInforme areaDescripcion'); 
	areaTextoFormulario($codigoCentro.'57','Productos o sustancias',$datos,'areaInforme areaDescripcion');
}

function camposDatosGenerales($datos){
	$centros=array();
	$centros=obtenerCentrosTrabajo($datos['codigoContrato']);
	//echo "<h3 class='apartadoFormulario'>Datos Generales</h3>";
	abreColumnaCampos('span3 camposGeneralesTomaDatos');

	campoOculto($datos['pregunta6'],'pregunta6Anterior','NO');

	campoRadioFormulario(6,'Cambio datos del contrato',$datos);
	campoTextoFormulario(0,'Razón social',$datos,'input-large');
	campoTextoFormulario(1,'CIF',$datos,'input-small');
	campoTextoFormulario(2,'Domicilio',$datos);
	campoTextoFormulario(3,'Código Postal',$datos,'input-mini pagination-right');
	campoSelectConsultaFormulario(4,'Actividad','SELECT codigo,nombre AS texto FROM actividades ORDER BY nombre',$datos);
	campoCNAEFormulario(5,'CNAE',$datos);
	
	//campoTextoFormulario(6,'Centro/s de trabajo',$datos);
	//campoSelectFormulario(6,'Centro/s de trabajo',$datos,0,$centros['nombres'],$centros['valores']);

	campoTextoFormulario(7,'Teléfono',$datos,'input-small pagination-right');
	
	cierraColumnaCampos();
	abreColumnaCampos();
	
	campoTextoFormulario(8,'Acompañado en la visita por',$datos);
	campoFechaFormulario(9,'Fecha',$datos);
	campoTextoFormulario(10,'Horarios',$datos);
	campoRadioFormulario(11,'Preferencia documentación',$datos,'Formato electrónico',array('Formato electrónico','Formato papel'),array('Formato electrónico','Formato papel'));
	campoTextoFormulario(12,'Nº trabajadores',$datos,'input-mini pagination-right');
	campoTextoFormulario(15,'Mutua',$datos);
	campoRadioFormulario(13,'Trabajadores autónomos',$datos);
	campoRadioFormulario(14,'Realiza obras de construcción',$datos);
	campoRadioFormulario(17,'Tiene órgano de representación',$datos);
	campoTextoFormulario(18,'Interlocutor con el SPA',$datos);
	campoTextoFormulario(19,'DNI',$datos);
	campoFechaFormulario(21,'Desde',$datos);
	campoObservacionesDatosGenerales($datos);

	cierraColumnaCampos(true);
}

function campoObservacionesDatosGenerales($datos){
	echo "<div class='hide' id='cajaObservacionesDatosGenerales'>";
	areaTextoFormulario(16,'Observaciones',$datos);
	echo "</div>";
}


function camposTrabajadoresSensibles($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Trabajadores sensibles</h3>";
	abreColumnaCampos();

	campoRadioFormulario($codigoCentro.'17','Menores de edad',$datos);
	campoRadioFormulario($codigoCentro.'18','Embarazadas',$datos);
	campoRadioFormulario($codigoCentro.'50','Minusvalía - incapacidad',$datos);
	campoTextoFormulario($codigoCentro.'19','Otros (indicar el supuesto)',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	//creaTablaPuestosTomaDatos($datos['codigo'],$datos['codigoContrato'],'SI',$codigoCentro);
	creaTablaTrabajadoresSensibles($datos['codigo'],$datos['codigoContrato'],$codigoCentro);

	cierraColumnaCampos(true);
}


function creaTablaTrabajadoresSensibles($codigoFormularioPRL,$codigoContrato,$codigoCentro){
	    echo "
	    <div class='control-group'>                     
	        <label class='control-label'>Indica el/los trabajadores:</label>
	        <div class='controls'>
	            <table class='tabla-simple mitadAncho' id='tablaEmpleadosSensibles".$codigoCentro."'>
	                <thead>
	                    <tr>
	                        <th> Empleados sensibles </th>
	                        <th> </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($codigoFormularioPRL!=false){
	                $consulta=consultaBD("SELECT codigoEmpleado FROM empleados_sensibles_formulario_prl WHERE codigoFormularioPRL='$codigoFormularioPRL' AND codigoCentroTrabajo='$codigoCentro'");
	                while($empleado=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaTrabajadoresSensibles($i,$codigoContrato,$empleado,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaTrabajadoresSensibles(0,$codigoContrato,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaEmpleadosSensibles".$codigoCentro."\");'><i class='icon-plus'></i> Añadir empleado</button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaEmpleadosSensibles".$codigoCentro."\");'><i class='icon-trash'></i> Eliminar empleado</button>
	        </div>
	    </div>";
}

function imprimeLineaTablaTrabajadoresSensibles($i,$codigoContrato,$datos,$codigoCentro){
	$j=$i+1;

	$queryEmpleados="SELECT empleados.codigo, CONCAT(nombre,' ',apellidos) AS texto 
					 FROM empleados INNER JOIN clientes ON empleados.codigoCliente=clientes.codigo 
					 INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente 
					 INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta 
					 WHERE contratos.codigo='$codigoContrato' AND trabajadorSensible='SI' AND aprobado='SI' AND empleados.eliminado='NO' 
					 ORDER BY nombre, apellidos;";
    echo "<tr>";
        campoSelectConsulta('codigoEmpleado'.$codigoCentro.'_'.$i,'',$queryEmpleados,$datos['codigoEmpleado'],'selectpicker span4 show-tick','data-live-search="true"','',1,false);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function camposMaquinarias($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Equipos de trabajo / Maquinas</h3>";
	abreColumnaCampos();
		creaTablaMaquinaria($datos['codigo'],$datos['codigoContrato'],$codigoCentro,'FIJA');
	cierraColumnaCampos();
	abreColumnaCampos();
		creaTablaMaquinaria($datos['codigo'],$datos['codigoContrato'],$codigoCentro,'MANUAL');
	cierraColumnaCampos(true);

	creaTablaFuncionesResponsabilidades2($datos['codigo'],$datos['codigoContrato'],$codigoCentro);
}

function creaTablaMaquinaria($codigoFormularioPRL,$codigoContrato,$codigoCentro,$tipo){
		$titulo='Maquinaria fija';
		$tabla="tablaMaquinariaFija".$codigoCentro;
		if($tipo=='MANUAL'){
			$titulo='Herramientas y útiles';
			$tabla="tablaMaquinariaManual".$codigoCentro;
		}
	    echo "
	    <div class='control-group'>                     
	        <div class='controls'>
	            <table class='tabla-simple mitadAncho' id='".$tabla."'>
	                <thead>
	                    <tr>
	                        <th> ".$titulo." </th>
	                        <th> </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($codigoFormularioPRL!=false){
	                $consulta=consultaBD("SELECT * FROM maquinaria_formulario_prl WHERE codigoFormularioPRL='$codigoFormularioPRL' AND codigoCentroTrabajo='$codigoCentro' AND tipo='$tipo'");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaMaquinaria($i,$codigoContrato,$item,$codigoCentro,$tipo);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaMaquinaria(0,$codigoContrato,false,$codigoCentro,$tipo);
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"".$tabla."\");'><i class='icon-plus'></i> Añadir </button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"".$tabla."\");'><i class='icon-trash'></i> Eliminar</button>
	        </div>
	    </div>";
}

function imprimeLineaTablaMaquinaria($i,$codigoContrato,$datos,$codigoCentro,$tipo){
	$j=$i+1;

    echo "<tr>";
        campoTextoTabla('maquinaria'.$tipo.$codigoCentro.'_'.$i,$datos['nombre']);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function camposFuncionesResponsabilidades($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Funciones y responsabilidades</h3>";

	creaTablaFuncionesResponsabilidades($datos['codigo'],$datos['codigoContrato'],$codigoCentro);

}


function creaTablaFuncionesResponsabilidades($codigoFormularioPRL,$codigoContrato,$codigoCentro){
	    echo "
	    <div class='control-group'>                     
	        <div class='controls' style='margin-left:10px;'>
	            <table class='tabla-simple' id='tablaFunciones".$codigoCentro."'>
	                <thead>
	                    <tr>
	                    	<th> Empleado </th>
	                    	<th> Puesto de trabajo </th>
	                    	<th> Función </th>
	                        <th> Sección / Area </th>
	                        <th> Productos o sustancias </th>
	                        <th> </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($codigoFormularioPRL!=false){
	                $consulta=consultaBD("SELECT * FROM funciones_formulario_prl WHERE codigoFormularioPRL='$codigoFormularioPRL' AND codigoCentroTrabajo='$codigoCentro'");
	                while($empleado=mysql_fetch_assoc($consulta)){
	                   imprimeLineaTablaFuncionesResponsabilidades($i,$codigoContrato,$empleado,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaFuncionesResponsabilidades(0,$codigoContrato,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFilaFunciones(\"tablaFunciones".$codigoCentro."\");'><i class='icon-plus'></i> Añadir función</button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFilaFunciones(\"tablaFunciones".$codigoCentro."\");'><i class='icon-trash'></i> Eliminar función</button>
	        </div>
	    </div>";
}

function imprimeLineaTablaFuncionesResponsabilidades($i,$codigoContrato,$datos,$codigoCentro){
	$j=$i+1;

	$centro=consultaBD('SELECT codigoCliente FROM clientes_centros WHERE codigo='.$codigoCentro,false,true);
	if($datos!=false){
		campoOculto($datos['codigo'],'existeEmpleadoFunciones'.$codigoCentro.'_'.$i,$valorPorDefecto='',$clase='hide');
	}
    echo "<tr>";
    	campoSelectConsulta('codigoEmpleadoFunciones'.$codigoCentro.'_'.$i,'','SELECT codigo, CONCAT(nombre," ",apellidos) AS texto FROM empleados WHERE codigoCliente='.$centro['codigoCliente'].' ORDER BY nombre,apellidos',$datos['codigoEmpleado'],'selectpicker span4 show-tick selectEmpleadoFunciones',"data-live-search='true'",'',1);
    	if($datos['codigoEmpleado']!=NULL){
    		$puesto=consultaBD('SELECT puestos_trabajo.nombre FROM empleados INNER JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo WHERE empleados.codigo='.$datos['codigoEmpleado'],false,true);
    	} else {
    		$puesto=false;
    	}
    	campoTextoTabla('puestoEmpleadoFunciones'.$codigoCentro.'_'.$i,$puesto['nombre'],'input-large',true);
    	campoTextoTabla('funcionesFunciones'.$codigoCentro.'_'.$i,$datos['funciones']);
    	campoTextoTabla('areaFunciones'.$codigoCentro.'_'.$i,$datos['area'],'input-small');
    	areaTextoTabla('productosFunciones'.$codigoCentro.'_'.$i,$datos['productos'],'areaProductosSustancias'); 
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function creaTablaFuncionesResponsabilidades2($codigoFormularioPRL,$codigoContrato,$codigoCentro){
	    echo "
	    <div class='control-group'>                     
	        <div class='controls' style='margin-left:10px;'>
	            <table class='tabla-simple' id='tablaFunciones".$codigoCentro."_2'>
	                <thead>
	                    <tr>
	                    	<th> Empleado </th>
	                    	<th> Maquinaria y equipos usados </th>
	                    	<th> Herramientas </th>
	                        <th> Medios mecanicos de carga </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($codigoFormularioPRL!=false){
	                $consulta=consultaBD("SELECT * FROM funciones_formulario_prl WHERE codigoFormularioPRL='$codigoFormularioPRL' AND codigoCentroTrabajo='$codigoCentro'");
	                while($empleado=mysql_fetch_assoc($consulta)){
	                   imprimeLineaTablaFuncionesResponsabilidades2($i,$codigoContrato,$empleado,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaFuncionesResponsabilidades2(0,$codigoContrato,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	        </div>
	    </div>";
}

function imprimeLineaTablaFuncionesResponsabilidades2($i,$codigoContrato,$datos,$codigoCentro){

    echo "<tr>";
    	if($datos['codigoEmpleado']!=NULL){
    		$empleado=consultaBD('SELECT CONCAT(nombre," ",apellidos) AS empleado FROM empleados WHERE codigo='.$datos['codigoEmpleado'],false,true);
    		$empleado=$empleado['empleado'];
    	} else {
    		$empleado=false;
    	}
    	campoTextoTabla('nombreEmpleadoFunciones'.$codigoCentro.'_'.$i,$empleado,'input-large',true);
    	areaTextoTabla('maquinariaFunciones'.$codigoCentro.'_'.$i,$datos['maquinaria'],'areaMaquinaria'); 
    	areaTextoTabla('herramientasFunciones'.$codigoCentro.'_'.$i,$datos['herramientas'],'areaMaquinaria'); 
    	areaTextoTabla('medioFunciones'.$codigoCentro.'_'.$i,$datos['medio'],'areaMaquinaria'); 
    echo "
    </tr>";
}

function creaTablaPuestosTomaDatos($codigoPRL,$codigoCliente,$sensible,$codigoCentro){
		$cliente=consultaBD('SELECT clientes.codigo FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='.$codigoCliente,true,true);
		$codigoCliente=$cliente['codigo'];
	    echo "
	    <div class='control-group'>                     
	        <label class='control-label'>Indica el/los puesto/s:</label>
	        <div class='controls'>
	            <table class='tabla-simple mitadAncho' id='tablaPuestosTrabajo".$codigoCentro.$sensible."'>
	                <thead>
	                    <tr>
	                        <th> Puesto de trabajo </th>
	                        <th> Sección / Area </th>
	                        <th> </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($codigoPRL!=false){
	                $consulta=consultaBD("SELECT codigoPuestoTrabajo, seccionPuestoTrabajo FROM puestos_trabajo_formulario_prl WHERE codigoFormularioPRL='$codigoPRL' AND sensible='$sensible' AND codigoCentroTrabajo='$codigoCentro'");
	                while($puesto=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaPuestosTrabajo($i,$codigoCliente,$puesto,$sensible,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaPuestosTrabajo(0,$codigoCliente,false,$sensible,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaPuestosTrabajo".$codigoCentro.$sensible."\");'><i class='icon-plus'></i> Añadir puesto</button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaPuestosTrabajo".$codigoCentro.$sensible."\");'><i class='icon-trash'></i> Eliminar puesto</button>
	        </div>
	    </div>";
}

function imprimeLineaTablaPuestosTrabajo($i,$codigoCliente,$datos,$sensible,$codigoCentro){
	$j=$i+1;

    echo "<tr>";
        campoSelectConsulta('codigoPuestoTrabajo'.$sensible.$codigoCentro.$i,'',"SELECT codigo, nombre AS texto FROM puestos_trabajo ORDER BY nombre;",$datos['codigoPuestoTrabajo'],'selectpicker span3 show-tick','data-live-search="true"','',1,false);
        campoTextoTabla('seccionPuestoTrabajo'.$sensible.$codigoCentro.$i,$datos['seccionPuestoTrabajo']);
        campoOculto($sensible,'sensible'.$sensible.$codigoCentro.$i);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}



function camposEmergencia($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Procedimientos de emergencia</h3>";
	
	abreColumnaCampos();
	
	campoRadioFormulario($codigoCentro.'20','Dispone la empresa de algún procedimiento de emergencia',$datos);
	
	cierraColumnaCampos();
	abreColumnaCampos();

	areaTextoFormulario($codigoCentro.'51','Comentarios',$datos);
	
	cierraColumnaCampos(true);
}


function camposMateriasPrimas($datos,$codigoCentro){
	    echo "<br />
	    <h3 class='apartadoFormulario'>Materias primas utilizadas</h3>
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaMateriasPrimas".$codigoCentro."'>
	            <thead>
	                <tr>
	                    <th> Materia </th>
	                    <th> Lugar </th>
	                    <th> Cantidad </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM materias_primas_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($materia=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaMateriasPrimas($i,$materia,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaMateriasPrimas(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaMateriasPrimas".$codigoCentro."\");'><i class='icon-plus'></i> Añadir materia prima</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaMateriasPrimas".$codigoCentro."\");'><i class='icon-trash'></i> Eliminar materia prima</button>
	    </div><br />";
}

function imprimeLineaTablaMateriasPrimas($i,$datos,$codigoCentro){
	$j=$i+1;

    echo "<tr>";
        	campoTextoTabla('materiaPrima'.$codigoCentro.'_'.$i,$datos['materia']);
        	campoTextoTabla('lugar'.$codigoCentro.'_'.$i,$datos['lugar']);
        	campoTextoTabla('cantidad'.$codigoCentro.'_'.$i,$datos['cantidad']);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}


function camposCentro($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Características del centro</h3>";
	abreColumnaCampos();

	campoNumeroFormulario($codigoCentro.'21','Superficie total (m<sup>2</sup>)',$datos);
	campoNumeroFormulario($codigoCentro.'22','Ocupación (nº personas)',$datos);
	campoNumeroFormulario($codigoCentro.'23','Nº de plantas',$datos);
	//areaTextoFormulario(51,'Enumeración de instalaciones',$datos,'campoInstalaciones');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoRadioFormulario($codigoCentro.'24','Sectorización',$datos);
	campoTextoFormulario($codigoCentro.'25','Tipo de estructura',$datos);
	campoRadioFormulario($codigoCentro.'49','Planos disponibles',$datos);
	campoFicheroPlano($datos,$codigoCentro);

	cierraColumnaCampos(true);

	echo "<br clearr='all'><h3 class='apartadoFormulario'>Instalaciones</h3>";

	abreColumnaCampos();
		creaTablaInstalaciones($datos,$codigoCentro);
		areaTextoFormulario($codigoCentro.'70','Instalación de gas-oil',$datos);
	cierraColumnaCampos();

	abreColumnaCampos();
		areaTextoFormulario($codigoCentro.'68','Instalación eléctrica',$datos);
		areaTextoFormulario($codigoCentro.'69','Instalación de aire comprimido',$datos);
	cierraColumnaCampos();

	tablaGas($datos,$codigoCentro);
}

function tablaGas($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaGas$codigoCentro'>
	            <thead>
	            	<tr>
	                    <th colspan='4'> Dimensiones reglamentarias de los lugares de trabajo (R.D. 486/1997):  </th>
	                    <th> </th>
	                </tr>
	                <tr>
	                    <th> Zona </th>
	                    <th> m. suelo - techo </th>
	                    <th> m<sup>2</sup>. por trabajador</th>
	                    <th> m<sup>3</sup>. por trabajador</th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM gas_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaGas($i,$item,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaGas(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaGas$codigoCentro\");'><i class='icon-plus'></i> Añadir</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaGas$codigoCentro\");'><i class='icon-trash'></i> Eliminar</button>
	    </div><br />";
}

function imprimeLineaTablaGas($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
        	campoTextoTabla('zonaGas'.$codigoCentro.'_'.$i,$datos['zona']);
        	campoTextoTabla('sueloGas'.$codigoCentro.'_'.$i,$datos['suelo'],'input-mini');
        	campoTextoTabla('m2Gas'.$codigoCentro.'_'.$i,$datos['m2'],'input-mini');
        	campoTextoTabla('m3Gas'.$codigoCentro.'_'.$i,$datos['m3'],'input-mini');
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function campoFicheroPlano($datos,$codigoCentro){
	echo "<div class='hide' id='div".$codigoCentro."49'>";
			campoFichero('pregunta'.$codigoCentro.'55','Fichero de planos',0,$datos,'../documentos/planificacion/','Ver/Descargar',false);
	echo "</div>";
}

function camposHigienicos($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Servicios higiénicos</h3>";
	abreColumnaCampos();
		areaTextoFormulario($codigoCentro.'65','Descripción',$datos);
	cierraColumnaCampos(true);
	tablaHigienicos($datos,$codigoCentro);
}

function tablaHigienicos($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaHigienicos$codigoCentro'>
	            <thead>
	                <tr>
	                    <th> Instalación </th>
	                    <th> Descripción </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM higienicos_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaHigienicos($i,$item,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaHigienicos(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaHigienicos$codigoCentro\");'><i class='icon-plus'></i> Añadir</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaHigienicos$codigoCentro\");'><i class='icon-trash'></i> Eliminar</button>
	    </div><br />";
}

function imprimeLineaTablaHigienicos($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
        	campoTextoTabla('nombreHigienico'.$codigoCentro.'_'.$i,$datos['nombre'],'span4');
        	areaTextoTabla('descripcionHigienico'.$codigoCentro.'_'.$i,$datos['descripcion'],'areaDescripcionCentro');
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function camposIluminacion($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Iluminación</h3>";
	abreColumnaCampos();
		areaTextoFormulario($codigoCentro.'66','Descripción',$datos);
	cierraColumnaCampos(true);
	tablaIluminacion($datos,$codigoCentro);
	abreColumnaCampos();
		areaTextoFormulario($codigoCentro.'67','Iluminación de emergencia',$datos);
	cierraColumnaCampos(true);
}

function tablaIluminacion($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaIluminacion$codigoCentro'>
	            <thead>
	                <tr>
	                    <th> Iluminacion </th>
	                    <th> Descripción </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM iluminacion_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaIluminacion($i,$item,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaIluminacion(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaIluminacion$codigoCentro\");'><i class='icon-plus'></i> Añadir</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaIluminacion$codigoCentro\");'><i class='icon-trash'></i> Eliminar</button>
	    </div><br />";
}

function imprimeLineaTablaIluminacion($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
        	campoTextoTabla('nombreIluminacion'.$codigoCentro.'_'.$i,$datos['nombre'],'span4');
        	areaTextoTabla('descripcionIluminacion'.$codigoCentro.'_'.$i,$datos['descripcion'],'areaDescripcionCentro');
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function camposEvacuacion($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Medidas para casos de emergencia y evacuación</h3>";
	
	abreColumnaCampos();
	
	tablaEvacuacion($datos,$codigoCentro);
	
	cierraColumnaCampos(true);
}

function tablaEvacuacion($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaEvacuacion$codigoCentro'>
	            <thead>
	                <tr>
	                    <th colspan='2'> Zona </th>
	                    <th> Descripción </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM evacuacion_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaEvacuacion($i,$item,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaEvacuacion(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaEvacuacion$codigoCentro\");'><i class='icon-plus'></i> Añadir</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaEvacuacion$codigoCentro\");'><i class='icon-trash'></i> Eliminar</button>
	    </div><br />";
}

function imprimeLineaTablaEvacuacion($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
    		campoSelect('tipoEvacuacion'.$codigoCentro.'_'.$i,'',array('','Pasillos y superficies de transito','Puertas y salidas','Señalizaciones','Vías y salidas de emergencia','Equipos de extinción de incendios','Primeros auxilios'),array(0,1,2,3,4,5,6),$datos['tipo'],'selectpicker span4 show-tick',"data-live-search='true'",1);
        	campoTextoTabla('zonaEvacuacion'.$codigoCentro.'_'.$i,$datos['zona'],'span4');
        	areaTextoTabla('descripcionEvacuacion'.$codigoCentro.'_'.$i,$datos['descripcion'],'areaDescripcionCentro');
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function camposRecursos($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Recursos</h3>";
	
	abreColumnaCampos('sinFlotar');
	
	tablaRecursosH($datos,$codigoCentro);
	
	cierraColumnaCampos(true);

	abreColumnaCampos('sinFlotar');
	
	tablaRecursosT($datos,$codigoCentro);
	
	cierraColumnaCampos(true);

	abreColumnaCampos('sinFlotar');
	
	tablaRecursosE($datos,$codigoCentro);
	
	cierraColumnaCampos(true);
}

function tablaRecursosH($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaRecursosH$codigoCentro'>
	            <thead>
	            	<tr>
	                    <th colspan='5'> Recursos humanos de los que se dispone </th>
	                </tr>
	                <tr>
	                    <th> Recurso </th>
	                    <th> Identificación </th>
	                    <th> Tipo </th>
	                    <th> Puesto </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM recursos_humanos_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaRecursosH($i,$item,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaRecursosH(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaRecursosH$codigoCentro\");'><i class='icon-plus'></i> Añadir</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaRecursosH$codigoCentro\");'><i class='icon-trash'></i> Eliminar</button>
	    </div><br />";
}

function imprimeLineaTablaRecursosH($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
    		campoTextoTabla('recursoRecursosH'.$codigoCentro.'_'.$i,$datos['recurso']);
    		campoTextoTabla('identificacionRecursosH'.$codigoCentro.'_'.$i,$datos['identificacion']);
    		campoSelect('tipoRecursosH'.$codigoCentro.'_'.$i,'',array('','Recurso propio','Recurso externo'),array(0,'PROPIO','EXTERNO'),$datos['tipo'],'selectpicker span3 show-tick',"data-live-search='true'",1);
        	campoTextoTabla('puestoRecursosH'.$codigoCentro.'_'.$i,$datos['puesto']);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function tablaRecursosT($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaRecursosT$codigoCentro'>
	            <thead>
	            	<tr>
	                    <th colspan='3'> Recursos materiales y técnicos de los que se dispone </th>
	                </tr>
	                <tr>
	                    <th> Recurso </th>
	                    <th> Tipo </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM recursos_tecnicos_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaRecursosT($i,$item,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaRecursosT(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaRecursosT$codigoCentro\");'><i class='icon-plus'></i> Añadir</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaRecursosT$codigoCentro\");'><i class='icon-trash'></i> Eliminar</button>
	    </div><br />";
}

function imprimeLineaTablaRecursosT($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
    		campoTextoTabla('recursoRecursosT'.$codigoCentro.'_'.$i,$datos['recurso'],'span4');
    		campoSelect('tipoRecursosT'.$codigoCentro.'_'.$i,'',array('','Espacios','Técnicos','Equipos'),array(0,'ESPACIOS','TECNICOS','EQUIPOS'),$datos['tipo'],'selectpicker span3 show-tick',"data-live-search='true'",1);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function tablaRecursosE($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaRecursosE$codigoCentro'>
	            <thead>
	            	<tr>
	                    <th colspan='5'> Recursos económicos de los que se dispone </th>
	                </tr>
	                <tr>
	                    <th> Recurso </th>
	                    <th> Tipo </th>
	                    <th> Descripción </th>
	                    <th> Importe asignado (€ / año)</th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM recursos_economicos_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaRecursosE($i,$item,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaRecursosE(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaRecursosE$codigoCentro\");'><i class='icon-plus'></i> Añadir</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaRecursosE$codigoCentro\");'><i class='icon-trash'></i> Eliminar</button>
	    </div><br />";
}

function imprimeLineaTablaRecursosE($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
    		campoTextoTabla('recursoRecursosE'.$codigoCentro.'_'.$i,$datos['recurso']);
    		campoSelect('tipoRecursosE'.$codigoCentro.'_'.$i,'',array('','Obligatorio','No obligatorio'),array(0,'OBLIGATORIO','NOOBLIGATORIO'),$datos['tipo'],'selectpicker span3 show-tick',"data-live-search='true'",1);
    		areaTextoTabla('descripcionRecursosE'.$codigoCentro.'_'.$i,$datos['descripcion'],'areaDescripcionCentro');
    		campoTextoTabla('importeRecursosE'.$codigoCentro.'_'.$i,$datos['importe']);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function camposInstalaciones($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Instalaciones que pueden generar una emergencia</h3>";
	abreColumnaCampos('span4');
	campoDato('Suministro eléctrico','');
	campoNumeroFormulario($codigoCentro.'26','Voltaje (V)',$datos);
	campoNumeroFormulario($codigoCentro.'27','Potencia contratada (Kw)',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	areaTextoFormulario($codigoCentro.'28','Riesgos especiales (explosión, inundación, etc)',$datos);

	cierraColumnaCampos(true);
}

function camposAlarmas($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Medidas de alarma</h3>";
	abreColumnaCampos();

	campoRadioFormulario($codigoCentro.'29','Alarma general',$datos);
	campoRadioFormulario($codigoCentro.'30','Detección',$datos);
	campoRadioFormulario($codigoCentro.'31','Walky Talky',$datos);
	areaTextoFormulario($codigoCentro.'35','Observaciones',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();

	campoRadioFormulario($codigoCentro.'32','Pulsadora de alarma',$datos);
	campoRadioFormulario($codigoCentro.'33','Teléfono interno',$datos);
	campoRadioFormulario($codigoCentro.'34','Otros',$datos);;

	cierraColumnaCampos(true);
}

function camposIncendio($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Medidas de protección contra incendios</h3>";
	
	tablaExtintores($datos,$codigoCentro);
	tablaBIE($datos,$codigoCentro);
	tablaOtros($datos,$codigoCentro);
}

function tablaExtintores($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaExtintores$codigoCentro'>
	            <thead>
	            	<tr>
	            		<th colspan='8'>EXTINTORES</th>
	            	</tr>
	                <tr>
	                    <th> Nº </th>
	                    <th> KG </th>
	                    <th> Tipo </th>
	                    <th> Eficacia </th>
	                    <th> Señalizado </th>
	                    <th> Revisado </th>
	                    <th> Ubicación </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM extintores_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($extintor=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaExtintores($i,$extintor,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaExtintores(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaExtintores$codigoCentro\");'><i class='icon-plus'></i> Añadir extintor</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaExtintores$codigoCentro\");'><i class='icon-trash'></i> Eliminar extintor</button>
	    </div><br />";
}

function imprimeLineaTablaExtintores($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
        	campoTextoTabla('numero'.$codigoCentro.'_'.$i,$datos['numero'],'input-small pagination-right');
        	campoTextoTabla('kg'.$codigoCentro.'_'.$i,$datos['kg'],'input-mini pagination-right');
        	campoTextoTabla('tipo'.$codigoCentro.'_'.$i,$datos['tipo'],'input-small pagination-right');
        	campoTextoTabla('eficacia'.$codigoCentro.'_'.$i,$datos['eficacia'],'input-small pagination-right');
        	campoSelect('senializado'.$codigoCentro.'_'.$i,'',array('Si','No'),array('SI','NO'),$datos['senializado'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	campoSelect('revisado'.$codigoCentro.'_'.$i,'',array('Si','No'),array('SI','NO'),$datos['revisado'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	areaTextoTabla('ubicacionExtintor'.$codigoCentro.'_'.$i,$datos['ubicacion']);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function tablaBIE($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaBIE$codigoCentro'>
	            <thead>
	            	<tr>
	            		<th colspan='7'>BOCAS DE INCENDIO EQUIPADAS (BIE)</th>
	            	</tr>
	                <tr>
	                    <th> Nº </th>
	                    <th> Medida </th>
	                    <th> Presión </th>
	                    <th> Señalizado </th>
	                    <th> Revisado </th>
	                    <th> Ubicación </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM bie_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($extintor=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaBIE($i,$extintor,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaBIE(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaBIE$codigoCentro\");'><i class='icon-plus'></i> Añadir BIE</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaBIE$codigoCentro\");'><i class='icon-trash'></i> Eliminar BIE</button>
	    </div><br />";
}

function imprimeLineaTablaBIE($i,$datos,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
        	campoTextoTabla('numeroBIE'.$codigoCentro.'_'.$i,$datos['numero'],'input-small pagination-right');
        	campoSelect('medidaBIE'.$codigoCentro.'_'.$i,'',array('25 mm','45 mm'),array('25','45'),$datos['medida'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	campoTextoTabla('presion'.$codigoCentro.'_'.$i,$datos['presion'],'input-small pagination-right');
        	campoSelect('senializado'.$codigoCentro.'_'.$i,'',array('Si','No'),array('SI','NO'),$datos['senializado'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	campoSelect('revisado'.$codigoCentro.'_'.$i,'',array('Si','No'),array('SI','NO'),$datos['revisado'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	areaTextoTabla('ubicacion'.$codigoCentro.'_'.$i,$datos['ubicacion']);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function tablaOtros($datos,$codigoCentro){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaOtros$codigoCentro'>
	            <thead>
	            	<tr>
	            		<th colspan='2'>OTROS</th>
	            	</tr>
	            </thead>
	            <tbody>";
	        	echo '<tr><td>Columna seca</td>';
	          	campoSelectFormulario($codigoCentro.'35','',$datos,1);
	          	echo '</tr><tr><td>Hidrante exterior</td>';
	          	campoSelectFormulario($codigoCentro.'36','',$datos,1);
	          	echo '</tr><tr><td>Medios humanos (Equipos de emergencia convenientemente formados)</td>';
	          	campoSelectFormulario($codigoCentro.'37','',$datos,1);
	          	echo '</tr><tr><td>Punto de reunión señalizado</td>';
	          	campoSelectFormulario($codigoCentro.'38','',$datos,1);
	          	echo '</tr><tr><td>Lugar</td>';
	          	campoTextoTablaFormulario($codigoCentro.'39',$datos);
	    echo "	</tbody>
	        </table>
	    </div><br />";
}

function camposPrimerosAuxilios($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Primeros auxilios</h3>";
	abreColumnaCampos('sinFlotar');

	//campoTextoFormulario($codigoCentro.'40','Cantidad de botiquines',$datos,'input-mini pagination-right');
	campoOculto('','pregunta'.$codigoCentro.'40');
	tablaPrimeros('Botiquines',$datos,'botiquines_formulario_prl','Botiquines',$codigoCentro);
	//campoTextoFormulario($codigoCentro.'41','Cantidad de locales de primeros auxilios',$datos,'input-mini pagination-right');
	campoOculto('','pregunta'.$codigoCentro.'41');
	tablaPrimeros('Locales de primeros auxilios',$datos,'locales_formulario_prl','Locales',$codigoCentro);
	
	//campoTextoFormulario($codigoCentro.'42','Cantidad de medios humanos',$datos,'input-mini pagination-right');
	campoOculto('','pregunta'.$codigoCentro.'42');
	tablaPrimeros('Medios humanos',$datos,'medios_humanos_formulario_prl','Medios',$codigoCentro);
	campoRadioFormulario($codigoCentro.'52','Formación en primeros auxilios',$datos);
	campoRadioFormulario($codigoCentro.'53','Desfibrilador',$datos);

	cierraColumnaCampos(true);
}

function tablaPrimeros($texto,$datos,$tabla,$prefijo,$codigoCentro){
	    echo "
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='".$tabla.$codigoCentro."'>
	            <thead>
	            	<tr>
	            		<th colspan='5'>".$texto."</th>
	            	</tr>
	                <tr>
	                	<th> Nº </th>
	                    <th> Señalizado </th>
	                    <th> Revisado </th>
	                    <th> Ubicación </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM ".$tabla." WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaPrimeros($i,$item,$prefijo,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaPrimeros(0,false,$prefijo,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"".$tabla.$codigoCentro."\");'><i class='icon-plus'></i> Añadir $prefijo</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"".$tabla.$codigoCentro."\");'><i class='icon-trash'></i> Eliminar $prefijo</button>
	    </div><br />";
}

function imprimeLineaTablaPrimeros($i,$datos,$prefijo,$codigoCentro){
	$j=$i+2;

    echo "<tr>";
    		campoTextoTabla('numero'.$prefijo.$codigoCentro.'_'.$i,$datos['numero'],'input-mini pagination-right');
        	campoSelect('senializado'.$prefijo.$codigoCentro.'_'.$i,'',array('Si','No'),array('SI','NO'),$datos['senializado'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	campoSelect('revisado'.$prefijo.$codigoCentro.'_'.$i,'',array('Si','No'),array('SI','NO'),$datos['revisado'],'selectpicker span2 show-tick','data-live-search="true"',1);
        	areaTextoTabla('ubicacion'.$prefijo.$codigoCentro.'_'.$i,$datos['ubicacion']);
    echo "	<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    	  </tr>";
}

function camposVestuarios($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Vestuarios</h3>";
	abreColumnaCampos();

	campoRadioFormulario($codigoCentro.'41','Lavabo/s',$datos);
	campoTextoFormulario($codigoCentro.'42','Cantidad',$datos,'input-mini pagination-right');
	campoRadioFormulario($codigoCentro.'43','Ducha/s',$datos);
	campoTextoFormulario($codigoCentro.'44','Cantidad',$datos,'input-mini pagination-right');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoRadioFormulario($codigoCentro.'45','¿Mixto?',$datos);
	campoRadioFormulario($codigoCentro.'46','Taquillas',$datos);
	campoRadioFormulario($codigoCentro.'47','En caso anterior afirmativo, ¿disponen de compartimentos separados?',$datos);;

	cierraColumnaCampos(true);
}

function camposPuestosTrabajos($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Puestos de trabajos</h3>";
	abreColumnaCampos();
	creaTablaPuestosTomaDatos($datos['codigo'],$datos['codigoContrato'],'NO',$codigoCentro);
	cierraColumnaCampos(true);
}

function camposObservaciones($datos,$codigoCentro){
	echo "<h3 class='apartadoFormulario'>Observaciones</h3>";
	abreColumnaCampos();
	areaTextoFormulario($codigoCentro.'48','Observaciones',$datos,'areaInforme');
	cierraColumnaCampos(true);
}

//Fin parte Toma de Datos

/*function tomaDatos(){
	$prl =datosRegistro('formularioPRL',$_GET['codigoPRL']);
	$formulario = recogerFormularioServicios($prl);
	$formulario = completarDatosGeneralesPRL($formulario,$prl['codigoCliente']);

	abreVentanaGestionConBotones('Toma de datos PRL','index.php','','icon-edit','margenAb');
	campoOculto($_GET['codigoPRL'],'codigoPRL');
	abreColumnaCampos();
		campoTextoFormulario(1,'Consultor',$formulario);
		//campoRadio('toma_datos'.'Toma de datos finalizada',$trabajo['toma_datos']);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoFechaFormulario(2,'Fecha',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DATOS GENERALES</h3>";

	abreColumnaCampos();
		campoTextoFormulario(3,'Razón Social',$formulario);
		campoTextoFormulario(4,'Dirección Social',$formulario);
		campoTextoFormulario(10,'Código postal',$formulario);
		campoTextoFormulario(8,'Representate legal',$formulario);
		campoTextoFormulario(6,'Teléfono',$formulario);
		campoTextoFormulario(7,'Nº de trabajadores',$formulario);
		campoRadioFormulario(16,'¿Hay trabajadores en centros ajenos?',$formulario);
		campoTextoFormulario(67,'Sector',$formulario);
		campoTextoFormulario(68,'Persona de contacto',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(9,'CIF',$formulario);
		campoTextoFormulario(5,'Localidad',$formulario);
		campoTextoFormulario(11,'Provincia',$formulario);
		campoTextoFormulario(14,'NIF',$formulario);
		campoTextoFormulario(12,'Email',$formulario);
		campoTextoFormulario(15,'Nº de centros',$formulario);
		echo "<br/><br/><br/>";
		campoTextoFormulario(13,'Actividad empresarial',$formulario);
		campoTextoFormulario(69,'Responsable del Plan de Prevención',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DESCRIPCIÓN DEL CENTRO</h3>";

	abreColumnaCampos();
		campoTextoFormulario(17,'Localización (zona industrial, comercial, residencial)',$formulario);
		campoTextoFormulario(19,'Nº de plantas',$formulario);
		campoTextoFormulario(21,'Aforo máximo',$formulario);
		
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(18,'Superficie m2',$formulario);
		echo "<br/>";
		campoTextoFormulario(20,'Tipo de plantas (bajos, primera planta, ático)',$formulario);
		campoRadioFormulario(22,'¿Mismo nivel?',$formulario);
	cierraColumnaCampos();

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Nombre y apellidos' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaEmpleadosPRL'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
                  			<th colspan='6'>Trabajadores</th>
                  		</tr>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Nombre y apellidos </th>
							<th> DNI </th>
							<th> Puesto de trabajo </th>
							<th> Descripción funciones </th>
							<th> ¿Salen fuera del centro? </th>
							<th> En caso afirmativo ¿Tipo de vehículo? </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($prl){
        	$empleados = consultaBD("SELECT * FROM empleados_prl WHERE codigoTrabajo=".$prl['codigo'],true);
        	while($empleado = mysql_fetch_assoc($empleados)){
        		echo "<tr>";
    				campoTextoTabla('nombreEmpleadoPRL'.$indice,$empleado['nombre']);
    				campoTextoTabla('dniEmpleadoPRL'.$indice,$empleado['dni'],'input-small');
    				campoTextoTabla('puestoEmpleadoPRL'.$indice,$empleado['puesto'],'input-small');
    				areaTextoTabla('funcionesEmpleadoPRL'.$indice,$empleado['funciones']);
    				campoSelect('salenEmpleadoPRL'.$indice,'',array('Si','No'),array('SI','NO'),$empleado['salen'],'selectpicker span2 show-tick','data-live-search="true"',1);
    				campoTextoTabla('vehiculoEmpleadoPRL'.$indice,$empleado['vehiculo'],'input-small');
				echo"
					</tr>";
        		$indice++;
        	}
        }
    	for($indice;$indice<5;$indice++){
    		echo "<tr>";
    			campoTextoTabla('nombreEmpleadoPRL'.$indice);
    			campoTextoTabla('dniEmpleadoPRL'.$indice,'','input-small');
    			campoTextoTabla('puestoEmpleadoPRL'.$indice,'','input-small');
    			areaTextoTabla('funcionesEmpleadoPRL'.$indice);
    			campoSelect('salenEmpleadoPRL'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
    			campoTextoTabla('vehiculoEmpleadoPRL'.$indice,'','input-small');
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaEmpleadosPRL\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Centro de trabajo' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaCentrosPRL'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
                  			<th colspan='5'>Centros de trabajo</th>
                  		</tr>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Centro de trabajo </th>
							<th> Nº delegados<br/>de prevención </th>
							<th> Comite de<br/>seguridad y salud </th>
							<th> Trabajador designado </th>
							<th> Consulta directa <br/>a los trabajadores </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($prl){
        	$centros = consultaBD("SELECT * FROM centros_prl WHERE codigoTrabajo=".$prl['codigo'],true);
        	while($centro = mysql_fetch_assoc($centros)){
        		echo "<tr>";
    				campoTextoTabla('nombreCentroPRL'.$indice,$centro['nombre']);
    				campoTextoTabla('delegadoCentroPRL'.$indice,$centro['delegado'],'input-small');
    				campoTextoTabla('comiteCentroPRL'.$indice,$centro['comite']);
    				campoTextoTabla('trabajadorCentroPRL'.$indice,$centro['trabajador']);
    				campoSelect('consultaCentroPRL'.$indice,'',array('Si','No'),array('SI','NO'),$centro['consulta'],'selectpicker span2 show-tick','data-live-search="true"',1);
				echo"
					</tr>";
        		$indice++;
        	}
        }

    		echo "<tr>";
    			campoTextoTabla('nombreCentroPRL'.$indice);
    			campoTextoTabla('delegadoCentroPRL'.$indice,'','input-small');
    			campoTextoTabla('comiteCentroPRL'.$indice);
    			campoTextoTabla('trabajadorCentroPRL'.$indice);
    			campoSelect('consultaCentroPRL'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
			echo"
				</tr>";
    
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaCentrosPRL\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";

	echo "<br clear='all'><h3 class='apartadoFormulario'> INSTALACIONES</h3>";
	abreColumnaCampos();
		campoRadioFormulario(23,'Dispone de instalación de acondicionamiento de aire (calefacción, aire)',$formulario);
		campoRadioFormulario(32,'¿Dispone de cuadro electríco',$formulario);
		echo "<div id='div32' class='hidden'>";
			campoRadioFormulario(33,'Está señalizado',$formulario);
			campoTextoFormulario(34,'Ubicación',$formulario);
		echo "</div>";
		campoRadioFormulario(25,'Dispone de instalación de extracción de humos (campana extractora)',$formulario);


	cierraColumnaCampos();

	abreColumnaCampos();
		echo "<div id='div23' class='hidden'>";
		campoRadioFormulario(24,'¿Cuales?',$formulario,'CALEFACCION',array('Calefaccion','Aire'),array('CALEFACCION','AIRE'));
		echo "</div>";
		echo "<br/><br/>";
		campoRadioFormulario(26,'Dispone de Montacargas/ascensores',$formulario);
		echo "<div id='div26' class='hidden'>";
			campoTextoFormulario(27,'¿Cuantos?',$formulario);
			campoTextoFormulario(28,'Empresa de mantenimiento',$formulario);
		echo "</div>";
		echo "Dispones de otras:";
		campoRadioFormulario(29,'Aire',$formulario);
		campoRadioFormulario(30,'Gas',$formulario);
		campoRadioFormulario(31,'Frigos',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> ORGANIZACIÓN EMPRESA</h3>";

	abreColumnaCampos();
		campoRadioFormulario(75,'Dirección',$formulario,'INTERMEDIO',array('Mando intermedio','Trabajadores'),array('INTERMEDIO','TRABAJADORES'),true);
		campoRadioFormulario(35,'Qué medio de comunicación se utiliza para la prevención',$formulario,'ORAL',array('Oral','Correo electrónico','Carta'),array('ORAL','CORREO','CARTA'),true);
		campoRadioFormulario(36,'Qué recursos dispone para esta actividad:',$formulario,'OFICINA',array('Oficina','Equipos informáticos','Impresora','Otros'),array('OFICINA','EQUIPOS','IMPRESORA','OTROS'),true);
		campoTextoFormulario(71,'Trabajador responsable de la seguridad',$formulario);
		campoTextoFormulario(72,'Trabajador responsable de la ergonomía y psicología',$formulario);

	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(76,'Tipo de prevención',$formulario,'EMPRESARIO',array('Empresario','Trabajador designado'),array('EMPRESARIO','TRABAJADOR'),true);
		campoRadioFormulario(66,'Formación del responsable de prevención',$formulario,'BASICO',array('Básico','Medio','Superior'),array('BASICO','MEDIO','SUPERIOR'),true);
		campoRadioFormulario(70,'Servicio de prevencion:',$formulario,'PROPIO',array('Propio','Mancomunado','Ajeno','Sistema Mixto'),array('PROPIO','MANCOMUNADO','AJENO','MIXTO'),true);
		campoTextoFormulario(73,'Trabajador responsable de la higiene',$formulario);
		campoTextoFormulario(74,'Trabajador responsable de la vigilancia de la salud',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> MEDIDAS DE EMERGENCIA</h3>";

	abreColumnaCampos();
		campoRadioFormulario(37,'¿Alguien en la empresa dispone de formación en primeros auxilios?',$formulario);
		echo "<br/>";
		campoTextoFormulario(38,'Instalación de seguridad contraincendios perteneciente al edificio donde se ubica el local o la oficina',$formulario);
		campoTextoFormulario(41,'Nº de Bies',$formulario);
		echo "<br/>";
		campoRadioFormulario(43,'Sistema de detección automática de incendios',$formulario);
		campoRadioFormulario(45,'Extintores',$formulario);
		echo "<div id='div45' class='hidden'>";
			echo "Tipos y carácteristicas:";
			campoRadioFormulario(47,'ABC',$formulario);
			campoRadioFormulario(48,'Espuma',$formulario);
			campoRadioFormularioConLogo(51,'Señal de seguridad de cada extintor',$formulario,'iconExtintor.jpg');
		echo "</div>";
		campoRadioFormulario(53,'En todo caso existirá el alumbrado de emergencia en las puertas de salida',$formulario);
		campoRadioFormulario(55,'Sistema de alarmas',$formulario,'AUTOMATICA',array('Alarma automática de incendios','Megafonía/telefonía','Alarma manual de incendios (con pulsador de alarma)'),array('AUTOMATICA','MEGAFONIA','MANUAL'),true);

	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(39,'¿Alguien en la empresa dispone de formación en extinción de incendios?',$formulario);
		campoTextoFormulario(40,'Instalación de seguridad contraincendios formada por red de agua y Bies (Bocas de incendio equipadas)',$formulario);
		campoRadioFormularioConLogo(42,'Señal de Seguridad de cada BIE',$formulario,'iconBie.png');
		campoRadioFormulario(44,'Sistema de actuación automática de incendios (sprinkkles)',$formulario);
		echo "<div id='div45_1' class='hidden'>";
			campoTextoFormulario(46,'Nº de Extintores',$formulario);
			echo "<br/>";
			campoRadioFormulario(49,'CO2',$formulario);
			campoRadioFormulario(50,'Otros',$formulario);
		echo "</div>";
		echo "<br/>";
		campoRadioFormulario(52,'Alumbrado de emergencia',$formulario);
		campoRadioFormulario(54,'Nº de lámparas alumbrado de emergencia',$formulario);
		echo "<div id='div26_1' class='hidden'>";
			campoRadioFormularioConLogo(56,'Si dispone de ascensores/montacargas deberá contar con la preceptiva señal de seguridad',$formulario,'iconAscensor.png');
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> RECURSOS TÉCNICOS EN CASO DE EVACUACIÓN</h3>";
	abreColumnaCampos();
		campoRadioFormularioConLogo(57,'Señal de salvamento. Recorrido de evacuación horizontal',$formulario,'iconHorizontal.jpg');
		campoRadioFormularioConLogo(58,'Señal de salvamento. Recorrido de evacuación vertica',$formulario,'iconVertical.png');
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormularioConLogo(59,'Señal de salvamento. Recorrido de evacuación',$formulario,'iconDireccion.png');
		campoRadioFormularioConLogo(60,'Señal de salvamento. Teléfono de salvamento',$formulario,'iconTelefono.png');
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> RECURSOS TÉCNICOS EN CASO DE PRIMEROS AUXILIOS</h3>";
	abreColumnaCampos();
		campoRadioFormularioConLogo(61,'Botiquín. En todo caso existirá en el centro de trabajo un botiquín de primeros auxilios junto con la señalización informativa correspondiente',$formulario,'iconBotiquin.png');
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormularioConLogo(62,'Sistema lavaojos. Señal informativa',$formulario,'iconLavaojos.png');
		campoRadioFormulario(63,'Otros',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> VIGILANCIA DE SALUD</h3>";
	abreColumnaCampos();
		campoRadioFormulario(64,'¿Dispone usted de un servicio de Vigilancia de la Salud (reconocimientos médicos)?',$formulario);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormulario(65,'¿En caso negativo, querría que le pusiéramos en contacto con un colaborador nuestro?',$formulario);
	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}*/

function imprimeFormularioPRL($where){
	global $_CONFIG;
	$tipos=array(1=>'SPA 4 Especialidades',2=>'SPA Vigilancia de la salud',3=>'SPA Especialides técnicas',4=>'Otras actuaciones');

	$whereEjercicio=defineWhereEjercicio('',array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaMail'));
	$where.=str_replace('WHERE',' AND (',$whereEjercicio).' OR (visitada="NO" OR planPrev="NO" OR pap="NO" OR info="NO" OR memoria="NO" OR vs="NO")';

	if($_SESSION['ejercicio']!='' && $_SESSION['ejercicio']!='Todos'){
		$where.='AND YEAR(f.fecha)<='.$_SESSION['ejercicio'].')';
	}

	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FACTURACION'){
		$where.=' AND codigoUsuarioTecnico='.$_SESSION['codigoU'];
	}
	conexionBD();
	$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, visitada, planPrev, pap, info, f.horasPrevistas, memoria, fechaVisita, fechaPlanPrev, fechaPap, fechaInfo,  
						  fechaMemoria, vs, mesVS, anioVS, codigoUsuarioTecnico, CONCAT(EMPNOMBRE,' / ',EMPMARCA) AS cliente,f.codigoContrato, ofertas.codigoCliente, formulario, opcion, 
						  suspendido, (ofertas.total-ofertas.importe_iva) AS importe, incrementoIPC, incrementoIpcAplicado
						  FROM formularioPRL f 
						  LEFT JOIN contratos ON f.codigoContrato=contratos.codigo 
						  LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo 
						  LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
						  ".$where." AND f.eliminado='NO' AND f.aprobado='SI' AND f.visible='SI';");


	/*if($_SESSION['tipoUsuario'] == 'ADMIN'){
		$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, c.razon_s AS cliente FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo ORDER BY c.razon_s;");
	} else {
		$director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],true, true);
		if($director['director'] == 'SI'){
			$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, c.razon_s AS cliente FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE c.empleado IN (SELECT codigo FROM usuarios WHERE tipo LIKE '".$director['tipo']."') ORDER BY c.razon_s;");
		} else {
			$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, c.razon_s AS cliente FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE c.empleado LIKE ".$_SESSION['codigoU']." ORDER BY c.razon_s;");
		}
	}*/
	
	$tabla='';
	$iconoValidado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		$suspendido=$datos['suspendido']=='SI'?'<i class="icon-exclamation iconoFactura icon-danger animated infinite flash"></i> ':'';
		$referencia='';
		
		if($datos['codigoContrato']!=NULL){
			$contrato=consultaBD('SELECT * FROM contratos WHERE codigo="'.$datos['codigoContrato'].'";',false,true);
			$cliente=consultaBD('SELECT * FROM clientes WHERE codigo="'.$datos['codigoCliente'].'";',false,true);
			$referencia=formateaReferenciaContrato($cliente,$contrato);
		}
		
		$visitada=consultaBD('SELECT * FROM planificacion_visitas WHERE codigoFormulario='.$datos['codigo'],false,true);
		
		if($visitada){
			if($visitada['fechaReal']=='' || $visitada['fechaReal']=='0000-00-00'){
				$datos['visitada']='NO';
			} else{
				$datos['visitada']='SI';
			}
		}
		$tabla.= "
			<tr>
				<td>".$referencia."</td>
				<td>".$tipos[$datos['opcion']]."</td>
				<td>".$suspendido.$datos['cliente']."</td>
				<td class='centro'>".$iconoValidado[$datos['visitada']];
				if($datos['visitada'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaVisita']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['planPrev']];
				if($datos['planPrev'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaPlanPrev']);
                }
                $tabla.="</td>		
				<td class='centro'>".$iconoValidado[$datos['pap']];
				if($datos['pap'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaPap']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['info']];
				if($datos['info'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaInfo']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['memoria']];
				if($datos['memoria'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaMemoria']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['vs']];
				if($datos['vs'] == 'NO'){
                    if($datos['mesVS']!='00' && $datos['anioVS']!='00'){
                    	$tabla.="<br/>".obtieneFechaLimite($datos['anioVS'].'-'.$datos['mesVS'].'-01');
                	}
                }
                $tabla.="</td>	
				<td class='centro'>".$datos['horasPrevistas'];
                $tabla.="</td>	
                <td> ".compruebaImporteContrato($datos['importe'],$datos)." € </td>				 
				<td class='centro'>
					<div class='btn-group'>
					<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
					<ul class='dropdown-menu' role='menu'>
						<li><a href='".$_CONFIG['raiz']."planificacion/gestion.php?codigo=".$datos['codigo']."' ><i class='icon-search-plus'></i> Detalles</a></li>
						<li class='divider'></li>
						<li><a href='".$_CONFIG['raiz']."planificacion/generaMemoriaExcel.php?codigo=".$datos['codigo']."' class='noAjax' ><i class='icon-cloud-download'></i> Memoria</a></li>
					</ul>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	cierraBD();
	echo $tabla;
}

function imprimeFormularioPRLeliminados($where=false){
	global $_CONFIG;

	$where=defineWhereEjercicio($where,array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaMail'));
	if($_SESSION['tipoUsuario']!='ADMIN'){
		$where.=' AND codigoUsuarioTecnico='.$_SESSION['codigoU'];
	}
	conexionBD();
	$consulta=consultaBD("SELECT f.codigo AS codigo, f.fecha AS fecha, visitada, planPrev, pap, info, f.horasPrevistas, memoria, fechaVisita, fechaPlanPrev, fechaPap, fechaInfo,  fechaMemoria, vs, mesVS, anioVS, codigoUsuarioTecnico, EMPMARCA AS cliente,f.codigoContrato, ofertas.codigoCliente FROM formularioPRL f LEFT JOIN contratos ON f.codigoContrato=contratos.codigo LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND f.eliminado='SI' AND f.aprobado='SI';");
	
	$tabla='';
	$iconoValidado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		
		$referencia='';
		if($datos['codigoContrato']!=NULL){
			$contrato=consultaBD('SELECT * FROM contratos WHERE codigo="'.$datos['codigoContrato'].'";',false,true);
			$cliente=consultaBD('SELECT * FROM clientes WHERE codigo="'.$datos['codigoCliente'].'";',false,true);
			$referencia=formateaReferenciaContrato($cliente,$contrato);
		}
		$tabla.= "
			<tr>
				<td>".$referencia."</td>
				<td>".$datos['cliente']."</td>
				<td class='centro'>".$iconoValidado[$datos['visitada']];
				if($datos['visitada'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaVisita']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['planPrev']];
				if($datos['planPrev'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaPlanPrev']);
                }
                $tabla.="</td>		
				<td class='centro'>".$iconoValidado[$datos['pap']];
				if($datos['pap'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaPap']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['info']];
				if($datos['info'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaInfo']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['memoria']];
				if($datos['memoria'] == 'NO'){
                    $tabla.="<br/>".obtieneFechaLimite($datos['fechaMemoria']);
                }
                $tabla.="</td>	
				<td class='centro'>".$iconoValidado[$datos['vs']];
				if($datos['vs'] == 'NO'){
                    if($datos['mesVS']!='00' && $datos['anioVS']!='00'){
                    	$tabla.="<br/>".obtieneFechaLimite($datos['anioVS'].'-'.$datos['mesVS'].'-01');
                	}
                }
                $tabla.="</td>	
				<td class='centro'>".$datos['horasPrevistas'];
                $tabla.="</td>					 
				<td class='centro'>
					<a class='btn btn-propio' href='".$_CONFIG['raiz']."planificacion/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	echo $tabla;
	cierraBD();
	
}




function obtieneCliente(){
	$datos=arrayFormulario();//Recibirá el codigo como si vienera de un campo del formulario
	$cliente=consultaBD("SELECT * FROM clientes WHERE codigo=".$datos['codigo'],true,true);

	echo json_encode($cliente);//IMPORTANTE: un echo, no un return, y convierte el array en formato JSON.
}

function estadisticasFormulariosRestrict($where){
	$res=array();

	conexionBD();
	$whereEjercicio=defineWhereEjercicio('',array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaMail'));
	$where.=str_replace('WHERE',' AND (',$whereEjercicio).' OR (visitada="NO" OR planPrev="NO" OR pap="NO" OR info="NO" OR memoria="NO" OR vs="NO")';

	if($_SESSION['ejercicio']!='' && $_SESSION['ejercicio']!='Todos'){
	 	$where.='AND YEAR(f.fecha)<='.$_SESSION['ejercicio'].')';
	}

	if($_SESSION['tipoUsuario']!='ADMIN' && $_SESSION['tipoUsuario']!='FACTURACION'){
		$where.=' AND codigoUsuarioTecnico='.$_SESSION['codigoU'];
	}
	$consulta=consultaBD("SELECT COUNT(f.codigo) AS total FROM formularioPRL f LEFT JOIN contratos ON f.codigoContrato=contratos.codigo LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND f.aprobado='SI' AND f.eliminado='NO' AND f.visible='SI' ORDER BY EMPNOMBRE;",false,true);

	/*if($_SESSION['tipoUsuario'] == 'ADMIN'){
		$consulta=consultaBD("SELECT COUNT(f.codigo) AS total FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo ORDER BY c.razon_s;",false,true);
	} else {
		$director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],false, true);
		if($director['director'] == 'SI'){
			$consulta=consultaBD("SELECT COUNT(f.codigo) AS total FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE c.empleado IN (SELECT codigo FROM usuarios WHERE tipo LIKE '".$director['tipo']."') ORDER BY c.razon_s;",false,true);
		} else {
			$consulta=consultaBD("SELECT COUNT(f.codigo) AS total FROM formularioPRL f INNER JOIN clientes c ON f.codigoCliente = c.codigo WHERE c.empleado LIKE ".$_SESSION['codigoU']." ORDER BY c.razon_s;",false,true);
		}
	}*/

	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

function estadisticasPlanificacionesEliminadas($eliminado){
	$res=array();

	conexionBD();
	$where=defineWhereEjercicio("WHERE f.eliminado='$eliminado' AND f.aprobado='SI'",array('f.fecha','fechaAprobada','fechaVisita','fechaPlanPrev','fechaPap','fechaInfo','fechaMemoria','fechaMail'));
	if($_SESSION['tipoUsuario']!='ADMIN'){
		$where.=' AND codigoUsuarioTecnico='.$_SESSION['codigoU'];
	}
	$consulta=consultaBD("SELECT COUNT(f.codigo) AS total FROM formularioPRL f LEFT JOIN contratos ON f.codigoContrato=contratos.codigo LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." ORDER BY EMPNOMBRE;",false,true);


	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

function generaNumero(){
	$consulta=consultaBD("SELECT MAX(codigoInterno) AS numero FROM formularioPRL", true, true);
	
	return $consulta['numero']+1;
}

function generaEventosAgenda(){	
	conexionBD();
	$consulta=consultaBD("SELECT * FROM formularioPRL",true);
	$tareas=array();
	$i=0;
	while($prl=mysql_fetch_assoc($consulta)){
		$cliente=consultaBD('SELECT clientes.codigo FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='.$prl['codigoContrato'],true,true);
		if($prl['visitada']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaVisita'];
			$tareas[$i]['tarea']='Visita';
			$i++;
		}
		if($prl['planPrev']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaPlanPrev'];
			$tareas[$i]['tarea']='Plan prevención';
			$i++;
		}
		if($prl['er']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaEr'];
			$tareas[$i]['tarea']='Evaluación de riesgos';
			$i++;
		}
		if($prl['pap']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaPap'];
			$tareas[$i]['tarea']='Planificación preventiva';
			$i++;
		}
		if($prl['info']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaInfo'];
			$tareas[$i]['tarea']='Información a los trabajadores';
			$i++;
		}
		if($prl['cert']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaCert'];
			$tareas[$i]['tarea']='Certificados de formación';
			$i++;
		}
		if($prl['memoria']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaMemoria'];
			$tareas[$i]['tarea']='Memoria';
			$i++;
		}
		if($prl['vs']=='NO'){
			$tareas[$i]['codigoInterno']=$prl['codigoInterno'];
			$tareas[$i]['codigoCliente']=$cliente['codigo'];
			$tareas[$i]['fecha']=$prl['fechaVs'];
			$tareas[$i]['tarea']='Vigilancia de la salud';
			$i++;
		}
	}
	$calendario="";
	
	foreach ($tareas as $datos) {
		$cliente=datosRegistro('clientes',$datos['codigoCliente']);
		$fechaInicio=explode('-',$datos['fecha']);
		$fechaFin=explode('-',$datos['fecha']);
		$fechaInicio[1]--;
		$fechaFin[1]--;
		$horaInicio=explode(':','08:00');
		$horaFin=explode(':','08:00');

	    $calendario.="
	    	{
	    		id: ".$datos['codigoInterno'].",
	    		title: '".$cliente['EMPNOMBRE']." - ".$datos['tarea']."',
			    start: new Date(".$fechaInicio[0].", ".$fechaInicio[1].", ".$fechaInicio[2].", ".$horaInicio[0].", ".$horaInicio[1]."),
		    	end: new Date(".$fechaFin[0].", ".$fechaFin[1].", ".$fechaFin[2].", ".$horaFin[0].", ".$horaFin[1]."),
		    	allDay: true,
		    	backgroundColor: '#009F93', 
		    	borderColor: '#009F93',
		    	codigoGestion: 'NO'
	    	},";
  	}

  	cierraBD();

  	$tareas=substr_replace($calendario, '', strlen($calendario)-1, strlen($calendario));//Para quitar última coma  	
  	echo $tareas;
}

function obtieneFechaLimiteAJAX($fecha,$suma){
	$fecha = formateaFechaBD($fecha);
	$fecha = new DateTime($fecha);
	$fecha->add(new DateInterval($suma));
	return $fecha->format('d/m/Y');
}

function tablaFicheros($datos,$num=1){
	if($num==1){
		$tabla='tablaDocumentos1';
		$bd='documentos_plan_prev';
		$prefijo='plan';
	} else {
		$tabla='tablaDocumentos2';
		$bd='documentos_info';
		$prefijo='info';
	}
	echo"
	 		<center>
	 		
				<table class='table table-bordered mitadAncho' id='".$tabla."'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Nombre </th>
							<th> Fichero </th>
							<!--th> Eliminar </th-->
                    	</tr>
                  	</thead>
                  	<tbody>";

					    $i=0;
						if($datos){
							$consulta=consultaBD("SELECT * FROM ".$bd." WHERE codigoFormulario=".$datos['codigo'],true);				  
							while($fichero=mysql_fetch_assoc($consulta)){
							 	echo "<tr>";
							 		campoOculto($fichero['codigo'],'codigoFichero_'.$prefijo.$i);
									campoTextoTabla('nombreSubido_'.$prefijo.$i,$fichero['nombre']);
									campoFichero('ficheroSubido_'.$prefijo.$i,'',1,$fichero['fichero'],'../documentos/planificacion/','Ver/descargar');
									//echo "<td><input type='checkbox' name='eliminaFichero_".$prefijo.$i."' value='".$fichero['codigo']."'></td>";
								echo"
									</tr>";
								$i++;
							}
						}
						campoOculto($i,'ficherosTotales_'.$prefijo);
						$i=0;
						echo "<tr>";
							campoTextoTabla('nombre_'.$prefijo.'0');
							campoFichero('fichero_'.$prefijo.'0','Documento',1);
							//echo "<td></td>";
						echo "</tr>";

						echo "
					</tbody>
	                </table>
	            

					<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"".$tabla."\");'><i class='icon-plus'></i> Añadir documento</button>
				</center><br />";
}

function eliminaPlanificacion(){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();

    for($i=0;isset($datos['codigo'.$i]);$i++){
		$imagenes=consultaBD("SELECT * FROM documentos_plan_prev WHERE codigoFormulario='".$datos['codigo'.$i]."';");
		while($imagen=mysql_fetch_assoc($imagenes)){
        	if($imagen['fichero'] != '' && $imagen['fichero'] != 'NO'){
            	unlink('ficheros/'.$imagen['fichero']);
        	}
    	}
    	$imagenes=consultaBD("SELECT * FROM documentos_info WHERE codigoFormulario='".$datos['codigo'.$i]."';");
		while($imagen=mysql_fetch_assoc($imagenes)){
        	if($imagen['fichero'] != '' && $imagen['fichero'] != 'NO'){
            	unlink('ficheros/'.$imagen['fichero']);
        	}
    	}
        $consulta=consultaBD("DELETE FROM formularioPRL WHERE codigo='".$datos['codigo'.$i]."';");
        if(!$consulta){
            $res=false;
            echo mysql_error();
        }
    }
    cierraBD();

    return $res;
}


function compruebaCambioDatosContrato(){
	global $_CONFIG;
	$res=true;
	$datos=arrayFormulario();

	if($datos['pregunta6']=='SI' && $datos['pregunta6Anterior']=='NO'){//Se cambia por primera vez...

		$mensaje="El motivo de este mensaje es informarle que se han modificado los datos del contrato para el cliente cliente ".$datos['pregunta0']." en su toma de 
				  datos del apartado Planificación (https://crmparapymes.com.es".$_CONFIG['raiz'].").<br><br/>
				  En las observaciones de la misma se ha indicado lo siguiente:<br /><br />
				  <strong><i>".nl2br($datos['pregunta16'])."</i></strong><br /><br />
				  Este mensaje se ha generado automáticamente desde el software. Por favor, no lo responda.";


		$res=enviaEmail('info@anescoprl.es', 'Cambio de información de contrato en Toma de Datos - Anesco', $mensaje);
	}

	return $res;
}



function creaTablaInstalaciones($datos,$codigoCentro){
	    echo "
	    <div class='control-group'>                     
	        <label class='control-label'>Enumeración de instalaciones:</label>
	        <div class='controls'>
	            <table class='tabla-simple mitadAncho' id='tablaInstalaciones".$codigoCentro."'>
	                <thead>
	                    <tr>
	                        <th> Instalaciones </th>
	                        <th> </th>
	                    </tr>
	                </thead>
	                <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos){
	                $consulta=consultaBD("SELECT codigoInstalacion FROM instalaciones_formulario_prl WHERE codigoFormularioPRL='".$datos['codigo']."' AND codigoCentroTrabajo='$codigoCentro';");
	                while($instalacion=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaInstalaciones($i,$instalacion,$codigoCentro);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaInstalaciones(0,false,$codigoCentro);
	            }

	            cierraBD();
	      
	    echo "      </tbody>
	            </table>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaInstalaciones".$codigoCentro."\");'><i class='icon-plus'></i> Añadir instalación</button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaInstalaciones".$codigoCentro."\");'><i class='icon-trash'></i> Eliminar instalación</button>
	        </div>
	    </div>";
}

function imprimeLineaTablaInstalaciones($i,$datos,$codigoCentro){
	$j=$i+1;

    echo "<tr>";
        campoSelectConsulta('codigoInstalacion'.$codigoCentro.'_'.$i,'',"SELECT codigo, nombre AS texto FROM instalaciones ORDER BY nombre;",$datos['codigoInstalacion'],'selectpicker span4 show-tick','data-live-search="true"','',1,false);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function campoUsuarioTecnicoPlanificacion($datos){
	if($_SESSION['tipoUsuario']=='ADMIN'){
		campoSelectConsulta('codigoUsuarioTecnico','Técnico/Enfermera','SELECT codigo, CONCAT(nombre, " ", apellidos,IF(habilitado="NO"," (Inactivo)","")) AS texto FROM usuarios WHERE tipo IN("TECNICO","ENFERMERIA","MEDICO") AND (habilitado="SI" OR codigo="'.$datos['codigoUsuarioTecnico'].'");',$datos);
	}
	else{
		campoOculto($datos,'codigoUsuarioTecnico',$_SESSION['codigoU']);
	}
}


function creaPestaniasFormularioPRL(){
	$codigoFormulario=$_GET['codigoPRL'];
	$res=array();
	$paginas=array('<i class="icon-file-text-o"></i> Datos generales');

	$consulta=consultaBD("SELECT clientes_centros.codigo, direccion 
						  FROM clientes_centros INNER JOIN ofertas ON clientes_centros.codigoCliente=ofertas.codigoCliente
						  INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
						  INNER JOIN formularioPRL ON contratos.codigo=formularioPRL.codigoContrato
						  WHERE direccion!='' AND formularioPRL.codigo=$codigoFormulario",true);

	while($centro=mysql_fetch_assoc($consulta)){
		array_push($paginas,"<i class='icon-building-o'></i> Centro en ".$centro['direccion']);
		array_push($res,$centro['codigo']);
	}

	creaPestaniasAPI($paginas);

	return $res;
}

function generaPDF($codigo){
	global $_CONFIG;

	$datos=consultaBD("SELECT formularioPRL.codigo, formularioPRL.fecha, clientes.codigo AS codigoCliente, formularioPRL.formulario, clientes.EMPPC, clientes.EMPANEXO, clientes.EMPRL, clientes.EMPRLDNI, ofertas.opcion, ofertas.especialidadTecnica, ofertas.otraOpcion, clientes.EMPNOMBRE, ficheroLogo, ficheroOrganigrama, codigoUsuarioTecnico, clientes.otrasActividades, clientes.EMPACTIVIDAD, ficheroEstructura, clientes.EMPMARCA
					   FROM formularioPRL 
					   INNER JOIN contratos ON formularioPRL.codigoContrato=contratos.codigo
					   INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
					   INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
					   LEFT JOIN actividades ON clientes.EMPACTIVIDAD=actividades.codigo
					   WHERE formularioPRL.codigo='$codigo';",true,true);
	$datos['especialidadTecnica']=explode('&{}&', $datos['especialidadTecnica']);
	$arrayCNAE=arrayCNAE();
	$direccionesCentros='';
	$centros=consultaBD('SELECT * FROM clientes_centros WHERE codigoCliente='.$datos['codigoCliente'],true);
	while($centro=mysql_fetch_assoc($centros)){
		if($direccionesCentros==''){
			$direccionesCentros='- '.$centro['direccion'];
		} else {
			$direccionesCentros.='<br/>- '.$centro['direccion'];
		}
	}
	$centros=consultaBD('SELECT * FROM clientes_centros WHERE codigoCliente='.$datos['codigoCliente'],true);
	
	obtienePreguntasFormularioPDF($datos);

    $actividad=datosRegistro('actividades',$datos['pregunta4']);
    $puestos=consultaBD('SELECT nombre, seccionPuestoTrabajo AS zonaTrabajo, tareas  FROM puestos_trabajo_formulario_prl INNER JOIN puestos_trabajo ON puestos_trabajo_formulario_prl.codigoPuestoTrabajo=puestos_trabajo.codigo WHERE codigoFormularioPRL='.$datos['codigo'].' GROUP BY puestos_trabajo.codigo ORDER BY nombre',true);
    $logoCliente=$datos['EMPNOMBRE'];
	if($datos['ficheroLogo']!='' && $datos['ficheroLogo']!='NO'){
		$logoCliente="<img src='../clientes/imagenes/".$datos['ficheroLogo']."' />";
	} else {
		$logoCliente=$datos['EMPNOMBRE'];
	}

	if($datos['ficheroOrganigrama']!='' && $datos['ficheroOrganigrama']!='NO'){
		$organigrama="<img class='organigrama' src='ficheros/".$datos['ficheroOrganigrama']."' />";
	} else {
		$organigrama="ORGANIGRAMA NO DISPONIBLE";
		//$organigrama="<img class='organigrama' src='../img/organigramaPRL.png' alt='Organigrama' />";
	}
	if($datos['ficheroEstructura']!='' && $datos['ficheroEstructura']!='NO'){
		$estructura="<img class='Estructura' src='ficheros/".$datos['ficheroEstructura']."' />";
	} else {
		$estructura="ESTRUCTURA PREVENTIVA NO DISPONIBLE";
		//$organigrama="<img class='organigrama' src='../img/organigramaPRL.png' alt='Organigrama' />";
	}
	$organo='Actualmente no existen órganos de representación';
	if($datos['pregunta17']=='SI'){
		$organo='Existen órganos de representación';
	}
	$tecnico=datosRegistro('usuarios',$datos['codigoUsuarioTecnico']);
	$firmasCodigo=array(145,146,150);
	$firmas=array(145=>'firmaAdan.png',146=>'firmaAngeles.png',150=>'firmaMarta.png');
	if(in_array($tecnico['codigo'], $firmasCodigo)){
		$firma='<img style="width:150px" src="../img/'.$firmas[$tecnico['codigo']].'" />';
	}  else {
		$firma='';
	}
	$datos['fecha']=date('Y-m-d');
    $contenido = "
	<style type='text/css'>
	<!--
			html{
				margin:0px;
				padding:0px;
				width:100%;
			}
	        body{
	            font-size:12px;
	            font-family: Calibri;
	            font-weight: lighter;
	            line-height: 24px;
	            margin:0px;
				padding:0px;
				width:100%;

	        }

	        .portada{
	        	border:2px solid #02215c;
	        	height:95%;
	        	margin:20px;
	        }

	        .titlePortada{
	        	text-align:center;
	        	font-size:56px;
	        	color:#02215c;
	        	font-weight:bold;
	        	margin-top:240px;
	        	line-height:64px;
	        }

	        .imagenFondo{
	        	position:absolute;
	        	top:10%;
	        	z-index:1;
	        	left:-3%
	        }

	        .imagenFondo img{
	        	width:103%;
	        	position:absolute;
	        	display:inline-block;
	        }

	        .imagenFondo2{
	        	position:absolute;
	        	z-index:1;
	        	top:-20px;
	        	left:-30px;
	        }

	        .imagenFondo2 img{
	        	width:104%;
	        	height:100%;
	        	display:inline-block;
	        }

	       .imagenFondo3{
	        	position:absolute;
	        	z-index:1;
	        	top:-133px;
	        	left:-205px;
	        }

	        .imagenFondo3 img{
	        	width:104%;
	        	height:100%;
	        	width:800px;
	        	display:inline-block;
	        }

	        .nombreEmpresa{
	        	font-size:36px;
	        	color:#02215c;
	        	text-align:center;
	        	margin-top:180px;
	        	width:100%;
	        	line-height:48px;
	        }

	        .nombreComercial{
	        	font-size:26px;
	        	margin-top:0px;
	        }

	        .cabecera{
	        	border-top:1px solid #02215c;
	        	border-bottom:1px solid #02215c;
	        	padding:0px;
	        	margin-bottom:200px;
	        	width:100%;
	        }

	        .cabecera .a20{
	        	width:20%;
	        	text-align:center;
	        }

	        .cabecera .a60{
	        	width:60%;
	        	color:#02215c;
	        	text-align:center;
	        	font-weight:bold;
	        	font-size:18px;
	        }

	        .cabecera img{
	        	width:58%;
	        }

	        .cabecera .tituloCabecera{
	        	color:#02215c;
	        	text-align:center;
	        	font-weight:bold;
	        	margin-left:140px;
	        	font-size:18px;
	        	margin-top:-25px;
	        }

	        .cabecera .logoCliente{
	        	position:absolute;
	        	right:0px;
	        	top:0px;
	        	background:red;
	        	width:100px;
	        }

	        .cajaRevision{
	        	color:#02215c;
	        	margin-left:80px;
	        }

	        .cajaNumeroPagina{
	        	color:#02215c;
	        	position:absolute;
	        	right:80px;
	        	bottom:0px;	
	        }

	        .tituloIndice{
	        	text-align:center;
	        	font-size:20px;
	        	margin-top:20px;
	        	margin-bottom:20px;
	        	color:#02215c;
	        }

	        .indice{
	        	margin-left:80px;
	        }


	        .indice td{
	        	font-size:14px;
	        	color:#02215c;
	        	padding-top:35px;
	        }

	        .indice .texto{
	        	font-weight:bold;
	        }

	        .indice .sub{
	        	font-weight:normal;
	        	padding-left:10px;
	        }

	        .indice .pagina{
	        	font-weight:bold;
	        }

	        .container{
	        	width:75%;
	        	margin-left:90px;
	        }

	        .container2{
	        	border:1px solid #02215c;
	        	padding:5px;
	        	margin-top:20px;
	        }

	        .tituloSeccion{
	        	font-size:14px;
	        	color:#02215c;
	        	font-weight:bold;
	        }

	        p, li {
	        	text-align:justify;
	        	font-size:11px;
	        	line-height:22px;
	        }

	        li{
	        	padding-bottom:18px;
	        }

	        .tituloIndice{
	        	text-align:center;
	        	font-size:20px;
	        	margin-top:20px;
	        	margin-bottom:20px;
	        	color:#02215c;
	        }

	        .tablaDatos{
	        	width:100%;
	        	border:1px solid #000;
	        	border-collapse:collapse;
	        }

	        .tablaDatos th,
	        .tablaDatos td{
	        	border:1px solid #000;
	        	font-size:11px;
	        	padding-top:3px;
	        	padding-bottom:3px;
	        	padding-left:8px;
	        }

	        .tablaDatos th{
	        	background:#02215c;
	        	font-weight:bold;
	        	color:#FFF;
	        }

	        .tablaDatos .a10{
	        	width:10%;
	        }

	        .tablaDatos .a15{
	        	width:15%;
	        }

	        .tablaDatos .a20{
	        	width:20%;
	        }

	        .tablaDatos .a25{
	        	width:25%;
	        }

	        .tablaDatos .a30{
	        	width:30%;
	        }

	        .tablaDatos .a33,
	        .noBorde .a33{
	        	width:33%;
	        }

	        .tablaDatos .a35{
	        	width:35%;
	        }

	        .tablaDatos .a45{
	        	width:45%;
	        }

	        .tablaDatos .a65{
	        	width:65%;
	        }

	        .tablaDatos .a70{
	        	width:70%;
	        }

	        .tablaDatos .a85{
	        	width:85%;
	        }

	        .tablaDatos .a100{
	        	width:100%;
	        }

	        .tablaDatos .centro{
	        	text-align:center;
	        }

	        .desconocido{
	        	color:red;
	        	font-weight:bold;
	        }

	        .noBorde{
	        	width:100%;
	        	border:none;
	        }

	        .noBorde th,
	        .noBorde td{
	        	font-size:11px;
	        	padding-top:3px;
	        	padding-bottom:3px;
	        	padding-left:8px;
	        	font-weight:bold;
	        	height:15px;
	        	border:none;
	        	border-collapse:collapse;
	        }

	        .noBorde th{
	        	background:#02215c;
	        	font-weight:bold;
	        	color:#FFF;
	        }

	        .noBorde .tdGris{
	        	background:#BBB;
	        }

	        p.tituloIndice{
	        	text-align:center;
	        	font-size:20px;
	        	margin-top:40px;
	        	margin-bottom:20px;
	        	color:#02215c;
	        	font-weight:bold;
	        }

	        .organigrama{
	        	width:80%;
	        }
	-->
	</style>
	

	<page backbottom='0mm' backleft='0mm' backright='0mm' >
		<div class='imagenFondo'><img src='../img/fondoOferta.jpeg' alt='Anesco' /></div>
		<div class='portada'>
			<div class='titlePortada'>PLAN DE<br/>PREVENCIÓN</div>

			<div class='nombreEmpresa'>".$datos['EMPNOMBRE']."</div>
			<div class='nombreEmpresa nombreComercial'>".$datos['EMPMARCA']."</div>
			<div class='container'>
			<table class='noBorde'>
				<tr>	
					<th class='a33'>ELABORADO POR:</th>
					<th class='a33'>REVISIÓN:</th>
					<th class='a33'>APROBADO:</th>
				</tr>
				<tr>
					<td class='a33'>".$tecnico['nombre']." ".$tecnico['apellidos']."</td>
					<td class='a33'></td>
					<td class='a33'></td>
				</tr>
				<tr>
					<td class='a33'>".$firma."</td>
					<td class='a33'></td>
					<td class='a33'></td>
				</tr>
				<tr>
					<td class='a33 tdGris'>TÉCNICO SUPERIOR EN PREVENCIÓN DE RIESGOS LABORALES</td>
					<td class='a33 tdGris'></td>
					<td class='a33 tdGris'>REPRESENTANTE LEGAL:</td>
				</tr>
				<tr>
					<td class='a33 tdGris'></td>
					<td class='a33 tdGris'></td>
					<td class='a33 tdGris'></td>
				</tr>
				<tr>
					<td class='a33 tdGris'>FECHA: ".formateaFechaWeb($datos['fecha'])."</td>
					<td class='a33 tdGris'>FECHA</td>
					<td class='a33 tdGris'>FECHA</td>
				</tr>
				<tr>
					<td class='a33 tdGris'></td>
					<td class='a33 tdGris'></td>
					<td class='a33 tdGris'></td>
				</tr>
				<tr>
					<td class='a33'>ANESCO SALUD Y PREVENCIÓN, S.L.</td>
					<td class='a33'></td>
					<td class='a33'></td>
				</tr>
			</table>
			</div>
		</div>

	</page>";

$contenido.="
	<page backtop='35mm' backleft='25mm' backright='25mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
	</page>

	<page footer='page' backtop='35mm' backleft='25mm' backright='25mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div>
		<bookmark title='1. INTRODUCCIÓN' level='0' ></bookmark>
		<h1 class='tituloSeccion'>1. INTRODUCCIÓN</h1>
		<p>La <b>Ley 31/1995</b>, de 8 de noviembre, de Prevención de Riesgos Laborales y el <b>Real Decreto 39/1997</b>, de 17 de enero, Reglamento de los Servicios de Prevención constituyen los pilares fundamentales dentro del marco normativo que regula actualmente la seguridad y la salud en el trabajo. La aplicación de la Ley de Prevención de Riesgos laborales persigue no sólo la ordenación de las obligaciones y responsabilidades de los actores inmediatamente relacionados con el hecho laboral, sino fomentar una nueva <b>cultura de la prevención.</b><br/><br/>

La entrada en vigor de la <b>Ley 54/2003</b>, de 12 de diciembre, de reforma del marco normativo de la prevención de riesgos laborales así como el <b>RD 604/2006</b>, de 19 de mayo, por el que se modifican el RD 39/1997 y el RD 1627/1997, de 24 de octubre, por el que se establecen las disposiciones mínimas de seguridad y salud en las obras de construcción, da un nuevo enfoque con objeto de combatir de manera efectiva la siniestralidad laboral y fomentar una auténtica cultura de la prevención de los riesgos en el trabajo, que asegure el cumplimiento efectivo y real de las obligaciones. <br/><br/>

A través de la implantación y aplicación de un <b>Plan de Prevención de Riesgos Laborales</b>, los empresarios desarrollaran la <b>integración de la actividad preventiva</b> en el sistema de gestión de su empresa, comprendiendo tanto los niveles jerárquicos como el conjunto de todas las actividades
De este modo, el objeto de este informe es servir de herramienta a través de la cual la empresa pueda integrar la Prevención en el Sistema General de Gestión en todos los niveles jerárquicos de la empresa, con el fin de asegurar una protección eficaz del trabajador frente a los riesgos laborales presentes en el desarrollo de su trabajo.<br/><br/>

En este sentido el Plan de prevención de riesgos laborales debe ser aprobado por la dirección de la empresa, asumido por toda su estructura organizativa, en particular por todos sus niveles jerárquicos, y conocido por todos sus trabajadores.<br/><br/>

Los trabajadores y sus representantes deberán contribuir a la integración de la prevención de riesgos laborales y colaborar en la adopción y el cumplimiento de las medidas preventivas a través de los mecanismos de que Ley de Prevención de Riesgos Laborales recoge al efecto.<br/><br/>

Dicho Plan habrá de reflejarse en un documento que se conservará a disposición de la autoridad laboral, de las autoridades sanitarias y de los representantes de los trabajadores.
</p>
		</div>
	</page>

	<page footer='page' backtop='35mm' backleft='25mm' backright='25mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div>
		<bookmark title='1.1 IDENTIFICACIÓN DE LA EMPRESA' level='1' ></bookmark>
		<h1 class='tituloSeccion'>1.1 IDENTIFICACIÓN DE LA EMPRESA:</h1>
		
		<table class='tablaDatos'>
			<tr>
				<th class='a35'>NOMBRE EMPRESA</th>
				<td class='a65'>".$datos['pregunta0']."</td>
			</tr>
			<tr>
				<th class='a35'>CIF</th>
				<td class='a65'>".$datos['pregunta1']."</td>
			</tr>
			<tr>
				<th class='a35'>ACTIVIDAD</th>
				<td class='a65'>".$actividad['nombre']."</td>
			</tr>
			<tr>
				<th class='a35'>CNAE</th>
				<td class='a65'>".$datos['pregunta5']."</td>
			</tr>
			<tr>
				<th class='a35'>OTRAS ACTIVIDADES</th>
				<td class='a65'>".$datos['otrasActividades']."</td>
			</tr>
			<tr>
				<th class='a35'>Incluida Anexo I</th>
				<td class='a65'>".ucwords(strtolower($datos['EMPANEXO']))."</td>
			</tr>
			<tr>
				<th class='a35'>DOMICILIO FISCAL</th>
				<td class='a65'>".$datos['pregunta2']."</td>
			</tr>
			<tr>
				<th class='a35'>DOMICILIO DE LOS CENTROS</th>
				<td class='a65'>".$direccionesCentros."</td>
			</tr>
			<tr>
				<th class='a35'>RESPONSABLE</th>
				<td class='a65'>".$datos['EMPPC']."</td>
			</tr>
			<tr>
				<th class='a35'>Nº TRABAJADORES</th>
				<td class='a65'>".$datos['pregunta12']."</td>
			</tr>
		</table>
		<br/><br/>
		".nl2br($datos['pregunta20'])."
		<br/><br/>
		<table class='tablaDatos' style='text-align:justify;'>
			<tr>
				<th colspan='3' class='a100 centro'>PUESTO DE TRABAJO</th>
			</tr>
			<tr>
				<th class='a33 centro'>PUESTO</th>
				<th class='a33 centro'>SECCIÓN/ÁREA</th>
				<th class='a33 centro'>FUNCIONES</th>
			</tr>
		";
		while($puesto=mysql_fetch_assoc($puestos)){
			$contenido.="<tr>
							<td class='a33'>".$puesto['nombre']."</td>
							<td class='a33'>".$puesto['zonaTrabajo']."</td>
							<td class='a33' style='padding-right:5px;'>".nl2br($puesto['tareas'])."</td>
						</tr>";
		}
		$contenido.="
		</table>
		<br/><br/>
		<bookmark title='1.2 DESCRIPCIÓN DE LOS CENTROS DE TRABAJOS' level='1' ></bookmark>
		<h1 class='tituloSeccion'>1.2 DESCRIPCIÓN DE LOS CENTROS DE TRABAJOS</h1>";
		$texto='';
		$total=0;
		$ol='<div style="text-align:justify,font-size:11px,line-height:22px;">';
		while($centro=mysql_fetch_assoc($centros)){
			$total++;
			$ol.=' - '.$centro['direccion'].'<br/>';

			$nombre=$centro['nombre'];
			$texto.="<p><u><b>Centro de trabajo ".$total." ".$nombre."</b></u><br/>
			".$datos['pregunta'.$centro['codigo'].'56']."<br/><br/>
			<u>SERVICIOS HIGIÉNICOS</u><br/>
			".$datos['pregunta'.$centro['codigo'].'65']."</p>";
			$listado=consultaBD('SELECT * FROM higienicos_formulario_prl WHERE codigoCentroTrabajo='.$centro['codigo'],true);
			while($item=mysql_fetch_assoc($listado)){
				$texto.='<p><b>- <u>'.$item['nombre'].':</u></b> '.$item['descripcion'].'</p>';
			}
			$texto.="
			<p><u>ILUMINACIÓN</u><br/>
			".$datos['pregunta'.$centro['codigo'].'66']."</p>";
			$listado=consultaBD('SELECT * FROM iluminacion_formulario_prl WHERE codigoCentroTrabajo='.$centro['codigo'],true);
			while($item=mysql_fetch_assoc($listado)){
				$texto.='<p><b>- <u>'.$item['nombre'].':</u></b> '.$item['descripcion'].'</p>';
			}
			$texto.="<p><u>ILUMINACIÓN DE EMERGENCIA</u><br/>
			".$datos['pregunta'.$centro['codigo'].'67']."</p>
			<p><u>INSTALACIONES</u></p>
			<p><b>Instalación eléctrica</b><br/>
			".$datos['pregunta'.$centro['codigo'].'68']."</p>
			<p><b>Instalación de aire comprimido</b><br/>
			".$datos['pregunta'.$centro['codigo'].'69']."</p>
			<p><b>Instalación de gas - oil</b><br/>
			".$datos['pregunta'.$centro['codigo'].'70']."</p>
			<p>Dimensiones reglamentarias de los lugares de trabajo (R.D. 486/1997):</p>";
			$listado=consultaBD('SELECT * FROM gas_formulario_prl WHERE codigoCentroTrabajo='.$centro['codigo'],true);
			while($item=mysql_fetch_assoc($listado)){
				$texto.='<p style="margin-left:10px;"><b>- '.$item['zona'].':</b></p>';
				$texto.='<p style="margin-left:20px;"> - '.$item['suelo'].' m. suelo - techo<br/>
				 - '.$item['m2'].' m<sup>2</sup>. por trabajador<br/>
				 - '.$item['m3'].' m<sup>3</sup>. por trabajador</p>';
			}
			$texto.="<p><u>MEDIDAS PARA CASOS DE EMERGENCIA Y  EVACUACION</u></p>
			<p>Tal como dispone el articulo 20. de la Ley 31/1995, se preverán las posibles situaciones de emergencia y las medidas necesarias en materia de primeros auxilios, lucha contra incendios y evacuación, estudiándose por tanto dicha materia en los siguientes epígrafes.</p>";
			$listado=consultaBD('SELECT * FROM evacuacion_formulario_prl WHERE codigoCentroTrabajo='.$centro['codigo'].' ORDER BY tipo',true);
			$tiposE=array('','PASILLOS Y SUPERFICIOS DE TRANSITO','PUERTAS Y SALIDAS','SEÑALIZACIONES','VÍAS Y SALIDAS DE EMERGENCIA','EQUIPOS DE EXTINCIÓN DE INCENDIOS','PRIMEROS AUXILIOS');
			$tipoAnt='';
			while($item=mysql_fetch_assoc($listado)){
				$puntos=$item['zona']==''?'':':';
				if($tipoAnt!=$item['tipo']){
					$texto.='<p style="margin-left:10px;"><b>- '.$tiposE[$item['tipo']].'</b></p>';
					$tipoAnt=$item['tipo'];
				}
				$texto.='<p style="margin-left:20px;"><b> - '.$item['zona'].$puntos.'</b> '.$item['descripcion'].'</p>';
			}
		}
		$ol.='</div>';
		if($total > 1){
$contenido.="<p>La empresa cuenta con ".$total." centros de trabajo que se encuentran en:</p>".$ol;
		} else {
$contenido.="<p>La empresa cuenta con ".$total." centro de trabajo que se encuentra en:</p>".$ol;
		}
$contenido.=$texto;
if(in_array($datos['EMPACTIVIDAD'], array(81,56,100,69,103,62,60,79,50,146,33,119,129,92,27,83,82,105,38,35,127,111,110,70,36,101,67,116,120,99))){
$contenido.="
		<bookmark title='1.3 OTRAS ZONAS' level='1' ></bookmark>
		<h1 class='tituloSeccion'>1.3 OTRAS ZONAS</h1>
		<p>Además de los centros / las zonas descritas, los trabajadores de ".$datos['pregunta0']." prestarán sus servicios en zonas de trabajo propias de las obras de construcción y reforma o mantenimiento, por lo que, dada la variabilidad de las mismas, no se describen </p>";
}
$contenido.="</div>
	</page>

	<page footer='page' backtop='35mm' backleft='25mm' backright='25mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div>
		<bookmark title='2. OBJETO' level='0' ></bookmark>
		<h1 class='tituloSeccion'>2. OBJETO</h1>
		<p>Es objeto del presente documento es diseñar el Plan de Prevención de Riesgos Laborales relativo a la empresa <b>".$datos['pregunta0']."</b> en el que se definen la estructura, las responsabilidades, las funciones, las prácticas, los procedimientos, los procesos y los recursos necesarios para conseguir la integración de la prevención de riesgos laborales en el sistema de gestión de la empresa.<br/><br/>

		El presente informe se realiza a instancias de la empresa, de conformidad con el contrato de concierto preventivo con ANESCO PRL, S.L., y para el debido cumplimiento de las exigencias de la legislación vigente, en especial, el artículo 16 de la Ley 31/95 de Prevención de Riesgos Laborales.</p>
		<bookmark title='3. ALCANCE' level='0' ></bookmark>
		<h1 class='tituloSeccion'>3. ALCANCE</h1>
		<p>El alcance del presente documento es de aplicación a todas las actividades desarrolladas por <b>".$datos['pregunta0']."</b>.</p>

		<bookmark title='4. POLÍTICA' level='0' ></bookmark>
		<h1 class='tituloSeccion'>4. POLÍTICA</h1>
		<p>La Política en materia de prevención tiene por objeto la promoción de la mejora de las condiciones de trabajo dirigida a elevar el nivel de protección de la seguridad y salud de los trabajadores.</p>
		</div>
	</page>

	<page footer='page' backtop='35mm' backleft='10mm' backright='10mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div class='container2'>
		<p>La Dirección de  <b>".$datos['pregunta0']."</b> con objeto de asegurar que la calidad de su Servicio, cumpla con los objetivos definidos, comprometiéndose con la de protección del Medio Ambiente y de la Seguridad y Salud de sus trabajadores, ha decidido poner en marcha un Sistema de Gestión de Calidad, Medio ambiente y Seguridad y Salud Laboral, basado en las normas UNE-EN-ISO 9001:2008, UNE EN ISO 14001:2004 y OHSAS 18001:2007.<br/><br/>

			Por ello, y como equipo que formamos, es necesaria que la idea de <b>CALIDAD GENERAL</b> esté presente y sea considerada como un objetivo prioritario en todas las actividades y decisiones, así como una exigencia para todos nuestros colaboradores incluyendo nuestros proveedores, en orden de poder mejorar y cumplir con los objetivos establecidos, siempre cumpliendo las exigencias en materia de Seguridad y Salud Laboral y con el máximo respeto a la protección del medio ambiente. La Dirección de <b>".$datos['pregunta0']."</b>, se responsabiliza de proporcionar los medios necesarios, así como de difundir su conocimiento entre el personal que desarrolla las actividades del mismo para su correcta implantación.<br/><br/>

			La Dirección de <b>".$datos['pregunta0']."</b> establece unos objetivos generales que se exponen a continuación:
			</p>
			<ul>
				<li>Prevenir la aparición de problemas, la contaminación y los daños de los trabajadores, así como el deterioro de su salud. Todo ello unido a un compromiso firme con la mejora continua en todos los ámbitos del sistema (calidad, medioambiente y SST). Para ello se desarrollarán planes de objetivos y de formación que permitan alcanzar dicha mejora y el control y reducción de la siniestralidad.</li>
				<li>Igualmente se establece el compromiso con el cumplimiento de los requisitos establecidos por parte de los clientes, así como los provenientes de la legislación aplicable en cada momento.</li>
				<li>Reducir el número de productos/servicios no conformes.</li>
				<li>Mantener y superar su posición y las expectativas de mercado aplicando todas aquellas nuevas tecnologías que pudieran aparecer y que permitieran mejorar la calidad de los trabajos realizados, el desempeño ambiental y la protección de la salud de los trabajadores.</li>
			</ul>
			<p>
			Todo lo anteriormente descrito permitirá incrementar la reputación de <b>".$datos['pregunta0']."</b>, mejorando su competitividad y alcanzando la plena satisfacción del cliente en la calidad del producto recibido y en el servicio suministrado.<br/><br/>

			La Dirección anima a todo el personal a contribución con sus ideas, pues es consciente de que el éxito de la organización radica no en buscar culpables de los errores, sino las causas que los han originado para corregirlos y prevenir la aparición de los mismos. Es necesarios, por tanto, la motivación del personal y el convencimiento de la importancia de cada una de las funciones para poder dar cumplimiento a nuestros objetivos
		</p>
			<div style='text-align:right;margin-top:10px;margin-top:5px;'>
				Fdo.: ".$datos['EMPRL']."
			</div>
		</div>
		<div class='container'>
		<br/><br/><p><b>Nota: Se dispondrán copias de este documento en los centros de Trabajo donde ".$datos['pregunta0']." realizase actividad. </b></p>
		</div>
	</page>

	<page footer='page' backtop='35mm' backleft='25mm' backright='25mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div>
			<bookmark title='5. OBJETIVOS Y METAS' level='0' ></bookmark>
			<h1 class='tituloSeccion'>5. OBJETIVOS Y METAS</h1>
			<p>Los Objetivos se establecen sobre la base de su pertinencia con las propias metas de la empresa, respetándose las expectativas y necesidades actuales de sus clientes, así como los aspectos medioambientales y de seguridad y salud en el trabajo. La comprensión de estos objetivos por los diferentes estamentos y niveles de la empresa es asegurada mediante una información adecuada y una comunicación constante entre ellos.<br/><br/>

				Los objetivos preventivos que pretende alcanzar <b>".$datos['pregunta0']."</b> a tenor de los principios recogidos en la Política Preventiva son los siguientes:</p>
			<ul>
				<li><b>Objetivos</b> para indicadores de primer nivel: <b>Siniestralidad</b>.<br/>No superar los índices de accidentalidad obtenida en el año anterior.</li>
				<li><b>Objetivos</b> para los indicadores de segundo nivel: <b>Actividad preventiva programada.</b><br/>
				Cumplir con los principios esenciales indicados en la política preventiva de la entidad.<br/>
				Asegurar el cumplimiento de la normativa de aplicación.<br/>
				Llevar a cabo con el grado de cumplimiento más elevado posible, evaluándose en porcentaje de actuaciones realizadas tanto en número como en inversión requerida, de las actividades preventivas programadas</li>
				<li><b>Objetivos</b> para los indicadores de tercer nivel:<b>Revisión de objetivos.</b><br/>Se revisarán en las reuniones periódicas de prevención de riesgos laborales entre los Jefes de equipos y Jefes de Producción con el departamento de Calidad y Prevención.</li>
			</ul>
			<p><b>Véase: Manual de Gestión Integrada de ".$datos['pregunta0'].", apdo. 5.4. Planificación, en su apdo. 4, Planificación y objetivos para la Identificación de peligros, evaluación de riesgos y determinación de controles</b></p>

			<bookmark title='6. RECURSOS' level='0' ></bookmark>
			<h1 class='tituloSeccion'>6. RECURSOS</h1>
			<p>Para alcanzar dichos objetivos <b>".$datos['pregunta0']."</b> pone a disposición de las personas encargadas de gestionar la prevención de riesgos laborales en la empresa los siguientes recursos humanos, técnicos, materiales y económicos de que va a disponer al efecto son:</p>";
			$listados=array();
			$tablas=array();
			$tipos=array(''=>'','PROPIO'=>'Recurso propio','EXTERNO'=>'Recurso externo');
			$recursos=consultaBD('SELECT * FROM recursos_humanos_formulario_prl WHERE codigoFormularioPRL='.$datos['codigo'],true);
			$listados['humanos']='';
			$tablas['humanos']='';
			while($item=mysql_fetch_assoc($recursos)){
				if($listados['humanos']!=''){
					$listados['humanos'].='<br/>';
				}
				$listados['humanos'].='- '.$item['recurso'];
				$tablas['humanos'].="
				<tr>
					<td class='a25'>".$item['recurso']."</td>
					<td class='a25'>".$item['identificacion']."</td>
					<td class='a25'>".$tipos[$item['tipo']]."</td>
					<td class='a25'>".$item['puesto']."</td>
				</tr>";
			}
			if($tablas['humanos']==''){
				$tablas['humanos'].="
				<tr>
					<td class='a25'><br/></td>
					<td class='a25'> </td>
					<td class='a25'> </td>
					<td class='a25'> </td>
				</tr>";
			}
			$tipos=array(''=>'','ESPACIOS'=>'Espacios','TECNICOS'=>'Técnicos','EQUIPOS'=>'Equipos');
			$recursos=consultaBD('SELECT * FROM recursos_tecnicos_formulario_prl WHERE codigoFormularioPRL='.$datos['codigo'].' ORDER BY tipo',true);
			$listados['tecnicos']='';
			$tablas['tecnicos']='';
			$tipoAnt='';
			while($item=mysql_fetch_assoc($recursos)){
				if($listados['tecnicos']!=''){
					$listados['tecnicos'].='<br/>';
				}
				if($tipoAnt!=$item['tipo']){
					$listados['tecnicos'].='- <b>'.$tipos[$item['tipo']].'</b><br/>';
					$tipoAnt=$item['tipo'];
				}
				$listados['tecnicos'].=' '.$item['recurso'];
				$tablas['tecnicos'].="
				<tr>
					<td class='a65'>".$item['recurso']."</td>
					<td class='a35'>".$tipos[$item['tipo']]."</td>
				</tr>";
			}
	$contenido.="
			<p><b>Recursos humanos</b></p>
			<p>".$listados['humanos']."</p>
			<p><b>Recursos Técnicos y materiales</b></p> 
			<p>".$listados['tecnicos']."</p>
			<p>Además, la asignación de los recursos humanos, técnicos, materiales y económicos será objeto de cuantificación y designación en el documento de planificación de la actividad preventiva, que debe planificarse para un periodo determinado, estableciéndose las fases y prioridades de su desarrollo en función de la magnitud de los riesgos. En ella se identificará la actividad adoptada, el plazo para llevarla a cabo, la designación de responsables y los recursos humanos y materiales para su ejecución. </p>

			<p>La Planificación de la actividad preventiva estará contenida en un documento anexo a este Plan.</p>


			<bookmark title='6.1 RECURSOS HUMANOS DE LOS QUE SE DISPONE' level='1' ></bookmark>
			<h1 class='tituloSeccion'>6.1 RECURSOS HUMANOS DE LOS QUE SE DISPONE</h1>
			<table class='tablaDatos'>
				<tr>
					<th class='a25'>RECURSO</th>
					<th class='a25'>IDENTIFICACIÓN</th>
					<th class='a25'>TIPO</th>
					<th class='a25'>PUESTO</th>
				</tr>
				".$tablas['humanos']."
			</table>
			
			<bookmark title='6.2 RECURSOS MATERIALES Y TÉCNICOS DE LOS QUE SE DISPONE' level='1' ></bookmark>
			<h1 class='tituloSeccion'>6.2 RECURSOS MATERIALES Y TÉCNICOS DE LOS QUE SE DISPONE</h1>
			<table class='tablaDatos'>
				<tr>
					<th class='a65'>RECURSO</th>
					<th class='a35'>TIPO</th>
				</tr>
				".$tablas['tecnicos']."
			</table>
			
			<bookmark title='6.3 RECURSOS ECONÓMICOS DE LOS QUE SE DISPONE' level='1' ></bookmark>
			<h1 class='tituloSeccion'>6.3 RECURSOS ECONÓMICOS DE LOS QUE SE DISPONE</h1>";
			
			$tipos=array(''=>'','OBLIGATORIO'=>'Obligatorio','NOOBLIGATORIO'=>'No obligatorio');
			$recursos=consultaBD('SELECT * FROM recursos_economicos_formulario_prl WHERE codigoFormularioPRL='.$datos['codigo'].' ORDER BY tipo',true);
			$total=0;
			
			while($item=mysql_fetch_assoc($recursos)){
				$contenido.="
				<table class='tablaDatos'>
					<tr>
						<th class='a15'>RECURSO:</th>
						<th class='a85'>".$item['recurso']."</th>
					</tr>
					<tr>
						<th class='a15'>TIPO:</th>
						<th class='a85'>".$tipos[$item['tipo']]."</th>
					</tr>
					<tr>
						<td colspan='2' class='a100'>Descripción del recurso económico:<br/><br/>
						".nl2br($item['descripcion'])."</td>
					</tr>
					<tr>
						<td colspan='2' class='a100' style='text-align:right;padding-right:10px;'>Importe asignado: ".formateaNumeroWeb($item['importe'])." euros/año</td>
					</tr>
				</table>";
				
				$total=floatval($total) + floatval($item['importe']);
			}
		
			if($total!=0){
				$contenido=substr($contenido, 0,-7);
				
				$contenido.="
					<tr>
						<td colspan='2' class='a100' style='text-align:right;padding-right:10px;font-weight:bold;'>IMPORTE ASIGNADO TOTAL: ".formateaNumeroWeb($total)." euros/año</td>
					</tr>
				</table>";
			}

			$contenido.="
		</div>
	</page>

	<page footer='page' backtop='35mm' backleft='25mm' backright='25mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div>
			<bookmark title='7. ESTRUCTURA ORGANIZATIVA' level='0' ></bookmark>
			<h1 class='tituloSeccion'>7. ESTRUCTURA ORGANIZATIVA</h1>
			<bookmark title='7.1 ORGANIGRAMA' level='1' ></bookmark>
			<h1 class='tituloSeccion'>7.1 ORGANIGRAMA</h1>
			".$organigrama."
		</div>
	</page>

	<page footer='page' backtop='35mm' backleft='25mm' backright='25mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div>
		<bookmark title='7.2 LISTADO DE ÁREAS DE ACTIVIDAD Y PROCESOS PRODUCTIVOS' level='1' ></bookmark>
		<h1 class='tituloSeccion'>7.2 LISTADO DE ÁREAS DE ACTIVIDAD Y PROCESOS PRODUCTIVOS</h1>
		<p><b>".$datos['pregunta0']."</b> desempeña su actividad en <b>".$actividad['nombre']."</b>.<br/>Los procesos más habituales a desarrollar serán:</p>
		<ul style='list-style-type:square;'>";
		$procesos=consultaBD('SELECT * FROM actividades_procesos WHERE codigoActividad='.$actividad['codigo'],true);
		while($proceso=mysql_fetch_assoc($procesos)){
			$contenido.="<li>".$proceso['nombre'].".</li>";
		}
		$contenido.="
		</ul>
		<bookmark title='8. ORGANIZACIÓN PREVENTIVA' level='0' ></bookmark>
		<h1 class='tituloSeccion'>8. ORGANIZACIÓN PREVENTIVA</h1>
		<p>En la Organización Preventiva de la empresa se incluye la estructura de la empresa con los recursos humanos que dispone para la gestión de la actividad preventiva, incluyendo las funciones de estos recursos.</p>
		</div>
	</page>

	<page footer='page' backtop='35mm' backleft='25mm' backright='25mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div>
		<bookmark title='8.1 ESTRUCTURA PREVENTIVA' level='1' ></bookmark>
		<h1 class='tituloSeccion'>8.1 ESTRUCTURA PREVENTIVA</h1>
		".$estructura."
		<bookmark title='8.2 MODALIDAD PREVENTIVAS' level='1' ></bookmark>
		<h1 class='tituloSeccion'>8.2. MODALIDAD PREVENTIVA</h1>
		<p>La Empresa ha optado por la modalidad preventiva de concertación de la actividad preventiva con el Servicio de Prevención Ajeno <b>ANESCO SALUD Y PREVENCIÓN</b> en las especialidades de: </p>
		<ul style='list-style-type:square;'>";
		if($datos['opcion']==1 || ($datos['opcion']==3 && in_array('Seguridad en el Trabajo', $datos['especialidadTecnica']))){
			$contenido.="<li>Seguridad en el Trabajo.</li>";
		}
		if($datos['opcion']==1 || ($datos['opcion']==3 && in_array('Higiene Industrial', $datos['especialidadTecnica']))){
			$contenido.="<li>Higiene Industrial.</li>";
		}
		if($datos['opcion']==1 || ($datos['opcion']==3 && in_array('Ergonomía y Psicosociología', $datos['especialidadTecnica']))){
			$contenido.="<li>Ergonomía y Psicosociología Aplicada.</li>";
		}
		if($datos['opcion']==1 || $datos['opcion']==2){
			$contenido.="<li>Vigilancia de la Salud.</li>";
		}
		if($datos['opcion']==4){
			$contenido.="<li>".$datos['otraOpcion'].".</li>";
		}
		$contenido.="
		</ul>
		</div>
	</page>

	<page footer='page' backtop='35mm' backleft='25mm' backright='25mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div>
		<p>Como complemento al modelo de organización de la prevención en la empresa y con el fin de favorecer la integración de la actividad preventiva en el seno de la misma, se dispone de las figuras que se indican a continuación:</p>
		<ul style='list-style-type:square;'>
			<li>Interlocutor con el Servicio de Prevención Ajeno.</li>
		</ul>
		<p>Centrados a partir de la política de prevención adoptada existe un <b>modelo bidireccional de comunicación, coordinación y actuación</b> en todos los niveles de la organización</p>
		<img src='../img/modelo_bidireccional.png' alt='Modelo bidireccional' />
		<p>Los distintos cauces de comunicación entre los responsables y miembros de cada uno de los departamentos enumerados en el organigrama arriba indicado quedarán reflejados en los procedimientos de gestión e instrucciones operativas, de manera que se asegure una comunicación adecuada, en reuniones, envíos de escritos, correos electrónicos, comunicación telefónica, etc.<br/><br/>

		La comunicación entre <b>".$datos['pregunta0']."</b> y <b>ANESCO SALUD Y PREVENCIÓN</b> se hará por los cauces habituales de comunicación telefónica, correo electrónico, etc. cuyos números han sido facilitados por el Servicio de Prevención. <b>Comunicación SPA Ajeno.</b></p>
		</div>
	</page>

	<page footer='page' backtop='35mm' backleft='25mm' backright='25mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div>
		<bookmark title='8.3 ÓRGANOS DE REPRESENTACIÓN' level='1' ></bookmark>
		<h1 class='tituloSeccion'>8.3. ÓRGANOS DE REPRESENTACIÓN</h1>
		".$organo."
		<bookmark title='9. FUNCIONES Y RESPONSABILIDADES' level='0' ></bookmark>
		<h1 class='tituloSeccion'>9. FUNCIONES Y RESPONSABILIDADES</h1>
		<p>En función del organigrama descrito en la página anterior, describimos a continuación las funciones de cada puesto, así como la responsabilidad en materia preventiva, en lo referente a la ejecución y/o control del plan de prevención, así como de todas las acciones preventivas a realizar. <br/>

			En cualquier caso, queda entendido que las responsabilidades descritas no sustituyen, sino que complementan, a las descritas en la normativa interna de seguridad y salud, ya que las mismas deben ser cumplidas por todos y cada uno de los operarios de la empresa, independientemente de su puesto.</p>
		<table class='tablaDatos'>
			<tr>
				<th class='a30 centro'>Función</th>
				<th class='a40 centro'>Nombre y apellidos</th>
				<th class='a15 centro'>DNI</th>
				<th class='a15 centro'>FECHA</th>
			</tr>
			<tr>
				<td class='a30'>GERENTE</td>
				<td class='a40'>".$datos['EMPRL']."</td>
				<td class='a15 centro'>".$datos['EMPRLDNI']."</td>
				<td class='a15'>Según escrituras de poder</td>
			</tr>
			<tr>
				<td class='a30'>INTERLOCUTOR<br/>CON EL SPA</td>
				<td class='a40'>".$datos['pregunta18']."</td>
				<td class='a15 centro'>".$datos['pregunta19']."</td>
				<td class='a15'>".$datos['pregunta21']."</td>
			</tr>
			<tr>
				<td class='a30'><br/></td>
				<td class='a40'></td>
				<td class='a15 centro'></td>
				<td class='a15'></td>
			</tr>
		</table>
		<br/>
		<table class='tablaDatos'>
			<tr>
				<th class='a35 centro'>Función</th>
				<th class='a45 centro'>Nombre y apellidos</th>
				<th class='a20 centro'>DNI</th>
			</tr>";
		$empleados=consultaBD('SELECT * FROM funciones_formulario_prl INNER JOIN empleados ON funciones_formulario_prl.codigoEmpleado=empleados.codigo WHERE codigoFormularioPRL='.$datos['codigo'],true);
		while($empleado=mysql_fetch_assoc($empleados)){
				$contenido.="<tr>
					<td class='a35'>".$empleado['funciones']."</td>
					<td class='a45'>".$empleado['nombre']." ".$empleado['apellidos']."</td>
					<td class='a20 centro'>".$empleado['dni']."</td>
				</tr>";
		}
		$contenido.="
		</table>
		<p>
		<b>DIRECCIÓN DE LA EMPRESA:</b><br/>
		Tiene la responsabilidad de garantizar la seguridad y salud de los trabajadores a su cargo, para ello impondrá las medidas necesarias:<br/>
		a)	Determinar una política preventiva y transmitirla a la organización.<br/>
		b)	Determinar los objetivos y metas a alcanzar.<br/>
		c)	Definir las funciones y responsabilidades de cada nivel jerárquico a fin de que se cumplan dichos objetivos.<br/>
		d)	Asegurar el cumplimiento de los preceptos contemplados en la normativa de aplicación.<br/>
		e)	Fijar y documentar los objetivos y metas esperados a tenor de la política preventiva.<br/>
		f)	Establecer una modalidad organizativa de la prevención.<br/>
		g)	Liderar el desarrollo y mejora continua del sistema de gestión de la prevención de riesgos laborales establecido.<br/>
		h)	Facilitar los medios humanos y materiales necesarios para el desarrollo de las acciones establecidas para el alcance de los objetivos.<br/>
		i)	Asumir un compromiso participativo en diferentes actuaciones preventivas, para demostrar su liderazgo en el sistema de gestión preventiva.<br/>
		j)	Adoptar las acciones correctoras y preventivas necesarias para corregir las posibles desviaciones que se detecten en el Plan de Prevención.<br/>
		k)	Asegurar que la organización disponga de la formación necesaria para desarrollar las funciones y responsabilidades establecidas.<br/>
		l)	Designar a uno o varios trabajadores para la asunción del S.G.P.R.L., que coordinen el sistema, controlen su evolución y le mantengan informado.<br/>
		m)	Establecer las competencias de cada nivel organizativo para el desarrollo de las actividades preventivas definidas en los procedimientos.<br/>
		n)	Asignar los recursos necesarios, tanto humanos como materiales, para conseguir los objetivos establecidos.<br/>
		o)	Integrar los aspectos relativos al S.G.P.R.L. en el sistema general de gestión de la entidad.<br/>
		p)	Participar de forma “proactiva” en el desarrollo de la actividad preventiva que se desarrolla, a nivel de los lugares de trabajo, para poder estimular comportamientos eficientes, detectar deficiencias y demostrar interés por su solución. <br/>
		q)	Realizar periódicamente análisis de la eficacia del sistema de gestión y en su caso establecer las medidas de carácter general que se requieran para adaptarlo a los principios marcados en la política preventiva.<br/>
		r)	Favorecer la consulta y participación de los trabajadores conforme a los principios indicados en la normativa de aplicación.<br/>
		s)	Todas aquellas otras funciones y responsabilidades definidas en el Sistema de Gestión Integrado de la empresa y en la Ley de PRL.
		</p>

		<p>
		<b>Los RECURSOS PREVENTIVOS</b> deberán vigilar el cumplimiento de las actividades preventivas en relación con los riesgos derivados de la situación para conseguir un adecuado control del riesgo.<br/> Dicha vigilancia incluirá:<br/>
		a)	La comprobación de la eficacia de las actividades preventivas.<br/>
		b)	La adecuación de tales actividades a los riesgos que pretenden prevenirse o a la aparición de riesgos no previstos y derivados de la situación que determina la necesidad de la presencia de los recursos preventivos.<br/>
		c)	En el caso de observar deficiente cumplimiento de las actividades preventivas:<br/>
 		- Dar las indicaciones necesarias para el correcto e inmediato cumplimiento de dichas actividades<br/>
		- Dar a conocer a la persona responsable de la empresa estas circunstancias, para que adopte las medidas necesarias.<br/>
		- En el caso de que se observe ausencia, insuficiencia o falta de adecuación de las medidas preventivas deberá hacerlo saber al empresario para que las adopte.<br/>
		- Además de las funciones detalladas como trabajador.<br/>
		d)	Todas aquellas otras funciones y responsabilidades definidas en el Sistema de Gestión Integrado de la empresa y en la Ley de PRL. 
		</p>
		<p><br/><br/>
		<b>Todos LOS EMPLEADOS</b> deberán:<br/>
		a) Velar por su propia seguridad y salud en el trabajo y por la de los demás, cumpliendo con las medidas de prevención adoptadas, de acuerdo con la formación recibida y siguiendo las órdenes del superior jerárquico.<br/>
		b)	Conocer y cumplir la normativa, procedimientos e instrucciones que afecten a su trabajo, en particular a las medidas de prevención y protección<br/>
		c)	Usar adecuadamente los medios con los que desarrolle su actividad.<br/>
		d)	Utilizar y conservar correctamente los medios y equipos de protección personal, que, en su caso, les sean facilitados.<br/>
		e)	Comunicar e Informar inmediatamente al superior jerárquico directo y al personal con funciones específicas en prevención sobre cualquier condición o práctica que pueda suponer un peligro para la seguridad y salud de los trabajadores, así como notificar la ocurrencia de accidentes o incidentes potencialmente peligrosos.<br/>
		f)	Cooperar con los superiores directos para garantizar unas condiciones de trabajo seguras.<br/>
		g)	Comunicar al Servicio de Personal la situación de embarazo o lactancia y aquéllas en las que se les pueda considerar como especialmente sensibles a los riesgos a los que está expuesto en su puesto de trabajo. <br/>
		h)	Sugerir las medidas que considere oportunas en su ámbito de trabajo para mejorar la calidad, la seguridad y la eficacia del mismo, conforme al procedimiento establecido al efecto.<br/>
		i)	Respetar en todo momento las indicaciones realizadas por los técnicos de prevención en las evaluaciones de riesgos de sus centros y puestos de trabajo, y cualquier otra instrucción preventiva que se les entreguen.<br/>
		j)	Colaborar con el personal del Servicio de Prevención de Riesgos Laborales durante sus visitas a los centros de trabajo y en las investigaciones de accidentes o incidentes y en todo aquello que sea preciso en materia de prevención de riesgos laborales.<br/>
		k)	Colaborar en la elaboración e implantación del plan de emergencia y evacuación en el centro de trabajo donde desarrollen su actividad.<br/>
		l)	Utilizar los Equipos de Protección Individual que se les proporcionen, de acuerdo con las instrucciones que se les suministren, en las que le indique su superior jerárquico o que se determinen en la evaluación de riesgos. Teniendo en cuenta el riesgo existente, usarlos de forma segura y mantenerlos en buen estado de conservación.<br/>
		m)	Firmar los documentos que acrediten la entrega y recepción de documentación en materia de prevención de riesgos, de los equipos de protección individual o de cualquier otro material relacionado con la seguridad y salud en el trabajo.<br/>
		n)	Participar en las actividades formativas o informativas en materia de prevención de riesgos laborales organizadas por la Consejería para los diferentes puestos de trabajo.<br/>
		o)	Mantener limpio y ordenado su entorno de trabajo, localizando los equipos y materiales en los lugares asignados. Eliminar lo innecesario y clasificar lo útil<br/>
		p)	No anular y utilizar correctamente los equipos y dispositivos de seguridad, en particular los de protección individual.<br/>
		q)	Utilizar y ajustar, alterar o reparar el equipo sólo si se está autorizado.<br/>
		r)	Cooperar con la empresa en todas aquellas actividades destinadas a la prevención de riesgos laborales.<br/>
		s)	Cooperar en las labores de extinción de incendios, evacuación en caso de emergencia y salvamento de las víctimas en caso de accidente. <br/>
		t)	En general, cumplir las normas establecidas en la empresa, las instrucciones recibidas de los superiores jerárquicos y las señales existentes, así como seguir la política de prevención. Preguntar al personal responsable en caso de dudas acerca del contenido o forma de aplicación de las normas e instrucciones, o sobre cualquier duda relativa al modo de desempeñar su trabajo. Todas aquellas otras funciones y responsabilidades definidas en el Sistema de Gestión Integrado de la empresa y en la Ley de PRL.
		</p>
		<p>
		<b>Los DELEGADOS DE PREVENCIÓN</b> deberán:<br/>
		a)	Colaborar con la con la dirección del centro en la mejora de la acción preventiva. <br/>
		b)	Promover y fomentar la cooperación de los trabajadores en la ejecución de la normativa sobre prevención de riesgos laborales, así como en el desarrollo del Plan de Acción anual en función de la priorización de las materias y fechas de ejecución establecidas en el mismo. <br/>
		c)	Ser consultados, con carácter previo a su ejecución, acerca de las decisiones a que se refiere el artículo 33 de la Ley 31/1995.<br/>
		d)	Ejercer una función de vigilancia y control sobre el cumplimiento de la normativa de prevención de riesgos laborales. <br/>
		e)	En el ejercicio de las competencias atribuidas a los Delegados de Prevención, estos se encuentran facultados para:<br/>
		- Acompañar a los técnicos en las evaluaciones de carácter preventivo del medio ambiente de trabajo, así como, en los términos previstos en el artículo 40 de la Ley 31/1995, a los Inspectores de Trabajo y Seguridad Social en las visitas y verificaciones que realicen en los centros de trabajo para comprobar el cumplimiento de la normativa sobre prevención de riesgos laborales, pudiendo formular ante ellos las observaciones que estimen oportunas.<br/>
		- Tener acceso, con las limitaciones previstas en el apartado 4 del artículo 22 de la Ley 31/1995, a la información y documentación relativa a las condiciones de trabajo que sean necesarias para el ejercicio de sus funciones y, en particular, a las previstas en los artículos 18 y 23 de la ya mencionada Ley. Cuando la información esté sujeta a las limitaciones reseñadas, solo podrá ser suministradas de manera que se garantice el respeto la confidencialidad de los datos de los trabajadores.<br/>
		- Ser informado sobre los daños producidos en la salud de los trabajadores una vez que aquel hubiese tenido conocimiento de ellos, pudiendo presentarse, aún fuera de su jornada laboral, en el lugar de los hechos para conocer las circunstancias de los mismos<br/>
		- Recibir las informaciones procedentes de las personas u órganos encargados de las actividades de protección y prevención en la empresa, así como de los órganos competentes para la seguridad y salud de los trabajadores, sin perjuicio de lo dispuesto en el artículo 40 de la Ley 31/1995, en materia de colaboración con la Inspección de Trabajo y Seguridad Social.<br/>
		- Realizar visitas a los lugares de trabajo para ejercer una vigilancia y control del estado de las condiciones de trabajo, pudiendo, a tal fin, acceder a cualquier zona de los mismos y comunicarse durante la jornada con los trabajadores, de manera que no se altere el normal desarrollo de la actividad del centro.<br/>
		- Recabar la adopción de medidas de carácter preventivo y para la mejora de los niveles de protección para la seguridad y salud de los trabajadores, pudiendo a tal fin efectuar propuestas al empresario, así como al comité de Seguridad y Salud para su discusión en el mismo.<br/>
		- Proponer al órgano de representación de los trabajadores la adopción del acuerdo de paralización de actividades a que se refiere el apartado 3 del artículo 21 de la Ley de Prevención de Riesgos Laborales<br/>
		f)	Todas aquellas otras funciones y responsabilidades definidas en el Sistema de Gestión Integrado de la empresa y en la Ley de PRL 
		</p>
		<p><br/><br/><br/><br/>
		<b>El INTERLOCUTOR CON EL SERVICIO DE PREVENCIÓN AJENO</b> deberá realizar:<br/>
		a)	Coordinarse con el Servicios de Prevención Ajeno, asegurando su adecuada actuación. <br/>
		b)	Participar en la realización y revisión de la evaluación de riesgos cuando sea necesario, en particular las evaluaciones específicas, según obras y/o servicios contratados y planes de seguridad y salud en obras. <br/>
		c)	Gestionar la compra de los equipos de protección individual <br/>
		d)	Revisar el cumplimiento de los equipos de trabajo de los requisitos establecidos en el Real Decreto 1215/97. <br/>
		e)	Mantener al día un listado de los equipos de trabajo y su estado con respecto a la legislación vigente. <br/>
		f)	Establecer los controles activos para el aseguramiento de las medidas de seguridad y salud, desarrollando la supervisión y control que se considere oportuna por parte del Servicio. <br/>
		g)	Establecer los equipos e instalaciones sometidos a requisitos de compra en base a requisitos de seguridad y salud. <br/>
		h)	Revisar el cumplimiento de las normas de seguridad de los equipos de trabajo antes de su puesta en marcha tras su instalación, reparación o haberse visto involucrado en un accidente. <br/>
		i)	Controlar la legislación aplicable en materia de prevención, definiendo los requisitos de aplicación. <br/>
		j)	Elaboración y mantenimiento del Plan de Autoprotección. <br/>
		k)	Convocar a los trabajadores a los reconocimientos médicos, comunicando a las unidades aquellas personas que debiendo someterse a un reconocimiento médico específico no lo han llevado a cabo en el plazo establecido. <br/>
		l)	Supervisión de las condiciones de trabajo, asegurando que cumplen con los requisitos definidos en la evaluación de riesgos. <br/>
		m)	Asegurarse, mediante revisión, que todas las instrucciones y métodos de trabajo incluyen las normas de seguridad necesarias. <br/>
		n)	Retener la aprobación de instrucciones y métodos de trabajo no incluyan las medidas de seguridad necesarias o que las incluidas no sean adecuadas. <br/>
		o)	Retener la aprobación de instrucciones y métodos de trabajo no incluya actividades no evaluadas hasta su evaluación y definición de las medidas de seguridad necesarias. <br/>
		p)	Establecimiento de las no conformidades que se consideren oportunas para el aseguramiento de los requisitos establecidos. <br/>
		q)	Elaboración, gestión y mantenimiento del Sistema de Gestión de la Prevención. <br/>
		r)	Coordinar los aspectos de seguridad y salud con las contratas y subcontratas. <br/>
		s)	Asistir a las reuniones del Comité de Seguridad y Salud. <br/>
		t)	Participar en el diseño, aplicación y coordinación en el cumplimiento de los Planes y Programas de prevención. <br/>
		u)	Diseño e impartición de la formación en materia de prevención a los trabajadores. <br/>
		v)	Gestión de la formación impartida por entidades externas. <br/>
		w)	Todas aquellas otras funciones y responsabilidades definidas en el Sistema de Gestión Integrado de la empresa y en la Ley de PRL. <br/>
		</p>
		<p>
		<br/><br/><br/><br/><br/><br/>
		<b>El SERVICIO DE PREVENCIÓN AJENO</b> deberá realizar:<br/>
		a)	El diseño, aplicación y coordinación del Plan de Prevención de Riesgos Laborales que permita la integración de la prevención en la empresa. <br/>
		b)	La evaluación de los riesgos que pueden afectar a la seguridad y salud de los trabajadores. <br/>
		c)	La planificación de la actividad preventiva, y la determinación de las prioridades en adopción de las medidas preventivas adecuadas y la vigilancia de su eficacia. <br/>
		d)	La información y formación de los trabajadores; apoyo y supervisión de las actividades formativas realizadas por el departamento de prevención. <br/>
		e)	Revisión de los planes de emergencia. <br/>
		f)	El desarrollo de la normativa interna de aplicación necesaria para que la empresa lleve a cabo la Gestión de la Prevención de Riesgos Laborales.<br/>
		g)	El desarrollo de las actividades de vigilancia de la salud de los trabajadores en relación con los riesgos derivados del trabajo. <br/>
		h)	Todas aquellas otras funciones y responsabilidades definidas en el Sistema de Gestión Integrado de la empresa y en la Ley de PRL. 
		</p>
		</div>
	</page>

	<page footer='page' backtop='35mm' backleft='25mm' backright='25mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div>
		<bookmark title='10. EVALUACIÓN DE RIESGOS' level='0' ></bookmark>
		<h1 class='tituloSeccion'>10. EVALUACIÓN DE RIESGOS</h1>
		<p>
		Por su importancia en la correcta gestión de la prevención, el RD 39/1997 de RSP, obliga al empresario a realizar una evaluación de riesgos, en concreto, en su artículo 3.2 a). Por otra parte, el artículo 15 de la LPRL, incluye la evaluación de riesgos como uno de los “principios de la actividad preventiva”.<br/><br/>

		El RSP, en su artículo 3, define la evaluación de riesgos, como el proceso dirigido a estimar la magnitud de aquellos riesgos QUE NO HAYAN PODIDO EVITARSE, obteniendo la información necesaria para que el empresario esté en condiciones de tomar una decisión apropiada sobre la necesidad de adoptar medidas preventivas y, en tal caso, sobre el tipo de medidas que deben adoptarse.<br/><br/>

		La Evaluación de Riesgos pretende poner de manifiesto las posibles situaciones de riesgo, para aplicar una acción correctora y eliminar o minimizar el riesgo en su máxima medida.<br/><br/>
		La <b>metodología de evaluación</b> se ha basado en las indicaciones que se establecen: </p>
		<ul class='list-style-type:square;'>
			<li>La Ley 31/1995 de Prevención de Riesgos Laborales (modificada por la Ley 54/2003)</li>
			<li>El R.D. 39/1997 por el que se aprueba el Reglamento de los Servicios de Prevención.</li>
			<li>Disposiciones y Reales Decretos que complementan y desarrollan las normas anteriormente citadas que establecen las condiciones mínimas de seguridad y salud a cumplir por el empresario.</li>
			<li>Criterio general de evaluación basado en la Guía del Instituto Nacional de Seguridad e Higiene en el Trabajo: “Evaluación de Riesgos Laborales”.</li>
		</ul>
		<p>
		La evaluación se dividirá en dos partes:<br/>
		A) CONDICIONES GENERALES DE SEGURIDAD Y SALUD<br/>
		B) EVALUACIÓN ESPECÍFICA DE RIESGOS POR PUESTOS<br/><br/>

		En la evaluación se contemplarán todos los puestos de la empresa y considerará, en su caso, la necesidad de asegurar la protección de los trabajadores especialmente sensibles a determinados riesgos (por sus características personales, estado biológico o discapacidad física, psíquica o sensorial).<br/><br/>
		La evaluación inicial de riesgos deberá ser revisada, según el art. 4.2 del RD 39/97, con ocasión de que los puestos de trabajo puedan verse afectados por:
		</p>
		<ul style='list-style-type:none;'>
			<li>I. Incorporación de “algo nuevo” (art. 4.2.a RD 39/97): la elección de equipos de trabajo, de sustancias o preparados químicos, de introducción de nuevas tecnologías o la modificación en el acondicionamiento de los lugares de trabajo.</li>
			<li>II. El cambio en las condiciones de trabajo (art. 4.2.b RD 39/97).</li>
		</ul>
		</div>
	</page>

	<page footer='page' backtop='35mm' backleft='25mm' backright='25mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div>
		<ol style='list-style-type:none;'>
			<li>III. La incorporación de un trabajador cuyas características personales o estado biológico conocido lo hagan especialmente sensible a las condiciones del puesto (art. 4.2.c RD 39/97)</li>
			<li>IV. El acuerdo o convenio entre las partes (art. 6.2 RD 39/97)</li>
			<li>V. Por el análisis de la situación epidemiológica (art. 6.1 d RD 39/97)</li>
			<li>VI. Por la investigación de las causas de los accidentes y sus daños (art. 6.1 a RD 39/97)</li>
			<li>VII. Detección de daños para la Salud en determinados puestos o inadecuación o insuficiencia de medidas preventivas (art. 6.1 RD 39/97)</li>
			<li>VIII. Por la acción o ejecución de las actividades para la reducción del riesgo (art. 6.1 c RD 39/97)</li>
			<li>IX. Por la acción o ejecución de actividades para el control del riesgo (art. 6.1 c RD 39/97)</li>
			<li>X. Cuando así lo establezca una disposición específica o normativa de aplicación (art. 6.1 RD 39/97)</li>
			<li>XI. El paso o transcurrir del tiempo (art. 6.2 RD 39/97)</li>
		</ol>
		<p>
		Las evaluaciones iniciales y/o periódicas realizadas para la empresa <b>".$datos['pregunta0']."</b> estarán contenidas en este Plan como documentos anexos.<br/><br/>

		<b>".$datos['pregunta0']."</b> ha establecido, implementado y mantienen procedimientos para la identificación continua de los peligros, evaluación de riesgos y la determinación de los controles necesarios. <br/><br/>

		Para la gestión de los cambios, <b>".$datos['pregunta0']."</b> identifica los peligros y los riesgos de la seguridad y salud en el trabajo asociados con los cambios en la organización, el sistema de gestión de la SST, o en sus actividades, antes de la incorporación de dichos cambios.<br/>

		La organización se asegura de que se consideran los resultados de estas evaluaciones al determinar los controles.<br/>
		Al establecer los controles o considerar cambios en los controles existentes se considera la reducción de riesgos de acuerdo con la siguiente jerarquía:<br/></p>
		<ul>
			<li>eliminación;</li>
			<li>sustitución;</li>
			<li>controles de ingeniería;</li>
			<li>señalización/ advertencias y/ o controles administrativos;</li>
			<li>protecciones colectivas</li>
			<li>equipos de protección personal.</li>
		</ul>

		<p><b>".$datos['pregunta0']."</b> documenta y mantienen actualizados los resultados de la identificación de peligros, evaluación de riesgos y los controles determinados.<br/><br/>
		Asimismo, se asegura de que los riesgos y los controles determinados se tengan en cuenta al establecer, implementar y mantener su sistema de gestión de la prevención.
		</p>



		<bookmark title='11. INFORMACIÓN, CONSULTA Y PARTICIPACIÓN' level='0' ></bookmark>
		<h1 class='tituloSeccion'>11. INFORMACIÓN, CONSULTA Y PARTICIPACIÓN</h1>
		<p>A fin de dar cumplimiento al deber de protección establecido en la presente Ley, art. 18, <b>".$datos['pregunta0']."</b> tiene implantado el procedimiento comunicación, por el que se definen las actuaciones a realizar para que los trabajadores reciban todas las informaciones necesarias, en relación con:
		</p>
		<ul style='list-style-type:square;'>
			<li>Los riesgos para la seguridad y la salud de los trabajadores en el trabajo, tanto aquellos que afecten a la empresa en su conjunto como a cada tipo de puesto de trabajo o función.</li>
			<li>Las medidas y actividades de protección y prevención aplicables a los riesgos señalados en el apartado anterior.</li>
			<li>Las medidas adoptadas de conformidad con lo dispuesto en el artículo 20 de la Ley 31/95 de Prevención de Riesgos Laborales.</li>
		</ul>
		<p>
		Asimismo, <b>".$datos['pregunta0']."</b> facilitará a los trabajadores, a través de sus representantes, dicha información, no obstante, a cada trabajador se le informa de los riesgos específicos que afecten a su puesto de trabajo o función y de las medidas de protección y prevención aplicables a dichos riesgos.<br/><br/>

<b>".$datos['pregunta0']."</b> consultara a sus trabajadores y permitirá su participación en el marco de todas las cuestiones que afecten a la seguridad y a la salud en el trabajo, teniendo derecho a efectuar propuestas a la empresa.<br/>
		</p>
		<bookmark title='12. FORMACIÓN' level='0' ></bookmark>
		<h1 class='tituloSeccion'>12. FORMACIÓN</h1>
		<p>En cumplimiento del art. 19 LPRL y del deber de protección, <b>".$datos['pregunta0']."</b> garantiza que cada trabajador reciba una formación teórica y práctica, suficiente y adecuada, en materia preventiva, tanto en el momento de su contratación, cualquiera que sea la modalidad o duración de ésta, como cuando se produzcan cambios en las funciones que desempeñe o se introduzcan nuevas tecnologías o cambios en los equipos de trabajo, tal y como recoge en su procedimiento tal del sistema de Gestión.<br/><br/>

			La formación está centrada específicamente en el puesto de trabajo o función de cada trabajador, adaptándose a la evolución de los riesgos y a la aparición de otros nuevos y repetirse periódicamente, si fuera necesario.<br/>
			Esta formación se impartirá, siempre que sea posible, dentro de la jornada de trabajo o, en su defecto, en otras horas, pero con el descuento en aquélla del tiempo invertido en la misma. La formación se podrá impartir por <b>".$datos['pregunta0']."</b> mediante medios propios o concertándola con servicios ajenos, y su coste no recaerá en ningún caso sobre los trabajadores.<br/>
			La información y los planes de formación de los trabajadores estarán contenidos en el documento de Planificación.
		</p>
		</div>
	</page>

	<page footer='page' backtop='35mm' backleft='25mm' backright='25mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div>
		<bookmark title='13. COORDINACIÓN DE ACTIVIDADES EMPRESARIALES' level='0' ></bookmark>
		<h1 class='tituloSeccion'>13. COORDINACIÓN DE ACTIVIDADES EMPRESARIALES</h1>
		<bookmark title='13.1 ÁMBITO Y OBJETO DE APLICACIÓN' level='1' ></bookmark>
		<h1 class='tituloSeccion'>13.1. ÁMBITO Y OBJETO DE APLICACIÓN</h1>
		<p><b>La Ley 31/1995 de Prevención de Riesgos Laborales establece en su artículo 24 la necesidad de llevar a cabo una</b> Coordinación de actividades empresariales. Concretamente dice:<br/><br/>

		<i>“1. Cuando en un mismo centro de trabajo desarrollen actividades trabajadores de dos o más empresas, éstas deberán cooperar en la aplicación de la normativa sobre prevención de riesgos laborales. A tal fin, establecerán los medios de coordinación que sean necesarios en cuanto a la protección y prevención de riesgos laborales y la información sobre los mismos a sus respectivos trabajadores, en los términos previstos en el <b>apartado 1 del artículo 18 de esta Ley.</b><br/>
			2. El empresario titular del centro de trabajo adoptará las medidas necesarias para que aquellos otros empresarios que desarrollen actividades en su centro de trabajo reciban la información y las instrucciones adecuadas, en relación con los riesgos existentes en el centro de trabajo y con las medidas de protección y 	prevención correspondientes, así como sobre las medidas de emergencia a aplicar, para su traslado a sus respectivos trabajadores.<br/>
			3. Las empresas que contraten o subcontraten con otras la realización de obras o servicios correspondientes a la propia actividad de aquéllas y que se desarrollen en sus propios centros de trabajo deberán vigilar el cumplimiento por dichos contratistas y subcontratistas de la normativa de prevención de riesgos laborales.<br/>
			4. Las obligaciones consignadas en el último párrafo del apartado 1 del <b>artículo 41 de esta Ley</b> serán también de aplicación, respecto de las operaciones contratadas, en los supuestos en que los trabajadores de la empresa contratista o subcontratista no presten servicios en los centros de trabajo de la empresa principal, siempre que tales trabajadores deban operar con maquinaria, equipos, productos, materias primas o útiles 	proporcionados por la empresa principal.<br/>
			5. Los deberes de cooperación y de información e instrucción recogidos en los apartados 1 y 2 serán de aplicación respecto de los trabajadores autónomos que desarrollen actividades en dichos centros de trabajo.“</i>.
		</p>
		<bookmark title='13.2 CONTRATAS, SUBCONTRATAS, TRABAJADORES, AUTONÓNOMOS' level='1' ></bookmark>
		<h1 class='tituloSeccion'>13.2. CONTRATAS, SUBCONTRATAS, TRABAJADORES AUTONÓNOMOS</h1>
		<p>Con la entrada en vigor del nuevo R.D. 171/2004, de 30 de enero, por el que se desarrolla el artículo 24 de la Ley 31/1995, de Prevención de Riesgos Laborales, en materia de coordinación de actividades empresariales, se impulsa aún más la necesidad de COORDINACIÓN EN MATERIA PREVENTIVA ENTRE EMPRESAS.<br/><br/>

		Así, tenemos:<br/>
		Cuando <b>".$datos['pregunta0']."</b> esté desarrollando actividades en un mismo centro de trabajo donde desarrollen actividades trabajadores de dos o más empresas, las empresas deberán cooperar en la aplicación de la normativa sobre prevención de riesgos laborales. A tal fin, establecerán los medios de coordinación que sean necesarios en cuanto a la protección y prevención de riesgos laborales y la información sobre los mismos a sus respectivos trabajadores, en los términos previstos en el apartado 1 del artículo 18 de la LPRL.<br/><br/>
		Éstas deberán cooperar en la aplicación de la normativa sobre prevención de riesgos laborales. Apartado 1, artículo 24 de la Ley 31/1995, de 8 de noviembre, de Prevención de Riesgos Laborales.  Además, se 	tendrán en cuenta las obligaciones previstas en el Real Decreto 171/2004, de 30 de enero, por el que se 	desarrolla el artículo 24 de la Ley 31/1995 anteriormente citada, en materia de coordinación de actividades empresariales. <br/><br/>
		Este R.D. se presenta con la obligatoriedad de informarse recíprocamente sobre los riesgos específicos de las actividades que desarrollen las empresas que desarrollen sus actividades en un mismo centro de trabajo. Así mismo, dicha información deberá ser suficiente y tendrá que proporcionarse antes del inicio de las actividades.<br/><br/>
		Además, el empresario principal debe vigilar el cumplimiento de la normativa de prevención de riesgos laborales por parte de las empresas contratistas y subcontratistas de obras y servicios correspondientes a 	su propia actividad y que desarrollen en su propio centro de trabajo (Art. 10. R.D. 171/2004), por lo que debe demandar a las empresas concurrentes la siguiente documentación:
		</p>
		<ol>
			<li>Indicación de la modalidad de organización preventiva que tiene su empresa en cumplimiento del Art. 10 del R.D. 39/1.997. </li>
			<li>Evaluación de riesgos, correspondiente a cada puesto de trabajo que ha de intervenir en el centro de trabajo.</li>
			<li>Acreditación de los trabajadores que se encuentren en el centro de trabajo y corriente de pago en la Seguridad Social (TC1 y TC2). <br/>
			A)	4. Certificado de aptitud del trabajador para el puesto que va a desarrollar (vigilancia de la salud, Art. 22 	Ley 31/1.995). </li>
			<li>Certificado de haber recibido la formación e información preventiva preceptiva (Art. 18 y 19 Ley 31/1.995). </li>
			<li>Nombramiento de un trabajador como coordinador de la seguridad y salud en el centro de trabajo.</li>
			<li>Determinación del modelo preventivo.</li>
			<li>Justificante de entrega de Equipos de Protección Individual (EPI´s) firmados por el trabajador. </li>
		</ol>
		<p><b>DOCUMENTACIÓN EN CASO DE ÁPORTAR MAQUINARIA Y/O MEDIOS AUXILIARES (PROPIOS, SUBCONTRATADOS O ALQUILADOS)</b></p>
		<ol>
			<li>Relación de máquinas y/o medios auxiliares que tienen previsto utilizar en el centro de trabajo. </li>
			<li>Relación del personal autorizado y con formación (especial y/o adecuada) para el uso de las máquinas y/o medios auxiliares descritos en el apartado anterior.</li>
			<li>Certificado de características y certificado de conformidad de los equipos de trabajo de acuerdo con los requisitos del R.D. 1215/97.</li>
		</ol>
		<bookmark title='13.3 RECURSOS PREVENTIVOS' level='1' ></bookmark>
		<h1 class='tituloSeccion'>13.3. RECURSOS PREVENTIVOS</h1>
		<p>Respecto a los denominados Recursos Preventivos, el <b>Artículo 32 bis. (Introducido por el artículo 4.3. de la Ley 54/2003)</b> establece la necesidad de presencia de recursos preventivos en las empresas. Dicho artículo establece:<br/><br/>
		<i>“1. La presencia en el centro de trabajo de los recursos preventivos, cualquiera que sea la modalidad de 	organización de dichos 	recursos, será necesaria en los siguientes casos:<br/>
		* A completar por el técnico en función del organigrama de la empresa.<br/>
		a) Cuando los riesgos puedan verse agravados o modificados en el desarrollo del proceso o la actividad, por 	la concurrencia de operaciones diversas que se desarrollan sucesiva o simultáneamente y que hagan 	preciso el control de la correcta aplicación de 	los métodos de trabajo.<br/>
		b) Cuando se realicen actividades o procesos que reglamentariamente sean considerados como peligrosos o 	con riesgos especiales.<br/>
		c)Cuando la necesidad de dicha presencia sea requerida por la Inspección de Trabajo y Seguridad Social, si 	las circunstancias del caso así lo exigieran debido a las condiciones de trabajo detectadas.<br/><br/>
		2.Se consideran recursos preventivos, a los que el empresario podrá asignar la presencia, los siguientes:<br/>
		a) Uno o varios trabajadores designados de la empresa.<br/>
		b) Uno o varios miembros del servicio de prevención propio de la empresa.<br/>
		c) Uno o varios miembros del o los servicios de prevención ajenos concertados por la empresa.<br/>
		Cuando la presencia sea realizada por diferentes recursos preventivos éstos deberán colaborar entre sí.<br/><br/>
		3. Los recursos preventivos a que se refiere el apartado anterior deberán tener la capacidad suficiente, disponer de los medios necesarios y ser suficientes en número para vigilar el cumplimiento de las actividades preventivas, debiendo permanecer en el centro de trabajo durante el tiempo en que se mantenga la situación que determine su presencia.<br/><br/>
		4. No obstante lo señalado en los apartados anteriores, el empresario podrá asignar la presencia de forma 	expresa a uno o varios trabajadores de la empresa que, sin formar parte del servicio de prevención propio ni ser trabajadores designados, reúnan los conocimientos, la calificación y la experiencia necesarios en las actividades o procesos a que se refiere el apartado 1 y cuenten con la formación preventiva correspondiente, como mínimo, a las funciones del nivel básico. En este supuesto, tales trabajadores deberán mantener la necesaria colaboración con los recursos preventivos del empresario”.</i>
		</p>
		<bookmark title='13.4 MEDIOS DE COORDINACIÓN' level='1' ></bookmark>
		<h1 class='tituloSeccion'>13.4. MEDIOS DE COORDINACIÓN</h1>
		<p>El Capítulo V, del R.D. 171/2004, establece la necesidad de llevar a cabo medios de coordinación cuando 	existan en un mismo centro de trabajo dos o más empresas concurrentes.  A continuación, se exponen los 	medios de coordinación propuestos por la normativa vigente:</p>
		<ul style='list-style-type:none;'>
			<li><img src='../img/checkPP.png' /> Intercambio de información.</li>
			<li><img src='../img/checkPP.png' /> Reuniones periódicas.</li>
			<li><img src='../img/checkPP.png' /> Reuniones con los delegados de prevención de las subcontratas.</li>
			<li><img src='../img/checkPP.png' /> Impartición de instrucciones sobre normas de seguridad a llevar a cabo en la empresa.</li>
			<li><img src='../img/checkPP.png' /> Establecimiento conjunto de medidas específicas de prevención de los riesgos existentes en el centro de trabajo</li>
			<li><img src='../img/checkPP.png' /> Presencia de los recursos preventivos de las empresas concurrentes</li>
			<li><img src='../img/checkPP.png' /> La designación de un encargado de la coordinación de las actividades preventivas.</li>
		</ul>
		<bookmark title='14. CONTRATACIÓN DE TRABAJADORES DE UNA EMPRESA DE TRABAJO TEMPORAL' level='0' ></bookmark>
		<h1 style='margin-top:0px' class='tituloSeccion'>14. CONTRATACIÓN DE TRABAJADORES DE UNA EMPRESA DE TRABAJO TEMPORAL</h1>
		<p>Cuando la empresa decida la contratación de los servicios de personal perteneciente a una empresa de trabajo temporal seguirá las siguientes normas para dar cumplimiento tanto al Art. 28 de la Ley 31/95 de Prevención de Riesgos Laborales (Obligaciones de la empresa usuaria), como al R. D 216/99 Sobre empresas de trabajo temporal:<br/><br/>

			Antes del inicio del trabajo se informará a la empresa de trabajo temporal de la información acerca del puesto de trabajo a desempeñar, características, cualificaciones del trabajador necesarias, así como cualquier riesgo y equipos de protección necesario. Recordando a la ETT su obligación de trasladar dicha información al trabajador, así como facilitarle la formación necesaria.<br/><br/>

			Asimismo, la empresa está obligada a exigir a la ETT, (Art. 4 R. D. 216/1999) la información necesaria para asegurarse de que el trabajador puesto a su disposición reúne las siguientes condiciones:<br/><br/>

			<b>a.</b>	Ha sido considerado apto a través de un adecuado reconocimiento de su estado de salud para la realización de los servicios que deba prestar en las condiciones en que hayan de ser efectuados, de conformidad con lo dispuesto en el Art. 22 de la Ley de Prevención de Riesgos Laborales y en el Art. 37.3 del Reglamento de los Servicios de Prevención.<br/><br/>

			<b>b.</b>	Posee las Cualificaciones y capacidades requeridas para el desempeño de las tareas que se le encomienden en las condiciones en que vayan a efectuarse y cuenta con la formación necesaria, todo ello en relación con la prevención de los riesgos a los que pueda estar expuesto, en los términos previstos en el Art. 19 de la Ley de Prevención de Riesgos Laborales y en sus disposiciones de desarrollo.<br/><br/>
			<b>c.</b>	Ha recibido las informaciones relativas a las características propias del puesto de trabajo y de las tareas a desarrollar, a las Cualificaciones y aptitudes requeridas y a los resultados de la evaluación de riesgos, conforme la información suministrada por la empresa usuaria.<br/>
			Otras obligaciones de la empresa usuaria respecto los trabajadores a incorporarse de la ETT, son: (Art. 4 R. D. 216/99).<br/><br/>

			<img src='../img/checkPP.png'> La empresa usuaria no permitirá el inicio de la prestación de servicios en la misma de un trabajador puesto a su disposición hasta que no tenga constancia del cumplimiento de las obligaciones del apartado anterior.<br/><br/>

			<img src='../img/checkPP.png'> La empresa usuaria informará a los delegados de prevención o, en su defecto, a los representantes legales de sus trabajadores, de la incorporación de todo trabajador puesto a disposición por una empresa de trabajo temporal, especificando el puesto de trabajo a desarrollar, sus riesgos y medidas preventivas y la información y formación recibidas por el trabajador. El trabajador podrá dirigirse a estos representantes en el ejercicio de sus derechos reconocidos en el presente Real Decreto y, en general, en el conjunto de la legislación sobre prevención de riesgos laborales.<br/><br/>

			La información a la que se refiere el párrafo anterior será igualmente facilitada por la empresa usuaria a su servicio de prevención o, en su caso, a los trabajadores designados para el desarrollo de las actividades preventivas.<br/><br/>

			En los siguientes documentos se describe la información a suministrar, así como un documento de entrega de equipos de protección:<br/><br/>
		</p>
		<bookmark title='15. CONTRATACIÓN DE TRABAJADORES ESPECIALMENTE SENSIBLES' level='0' ></bookmark>
		<h1 class='tituloSeccion'>15. CONTRATACIÓN DE TRABAJADORES ESPECIALMENTE SENSIBLES</h1>
		<p>Atendiendo al Art. 25 de la Ley de Prevención de Riesgos Laborales, referente a la protección de trabajadores 	especialmente sensibles a determinados riesgos, el empresario  tendrá en cuenta en las evaluaciones de riesgos a aquellos trabajadores que, por sus características personales o estado biológico conocido, incluidos 	aquellos que tengan reconocida la situación de discapacidad física, psíquica o sensorial, sean especialmente sensibles a los riesgos derivados del trabajo, con objeto de adoptar las medidas preventivas y de protección necesarias.<br/><br/>

		A estos efectos deberá comunicar a ANESCO la existencia de dichos trabajadores para poder asesorar al empresario sobre la forma de proteger adecuadamente a dichos trabajadores. Dicha comunicación a ANESCO competerá al Departamento de Prevención de la Empresa, y se realizará a través de un comunicado por escrito que contenga los nombres y apellidos del trabajador sensible, categoría profesional, y puesto que desempeña, desplazándose un técnico de ANESCO para analizar las funciones que desempeñe y recogerlas a través de anexo a la presente evaluación con las medidas de prevención y protección necesarias.
		</p>
		<bookmark title='16. PROTECCIÓN DE MENORES' level='0' ></bookmark>
		<h1 class='tituloSeccion'>16. PROTECCIÓN DE MENORES</h1>
		<p>El empresario, antes de la incorporación al trabajo de jóvenes menores de 18 años y, previamente a una modificación de sus condiciones de trabajo, efectuará una evaluación de los puestos de trabajo a desempeñar 	en cualquier actividad susceptible de presentar riesgos que puedan poner en peligro la seguridad o salud de 	estos trabajadores (Artículo 27 de la Ley 31/1995). A estos efectos deberá comunicar a ANESCO la existencia de dichos trabajadores para poder asesorar al empresario sobre la forma de proteger adecuadamente a 	dichos trabajadores. Dicha comunicación a ANESCO competerá al Departamento de Prevención de la 	Empresa, y se realizará a través de un comunicado por escrito que contenga los nombres y apellidos del 	trabajador, edad, categoría profesional, y puesto que vaya a desempeñar, desplazándose un técnico de ANESCO para analizar las funciones que desempeñe, naturaleza, grado y duración de la exposición a riesgos en su puesto de trabajo. Se realizará evaluación específica por el Dpto. técnico de ANESCO y la empresa informará directamente al trabajador y a sus padres o tutores de las conclusiones de la misma, con los riesgos y medidas preventivas a adoptar, para el conocimiento por éstos.
		</p><br/><br/><br/>
		<bookmark title='17. PROTECCIÓN A LA MATERNIDAD' level='0' ></bookmark>
		<h1 style='margin-top:0px' class='tituloSeccion'>17. PROTECCIÓN A LA MATERNIDAD</h1>
		<p>Si los resultados de la evaluación revelasen un riesgo para la seguridad y salud o una posible repercusión sobre el embarazo o la lactancia de las trabajadoras embarazadas, el empresario adoptará las medidas necesarias para evitar la exposición a dicho riesgo según contempla el artículo 26 de la Ley de Prevención de Riesgos Laborales, debiendo comunicar a ANESCO dichas circunstancias para poder asesorar al empresario sobre la forma de proteger adecuadamente a dichos trabajadores. Dicha comunicación la realizará el Departamento de Prevención de la Empresa y posterior visita a través del técnico para analizar las funciones que desempeñe	y recogerlas a través de anexo a la presente evaluación con las medidas de prevención y protección necesarias 	en su caso o, si estas no fueran posibles, establecimiento de reunión entre ANESCO, Dpto. Laboral o RR.HH 	de la empresa y dirección de ésta, para el análisis de nuevo puesto de trabajo que no contemple riesgos para 	la trabajadora o el feto o, en última instancia, la suspensión del contrato de trabajo conforme al sistema General de la Seguridad Social para dichas situaciones, previo informe de ANESCO.<br/><br/>

			El empresario adoptará las medidas necesarias para evitar la exposición de la trabajadora afectada a riesgos laborales durante su jornada, a través de una adaptación de las condiciones o del tiempo de trabajo. <br/><br/>

			Cuando la adaptación de las condiciones o del tiempo de trabajo no resultase posible o, a pesar de tal adaptación, las condiciones de un puesto de trabajo pudieran influir negativamente en la salud de la trabajadora embarazada o del feto, y así lo certifique el médico que en el régimen de la Seguridad Social aplicable asista facultativamente a la trabajadora, ésta deberá desempeñar un puesto de trabajo o función diferente y compatible con su estado. <br/><br/>

			El empresario deberá determinar, previa consulta con los representantes de los trabajadores, la relación de los puestos de trabajo exentos de riesgos a estos efectos. El cambio de puesto o función se llevará a cabo de conformidad con las reglas y criterios que se apliquen en los supuestos de movilidad funcional y tendrá efectos hasta el momento en que el estado de salud de la trabajadora permita su reincorporación al anterior puesto. <br/><br/>

			En el supuesto de que no existiese puesto de trabajo o función compatible, la trabajadora podrá ser destinada a un puesto no correspondiente a su grupo o a categoría equivalente, si bien conservará el derecho al conjunto 	de retribuciones de su puesto de origen.<br/>
			Lo dispuesto anteriormente también se aplicará durante el período de lactancia, si las condiciones de trabajo pudieran influir negativamente en la salud de la mujer o del hijo y así lo certificase el médico que, en el régimen de Seguridad Social aplicable, asista facultativamente a la trabajadora. <br/><br/>

			Las trabajadoras embarazadas tendrán derecho a ausentarse del trabajo, con derecho a remuneración, para la realización de exámenes prenatales y técnicas de preparación al parto, previo aviso al empresario y 	justificación de la necesidad de su realización dentro de la jornada de trabajo.<br/>
		</p>
		</div>
	</page>

	<page footer='page' backtop='35mm' backleft='25mm' backright='25mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div>
		<bookmark title='18. SEGUIMIENTO Y MEDICIÓN' level='0' ></bookmark>
		<h1 class='tituloSeccion'>18. SEGUIMIENTO Y MEDICIÓN</h1>
		<p>
		La evaluación periódica de los objetivos marcados y del sistema preventivo y del conjunto de elementos fundamentales que lo componen es una actividad ineludible que ha de permitir su mejora continua y garantizar, tanto el cumplimiento de las exigencias reglamentarias en materia de gestión preventiva, como la eficacia del propio sistema.<br/><br/>
		La realización de auditorías del Plan de Prevención, con la consiguiente valoración del grado de cumplimiento de la implantación, estará incluida en el documento de Planificación de la Actividad Preventiva.<br/><br/>

		Las condiciones de seguridad de los lugares, instalaciones, equipos y ambiente de trabajo deben controlarse periódicamente y deben someterse aún mantenimiento adecuado para evitar el deterioro de las mismas a lo largo del tiempo. Las actividades específicas de mantenimiento y control necesarias a tal efecto están establecidas en función de los resultados de la evaluación de riesgos, aunque en algunos casos dichas actividades se concretan y vienen directamente impuestas por una normativa específica.<br/>

		</p>
		<bookmark title='19. MEDIDAS DE EMERGENCIA' level='0' ></bookmark>
		<h1 class='tituloSeccion'>19. MEDIDAS DE EMERGENCIA</h1>
		<p>
		<b>".$datos['pregunta0']."</b> en aplicación del art. 20 de la LPRL, y teniendo en cuenta su tamaño y actividad, así como la posible presencia de personas ajenas a la misma, ha analizado las posibles situaciones de emergencia y adoptado las medidas necesarias en materia de primeros auxilios, lucha contra incendios y evacuación de los trabajadores, designando para ello al personal encargado de poner en práctica estas medidas y comprobando periódicamente, en su caso, su correcto funcionamiento. El citado personal dispone de la formación necesaria, suficiente en número y dispone del material adecuado, en función de las circunstancias antes señaladas.
		</p>
		<bookmark title='20. DAÑOS A LA SALUD: ACCIDENTES DE TRABAJO Y ENFERMEDADES PROFESIONALES' level='0' ></bookmark>
		<h1 class='tituloSeccion'>20. DAÑOS A LA SALUD: ACCIDENTES DE TRABAJO Y ENFERMEDADES PROFESIONALES</h1>
		<p>
		Cuando se produzca un daño para la salud de los trabajadores o cuando, con ocasión de la vigilancia de la salud de los mismos aparezcan indicios de que las medidas de prevención resultan insuficientes, el empresario llevará a cabo una investigación al respecto, al objeto de detectar las causas de los mismos.<br/><br/>

		En relación con dichos daños, deben comunicarse a la autoridad competente los datos e informaciones indicados en la normativa específica aplicable (partes de accidentes de trabajo a través del Sistema DELTA y de enfermedades profesionales a través del sistema CEPROSS). Asimismo, se comunicará a la Autoridad Sanitaria las posibles sospechas de enfermedades profesionales

		</p>
		</div>
	</page>

	<page footer='page' backtop='35mm' backleft='25mm' backright='25mm' backbottom='0mm'>
		<page_header>
			<div class='imagenFondo2'><img src='../img/fondoOfertaBN.jpg' alt='Anesco' /></div>
			<table class='cabecera'>
				<tr>
					<td class='a20'><img src='../img/logoOferta.png' /></td>
					<td class='a60'>PLAN DE PREVENCIÓN</td>
					<td class='a20'>".$logoCliente."</td>
				</tr>
			</table>
		</page_header>
		
		<div>
		<bookmark title='21. VIGILANCIA DE LA SALUD' level='0' ></bookmark>
		<h1 class='tituloSeccion'>21. VIGILANCIA DE LA SALUD</h1>
		<p>
		<b>".$datos['pregunta0']."</b> garantiza a los trabajadores a su servicio la vigilancia periódica de su estado de salud en función de los riesgos inherentes al trabajo.<br/><br/>
			Esta vigilancia se lleva a cabo cuando el trabajador preste su consentimiento. De este carácter voluntario sólo se exceptuarán, previo informe de los representantes de los trabajadores, los supuestos en los que la realización de los reconocimientos sea imprescindible para evaluar los efectos de las condiciones de trabajo sobre la salud de los trabajadores o para verificar si el estado de salud del trabajador puede constituir un peligro para el mismo, para los demás trabajadores o para otras personas relacionadas con la empresa o cuando así esté establecido en una disposición legal en relación con la protección de riesgos específicos y actividades de especial peligrosidad.<br/>
			Los resultados de la vigilancia a que se refiere el apartado anterior serán comunicados a los trabajadores afectados.<br/><br/>

			El acceso a la información médica de carácter personal se limitará al personal médico y a las autoridades sanitarias que lleven a cabo la vigilancia de la salud de los trabajadores, sin que pueda facilitarse a <b>".$datos['pregunta0']."</b> o a otras personas sin consentimiento expreso del trabajador.<br/><br/>

			No obstante lo anterior, <b>".$datos['pregunta0']."</b> y las personas u órganos con responsabilidades en materia de prevención serán informados de las conclusiones que se deriven de los reconocimientos efectuados en relación con la aptitud del trabajador para el desempeño del puesto de trabajo o con la necesidad de introducir o mejorar las medidas de protección y prevención, a fin de que puedan desarrollar correctamente su funciones en materia preventiva.<br/><br/>

			Las medidas de vigilancia y control de la salud de los trabajadores se llevarán a cabo por personal sanitario con competencia técnica, formación y capacidad acreditada.

		</p>
		<bookmark title='22. PRACTICAS, PROCEDIMIENTOS Y PROCESOS' level='0' ></bookmark>
		<h1 class='tituloSeccion'>22. PRACTICAS, PROCEDIMIENTOS Y PROCESOS</h1>
		<p>Con el fin de lograr la máxima eficacia preventiva posible, ".$datos['pregunta0']." promueve la creación de una serie de procedimientos e instrucciones operativas que se van desarrollando e implantando de forma progresiva. Este conjunto de documentos forma parte del Plan de Prevención y se van desarrollando en función de las necesidades que se detecten. Siendo responsabilidad del personal designado para su difusión y puesta en marcha entre todos los trabajadores que se puedan ver afectados por los mismos</p>
		<bookmark title='22.1 LISTADO DE PROCEDIMIENTOS DE GESTIÓN O TÉCNICOS' level='0' ></bookmark>
		<h1 class='tituloSeccion'>22.1. LISTADO DE PROCEDIMIENTOS DE GESTIÓN O TÉCNICOS</h1>
		<p>Los procedimientos de gestión servirán para definir de forma sistemática como se debe realizar cada actuación preventiva. <br/>
		Los procedimientos o instrucciones técnicas desarrollarán con detalle aquellos aspectos concretos de necesario cumplimiento en la realización de un trabajo o tarea.</p>
		</div>
	</page>";


	return $contenido;
}


function obtienePreguntasFormularioPDF(&$datos){
	
	$formulario=explode('&{}&',$datos['formulario']);
    
    if(count($formulario)>1){
        foreach($formulario as $pregunta){
            $partes=explode('=>',$pregunta);
            $datos[$partes[0]]=$partes[1];
        }
    }


    //Parte de comprobación de campos nuevos en formularios antiguos (para comprobarlo, comentar líneas y ver https://crmparapymes.com.es/anesco2/planificacion/generaPlanPrevencion.php?codigo=834)
    if(!isset($datos['pregunta140556'])){
    	$datos['pregunta140556']='';
    }

    for($i=140565;$i<=140570;$i++){
		if(!isset($datos['pregunta'.$i])){
	    	$datos['pregunta'.$i]='';
	    }    	
    }

}