<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de instalaciones

function operacionesInstalaciones(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('instalaciones');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('instalaciones');
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoInstalacion($_POST['elimina']);
	}

	mensajeResultado('nombre',$res,'instalación');
    mensajeResultado('elimina',$res,'instalación', true);
}


function gestionInstalacion(){
	operacionesInstalaciones();
	
	abreVentanaGestionConBotones('Gestión de instalaciones','index.php');
	$datos=compruebaDatos('instalaciones');

	campoTexto('nombre','Nombre',$datos,'span6');
	campoOculto($datos,'eliminado','NO');

	cierraVentanaGestion('index.php');
}


function imprimeInstalaciones($estado='NO'){
	$consulta=consultaBD("SELECT * FROM instalaciones WHERE eliminado='$estado';",true);
	while($datos=mysql_fetch_assoc($consulta)){

		echo "<tr>
				<td> ".$datos['nombre']." </td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	  	</td>
			 </tr>";
	}
}


function cambiaEstadoInstalacion($estado){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();
    for($i=0;isset($datos['codigo'.$i]);$i++){
        $res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],'instalaciones',false);
    }
    cierraBD();

    return $res;
}

//Fin parte de instalaciones