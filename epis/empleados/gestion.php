<?php
  $seccionActiva=10;
  include_once("../cabecera.php");
  gestionEmpleados();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
		$('#fechaNacimiento').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');calcular_edad('#fechaNacimiento');});
		$(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    
		$("#nif").focusout(function(){
    		isValidNif($(this).val());
		}); 

		$('#telefono, #fax').attr('maxlength','9');
	});

	function calcular_edad(id){
    var hoy=new Date()
    var fecha=$(id).val();
    var array_fecha = fecha.split("/")
    //si el array no tiene tres partes, la fecha es incorrecta
    if (array_fecha.length!=3)
       return false

    //compruebo que los ano, mes, dia son correctos
    var ano
    ano = parseInt(array_fecha[2]);
    if (isNaN(ano))
       return false

    var mes
    mes = parseInt(array_fecha[1]);
    if (isNaN(mes))
       return false

    var dia
    dia = parseInt(array_fecha[0]);
    if (isNaN(dia))
       return false

    //si el año de la fecha que recibo solo tiene 2 cifras hay que cambiarlo a 4
    if (ano<=99)
       ano +=1900

    //resto los años de las dos fechas
    edad=hoy.getFullYear()- ano - 1; //-1 porque no se si ha cumplido años ya este año
    //si resto los meses y me da menor que 0 entonces no ha cumplido años. Si da mayor si ha cumplido
    if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0
       edad = edad
    if (hoy.getMonth() + 1 - mes > 0)
       edad = edad+1

    //entonces es que eran iguales. miro los dias
    //si resto los dias y me da menor que 0 entonces no ha cumplido años. Si da mayor o igual si ha cumplido
    if (hoy.getUTCDate() - dia >= 0)
       edad = edad + 1

    $('#edad').val(edad);
} 
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>