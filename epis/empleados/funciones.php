<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión Clientes


function operacionesEmpleados(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaEmpleado();
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaEmpleado();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaItem('empleados','codigoEmpleado');
	}

	mensajeResultado('nombre',$res,'Empleado');
    mensajeResultado('elimina',$res,'Empleado', true);
}

function insertaEmpleado(){
	$res=true;
	
	$res=insertaDatos('empleados','','../documentacion/fotos');
	$codigoEmpleado=mysql_insert_id();
	$res=insertaUsuario($codigoEmpleado);
	
	return $res;
}

function actualizaEmpleado(){
	$res=true;
	$res=actualizaDatos('empleados','','../documentacion/fotos');
	$res=actualizaContraseña();
	return $res;
}

function actualizaContraseña(){
	$res=true;
	$usuario=consultaBD('SELECT usuarios.codigo FROM usuarios_empleados INNER JOIN usuarios ON usuarios_empleados.codigoUsuario=usuarios.codigo WHERE usuarios_empleados.codigoEmpleado='.$_POST['codigo'],true,true);
	$res=consultaBD('UPDATE usuarios SET nombre="'.$_POST['nombre'].'",apellidos="'.$_POST['apellidos'].'",dni="'.$_POST['dni'].'",email="'.$_POST['email'].'",nombre="'.$_POST['telefono'].'",usuario="'.$_POST['usuario'].'",clave="'.$_POST['clave'].'" WHERE codigo='.$usuario['codigo'],true);
	return $res;
}

function insertaUsuario($codigoCliente){
	$res=true;
	$res=consultaBD("INSERT INTO usuarios(codigo,nombre,apellidos,dni,email,telefono,usuario,clave,tipo) VALUES (NULL,'".$_POST['nombre']."','".$_POST['apellidos']."','".$_POST['dni']."','".$_POST['email']."','".$_POST['telefono']."','".$_POST['usuario']."','".$_POST['clave']."','EMPLEADO');");
	$codigoUsuario=mysql_insert_id();
    $res=consultaBD('INSERT INTO usuarios_empleados VALUES (NULL,'.$codigoUsuario.','.$codigoCliente.');');
    return $res;
}




function gestionEmpleados(){
	operacionesEmpleados();
	
	abreVentanaGestion('Gestión de Empleados','index.php','','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('empleados');
	
	$codigoCliente=obtieneCodigoClienteEmpleado($datos);
	campoOculto($codigoCliente,'codigoCliente');

	abreColumnaCampos();
		if(!$datos){
			$numeroEmpleado = generaNumero($_GET['codigoCliente']);
			$usuario=false;
		} else {
			$numeroEmpleado = $datos['codigoInterno'];
			$usuario=consultaBD('SELECT usuario,clave FROM usuarios_empleados INNER JOIN usuarios ON usuarios_empleados.codigoUsuario=usuarios.codigo WHERE usuarios_empleados.codigoEmpleado='.$datos['codigo'],true,true);
		}
		campoTexto('codigoInterno','Nº de empleado',$numeroEmpleado,'input-mini pagination-right');
		campoTexto('nombre','Nombre',$datos);
		campoTexto('apellidos','Apellidos',$datos);
		campoTexto('dni','DNI',$datos,'input-small');
	cierraColumnaCampos();	

	abreColumnaCampos();
		campoFecha('fechaNacimiento','Fecha de nacimiento',$datos);
		campoTexto('edad','Edad',$datos,'input-mini',true);
		campoTextoSimbolo('telefono','Teléfono','<i class="icon-phone"></i>',$datos,'input-small pagination-right');
		campoTextoSimbolo('email','eMail','<i class="icon-envelope"></i>',$datos,'input-large');
	cierraColumnaCampos();
	
	
	abreColumnaCampos();
		campoTexto('direccion','Dirección 1',$datos);
		campoTexto('direccion2','Dirección 2',$datos);
		campoTexto('cp','Código postal',$datos,'input-mini');
		campoTexto('ciudad','Ciudad',$datos);
		campoTexto('provincia','Provincia',$datos);
		campoSelectConsulta('codigoPuestoTrabajo','Puesto',"SELECT codigo, nombre AS texto FROM puestos_trabajo WHERE codigoCliente='$codigoCliente' ORDER BY nombre;",$datos,'selectpicker show-tick');
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTexto('pais','País',$datos);
		campoTexto('movil','Teléfono móvil',$datos);
		campoFecha('fechaAlta','Fecha de alta',$datos);
		areaTexto('objetivo','Objetivos',$datos);
		areaTexto('observaciones','Observaciones',$datos);
		campoFichero('ficheroFoto','Foto',0,$datos,'../documentacion/fotos/','Foto');
	cierraColumnaCampos();

	echo "<br clear='all'>";
	
	abreColumnaCampos();
		campoTexto('usuario','Usuario',$usuario);
		campoClave('clave','Contraseña',$usuario);
	cierraColumnaCampos();
		
		
	cierraVentanaGestion('index.php?codigo='.$codigoCliente,true);
}

function obtieneCodigoClienteEmpleado($datos){
	if(isset($_GET['codigoCliente'])){
		$res=$_GET['codigoCliente'];	
	}
	elseif($datos){
		$res=$datos['codigoCliente'];
	}
	else{
		$res='NULL';
	}

	return $res;
}

//Parte de campos personalizados

function campoSelectProvincia($nombreCampo,$datos){
	$valores=array();
	$nombres=array();
	$provinciasAgencia=array('01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');
	
	foreach ($provinciasAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}

	campoSelect($nombreCampo,'Provincia',$nombres,$valores,$datos);
}

//Fin parte de campos personalizados

function imprimeEmpleados($cliente){
	global $_CONFIG;

	conexionBD();

	$consulta=consultaBD("SELECT * FROM empleados WHERE codigoCliente = ".$cliente." ORDER BY codigoInterno;");
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td class='centro'>".$datos['codigoInterno']."</td>
				<td>".$datos['nombre']."</td>
				<td>".$datos['apellidos']."</td>
				<td>".$datos['dni']."</td>
				<td><a href='tel:".$datos['telefono']."'>".formateaTelefono($datos['telefono'])."</a></td>
				<td><a href='mailto:".$datos['email']."'>".$datos['email']."</a></td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>";
				if($_SESSION['tipoUsuario']=='ADMIN'){
				echo "<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>";
	        	}
			echo "</tr>";
	}
	cierraBD();
}

function generaNumero($cliente){
	$consulta=consultaBD("SELECT MAX(codigoInterno) AS numero FROM empleados WHERE codigoCliente=".$cliente, true, true);
	
	return $consulta['numero']+1;
}

function estadisticasEmpleadosRestrict($cliente){
	$res=array();

	conexionBD();
	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM empleados WHERE codigoCliente=".$cliente,false, true);

	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;
}

//Fin parte de gestión Clientes
