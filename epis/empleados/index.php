<?php
  $seccionActiva=10;
  include_once('../cabecera.php');
  
  $res=operacionesEmpleados();
  if(isset($_GET['codigo'])){
    $cliente=$_GET['codigo'];
  } else if (isset($_POST['codigoCliente'])){
    $cliente=$_POST['codigoCliente'];
  } else {
    $cliente=obtenerCodigoCliente(true);
  }
  $estadisticas=estadisticasEmpleadosRestrict($cliente);

?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de gestión Empleados:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-users"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Empleados registrados</div>
                     <!--div class="stat"> <i class="icon-send"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Enviados a la agencia</div-->
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Empleados</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="../clientes/index.php" class="shortcut noAjax"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label"> Volver</span> </a>
                <a href="<?php echo $_CONFIG['raiz']; ?>puestos-trabajo/index.php?codigoCliente=<?php echo $cliente; ?>" class="shortcut"><i class="shortcut-icon icon-sitemap"></i><span class="shortcut-label"> Puestos</span> </a>

                <?php 
                  if($_SESSION['tipoUsuario']=='ADMIN'){
                ?>

  				        <a href="<?php echo $_CONFIG['raiz']; ?>empleados/gestion.php?codigoCliente=<?php echo $cliente; ?>" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nuevo empleado</span> </a>
                  <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>

                <?php 
                  } 
                ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Empleados registrados</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Nº Empleado</th>
                  <th> Nombre </th>
                  <th> Apellidos </th>
                  <th> DNI </th>
                  <th> Teléfono </th>
                  <th> eMail </th>
                  <th class="centro"></th>
                  <?php if($_SESSION['tipoUsuario']=='ADMIN'){?>
                  <th><input type='checkbox' id="todo"></th>
                  <?php } ?>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeEmpleados($cliente);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>