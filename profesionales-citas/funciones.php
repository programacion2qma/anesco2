<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de profesionales de centros médicos

function operacionesProfesionalesCentros(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('profesionales_centros');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('profesionales_centros');
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminado('profesionales_centros',$_POST['elimina']);
	}

	mensajeResultado('nombre',$res,'profesional');
    mensajeResultado('elimina',$res,'profesional', true);
}


function gestionProfesional(){
	operacionesProfesionalesCentros();
	
	abreVentanaGestionConBotones('Gestión de profesional de centro médico','index.php','span3');
	$datos=compruebaDatos('profesionales_centros');

	campoTexto('nombre','Nombre',$datos);
	campoTexto('apellidos','Apellidos',$datos);
	campoSelectConsulta('codigoCentroMedico','Centro médico',"SELECT codigo, nombre AS texto FROM centros_medicos ORDER BY nombre",$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	areaTexto('observaciones','Observaciones',$datos);	
	campoOculto($datos,'eliminado','NO');

	cierraVentanaGestion('index.php',true);
}


function imprimeProfesionales($eliminado){
	$consulta=consultaBD("SELECT profesionales_centros.codigo, profesionales_centros.nombre, apellidos, centros_medicos.nombre AS centro 
						  FROM profesionales_centros LEFT JOIN centros_medicos ON profesionales_centros.codigoCentroMedico=centros_medicos.codigo
						  WHERE profesionales_centros.eliminado='$eliminado';",true);

	while($datos=mysql_fetch_assoc($consulta)){

		echo "<tr>
				<td> ".$datos['nombre']." </td>
				<td> ".$datos['apellidos']." </td>
				<td> ".$datos['centro']." </td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	  	</td>
			 </tr>";
	}
}

//Fin parte de profesionales de centros médicos