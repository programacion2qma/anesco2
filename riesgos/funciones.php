<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de empleados

function operacionesRiesgos(){
	$res=true;

	if(isset($_POST['codigo'])){
    	$res=actualizaDatos('riesgos');
  	}
  	elseif(isset($_POST['nombre'])){
    	$res=insertaDatos('riesgos');
  	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('riesgos');
	}

	mensajeResultado('nombre',$res,'Riesgos');
    mensajeResultado('elimina',$res,'Riesgos', true);
}

function imprimeRiesgos(){
	global $_CONFIG;

	$codigoS=$_SESSION['codigoU'];
	$consulta=consultaBD("SELECT riesgos.codigo,riesgos.codigoInterno,riesgos.nombre, riesgos_tipos.nombre AS tipo FROM riesgos INNER JOIN riesgos_tipos ON riesgos.tipo=riesgos_tipos.codigo ORDER BY codigoInterno;", true);

	while($datos=mysql_fetch_assoc($consulta)){
		echo "
		<tr>
			<td> ".$datos['tipo']." </td>
			<td> ".$datos['nombre']." </td>
        	<td class='td-actions'>
        		<a href='".$_CONFIG['raiz']."riesgos/gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
        	</td>
        	<td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
    	</tr>";
	}
}

function gestionRiesgos(){
	operacionesRiesgos();

	abreVentanaGestionConBotones('Gestión de riesgos','index.php');
	$datos=compruebaDatos('riesgos');
	if($datos){
		$codigoInterno=$datos['codigoInterno'];
	} else {
		$codigoInterno=generaNumero() < 100 ? '0'.generaNumero() : generaNumero();
	}
	campoSelectConsulta('tipo','Tipo','SELECT codigo, nombre AS texto FROM riesgos_tipos ORDER BY codigo',$datos);
	//campoTexto('codigoInterno','Código',$codigoInterno,'input-small');
	campoOculto($codigoInterno,'codigoInterno');
	campoTexto('nombre','Nombre',$datos,'span4');

	

	cierraVentanaGestion('index.php');
}

function generaNumero(){
	$consulta=consultaBD("SELECT MAX(codigoInterno) AS max FROM riesgos",true, true);

	return $consulta['max'] + 10;
}

//Fin parte de empleados


