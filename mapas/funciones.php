<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión de epis


function operacionesMapas(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaMapa();
	}
	elseif(isset($_POST['codigoFormularioPRL'])){
		$res=insertaMapa();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('mapas');
	}

	mensajeResultado('codigoFormularioPRL',$res,'Mapa de proceso');
    mensajeResultado('elimina',$res,'Mapa de proceso', true);
}


function insertaMapa(){
	$res=true;
	$res=insertaDatos('mapas',time(),'ficheros');
	$codigo=$res;
	$res=$res && insertaTipos($codigo);
	return $res;
}

function actualizaMapa(){
	$res=true;
	$res=actualizaDatos('mapas',time(),'ficheros');
	$res=$res && insertaTipos($_POST['codigo']);
	return $res;
}

function insertaTipos($codigo){
	$res=true;

	conexionBD();
	$sql='codigo NOT IN(0';
	$i=0;
	while(isset($_POST['nombre'.$i])){
		if(isset($_POST['codigoExiste'.$i]) && $_POST['codigoExiste'.$i]!=''){
			$res=consultaBD('UPDATE tipos_procesos SET nombre="'.$_POST['nombre'.$i].'",descripcion="'.$_POST['descripcion'.$i].'",color="'.$_POST['color'.$i].'" WHERE codigo='.$_POST['codigoExiste'.$i]);
			$sql.=','.$_POST['codigoExiste'.$i];
		} else {
			$res=consultaBD('INSERT INTO tipos_procesos VALUES(NULL,'.$codigo.',"'.$_POST['nombre'.$i].'","'.$_POST['descripcion'.$i].'","'.$_POST['color'.$i].'");');
			$sql.=','.mysql_insert_id();
		}
		$i++;
	}
	$sql.=')';
	$res=consultaBD('DELETE FROM tipos_procesos WHERE '.$sql.' AND codigoMapa='.$codigo);
	cierraBD();
	return $res;
}


function gestionMapas(){
	operacionesMapas();
	
	abreVentanaGestionConBotones('Gestión de Mapas de proceso','index.php','','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('mapas');

	$nombres=array();
	$valores=array();
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']!='ADMIN'){
		$where.=' AND codigoUsuarioTecnico='.$_SESSION['codigoU'];
	}
	$consulta=consultaBD("SELECT f.codigo AS codigo, codigoUsuarioTecnico, CONCAT(EMPNOMBRE,' / ',EMPMARCA) AS cliente,f.codigoContrato, ofertas.codigoCliente
		FROM formularioPRL f 
		LEFT JOIN contratos ON f.codigoContrato=contratos.codigo 
		LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo 
		LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND f.eliminado='NO' AND f.aprobado='SI';",true);
	while($item=mysql_fetch_assoc($consulta)){
		if($item['codigoContrato']!=NULL && $item['codigoCliente']!=NULL){
			$referencia='';
			$contrato=consultaBD('SELECT * FROM contratos WHERE codigo='.$item['codigoContrato'],false,true);
			$cliente=consultaBD('SELECT * FROM clientes WHERE codigo='.$item['codigoCliente'],false,true);
			$referencia=' - Nº de contrato: '.formateaReferenciaContrato($cliente,$contrato);
			array_push($valores, $item['codigo']);
			array_push($nombres, $item['cliente'].$referencia);
		}
	}

	abreColumnaCampos();
		campoSelect('codigoFormularioPRL','Planificación',$nombres,$valores,$datos,'selectpicker span6 show-tick');
	cierraColumnaCampos(true);
	
	abreColumnaCampos();
		campoTexto('entrada','Entrada',$datos);	
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTexto('salida','Salida',$datos);	
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoFichero('ficheroMapa','Mapa de procesos',0,$datos,'ficheros/','Descargar');
	cierraColumnaCampos(true);

	echo "<h3 class='apartadoFormulario'>Tipos de procesos</h3>";
	tablaTipos($datos);
	
	cierraVentanaGestion('index.php');
}

function tablaTipos($datos){
	    echo "<br />
	    <div class='centro'>
		    <table class='tabla-simple mitadAncho' id='tablaTipos'>
	            <thead>
	                <tr>
	                    <th> Nombre</th>
	                    <th> Descripción </th>
	                    <th> Color </th>
	                    <th> </th>
	                </tr>
	            </thead>
	            <tbody>";
	        
	            $i=0;

	            conexionBD();

	            if($datos!=false){
	                $consulta=consultaBD("SELECT * FROM tipos_procesos WHERE codigoMapa=".$datos['codigo']);
	                while($item=mysql_fetch_assoc($consulta)){
	                    imprimeLineaTablaTipos($i,$item);
	                    $i++;
	                }
	            }
	            
	            if($i==0){
	                imprimeLineaTablaTipos(0,false);
	            }

	            cierraBD();
	      
	    echo "	</tbody>
	        </table>
	        <button type='button' class='btn btn-small btn-success' onclick='insertaFilaTipos(\"tablaTipos\");'><i class='icon-plus'></i> Añadir Tipo</button> 
	        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaTipos\");'><i class='icon-trash'></i> Eliminar Tipo</button>
	    </div><br />";
}

function imprimeLineaTablaTipos($i,$datos){
	$j=$i+1;

    echo "<tr>";
    		campoOculto($datos['codigo'],'codigoExiste'.$i);
        	campoTextoTabla('nombre'.$i,$datos['nombre']);
        	areaTextoTabla('descripcion'.$i,$datos['descripcion']);
        	campoTextoTabla('color'.$i,$datos['color'],'colorTareas input-small');
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function imprimeMapas(){
	global $_CONFIG;
	$where='WHERE 1=1';
	if($_SESSION['tipoUsuario']!='ADMIN'){
		$where.=' AND codigoUsuarioTecnico='.$_SESSION['codigoU'];
	}
	conexionBD();
	$consulta=consultaBD("SELECT mapas.codigo AS codigoMapa, f.codigo AS codigo, codigoUsuarioTecnico, CONCAT(EMPNOMBRE,' / ',EMPMARCA) AS cliente,f.codigoContrato, ofertas.codigoCliente FROM mapas INNER JOIN formularioPRL f ON mapas.codigoFormularioPRL=f.codigo LEFT JOIN contratos ON f.codigoContrato=contratos.codigo LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo ".$where." AND f.eliminado='NO' AND f.aprobado='SI';");

	$tabla='';
	while($datos=mysql_fetch_assoc($consulta)){
		$referencia='';
		if($datos['codigoContrato']!=NULL && $datos['codigoCliente']!=NULL){
			$contrato=consultaBD('SELECT * FROM contratos WHERE codigo='.$datos['codigoContrato'],false,true);
			$cliente=consultaBD('SELECT * FROM clientes WHERE codigo='.$datos['codigoCliente'],false,true);
			$referencia=formateaReferenciaContrato($cliente,$contrato);
		}
		$tabla.= "
			<tr>
				<td>".$datos['cliente']."</td>
				<td>".$referencia."</td>";
                $tabla.="</td>					 
				<td class='centro'>
					<div class='btn-group'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."mapas/gestion.php?codigo=".$datos['codigoMapa']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
						    <li class='divider'></li>
						    <li><a href='".$_CONFIG['raiz']."indicadores/index.php?codigo=".$datos['codigoMapa']."'><i class='icon-sitemap'></i> Mapa de proceso</i></a></li>";
				$tabla.="
						</ul>
					</div>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	cierraBD();
	echo $tabla;
}

//Fin parte de gestión de epis