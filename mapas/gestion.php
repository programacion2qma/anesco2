<?php
  $seccionActiva=9;
  include_once("../cabecera.php");
  gestionMapas();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-colorpicker.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$('.selectpicker').selectpicker();
	$(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
	$('.colorTareas').colorpicker();
});

function insertaFilaTipos($tabla){
	insertaFila($tabla);
	$('.colorTareas').colorpicker();
}
</script>
</div><!-- contenido -->
<?php include_once('../pie.php'); ?>