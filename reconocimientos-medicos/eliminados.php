<?php
  $seccionActiva=3;
  include_once('../cabecera.php');
  
  $res=operacionesReconocimientos();
  $estadisticas=estadisticasGenericas('reconocimientos_medicos',false,"eliminado='SI'");
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de reconocimientos médicos:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-trash"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Reconocimiento/s eliminado/s</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de reconocimientos médicos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="index.php" class="shortcut"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label">Volver</span> </a>
                <a href="javascript:void" id='reactivar' class="shortcut noAjax"><i class="shortcut-icon icon-check-circle"></i><span class="shortcut-label">Reactivar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		
      <form action='generaListado.php' method="post" class='noAjax'>
      <div class="span12">		    
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
                <h3>Reconocimientos médicos eliminados</h3>
                <div class="pull-right">
                    <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
                </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
                <?php
                    filtroReconocimientos();
                    campoOculto('','global');
                    campoOculto('','columnaOrdenada');
                    campoOculto('','sentidoOrden');
                    campoOculto('NO','eliminado');
                ?>
            <table class="table table-striped table-bordered datatable" id="tablaReconocimientos">
              <thead>
                <tr>
                  <th> Fecha </th>
                  <th> Nº Cliente</th>
                  <th> Empresa </th>
                  <th> Trabajador </th>
                  <th> DNI </th>
                  <th> Apto </th>
                  <th> V. apto </th>
                  <th> V. reco. </th>
                  <th> Cerrado </th>
                  <th> Facturado </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		    </form>


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    listadoTabla('#tablaReconocimientos','../listadoAjax.php?include=reconocimientos-medicos&funcion=listadoReconocimientos("SI");');
    
    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaReconocimientos');
    $('#botonFiltro').trigger('click');
    realizaBusquedaFiltrada('#cajaFiltros','#tablaReconocimientos');


    $('input[type=search]').change(function(){
      $('#global').val($(this).val());
    });

    $('#reactivar').click(function(){//Eliminación de registros
        var valoresChecks=recorreChecks();
        if(valoresChecks['codigo0']==undefined){
            alert('Por favor, seleccione antes un registro del listado.');
        }
        else if(confirm('¿Está seguro/a de que desea restaurar los registros seleccionados?')){
            valoresChecks['elimina']='NO';
            creaFormulario(document.URL,valoresChecks,'post');//Cambiado el nombre del documento por la URL.
        }
    });

  });
</script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>