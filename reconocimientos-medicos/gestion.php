<?php
  $seccionActiva=3;
  include_once("../cabecera.php");
  gestionReconocimiento();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../../api/js/funciones.js" type="text/javascript"></script>
<script src="../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>
<script src="../js/campoFicheroAnalitica.js" type="text/javascript"></script>
<script src="../js/funcionesCreacionEmpleado.js" type="text/javascript"></script>

<script src="../../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap.datatable.js" type="text/javascript"></script>

<script type="text/javascript">	

var modificado=false;
$(document).ready(function(){
	$('.selectpicker').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
		$(this).datepicker('hide');
		obtenerFechaSiguiente($('#periodicidad option:selected').val(),$(this).val());
	});

	if($(':submit').length==0){
		$('.widget-header .btn-propio').remove();//Eliminación del botón de guardado de la cabecera si el reco no es editable
	}

	$('.widget-header a, .form-actions a:not(.btn-success,.btn-default), .mainnav.menu a:not(.dropdown-toggle)').each(function( index ) {
		$(this).addClass('btnSalirReconocimiento');
		$(this).unbind();
		$(this).click(function(e){
			e.preventDefault();
			window.open($(this).attr('href'),'_self');
		});
	});
	
	$(document).on('change','input,textarea,select',function(){
		if(!modificado){
			modificado=true;

			$('.btnSalirReconocimiento').each(function( index ) {
		  		$(this).unbind();
		  		$(this).attr('enlace',$(this).attr('href'));
		  		$(this).attr('href','#');
				$(this).click(function(){
					var res=confirm('¿Seguro que quieres salir del reconocimiento médico sin guardar?');
					if(res==true){
						window.open($(this).attr('enlace'),'_self');
					}
				});
			});

		}

	});

	if($('#rmAnterior').length>0){
		$('.control-label').addClass('rmAnterior');
		$(".datoSinInput").each(function( index ) {
  			$(this).parent().find('.control-label').removeClass('rmAnterior');
		});
		$("input[type=radio],.input-prepend input,input[type=checkbox]").each(function( index ) {
  			$(this).change(function(){
  				$(this).parent().parent().parent().find('.control-label').removeClass('rmAnterior');
  			});
		});
		$("select,textarea,input").each(function( index ) {
  			$(this).change(function(){
  				$(this).parent().parent().find('.control-label').removeClass('rmAnterior');
  			});
		});

		$("table input,table select").each(function( index ) {
  			$(this).change(function(){
  				$(this).parent().parent().parent().parent().parent().parent().find('.control-label').removeClass('rmAnterior');
  			});
		});
		$("table .input-prepend input").each(function( index ) {
  			$(this).change(function(){
  				$(this).parent().parent().parent().parent().parent().parent().parent().find('.control-label').removeClass('rmAnterior');
  			});
		});
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
			$(this).datepicker('hide');
			obtenerFechaSiguiente($('#periodicidad option:selected').val(),$(this).val());
  			$(this).parent().parent().find('.control-label').removeClass('rmAnterior');
		});
	}

	//$('#fechaNacimientoCreacion').attr('readonly','readonly');
	$('#fechaAltaCreacion').attr('readonly','readonly');

	$('#codigoEmpleado').change(function(){
		oyenteEmpleado($(this).val());
		oyenteTituloVentana();
	});

	$('input[name=checkRecomendacion]').click(function(){
		oyenteCheckRecomendaciones($(this));
	});
	compruebaRecomendaciones();

	$('#obtenerDatosPorDefecto').click(function(){
		rellenaDatosPorDefecto();
		$(this).parent().parent().remove();
	});
	
	$('#obtenerDatosReconocimientoAnterior').click(function(){
		obtieneDatosReconocimientoAnterior();
	});

	$('#talla,#peso').change(function(){
		calculaIMC();
	});

	$('.tip').each(function(){
        var aviso=$(this).text();
        var campo=$(this).prev().find('.group-span-filestyle');

        campo.attr('data-toggle','tooltip');
        campo.attr('title',aviso);
        campo.tooltip(); 
        $(this).remove();
    });

	oyenteOtrasPruebas($('input[name=checkOtrasPruebasFuncionales]:checked').val())
	$('input[name=checkOtrasPruebasFuncionales]').change(function(){
    	oyenteOtrasPruebas($(this).val());
    });

    $('input[name=cerrado]').change(function(){
    	oyenteCerrado();
    });

    oyenteBloqueado();
    $('input[name=bloqueado]').change(function(){
    	oyenteBloqueado();
    });

    //Parte de indicador de datos analítica
    cuentaDatosAnomalosAnalitica();


    tipoReconocimiento($('input[name=tipoReconocimiento]:checked').val(),'#divTipoReconocimiento');
    $('input[name=tipoReconocimiento]').change(function(){
    	tipoReconocimiento($(this).val(),'#divTipoReconocimiento');
    });

    tipoReconocimiento($('#periodicidad option:selected').val(),'#divPeriodicidad');
    $('#periodicidad').change(function(){
    	tipoReconocimiento($(this).val(),'#divPeriodicidad');
    	obtenerFechaSiguiente($(this).val(),$('#fecha').val());
    });

    tipoReconocimiento($('#tipoGafasLentillas option:selected').val(),'#divTipoGafasLentillas');
    $('#tipoGafasLentillas').change(function(){
    	tipoReconocimiento($(this).val(),'#divTipoGafasLentillas');
    });

    $('#codigoPuestoTrabajo').change(function(){
    	var codigoPuesto=$(this).val();

    	obtieneEpisPuesto(codigoPuesto);
    	obtieneDatosProtocoloPuesto(codigoPuesto);
    });

    tab=1;
    for(var i=1;i<5;i++){
   		$('#estudioVision'+i).attr('tabindex',tab);
   		tab++;
   		j=i+4;
   		$('#estudioVision'+j).attr('tabindex',tab);
   		tab++;
   		j=j+4;
   		$('#estudioVision'+j).attr('tabindex',tab);
   		tab++;
   	}


   	//Parte de gestión de colores
	$('.menu-colores .menu a').click(function(e){
		e.preventDefault();
		var estado=$(this).attr('estado');
		var campo=$(this).attr('campo');
		var pestania=$(this).parent().parent().parent().parent().parent();

		pestania.removeClass('correcto');
		pestania.removeClass('advertencia');
		pestania.removeClass('anormal');
		pestania.addClass(estado);
		$('#'+campo).val(estado);
	});
	//Fin parte de gestión de colores

	//Parte de título ventana
	oyenteTituloVentana();
	//Fin parte título ventana


	//Parte de oyente de enfermedades en antecedentes
	oyenteTablaEnfermedadesAntecedentes();
	//Fin aprte de oyente de enfermedades en antecedentes

	oyenteTablaComentariosAnalitica();

	//Parte de oyente de EPIS
	oyenteEpis();
	$('input[name="toleraEpis"]').change(function(){
		oyenteEpis();
	});
	//Fin parte de oyente de EPIS

	//Parte oyente estudio visión
	oyenteEstudioVision();
	//Fin parte oyente estudio visión

	//Parte oyente estudio audición
	oyenteEstudioAudicion();
	//Fin parte oyente estudio audición

	//Parte de dinamometría
	oyenteResultadoDinamometria();
	//Fin parte de dinamometría

	//Parte índice paquetes-año
	$('#aniosFumando,#tabacoDia').change(function(){
		oyenteIndicePaquetesAnio();
	});
	//Fin parte índice paquetes-año

	//Parte de bebidas estimulantes
	oyenteBebidasEstimulantes();
	$('input[name=bebidasEstimulantes]').change(function(){
		oyenteBebidasEstimulantes();
	});
	//Fin parte de bebidas estimulantes

	//Parte de ejercicios
	oyenteEjercicios();
	$('#ejercicio').change(function(){
		oyenteEjercicios();
	});
	//Fin parte de ejercicios

	//Parte oyentes botones cierre/bloqueo/visible
	$('.tabs-reconocimientos .nav-tabs>li.contenedorBoton .btn-small').click(function(){
		var campo=$(this).attr('campo');
		var valor=$(this).attr('valor');

		$('#'+campo).val(valor);
		$('form').submit();
	});
	//Fin parte oyentes botones cierre/bloqueo/visible
	$('.selectHabito').change(function(){
		var id='#'+$(this).attr('id')+'Texto';
		$(id).val($(this).val());
	});

	observacionesVision($('input[name=checkObservacionesVision7]').attr('checked'));
	$('input[name=checkObservacionesVision7]').change(function(){
		observacionesVision($(this).attr('checked'));
	});

	gafasYLentillas($('input[name=checkTipoGafasLentillas4]').attr('checked'));
	$('input[name=checkTipoGafasLentillas4]').change(function(){
		gafasYLentillas($(this).attr('checked'));
	});

	oyenteDescPersona();
	$('#divDescPersona input').change(function(){
		oyenteDescPersona();
	});

	//oyenteEspirometria();
	$('#divCompruebaEspirometria input').change(function(){
		oyenteEspirometria();
	});




	$('a[href="#3"]').click(function(){

		if(!$.fn.DataTable.fnIsDataTable($('#tablaEnfermedadesReconocimiento')[0])){//Compruebo que el datatable no esté inicializado, para hacerlo
		
			var tablaEnfermedades=$('#tablaEnfermedadesReconocimiento').dataTable({
			    "sDom": "<'row-fluid arriba'<'span12'f>r>t<'row-fluid abajo'<'span12'i>>",
			    "bPaginate": false,
			    "stateSave":false,
			    "aoColumnDefs": [
			      { "bSortable": false, "aTargets": [ 1,2,3,4 ] }
			    ],
			    "oLanguage": {
			      "sLengthMenu": "_MENU_ registros por página",
			      "sSearch":"Búsqueda:",
			      "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente","sLast":"Último","sFirst":"Primero"},
			      "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
			      "sEmptyTable":"Aún no hay datos que mostrar",
			      "sInfoEmpty":"",
			      'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
			      'sZeroRecords':'No se han encontrado coincidencias'
			    },
			    "bScrollCollapse":true,
			    "sScrollY":'200px',
			    "bScrollAutoCss": false,
			});

			setTimeout(function(){
				tablaEnfermedades.fnSort([[0,'asc']]);
			},400);
		}

	});


	$('a[href="#7"]').click(function(){

		if(!$.fn.DataTable.fnIsDataTable($('#tablaComentariosAnalitica')[0])){//Compruebo que el datatable no esté inicializado, para hacerlo
		
			var tablaComentarios=$('#tablaComentariosAnalitica').dataTable({
			    "sDom": "<'row-fluid arriba'<'span12'f>r>t<'row-fluid abajo'<'span12'i>>",
			    "bPaginate": false,
			    "stateSave":false,
			    "aoColumnDefs": [
			      { "bSortable": false, "aTargets": [ 1 ] }
			    ],
			    "oLanguage": {
			      "sLengthMenu": "_MENU_ registros por página",
			      "sSearch":"Búsqueda:",
			      "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente","sLast":"Último","sFirst":"Primero"},
			      "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
			      "sEmptyTable":"Aún no hay datos que mostrar",
			      "sInfoEmpty":"",
			      'sInfoFiltered':"(Filtrado de un total de _MAX_ registros)",
			      'sZeroRecords':'No se han encontrado coincidencias'
			    },
			    "bScrollCollapse":true,
			    "sScrollY":'200px',
			    "bScrollAutoCss": false,
			});

			setTimeout(function(){
				tablaComentarios.fnSort([[0,'asc']]);
			},400);
		}

	});


	//Oyente genérico para campos de texto asociados a Si/No
	$('input[name=trastornosCongenitos]:checked,input[name=minusvaliaReconocida]:checked,input[name=bajasProlongadas]:checked,input[name=fracturas]:checked,input[name=acudirMedicoEspecialista]:checked,input[name=checkAgudezaVisual]:checked').each(function(){
		oyenteGenericoCampoTexto($(this));
	});

	$('input[name=trastornosCongenitos],input[name=minusvaliaReconocida],input[name=bajasProlongadas],input[name=fracturas],input[name=acudirMedicoEspecialista],input[name=checkAgudezaVisual]').change(function(){
		oyenteGenericoCampoTexto($(this));
	});
	//Fin oyente genérico para campos de texto asociados a Si/No

	//Oyente genérico para campos de texto asociados a checks
	for(var i=1;i<=4;i++){
		oyenteGenericoCampoTextoAsociadoCheck($('input[name=checkAlergias'+i+']'));
	}
	$('input[name=checkAlergias1],input[name=checkAlergias2],input[name=checkAlergias3],input[name=checkAlergias4]').click(function(){
		oyenteGenericoCampoTextoAsociadoCheck($(this));
	});
	//Fin oyente genérico para campos de texto asociados a checks

	//Oyentes para definir pestaña actual al guardar
	$('.tabs-reconocimientos > .nav-tabs > li > a').click(function(){
		var pestania=$(this).attr('href').replace('#','');//Reemplazo el # porque no se transmite por GET
		$('form').attr('action','?pestania='+pestania);
	});

	<?php 
	if(isset($_GET['pestania'])){
		echo "$('a[href=#".$_GET['pestania']."]').click();";
	}
	?>
	//Fin oyentes pare definir pestaña actual al guardar


	//Parte de radios de juicio clínico
	$('input[name=debeAcudirMedico]').change(function(){
		vuelcaDatosRadioEnRecomendaciones($(this).val(),'Debe acudir a su médico de cabecera.',true);
	});

	$('input[name=acudirMedicoEspecialista],#textoAcurdirMedicoEspecialista').change(function(){
		vuelcaDatosRadioEnRecomendaciones($('input[name=acudirMedicoEspecialista]:checked').val(),$('#textoAcurdirMedicoEspecialista').val(),true);
	});

	//Fin parte de radios de juicio clínico

	if($('#cerrado').val()=='SI'){
		$('#1,#11,#2,#3,#13,#3,#14,#9,#10').find('.selectpicker').selectpicker('destroy');
		$('#1,#11,#2,#3,#13,#3,#14,#9,#10').find('select,input:not(#codigo,#codigoUsuario,#perfilUsuario,#editable,#enviado,#cerrado,#bloqueado,#reconocimientoVisible,#aptoVisible,#reconocimientoVisibleAnterior,#aptoVisibleAnterior),textarea').attr('readonly',true)
		.click(function(e){
			e.preventDefault();
		});
		$('#1,#11,#2,#3,#13,#3,#14,#9,#10').find('select option:not(:selected)').attr('disabled',true);
	}
	if($('#bloqueado').val()=='SI'){
		$(document).find('.selectpicker').selectpicker('destroy');
		$(document).find('select,input:not(#codigo,#codigoUsuario,#perfilUsuario,#editable,#enviado,#cerrado,#bloqueado,#reconocimientoVisible,#aptoVisible,#reconocimientoVisibleAnterior,#aptoVisibleAnterior,#filtroEjercicio,:submit),textarea,.tab-content button').attr('readonly',true)
		.click(function(e){
			e.preventDefault();
		});
		$(document).find('select option:not(:selected)').attr('disabled',true);
	}


	//Parte oyentes frecuencia cardiaca
	$('#frecuenciaCardiaca').change(function(){
		oyenteFrecuenciaCardiaca($(this).val());
	});

	$('#sistolica,#diastolica').change(function(){
		oyenteTensionArterial();
	});
	//Fin parte de oyentes frecuencia cardiaca


	oyenteTabaco();
	$('#tipoFumador').change(function(){
		oyenteFumador($(this).val());
		oyenteTabaco();
	});

	oyenteCheckAnalitica();
	$('input[name=checkAnalitica]').change(function(){
		oyenteCheckAnalitica();
	});


	oyenteGenericoCampoTexto($('input[name=checkTratamientoHabitual]:checked'));
	$('input[name=checkTratamientoHabitual]').change(function(){
		oyenteGenericoCampoTexto($(this));
	});

	oyenteVacunaciones();
	$('input[name=checkVacunaciones]').change(function(){
		oyenteVacunaciones();
	});

	oyenteAlcohol();
	$('#alcohol').change(function(){
		oyenteAlcohol();
	});


	$('#valoracionEspirometrica').change(function(){
		oyenteValoracionEspirometria();
	});

	$('#valoresAnaliticaNormales').click(function(){
		oyenteBotonAnalitica();
	});

});

function oyenteAlcohol(){
	var alcohol=$('#alcohol').val();
	if(alcohol!=''){
		$('#cajaAlcohol').removeClass('hide');
	}
	else{
		$('#cajaAlcohol').addClass('hide');
	}
}

function oyenteVacunaciones(){
	if($('input[name=checkVacunaciones]:checked').val()=='SI'){
		$('#cajaVacunaciones').removeClass('hide');
	}
	else{
		$('#cajaVacunaciones').addClass('hide');
	}
}

function oyenteCheckAnalitica(){
	if($('input[name=checkAnalitica]:checked').val()=='SI'){
		$('.cajaAnalitica').removeClass('hide');
	}
	else{
		$('.cajaAnalitica').addClass('hide');
	}
}

function oyenteTabaco(){
	if($('#tipoFumador').val()=='Fumador'){
		$('#cajaFumador').removeClass('hide');
	}
	else{
		$('#cajaFumador').addClass('hide');	
	}
}

function oyenteFumador(tablaco){
	var recomendacion='Aconsejamos  abandone  el hábito tabáquico.';
	var recomendaciones=$('#recomendaciones').val().replace(recomendacion,'');

	if(tablaco=='Fumador'){
		if(recomendaciones==''){
			$('#recomendaciones').val(recomendacion);
		}
		else{
			$('#recomendaciones').val(recomendaciones+'\n'+recomendacion);
		}
	}
	else{
		$('#recomendaciones').val(recomendaciones);
	}
}

function oyenteFrecuenciaCardiaca(frecuencia){
	var recomendacion1='La mejor manera de bajar de forma gradual y segura tu frecuencia cardiaca es incorporar a tu rutina el ejercicio aeróbico de forma regular combinada con una dieta saludable. Debe evitar la cafeína, alcohol y en caso de fumar abandonar el hábito tabáquico.';
	var recomendacion2='Debe acudir a su médico para control de la frecuencia cardiaca. Debe evitar la cafeína, alcohol y en caso de fumar abandonar el hábito tabáquico.';
	var recomendaciones=$('#recomendaciones').val().replace(recomendacion1,'').replace(recomendacion2,'');
	var salto='\n';

	if(recomendaciones==''){
		salto='';
	}

	frecuencia=formateaNumeroCalculo(frecuencia);

	if(frecuencia>=95 && frecuencia<=119){
		recomendaciones+=salto+recomendacion1;
		$('#recomendaciones').val(recomendaciones);
	}
	else if(frecuencia>=120){
		recomendaciones+=salto+recomendacion2;
		$('#recomendaciones').val(recomendaciones);
	}
	else{
		$('#recomendaciones').val(recomendaciones);//Para que borre las posibles recomendaciones anteriores sobre FC que ya no aplican
	}
}

function oyenteTensionArterial(){//Función copiada del PHP
	var si=formateaNumeroCalculo($('#sistolica').val());
	var di=formateaNumeroCalculo($('#diastolica').val());
	var recomendaciones=$('#recomendaciones').val();
	var res='';

	if(si>139 && di>89){
		if(si>210 && di<120){
			res='Los valores de la tensión arterial (TA) deben ser valorados y controlados por su médico de cabecera al estar clasificado como HIPERTENSIÓN. Estadio IV (muy severa) según la JNCV.';
		} 
		else if((si>=180 && si<=209) && (di>=110 && di<=119)){
			res='Los valores de la tensión arterial (TA) deben ser valorados y controlados por su médico de cabecera al estar clasificado como HIPERTENSIÓN. Estadio III (severa) según la JNCV.';
		} 
		else if((si>=160 && si<=179) && (di>=100 && di<=109)){
			res='Los valores de la tensión arterial (TA) deben ser valorados y controlados por su médico de cabecera al estar clasificado como HIPERTENSIÓN. Estadio II (moderada) según la JNCV.';
		} 
		else if((si>=140 && si<=159) && (di>=90 && di<=99)){
			res='Los valores de la tensión arterial (TA) deben ser valorados y controlados por su médico de cabecera al estar clasificado como HIPERTENSIÓN. Estadio I (ligera) según la JNCV.';
		}
	} 
	else if(si>139 && di<90){
		res='Los valores de la tensión arterial (TA) deben ser valorados y controlados por su médico de cabecera al estar clasificado como HTA sistólica aislada según la JNCV.';
	} 
	else {
		if(si<130 && di<85){
			res='La tensión arterial indica NORMOTENSIÓN. Normal según la JNCV.';
		} 
		else if((si>=130 && si<=139) && (di>=85 && di<=89)){
			res='La tensión arterial indica NORMOTENSIÓN. Normal alta según la JNCV.';
		}
	}

	if(res!=''){
		
		if(recomendaciones==''){
			$('#recomendaciones').val(res);
		}
		else{
			
			recomendaciones=recomendaciones.replace(res,'');
			recomendaciones=recomendaciones.replace('\n\n','\n');
			recomendaciones=recomendaciones+'\n'+res;
			$('#recomendaciones').val(recomendaciones);	
		}

	}
}

function oyenteOtrasPruebas(valor){
	if(valor=='SI'){
		$('#divOtrasPruebas').removeClass('hide');
	} else {
		$('#divOtrasPruebas').addClass('hide');
	}
}

function oyenteEmpleado(codigoEmpleado){
	if(codigoEmpleado=='NULL'){
		$('#edad').text('-');
	}
	else{
		var consulta=$.post('../listadoAjax.php?include=reconocimientos-medicos&funcion=obtieneDatosEmpleadoReconocimientoMedico();',{'codigoEmpleado':codigoEmpleado},
		function(respuesta){
			$('#codigoPuestoTrabajo').val(respuesta.codigoPuestoTrabajo).selectpicker('refresh');
			$('#edadReconocimiento').val(respuesta.edad);
			$('#fechaNacimiento').val(respuesta.fechaNacimiento);
			$('input[name=sexo][value='+respuesta.sexo+']').attr('checked',true);

			obtieneEpisPuesto($('#codigoPuestoTrabajo option:selected').val());
		},'json');
	}
}

function compruebaOpcionCheckClasificacion(nombreCampo,valor){
	var checked=false;

	if(valor=='SI'){
		checked=true;
	}
	
	$('input[name='+nombreCampo+']').attr('checked',checked);
}

function oyenteCheckRecomendaciones(campo){
	var texto=campo.parent().text().trim()+' ';
	var recomendaciones=$('#recomendaciones').val();

	if(campo.attr('checked')){
		if(recomendaciones==''){
			$('#recomendaciones').val(texto);
		} else {
			$('#recomendaciones').val(recomendaciones+'\n'+texto);
		}
	}
	else{
		recomendaciones=recomendaciones.replace('\n'+texto,'');
		recomendaciones=recomendaciones.replace(texto,'');
		$('#recomendaciones').val(recomendaciones);
	}
}

function compruebaRecomendaciones(){
	var recomendaciones=$('#recomendaciones').val();
	var texto='';
	$("input[name=checkRecomendacion]").each(function( index ) {
		texto=$(this).parent().text().trim()+' ';
		if(recomendaciones.indexOf(texto) > -1){
			$(this).attr('checked','checked');
		}
	});
}


function rellenaDatosPorDefecto(){
	$('#periodicidad').val('Anual').selectpicker('refresh');
	var codigoPuesto=$('#codigoPuestoTrabajo').val();

	if(codigoPuesto!='NULL'){
		var codigoCliente=$('#codigoCliente').val();
		var consulta=$.post('../listadoAjax.php?include=reconocimientos-medicos&funcion=obtieneDatosProtocoloPuesto();',{'codigoPuesto':codigoPuesto,'codigoCliente':codigoCliente},function(respuesta){
			
			if(respuesta.riesgos!=''){
				$('#riesgoPuesto').val(respuesta.riesgos);
			}
			else{
				$('#riesgoPuesto').val('Los recogidos en la evaluación de riesgos.');		
			}

		},'json');
	} 
	else {
		var codigoEmpleado=$('#codigoEmpleado').val();

		$.post('../listadoAjax.php?include=reconocimientos-medicos&funcion=obtienePuestoManualEmpleado();',{'codigoEmpleado':codigoEmpleado},function(respuesta){
			$('#puestoManual').val(respuesta.puestoManual);
			$('#codigoPuestoTrabajo').val(respuesta.codigoPuestoTrabajo).selectpicker('refresh');
		},'json');

		$('#riesgoPuesto').val('Los recogidos en la evaluación de riesgos.');
	}

	$('#riesgosExtralaborales').val('No refiere.');
	
	$('input[name=checkVacunaciones0]').attr('checked',true);
	$('#ejercicio').val('Desconocido').selectpicker('refresh');
	$('#alimentacion').val('Su alimentación es equilibrada-variada').selectpicker('refresh');
	$('#calidadSuenio').val('Bien: 6 o más horas').selectpicker('refresh');
	$('#historiaActual').val('Actualmente se encuentra bien, me refiere desconocer limitación alguna que le dificulte la realización de su trabajo.');

	$('#fracturas').val('No refiere fracturas con limitaciones para su actividad laboral.');
	$('#riesgosLaborales').val('Caídas, movimientos repetidos, posturas forzadas, cargas, sobre esfuerzos, dermatitis, fatiga física, fatiga mental, fatiga postural.');
	$('#minusvaliaReconocida').val('No.');

	$('#inspeccionGeneral').find('option:contains("Buen estado general. Consciente, orientado y colaborador.")').attr('selected',true);
	$('#orl').find('option:contains("Exploración faríngea normal.")').attr('selected',true);
	$('#oftalmologia').find('option:contains("Inspección ocular sin hallazgos específicos.")').attr('selected',true);
	$('#exploracionNeurologica').find('option:contains("Dentro de los límites de la normalidad; no problemas de equilibrio, coordinación, fuerza ni sensibilidad.")').attr('selected',true);
	$('#pielMucosas').find('option:contains("Normal coloración e hidratación.")').attr('selected',true);
	$('#auscultacionPulmonar').find('option:contains("Buen murmullo vesicular en ambos campos pulmonares.")').attr('selected',true);
	$('#aparatoDigestivo').find('option:contains("ABDOMEN: Blando y depresible. No dolor a la palpación profunda. No se evidencia masas ni megalias.")').attr('selected',true);
	$('#extremidades').find('option:contains("Sin edemas ni signos de trombosis venosa profunda.")').attr('selected',true);
	$('#auscultacionCardiaca').find('option:contains("Corazón rítmico sin soplos ni extratonos.")').attr('selected',true);
	$('#sistemaUrogenital').find('option:contains("Puño percusión renal negativa.")').attr('selected',true);
	$('#aparatoLocomotor').find('option:contains("Sin hallazgos específicos. Columna vertebral sin manifestaciones de efectos limitativos en la actualidad.")').attr('selected',true);
	$('inspeccionGeneral,#orl,#oftalmologia,#exploracionNeurologica,#pielMucosas,#auscultacionPulmonar,#aparatoDigestivo,#extremidades,#auscultacionCardiaca,#sistemaUrogenital,#aparatoLocomotor').selectpicker('refresh');


	$('input[name=checkElectrocardiograma][value=SI]').attr('checked',true);
	$('#electrocardiograma').val('Sin alteraciones en la repolarización.');

	$('input[name=checkAnalitica][value=SI]').attr('checked',true);
	$('#hemograma').val('Parámetros dentro de la normalidad.');	
	$('#bioquimica').val('Parámetros dentro de la normalidad.');
	$('#orina').val('Parámetros dentro de la normalidad.');
	$('#analitica').val('Los valores encontrados en los análisis están dentro de la normalidad.');
	oyenteCheckAnalitica();

	$('input[name=checkDinamometria][value=SI]').attr('checked',true);
	$('#dinamometria').val('Normal.');	
	$('#rxTorax').val('Prueba no realizada, según protocolos médicos aplicados.');

	//$('#recomendaciones').val('El resultado de su Tensión Arterial (TA) es:  óptima (clasificación de la hipertensión arterial de acuerdo al JNC-V).\nRecomendamos revisar la visión por lo menos una vez al año. ');
	$('#recomendaciones').val('Recomendamos revisar la visión por lo menos una vez al año.');

	$('#saturacionO2').val('Normal, entre 95-99%').selectpicker('refresh');


	$('#antecedentesPersonales1').val('No HTA, no diabetes, no dislipemia.');
	$('#antecedentesPersonales2').val('');
	$('#antecedentesPersonales3').val('');
	$('#antecedentesPersonales4').val('');
	$('#antecedentesPersonales5').val('');
	$('#antecedentesPersonales6').val('');
	$('#antecedentesPersonales7').val('');
	$('#antecedentesPersonales8').val('');
	$('#antecedentesPersonales9').val('');
	$('#antecedentesFamiliares').val('');
	$('#antecedentesPersonales').val('Riesgo cardiovascular: No HTA, no diabetes, no dislipemia. \nEnf.Cardiorrespiratorios: No cardiopatía, ni broncopatías. \nEnf.Digestivo: No antecedentes digestivos ni hepatobiliares. \nEnf.Genitourinario: No antecedentes urinarios. \nEnf.Ap.locomotor y neurológico: No refiere. \nEnf.ORL: No refiere. \nEnf.Oftalmológico: No refiere. \nCirugía: No refiere.');

	$('#botonExploracioNormalPulsado').val('SI');
}

function calculaIMC(){
	var talla=formateaNumeroCalculoIMC($('#talla').val());
	var peso=formateaNumeroCalculoIMC($('#peso').val());

	if(talla!='' && peso!=''){
		talla=talla/100;
		var imc=peso/(Math.pow(talla,2));

		$('#imc').val(formateaNumeroWeb(imc));

		generaObservacionesIMC(imc);

	}
	else{
		$('#imc').val('');
	}
}


function generaObservacionesIMC(imc){
	var peso='';

	if(imc>=50){
		peso='Su I.M.C. indica Obesidad tipo IV (extrema).';
	} 
	else if (imc>=40) {
		peso='Su I.M.C. indica Obesidad tipo III (morbida).';
	} 
	else if (imc>=35) {
		peso='Su I.M.C. indica Obesidad tipo II.';
	} 
	else if (imc>=30) {
		peso='Su I.M.C. indica Obesidad tipo I.';
	} 
	else if (imc>=27) {
		peso='Su I.M.C. indica Sobrepeso grado II (preobesidad).';
	} 
	else if (imc>=25) {
		peso='Su I.M.C. indica Sobrepeso grado I.';
	} 
	else if (imc>=18){
		peso='Su I.M.C. indica Normopeso.';
	} 
	else if (imc<18){
		peso='Su I.M.C. indica Peso insuficiente.';
	}

	var recomendaciones=$('#recomendaciones').val().replace(peso,'');

	if(recomendaciones==''){
		$('#recomendaciones').val(peso);
	}
	else{
		$('#recomendaciones').val(recomendaciones+'\n'+peso);	
	}
}

function formateaNumeroCalculoIMC(numero){
	var res=numero;

	if(isNaN(res)){
		res=formateaNumeroCalculo(res);
	}

	return res;
}


function obtieneDatosReconocimientoAnterior(){
	var codigoEmpleado=$('#codigoEmpleado').val();
	if(codigoEmpleado=='NULL'){
		alert('Por favor, seleccione primero un empleado.');
	}
	else{
		var consulta=$.post('../listadoAjax.php?include=reconocimientos-medicos&funcion=consultaReconocimientoMedicoAnteriorEmpleado();',{'codigoEmpleado':codigoEmpleado});
		consulta.done(function(respuesta){
			if(respuesta=='NO'){
				alert('No hay reconocimientos anteriores para el empleado seleccionado.');
			}
			else{
				var codigoContrato=$('#codigoContrato').val();
				creaFormulario('?',{'codigoReconocimientoAnterior':respuesta,'codigoContrato':codigoContrato},'post');
			}
		});
	}
}

function oyenteCerrado(){
	if($('input[name=cerrado]:checked').val()=='SI'){
		$('input[name=bloqueado][value=SI]').attr('checked',true);
	}
}

function oyenteBloqueado(){
	if($('input[name=bloqueado]:checked').val()=='SI' || $('input[name=bloqueado][type=hidden]').val()=='SI'){
		$(':submit.btn-propio,.widget-header .btn-propio').attr('disabled',true);
		$(':submit.btn-propio,.widget-header .btn-propio').addClass('hide');
	}
	else{
		$(':submit.btn-propio,.widget-header .btn-propio').attr('disabled',false);
		$(':submit.btn-propio,.widget-header .btn-propio').removeClass('hide');
	}
}


/*
	La siguiente función cuenta todas las ocurrencias de los * en los campos donde se vuelcan los datos de los PDF de las analíticas.
	Los * indican valores altos o bajos, por lo que se cuentan el número de ellos para poner un indicador en la pestaña de analíticas
*/
function cuentaDatosAnomalosAnalitica(){
	var hemograma=$('#hemograma').val();
	var bioquimica=$('#bioquimica').val();
	var otrasPruebas=$('#otrasPruebas').val();
	//var otrasPruebasFuncionales=$('#otrasPruebasFuncionales').val();
	var orina=$('#orina').val();

	var contador=0;

	contador=parseInt(contador)+(hemograma.match(/\*/g) || []).length;
	contador=parseInt(contador)+(bioquimica.match(/\*/g) || []).length;
	contador=parseInt(contador)+(otrasPruebas.match(/\*/g) || []).length;
	//contador=parseInt(contador)+(otrasPruebasFuncionales.match(/\*/g) || []).length;
	contador=parseInt(contador)+(orina.match(/\*/g) || []).length;

	//El siguiente selector se corresponde con la pestaña de analíticas
	if(contador>0){
		$('a[href=#7]').append("<div class='avisoAnalitica badge'>"+contador+"</div>");
	}
}


function tipoReconocimiento(valor,div){
	if(valor=='Otros'){
		$(div).removeClass('hide');
	} else {
		$(div).addClass('hide');
	}
}

function obtenerFechaSiguiente(tipo,fecha){
	fecha=fecha.split('/');
	var res=new Array();
	res['mes']='';
	res['anio']='';
	if(tipo=='Anual'){
		res['mes']=fecha[1];
		res['anio']=parseInt(fecha[2])+1;
	} else if(tipo=='Semestral') {
		res=preparaSiguienteFecha(6,fecha);
	} else if(tipo=='Trimestral') {
		res=preparaSiguienteFecha(3,fecha);
	} else if(tipo=='Mensual') {
		res=preparaSiguienteFecha(1,fecha);
	}
	$('#mesSiguienteReconocimiento').val(res['mes']);
	$('#mesSiguienteReconocimiento').selectpicker('refresh');
	$('#anioSiguienteReconocimiento').val(res['anio']);
}

function preparaSiguienteFecha(suma,fecha){
	var res=new Array();
	res['mes']=parseInt(fecha[1])+suma;
	res['anio']=parseInt(fecha[2]);
	if(res['mes']>12){
		res['mes']-=12;
		res['anio']+=1;
	}
	if(res['mes']<10){
		res['mes']='0'+res['mes'];
	}
	return res;
}

function obtieneEpisPuesto(codigoPuesto){
	if(codigoPuesto!='NULL'){
		$('#codigoEpi0').val('NULL');
		$('#codigoEpi0').selectpicker('refresh');
		var elimina=false;
		$("#tablaEpis tbody tr").each(function( index ) {
  			if(elimina){
  				$(this).remove();
  			} else {
  				elimina=true;
  			}
		});
		var consulta=$.post('../listadoAjax.php?include=reconocimientos-medicos&funcion=obtieneEpisPuesto();',{'codigoPuesto':codigoPuesto},
		function(respuesta){
			for(var i=0;i<respuesta.length;i++){
				if(i>0){
					insertaFila('tablaEpis');
				}
				$('#codigoEpi'+i).val(respuesta[i]);
				$('#codigoEpi'+i).selectpicker('refresh');
			}
		},'json');

	}
}


function obtieneDatosProtocoloPuesto(codigoPuesto){
	if(codigoPuesto!='NULL'){
		var codigoCliente=$('#codigoCliente').val();

		var consulta=$.post('../listadoAjax.php?include=reconocimientos-medicos&funcion=obtieneDatosProtocoloPuesto();',{'codigoPuesto':codigoPuesto,'codigoCliente':codigoCliente},function(respuesta){
			$('#riesgoPuesto').val(respuesta.riesgos);
			$('#protocoloClasificacion').val(respuesta.protocolos);
			
			if(respuesta.otrasPruebas=='SI'){
				$('input[name=checkOtrasPruebasFuncionales][value=SI]').attr('checked',true);
				$('#divOtrasPruebas').removeClass('hide');
				$('#tablaOtrasPruebas tbody').html(respuesta.pruebas);
				$('#tablaOtrasPruebas tbody .selectpicker').selectpicker();
			}

		},'json');

	}
}


function oyenteTituloVentana(){
	var empresa=$('#empresa').text();

	if($('#empleado').text()!=undefined && $('#empleado').text()!=''){
		var empleado=$('#empleado').text();
	}
	else{
		var empleado=$('#codigoEmpleado').find('option:selected').text();
	}

	var titulo='Reconocimiento de '+empleado+' - '+empresa;
	if(titulo.length>120){
		titulo=titulo.slice(0,120)+'...';
	}


	$('.widget-header h3').text(titulo);
}


function oyenteTablaEnfermedadesAntecedentes(){

	$(document).on('change','#tablaEnfermedadesReconocimiento input[type=checkbox]',function(){
		
		var campo=$(this);
		var marcado=campo.attr('checked');
		var nombre=campo.attr('name');
		var fila=obtieneFilaCampo(campo);

		var enfermedad=campo.parent().parent().parent().find('td:nth-child(1)').text().trim();
		var recomendaciones=$('#descripcionEnfermedad'+fila).text().trim();
		var n=$('#numCampo'+fila).val();

		gestionaTextoTablaEnfermedadesAntecedentes(marcado,nombre,'#antecedentesPersonales'+n,'Personales',enfermedad);
		gestionaTextoTablaEnfermedadesAntecedentes(marcado,nombre,'#antecedentesFamiliares','Familiares',enfermedad);
		gestionaTextoTablaEnfermedadesAntecedentes(marcado,nombre,'#recomendaciones','Recomendaciones',recomendaciones);

	});
}

function gestionaTextoTablaEnfermedadesAntecedentes(checkMarcado,nombreCampo,selectorDestino,nombreComprobar,texto){
	if(checkMarcado && nombreCampo.indexOf(nombreComprobar)>-1){

		//Parte de supresión de textos por defecto
		$(selectorDestino).val($(selectorDestino).val().replace('No HTA, no diabetes, no dislipemia.',''));
		//Fin parte de supresión de textos por defecto

		if(selectorDestino.indexOf('Familiares')>-1){
			$(selectorDestino).val($(selectorDestino).val()+texto+'\n');
		}
		else{
			$(selectorDestino).val($(selectorDestino).val()+texto+' ');
		}
	}
	else if(!checkMarcado && nombreCampo.indexOf(nombreComprobar)>-1){
		$(selectorDestino).val($(selectorDestino).val().replace(texto+' ',''));
	}

}

function oyenteEpis(){
	var marcado=$('input[name="toleraEpis"]:checked').val();
	
	if(marcado=='SI'){
		$('#cajaIntoleranciasEpis').addClass('hide');
	}
	else{
		$('#cajaIntoleranciasEpis').removeClass('hide');
	}
}

function oyenteEstudioVision(){
	var temporizador=false;
	var campo;
	//compruebaLineaBinocularEstudioVision();
	$('#tablaEstudioVision').find('input').on('keyup',function(){
		campo=$(this);

		if(temporizador!=false){
			clearTimeout(temporizador);
		}

		temporizador=setTimeout(function(){
			compruebaCampoEstudioVision(campo);
		},500)
	});
}

function compruebaCampoEstudioVision(campo){
	var valor=campo.val();

	if(valor!='0' && valor!='2' && valor!='4' && valor!='6' && valor!='8' && valor!='10' && valor!='12'){
		campo.val('');
	}

	compruebaLineaBinocularEstudioVision();
}

/*
	La siguiente función comprueba los campos de la línea "Binocular" de la tabla de estudio de visión, para cambiar el color de la pestaña "Visión" en base a los siguientes criterios:
	-	Si todos los campos son >=8, la pestaña debe ser verde.
	-	Si algún campo es >=6 y <8, la pestaña debe ser naranja.
	-	Si algún campo es <6, la pestaña debe ser roja.
*/
function compruebaLineaBinocularEstudioVision(){
	var campo1=$('#estudioVision9').val()==''?0:$('#estudioVision9').val();
	var campo2=$('#estudioVision10').val()==''?0:$('#estudioVision10').val();
	var campo3=$('#estudioVision11').val()==''?0:$('#estudioVision11').val();
	var campo4=$('#estudioVision12').val()==''?0:$('#estudioVision12').val();
	var menuColoresPestania=$('a[href="#5"]').parent().find('.menu-colores');

	campo1=Number(campo1);
	campo2=Number(campo2);
	campo3=Number(campo3);
	campo4=Number(campo4);

	if(
		(campo1>=8 || campo2>=8 || campo3>=8 || campo4>=8) &&
		(
			(campo1>=8 || campo1==0) && 
			(campo2>=8 || campo2==0) && 
			(campo3>=8 || campo3==0) && 
			(campo4>=8 || campo4==0) 
		)
	){
		menuColoresPestania.find('a[estado="correcto"]').click();
	$('#observacionesEstudioVision').val('Normal.');
	}
	else if(campo1==0 && campo2==0 && campo3==0 && campo4==0){
		menuColoresPestania.find('a[estado=""]').click();
	}
	else if(campo1<6 || campo2<6 || campo3<6 || campo4<6){
		//menuColoresPestania.find('a[estado="anormal"]').click();
		menuColoresPestania.find('a[estado="advertencia"]').click();
		$('#observacionesEstudioVision').val('Se aconseja control de la agudeza visual por su oftalmólogo.');
	}
	else{
		menuColoresPestania.find('a[estado="advertencia"]').click();
		$('#observacionesEstudioVision').val('Se aconseja control de la agudeza visual por su oftalmólogo.');
	}
}

function oyenteEstudioAudicion(){
	var temporizador=false;

	$('#tablaEstudioAudicion').find('input').on('keypress',function(){

		if(temporizador!=false){
			clearTimeout(temporizador);
		}

		temporizador=setTimeout(function(){
			compruebaCriterioEliEstudioAudicion();
		},500)
	});
}

function compruebaCriterioEliEstudioAudicion(){
	var menuColoresPestania=$('a[href="#6"]').parent().find('.menu-colores');

	var campos=[
		$('#estudioAudiometria1').val(),
		$('#estudioAudiometria2').val(),
		$('#estudioAudiometria3').val(),
		$('#estudioAudiometria4').val(),
		$('#estudioAudiometria5').val(),
		$('#estudioAudiometria6').val(),
		$('#estudioAudiometria7').val(),
		$('#estudioAudiometria8').val(),
		$('#estudioAudiometria9').val(),
		$('#estudioAudiometria10').val(),
		$('#estudioAudiometria11').val(),
		$('#estudioAudiometria12').val(),
		$('#estudioAudiometria13').val(),
		$('#estudioAudiometria14').val()
	]

	var res='correcto';
	var valores=[]
	var sinRellenar=false;

	for(var i=0;i<campos.length;i++){
		var valor=Number(campos[i]);

		if(valor==''){
			res='';
			sinRellenar=true;
			break;
		}
		else if(valor>25 && valor<=55 && res!='anormal'){
			res='advertencia';
		}
		else if(valor>55){
			res='anormal';
		}

		valores.push(valor);
	}
	
	if(!sinRellenar){
		menuColoresPestania.find('a[estado="'+res+'"]').click();

		var valoresOidoDerecho=valores.slice(0,7);
		var valoresOidoIzquierdo=valores.slice(7);

		var valoracionOidoDerecho=calculaResultadoAudiometriaPorOido(valoresOidoDerecho);
		var valoracionOidoIzquierdo=calculaResultadoAudiometriaPorOido(valoresOidoIzquierdo);

		$('#observacionesAudiometria').text('Oido derecho: '+valoracionOidoDerecho+'.\n'+'Oido izquierdo: '+valoracionOidoIzquierdo+'.');
	}
	
}


function calculaResultadoAudiometriaPorOido(valores){
	var valoracion='';
	for(var i=0,medicion=1;i<valores.length;i++,medicion++){
		var valor=valores[i];

		if(medicion<5 && valor<=25){
			valoracion+='a';
		}
		else if(medicion<5 && valor>25 && valor<=55){
			valoracion+='e';
		}
		else if(medicion<5 && valor>55){
			valoracion+='f';
		}
		else if(medicion>=5 && valor<=25){
			valoracion+='b';
		}
		else if(medicion>=5 && valor>25 && valor<=55){
			valoracion+='c';
		}
		else if(medicion>=5 && valor>55){
			valoracion+='d';
		}

	}

	if(valoracion=='aaaabbb'){
		valoracion='Normal (a-b)';
	}
	else if(valoracion=='aaaaccc'){
		valoracion='Trauma acústico inicial (a-c)';
	}
	else if(valoracion=='aaaaddd' || valoracion=='aaaaddc' || valoracion=='aaaadcc'){
		valoracion='Trauma acústico avanzado (a-d / a-d-c)';
	}
	else if(
		valoracion=='aaaeccc' || valoracion=='aaeeccc' || valoracion=='aeeeccc' ||
		valoracion=='aaaeddd' || valoracion=='aaeeddd' || valoracion=='aeeeddd' ||
		valoracion=='aaaeddc' || valoracion=='aaeeddc' || valoracion=='aeeeddc' || valoracion=='aaaedcc' || valoracion=='aaeedcc' || valoracion=='aeeedcc'
	){
		valoracion='Hipoacusia leve (a-e-c / a-e-d / a-e-d-c)';
	}
	else if(valoracion=='eeeeddd' || valoracion=='eeeeddc' || valoracion=='eeeedcc'){
		valoracion='Hipoacusia moderada (e-d /e-d-c)';
	}
	else if(
		valoracion=='eeefddd' || valoracion=='eeffddd' || valoracion=='efffddd' ||
		valoracion=='eeefddc' || valoracion=='eeffddc' || valoracion=='efffddc' ||
		valoracion=='eeefdcc' || valoracion=='eeffdcc' || valoracion=='efffdcc'
	){
		valoracion='Hipoacusia avanzada (e-f-d / e-f-d-c)';
	}
	else{
		valoracion='Otras alteraciones';
	}

	return valoracion;
}

function oyenteResultadoDinamometria(){
	
	$('#manoDerecha,#manoIzquierda,#manoDominante').change(function(){
		var manoDominante=$('#manoDominante').val();
		var mano=false;
		var edad=$('#edad').text();
		var sexo=$('input[name=sexo]:checked').val();

		if(manoDominante=='Derecha'){
			mano=$('#manoDerecha').val();
		}
		else if(manoDominante=='Izquierda'){
			mano=$('#manoIzquierda').val();
		}


		if(mano && edad!='' && edad>0){
			calculaResultadoDinamometria(mano,edad,sexo);
		}

	});

}

function calculaResultadoDinamometria(valorMano,edad,sexo){
	var res;
	var valoresSexo;
	var indiceEdad;
	var rangoSeleccionado;

	//Esquema de valores en base a tabla enviada
	var valoresReferencia={
		'hombre':
		{
			'10-11':[12.6,22.4],
			'12-13':[19.4,31.2],
			'14-15':[28.5,44.3],
			'16-17':[32.6,52.4],
			'18-19':[35.7,55.5],
			'20-24':[36.8,56.6],
			'25-29':[37.7,57.5],
			'30-34':[36.0,55.8],
			'35-39':[35.8,55.6],
			'40-44':[35.5,55.3],
			'45-49':[34.7,54.5],
			'50-54':[32.9,50.7],
			'55-59':[30.7,48.5],
			'60-64':[30.2,48.0],
			'65-69':[28.2,44.0],
			'70-99':[21.3,35.1]
		},
		'mujer':
		{
			'10-11':[11.8,21.6],
			'12-13':[14.6,24.4],
			'14-15':[15.5,27.3],
			'16-17':[17.2,29.0],
			'18-19':[19.2,31.0],
			'20-24':[21.5,35.3],
			'25-29':[25.6,41.4],
			'30-34':[21.5,35.3],
			'35-39':[20.3,34.1],
			'40-44':[18.9,32.7],
			'45-49':[18.6,32.4],
			'50-54':[18.1,31.9],
			'55-59':[17.7,31.5],
			'60-64':[17.2,31.0],
			'65-69':[15.4,27.2],
			'70-99':[14.7,24.5]
		}
	};

	if(sexo=='V'){
		valoresSexo=valoresReferencia['hombre'];
	}
	else{
		valoresSexo=valoresReferencia['mujer'];	
	}

	indiceEdad=obtieneIndiceEdadResultadoDinamometria(edad);
	rangoSeleccionado=valoresSexo[indiceEdad];

	if(valorMano.indexOf('.')==-1){
		valorMano=formateaNumeroCalculo(valorMano);
	}

	if(valorMano<rangoSeleccionado[0]){
		res='Débil';
	}
	else if(valorMano>rangoSeleccionado[1]){
		res='Fuerte';
	}
	else{
		res='Normal';
	}

	$('#resultadoDinamometria').val(res).selectpicker('refresh');
}

function obtieneIndiceEdadResultadoDinamometria(edad){
	var res='';

	edad=Number(edad);

	if(edad<=11){
		res='10-11';
	}
	else if(edad<=13){
		res='12-13';
	}
	else if(edad<=15){
		res='14-15';
	}
	else if(edad<=17){
		res='16-17';
	}
	else if(edad<=19){
		res='18-19';
	}
	else if(edad<=24){
		res='20-24';
	}
	else if(edad<=29){
		res='25-29';
	}
	else if(edad<=34){
		res='30-34';
	}
	else if(edad<=39){
		res='35-39';
	}
	else if(edad<=44){
		res='40-44';
	}
	else if(edad<=49){
		res='45-49';
	}
	else if(edad<=54){
		res='50-54';
	}
	else if(edad<=59){
		res='55-59';
	}
	else if(edad<=64){
		res='60-64';
	}
	else if(edad<=69){
		res='65-69';
	}
	else if(edad>69){
		res='70-99';
	}

	return res;
}


function oyenteIndicePaquetesAnio(){

	var aniosFumando=$('#aniosFumando').val().trim();
	var tabacoDia=$('#tabacoDia').val().trim();

	if(aniosFumando!='' && tabacoDia!=''){
		aniosFumando=Number(aniosFumando);
		tabacoDia=Number(tabacoDia);

		var res=aniosFumando*tabacoDia/20;
		$('#indicePaquetesAnio').val(formateaNumeroWeb(res));
	}
}

function oyenteBebidasEstimulantes(){
	
	if($('input[name=bebidasEstimulantes]:checked').val()=='SI'){
		$('#cajaBebidasEstimulantes').removeClass('hide');
	}
	else{
		$('#cajaBebidasEstimulantes').addClass('hide');
	}

}

function oyenteEjercicios(){
	var ejercicio=$('#ejercicio').val();

	if(ejercicio=='Realiza ejercicio físico'){
		$('#cajaEjercicios').removeClass('hide');
	}
	else{
		$('#cajaEjercicios').addClass('hide');
		var recomendaciones=$('#recomendaciones').val().trim();

		if(ejercicio!='Desconocido' && recomendaciones.indexOf('Aconsejamos realizar actividades físicas de forma regular.')==-1){
			var recomendacion='Aconsejamos realizar actividades físicas de forma regular.';

			if(recomendaciones==''){
				$('#recomendaciones').val(recomendacion);
			}
			else{
				$('#recomendaciones').val(recomendaciones+'\n'+recomendacion);	
			}

		}
	}
}

function observacionesVision(checked){
	if(checked=='checked'){
		$('#divObservacionesVision').removeClass('hide');
	} else {
		$('#divObservacionesVision').addClass('hide');
		$('#observacionesVision').val('');
	}
}

function gafasYLentillas(checked){
	if(checked=='checked'){
		$('#divTipoGafasLentillas').removeClass('hide');
	} else {
		$('#divTipoGafasLentillas').addClass('hide');
		$('#tipoGafasLentillasTexto').val('');
	}
}

function oyenteDescPersona(){
	var sexo = $('input[name=sexo]:checked').val() == 'V'?'Varón':'Mujer';
 	$('#sexoDato').html(sexo);
	$('#pesoDato').html($('#peso').val()+' kg');
	$('#tallaDato').html($('#talla').val()+' cm');
}


function oyenteEspirometria(){
	var res='';
	var menuColoresPestania=$('a[href="#15"]').parent().find('.menu-colores');

	var fvc=$('#fvc').val();
	var fev1=$('#fev1').val();
	var itiff=$('#itiff').val();
	var valoracion='Patrón obstructivo';

	if(fvc!='' && fev1!='' && itiff!=''){
		fvc=Number(fvc);
		fev1=Number(fev1);
		itiff=Number(itiff);

		res='advertencia';

		if((fvc>=80 && fvc<=120) && (fev1>=80 && fev1<=120) && itiff>=75){
			res='correcto';
			valoracion='Normal';
		}
		
	}

	menuColoresPestania.find('a[estado="'+res+'"]').click();
	$('#valoracionEspirometrica').val(valoracion).selectpicker('refresh');
	oyenteValoracionEspirometria();
}



function oyenteGenericoCampoTexto(campoRadio){
	var contenedorTexto=campoRadio.parent().parent().parent().next();
	if(campoRadio.val()=='NO'){
		contenedorTexto.addClass('hide');
	}
	else{
		contenedorTexto.removeClass('hide');
	}
}

function oyenteGenericoCampoTextoAsociadoCheck(campoCheck){
	var id=campoCheck.attr('name')+'Texto';
	
	if(campoCheck.attr('checked')){
		$('#'+id).removeClass('hide');
	}
	else{
		$('#'+id).addClass('hide');
	}
}

function vuelcaDatosRadioEnRecomendaciones(valorCheck,textoVolcar,alPrincipio){
	var recomendaciones=$('#recomendaciones').val().trim();

	if(valorCheck=='SI'){
		
		if(recomendaciones==''){
			$('#recomendaciones').val(textoVolcar);
		}
		else if(alPrincipio!=undefined && alPrincipio){
			$('#recomendaciones').val(textoVolcar+'\n'+recomendaciones);
		}
		else{
			$('#recomendaciones').val(recomendaciones+'\n'+textoVolcar);
		}
	}
	else{
		$('#recomendaciones').val(recomendaciones.replace(textoVolcar,''));
		$('#recomendaciones').val($('#recomendaciones').val().replace('\n\n','\n'));
	}
}


function oyenteTablaComentariosAnalitica(){

	$(document).on('change','#tablaComentariosAnalitica td:nth-child(2) input[type=checkbox]',function(){
		
		var campo=$(this);
		var marcado=campo.attr('checked');
		//var nombre=campo.attr('name');
		var fila=obtieneFilaCampo(campo);

		var recomendaciones=$('#recomendacionesComentario'+fila).text().trim();

		//gestionaTextoTablaEnfermedadesAntecedentes(marcado,nombre,'#recomendaciones','Comentario',recomendaciones);

		if(marcado){
			marcado='SI';
		}
		else{
			marcado='NO';
		}

		vuelcaDatosRadioEnRecomendaciones(marcado,recomendaciones);

	});
}

function oyenteValoracionEspirometria(){
	var valoracion=$('#valoracionEspirometrica').val();

	$('#observacionesEspirometria').val(valoracion);
}


function oyenteBotonAnalitica(){
	$('#hemograma').val('Parámetros dentro de la normalidad.');
	$('#bioquimica').val('Parámetros dentro de la normalidad.');
	$('#orina').val('Parámetros dentro de la normalidad.');
	$('#analitica').val('Los valores encontrados en los análisis que se han efectuado están dentro de la normalidad.');
}

</script>

<script src="../js/desplegables.js?v=<?=time()?>" type="text/javascript"></script>

</div><!-- contenido -->

<?php include_once('../pie.php'); ?>