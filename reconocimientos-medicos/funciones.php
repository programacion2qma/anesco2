<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales
//Inicio de funciones específicas


//Parte de gestión de reconocimientos médicos


function operacionesReconocimientos(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaReconocimientoMedico();
	}
	elseif(isset($_POST['codigoEmpleado'])){
		$res=creaReconocimientoMedico();
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoReconocimiento($_POST['elimina']);
	}

	mensajeResultado('codigoEmpleado',$res,'reconocimiento');
    mensajeResultado('elimina',$res,'reconocimiento', true);
}


function creaReconocimientoMedico(){
	formateaCamposReconocimiento();
	$res=insertaDatos('reconocimientos_medicos',array(time(),time().'1'),'../documentos/analiticas-procesadas');

	if($res){
		$codigoReconocimiento=$res;
		$datos=arrayFormulario();

		conexionBD();
		$res=insertaEpisReconocimientoMedico($codigoReconocimiento,$datos);
		$res=$res && insertaPruebasReconocimientoMedico($codigoReconocimiento,$datos);
		$res=$res && insertaEnfermedadesSelectorEnfermedadesReconocimientoMedico($codigoReconocimiento,$datos);
		$res=$res && insertaComentariosSelectorComentariosReconocimientoMedico($codigoReconocimiento,$datos);
		$res=$res && insertaBebidasEstimulantesReconocimientoMedico($codigoReconocimiento,$datos);
		$res=$res && insertaEjerciciosReconocimientoMedico($codigoReconocimiento,$datos);
		$res=$res && actualizaFechaNacimientoSexoEmpleado($datos);

		$res=$res && compruebaEnvioCorreosNotificacion($codigoReconocimiento,$datos);

		cierraBD();
	}

	return $res;
}

function actualizaReconocimientoMedico(){
	formateaCamposReconocimiento();
	$res=actualizaDatos('reconocimientos_medicos',time(),'../documentos/analiticas-procesadas');

	if($res){
		$datos=arrayFormulario();
		$codigoReconocimiento=$datos['codigo'];

		conexionBD();
		$res=insertaEpisReconocimientoMedico($codigoReconocimiento,$datos,true);
		$res=$res && insertaPruebasReconocimientoMedico($codigoReconocimiento,$datos,true);
		$res=$res && insertaEnfermedadesSelectorEnfermedadesReconocimientoMedico($codigoReconocimiento,$datos,true);
		$res=$res && insertaComentariosSelectorComentariosReconocimientoMedico($codigoReconocimiento,$datos,true);
		$res=$res && insertaBebidasEstimulantesReconocimientoMedico($codigoReconocimiento,$datos,true);
		$res=$res && insertaEjerciciosReconocimientoMedico($codigoReconocimiento,$datos,true);
		$res=$res && actualizaFechaNacimientoSexoEmpleado($datos);

		$res=$res && compruebaEnvioCorreosNotificacion($codigoReconocimiento,$datos);

		cierraBD();
	}

	return $res;
}

function actualizaFechaNacimientoSexoEmpleado($datos){
	$res=true;

	if(isset($datos['fechaNacimiento'])){
		$fechaNacimiento=$datos['fechaNacimiento'];
		$sexo=$datos['sexo'];
		$codigoEmpleado=$datos['codigoEmpleado'];
		
		$res=consultaBD("UPDATE empleados SET fechaNacimiento='$fechaNacimiento', sexoEmpleado='$sexo' WHERE codigo='$codigoEmpleado';");
	}

	return $res;
}

function formateaCamposReconocimiento(){
	if(isset($_POST['indicePaquetesAnio'])){
		$_POST['indicePaquetesAnio']=formateaNumeroWeb($_POST['indicePaquetesAnio'],true);
	}
}

function compruebaEnvioCorreosNotificacion($codigoReconocimiento,$datos){
	$res=true;
	
	if(($datos['aptoVisible']=='SI' && $datos['aptoVisibleAnterior']=='NO') || isset($datos['enviarApto'])){
		$res=$res && enviaCorreoAvisoApto($datos);
	}

	if(($datos['reconocimientoVisible']=='SI' && $datos['reconocimientoVisibleAnterior']=='NO') || isset($datos['enviarReco'])){
		$res=$res && enviaCorreoAvisoReconocimientoMedico($datos);
	}

	return $res;
}


function enviaCorreoAvisoApto($datos){
	$res=true;

	$cliente=consultaBD("SELECT EMPNOMBRE AS nombre, EMPEMAILPRINC AS email, CONCAT(empleados.nombre,' ',empleados.apellidos) AS empleado, usuarios.usuario, usuarios.clave
						 FROM clientes INNER JOIN empleados ON clientes.codigo=empleados.codigoCliente 
						 INNER JOIN usuarios_clientes ON clientes.codigo=usuarios_clientes.codigoCliente
						 INNER JOIN usuarios ON usuarios_clientes.codigoUsuario=usuarios.codigo
						 INNER JOIN reconocimientos_medicos ON empleados.codigo=reconocimientos_medicos.codigoEmpleado
						 WHERE reconocimientos_medicos.codigo='".$datos['codigo']."';",false,true);

    if(trim($cliente['email'])!=''){

    	$mensaje="
    	<a href='http://anescoprl.es/'><img src='https://crmparapymes.com.es/anesco2/img/correoCartaAptitud.jpg' style='width:50%;height:auto' /></a><br />
    	<br />
    	A/A: ".$cliente['nombre']."<br />
    	<br />
    	Se ha puesto a su disposición la <strong>Carta de Aptitud</strong> del empleado <strong>".trim($cliente['empleado'])."</strong>.<br />
    	<br />
    	Para proceder a su descarga, acceda al área privada de <strong>ANESCO SALUD Y PREVENCIÓN</strong> con los siguientes datos:<br />
    	<br />
    	<ul>
    		<li>URL: <strong><a href='http://anescoprl.es/'>http://anescoprl.es/</a></strong><br /><br /></li>
    		<li>Usuario: <strong>".$cliente['usuario']."</strong><br /><br /></li>
    		<li>Clave: <strong>".$cliente['clave']."</strong></li>
    	</ul>
    	<br />
    	Sin otro particular, aprovechamos la ocasión para enviarle un cordial saludo.<br />
    	<br />
    	<p style='color:#61B4DD;font-weight:bold;'>ANESCO SALUD Y PREVENCIÓN, SL</p><br />
    	<img src='https://crmparapymes.com.es/anesco2/img/logo.png' /><br />
    	<br />
    	<a href='http://anescoprl.es'>http://anescoprl.es</a><br />
    	<a href='tel:954109393'>954 109 393</a><br />
    	<p style='color:#AAA;font-size:10px;font-style:italic;text-align:justify;width:50%;'>De conformidad con lo dispuesto en el Reglamento (UE) 2016/679 de 27 de abril de 2016, y 
    	normativas vigentes sobre protección de datos personales, le informamos que sus datos personales, serán tratados bajo la responsabilidad de ANESCO SALUD Y PREVENCION SL 
    	con la finalidad de mantener la relación contractual y comercial con usted. Los datos se conservarán mientras exista un interés mutuo para ello, en función de los plazos 
    	legales aplicables. Los datos no serán comunicados a terceros, salvo obligación legal. Le informamos que puede ejercer los derechos de acceso, rectificación, supresión, 
    	limitación, portabilidad y oposición al tratamiento, enviando un correo electrónico a info@anescoprl.es.</p>";

	   	$res=enviaEmail($cliente['email'],'Descarga de Carta de Aptitud - ANESCO',$mensaje);
	}

	return $res;
}

function enviaCorreoAvisoReconocimientoMedico($datos){
	$res=true;

	$empleado=consultaBD("SELECT CONCAT(empleados.nombre,' ',empleados.apellidos) AS empleado, usuarios.usuario, usuarios.clave, empleados.email, reconocimientos_medicos.fecha
						  FROM empleados INNER JOIN usuarios_empleados ON empleados.codigo=usuarios_empleados.codigoEmpleado
						  INNER JOIN usuarios ON usuarios_empleados.codigoUsuario=usuarios.codigo
						  INNER JOIN clientes ON empleados.codigoCliente=clientes.codigo
						  INNER JOIN reconocimientos_medicos ON empleados.codigo=reconocimientos_medicos.codigoEmpleado
						  WHERE reconocimientos_medicos.codigo='".$datos['codigo']."';",false,true);

    if(trim($empleado['email'])!=''){

    	$mensaje="
    	<a href='http://anescoprl.es/'><img src='https://crmparapymes.com.es/anesco2/img/correoReco.jpg' style='width:50%;height:auto' /></a><br />
    	<br />
    	A/A: ".trim($empleado['empleado'])."<br />
    	<br />
    	Se ha puesto a su disposición el informe de su <strong>Reconocimiento Médico</strong> realizado el día <strong>".formateaFechaWeb($empleado['fecha'])."</strong>.<br />
    	<br />
    	Para proceder a su descarga, acceda al área privada de <strong>ANESCO SALUD Y PREVENCIÓN</strong> con los siguientes datos:<br />
    	<br />
    	<ul>
    		<li>URL: <strong><a href='http://anescoprl.es/'>http://anescoprl.es/</a></strong><br /><br /></li>
    		<li>Usuario: <strong>".$empleado['usuario']."</strong><br /><br /></li>
    		<li>Clave: <strong>".$empleado['clave']."</strong></li>
    	</ul>
    	<br />
    	Sin otro particular, aprovechamos la ocasión para enviarle un cordial saludo.<br />
    	<br />
    	<p style='color:#61B4DD;font-weight:bold;'>ANESCO SALUD Y PREVENCIÓN, SL</p><br />
    	<img src='https://crmparapymes.com.es/anesco2/img/logo.png' /><br />
    	<br />
    	<a href='http://anescoprl.es'>http://anescoprl.es</a><br />
    	<a href='tel:954109393'>954 109 393</a><br />
    	<p style='color:#AAA;font-size:10px;font-style:italic;;text-align:justify;width:50%;'>De conformidad con lo dispuesto en el Reglamento (UE) 2016/679 de 27 de abril de 2016, y 
    	normativas vigentes sobre protección de datos personales, le informamos que sus datos personales, serán tratados bajo la responsabilidad de ANESCO SALUD Y PREVENCION SL 
    	con la finalidad de mantener la relación contractual y comercial con usted. Los datos se conservarán mientras exista un interés mutuo para ello, en función de los plazos 
    	legales aplicables. Los datos no serán comunicados a terceros, salvo obligación legal. Le informamos que puede ejercer los derechos de acceso, rectificación, supresión, 
    	limitación, portabilidad y oposición al tratamiento, enviando un correo electrónico a info@anescoprl.es.</p>";

		$destinatario=trim($empleado['email']);

	   $res=enviaEmail($destinatario,'Descarga de Reconocimiento - ANESCO',$mensaje);
	}

	return $res;
}

function insertaEpisReconocimientoMedico($codigoReconocimiento,$datos,$actualizacion=false){
	$res=true;
	
	if($actualizacion){
		$res=consultaBD("DELETE FROM epis_en_reconocimiento_medico WHERE codigoReconocimientoMedico=$codigoReconocimiento");
	}

	for($i=0;isset($datos['codigoEpi'.$i]);$i++){
		$res=$res && consultaBD("INSERT INTO epis_en_reconocimiento_medico VALUES(NULL,$codigoReconocimiento,".$datos['codigoEpi'.$i].");");
	}


	return $res;
}

function insertaPruebasReconocimientoMedico($codigoReconocimiento,$datos,$actualizacion=false){
	$res=true;
	
	if($actualizacion){
		$res=consultaBD("DELETE FROM reconocimientos_medicos_otras_pruebas WHERE codigoRM=$codigoReconocimiento");
	}

	for($i=0;isset($datos['otrasPruebas_'.$i]);$i++){
		$pruebas=$datos['otrasPruebas_'.$i];
		$valoracionEconomica=formateaNumeroWeb($datos['valoracionEconomica'.$i],true);
		$resultado=$datos['resultado'.$i];

		$res=$res && consultaBD("INSERT INTO reconocimientos_medicos_otras_pruebas VALUES(NULL,$codigoReconocimiento,'$pruebas','$valoracionEconomica','$resultado');");
	}


	return $res;
}


function insertaEnfermedadesSelectorEnfermedadesReconocimientoMedico($codigoReconocimiento,$datos,$actualizacion=false){
	$res=true;
	
	if($actualizacion){
		$res=consultaBD("DELETE FROM enfermedades_antecedentes_reconocimientos_medicos WHERE codigoReconocimiento='$codigoReconocimiento';");
	}

	for($i=0;isset($datos['codigoEnfermedad'.$i]);$i++){
		$codigoEnfermedad=$datos['codigoEnfermedad'.$i];
		$checkAntecedentesPersonales=compruebaCampoCheckEnfermedades($datos,'checkAntecedentesPersonales'.$i);
		$checkAntecedentesFamiliares=compruebaCampoCheckEnfermedades($datos,'checkAntecedentesFamiliares'.$i);
		$checkAdjuntarRecomendaciones=compruebaCampoCheckEnfermedades($datos,'checkAdjuntarRecomendaciones'.$i);

		$res=$res && consultaBD("INSERT INTO enfermedades_antecedentes_reconocimientos_medicos VALUES(NULL,'$codigoReconocimiento','$codigoEnfermedad','$checkAntecedentesPersonales','$checkAntecedentesFamiliares','$checkAdjuntarRecomendaciones');");
	}


	return $res;
}

function insertaComentariosSelectorComentariosReconocimientoMedico($codigoReconocimiento,$datos,$actualizacion=false){
	$res=true;
	
	if($actualizacion){
		$res=consultaBD("DELETE FROM comentarios_analiticas_reconocimientos_medicos WHERE codigoReconocimiento='$codigoReconocimiento';");
	}

	for($i=0;isset($datos['codigoComentario'.$i]);$i++){
		
		$codigoComentario=$datos['codigoComentario'.$i];
		$checkRecomendacionesComentario=compruebaCampoCheckEnfermedades($datos,'checkRecomendacionesComentario'.$i);
		$checkAdjuntoComentario=compruebaCampoCheckEnfermedades($datos,'checkAdjuntoComentario'.$i);

		$res=$res && consultaBD("INSERT INTO comentarios_analiticas_reconocimientos_medicos VALUES(NULL,'$codigoReconocimiento','$codigoComentario','$checkRecomendacionesComentario','$checkAdjuntoComentario');");
	}


	return $res;
}


function compruebaCampoCheckEnfermedades($datos,$indice){
	$res='NO';

	if(isset($datos[$indice])){
		$res=$datos[$indice];
	}

	return $res;
}


function insertaBebidasEstimulantesReconocimientoMedico($codigoReconocimiento,$datos,$actualizacion=false){
	$res=true;
	
	if($actualizacion){
		$res=consultaBD("DELETE FROM bebidas_estimulantes_reconocimiento_medico WHERE codigoReconocimientoMedico='$codigoReconocimiento';");
	}

	for($i=0;isset($datos['bebida'.$i]);$i++){
		
		$bebida=$datos['bebida'.$i];
		$unidadesDia=$datos['unidadesDia'.$i];

		$res=$res && consultaBD("INSERT INTO bebidas_estimulantes_reconocimiento_medico VALUES(NULL,'$unidadesDia','$bebida','$codigoReconocimiento');");
	}


	return $res;
}


function insertaEjerciciosReconocimientoMedico($codigoReconocimiento,$datos,$actualizacion=false){
	$res=true;
	
	if($actualizacion){
		$res=consultaBD("DELETE FROM ejercicios_reconocimiento_medico WHERE codigoReconocimiento='$codigoReconocimiento';");
	}

	for($i=0;isset($datos['ejercicio'.$i]);$i++){
		
		$ejercicio=$datos['ejercicio'.$i];
		$frecuencia=$datos['frecuencia'.$i];

		$res=$res && consultaBD("INSERT INTO ejercicios_reconocimiento_medico VALUES(NULL,'$ejercicio','$frecuencia','$codigoReconocimiento');");
	}


	return $res;
}



function cambiaEstadoEliminadoReconocimiento($estado){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],'reconocimientos_medicos',false);
	}
	cierraBD();

	return $res;
}


function gestionReconocimiento(){
	operacionesReconocimientos();
	
	abreVentanaGestionConBotonesReconocimiento('Reconocimiento de ','?','','icon-edit','formularioCompacto',true,'noAjax');
	$datos=compruebaDatosReconocimientosMedicos();
	$codigoContrato=$datos['codigoContrato'];

	if(!$codigoContrato && isset($_POST['codigoContrato'])){
		$codigoContrato=$_POST['codigoContrato'];
	} 
	elseif(isset($_GET['codigoContratoCita']) && $_GET['codigoContratoCita']!='NULL'){
		$codigoContrato=$_GET['codigoContratoCita'];
	}

	campoOculto($codigoContrato,'codigoContrato');
	campoOculto($datos,'eliminado','NO');
	campoOculto($_SESSION['codigoU'],'codigoUsuario');//En cada actualización, debe cambiar el codigoUsuario
	campoOculto($_SESSION['tipoUsuario'],'perfilUsuario');
	campoOculto('SI','editable');
	campoOculto($datos,'enviado','NO');
	campoOculto($datos,'cerrado','NO');
	campoOculto($datos,'bloqueado','NO');
	campoOculto($datos,'reconocimientoVisible','NO');
	campoOculto($datos,'aptoVisible','NO');
	campoOculto($datos['reconocimientoVisible'],'reconocimientoVisibleAnterior','NO');
	campoOculto($datos['aptoVisible'],'aptoVisibleAnterior','NO');

	//camposCabeceraReconocimiento($codigoContrato,$datos);

	creaPestaniasReconocimiento($datos);

		camposIdentificacion($codigoContrato,$datos);
		camposRiesgosProtocolizacionEpis($datos);
		camposHabitos($datos);
		camposAntecedentes($datos);
		camposAntecedentes2($datos);
		camposExploracion($datos);
		camposExploracionInterna($datos);
		camposVision($datos);
		camposEspirometria($datos);
		camposAudicion($datos);
		camposHemograma($datos);
		camposOtrasPruebas($datos);
		camposRadiologia($datos);
		camposJuicioClinico($datos);
		camposRecomendaciones($datos);

	cierraPestaniasAPI();

	cierraVentanaGestionReconocimiento($datos,'index.php',true);

	creaVentanaCreacionEmpleado($codigoContrato);
}

function abreVentanaGestionConBotonesReconocimiento($titulo,$destino,$claseCampos='',$icono='icon-edit',$margen='',$fichero=false,$claseFormulario='',$destinoVolver='index.php',$botonGuardar=true,$texto='Guardar',$iconoGuardar='icon-check',$botonVolver=true,$textoVolver='Volver',$iconoVolver='icon-chevron-left'){
    $modoTransmision='';
    if($fichero){
        $modoTransmision='enctype="multipart/form-data"';
    }

    echo "
    <div class='main' id='contenido'>
      <div class='main-inner'>
        <div class='container'>
          <div class='row'>

          <div class='span12 $margen'>
            <div class='widget'>
                <div class='widget-header'> <i class='icon-list'></i><i class='icon-chevron-right separadorSeccion'></i><i class='$icono'></i>
                  <h3>$titulo</h3>
                  <div class='pull-right'>";

                    if($botonVolver){
                        echo "<a href='$destinoVolver' class='btn btn-small btn-default btnSalirReconocimiento'><i class='$iconoVolver'></i> $textoVolver</a> &nbsp;";
                    }

                    if($botonGuardar){                      
                        echo "<button type='button' onclick=\"$(':submit').click();\" class='btn btn-small btn-propio'><i class='$iconoGuardar'></i> $texto</button>";
                    }

    echo "         </div>
                </div>
                <!-- /widget-header -->
                <div class='widget-content'>
                  
                  <div class='tab-pane' id='formcontrols'>
                    <form id='edit-profile' class='form-horizontal $claseFormulario' action='$destino' method='post' $modoTransmision>
                      <fieldset class='$claseCampos'>";
}

function cierraVentanaGestionReconocimiento($datos,$destino,$columnas=false,$mostrarBotonGuardar=true,$texto='Guardar',$icono='icon-check',$botonVolver=true,$textoVolver='Volver',$iconoVolver='icon-chevron-left',$salto=true){
	
	$botonGuardar=" <button type='submit' class='btn btn-propio btn-small'><i class='$icono'></i> $texto</button>";

	if($datos && $datos['editable']=='NO' && $datos['codigoUsuario']!=$_SESSION['codigoU']){
		
		$usuario=consultaBD("SELECT CONCAT(nombre,' ',apellidos) AS nombre FROM usuarios WHERE codigo='".$datos['codigoUsuario']."';",true,true);

		$botonGuardar='	<div class="alert alert-warning pull-right anchoAuto">
				  			<i class="icon-exclamation-triangle"></i> 
							No se puede guardar el reconocimiento porque<br />el usuario '.$usuario['nombre'].' está escribiendo en él.
						</div>';
	}



	if($columnas){
		echo "		  </fieldset>
			  		  <fieldset class='sinFlotar'>";
	}
	if($salto){
		echo "			<br />";
	}


	echo "
						
	                    <div class='form-actions'>";
	if($botonVolver){
	    echo "            <a href='$destino' class='btn btn-default btn-small btnSalirReconocimiento'><i class='$iconoVolver'></i> $textoVolver</a>";
	}

	if($mostrarBotonGuardar){                      
	    echo 			 $botonGuardar;
	}

	if($datos){
		echo "
						  <a href='generaReconocimiento.php?encp=".md5($datos['codigo'])."' class='btn btn-success btn-small noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar reconocimiento</a>
	    				  <a href='generaApto.php?encp=".md5($datos['codigo'])."' class='btn btn-success btn-small noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar apto</a>";
	}

	if(isset($datos['codigoOferta'])){
		echo "
						  <a href='../ofertas/gestion.php?codigo=".$datos['codigoOferta']."' class='btn btn-default btn-small noAjax' target='_blank'><i class='icon-external-link'></i> Ver oferta</a>
						  <a href='../contratos/gestion.php?codigo=".$datos['codigoContrato']."' class='btn btn-default btn-small noAjax' target='_blank'><i class='icon-external-link'></i> Ver contrato</a>";
	}

	if($datos['aptoVisible']=='SI'){
		echo " <input type='submit' name='enviarApto' class='btn btn-inverse btn-small' value='Reenviar apto' />";
	}

	if($datos['reconocimientoVisible']=='SI'){
		echo " <input type='submit' name='enviarReco' class='btn btn-inverse btn-small' value='Reenviar reco' />";
	}

	echo "
	                    </div> <!-- /form-actions -->
	                  </fieldset>
	                </form>
	                </div>


	            </div>
	            <!-- /widget-content --> 
	          </div>

	      </div>
	    </div>
	    <!-- /container --> 
	  </div>
	  <!-- /main-inner --> 
	</div>
	<!-- /main -->";
}


function compruebaDatosReconocimientosMedicos(){
	
	if(isset($_POST['codigoReconocimientoAnterior'])){
		$datos=arrayFormulario();
		$res=consultaBD("SELECT * FROM reconocimientos_medicos WHERE codigo=".$datos['codigoReconocimientoAnterior'],true,true);
		$res['codigoContrato']=$datos['codigoContrato'];
		$res['botonDatosAnterioresPulsado']='SI';
		$res['cerrado']='NO';
		$res['bloqueado']='NO';
		$res['reconocimientoVisible']='NO';
		$res['aptoVisible']='NO';
		$res=actualizaDatosRMAnterior($res);
		campoOculto('SI','rmAnterior');
	}
	else{
		$res=compruebaDatos('reconocimientos_medicos');
		
		conexionBD();

		if($res){
			//Obtengo el código de la oferta para poner un enlace a la misma desde el pie del formulario de reconocimientos
			$oferta=consultaBD("SELECT ofertas.codigo FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='".$res['codigoContrato']."';",false,true);
			$res['codigoOferta']=$oferta['codigo'];


			/*
				Explicación de concurrencia:
				1- En la obtención de datos se comprueba que el reconocimiento sea editable, y en tal caso se marca como no editable indicando el usuario actual.
				2- En la función gestionReconocimiento, el campoOculto 'editable' está a SI para el registro de nuevos reconocimientos.
				3- En la función cierraVentanaGestionReconocimiento se comprueba el estado del reco, y si no es editable para el usuario actual no muestre el submit.
				4- En el JS de gestion.php, se oculta el botón de guardar del widget-header.
				5- Cada vez que se accede al index.php de la sección, donde está el listado, se marcan como editables todos los recos bloqueados por el usuario actual, pues se entiende que ya ha salido de ellos.
			*/
			if($res['editable']=='SI' && isset($_SESSION['codigoU']) && $_SESSION['codigoU']!=''){
				consultaBD("UPDATE reconocimientos_medicos SET editable='NO', codigoUsuario=".$_SESSION['codigoU']." WHERE codigo=".$res['codigo'].";");//Para evitar que otro usuario entre a editar el reconocimiento.
				$res['editable']='NO';
				$res['codigoUsuario']=$_SESSION['codigoU'];
			}
		}

		cierraBD();
	}

	return $res;
}

function actualizaDatosRMAnterior($res){
	$res['numero']='';
	if($res['tipoReconocimiento']=='Inicial'){
		$res['tipoReconocimiento']='Periódico';
	}
	$fecha=fechaBD();
	$res['fecha']=$fecha;
	$fecha=new DateTime($fecha);
	if($res['periodicidad']=='Semestral'){
		$intervalo = new DateInterval('P6M');
	} 
	elseif($res['periodicidad']=='Trimestral'){
		$intervalo = new DateInterval('P3M');
	} 
	elseif($res['periodicidad']=='Mensual'){
		$intervalo = new DateInterval('P1M');
	}
	else{
		$intervalo = new DateInterval('P1Y');
	}

	$fecha->add($intervalo);
	$fecha=$fecha->format('Y-m-d');
	$fecha=explode('-', $fecha);

	$res['mesSiguienteReconocimiento']=$fecha[1];
	$res['anioSiguienteReconocimiento']=$fecha[0];

	$empleado=consultaBD('SELECT fechaNacimiento FROM empleados WHERE codigo='.$res['codigoEmpleado'],true,true);

	$res['edadReconocimiento']=calculaEdadEmpleado($empleado['fechaNacimiento']);
	
	return $res;
}

function imprimeReconocimientos($eliminados='NO'){
	$codigoUsuario=$_SESSION['codigoU'];

	$consulta=consultaBD("SELECT reconocimientos_medicos.codigo, reconocimientos_medicos.fecha, clientes.EMPNOMBRE, CONCAT(empleados.nombre,' ',empleados.apellidos) AS empleado,
					      empleados.dni, apto, checkAudiometria, checkOtrasPruebasFuncionales, checkEspirometria, checkElectrocardiograma, advertencia,
					  	  cerrado, reconocimientos_medicos.codigoUsuario, reconocimientos_medicos.editable, reconocimientos_medicos.eliminado, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS usuario
					      FROM reconocimientos_medicos LEFT JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
					      LEFT JOIN clientes ON empleados.codigoCliente=clientes.codigo 
					      LEFT JOIN usuarios ON reconocimientos_medicos.codigoUsuario=usuarios.codigo
						  WHERE reconocimientos_medicos.eliminado='$eliminados';");

	while($datos=mysql_fetch_assoc($consulta)){
		$apto=compruebaAptoReconocimientoMedico($datos['apto']);

		echo "
			<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['EMPNOMBRE']."</td>
				<td>".$datos['empleado']."</td>
				<td>".$datos['dni']."</td>
				<td>".$apto."</td>
				<td>".$datos['checkAudiometria']."</td>
				<td>".$datos['checkOtrasPruebasFuncionales']."</td>
				<td>".$datos['checkEspirometria']."</td>
				<td>".$datos['checkOrina']."</td>
				<td>".$datos['checkBioquimica']."</td>
				<td>".$datos['checkHemograma']."</td>
				<td>".$datos['checkElectrocardiograma']."</td>
				<td>".$datos['cerrado']."</td>
				".botonAccionesReconocimiento($datos,$codigoUsuario,$datos['codigoUsuario'],$datos['usuario'])."
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	
}


function filtroReconocimientos(){
	$datos=obtieneCamposFiltro();

	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(1,'Nº Cliente',$datos,'input-mini pagination-right');
	campoTexto(2,'Nombre empresa',$datos,'span3');
	campoFecha(0,'Fecha desde',$datos);
	campoFecha(10,'Hasta',$datos);
	campoTexto(3,'Trabajador',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(8,'Cerrado',$datos);
	campoSelectSiNoFiltro(11,'Advertencia',$datos);
	campoSelectTieneFiltro(14,'De profesional externo',$datos);
	campoTexto(15,'Nº laboratorio',$datos,'input-small pagination-right');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function obtieneCamposFiltro(){
	$res=array();

	for($i=0;$i<16;$i++){//16 porque el último campo del filtro es 15
		if(isset($_SESSION['filtro'.$i])){
			$res[$i]=$_SESSION['filtro'.$i];
		}
		else{
			$res[$i]=false;
		}
	}

	return $res;
}

function guardaValoresFiltro(){
	
	for($i=0;$i<16;$i++){//16 porque el último campo del filtro es 15
		if(isset($_GET['sSearch_'.$i]) && $_GET['sSearch_'.$i]!=''){
			if(substr_count($_GET['sSearch_'.$i],'/')==2){
				$_SESSION['filtro'.$i]=formateaFechaBD($_GET['sSearch_'.$i]);
			}
			else{
				$_SESSION['filtro'.$i]=$_GET['sSearch_'.$i];
			}
		}
		elseif(isset($_SESSION['filtro'.$i])){
			unset($_SESSION['filtro'.$i]);
		}
	}

}

function listadoReconocimientos($eliminados){
	global $_CONFIG;
	$codigoUsuario=$_SESSION['codigoU'];

	$columnas=array(
		'fecha',
		'EMPID',
		'empresa',
		'empleado',
		'dni',
		'apto',
		'aptoVisible',
		'reconocimientoVisible',
		'bloqueado',
		'facturaEliminada',
		'fecha',
		'advertencia',
		'mesSiguienteReconocimiento',
		'anioSiguienteReconocimiento',
		'codigoProfesionalExterno',
		'numero'
	);

	guardaValoresFiltro();

	$havingCerrado='';
	if($_SESSION['tipoUsuario']=='ENFERMERIA'){
		//$havingCerrado="AND cerrado='SI'";
	}

	$having=obtieneWhereListado("HAVING eliminado='$eliminados' $havingCerrado",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT reconocimientos_medicos.codigo, reconocimientos_medicos.fecha, CONCAT(clientes.EMPNOMBRE,' (',clientes.EMPMARCA,')') AS empresa, CONCAT(empleados.apellidos,', ',empleados.nombre) AS empleado,
		    empleados.dni, apto, checkAudiometria, checkOtrasPruebasFuncionales, checkEspirometria, checkElectrocardiograma, advertencia,
		  	cerrado, reconocimientos_medicos.codigoUsuario, reconocimientos_medicos.editable, reconocimientos_medicos.eliminado, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS usuario,
		  	mesSiguienteReconocimiento, anioSiguienteReconocimiento, clientes.EMPID, codigoProfesionalExterno, reconocimientos_medicos.numero, 
			IFNULL(facturas_reconocimientos_medicos.eliminado,'SI') AS facturaEliminada, aptoVisible, reconocimientoVisible, bloqueado

		    FROM reconocimientos_medicos LEFT JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
		    LEFT JOIN clientes ON empleados.codigoCliente=clientes.codigo 
		    LEFT JOIN usuarios ON reconocimientos_medicos.codigoUsuario=usuarios.codigo
		    LEFT JOIN reconocimientos_medicos_en_facturas ON reconocimientos_medicos.codigo=reconocimientos_medicos_en_facturas.codigoReconocimientoMedico
		    LEFT JOIN facturas_reconocimientos_medicos ON reconocimientos_medicos_en_facturas.codigoFacturaReconocimientoMedico=facturas_reconocimientos_medicos.codigo AND reconocimientos_medicos.codigoContrato=facturas_reconocimientos_medicos.codigoContrato

		    GROUP BY reconocimientos_medicos.codigo
		    $having";
	
	conexionBD();
	
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);

	$iconoBloqueado=array('SI'=>'<i class="icon-check-circle iconoFactura icon-success" title="Bloqueado"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger" title="Sin bloquear"></i>',''=>'<i class="icon-times-circle iconoFactura icon-danger" title="Sin bloquear"></i>');

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$apto=compruebaAptoReconocimientoMedico($datos['apto']);

		$advertencia='';
		if($datos['advertencia']=='SI'){
			$advertencia='<i class="icon-exclamation-circle icon-danger" title="Pendiente"></i>';
		}

		$facturado=compruebaFacturaReconocimientoMedicoListado($datos['codigo'],$datos['facturaEliminada']);

		$fila=array(
			formateaFechaWeb($datos['fecha']),
			$datos['EMPID'],
			$datos['empresa'],
			$datos['empleado'].' '.$advertencia,
			$datos['dni'],
			$apto,
			"<div class='centro'>".$datos['aptoVisible']."</div>",
			"<div class='centro'>".$datos['reconocimientoVisible']."</div>",
			"<div class='centro'>".$iconoBloqueado[$datos['bloqueado']]."</div>",
			"<div class='centro'>".$facturado."</div>",
			botonAccionesReconocimiento($datos,$codigoUsuario,$datos['codigoUsuario'],$datos['usuario']),
			"<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' />",
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	cierraBD();

	echo json_encode($res);
}

function compruebaFacturaReconocimientoMedicoListado($codigoReconocimiento,$facturaEliminada){
	$res='NO';

	if($facturaEliminada=='NO'){
		$res='SI';
	}
	else{
		$comprobacion=consultaBD("SELECT reconocimientos_medicos_en_facturas.codigo 
								  FROM reconocimientos_medicos_en_facturas INNER JOIN facturas_reconocimientos_medicos ON reconocimientos_medicos_en_facturas.codigoFacturaReconocimientoMedico=facturas_reconocimientos_medicos.codigo
								  WHERE codigoReconocimientoMedico='$codigoReconocimiento' AND eliminado='NO';");

		if(mysql_num_rows($comprobacion)>0){
			$res='SI';
		}
	}

	return $res;
}

function botonAccionesReconocimiento($datos,$codigoUsuario,$codigoUsuarioRM,$nombreUsuarioRM){
	$nombres=array();
	$direcciones=array();
	$iconos=array();
	$aperturaEnlaces=array();

	if($datos['editable']=='SI'){//Si el reconocimiento está editable se ven los detalles*
		array_push($nombres,'Detalles');
		array_push($direcciones,"reconocimientos-medicos/gestion.php?codigo=".$datos['codigo']);
		array_push($iconos,'icon-search-plus');
		array_push($aperturaEnlaces,0);
	}
	else{
		array_push($nombres,'En edición por '.$datos['usuario']);
		array_push($direcciones,"#' onclick='return false;");
		array_push($iconos,'icon-ban');
		array_push($aperturaEnlaces,2);
	}

	//if($datos['cerrado']=='SI' && $codigoUsuario==$codigoUsuarioRM){
		/*$res=botonAcciones(array('Detalles','Descargar Reconocimiento','Descargar Apto'),array("reconocimientos-medicos/gestion.php?codigo=".$datos['codigo'],"reconocimientos-medicos/generaReconocimiento.php?encp=".md5($datos['codigo']),"reconocimientos-medicos/generaApto.php?encp=".md5($datos['codigo'])),array('icon-search-plus','icon-cloud-download','icon-cloud-download'),array(0,1,1),true,'');*/

		array_push($nombres,'Descargar Reconocimiento');
		array_push($direcciones,"reconocimientos-medicos/generaReconocimiento.php?encp=".md5($datos['codigo']));
		array_push($iconos,'icon-cloud-download');
		array_push($aperturaEnlaces,1);

		array_push($nombres,'Descargar Apto');
		array_push($direcciones,"reconocimientos-medicos/generaApto.php?encp=".md5($datos['codigo']));
		array_push($iconos,'icon-cloud-download');
		array_push($aperturaEnlaces,1);
	//}

	if(empty($nombres)){
		$res="<td class='centro'><div class='centro'><div class='btn-group'><button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown' disabled='disabled' title='Bloqueado. Solo disponible para ".$nombreUsuarioRM."'><i class='icon-cogs'></i> <span class='caret'></span></button></div></div></td>";
	}
	else{
		$res=botonAcciones($nombres,$direcciones,$iconos,$aperturaEnlaces,true,'');
	}

	return $res;
}

function seleccionContrato(){
	abreVentanaGestionConBotones('Gestión de reconocimientos médicos - selección de contrato','gestion.php','','icon-edit','',true,'noAjax','index.php',true,'Continuar','icon-chevron-right');

	campoSelectContratoReconocimiento();
	campoDato('RR.MM. incluidos en contrato','-','incluidos');
	campoDato('RR.MM. restantes','-','restantes');

	cierraVentanaGestion('index.php',false,true,'Continuar','icon-chevron-right');
}


function campoSelectContratoReconocimiento(){
	$nombres=array('');
	$valores=array('NULL');

	$contratos=obtieneContratos();
	$nombres=$contratos['nombres'];
	$valores=$contratos['valores'];

	campoSelectHTML('codigoContrato','Contrato',$nombres,$valores,false,'selectpicker span8 show-tick',"data-live-search='true'");
}


function obtieneContratos(){
	$res=array('nombres'=>array(''),'valores'=>array('NULL'));
	$tipoUsuario=$_SESSION['tipoUsuario'];

	$consulta=consultaBD("SELECT contratos.codigo, contratos.codigoInterno, contratos.fechaInicio, clientes.EMPCP, clientes.EMPID, clientes.EMPNOMBRE, clientes.EMPMARCA,
						  facturaCobrada
						  FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
						  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
						  LEFT JOIN contratos_en_facturas ON contratos.codigo=contratos_en_facturas.codigoContrato
						  LEFT JOIN facturas ON contratos_en_facturas.codigoFactura=facturas.codigo
						  WHERE contratos.eliminado='NO' AND (ofertas.opcion=1 OR ofertas.opcion=2) AND enVigor='SI'
						  GROUP BY contratos.codigo;",true);

	while($datos=mysql_fetch_assoc($consulta)){

		if(($tipoUsuario!='ADMIN' && $datos['facturaCobrada']=='SI') || $tipoUsuario=='ADMIN'){
			
			$iconoCobrada='';
			if($datos['facturaCobrada']=='NO' || $datos['facturaCobrada']==NULL){
				$iconoCobrada=" <span class='label label-danger'><i class='icon-flag'></i> Pendiente de cobro</span>";
			}

			$numeroContrato=formateaReferenciaContrato($datos,$datos);
			$nombre='Nº: '.$numeroContrato.'. &nbsp; Cliente: '.$datos['EMPNOMBRE'].' ('.$datos['EMPMARCA'].')'.$iconoCobrada;

			array_push($res['nombres'],$nombre);
			array_push($res['valores'],$datos['codigo']);

		}

	}

	return $res;
}


function consultaReconocimientosContrato(){
	$res=array('incluidos'=>0,'restantes'=>0);

	$datos=arrayFormulario();
	extract($datos);

	conexionBD();

	$oferta=consultaBD("SELECT numRM FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='$codigoContrato';",false,true);
	$reconocimientos=consultaBD("SELECT COUNT(codigo) AS realizados FROM reconocimientos_medicos WHERE codigoContrato='$codigoContrato';",false,true);

	cierraBD();

	$res['incluidos']=$oferta['numRM'];
	$res['restantes']=$oferta['numRM']-$reconocimientos['realizados'];

	if($res['restantes']<0){
		$res['restantes']=0;
	}

	echo json_encode($res);
}


function creaPestaniasReconocimiento($datos){

	$clasePestaniaIdentificacion=obtieneClaseColorPestania($datos,'pestaniaIdentificacion');
	$clasePestaniaRiesgos=obtieneClaseColorPestania($datos,'pestaniaRiesgos');
	$clasePestaniaHabitos=obtieneClaseColorPestania($datos,'pestaniaHabitos');
	$clasePestaniaAntecedentes=obtieneClaseColorPestania($datos,'pestaniaAntecedentes');
	$clasePestaniaEPIs=obtieneClaseColorPestania($datos,'pestaniaEPIs');
	$clasePestaniaExploracion1=obtieneClaseColorPestania($datos,'pestaniaExploracion1');
	$clasePestaniaExploracion2=obtieneClaseColorPestania($datos,'pestaniaExploracion2');
	$clasePestaniaVision=obtieneClaseColorPestania($datos,'pestaniaVision');
	$clasePestaniaEspiro=obtieneClaseColorPestania($datos,'pestaniaEspiro');
	$clasePestaniaAudio=obtieneClaseColorPestania($datos,'pestaniaAudio');
	$clasePestaniaHemo=obtieneClaseColorPestania($datos,'pestaniaHemo');
	$clasePestaniaOtras=obtieneClaseColorPestania($datos,'pestaniaOtras');
	$clasePestaniaRadio=obtieneClaseColorPestania($datos,'pestaniaRadio');
	$clasePestaniaJuicio=obtieneClaseColorPestania($datos,'pestaniaJuicio');
	$clasePestaniaRecomendaciones=obtieneClaseColorPestania($datos,'pestaniaRecomendaciones');

	$botones=obtieneBotonesGestionCierreReconocimiento($datos);

	echo "<div class='tabbable tabs-reconocimientos'>
	       	<ul class='nav nav-tabs'>
		        
		        <li class='active ".$clasePestaniaIdentificacion."'>
		        	<a href='#1' class='noAjax' data-toggle='tab'><i class='icon-user'></i> Identificación</a>
		        	".generaMenuGestionColorPestania('pestaniaIdentificacion')."
		        </li>


		        <li class='".$clasePestaniaRiesgos."'>
		        	<a href='#11' class='noAjax' data-toggle='tab'><i class='icon-exclamation-circle'></i> Protocolos/EPIs</a>
		        	".generaMenuGestionColorPestania('pestaniaRiesgos')."
		        </li>

		        <li class='".$clasePestaniaHabitos."'>
		        	<a href='#2' class='noAjax' data-toggle='tab'><i class='icon-glass'></i> Datos-Hábitos</a>
		        	".generaMenuGestionColorPestania('pestaniaHabitos')."
		        </li>

		        <li class='".$clasePestaniaAntecedentes."'>
		        	<a href='#3' class='noAjax' data-toggle='tab'><i class='icon-users'></i> Antecedentes</a>
		        	".generaMenuGestionColorPestania('pestaniaAntecedentes')."
		        </li>

		        <li class='".$clasePestaniaEPIs."'>
		        	<a href='#13' class='noAjax' data-toggle='tab'><i class='icon-users'></i> Antecendes 2</a>
		        	".generaMenuGestionColorPestania('pestaniaEPIs')."
		        </li>

		        <li class='".$clasePestaniaExploracion1."'>
		        	<a href='#4' class='noAjax' data-toggle='tab'><i class='icon-male'></i> Exploración 1</a>
		        	".generaMenuGestionColorPestania('pestaniaExploracion1')."
		        </li>

		        <li class='".$clasePestaniaExploracion2."'>
		        	<a href='#14' class='noAjax' data-toggle='tab'><i class='icon-stethoscope'></i> Exploración 2</a>
		        	".generaMenuGestionColorPestania('pestaniaExploracion2')."
		        </li>

		        <li class='".$clasePestaniaVision."'>
		        	<a href='#5' class='noAjax' data-toggle='tab'><i class='icon-eye'></i> Visión</a>
		        	".generaMenuGestionColorPestania('pestaniaVision')."
		        </li>

		        <li class='".$clasePestaniaEspiro."'>
		        	<a href='#15' class='noAjax' data-toggle='tab'><i class='icon-circle-o'></i> Espirometría</a>
		        	".generaMenuGestionColorPestania('pestaniaEspiro')."
		        </li>

		        <li class='".$clasePestaniaAudio."'>
		        	<a href='#6' class='noAjax' data-toggle='tab'><i class='icon-deaf'></i> Audio/EKG</a>
		        	".generaMenuGestionColorPestania('pestaniaAudio')."
		        </li>

		        <li class='".$clasePestaniaHemo."'>
		        	<a href='#7' class='noAjax' data-toggle='tab'><i class='icon-tint'></i> Hemo/Bio/Orina</a>
		        	".generaMenuGestionColorPestania('pestaniaHemo')."
		        </li>

		        <li class='".$clasePestaniaOtras."'>
		        	<a href='#12' class='noAjax' data-toggle='tab'><i class='icon-flask'></i> Otras pruebas</a>
		        	".generaMenuGestionColorPestania('pestaniaOtras')."
		        </li>

		        <li class='".$clasePestaniaRadio."'>
		        	<a href='#8' class='noAjax' data-toggle='tab'><i class='icon-dashboard'></i> Radiología</a>
		        	".generaMenuGestionColorPestania('pestaniaRadio')."
		       	</li>

		        <li class='".$clasePestaniaJuicio."'>
		        	<a href='#9' class='noAjax' data-toggle='tab'><i class='icon-edit'></i> Juicio clínico</a>
		        	".generaMenuGestionColorPestania('pestaniaJuicio')."
		        </li>

		        <li class='".$clasePestaniaRecomendaciones."'>
		        	<a href='#10' class='noAjax' data-toggle='tab'><i class='icon-list-ol'></i> Recomendaciones</a>
		        	".generaMenuGestionColorPestania('pestaniaRecomendaciones')."
		        </li>

		        <li class='contenedorBoton'>
		        	".$botones['botonCierre']."
		        </li>

		        <li class='contenedorBoton'>
		        	".$botones['botonBloqueo']."
		        </li>

		        <li class='contenedorBoton'>
		        	".$botones['botonRecoVisible']."
		        </li>

		        <li class='contenedorBoton'>
		        	".$botones['botonAptoVisible']."
		        </li>
	       	</ul>

       <div class='tab-content'>";
}

function obtieneClaseColorPestania($datos,$indice){
	$res='';

	if(isset($datos[$indice])){
		$res=$datos[$indice];//correcto|advertencia|anormal
		campoOculto($datos[$indice],$indice);
	}
	else{
		campoOculto('',$indice);
	}

	return $res;
}

function obtieneBotonesGestionCierreReconocimiento($datos){
	$res=false;

	if($_SESSION['tipoUsuario']!='ENFERMERIA'){

		$res=array(
			'botonCierre'		=>	"<button type='button' class='btn-small btn btn-danger' campo='cerrado' valor='SI'><i class='icon-unlock'></i> Parte médica</button>",
			'botonBloqueo'		=>	"<button type='button' class='btn-small btn btn-danger' campo='bloqueado' valor='SI'><i class='icon-remove'></i> Sin cerrar</button>",
			'botonRecoVisible'	=>	"<button type='button' class='btn-small btn btn-danger' campo='reconocimientoVisible' valor='SI'><i class='icon-eye-slash'></i> Reconocimiento oculto</button>",
			'botonAptoVisible'	=>	"<button type='button' class='btn-small btn btn-danger' campo='aptoVisible' valor='SI'><i class='icon-eye-slash'></i> Apto oculto</button>"
		);


		if($datos['cerrado']=='SI'){
			$res['botonCierre']="<button type='button' class='btn-small btn btn-success' campo='cerrado' valor='NO'><i class='icon-lock'></i> Fin parte médica</button>";
		}

		if($datos['bloqueado']=='SI'){
			$res['botonBloqueo']="<button type='button' class='btn-small btn btn-success' campo='bloqueado' valor='NO'><i class='icon-check'></i> Cerrado</button>";
		}

		if($datos['reconocimientoVisible']=='SI'){
			$res['botonRecoVisible']="<button type='button' class='btn-small btn btn-success' campo='reconocimientoVisible' valor='NO'><i class='icon-eye'></i> Reconocimiento visible</button>";
		}

		if($datos['aptoVisible']=='SI'){
			$res['botonAptoVisible']="<button type='button' class='btn-small btn btn-success' campo='aptoVisible' valor='NO'><i class='icon-eye'></i> Apto visible</button>";
		}
	}


	return $res;
}

function generaMenuGestionColorPestania($nombreCampoAsociado){
	$res="
	<ul class='nav pull-right menu-colores'>
	    <li class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'><b class='caret'></b></a>
	      	<ul class='dropdown-menu menu'>
	        	<li><a href='#' estado='' class='noAjax' campo='$nombreCampoAsociado'><i class='icon-circle-o'></i> Sin rellenar</a></li>
	        	<li class='divider'></li>
	        	<li><a href='#' estado='correcto' class='noAjax' campo='$nombreCampoAsociado'><i class='icon-check-circle correcto'></i> Correcto</a></li>
	        	<li class='divider'></li>
	        	<li><a href='#' estado='advertencia' class='noAjax' campo='$nombreCampoAsociado'><i class='icon-exclamation-circle advertencia'></i> Advertencia</a></li>
	        	<li class='divider'></li>
	        	<li><a href='#' estado='anormal' class='noAjax' campo='$nombreCampoAsociado'><i class='icon-times-circle anormal'></i> Anormal</a></li>
	    	</ul>
		</li>
	</ul>";

	return $res;
}


function camposCabeceraReconocimiento($codigoContrato,$datos){
	conexionBD();

	$contrato=consultaBD("SELECT contratos.codigo, contratos.codigoInterno, contratos.fechaInicio, clientes.EMPCP, clientes.EMPID, clientes.EMPNOMBRE, clientes.EMPMARCA, clientes.codigo AS codigoCliente
			   			   FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
			   			   INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
			   			   WHERE contratos.codigo='$codigoContrato';",false,true);

	abreColumnaCampos('span6');
	campoDato('Cliente',"<a href='../clientes/gestion.php?codigo=".$contrato['codigoCliente']."' class='noAjax' target='_blank'>".$contrato['EMPNOMBRE'].' ('.$contrato['EMPMARCA'].')</a>','empresa');


	if($datos){
		$empleado=consultaBD("SELECT nombre, apellidos, dni FROM empleados WHERE codigo=".$datos['codigoEmpleado'],false,true);
		campoDato('Trabajador',"<a href='../empleados/gestion.php?codigo=".$datos['codigoEmpleado']."' class='noAjax' target='_blank'>".$empleado['nombre'].' '.$empleado['apellidos'].' - '.$empleado['dni']."</a>",'empleado');
		campoOculto($datos['codigoEmpleado'],'codigoEmpleado');
	}
	else{
		$codigoEmpleado=isset($_GET['codigoEmpleadoCita'])?$_GET['codigoEmpleadoCita']:false;
		campoSelectConsultaPlusDoble('codigoEmpleado','Empleado',"SELECT codigo, CONCAT(nombre,' ',apellidos,' - ',dni) AS texto FROM empleados WHERE aprobado='SI' AND eliminado='NO' AND codigoCliente=".$contrato['codigoCliente'],$codigoEmpleado,'selectpicker span3 selectPlus show-tick',"data-live-search='true'",'',0,false,'btn-success',"<i class='icon-plus'></i>");
	}

	campoOculto($contrato['codigoCliente'],'codigoCliente');//Para su uso en la creación de empleados

	campoRadio('omitirFacturacion','Omitir de facturación',$datos);

	cierraColumnaCampos();
	cierraBD();
	
	abreColumnaCampos();

	campoDato('Contrato',formateaReferenciaContrato($contrato,$contrato));

	if(!$datos || $datos['botonDatosAnterioresPulsado']=='NO'){
		campoDato('Reconocimiento anterior',"<button type='button' class='btn btn-small btn-propio' id='obtenerDatosReconocimientoAnterior'><i class='icon-folder-open-o'></i> Obtener datos</button>");
	}

	if(!$datos || $datos['botonExploracioNormalPulsado']=='NO'){
		campoDato('Datos por defecto',"<button type='button' class='btn btn-small btn-success' id='obtenerDatosPorDefecto'><i class='icon-smile-o'></i> Exploración normal</button>");
	}

	campoOculto($datos,'botonDatosAnterioresPulsado','NO');
	campoOculto($datos,'botonExploracioNormalPulsado','NO');

	cierraColumnaCampos(true);
	echo '<hr>';

}

function camposIdentificacion($codigoContrato,$datos){
	abrePestaniaAPI(1,true);

	$fecha=$datos;
	if(isset($_GET['fechaInicio'])){
		$fecha=$_GET['fechaInicio'];
	}

	camposCabeceraReconocimiento($codigoContrato,$datos);

	abreColumnaCampos();
	campoRadio('tipoReconocimiento','Tipo de reconocimiento',$datos,'Inicial',array('Inicial','Periódico','Tras baja de larga duración','Se niega','Otros'),array('Inicial','Periódico','Tras baja de larga duración','Se niega','Otros'),true);
	echo '<div id="divTipoReconocimiento" class="hide">';
		campoTexto('tipoReconocimientoTexto','Tipo',$datos);
	echo '</div>';
	campoFecha('fecha','Fecha',$fecha);
	campoSelect('periodicidad','Peridiocidad',array('','Anual','Semestral','Trimestral','Mensual','Otra'),array('','Anual','Semestral','Trimestral','Mensual','Otros'),$datos,'selectpicker span2 show-tick',"");
	echo '<div id="divPeriodicidad" class="hide">';
		campoTexto('periodicidadTexto','Peridiocidad',$datos);
	echo '</div>';
	campoSiguienteReconocimiento($datos);
	creaTablaPuestosAnteriores($datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	areaTexto('riesgosExtralaborales','Riesgos extralaborales',$datos);
	campoTexto('numero','Nº laboratorio',$datos,'input-mini pagination-right obligatorio');
	campoPuestoTrabajoReconocimientoMedico($codigoContrato,$datos);
	areaTexto('riesgoPuesto','Riesgos del puesto',$datos);

	cierraColumnaCampos();

	cierraPestaniaAPI();
}

function creaTablaPuestosAnteriores($datos){
    echo "
       <div class='control-group'>                     
           <label class='control-label'>Trabajos anteriores:</label>
           <div class='controls'>
               <table class='tabla-simple'>
                   <thead>
                       <tr>
                           <th> Puesto </th>
                           <th> Meses </th>
                       </tr>
                   </thead>
                   <tbody>
                   	<tr>";
                   		campoTextoTabla('puestoAnterior',$datos);
							campoTextoTabla('tiempoPuestoAnterior',$datos,'input-mini pagination-right');
    echo "			</tr>
         			<tr>";
         					campoTextoTabla('puestoAnterior2',$datos);
							campoTextoTabla('tiempoPuestoAnterior2',$datos,'input-mini pagination-right');
    echo "			</tr>
         			<tr>";
         					campoTextoTabla('puestoAnterior3',$datos);
							campoTextoTabla('tiempoPuestoAnterior3',$datos,'input-mini pagination-right');
         
    echo "      	</tr>
       			</tbody>
               </table>
           </div>
       </div>
       <br />";
}

function campoSiguienteReconocimiento($datos,$texto='Fecha siguiente reconocimiento',$nombreCampo1='mesSiguienteReconocimiento',$nombreCampo2='anioSiguienteReconocimiento',$clase='selectpicker span2 show-tick',$busqueda="data-live-search='true'"){
	$valor1=compruebaValorCampo($datos,$nombreCampo1);
	$valor2=compruebaValorCampo($datos,$nombreCampo2);
	$nombres=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	$valores=array('00','01','02','03','04','05','06','07','08','09','10','11','12');
	echo "
		<div class='control-group fechaSiguienteReconocimiento'>                     
			<label class='control-label' for='$nombreCampo1'>$texto:</label>
			<div class='controls'>";
	echo "<select name='$nombreCampo1' id='$nombreCampo1' class='$clase' $busqueda>";

	for($i=0;$i<count($nombres);$i++){
		echo "<option value='".$valores[$i]."'";

		if($valor1==$valores[$i]){
			echo " selected='selected'";
		}

		echo ">".$nombres[$i]."</option>";
	}
		
	echo "</select>";

	echo "<input type='number' name='$nombreCampo2' id='$nombreCampo2' class='input-mini pagination-right' value='$valor2' />";

	echo "
			</div>       
		</div>";
}


function campoPuestoTrabajoReconocimientoMedico($codigoContrato,$datos){
	conexionBD();

	$codigoCliente=consultaBD('SELECT codigoCliente FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo WHERE contratos.codigo='.$codigoContrato,false,true);
	$codigoCliente=$codigoCliente['codigoCliente'];

	$query="SELECT codigo, nombrePuesto AS texto FROM protocolizacion WHERE codigoCliente=".$codigoCliente." ORDER BY nombrePuesto";

	$consulta=consultaBD($query);
	/*if(mysql_num_rows($consulta)==0){//Hago esta comprobación para los casos de clientes que no tienen Evaluación de Riesgos (de puestos) hecha
		$query="SELECT codigo, nombre AS texto FROM puestos_trabajo ORDER BY nombre";
	}*/


	cierraBD();


	campoSelectConsulta('codigoPuestoTrabajo','Puesto',$query,$datos);
	/*$puestos=consultaBD("SELECT COUNT(codigo) AS total FROM protocolizacion WHERE codigoCliente=".$codigoCliente,true,true);
	if($puestos['total']>0){
		campoOculto('','puestoManual');
	} else {*/
		campoTexto('puestoManual','Puesto manual',$datos);
	//}
}


function camposHabitos($datos){
	abrePestaniaAPI(2);

	abreColumnaCampos();

	echo '<div id="divDescPersona">';
		campoRadio('sexo','Sexo',$datos,'V',array('V','M'),array('V','M'));
		campoEdadReconocimientoMedico($datos);
		campoTextoSimbolo('talla','Talla','cm',$datos);
		campoTextoSimbolo('peso','Peso','kg',$datos);
	echo '</div>';
	campoTexto('imc','I.M.C.',$datos,'input-mini pagination-right',true);
	campoTexto('sistolica','Sistólica',$datos,'input-mini pagination-right');
	campoTexto('diastolica','Diastólica',$datos,'input-mini pagination-right');
	campoTexto('frecuenciaCardiaca','Frecuencia cardiaca',$datos,'input-mini pagination-right');
	
	$saturacionO2=array('','Normal, entre 95-99%','Hipoxia leve, entre 91-94%','Hipoxia moderada, entre 86-90%','Hipoxia severa <86%');
	campoSelect('saturacionO2','Saturación O<sub>2</sub>',$saturacionO2,$saturacionO2,$datos);


	campoSelect('alcohol','Alcohol',array('','No consume','Esporádico','Habitual fin de semana','Habitual diario','Dependiente','Ex-Consumidor'),array('','No consume','Esporádico','Habitual fin de semana','Habitual diario','Dependiente','Ex-Consumidor'),$datos);
	echo "<div class='hide' id='cajaAlcohol'>";
		campoTexto('textoAlcohol','Alcohol',$datos);
	echo "</div>";

	campoSelect('tipoFumador','Tipo fumador',array('','No fumador','Ex-Fumador','Fumador','Desconocido'),array('','No fumador','Ex-Fumador','Fumador','Desconocido'),$datos);

	echo "<div class='hide' id='cajaFumador'>";
		campoTexto('aniosFumando','Años fumando',$datos,'input-mini pagination-right');
		campoTexto('tabacoDia','Tabaco (unidades/día)',$datos,'input-mini pagination-right');
		campoTexto('indicePaquetesAnio','Índice paquetes-año',formateaNumeroWeb($datos['indicePaquetesAnio']),'input-mini pagination-right');
	echo "</div>";
	
	cierraColumnaCampos();
	abreColumnaCampos();

	campoRadio('bebidasEstimulantes','Bebidas estimulantes',$datos);
	creaTablaBebidasEstimulantes($datos);

	campoRadio('checkTratamientoHabitual','Tratamiento habitual',$datos);
	echo "<div class='hide' id='cajaTratamientoHabitual'>";
		areaTexto('tratamientoHabitual','Tratamiento habitual',$datos);
	echo "</div>";

	campoRadio('checkVacunaciones','Vacunaciones',$datos);
	echo "<div class='hide' id='cajaVacunaciones'>";
		campoCheck('checkVacunaciones','Vacunaciones',$datos,array('Infancia','Td','VHA','VHB'),array('Infancia','Td','VHA','VHB'));
		campoTexto('otrasVacunaciones','Otras vacunaciones',$datos);
	echo "</div>";

	$ejercicios=array('','Desconocido','No realiza','No realiza ejercicio físico de forma habitual','Realiza ejercicio físico');
	campoSelect('ejercicio','Ejercicio',$ejercicios,$ejercicios,$datos);
	creaTablaEjercicios($datos);

	campoSelect('alimentacion','Alimentación',array('','Su alimentación es equilibrada-variada','Su alimentación es hipocalorica','Su alimentación es pobre en grasa','Su alimentación es pobre en CH','Su alimentación es vegetariana','Su alimentación es libre de gluten'),array('','Su alimentación es equilibrada-variada','Su alimentación es hipocalorica','Su alimentación es pobre en grasa','Su alimentación es pobre en CH','Su alimentación es vegetariana','Su alimentación es libre de gluten'),$datos,'selectpicker span3 show-tick selectHabito');
	campoTexto('alimentacionTexto','Alimentación',$datos,'span3');
	campoSelect('calidadSuenio','Calidad del sueño',array('','Bien: 6 o más horas','Poco: menos de 6 horas. Sin insomnio','Insomnio: Menos de 6 horas','Insomnio habitual'),array('','Bien: 6 o más horas','Poco: Menos de 6 horas. Sin insomnio','Insomnio: Menos de 6 horas','Insomnio habitual'),$datos,'selectpicker span3 show-tick selectHabito');
	campoTexto('calidadSuenioTexto','Calidad del sueño',$datos,'span3');
	areaTexto('historiaActual','Historia actual',$datos);

	cierraColumnaCampos();

	cierraPestaniaAPI();
}

function creaTablaBebidasEstimulantes($datos){
	echo "
    <div class='control-group hide' id='cajaBebidasEstimulantes'>                     
        <label class='control-label'>Indique bebidas:</label>
        <div class='controls'>
            <table class='tabla-simple' id='tablaBebidasEstimulantes'>
            	<thead>
            		<tr>
            			<th>Bebida</th>
            			<th>Unidades/día</th>
            			<th></th>
            		</tr>
            	</thead>
                <tbody>";
    
    			$i=0;
    			if($datos){
    				$consulta=consultaBD("SELECT * FROM bebidas_estimulantes_reconocimiento_medico WHERE codigoReconocimientoMedico='".$datos['codigo']."';",true);
    				
    				while($bebidas=mysql_fetch_assoc($consulta)){
    					imprimeLineaTablaBebidasEstimulantes($bebidas,$i);
    					$i++;
    				}

    			}

    			if($i==0){
    				imprimeLineaTablaBebidasEstimulantes(false,$i);
    			}

    echo "
    			</tbody>
            </table>
            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaBebidasEstimulantes\");'><i class='icon-plus'></i> Añadir bebida</button> 
            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaBebidasEstimulantes\");'><i class='icon-trash'></i> Eliminar bebida</button>
        </div>
    </div>
    <br />";
}

function imprimeLineaTablaBebidasEstimulantes($datos,$i){
	$j=$i+1;

    echo "
    <tr>";

    	campoTextoTabla('bebida'.$i,$datos['bebida']);
    	campoTextoTabla('unidadesDia'.$i,$datos['unidadesDia'],'input-mini pagination-right');

    echo "
    	<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    
    </tr>";
}


function creaTablaEjercicios($datos){
	echo "
    <div class='control-group hide' id='cajaEjercicios'>                     
        <label class='control-label'>Indique ejercicios:</label>
        <div class='controls'>
            <table class='tabla-simple' id='tablaEjercicios'>
            	<thead>
            		<tr>
            			<th>Ejercicio</th>
            			<th>Frecuencia</th>
            			<th></th>
            		</tr>
            	</thead>
                <tbody>";
    
    			$i=0;
    			if($datos){
    				$consulta=consultaBD("SELECT * FROM ejercicios_reconocimiento_medico WHERE codigoReconocimiento='".$datos['codigo']."';",true);
    				
    				while($bebidas=mysql_fetch_assoc($consulta)){
    					imprimeLineaTablaEjercicios($bebidas,$i);
    					$i++;
    				}

    			}

    			if($i==0){
    				imprimeLineaTablaEjercicios(false,$i);
    			}

    echo "
    			</tbody>
            </table>
            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaEjercicios\");'><i class='icon-plus'></i> Añadir ejercicio</button> 
            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaEjercicios\");'><i class='icon-trash'></i> Eliminar ejercicio</button>
        </div>
    </div>
    <br />";
}

function imprimeLineaTablaEjercicios($datos,$i){
	$j=$i+1;

    echo "
    <tr>";

    	campoTextoTabla('ejercicio'.$i,$datos['ejercicio']);
    	campoTextoTabla('frecuencia'.$i,$datos['frecuencia'],'input-small');

    echo "
    	<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    
    </tr>";
}



function campoEdadReconocimientoMedico($datos){
	$valor=false;

	if($datos){
		$valor=obtieneEdadEmpleadoReconocimientoMedico($datos['codigoEmpleado'],true);
	}

	campoFecha('fechaNacimiento','Nacimiento',$valor);
	
	$valorEdad=$datos;
	if($datos['edadReconocimiento']==0){
		$valorEdad=$valor['edad'];
	}

	campoTexto('edadReconocimiento','Edad',$valorEdad,'input-mini pagination-right');
}

function obtieneEdadEmpleadoReconocimientoMedico($codigoEmpleado,$conexion){
	$res=array('edad'=>'','fechaNacimiento'=>'');

	$empleado=consultaBD("SELECT fechaNacimiento FROM empleados WHERE codigo=$codigoEmpleado",$conexion,true);	

	$res['fechaNacimiento']=$empleado['fechaNacimiento'];
	$res['edad']=calculaEdadEmpleado($empleado['fechaNacimiento']);

	return $res;
}

function camposAntecedentes($datos){
	abrePestaniaAPI(3);

	abreColumnaCampos();

	campoTexto('antecedentesPersonales1','Riesgo cardiovascular',$datos);
	campoTexto('antecedentesPersonales2','Enf. Cardiorrespiratorios',$datos);
	campoTexto('antecedentesPersonales3','Enf.Digestivo',$datos);
	campoTexto('antecedentesPersonales4','Enf.Genitourinario',$datos);
	campoTexto('antecedentesPersonales5','Enf. Ap. locomotor y neur.',$datos);
	campoTexto('antecedentesPersonales6','Enf. ORL',$datos);
	campoTexto('antecedentesPersonales7','Enf. Oftalmológico',$datos);
	campoTexto('antecedentesPersonales8','Cirugía',$datos);
	campoTexto('antecedentesPersonales9','Otras',$datos);
	areaTexto('antecedentesFamiliares','Antecedentes familiares',$datos); 

	cierraColumnaCampos();
	abreColumnaCampos();
	$texto=$datos?$datos['antecedentesPersonales']:"Riesgo cardiovascular: No HTA, no diabetes, no dislipemia. \nEnf.Cardiorrespiratorios: No cardiopatía, ni broncopatías. \nEnf.Digestivo: No antecedentes digestivos ni hepatobiliares. \nEnf.Genitourinario: No antecedentes urinarios. \nEnf.Ap.locomotor y neurológico: No refiere. \nEnf.ORL: No refiere. \nEnf.Oftalmológico: No refiere. \nCirugía: No refiere.";
	
	areaTexto('antecedentesPersonales','Plantilla antecedentes personales',$texto);
	creaTablaSelectorEnfermedades($datos);

	cierraColumnaCampos(true);
	
	cierraPestaniaAPI();
}

function creaTablaSelectorEnfermedades($datos){
	echo "
	<table id='tablaEnfermedadesReconocimiento' class='table table-striped table-bordered datatable'>
		<thead>
			<tr>
				<th>Enfermedades</th>
				<th>Nº campo</th>
				<th>An. personales </th>
                <th>An. familiares </th>
                <th>Recom.</th>
			</tr>
		</thead>

        <tbody>";

        conexionBD();

        $i=0;

        $consulta=consultaBD("SELECT codigo, CONCAT(referencia,' - ',nombre) AS nombre, descripcion, numCampo FROM enfermedades WHERE eliminado='NO' ORDER BY referencia;");
        while($enfermedad=mysql_fetch_assoc($consulta)){
        	$valor=compruebaValoresEnfermedadCheckSelectorEnfermedades($datos,$enfermedad['codigo']);

        	echo "
        	<tr>
        		<td>".$enfermedad['nombre'];
        			 campoOculto($enfermedad['codigo'],'codigoEnfermedad'.$i);
        	echo "
        		</td>";
        	echo "
        		<td class='centro'>".$enfermedad['numCampo'];
        			 campoOculto($enfermedad['numCampo'],'numCampo'.$i);
        			 divOculto($enfermedad['descripcion'],'descripcionEnfermedad'.$i);
        	echo "
        		</td>";

        		campoCheckTabla('checkAntecedentesPersonales'.$i,$valor['checkAntecedentesPersonales']);
        		campoCheckTabla('checkAntecedentesFamiliares'.$i,$valor['checkAntecedentesFamiliares']);
        		campoCheckTabla('checkAdjuntarRecomendaciones'.$i,$valor['checkAdjuntarRecomendaciones']);

        	echo "
        	</tr>";

        	$i++;
        }


        cierraBD();

            	
	echo "
    	</tbody>
	</table>
	
    <br />";
}

function compruebaValoresEnfermedadCheckSelectorEnfermedades($datos,$codigoEnfermedad){
	$res=array(
		'checkAntecedentesPersonales'	=>	false,
		'checkAntecedentesFamiliares'	=>	false,
		'checkAdjuntarRecomendaciones'	=>	false
	);

	if($datos){
		$codigoReconocimiento=$datos['codigo'];

		$res=consultaBD("SELECT checkAntecedentesPersonales, checkAntecedentesFamiliares, checkAdjuntarRecomendaciones FROM enfermedades_antecedentes_reconocimientos_medicos WHERE codigoReconocimiento='$codigoReconocimiento' AND codigoEnfermedad='$codigoEnfermedad';",false,true);
	}

	return $res;
}


function camposAntecedentes2($datos){
	abrePestaniaAPI(13);

	abreColumnaCampos();

	campoRadio('trastornosCongenitos','Tras. congénitos o adquiridos',$datos);
	echo "<div class='cajaTextoAsociado hide'>";
	areaTexto('textoTrastornosCongenitos','Trastornos congénitos o adquiridos',$datos);
	echo "</div>";

	campoRadio('fracturas','Fracturas',$datos);
	echo "<div class='cajaTextoAsociado hide'>";
	areaTexto('textoFracturas','Fracturas o traumatismos',$datos);
	echo "</div>";
	
	campoRadio('minusvaliaReconocida','Minusvalía reconocida',$datos);
	echo "<div class='cajaTextoAsociado hide'>";
	areaTexto('textoMinusvalia','Minusvalía',$datos);
	echo "</div>";


	campoRadio('bajasProlongadas','Bajas prolongadas',$datos);
	echo "<div class='cajaTextoAsociado hide'>";
	areaTexto('textoBajasProlongadas','Bajas prolongadas',$datos); 
	echo "</div>";

	cierraColumnaCampos();
	abreColumnaCampos();


	campoCheck('checkAlergias','Alergias',$datos,array('No refiere','Medioambientales','Alimentarias','Medicamentosas','Laborales'),array('SI','SI','SI','SI','SI'),true);

	echo "
	<div class='hide' id='checkAlergias1Texto'>";
		areaTexto('textoAlergias1','Alergias medioambientales',$datos);
	echo "
	</div>
	<div class='hide' id='checkAlergias2Texto'>";
		areaTexto('textoAlergias2','Alergias alimentarias',$datos);
	echo "
	</div>
	<div class='hide' id='checkAlergias3Texto'>";
		areaTexto('textoAlergias3','Alergias medicamentosas',$datos);
	echo "
	</div>
	<div class='hide' id='checkAlergias4Texto'>";
		areaTexto('textoAlergias4','Alergias laborales',$datos);
	echo "
	</div>";

	cierraColumnaCampos();
	

	cierraPestaniaAPI();
}


function camposExploracion($datos){
	abrePestaniaAPI(4);

	campoSelectDesplegable('inspeccionGeneral','Inspección general',$datos);
	campoSelectDesplegable('oftalmologia','Oftalmología',$datos);
	campoSelectDesplegable('pielMucosas','Piel y mucosas',$datos);
	campoSelectDesplegable('aparatoLocomotor','Aparato locomotor',$datos);
	campoSelectDesplegable('extremidades','Extremidades',$datos);
	campoSelectDesplegable('cuello','Cuello',$datos);

	cierraPestaniaAPI();
}

function camposExploracionInterna($datos){
	abrePestaniaAPI(14);

	campoSelectDesplegable('orl','ORL',$datos);
	campoSelectDesplegable('exploracionNeurologica','Exploración neurológica',$datos);
	campoSelectDesplegable('auscultacionPulmonar','Auscultación pulmonar',$datos);
	campoSelectDesplegable('aparatoDigestivo','Aparato digestivo',$datos);
	campoSelectDesplegable('auscultacionCardiaca','Auscultación cardiaca',$datos);
	campoSelectDesplegable('sistemaUrogenital','Sistema urogenital',$datos);
	campoSelectDesplegable('otros','Otros',$datos);

	cierraPestaniaAPI();
}


function camposVision($datos){
	abrePestaniaAPI(5);

	campoRadio('checkAgudezaVisual','Estudio visual',$datos);

	echo "<div id='divEstudioVisual'>";

		abreColumnaCampos();

		campoCheck('checkObservacionesVision','Diagnóstico', $datos, array('Miopía', 'Hipermetropía', 'Astigmatismo', 'Presbicia', 'Miopía+Astigmatismo', 'Hipermetropía+Astigmatismo', 'Cirugía Refractaria','Otros'),array('SI','SI','SI','SI','SI','SI','SI','SI'),true);
		echo '<div id="divObservacionesVision" class="hide">';
			campoTexto('observacionesVision','Diagnóstico',$datos);
		echo '</div>';
		campoRadio('agudezaVisualCorregida','Agudeza visual corregida',$datos);
		campoRadio('traeGafasLentillas','Trae gafas/lentillas',$datos);
		
		cierraColumnaCampos();
		abreColumnaCampos();

		campoCheck('checkTipoGafasLentillas','Tipo de gafas/lentillas', $datos, array('Progresivas', 'Bifocales', 'Cerca', 'Lejos', 'Otro'),array('SI','SI','SI','SI','SI'));
		echo '<div id="divTipoGafasLentillas" class="hide">';
			campoTexto('tipoGafasLentillasTexto','Tipo de gafas/lentillas',$datos);
		echo '</div>';
		campoTexto('anioUltimaGraduacion','Año última graduación',$datos,'input-mini pagination-right');
		campoTexto('frecuenciaUso','Frecuencia de uso',$datos);
		campoSelect('percepcionColores','Percepción de colores',array('','Normal','Anormal'),array('','Normal','Anormal'),$datos);
		areaTexto('observacionesEstudioVision','Observaciones',$datos);

		cierraColumnaCampos();
		abreColumnaCampos('sinFlotar span8');

		creaTablaEstudioVisionReconocimientoMedico($datos);

		cierraColumnaCampos();

	echo "</div>";

	cierraPestaniaAPI();
}

function camposEspirometria($datos){
	abrePestaniaAPI(15);

	abreColumnaCampos();

	campoRadio('checkEspirometria','Espirometría',$datos);
	campoSelect('valoracionEspirometrica','Valoración espirometría',array('Normal','Patrón obstructivo','Patrón mixto','Alteraciones no obstructivas','Técnica incorrecta. No valorable'),array('Normal','Patrón obstructivo','Patrón mixto','Alteraciones no obstructivas','Técnica incorrecta. No valorable'),$datos);
	areaTexto('observacionesEspirometria','Observaciones',$datos); 

	cierraColumnaCampos();
	abreColumnaCampos();

	campoDato('Sexo','','sexoDato');
	campoDato('Talla','','tallaDato');
	campoDato('Peso','','pesoDato');
	echo '<div id="divCompruebaEspirometria">';
		campoTexto('fvc','F.V.C.',$datos,'input-mini pagination-right');
		campoTexto('fev1','FEV1',$datos,'input-mini pagination-right');
		campoTexto('itiff','I.TIFF.',$datos,'input-mini pagination-right');
	echo '</div>';
	campoTexto('fef25','FEF25-75%',$datos,'input-mini pagination-right');
	campoTexto('pef','PEF',$datos,'input-mini pagination-right');

	cierraColumnaCampos();

	cierraPestaniaAPI();
}


function creaTablaEstudioVisionReconocimientoMedico($datos){
        echo "
        <div class='control-group'>                     
            <label class='control-label'>Estudio visión:</label>
            <div class='controls'>
                <table class='tabla-simple' id='tablaEstudioVision'>
                    <thead>
                        <tr>
                        	<th rowspan='2'></th>
                            <th colspan='2'> Visión lejana </th>
                            <th colspan='2'> Visión cercana </th>
                        </tr>
                        <tr>
                        	<th> Con corrección </th>
                        	<th> Sin corrección </th>
                        	<th> Con corrección </th>
                        	<th> Sin corrección </th>
                        </tr>
                    </thead>
                    <tbody>
                    	<tr>
                    		<th> Ojo derecho </th>";
                    		campoTextoSimbolo('estudioVision1','','/12',$datos,'input-mini pagination-right',1);
                    		campoTextoSimbolo('estudioVision2','','/12',$datos,'input-mini pagination-right',1);
                    		campoTextoSimbolo('estudioVision3','','/12',$datos,'input-mini pagination-right',1);
                    		campoTextoSimbolo('estudioVision4','','/12',$datos,'input-mini pagination-right',1);
         echo "			</tr>
          				<tr>
          					<th> Ojo izquierdo </th>";
          					campoTextoSimbolo('estudioVision5','','/12',$datos,'input-mini pagination-right',1);
                    		campoTextoSimbolo('estudioVision6','','/12',$datos,'input-mini pagination-right',1);
                    		campoTextoSimbolo('estudioVision7','','/12',$datos,'input-mini pagination-right',1);
                    		campoTextoSimbolo('estudioVision8','','/12',$datos,'input-mini pagination-right',1);
        echo "			</tr>
          				<tr>
          					<th> Binocular </th>";
          					campoTextoSimbolo('estudioVision9','','/12',$datos,'input-mini pagination-right',1);
                    		campoTextoSimbolo('estudioVision10','','/12',$datos,'input-mini pagination-right',1);
                    		campoTextoSimbolo('estudioVision11','','/12',$datos,'input-mini pagination-right',1);
                    		campoTextoSimbolo('estudioVision12','','/12',$datos,'input-mini pagination-right',1);
          
        echo "      	</tr>
        			</tbody>
                </table>
            </div>
        </div>
        <br />";
}


function camposAudicion($datos){
	abrePestaniaAPI(6);

	abreColumnaCampos();

	campoRadio('checkAudiometria','Audiometría',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	areaTexto('observacionesAudiometria','Observaciones',$datos);

	cierraColumnaCampos();
	abreColumnaCampos('sinFlotar span8');

	creaTablaEstudioAudicionReconocimientoMedico($datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	campoRadio('checkElectrocardiograma','Electrocardiograma',$datos);
	areaTexto('electrocardiograma','Valoración electrocardiograma',$datos);

	cierraColumnaCampos();

	cierraPestaniaAPI();
}


function creaTablaEstudioAudicionReconocimientoMedico($datos){
        echo "
        <div class='control-group'>                     
            <label class='control-label'>Estudio audición:</label>
            <div class='controls'>
                <table class='tabla-simple' id='tablaEstudioAudicion'>
                    <thead>
                    	<tr>
                        	<th> 500 </th>
                        	<th> 1K </th>
                        	<th> 2K </th>
                        	<th> 3K </th>
                        	<th> 4K </th>
                        	<th> 6K </th>
                        	<th> 8K </th>
                        </tr>
                        <tr>
                            <th colspan='7'> Derecho </th>
                        </tr>
                    </thead>
                    <tbody>
                    	<tr>";
                    		campoTextoTabla('estudioAudiometria1',$datos,'input-mini pagination-right');
                    		campoTextoTabla('estudioAudiometria2',$datos,'input-mini pagination-right');
                    		campoTextoTabla('estudioAudiometria3',$datos,'input-mini pagination-right');
                    		campoTextoTabla('estudioAudiometria4',$datos,'input-mini pagination-right');
                    		campoTextoTabla('estudioAudiometria5',$datos,'input-mini pagination-right');
                    		campoTextoTabla('estudioAudiometria6',$datos,'input-mini pagination-right');
                    		campoTextoTabla('estudioAudiometria7',$datos,'input-mini pagination-right');
         echo "			</tr>
         				<tr>
         					<th colspan='7'> Izquierdo </th>
         				</tr>
          				<tr>";
          					campoTextoTabla('estudioAudiometria8',$datos,'input-mini pagination-right');
                    		campoTextoTabla('estudioAudiometria9',$datos,'input-mini pagination-right');
                    		campoTextoTabla('estudioAudiometria10',$datos,'input-mini pagination-right');
                    		campoTextoTabla('estudioAudiometria11',$datos,'input-mini pagination-right');
                    		campoTextoTabla('estudioAudiometria12',$datos,'input-mini pagination-right');
                    		campoTextoTabla('estudioAudiometria13',$datos,'input-mini pagination-right');
                    		campoTextoTabla('estudioAudiometria14',$datos,'input-mini pagination-right');          
        echo "      	</tr>
        			</tbody>
                </table>
            </div>
        </div>
        <br />";
}


function camposHemograma($datos){
	abrePestaniaAPI(7);

	abreColumnaCampos('span7');

		campoRadio('checkAnalitica','Analítica',$datos);

		echo "<div class='hide cajaAnalitica'>";

		campoDato('Autorellenar',"<button type='button' id='valoresAnaliticaNormales' class='btn btn-small btn-success'><i class='icon-smile-o'></i> Valores normales</button>");
		areaTexto('hemograma','Estudio de hemograma',$datos,'areaTextoAnalitica');	
		areaTexto('bioquimica','Estudio de bioquímica',$datos,'areaTextoAnalitica');
	
		echo "<div class='hide'>";//Oculto los campos de otras pruebas de la analítica porque Patricio me pidió eliminarlos, pero seguro que cuando entre en funcionamiento el procesado de las analíticas nos piden reactivarlo
		campoRadio('checkOtrasPruebas','Otras pruebas',$datos,'SI');
		areaTexto('otrasPruebas','Estudio de otras pruebas',$datos,'areaTextoAnalitica');	
		echo "</div>";

		areaTexto('orina','Estudio orina',$datos,'areaTextoAnalitica');
		campoTexto('analitica','Analítica',$datos,'span4');
		campoFicheroActualizable('ficheroAnalitica','Fichero analítica',0,$datos,'../documentos/analiticas-procesadas/','Descargar');

		echo "</div>";

	cierraColumnaCampos();	

	abreColumnaCampos('span4 cajaAnalitica');
		creaTablaSelectorComentariosAnalitica($datos);
	cierraColumnaCampos();

	cierraPestaniaAPI();
}


function campoFicheroActualizable($nombreCampo,$texto,$tipo=0,$valor=false,$ruta=false,$nombreDescarga='',$claseContenedor=''){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    if(!$valor || $valor=='NO'){
        if($tipo==0){
            echo "
            <div class='control-group $claseContenedor' id='contenedor-$nombreCampo'>                     
                <label class='control-label' for='$nombreCampo'>$texto:</label>
                <div class='controls'>
                    <input type='file' name='$nombreCampo' id='$nombreCampo' /><div class='tip'>Solo se admiten ficheros de 5 MB como máximo</div>
                </div> <!-- /controls -->       
            </div> <!-- /control-group -->";
        }
        elseif($tipo==1){
            echo "<td class='$claseContenedor' id='contenedor-$nombreCampo'><input type='file' name='$nombreCampo' id='$nombreCampo' /></td>";
        }
        else{
            echo "<span class='$claseContenedor' id='contenedor-$nombreCampo'><input type='file' name='$nombreCampo' id='$nombreCampo' /></span>";
        }
    }
    else{
        campoDescargaActualizable($nombreCampo,$texto,$ruta,$valor,$tipo,$nombreDescarga);
        campoOculto($valor,$nombreCampo);
    }
}

function campoDescargaActualizable($nombreCampo,$texto,$ruta,$valor,$tipo=0,$nombreDescarga=''){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    
    if($nombreDescarga==''){
        $nombreDescarga=$valor;
    }

    
    if($valor=='NO' || $valor==''){
        campoFicheroActualizable($nombreCampo,$texto,$tipo,$valor,$ruta);
    }
    else{
        if($tipo==0){
            echo "
                <div class='control-group' id='contenedor-$nombreCampo'>                     
                  <label class='control-label'>$texto:</label>
                  <div class='controls'>
                    <a class='btn btn-propio noAjax' href='$ruta$valor' target='_blank'><i class='icon-cloud-download'></i> $nombreDescarga</a>
                    <button type='button' class='iconoBorrado' id='elimina-$nombreCampo'><i class='icon-trash'></i> Eliminar</button>
                  </div> <!-- /controls -->       
                </div> <!-- /control-group -->";
        }
        elseif($tipo==1){
            echo "<td><a class='btn btn-propio noAjax descargaFichero' href='$ruta$valor' target='_blank' nombre='$nombreCampo'><i class='icon-cloud-download'></i> $nombreDescarga</a></td>";
        }
        else{
            echo "<a class='btn btn-propio noAjax' href='$ruta$valor' target='_blank'><i class='icon-cloud-download'></i> $nombreDescarga</a>";
        }

        campoFicheroActualizable($nombreCampo.'Oculto',$texto,$tipo,false,$ruta,'','hide');
    }
}

function camposOtrasPruebas($datos){
	abrePestaniaAPI(12);

	campoRadio('checkOtrasPruebasFuncionales','Otras pruebas funcionales',$datos);
	echo '<div id="divOtrasPruebas" class="hide">';
	creaTablaOtrasPruebas($datos['codigo']);
	echo '</div>';

	cierraPestaniaAPI();
}

function camposRadiologia($datos){
	abrePestaniaAPI(8);

	abreColumnaCampos();

	campoRadio('checkRadiologia','Radiología',$datos);
	campoTextoSimbolo('contabilizacionEconomica','Contabilización económica','<i class="icon-euro"></i>',$datos);
	areaTexto('radiologia','Estudio radiología',$datos);
	campoFicheroActualizable('ficheroRadiologia','Fichero radiologia',0,$datos,'../documentos/analiticas-procesadas/','Descargar');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoRadio('checkDinamometria','Dinamometría',$datos);
	creaTablaDinanometria($datos);
	campoSelect('resultadoDinamometria','Resultado dinamometría',array('','Normal','Débil','Fuerte'),array('','Normal','Débil','Fuerte'),$datos,'selectpicker span2 show-tick','');

	cierraColumnaCampos(true);

	cierraPestaniaAPI();
}

function creaTablaDinanometria($datos){
        echo "
        <div class='control-group'>                     
            <label class='control-label'>Estudio:</label>
            <div class='controls'>
                <table class='tabla-simple' id='tablaEstudioVision'>
                    <tbody>
                        <tr>
                        	<th> Mano derecha </th>";
                        	campoNumeroTabla('manoDerecha',$datos);
        echo "          </tr>
                    	<tr>
                    	<th> Mano izquierda </th>";
                    		campoNumeroTabla('manoIzquierda',$datos);
        echo "          </tr>
                    	<tr>
                    	<th> Mano dominante </th>";
                    		campoSelect('manoDominante','',array('','Derecha','Izquierda'),array('','Derecha','Izquierda'),$datos,'selectpicker span2',"",1);
       
        echo "      	</tr>
        			</tbody>
                </table>
            </div>
        </div>
        <br />";
}

function campoNumeroTabla($nombreCampo,$valor='0',$clase='input-small pagination-right',$min='',$max=''){//ACTUALIZACIÓN 20/01/2015: añadidos los parámetros min y max, que serían de la forma: "min='1'"
	$valor=compruebaValorCampo($valor,$nombreCampo);

	echo "<td>
      	<input type='number' name='$nombreCampo' id='$nombreCampo' class='$clase' value='$valor' $min $max />
      	</td>";
}


function camposJuicioClinico($datos){
	abrePestaniaAPI(9);

	abreColumnaCampos();

	$valoresApto=array(	'',
						'APTO',
						'NO APTO',
						'APTO pendiente de Analítica.',
						'APTO CON LIMITACIONES: el trabajador puede desarrollar las tareas fundamentales de su puesto, pero algunas otras no puede de ninguna forma o sólo parcialmente.',
						'APTO EN OBSERVACION: el trabajador puede desarrollar su trabajo pero los datos obtenidos del reconocimiento son insuficientes y estamos a la espera de más información, se trata de una situación temporal, que debe concluir su aptitud.',
						'NO APTO TEMPORAL: el trabajador no puede en el momento actual desarrollar las tareas fundamentales de su puesto de trabajo. Es tambien una situación temporal y limitada en el tiempo que debe modificarse en un plazo razonable.',
						'NO APTO DEFINITIVO: el trabajador no puede desarrollar las tareas fundamentales de su puesto de trabajo y no hay posibilidades de recuperación.',
						'SIN CRITERIO DE APTITUD: el criterio de aptitud será dado por el Servicio Medico de Empresa.',
						'CRITERIO DE APTITUD INCOMPLETO ó NO VALORABLE: el trabajador no realiza todas las pruebas necesarias para emitir un criterio de aptitud.'
					);

	campoSelect('apto','Consideraciones de apto',$valoresApto,$valoresApto,$datos);
	areaTexto('textoApto','Descripción apto',$datos);
	
	campoRadio('acudirMedicoEspecialista','Debe acudir a especialista',$datos);
	echo "<div class='cajaTextoAsociado hide'>";
	areaTexto('textoAcurdirMedicoEspecialista','Debe acudir a su médico especialista',$datos);
	echo "</div>";

	campoRadio('debeAcudirMedico','Debe acudir a su médico',$datos);
	campoRadio('advertencia','Registrar advertencia',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectConsulta('codigoProfesionalExterno','Profesional externo',"SELECT codigo, CONCAT(nombre,' ',apellidos,' (',precio,' €)') AS texto FROM profesionales_externos ORDER BY nombre;",$datos);
	areaTexto('observacionesNoImprimibles','Observaciones no imprimibles',$datos);

	cierraColumnaCampos();

	cierraPestaniaAPI();
}

function camposRecomendaciones($datos){
	abrePestaniaAPI(10);

	areaTextoRecomendaciones($datos);

	echo "<i class='icon-chevron-up flechaRecomendaciones' title='Marque los checks de abajo para incluirlos en el cuadro de texto'></i>";

	abreColumnaCampos('span5');

	campoCheckIndividual('checkRecomendacion','Beba abundante líquido (un total de 2 a 2,5 litros/día).');
	campoCheckIndividual('checkRecomendacion','No puede trabajar sin elementos de protección adecuados.');
	campoCheckIndividual('checkRecomendacion','No puede trabajar sin los elementos de protección adecuados.');
	campoCheckIndividual('checkRecomendacion','Restringido a trabajar a nivel de suelo , pero puede subir a escaleras.');
	campoCheckIndividual('checkRecomendacion','Restringido a trabajar a nivel de suelo.');
	campoCheckIndividual('checkRecomendacion','Son las que imponen: art 6 del E.T. y Decreto 26/071957.');
	campoCheckIndividual('checkRecomendacion','Su trabajo no requerirá el uso de los brazos por encima de los hombros.');
	campoCheckIndividual('checkRecomendacion','Debe reducir la ingesta de alimentos ricos en grasa de origen animal.');
	campoCheckIndividual('checkRecomendacion','Usar protecciones auditivas según RD 1316/89 (riesgos derivados del ruido).');
	campoCheckIndividual('checkRecomendacion','No puede trabajar sin protección auditivas en ambientes ruidosos.');

	cierraColumnaCampos();
	abreColumnaCampos('span3');

	
	campoCheckIndividual('checkRecomendacion','Debe reducir el consumo de tabaco.');
	campoCheckIndividual('checkRecomendacion','Tome alimentos ricos en fibra.');
	campoCheckIndividual('checkRecomendacion','No debe beber alcohol.');
	campoCheckIndividual('checkRecomendacion','No debe forzar la voz.');
	campoCheckIndividual('checkRecomendacion','No debe fumar.');
	campoCheckIndividual('checkRecomendacion','No puede estar en contacto con las siguientes substancias:');
	
	campoCheckIndividual('checkRecomendacion','No puede trabajar a mas de 4 metros de altura.');
	campoCheckIndividual('checkRecomendacion','No puede trabajar en puestos con mas de 80 dB A.');

	cierraColumnaCampos();
	abreColumnaCampos('span3');

	campoCheckIndividual('checkRecomendacion','Control de las alteraciones analíticas.');
	campoCheckIndividual('checkRecomendacion','Debe controlarse su tensión arterial.');
	campoCheckIndividual('checkRecomendacion','Debe disminuir su peso.');
	campoCheckIndividual('checkRecomendacion','Debe evitar ambientes ruidoso.');
	campoCheckIndividual('checkRecomendacion','Debe evitar estar de pie prolongadamente.');
	campoCheckIndividual('checkRecomendacion','Debe extremar la higiene buco-dental.');
	campoCheckIndividual('checkRecomendacion','Debe incrementar el ejercicio físico.');
	campoCheckIndividual('checkRecomendacion','Debe reducir el consumo de alcohol.');
	campoCheckIndividual('checkRecomendacion','Debe reducir el consumo de hidratos de carbono.');

	cierraColumnaCampos(true);

	cierraPestaniaAPI();
}

function areaTextoRecomendaciones($valor=false){
	$valor=compruebaValorCampo($valor,'recomendaciones');
	
	echo "<textarea name='recomendaciones' class='areaTextoRecomendaciones' id='recomendaciones'>$valor</textarea><br /><br />";
}


function camposRiesgosProtocolizacionEpis($datos){
	abrePestaniaAPI(11);

	campoTexto('protocoloClasificacion','Protocolos clasificación',$datos,'span8');

	campoRadio('toleraEpis','Tolera bien los EPIs',$datos,'SI');

	echo "<div class='hide' id='cajaIntoleranciasEpis'>";
	areaTexto('intoleranciasEpis','Intolerancias',$datos);
	echo "</div>";

	creaTablaEpis($datos['codigo'],'epis_en_reconocimiento_medico','codigoReconocimientoMedico');
	
	cierraPestaniaAPI();
}

function obtieneDatosProtocoloPuesto(){
	$res=array(
		'riesgos'		=>	'',
		'protocolos'	=>	'',
		'otrasPruebas'	=>	'NO',
		'pruebas'		=>	''
	);

	$datos=arrayFormulario();
	extract($datos);

	conexionBD();

	$protocolizacion=consultaBD("SELECT resumenRiesgos FROM protocolizacion WHERE codigo='$codigoPuesto';",false,true);
	$res['riesgos']=$protocolizacion['resumenRiesgos'];

	$consulta=consultaBD("SELECT nombre 
						  FROM protocolos 
						  WHERE codigo IN(
						  	SELECT codigoProtocolo FROM protocolos_asociados_protocolizacion INNER JOIN protocolizacion ON protocolos_asociados_protocolizacion.codigoProtocolizacion=protocolizacion.codigo
						  	WHERE protocolizacion.codigo='$codigoPuesto'
						  )
						  GROUP BY codigo
						  ORDER BY nombre;");

	while($datos=mysql_fetch_assoc($consulta)){
		$res['protocolos'].=$datos['nombre'].', ';
	}

	if($res['protocolos']!=''){
		$res['protocolos']=quitaUltimaComa($res['protocolos']);
	}



	ob_start();//Para almacenar los echo de imprimeLineaTablaOtrasPruebas() en el búffer

	$i=0;
	$consulta=consultaBD("SELECT texto, valoracionEconomica FROM pruebas_protocolizacion WHERE codigoProtocolizacion='$codigoPuesto';");
	while($datos=mysql_fetch_array($consulta)){
		$datos['resultado']='';//Para que no dé error por undefined index
		imprimeLineaTablaOtrasPruebas($i,$datos);

		$i++;
	}

	$pruebas=ob_get_clean();
	if($i>0){
		$res['otrasPruebas']='SI';
		$res['pruebas']=$pruebas;
	}

	cierraBD();

	echo json_encode($res);
}


function obtieneDatosEmpleadoReconocimientoMedico(){
	$res=array();

	$datos=arrayFormulario();
	extract($datos);

	conexionBD();

	$empleado=consultaBD("SELECT empleados.fechaNacimiento, puestos_trabajo.*, sexoEmpleado
						  FROM empleados LEFT JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo 
						  WHERE empleados.codigo=$codigoEmpleado",false,true);

	$res['codigoPuestoTrabajo']=$empleado['codigo'];
	$res['fechaNacimiento']=formateaFechaWeb($empleado['fechaNacimiento']);
	$res['edad']=calculaEdadEmpleado($empleado['fechaNacimiento']);
	$res['sexo']=$empleado['sexoEmpleado'];

	cierraBD();

	echo json_encode($res);
}

function compruebaCampoNULLEmpleadoReconocimiento($valor){
	$res=$valor;

	if($valor==NULL){
		$res='Sin definir';
	}

	return $res;
}

function consultaReconocimientoMedicoAnteriorEmpleado(){
	$res='NO';

	$datos=arrayFormulario();
	extract($datos);

	$consulta=consultaBD("SELECT codigo FROM reconocimientos_medicos WHERE codigoEmpleado='$codigoEmpleado' AND eliminado='NO' ORDER BY fecha DESC;",true,true);
	if($consulta){
		$res=$consulta['codigo'];
	}

	echo $res;
}

function datosQueFaltan($datos){
	for($i=0;$i<11;$i++){
		$datos['checkClasificacion'.$i]='';
	}
	$datos['riesgosLaborales']='';
	$datos['cafeDia']='';
	$datos['checkHipercolesteremia']='';
	$datos['checkHipertension']='';
	return $datos;
}


function generaPdfReconocimiento($codigoReconocimiento){
	$datos=consultaBD("SELECT reconocimientos_medicos.*, clientes.EMPNOMBRE AS empresa, CONCAT(empleados.nombre,' ',empleados.apellidos) AS empleado, EMPDIR, EMPLOC, EMPCP, EMPPROV,
					   empleados.dni, puestos_trabajo.nombre AS puesto, empleados.direccion, empleados.cp, empleados.ciudad, empleados.provincia,
					   empleados.fechaNacimiento, actividades.nombre AS actividad, puestos_trabajo.nombre AS puesto, 
					   empleados.trabajadorSensible, empleados.fechaNacimiento, clientes.codigo AS codigoCliente, puestos_trabajo.codigo AS codigoPuesto, reconocimientos_medicos.puestoManual,
					   protocolizacion.nombrePuesto

					   FROM reconocimientos_medicos LEFT JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
					   LEFT JOIN clientes ON empleados.codigoCliente=clientes.codigo 
					   LEFT JOIN actividades ON clientes.EMPACTIVIDAD=actividades.codigo
					   LEFT JOIN protocolizacion ON reconocimientos_medicos.codigoPuestoTrabajo=protocolizacion.codigo
					   LEFT JOIN puestos_trabajo ON protocolizacion.codigoPuestoTrabajo=puestos_trabajo.codigo
					   WHERE MD5(reconocimientos_medicos.codigo)='$codigoReconocimiento';",true,true);

	$vacunaciones=obtieneVacunasPdfReconocimiento($datos);
	$edad=calculaEdadEmpleado($datos['fechaNacimiento']);
	$firma=obtieneFirmaPdfReconocimiento();
	$meses=array(''=>'','01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
	$fecha=explode('-',$datos['fecha']);
	$fecha=$fecha[2].' de '.$meses[$fecha[1]].' de '.$fecha[0];
	$tipoReconocimiento=$datos['tipoReconocimiento']=='Otros'?$datos['tipoReconocimientoTexto']:$datos['tipoReconocimiento'];
	$rowspan=1;
	$puestosAnteriores='';
	for($i=2;$i<=3;$i++){
		if($datos['puestoAnterior'.$i]!=''){
			$puestosAnteriores.="
					<tr>
					<td class='col2-2-1'>".$datos['puestoAnterior'.$i]."</td>
					<td class='col2-2-2 centro'>".$datos['tiempoPuestoAnterior'.$i]." meses</td>
					</tr>";
			$rowspan++;
		}
	}
	$riesgosExtralaborales=$datos['riesgosExtralaborales']==''?'No refiere aficiones ni actividades de riesgos extra laborales.':$datos['riesgosExtralaborales'];
	
	$epis=obtenerEpisPDF($datos);
	$alteracion=$datos['toleraEpis']=='SI'?'No refiere.':$datos['intoleranciasEpis'];
	$familiares=$datos['antecedentesFamiliares']==''?'No refiere antecedentes médicos de interés.':$datos['antecedentesFamiliares'];
	$vacunaciones=obtenerVacunacionesPDF($datos);
	
	$fumador=$datos['tipoFumador'];
	if($fumador=='Fumador'){
		$fumador.=', '.$datos['tabacoDia'].' cig/día desde hace '.$datos['aniosFumando'].' año/s.';
		$indice=$datos['tabacoDia']/20 * $datos['aniosFumando'];
		$fumador.='<br/><b>Índice de paquetes/año: </b>'.$indice;
	} 
	

	$listadoAlcohol=array(''=>'','No consume'=>'No consumidor de alcohol','Esporádico'=>'Consumidor esporádico de alcohol','Habitual fin de semana'=>'Consumidor habitual en fines de semana de alcohol','Habitual diario'=>'Consumidor habitual diario de alcohol','Dependiente'=>'Consumidor dependiente de alcohol','Ex-Consumidor'=>'Ex-Consumidor de alcohol');
	$alcohol=$listadoAlcohol[$datos['alcohol']];
	if($alcohol!=''){
		$alcohol.=' (se considera consumo de riesgo si es >280 gr/semana). '.$datos['textoAlcohol'];
	}
	$bebidas="<tr><td class='col2-2'>No toma bebidas estimulantes</td></tr>";
	$listado=consultaBD('SELECT * FROM bebidas_estimulantes_reconocimiento_medico WHERE codigoReconocimientoMedico='.$datos['codigo'],true);
	$rowBebidas=4;
	while($bebida=mysql_fetch_assoc($listado)){
		if($bebida['bebida']!=''){
			if($rowBebidas==4){
				$bebidas="";
			}
			$bebidas.="<tr><td class='col2-2'>Toma ".$bebida['unidadesDia'].' '.$bebida['bebida'].'/día</td></tr>';
			$rowBebidas++;
		}
	}
	if($rowBebidas>4){
		$rowBebidas--;
	}

	$sexo=array('V'=>'Varón','M'=>'Mujer');
	
	$edad=$datos['edadReconocimiento'];
	if($edad==0){
		$edad=calculaEdadEmpleado($datos['fechaNacimiento']);
	}
	
	$fmaxima=0;
	if($edad>0){
		$fmaxima=220-$edad;
	}
	
	$pruebas=obtienePruebasPDF($datos);
	
	
	$listado=consultaBD('SELECT * FROM accidentes WHERE codigoEmpleado='.$datos['codigoEmpleado'].' ORDER BY fechaAccidente DESC',true);
	$accidente='';
	while($item=mysql_fetch_assoc($listado)){
		if($accidente!=''){
			$accidente.='<br/>';
		}
		$accidente.=formateaFechaWeb($item['fechaAccidente']).' - '.nl2br($item['descripcion']);;
	}
	if($accidente==''){
		$accidente='No refiere accidente de trabajo ni enfermedad profesional';
	}

	$antecedentesPersonales=obtieneAntecedentesPersonalesPDF($datos);

	$limitaciones='No refiere';
	if($datos['fracturas']=='SI'){
		$limitaciones=$datos['textoFracturas'];
	}

	$sobrepeso='No';
	if($datos['imc']>=50 || $datos['imc']>=40 || $datos['imc']>=35 || $datos['imc']>=30 || $datos['imc']>=27 || $datos['imc']>=25) {
		$sobrepeso='Si';
	}

	$frecuencia='normal';
	if($datos['frecuenciaCardiaca']<60){
		$frecuencia='baja';
	} else if($datos['frecuenciaCardiaca']>100){
		$frecuencia='alta';
	}
	

	$tensionArterial=obtieneTA($datos);

	$alergias=obtieneAlergiasPDF($datos);


	//Parte de dirección (se obtiene la del empleado y si no tiene, la de la empresa)
	$direccion=$datos['direccion'];
	$localidad=$datos['ciudad'];
	$cp=$datos['cp'];
	$provincia=$datos['provincia'];

	if($direccion==''){
		$direccion=$datos['EMPDIR'];
		$localidad=$datos['EMPLOC'];
		$cp=$datos['EMPCP'];
		$provincia=$datos['EMPPROV'];
	}
	//Fin parte de dirección

	//Parte de puesto (se prioriza el de la protocolización, si no el manual)
	$puesto=$datos['nombrePuesto'];
	if($puesto==''){
		$puesto=$datos['puestoManual'];
	}
	//Fin parte de puesto


	//Parte de riesgos cv
	$hiperglucemia=obtieneRiesgoHiperglucemia($datos['codigo']);

	$tabaco='No';
	if($datos['tipoFumador']=='Fumador'){
		$tabaco='Si';
	}

	$sedentarismo='Si';
	if($datos['ejercicio']=='Realiza ejercicio físico'){
		$sedentarismo='No';
	}
	//Fin parte de riesgos cv


	//Parte calidad del sueño
	$calidadSuenio=$datos['calidadSuenio'];
	if($datos['calidadSuenioTexto']!=''){
		$calidadSuenio.='. '.$datos['calidadSuenioTexto'];
	}
	//Fin parte calidad del sueño

	$minusvalia='No';
	if($datos['minusvaliaReconocida']=='SI'){
		$minusvalia='Si.<br />'.$datos['textoMinusvalia'];
	}

	$bajasProlongadas='No';
	if($datos['bajasProlongadas']=='SI'){
		$bajasProlongadas=$datos['textoBajasProlongadas'];
	}


	//Parte de tratamiento/medicación habitual (si es vacío se pone que no refiere)
	$medicacionHabitual=nl2br($datos['tratamientoHabitual']);
	if(trim($medicacionHabitual)==''){
		$medicacionHabitual='No refiere.';
	}
	//Fin parte de tratamiento/medicación habitual

    $contenido = "
	<style type='text/css'>
	<!--
		.cabecera{
			text-align:center;
			height:10px;
			padding:0px;
		}

		.logoCabecera{
			width:70px;
			height:70px;
			position:absolute;
			top:0px;
		}

		.ventanaCarta{
			color:#666;
			text-align:right;
			margin-top:20px;
			line-height:20px;
		}

		.confidencial{
			font-size:18px;
			font-weight:bold;
			color:#555;
			text-align:right;
			margin-top:20px;
			margin-right:150px;
		}

		.negrita{
			font-weight:bold;
		}

		.cajaTablaPortada{
			margin-top:50px;
			text-align:center;
			width:100%;
		}

		.tablaPortada{
			border:2px solid #060685;
			text-align:left;
			width:89%;
			margin-left:70px;
		}

		.tablaPortada td{
			width:65%;
			font-weight:bold;
		}

		.tablaPortada .negrita{
			padding-bottom:15px;
			width:35%;
		}

		.cajaPie{
			width:100%;
		}

		.cajaPie td{
			width:50%;
			padding-top:0px;
		}

		.aliDer{
			text-align:right;
		}

		.tablaDatos{
			border-collapse:collapse;
			width:100%;
			border:1px solid #EEE;
		}

		.tablaDatos th{
			background-color:#060685;
			color:#FFF;
			text-align:center;
			padding:2px;
		}

		.tablaDatos td{
			color:#444;
			vertical-align:top;
			border:1px solid #EEE;
			padding:3px;
		}

		.tablaConstantes td{
			width:10%;
		}

		.tablaDatos .conBorde{
			border:1px solid #AAA;
		}

		.tablaDatos .col1-2{
			width:35%;
			font-weight:bold;
		}

		.tablaDatos .col2-2{
			width:65%;
		}

		.tablaDatos .col2-2-1{
			width:45%;
		}

		.tablaDatos .col2-2-2{
			width:20%;
		}

		.col1-8, .col3-8, .col5-8, .col7-8{
			width:15%;
			vertical-align:middle;
			font-weight:bold;
		}

		.col2-8, .col4-8, .col6-8, .col8-8{
			width:10%;
			vertical-align:middle;
		}

		.col1-8-1{
			width:25%;
		}

		.col2-8-1{
			width:75%;
		}

		.col1-1{
			width:100%;
		}

		.textoPeque{
			font-size:10px;
			text-align:justify;
		}

		.tablaFirma td{
			width:33%;
		}

		.celdaMedico{
			vertical-align:middle;
			text-align:center;
			font-size:11px;
		}

		.celdaFirma{
			text-align:center;
		}

		.celdaFirma img{
			width:70%;
		}

		.celdaGraficoAudiometria{
			text-align:center;
			width:50%;
		}
		.celdaGraficoAudiometria img{
			width:80%;
		}

		.leyendaEspirometria{
			font-weight:normal;
			font-size:9px;
			vertical-align:top;
			text-align:left;
		}

		.col1-6,
		.col2-6,
		.col3-6,
		.col4-6,
		.col5-6,
		.col6-6{
			width:16.5%
		}

		.col1-6,
		.col3-6,
		.col5-6{
			font-weight:bold;
		}

		.estudioVision{
			width:100%;
			border-collapse:collapse;
		}

		.estudioVision td, .estudioVision th{
			width:20%;
			border:1px solid #EEE;
			color:555;
			padding:2px;
			font-size:12px;
		}

		.estudioVision th{
			text-align:center;
			font-weight:normal;
		}

		.estudioVision .cabeceraVision{
			text-align:center;
			font-size:13px;
			vertical-align:top;
		}

		.celdaBlanco{
			border:none;
		}

		.estudioVision .celdaBlancoConBordes{
			border:none;
			border-bottom:1px solid #EEE;
			border-righ:1px solid #EEE;
		}

		.estudioVision .visionCercana{
			background:#FEEBED;
			text-align:center;
		}

		.estudioVision .visionLejana{
			background:#EAFED9;
			text-align:center;
		}

		.a15{
			width:15%;
		}

		.a20{
			width:20%;
		}

		.a21{
			width:21%;
		}

		.a30{
			width:30%;
		}

		.a50{
			width:50%;
		}

		.a70{
			width:70%;
		}

		.a79{
			width:79%;
		}

		.a80{
			width:80%;
		}

		.a100{
			width:100%;
		}

		.left{
			text-align:left;
		}

		.centro{
			text-align:center;
		}

		.tablaViaAerea td,
		.tablaViaAerea th{
			text-align:center;
			width:11.4%;
		}

		.tablaViaAerea .titulo{
			text-align:left;
			width:20%;
		}

		.tablaViaAerea th{
			background-color:#CCE9F7;
			color:#000;
		}

		.listaVisual{
			list-style-type:none;
		}

		.sabias{
			background-color:#CCE9F7;
			color:#000;
		}

		.recomendaciones{
			display:block;
			text-align:justify;
			width:100%;
			padding-right:20px;
		}
	-->
	</style>
	<page footer='page'>

		".obtieneCabeceraPdfReconocimiento($datos)."
		
		<div class='ventanaCarta'>
			A/A D./Dª.: <b>".$datos['empleado']."</b><br />
			".$datos['empresa']."<br/>
			".$direccion."<br/>
			".$localidad."<br/>
			".$cp.". ".$provincia."
		</div>

		<div class='confidencial'>
			CONFIDENCIAL
		</div>

		<div class='cajaTablaPortada'>
			<h4>INFORME DE RECONOCIMIENTO MÉDICO</h4>
			<table class='tablaPortada'>
				<tr>
					<td class='negrita'>Nombre trabajador:</td>
					<td>".$datos['empleado']."</td>
				</tr>
				<tr>
					<td class='negrita'>DNI:</td>
					<td>".$datos['dni']."</td>
				</tr>
				<tr>
					<td class='negrita'>Tipo reconocimiento:</td>
					<td>".$tipoReconocimiento."</td>
				</tr>
				<tr>
					<td class='negrita'>Empresa:</td>
					<td>".$datos['empresa']."</td>
				</tr>
				<tr>
					<td class='negrita'>Fecha:</td>
					<td>".$fecha."</td>
				</tr>
				<tr>
					<td class='negrita'>Nº reconocimiento:</td>
					<td>".str_pad($datos['codigo'],6,'0',STR_PAD_LEFT)."</td>
				</tr>
			</table>
		</div>

		".obtienePiePdfReconocimiento($datos)."
	</page>

	<page footer='page'>

		".obtieneCabeceraPdfReconocimiento($datos)."

		<table class='tablaDatos'>
			<tr>
				<th colspan='3'>HISTORIA LABORAL</th>
			</tr>
			<tr>
				<td class='col1-2'>Empresa:</td>
				<td colspan='2' class='col2-2'>".$datos['empresa']."</td>
			</tr>
			<tr>
				<td class='col1-2'>Actividad de la empresa:</td>
				<td colspan='2' class='col2-2'>".$datos['actividad']."</td>
			</tr>
			<tr>
				<td class='col1-2'>Trabajo actual:</td>
				<td colspan='2' class='col2-2'>".$puesto."</td>
			</tr>
			<tr>
				<td rowspan='".$rowspan."' class='col1-2'>Trabajos anteriores:</td>
				<td class='col2-2-1'>".$datos['puestoAnterior']."</td>
				<td class='col2-2-2 centro'>".$datos['tiempoPuestoAnterior']." meses</td>
			</tr>
			".$puestosAnteriores."
			<tr>
				<td class='col1-2'>Riesgos laborales:</td>
				<td colspan='2' class='col2-2'>".nl2br($datos['riesgoPuesto'])."</td>
			</tr>
			<tr>
				<td class='col1-2'>Riesgos extra laborales:</td>
				<td colspan='2' class='col2-2'>".nl2br($riesgosExtralaborales)."</td>
			</tr>
			<tr>
				<td class='col1-2'>Accidente de trabajo:</td>
				<td colspan='2' class='col2-2'>".$accidente."</td>
			</tr>
			<tr>
				<td class='col1-2'>Minusvalía / incapacidad reconocida:</td>
				<td colspan='2' class='col2-2'>".nl2br($minusvalia)."</td>
			</tr>
			<tr>
				<td class='col1-2'>Trabajador sensible:</td>
				<td colspan='2' class='col2-2'>".ucfirst(strtolower($datos['trabajadorSensible']))."</td>
			</tr>
			<tr>
				<td class='col1-2'>Protocolos aplicados:</td>
				<td colspan='2' class='col2-2'>".$datos['protocoloClasificacion']."</td>
			</tr>
			<tr>
				<td class='col1-2'>EPIs que utiliza:</td>
				<td colspan='2' class='col2-2'>".$epis."</td>
			</tr>
			<tr>
				<td class='col1-2'>Alteración que le impida usar algún EPIs:</td>
				<td colspan='2' class='col2-2'>".nl2br($alteracion)."</td>
			</tr>
		</table>
		<br/>
		<table class='tablaDatos'>
			<tr>
				<th class='a100'>HISTORIA CLÍNICA</th>
			</tr>
		</table>
		<br/>
		<table class='tablaDatos'>
			<tr>
				<th class='left' colspan='2'>ANTECEDENTES</th>
			</tr>
			<tr>
				<td class='col1-2'>PERSONALES:</td>
				<td class='col2-2'>".nl2br($antecedentesPersonales)."</td>
			</tr>
			<tr>
				<td class='col1-2'>FAMILIARES:</td>
				<td class='col2-2'>".nl2br($familiares)."</td>
			</tr>
			<tr>
				<td class='col1-2'>ALERGIAS:</td>
				<td class='col2-2'>".nl2br($alergias)."</td>
			</tr>
			<tr>
				<td class='col1-2'>VACUNACIONES:</td>
				<td class='col2-2'>".$vacunaciones."</td>
			</tr>
			<tr>
				<td class='col1-2'>FRACTURAS:</td>
				<td class='col2-2'>".nl2br($limitaciones)."</td>
			</tr>
			<tr>
				<td class='col1-2'>BAJAS PROLONGADAS:</td>
				<td class='col2-2'>".nl2br($bajasProlongadas)."</td>
			</tr>
		</table>
		".obtienePiePdfReconocimiento($datos)."
	</page>

	<page footer='page'>

		".obtieneCabeceraPdfReconocimiento($datos)."

		<table class='tablaDatos'>
			<tr>
				<th clase='a100 left' colspan='2'>FACTORES DE RIESGOS CARDIOVASCULARES</th>
			</tr>
			<tr>
				<td class='a50'>
					Hiperglucemia: ".$hiperglucemia."
				</td>
				<td class='a50'>
					Sobrepeso: ".$sobrepeso."
				</td>
			</tr>
			<tr>
				<td class='a50'>
					Tabaco: ".$tabaco."
				</td>
				<td class='a50'>
					Sedentarismo: ".$sedentarismo."
				</td>
			</tr>
		</table>
		<br/>
		<table class='tablaDatos'>
			<tr>
				<th class='left' colspan='2'>HABITOS PERSONALES</th>
			</tr>
			<tr>
				<td rowspan='3' class='col1-2'>FISIOLOGICOS:</td>
				<td class='col2-2'>ALIMENTACIÓN: ".$datos['alimentacion'].$datos['alimentacionTexto']."</td>
			</tr>
			<tr>
				<td class='col2-2'>DEPORTE: ".$datos['ejercicio']."</td>
			</tr>
			<tr>
				<td class='col2-2'>SUEÑO: ".$calidadSuenio."</td>
			</tr>
			<tr>
				<td rowspan='".$rowBebidas."' class='col1-2'>TÓXICOS:</td>
				<td class='col2-2'>".$fumador.".</td>
			</tr>
			<tr>
				<td class='col2-2'>".$alcohol."</td>
			</tr>
			".$bebidas.".
			<tr>
				<td class='col2-2'>Drogas: No refiere.</td>
			</tr>
			<tr>
				<td class='col1-2'>MEDICACIÓN HABITUAL:</td>
				<td class='col2-2'>".$medicacionHabitual."</td>
			</tr>
		</table>
		<br/>
		<table class='tablaDatos'>
			<tr>
				<th class='a100'>HISTORIA ACTUAL</th>
			</tr>
			<tr>
				<td class='a100'>".nl2br($datos['historiaActual'])."</td>
			</tr>
		</table>
		<br/>
		<table class='tablaDatos tablaConstantes'>
			<tr>
				<th colspan='10'>CONSTANTES Y VALORES ANTROPOMÉTRICOS</th>
			</tr>
			<tr>
				<td><strong>EDAD</strong>:</td>
				<td>".$edad." años</td>
				<td><strong>SEXO</strong>:</td>
				<td>".$sexo[$datos['sexo']]."</td>
			</tr>
			<tr>
				<td><strong>TALLA</strong>:</td>
				<td>".$datos['talla']." cms</td>
				<td><strong>PESO</strong>:</td>
				<td>".$datos['peso']." kgs</td>
			</tr>
			<tr>
				<td><strong>I.M.C.</strong>:</td>
				<td>".$datos['imc']."</td>
				<td><strong>SATURACIÓN O<sub>2</sub></strong>:</td>
				<td>".$datos['saturacionO2']."</td>
			</tr>
			<tr>
				<td><strong>T.A. SISTÓLICA</strong>:</td>
				<td>".$datos['sistolica']."</td>
				<td><strong>T.A. DIASTÓLICA</strong>:</td>
				<td>".$datos['diastolica']."</td>
			</tr>
			<tr>
				<td><strong>F. CARDIACA</strong>:</td>
				<td>".$datos['frecuenciaCardiaca']." lpm</td>
				<td><strong>F.C. MÁXIMA</strong>:</td>
				<td>".$fmaxima." lpm</td>
			</tr>
			<tr>
				<td class='a21 celdaBlanco'></td>
				<td colspan='3' class='a79 celdaBlanco'></td>
			</tr>

		</table>

		".obtienePiePdfReconocimiento($datos)."
	</page>

	<page footer='page'>

		".obtieneCabeceraPdfReconocimiento($datos)."
		
		<table class='tablaDatos'>
			<tr>
				<th colspan='2'>EXPLORACIÓN FÍSICA</th>
			</tr>
			<tr>
				<td class='col1-2'>INSPECCIÓN GENERAL:</td>
				<td class='col2-2'>".nl2br(recogeDesplegable($datos['inspeccionGeneral']))."</td>
			</tr>
			<tr>
				<td class='col1-2'>PIEL Y MUCOSAS:</td>
				<td class='col2-2'>".nl2br(recogeDesplegable($datos['pielMucosas']))."</td>
			</tr>
			<tr>
				<td class='col1-2'>CAVIDAD OROFARINGEA:</td>
				<td class='col2-2'>".nl2br(recogeDesplegable($datos['orl']))."</td>
			</tr>
			<tr>
				<td class='col1-2'>OFTALMOLOGÍA:</td>
				<td class='col2-2'>".nl2br(recogeDesplegable($datos['oftalmologia']))."</td>
			</tr>
			<tr>
				<td class='col1-2'>CUELLO:</td>
				<td class='col2-2'>".nl2br(recogeDesplegable($datos['cuello']))."</td>
			</tr>
			<tr>
				<td class='col1-2'>SISTEMA CARDIOVASCULAR:</td>
				<td class='col2-2'>AUSCULTACION CARDIACA: ".nl2br(recogeDesplegable($datos['auscultacionCardiaca']))."</td>
			</tr>
			<tr>
				<td class='col1-2'>APARATO RESPIRATORIO:</td>
				<td class='col2-2'>AUSCULTACION PULMONAR: ".nl2br(recogeDesplegable($datos['auscultacionPulmonar']))."</td>
			</tr>
			<tr>
				<td class='col1-2'>APARATO DIGESTIVO:</td>
				<td class='col2-2'>".nl2br(recogeDesplegable($datos['aparatoDigestivo']))."</td>
			</tr>
			<tr>
				<td class='col1-2'>EXPLORACIÓN NEUROLÓGICA:</td>
				<td class='col2-2'>".nl2br(recogeDesplegable($datos['exploracionNeurologica']))."</td>
			</tr>
			<tr>
				<td class='col1-2'>APARATO LOCOMOTOR:</td>
				<td class='col2-2'>".nl2br(recogeDesplegable($datos['aparatoLocomotor']))."</td>
			</tr>
			<tr>
				<td class='col1-2'>SISTEMA UROGENITAL:</td>
				<td class='col2-2'>".nl2br(recogeDesplegable($datos['sistemaUrogenital']))."</td>
			</tr>
			<tr>
				<td class='col1-2'>EXTREMIDADES:</td>
				<td class='col2-2'>".nl2br(recogeDesplegable($datos['extremidades']))."</td>
			</tr>
		</table>
		<br/>";

	if($datos['checkDinamometria']=='SI'){
		$contenido.="
		<table class='tablaDatos'>
			<tr>
				<th colspan='6'>DINAMOMETRÍA</th>
			</tr>
			<tr>
				<td class='a15 centro'>MSD:</td>
				<td class='a15 centro'>".$datos['manoDerecha']."</td>
				<td class='a15 centro'>MSI:</td>
				<td class='a15 centro'>".$datos['manoIzquierda']."</td>
				<td class='a20 centro'>Mano dominante:</td>
				<td class='a20 centro'>".$datos['manoDominante']."</td>
			</tr>
			<tr>
				<td colspan='2' class='a30 centro'>VALORACIÓN:</td>
				<td colspan='4' class='a70'>".$datos['resultadoDinamometria']."</td>
			</tr>
		</table>
		<br/>";
	}

	$contenido.="
		<table class='tablaDatos'>
			<tr>
				<th class='a100' colspan='2'>RESUMEN DE LAS PRUEBAS COMPLEMENTARIAS</th>
			</tr>
			
			".$pruebas."
			
		</table>

		".obtienePiePdfReconocimiento($datos)."
	</page>";


	//Parte de pruebas complementarias
	$contenido.=compruebaEspirometriaAudiometriaElectrocardiogramaPdfReconocimiento($datos);
	$contenido.=compruebaAgudezaVisualPdfReconocimiento($datos);
	$contenido.=compruebaAnaliticaPdfReconocimiento($datos);
	$contenido.=finalRM($datos,$puesto);
	

	return $contenido;
}

function obtieneAntecedentesPersonalesPDF($datos){
	$res='';

	$res.=obtieneValorCampoAntecedentesPersonales($datos,'antecedentesPersonales1','Riesgo cardiovascular');
	$res.=obtieneValorCampoAntecedentesPersonales($datos,'antecedentesPersonales2','Enf. Cardiorrespiratorios');
	$res.=obtieneValorCampoAntecedentesPersonales($datos,'antecedentesPersonales3','Enf. Digestivo');
	$res.=obtieneValorCampoAntecedentesPersonales($datos,'antecedentesPersonales4','Enf. Genitourinario');
	$res.=obtieneValorCampoAntecedentesPersonales($datos,'antecedentesPersonales5','Enf. Ap. locomotor y neur.');
	$res.=obtieneValorCampoAntecedentesPersonales($datos,'antecedentesPersonales6','Enf. ORL');
	$res.=obtieneValorCampoAntecedentesPersonales($datos,'antecedentesPersonales7','Enf. Oftalmológico');
	$res.=obtieneValorCampoAntecedentesPersonales($datos,'antecedentesPersonales8','Cirugía');
	$res.=obtieneValorCampoAntecedentesPersonales($datos,'antecedentesPersonales9','Otras');

	return $res;
}

function obtieneValorCampoAntecedentesPersonales($datos,$indiceCampo,$titulo){
	$res='<strong>'.$titulo.'</strong>: No refiere.';

	if($datos[$indiceCampo]!=''){
		$res='<strong>'.$titulo.'</strong>: '.$datos[$indiceCampo];
	}

	$res.='<br />';

	return $res;
}

function obtieneRiesgoHiperglucemia($codigoReconocimiento){
	$res='No';

	$datos=consultaBD("SELECT codigo FROM enfermedades_antecedentes_reconocimientos_medicos WHERE codigoReconocimiento='$codigoReconocimiento' AND codigoEnfermedad='98' AND checkAntecedentesPersonales='SI';",true,true);
	if($datos){
		$res='Si';
	}

	return $res;
}


function obtienePruebasPDF($datos){
	$pruebas='';

	//El siguiente array tiene la sintaxis: nombreCampo=>array(texto campo,nombreCampoValoracion)
	$camposPruebas=array(
					'checkAnalitica'			=>	array('ANALÍTICA','analitica'),
					'checkAudiometria'			=>	array('AUDIOMETRÍA','observacionesAudiometria'),
					'checkElectrocardiograma'	=>	array('ELECTROCARDIOGRAMA','electrocardiograma'),
					'checkEspirometria'			=>	array('ESPIROMETRIA','observacionesEspirometria'),
					'checkDinamometria'			=>	array('DINAMOMETRIA','resultadoDinamometria'),
					'checkRadiologia'			=>	array('PRUEBAS DE IMAGEN','radiologia')
				);

	foreach($camposPruebas as $nombreCampo => $datosPrueba) {
		
		if($datos[$nombreCampo]=='SI'){
			$resultadoPrueba=$datos[$datosPrueba[1]];

			$pruebas.='<tr><td><b>'.$datosPrueba[0].'</b>:</td><td>'.$resultadoPrueba.'</td></tr>';
		}

	}
	
	if($datos['checkAgudezaVisual']=='SI'){
		$pruebas.='<tr><td><b>ESTUDIO VISIÓN</b>:</td><td>'.$datos['observacionesEstudioVision'].'</td></tr>';
	}


	if($datos['checkOtrasPruebasFuncionales']=='SI'){
		$otrasPruebas=consultaBD('SELECT desplegables.texto, resultado FROM reconocimientos_medicos_otras_pruebas INNER JOIN desplegables ON reconocimientos_medicos_otras_pruebas.texto=desplegables.codigo WHERE codigoRM='.$datos['codigo'],true);
		while($item=mysql_fetch_assoc($otrasPruebas)){
			
			$pruebas.='<tr><td><b>'.$item['texto'].'</b>:</td><td>'.nl2br($item['resultado']).'</td></tr>';

		}
	}

	return $pruebas;
}

function obtieneValorCheckEstudioVisual($datos,$indiceCampo,$texto){
	$res='';

	if($datos[$indiceCampo]=='SI'){
		$res=$texto.'<br />';
	}

	return $res;
}

function obtieneAlergiasPDF($datos){
	
	$res=obtieneTextoAlergiaPDF($datos,'Medioambientales','checkAlergias1','textoAlergias1');
	$res.=obtieneTextoAlergiaPDF($datos,'Alimentarias','checkAlergias2','textoAlergias2');
	$res.=obtieneTextoAlergiaPDF($datos,'Medicamentosas','checkAlergias3','textoAlergias3');
	$res.=obtieneTextoAlergiaPDF($datos,'Laborales','checkAlergias4','textoAlergias4');

	return $res;
}

function obtieneTextoAlergiaPDF($datos,$titulo,$indiceCheck,$indiceTexto){
	$res='';

	if($datos[$indiceCheck]=='SI'){
		$res=$titulo.': '.$datos[$indiceTexto];
	}
	else{
		$res=$titulo.': No conocidas.';
	}

	$res.='<br />';

	return $res;
}


function recogeDesplegable($codigo){
	$res=datosRegistro('desplegables',$codigo);
	if(!$res){
		$res='';
	} else {
		$res=$res['texto'];
	}
	return $res;
}

function finalRM($datos,$puesto){
	$res='';
	$protocolos=$datos['protocoloClasificacion'];
		$res="
		<page footer='page'>

			".obtieneCabeceraPdfReconocimiento($datos)."

			<table class='tablaDatos'>
				<tr>
					<th class='a100'>CALIFICACIÓN</th>
				</tr>
				<tr>
					<td class='a100'>Tras la realización de los protocolos médicos específicos del Ministerio de Sanidad y Consumo de: <b>".$protocolos."</b> y teniendo en cuenta otros criterio médicos valorados: se le califica y dictamina a criterio medico: <b>".$datos['apto']."</b> para el puesto de <b>".$puesto."</b>.</td>
				</tr>
				<tr>
					<td style='text-align:center;font-weight:bold'>
						<img style='width:250px;border:none' src='../img/firmaRM-fake.png' /><br/>
						Dr. Peñas de Bustillo<br/>
 						Departamento de Vigilancia de la Salud<br/>
					</td>
				</tr>
				<tr>
					<td style='font-size:9px;'>
						En cumplimiento de la Ley 15/1999, de 13/12/99, de Protección de Datos de Carácter Personal, se informa que los datos de carácter personal que se solicitan en nuestros formularios se incluirán en una base de datos de carácter personal. Dicha base de datos se encuentra inscrita en la agencia de Protección de Datos conforme a lo dispuesto en la legislación vigente. Dispone de los derechos de información, acceso, rectificación, oposición y cancelación, en virtud de lo dispuesto en la LORTAD (Ley Orgánica de Protección de Datos de Carácter Personal), lo cual puede realizarse a través del mail <b>lopd@anescoprl.es</b> o por correo postal a: ANESCO S.L. Murillo, 1 - Planta. 2ª Sevilla
					</td>
				</tr>
			</table>
			<br/>
			<table class='tablaDatos'>
				<tr>
					<th class='a100'>OBSERVACIONES Y RECOMENDACIONES GENERALES</th>
				</tr>
			</table>

			<p class='recomendaciones'>".obtenerRecomendaciones(nl2br($datos['recomendaciones']))."</p>

			".obtienePiePdfReconocimiento($datos)."
		</page>";

	return $res;
}

function obtieneTA($datos){
	$si=$datos['sistolica'];
	$di=$datos['diastolica'];
	$res='que los valores no coinciden con los criterios del JNVC';
	if($si>139 && $di>89){
		if($si>210 && $di<120){
			$res='HIPERTENSIÓN. Estadio IV (muy severa) según la JNCV';
		} else if(($si>=180 && $si<=209) && ($di>=110 && $di<=119)){
			$res='HIPERTENSIÓN. Estadio III (severa) según la JNCV';
		} else if(($si>=160 && $si<=179) && ($di>=100 && $di<=109)){
			$res='HIPERTENSIÓN. Estadio II (moderada) según la JNCV';
		} else if(($si>=140 && $si<=159) && ($di>=90 && $di<=99)){
			$res='HIPERTENSIÓN. Estadio I (ligera) según la JNCV';
		}
	} else if($si>139 && $di<90){
		$res='HTA sistólica aislada según la JNCV';
	} else {
		if($si<130 && $di<85){
			$res='NORMOTENSIÓN. Normal según la JNCV';
		} else if(($si>=130 && $si<=139) && ($di>=85 && $di<=89)){
			$res='NORMOTENSIÓN. Normal alta según la JNCV';
		}
	}
	return $res;
}

function obtenerRecomendaciones($texto){
	$res='<ul class="listaVisual">';
	$texto=explode('<br />', $texto);
	foreach ($texto as $key => $value) {
		$value=trim($value);

		if($value!=''){
			$res.="<li><img src='../img/checkPP.png' /> ".$value."</li>";
		}
		else{
			$res.="<li></li>";
		}
	}
	$res.='</ul>';
	return $res;
}

function compruebaAgudezaVisualPdfReconocimiento($datos){
	$res='';
	if($datos['checkAgudezaVisual']=='SI' || $datos['checkRadiologia']=='SI'){
		$res="
		<page footer='page'>
			".obtieneCabeceraPdfReconocimiento($datos);
		if($datos['checkRadiologia']=='SI'){
			$res.="
			<table class='tablaDatos'>
				<tr>
					<th colspan='2'>PRUEBAS DE IMAGEN</th>
				</tr>
				<tr>
					<td class='col1-2'><b>Resultado radiología:</b></td>
					<td class='col2-2'>".nl2br($datos['radiologia'])."</td>
				</tr>
			</table>
			<br/>";
		}

		if($datos['checkAgudezaVisual']=='SI'){
			$tipo=obtieneTipoGafasLentillasPDF($datos);

			for($i=1;$i<13;$i++){
				$datos['estudioVision'.$i]=$datos['estudioVision'.$i]==''?'-':$datos['estudioVision'.$i].'/12';
			}


			//Parte de corrección visual
			$correccion=compruebaCheckTipoGafasLentillasPDF($datos,'checkObservacionesVision0','Miopía');
			$correccion.=compruebaCheckTipoGafasLentillasPDF($datos,'checkObservacionesVision1','Hipermetropía');
			$correccion.=compruebaCheckTipoGafasLentillasPDF($datos,'checkObservacionesVision2','Astigmatismo');
			$correccion.=compruebaCheckTipoGafasLentillasPDF($datos,'checkObservacionesVision3','Presbicia');
			$correccion.=compruebaCheckTipoGafasLentillasPDF($datos,'checkObservacionesVision4','Miopía+Astigmatismo');
			$correccion.=compruebaCheckTipoGafasLentillasPDF($datos,'checkObservacionesVision5','Hipermetropía+Astigmatismo');
			$correccion.=compruebaCheckTipoGafasLentillasPDF($datos,'checkObservacionesVision6','Cirugía Refractaria');
			$correccion.=compruebaCheckTipoGafasLentillasPDF($datos,'checkObservacionesVision7',$datos['observacionesVision']);

			if($correccion!=''){
				$correccion=quitaUltimaComa($correccion);
			}
			else{
				$correccion='Agudeza visual sin patologías.';
			}

			//Fin parte de corrección visual


			$res.="
			<table class='tablaDatos'>
				<tr>
					<th class='a100' colspan='2'>CONTROL VISIÓN</th>
				</tr>
				<tr>
					<td class='a100' colspan='2'>
						<ul class='listaVisual'>
							<li><img src='../img/checkPP.png' /> El trabajador/a  <b>".$datos['agudezaVisualCorregida']."</b> tiene la agudeza visual (AV) corregida (gafas/ lentillas).</li>";

						if($datos['agudezaVisualCorregida']=='SI'){
							$res.="
							<li><img src='../img/checkPP.png' /> El trabajador /a  <b>".$datos['traeGafasLentillas']."</b> trae las gafas /lentillas.</li>
							<li><img src='../img/checkPP.png' /> Tipo de Gafas: <b>".$tipo."</b></li>
							<li><img src='../img/checkPP.png' /> Año Ultima Graduación: ".$datos['anioUltimaGraduacion']."</li>";
						}

						$res.="
						</ul>
					</td>
				</tr>
				<tr>
					<td><b>Resultado:</b></td>
					<td>".$datos['observacionesEstudioVision']."</td>
				</tr>
			</table>
			<br />
			<table class='estudioVision'>
				<tr>
					<td class='celdaBlanco'></td>
					<th class='cabeceraVision visionLejana' colspan='2'>VISIÓN LEJANA</th>
					<th class='cabeceraVision visionCercana' colspan='2'>VISIÓN CERCANA</th>
				</tr>
				<tr>
					<td class='celdaBlancoConBordes'></td>
					<th class='visionLejana'>CON<br/>CORRECCIÓN</th>
					<th class='visionLejana'>SIN<br/>CORRECCIÓN</th>
					<th class='visionCercana'>CON<br/>CORRECCIÓN</th>
					<th class='visionCercana'>SIN<br/>CORRECCIÓN</th>
				</tr>
				<tr>
					<td>OJO DERECHO</td>
					<td class='visionLejana'>".$datos['estudioVision1']."</td>
					<td class='visionLejana'>".$datos['estudioVision2']."</td>
					<td class='visionCercana'>".$datos['estudioVision3']."</td>
					<td class='visionCercana'>".$datos['estudioVision4']."</td>
				</tr>
				<tr>
					<td>OJO IZQUIERDO</td>
					<td class='visionLejana'>".$datos['estudioVision5']."</td>
					<td class='visionLejana'>".$datos['estudioVision6']."</td>
					<td class='visionCercana'>".$datos['estudioVision7']."</td>
					<td class='visionCercana'>".$datos['estudioVision8']."</td>
				</tr>
				<tr>
					<td><b>BINOCULAR</b></td>
					<td class='visionLejana'><b>".$datos['estudioVision9']."</b></td>
					<td class='visionLejana'><b>".$datos['estudioVision10']."</b></td>
					<td class='visionCercana'><b>".$datos['estudioVision11']."</b></td>
					<td class='visionCercana'><b>".$datos['estudioVision12']."</b></td>
				</tr>
				<tr>
					<td colspan='5' style='height:10px;'> </td>
				</tr>
				<tr>
					<td> <b>Visión cromática:</b></td>
					<td colspan='4'>".$datos['percepcionColores']."</td>
				</tr>
				<tr>
					<td> <b>Diagnóstico:</b></td>
					<td colspan='4'>".$correccion.".</td>
				</tr>
			</table>";
			}
		/*$res.=	obtienePiePdfReconocimiento($datos)."
		</page>";*/ //Ahora se cerrará en analítica
	}
	else{
		$res="<page footer='page'>";
	}

	return $res;
}


function obtieneTipoGafasLentillasPDF($datos){
	$res=compruebaCheckTipoGafasLentillasPDF($datos,'checkTipoGafasLentillas0','Progresivas');
	$res.=compruebaCheckTipoGafasLentillasPDF($datos,'checkTipoGafasLentillas1','bifocales');
	$res.=compruebaCheckTipoGafasLentillasPDF($datos,'checkTipoGafasLentillas2','cerca');
	$res.=compruebaCheckTipoGafasLentillasPDF($datos,'checkTipoGafasLentillas3','lejos');
	$res.=compruebaCheckTipoGafasLentillasPDF($datos,'checkTipoGafasLentillas4',$datos['tipoGafasLentillasTexto']);


	if($res!=''){
		$res=quitaUltimaComa($res);
	}

	return $res;
}

function compruebaCheckTipoGafasLentillasPDF($datos,$indice,$texto){
	$res='';

	if($datos[$indice]=='SI'){
		$res=$texto.', ';
	}

	return $res;
}

function obtenerProtocoloresPDF($datos){
	$res='';
	$textos=array('General','Conductores','Cargas mov. repetidos','Posturas forzadas','Neuropatia por presión','Ruido','Altura','Dermatosis','Asma laboral','Asma laboral: Soldador','Silicosis y otras neumoconiosis');
	for($i=0;$i<11;$i++){
		if($datos['checkClasificacion'.$i]=='SI'){
			if($res!=''){
				$res.=', ';
			}
			$res.=$textos[$i];
		}
	}
	return $res;
}

function obtenerEpisPDF($datos){
	$res='';
	
	$listado=consultaBD('SELECT nombre FROM epis_en_reconocimiento_medico INNER JOIN epis ON epis_en_reconocimiento_medico.codigoEpi=epis.codigo WHERE codigoReconocimientoMedico='.$datos['codigo'],true);
	while($item=mysql_fetch_assoc($listado)){
		if($res!=''){
			$res.=', ';
		}
		$res.=$item['nombre'];
	}

	if($res==''){
		$res='Los recomendados por la E.R.';
	}

	return $res;
}

function obtenerVacunacionesPDF($datos){
	$res='';
	$textos=array('Infancia','Td','VHA','VHB');
	for($i=0;$i<4;$i++){
		if($datos['checkVacunaciones'.$i]!='NO'){
			if($res!=''){
				$res.=', ';
			}
			$res.=$textos[$i];
		}
	}
	if($datos['otrasVacunaciones']!=''){
		if($res!=''){
			$res.=', ';
		}
		$res.=$datos['otrasVacunaciones'];
	}
	return $res;
}

function generaGraficoAudiometriaOido($datos,$oido){
	$escala=29;//Marca el incremento que hay que meterle a los valores de la audiometría para incrustarlo en el eje Y del gráfico
	$inicioY=154;//Línea del 0 en el Y del gráfico
	$imagen=imagecreatefrompng('../documentos/reconocimientos/plantillaGrafico.png');//Carga de la plantilla

	if($oido=='DERECHO'){
		$punto1=$inicioY+($escala*($datos['estudioAudiometria1']/10));
		$punto2=$inicioY+($escala*($datos['estudioAudiometria2']/10));
		$punto3=$inicioY+($escala*($datos['estudioAudiometria3']/10));
		$punto4=$inicioY+($escala*($datos['estudioAudiometria4']/10));
		$punto5=$inicioY+($escala*($datos['estudioAudiometria5']/10));
		$punto6=$inicioY+($escala*($datos['estudioAudiometria6']/10));
		$punto7=$inicioY+($escala*($datos['estudioAudiometria7']/10));

		$color=imagecolorallocate($imagen,255,0,0);//Establecimiento del color de la línea			
		$nombreFichero='graficoOidoDerecho.png';
	}
	else{
		$punto1=$inicioY+($escala*($datos['estudioAudiometria8']/10));
		$punto2=$inicioY+($escala*($datos['estudioAudiometria9']/10));
		$punto3=$inicioY+($escala*($datos['estudioAudiometria10']/10));
		$punto4=$inicioY+($escala*($datos['estudioAudiometria11']/10));
		$punto5=$inicioY+($escala*($datos['estudioAudiometria12']/10));
		$punto6=$inicioY+($escala*($datos['estudioAudiometria13']/10));
		$punto7=$inicioY+($escala*($datos['estudioAudiometria14']/10));

		$color=imagecolorallocate($imagen,66,141,240);	
		$nombreFichero='graficoOidoIzquierdo.png';
	}

	imagesetthickness($imagen,3);//Establecimiento del grosor de la línea (por defecto es muy fina)
	

	imageline($imagen,'180',$punto1,'238',$punto2,$color);//Línea
	imagefilledrectangle($imagen,'176',$punto1-4,'184',$punto1+4,$color);//Cuadrado en el punto

	imageline($imagen,'238',$punto2,'296',$punto3,$color);
	imagefilledrectangle($imagen,'234',$punto2-4,'242',$punto2+4,$color);

	imageline($imagen,'296',$punto3,'354',$punto4,$color);
	imagefilledrectangle($imagen,'292',$punto3-4,'300',$punto3+4,$color);

	imageline($imagen,'354',$punto4,'412',$punto5,$color);
	imagefilledrectangle($imagen,'350',$punto4-4,'358',$punto4+4,$color);

	imageline($imagen,'412',$punto5,'470',$punto6,$color);
	imagefilledrectangle($imagen,'408',$punto5-4,'416',$punto5+4,$color);

	imageline($imagen,'470',$punto6,'528',$punto7,$color);
	imagefilledrectangle($imagen,'466',$punto6-4,'474',$punto6+4,$color);

	imagefilledrectangle($imagen,'524',$punto7-4,'532',$punto7+4,$color);

	imagepng($imagen,'../documentos/reconocimientos/'.$nombreFichero);
}



function compruebaAnaliticaPdfReconocimiento($datos){
	$res='</page>';

	if($datos['checkAnalitica']=='SI'){

		$res="
			
			<br /><br />		
			<table class='tablaDatos'>
				<tr>
					<th colspan='2'>ANALÍTICA</th>
				</tr>
				<tr>
					<td class='col1-2'>Hemograma:</td>
					<td class='col2-2'>".nl2br($datos['hemograma'])."</td>
				</tr>
				<tr>
					<td class='col1-2'>Bioquímica:</td>
					<td class='col2-2'>".nl2br($datos['bioquimica'])."</td>
				</tr>
				<tr>
					<td class='col1-2'>Orina:</td>
					<td class='col2-2'>".nl2br($datos['orina'])."</td>
				</tr>
				<tr>
					<td class='col1-2'>Resultado:</td>
					<td class='col2-2'>".$datos['analitica']."</td>
				</tr>
			</table>

			".obtienePiePdfReconocimiento($datos)."
		</page>";

	}

	return $res;
}


function compruebaEspirometriaAudiometriaElectrocardiogramaPdfReconocimiento($datos){
	$res='';

	if($datos['checkEspirometria']=='SI' || $datos['checkAudiometria']=='SI' || $datos['checkElectrocardiograma']=='SI'){
		$res="
		<page footer='page'>

			".obtieneCabeceraPdfReconocimiento($datos);
			

		if($datos['checkEspirometria']=='SI'){
			$edad=calculaEdadEmpleado($datos['fechaNacimiento']);
			$datos['itiff']=$datos['itiff']==''?100:$datos['itiff'];
			$res.="
			<table class='tablaDatos'>
				<tr>
					<th colspan='8'>ESPIROMETRÍA</th>
				</tr>
				<tr>
					<td class='col1-8'>F.V.C:</td>
					<td class='col2-8'>".$datos['fvc']." %</td>
					<td class='col3-8'>F.E.V.1:</td>
					<td class='col4-8'>".$datos['fev1']." %</td>
					<td class='col5-8'>I.TIFF.:</td>
					<td class='col6-8'>".$datos['itiff']." %</td>
				</tr>
				<tr>
					<td class='col1-8'>FEF25-75%:</td>
					<td class='col2-8'>".$datos['fef25']." %</td>
					<td class='col3-8'>PEF:</td>
					<td class='col4-8'>".$datos['pef']."</td>
					<td class='col7-8'>Fecha:</td>
					<td class='col8-8'>".formateaFechaWeb($datos['fecha'])."</td>
				</tr>
				<tr>
					<td colspan='2' class='col1-8-1'>FCV<br/>FEV 1<br/>I. TIFFENAU<br/>FEF25-75%<br/>PEF</td>
					<td colspan='6' class='col2-8-1'>Forced Vital Capacity<br/>
								Forced Espiratory Volume in 1 st second<br/>
								FEV1/FVC Tiffeneau modificado.<br/>
								Flujo espiratorio forzado entre el 25% y el75 % de la espiración.<br/>
								Peak expiratory flor. Flujo espiratorio máximo</td>
				</tr>
				<tr>
					<td colspan='2'><b>Resultado:</b></td>
					<td colspan='6'>".$datos['valoracionEspirometrica'].". ".nl2br($datos['observacionesEspirometria'])."</td>
				</tr>
				<tr>
					<td colspan='2' class='col1-8-1 sabias'><b>Sabías que...?</b></td>
					<td colspan='6' class='col2-8-1 sabias'>La espirometría es una prueba que permite conocer el estado de los pulmones de una persona midiendo el aire que es capaz de inspirar y espirar.</td>
				</tr>
			</table>
			<br />";
		}

		if($datos['checkElectrocardiograma']=='SI'){
			$res.="
			<table class='tablaDatos'>
				<tr>
					<th colspan='6'>ELECTROCARDIOGRAMA</th>
				</tr>
				<tr>
					<td class='col1-6'>T.A. Sistólica</td>
					<td class='col2-6 centro'>".$datos['sistolica']."</td>
					<td class='col3-6'>T.A. Diastólica</td>
					<td class='col4-6 centro'>".$datos['diastolica']."</td>
					<td class='col5-6'>FC (lat/min.):</td>
					<td class='col6-6 centro'>".$datos['frecuenciaCardiaca']."</td>
				</tr>
				<tr>
					<td><b>Resultado:</b></td><td colspan='5'>".nl2br($datos['electrocardiograma'])."</td>
				</tr>
			</table>";
	
		}

		if($datos['checkAudiometria']=='SI'){
			generaGraficoAudiometriaOido($datos,'IZQUIERDO');
			generaGraficoAudiometriaOido($datos,'DERECHO');

			$res.="
			<br/>
			<table class='tablaDatos'>
				<tr>
					<th colspan='2'>AUDIOMETRÍA</th>
				</tr>
				<tr>
					<td class='col1-2'><b>Resultado:</b></td>
					<td class='col2-2'>".nl2br($datos['observacionesAudiometria'])."</td>
				</tr>
			</table>
			<br/>
			<table class='tablaDatos tablaViaAerea'>
				<tr>
					<th class='titulo'>VIA AEREA</th>
					<th>500</th>
					<th>1000</th>
					<th>2000</th>
					<th>3000</th>
					<th>4000</th>
					<th>6000</th>
					<th>8000</th>
				</tr>
				<tr>
					<td class='titulo'>Oído derecho</td>
					<td>".$datos['estudioAudiometria1']."</td>
					<td>".$datos['estudioAudiometria2']."</td>
					<td>".$datos['estudioAudiometria3']."</td>
					<td>".$datos['estudioAudiometria4']."</td>
					<td>".$datos['estudioAudiometria5']."</td>
					<td>".$datos['estudioAudiometria6']."</td>
					<td>".$datos['estudioAudiometria7']."</td>
				</tr>
				<tr>
					<td class='titulo'>Oído izquierdo</td>
					<td>".$datos['estudioAudiometria8']."</td>
					<td>".$datos['estudioAudiometria9']."</td>
					<td>".$datos['estudioAudiometria10']."</td>
					<td>".$datos['estudioAudiometria11']."</td>
					<td>".$datos['estudioAudiometria12']."</td>
					<td>".$datos['estudioAudiometria13']."</td>
					<td>".$datos['estudioAudiometria14']."</td>
				</tr>
			</table>
			<br/>
			<table class='tablaDatos'>
				<tr>
					<td class='celdaGraficoAudiometria'>
						<span class='negrita'>Valores audiometría oído izquierdo</span><br /><br />
						<img src='../documentos/reconocimientos/graficoOidoIzquierdo.png' />
					</td>
					<td class='celdaGraficoAudiometria'>
						<span class='negrita'>Valores audiometría oído derecho</span><br /><br />
						<img src='../documentos/reconocimientos/graficoOidoDerecho.png' />
					</td>
				</tr>
			</table>
			<br /><br />
			<table class='tablaDatos'>
				<tr>
					<td class='col1-2 conBorde negrita'>CUADRO ELI:</td>
					<td class='col2-2 conBorde'>
						Normal (a-b)<br />
						Trauma acústico inicial ( a-c)<br />
						Trauma acústico avanzado ( a-d / a-d-c) <br />
						Hipoacusia leve (a-e-c / a-e-d / a-e-d-c) <br />
						Hipoacusia moderada (e-d / e-d-c) <br />
						Hipoacusia avanzada (e-f-d / e-f-d-c)
					</td>
				</tr>
			</table>";
		}


		$res.=obtienePiePdfReconocimiento($datos)."
		</page>";
	}

	return $res;
}


function obtieneCabeceraPdfReconocimiento($datos){
	//<h5>INFORME RECONOCIMIENTO MÉDICO DE ".$datos['empleado']."<br />DE FECHA DE ".formateaFechaWeb($datos['fecha'])." CON IDENTIFICADOR REC-".$datos['codigo']."</h5>;
	$res="
	<div class='cabecera'>
		
	</div>
	<img src='../img/logo.png' class='logoCabecera' />

	<br />
	<br />
	<br />
	<br />";

	return $res;
}

function obtienePiePdfReconocimiento($datos){
	$res="
	<page_footer>
		<table class='cajaPie'>
			<tr>
				<td>".$datos['empleado']."</td>
				<td class='aliDer'>".formateaFechaWeb($datos['fecha'])."</td>
			</tr>
		</table>
	</page_footer>";

	return $res;
}

function obtieneVacunasPdfReconocimiento($datos){
	$res='';
	$vacunas=array('Infancia','Td','VHA','VHB');
	
	for($i=0;$i<count($vacunas);$i++){
		$res.=compruebaCheckVacunacionPdfReconocimiento($vacunas[$i],$datos['checkVacunaciones'.$i]);
	}

	if($res!=''){
		$res=quitaUltimaComa($res);
		$res.='.';
	}

	return $res;
}

function compruebaCheckVacunacionPdfReconocimiento($vacuna,$valor){
	$res='';

	if($valor=='SI'){
		$res=$vacuna.', ';
	}

	return $res;
}

function generaPdfApto($codigoReconocimiento){
	$datos=consultaBD("SELECT reconocimientos_medicos.fecha, clientes.EMPNOMBRE AS empresa, CONCAT(empleados.apellidos,', ',empleados.nombre) AS empleado,
					   empleados.dni, puestos_trabajo.nombre AS puesto, reconocimientos_medicos.apto,
					   protocoloClasificacion, EMPPROV, tipoReconocimiento, riesgoPuesto, reconocimientos_medicos.periodicidad, mesSiguienteReconocimiento, anioSiguienteReconocimiento,
					   reconocimientos_medicos.puestoManual, protocolizacion.nombrePuesto, tipoReconocimientoTexto, textoApto, reconocimientos_medicos.periodicidadTexto

					   FROM reconocimientos_medicos LEFT JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
					   LEFT JOIN clientes ON empleados.codigoCliente=clientes.codigo 
					   LEFT JOIN protocolizacion ON reconocimientos_medicos.codigoPuestoTrabajo=protocolizacion.codigo
					   LEFT JOIN puestos_trabajo ON protocolizacion.codigoPuestoTrabajo=puestos_trabajo.codigo

					   WHERE MD5(reconocimientos_medicos.codigo)='$codigoReconocimiento';",true,true);

	$apto=compruebaAptoReconocimientoMedico($datos['apto']);
	$patologias='NO';
	if($apto!='Apto'){
		$patologias='';
	}

	//Parte de puesto (se prioriza el manual)
	$puesto=$datos['nombrePuesto'];
	if($puesto==''){
		$puesto=$datos['puestoManual'];
	}
	//Fin parte de puesto

	$tipoReconocimiento=$datos['tipoReconocimiento'];
	if($tipoReconocimiento=='Otros'){
		$tipoReconocimiento=$datos['tipoReconocimientoTexto'];
	}

	if($datos['periodicidad']=='Otros'){
		$datos['periodicidad']='Otra ('.$datos['periodicidadTexto'].')';
	}

	$firma=obtieneFirmaPdfReconocimiento();
	$fecha=explode('-', $datos['fecha']);
	$meses=array('00'=>'','01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
    $contenido = "
	<style type='text/css'>
	<!--

		.cabecera{
			text-align:center;
			margin-top:5px;
			width:100%;
		}

		.logo{
			width:100px;
			position:absolute;
			left:50px;
			top:50px;
		}

		.cajaCliente{
			text-align:right;
			position:absolute;
			right:100px;
			top:70px;
			font-size:14px;
		}

		.contenedor{
			width:70%;
			margin-left:140px;
			text-align:justify;
			margin-top:60px;
			font-size:14px;
		}

		.textoPie{
			font-size:14;
			color:#61B4DD;
			text-align:center;
			margin-bottom:7px;
		}

		.cajaFirma{
			margin-top:150px;
			text-align:center;
		}
		#gris{
			background:#DDD;
			position:absolute;
			top:0px;
			right:0px;
			width:40px;
			height:1124px;
		}

		#gris img{
			position:absolute;
			width:20px;
			top:160px;
			right:15px;
		}
		.imagenFondo{
	        position:absolute;
	        z-index:1;
	        top:-20px;
	        left:-30px;
	    }

	    .imagenFondo img{
	        width:98%;
	        height:100%;
	        display:inline-block;
	    }
	-->
	</style>
	<page>
		<div class='imagenFondo'><img src='../img/fondoInforme2.png' alt='Anesco' /></div>
		<div id='gris'>
		<img src='../img/bordeApto.png' />
		</div>
		<div class='cabecera'>
			<h4>INFORME MEDICO DE CALIFICACIÓN DE APTITUD LABORAL</h4>
			<img src='../img/logo.png' class='logo' />
			<div class='cajaCliente'>
				<table>
					<tr>
						<td>Empresa:</td>
						<td style='text-align:left;padding-left:10px;padding-bottom:5px;'><b>".$datos['empresa']."</b></td>
					</tr>
					<tr>
						<td>Trabajador/a:</td>
						<td style='text-align:left;padding-left:10px;padding-bottom:5px;'><b>".$datos['empleado']."</b></td>
					</tr>
					<tr>
						<td>DNI:</td>
						<td style='text-align:left;padding-left:10px;padding-bottom:5px;'><b>".$datos['dni']."</b></td>
					</tr>
				</table>
				<br /><br />
				En Sevilla, a ".$fecha[2]." de ".$meses[$fecha[1]]." de ".$fecha[0]."
			</div>
		</div>
		<div class='contenedor'>
			Según lo establecido  en  el  art.  22  de  la  LPRL  al  trabajador/a <b>".$datos['empleado'].", con D.N.I ".$datos['dni']."</b> perteneciente  a  su  empresa,  se  
			le  efectuó el  día  <b>".$fecha[2]." de ".$meses[$fecha[1]]." de ".$fecha[0]."</b>   examen  de  salud  específico  según  riesgo  laboral  de  
			tipo  <br /><b>".$tipoReconocimiento."</b>  con  el  siguiente resultado:
			<br />
			<br />
			Calificación y Dictamen de Aptitud Laboral (Criterio Médico) para el puesto de trabajo indicado: <b>".comprobarPunto($datos['apto']).". ".nl2br($datos['textoApto'])."</b>
			<br />
			<br />
			Puesto de trabajo: <b>".comprobarPunto($puesto).".</b><br/><br/>
			Empresa: <b>".comprobarPunto($datos['empresa']).".</b><br/><br/>

			<b>Protocolos específicos</b> aplicados: ".comprobarPunto($datos['protocoloClasificacion']).".<br/><br/>
			Para la calificación se ha tenido en cuenta los riesgos  asociados al puesto de trabajo: ".comprobarPunto($datos['riesgoPuesto']).".<br/><br/>
			Periodicidad:   <b>".$datos['periodicidad']."</b>,  si  no  existen  cambios  en  las  condiciones  laborales  o  personales  del  trabajador/a.<br />
			<br />
			Se realizará nuevo examen de salud: <b>".$meses[$datos['mesSiguienteReconocimiento']]." ".$datos['anioSiguienteReconocimiento']."</b>.<br/><br/>

			Este  Informe  de  Aptitud  se  emite en función de la exploración  y pruebas médicas realizadas, la  información  transmitida  por el trabajador/a  y  la evaluación de riesgos del puesto.<br/><br/>

 			Facilitaremos al trabajador/a  los resultados del presente reconocimiento y, en su caso, cualquier hallazgo clínico no laboral que pudiese existir, para que lo ponga en conocimiento de su médico de familia. <br/>
 			<p>Atentamente,</p>

		</div>

		<page_footer>

			<div class='cajaFirma'>
				<img style='width:250px;border:none' src='../img/firmaRM.png' /><br/>
				<b>Dr. Patricio Peñas de Bustillo</b><br/>
				Nº Colegiado 41/15275<br/>
				Medico Especialista en Medicina Del Trabajo<br/><br /><br />
				En cumplimiento del artículo 23 de la LPRL, este documento ha de formar parte de la<br />
				documentación a disposición de las Autoridades Sanitarias y Laborales competentes.
			</div>

			<div class='textoPie'>
				www.anescoprl.es
			</div>
		</page_footer>

	</page>";

	return $contenido;
}

function comprobarPunto($texto){
	$texto=trim($texto);

	$punto=substr($texto, -1);
	if($punto=='.'){
		$texto=substr($texto, 0,-1);
	}
	return $texto;
}

function obtieneFirmaPdfReconocimiento(){
	$res='../img/firmaRM.png';
	/*
	if($datos['ficheroFirma']!='' && $datos['ficheroFirma']!='NO' && file_exists('../documentos/usuarios/'.$datos['ficheroFirma'])){
		$res='../documentos/usuarios/'.$datos['ficheroFirma'];
	}
	*/
	return $res;
}

function obtieneEpisPuesto(){
	$res=array();
	$datos=arrayFormulario();
	extract($datos);
	$puesto=datosRegistro('protocolizacion',$codigoPuesto);
	$listado=consultaBD('SELECT codigoEpi FROM epis_de_puesto_trabajo WHERE codigoPuestoTrabajo='.$puesto['codigoPuestoTrabajo'],true);
	while($item=mysql_fetch_assoc($listado)){
		array_push($res, $item['codigoEpi']);
	}
	echo json_encode($res);
}

function registraDesplegable(){
	$res=false;
	$datos=arrayFormulario();
	extract($datos);
	conexionBD();
		$res=consultaBD('INSERT INTO desplegables VALUES (NULL,"'.$valor.'","'.$campo.'");');
		if($res){
			$codigo=mysql_insert_id();
			$res='<option value="NULL"></option>';
			$texto=explode('_', $campo);
			$texto=$texto[0];
			$listado=consultaBD('SELECT * FROM desplegables WHERE campo LIKE "'.$texto.'%" ORDER BY texto;');
			while($item=mysql_fetch_assoc($listado)){
				$res.='<option value="'.$item['codigo'].'"';
				if($codigo==$item['codigo']){
					$res.=' selected="selected"';
				}
				$res.='>'.$item['texto'].'</option>';
			}
		}
	cierraBD();
	echo $res;	
}

function eliminaDesplegable(){
	$res=false;
	$datos=arrayFormulario();
	extract($datos);
	conexionBD();
		$res=consultaBD('DELETE FROM desplegables WHERE codigo="'.$valor.'" AND campo="'.$campo.'";');
		if($res){
			$codigo=mysql_insert_id();
			$res='<option value="NULL"></option>';
			$texto=explode('_', $campo);
			$texto=$texto[0];
			$listado=consultaBD('SELECT * FROM desplegables WHERE campo LIKE "'.$texto.'%" ORDER BY texto;');
			while($item=mysql_fetch_assoc($listado)){
				$res.='<option value="'.$item['codigo'].'"';
				if($codigo==$item['codigo']){
					$res.=' selected="selected"';
				}
				$res.='>'.$item['texto'].'</option>';
			}
		}
	cierraBD();
	echo $res;	
}

function obtienePuestoManualEmpleado(){
	$datos=arrayFormulario();
	extract($datos);

	$datos=consultaBD("SELECT puestoManual, codigoPuestoTrabajo FROM empleados WHERE codigo='$codigoEmpleado';",true,true);

	echo json_encode($datos);
}


function creaTablaSelectorComentariosAnalitica($datos){
	echo "
	<table id='tablaComentariosAnalitica' class='table table-striped table-bordered datatable'>
		<thead>
			<tr>
				<th> Comentario </th>
				<th> Recomendaciones </th>
				<th> Adjunto </th>
			</tr>
		</thead>

        <tbody>";

        conexionBD();

        $i=0;

        $consulta=consultaBD("SELECT codigo, comentario, recomendaciones FROM comentarios_analiticas WHERE eliminado='NO' ORDER BY comentario;");
        while($comentario=mysql_fetch_assoc($consulta)){
        	$valor=compruebaValoresCheckSelectorComentariosAnaliticas($datos,$comentario['codigo']);

        	echo "
        	<tr>
        		<td>".$comentario['comentario'];
        			 campoOculto($comentario['codigo'],'codigoComentario'.$i);
        			 divOculto($comentario['recomendaciones'],'recomendacionesComentario'.$i);
        	echo "
        		</td>";

        		campoCheckTabla('checkRecomendacionesComentario'.$i,$valor['checkRecomendaciones']);
        		campoCheckTabla('checkAdjuntoComentario'.$i,$valor['checkAdjunto']);

        	echo "
        	</tr>";

        	$i++;
        }


        cierraBD();

            	
	echo "
    	</tbody>
	</table>
	
    <br />";
}

function compruebaValoresCheckSelectorComentariosAnaliticas($datos,$codigoComentarioAnalitica){
	$res=array(
		'checkRecomendaciones'	=>	false,
		'checkAdjunto'			=>	false,
	);

	if($datos){
		$codigoReconocimiento=$datos['codigo'];

		$res=consultaBD("SELECT checkRecomendaciones, checkAdjunto FROM comentarios_analiticas_reconocimientos_medicos WHERE codigoReconocimiento='$codigoReconocimiento' AND codigoComentarioAnalitica='$codigoComentarioAnalitica';",false,true);
	}

	return $res;
}
//Fin parte de gestión de reconocimientos médicos