<?php
	session_start();
	include_once("funciones.php");
  	compruebaSesion();

	//Carga de PHP Excel
	require_once('../../api/phpexcel/PHPExcel.php');
	require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');

	// Carga de la plantilla
	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("../documentacion/plantillaReconocimientos.xlsx");

	$objPHPExcel->setActiveSheetIndex(0);//Selección hoja
    $query=construyeConsultaExcelRRMM($_POST['eliminado']);
	conexionBD();
	$listado=consultaBD($query);
	$i=3;
	while($item=mysql_fetch_assoc($listado)){
		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($item['numero']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($item['nombre']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($item['apellidos']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue($item['dni']);
		$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue($item['sexo']);
		$objPHPExcel->getActiveSheet()->getCell('G'.$i)->setValue($item['edad']);
		$objPHPExcel->getActiveSheet()->getCell('H'.$i)->setValue($item['empresa']);
		$objPHPExcel->getActiveSheet()->getCell('I'.$i)->setValue(formateaFechaWeb($item['fecha']));
		$objPHPExcel->getActiveSheet()->getCell('J'.$i)->setValue('Básico');
		$i++;
	}
	cierraBD();

	foreach(range('B','J') as $columnID) {
     	$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
   	}

	$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	$objWriter->save('../documentacion/Listado_RRMM.xlsx');


	// Definir headers
	header("Content-Type: application/ms-xlsx");
	header("Content-Disposition: attachment; filename=Listado_RRMM.xlsx");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('../documentacion/Listado_RRMM.xlsx');

function construyeConsultaExcelRRMM($eliminado){

	$columnas=array(
		'fecha',
		'EMPID',
		'empresa',
		'empleado',
		'dni',
		'apto',
		'aptoVisible',
		'reconocimientoVisible',
		'bloqueado',
		'codigoFacturaReconocimientoMedico',
		'fecha',
		'advertencia',
		'mesSiguienteReconocimiento',
		'anioSiguienteReconocimiento',
		'codigoProfesionalExterno',
		'numero'
	);

	$having='HAVING eliminado="'.$eliminado.'"';

	$having=obtieneWhereListadoExcel($having,$columnas,false);
	$orden=obtieneOrdenListadoExcel($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT reconocimientos_medicos.codigo, reconocimientos_medicos.fecha, CONCAT(clientes.EMPNOMBRE,' (',clientes.EMPMARCA,')') AS empresa, empleados.nombre,empleados.apellidos, empleados.dni, apto, checkAudiometria, 
			checkOtrasPruebasFuncionales, checkEspirometria, checkElectrocardiograma, advertencia,
		  	cerrado, reconocimientos_medicos.codigoUsuario, reconocimientos_medicos.editable, reconocimientos_medicos.eliminado, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS usuario,
		  	mesSiguienteReconocimiento, anioSiguienteReconocimiento, clientes.EMPID, reconocimientos_medicos.numero, sexo, 
		  	YEAR(CURDATE())-YEAR(fechaNacimiento) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(fechaNacimiento,'%m-%d'), 0 , -1 ) AS edad, codigoProfesionalExterno,
		  	CONCAT(empleados.nombre,' ',empleados.apellidos) AS empleado, bloqueado

		    FROM reconocimientos_medicos LEFT JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
		    LEFT JOIN clientes ON empleados.codigoCliente=clientes.codigo 
		    LEFT JOIN usuarios ON reconocimientos_medicos.codigoUsuario=usuarios.codigo
		    LEFT JOIN reconocimientos_medicos_en_facturas ON reconocimientos_medicos.codigo=reconocimientos_medicos_en_facturas.codigoReconocimientoMedico
		    LEFT JOIN facturas_reconocimientos_medicos ON reconocimientos_medicos_en_facturas.codigoFacturaReconocimientoMedico=facturas_reconocimientos_medicos.codigo AND reconocimientos_medicos.codigoContrato=facturas_reconocimientos_medicos.codigoContrato

		    GROUP BY reconocimientos_medicos.codigo
		    $having";

	return $query;
}
?>