<?php
    session_start();
    include_once("funciones.php");
    compruebaSesion();

    $contenido=generaPdfApto($_GET['encp']);

    require_once('../../api/html2pdf/html2pdf.class.php');
    $html2pdf=new HTML2PDF('P','A4','es', true, 'UTF-8', array(0, 0, 0, 0));
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->WriteHTML($contenido);
    $html2pdf->Output('Carta_de_Aptitud.pdf');

?>