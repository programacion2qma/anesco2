<?php
    session_start();
    include_once("funciones.php");
    compruebaSesion();

    $contenido=generaPdfReconocimiento($_GET['encp']);

    require_once('../../api/html2pdf/html2pdf.class.php');
    $html2pdf=new HTML2PDF('P','A4','es',true,'UTF-8',array(20,10,20,15));
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->WriteHTML($contenido);
    $html2pdf->Output('Reconocimiento_medico.pdf');

    /*echo "<div style='border:1px solid red;margin:0 auto;width:900px;'>";
    echo $contenido;
    echo "</div>";*/

    // header('Content-Type: text/plain');
    // echo $contenido;