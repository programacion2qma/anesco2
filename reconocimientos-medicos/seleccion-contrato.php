<?php
  $seccionActiva=3;
  include_once("../cabecera.php");
  seleccionContrato();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>

<script type="text/javascript">	
$(document).ready(function(){
	$('.selectpicker').selectpicker();

	$('#codigoContrato').change(function(){
		oyenteContrato($(this).val());
	});

	$('.form-horizontal button[type=submit]').unbind();
 	$('.form-horizontal button[type=submit]').click(function(e){
 		e.preventDefault();
 		if($('#codigoContrato').val()=='NULL'){
 			alert('Para continuar debes seleccionar un contrato');
 		} else {
 			$('.form-horizontal').submit();
 		}
	});
});

function oyenteContrato(codigoContrato){
	if(codigoContrato=='NULL'){
		$('#incluidos').text('-');
		$('#restantes').text('-');
	}
	else{
		var consulta=$.post('../listadoAjax.php?include=reconocimientos-medicos&funcion=consultaReconocimientosContrato();',{'codigoContrato':codigoContrato},
		function(respuesta){
			$('#incluidos').text(respuesta.incluidos);
			$('#restantes').text(respuesta.restantes);
		},'json');
	}
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>