<?php
/*
	Script generado para comprobar las facturas con importes
	distintos entre la base de datos actual en producción y un backup
	previo, para realizar revisiones ante la incidencia notificada el 23/01/2024.
*/
session_start();
include_once('funciones.php');

$consulta = consultaBD("SELECT * FROM facturas", true);

$_CONFIG['nombreBD'] = 'anesco2Backup';
conexionBD();

while( $facturaActual=mysql_fetch_assoc($consulta) ){
	$codigoFactura = $facturaActual['codigo'];
	$baseImponible = $facturaActual['baseImponible'];
	$total = $facturaActual['total'];

	$facturaOriginal = consultaBD("SELECT * FROM facturas WHERE codigo='$codigoFactura' AND (baseImponible!=$baseImponible OR total!=$total)", false, true);

	if($facturaOriginal){
		echo "Factura $codigoFactura. Datos actuales online: base imponible $baseImponible, total $total. Datos de ayer: base imponible ".$facturaOriginal['baseImponible'].", total ".$facturaOriginal['total']."<br />";
	}
}

cierraBD();