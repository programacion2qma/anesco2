<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión Auditorias


function operacionesDocumentos(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaDocumento();
	}
	elseif(isset($_POST['fechaCreacion'])){
		$res=insertaDocumento();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('documentosVS');
	}

	mensajeResultado('fechaCreacion',$res,'Documento');
    mensajeResultado('elimina',$res,'Documento', true);
}


function insertaDocumento(){
	$res=insertaDatos('documentosVS');
	return $res;
}


function actualizaDocumento(){
	$res=actualizaDatos('documentosVS');
	return $res;
}


/*
TIPOS DE DOCUMENTOS
1 - Informe VS
4 - Comunicación al trabajador para la realización de la VS
5 - Programación reconocimientos médicos
8 - Carta Aptitud
12 - Protección maternidad

*/
function gestionDocumentos(){
	operacionesDocumentos();
	
	abreVentanaGestionConBotones('Gestión de DocumentosVS','index.php');
	$datos=compruebaDatos('documentosVS');
	
	campoFecha('fechaCreacion','Fecha creación',$datos);
	campoSelectContratos($datos);
	campoSelect('tipoDocumento','Documento',array('Informe VS','Comunicación al trabajador para la realización de la VS','Programación reconocimientos médicos','Carta aptitud','Protección maternidad'),array(1,4,5,8,12),$datos,'selectpicker span5 show-tick');
	campoRadio('enviado','Enviado',$datos);
	campoFechaBlanco('fechaEnvio','Fecha envío',$datos);
	campoRadio('visible','Visible',$datos);
	campoOculto($datos,'formulario');
	campoSelectEmpleadoDocumentoVS($datos);

	cierraVentanaGestion('index.php');
}

function campoSelectEmpleadoDocumentoVS($datos){
	echo "<div class='hide' id='cajaEmpleados'>";

	if(!$datos){
		campoSelect('codigoEmpleado','Empleado',array(''),array('NULL'));
	}
	else{
		obtieneCampoSelectEmpleadosContrato($datos);
	}

	echo "</div>";
}

function obtieneCampoSelectEmpleadosContrato($datos=false){
	if(!$datos){
		$datos=arrayFormulario();
		$valor=false;
	}
	else{
		$valor=$datos['codigoEmpleado'];
	}

	$consulta=campoSelectConsulta('codigoEmpleado','Empleado',"SELECT empleados.codigo, CONCAT(nombre,' ',apellidos) AS texto FROM empleados INNER JOIN ofertas ON empleados.codigoCliente=ofertas.codigoCliente INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='".$datos['codigoContrato']."';",$valor);
}

function campoSelectContratos($datos){
	$nombres=array();
	$valores=array();
	$contratos=consultaBD('SELECT * FROM contratos',true);
	while($contrato=mysql_fetch_assoc($contratos)){
		$oferta=datosRegistro('ofertas',$contrato['codigoOferta']);
		$cliente=datosRegistro('clientes',$oferta['codigoCliente']);
		$referencia=formateaReferenciaContrato($cliente,$contrato);
		array_push($valores, $contrato['codigo']);
		array_push($nombres, $referencia.' - '.$cliente['EMPNOMBRE']);
	}
	campoSelect('codigoContrato','Contrato',$nombres,$valores,$datos,'selectpicker span7 show-tick');
}

function imprimeDocumentos($where){
	global $_CONFIG;
	$consulta=consultaBD('SELECT documentosVS.* FROM documentosVS INNER JOIN contratos ON documentosVS.codigoContrato=contratos.codigo INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo '.$where,true);
	$tipos=array(1=>'Informe VS',4=>'Comunicación al trabajador para la realización de la VS',5=>'Programación reconocimientos médicos',8=>'Carta aptitud',12=>'Protección maternidad');
	while($datos=mysql_fetch_assoc($consulta)){
		$contrato=datosRegistro('contratos',$datos['codigoContrato']);
		$oferta=datosRegistro('ofertas',$contrato['codigoOferta']);
		$cliente=datosRegistro('clientes',$oferta['codigoCliente']);
		$referencia=formateaReferenciaContrato($cliente,$contrato);
		$enviado=$datos['enviado']=='SI'?formateaFechaWeb($datos['fechaEnvio']):'NO';
		echo "
			<tr>
				<td class='centro'>".formateaFechaWeb($datos['fechaCreacion'])."</td>
				<td>".$referencia."</td>
				<td>".$cliente['EMPNOMBRE']." (".$cliente['EMPMARCA'].")</td>
				<td>".$tipos[$datos['tipoDocumento']]."</td>
				<td class='centro'>".$enviado."</td>
				<td class='centro'>
					<div class='btn-group centro'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."documentos-vs/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li>
						    <li class='divider'></li>
						    <li><a target='_blank' class='noAjax' href='".$_CONFIG['raiz']."documentos-vs/generaDocumento.php?tipo=".$datos['tipoDocumento']."&codigo=".$datos['codigo']."'><i class='icon-download'></i> Descargar</i></a></li>
						    <li class='divider'></li>
							<li><a target='_blank' class='noAjax' href='".$_CONFIG['raiz']."correos/gestion.php?codigoDoc=".$datos['codigo']."'><i class='icon-send'></i> Enviar</i></a></li>
						</ul>
					</div>
				</td>
				<td><input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'></td>

			</tr>";
	}
}


function estadisticasDocumentos($where){
	$res=array();
	$res=consultaBD('SELECT COUNT(documentosVS.codigo) AS total FROM documentosVS INNER JOIN contratos ON documentosVS.codigoContrato=contratos.codigo INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo '.$where,true,true);
	return $res;
}


function generaDocumentoVS($tipoDocumento,$codigoDocumento,$correo=false){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();

	conexionBD();

    if($tipoDocumento==1){
        $nombre=generaDocumento1($codigoDocumento,$PHPWord,$correo);
    }
    elseif($tipoDocumento==4){
        $nombre=generaDocumento4($codigoDocumento,$PHPWord,$correo);
    }
    elseif($tipoDocumento==5){
        $nombre=generaDocumento5($codigoDocumento,$PHPWord,$correo);
    }
    elseif($tipoDocumento==8){
        $nombre=generaDocumento8($codigoDocumento,$PHPWord,$correo);
    }
    elseif($tipoDocumento==12){
        $nombre=generaDocumento12($codigoDocumento,$PHPWord,$correo);
    }

    cierraBD();

    return $nombre;
}



function generaDocumento1($codigo,$PHPWord,$correo){

	$datos=consultaBD("SELECT clientes.EMPNOMBRE, documentosVS.fechaCreacion, ficheroLogo, EMPPC FROM documentosVS INNER JOIN contratos ON documentosVS.codigoContrato=contratos.codigo INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo WHERE documentosVS.codigo = ".$codigo,false,true);
	$nombreFichero="informe_vs.docx";
	$documento=$PHPWord->loadTemplate('../documentacion/vs/plantilla_'.$nombreFichero);
	$documento->setValue("nombreEmpresa",utf8_decode($datos['EMPNOMBRE']));
	$documento->setValue("persona",utf8_decode($datos['EMPPC']));
	$documento->setValue("fecha",formateaFechaWeb($datos['fechaCreacion']));
	if($datos['ficheroLogo']!='NO' && $datos['ficheroLogo']!=''){
		copy('../clientes/imagenes/'.$datos['ficheroLogo'],'../clientes/imagenes/image3.png');
		$documento->replaceImage('../clientes/imagenes/','image3.png');
	}
	$documento->save('../documentacion/vs/'.$nombreFichero);
	
	if(!$correo){
		header("Content-Type: application/vnd.ms-docx");
    	header("Content-Disposition: attachment; filename=$nombreFichero");
    	header("Content-Transfer-Encoding: binary");
    }

    return '../documentacion/vs/'.$nombreFichero;
}

function generaDocumento4($codigo,$PHPWord,$correo){
	$datos=consultaBD("SELECT clientes.EMPNOMBRE, clientes.EMPID, documentosVS.fechaCreacion, ficheroLogo ,fechaVsReal FROM documentosVS INNER JOIN contratos ON documentosVS.codigoContrato=contratos.codigo INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo INNER JOIN formularioPRL ON formularioPRL.codigoContrato=contratos.codigo WHERE documentosVS.codigo = ".$codigo,false,true);
	$nombreFichero="comunicacion_al_trabajador_para_la_realizacion_de_la_vs.docx";
	$documento=$PHPWord->loadTemplate('../documentacion/vs/plantilla_'.$nombreFichero);
	$documento->setValue("nombreEmpresa",utf8_decode($datos['EMPNOMBRE']));
	if($datos['ficheroLogo']!='NO' && $datos['ficheroLogo']!=''){
		copy('../clientes/imagenes/'.$datos['ficheroLogo'],'../clientes/imagenes/image2.png');
		$documento->replaceImage('../clientes/imagenes/','image2.png');
	}
	$documento->save('../documentacion/vs/'.$nombreFichero);
	
	if(!$correo){
		header("Content-Type: application/vnd.ms-docx");
    	header("Content-Disposition: attachment; filename=$nombreFichero");
    	header("Content-Transfer-Encoding: binary");
    }

    return '../documentacion/vs/'.$nombreFichero;
}

function generaDocumento5($codigo,$PHPWord,$correo){
    $dias=array('01'=>'uno','02'=>'dos','03'=>'tres','04'=>'cuatro','05'=>'cinco','06'=>'seis','07'=>'siete','08'=>'ocho','09'=>'nueve','10'=>'diez','11'=>'once','12'=>'doce','13'=>'trece','14'=>'catorce','15'=>'quince','16'=>'dieciseis','17'=>'diecisiete','18'=>'dieciocho','19'=>'diecinueve','20'=>'veinte','21'=>'veintiuno','22'=>'veintidos','23'=>'veintitres','24'=>'veinticuatro','25'=>'veinticinco','26'=>'veintiseis','27'=>'veintisiete','28'=>'veintiocho','29'=>'veintinueve','30'=>'treinta','31'=>'treinta y uno');
    $meses=array('01'=>'enero','02'=>'febrero','03'=>'marzo','04'=>'abril','05'=>'mayo','06'=>'junio','07'=>'julio','08'=>'agosto','09'=>'septiembre','10'=>'octubre','11'=>'noviembre','12'=>'diciembre');
	$datos=consultaBD("SELECT clientes.EMPNOMBRE, clientes.EMPID, documentosVS.fechaCreacion, ficheroLogo ,fechaVsReal FROM documentosVS INNER JOIN contratos ON documentosVS.codigoContrato=contratos.codigo INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo INNER JOIN formularioPRL ON formularioPRL.codigoContrato=contratos.codigo WHERE documentosVS.codigo = ".$codigo,false,true);
	$nombreFichero="programacion_reconocimientos_medicos.docx";
	$documento=$PHPWord->loadTemplate('../documentacion/vs/plantilla_'.$nombreFichero);
	$documento->setValue("nombreEmpresa",utf8_decode($datos['EMPNOMBRE']));
	$documento->setValue("id",utf8_decode($datos['EMPID']));
	$fecha=explode('-', $datos['fechaCreacion']);
	$documento->setValue("fecha",utf8_decode($dias[$fecha[2]].' de '.$meses[$fecha[1]].' de '.$fecha[0]));
	$fechaVsReal=explode('-', $datos['fechaVsReal']);
	$documento->setValue("mes",utf8_decode(strtoupper($meses[$fechaVsReal[1]])));
	if($datos['ficheroLogo']!='NO' && $datos['ficheroLogo']!=''){
		copy('../clientes/imagenes/'.$datos['ficheroLogo'],'../clientes/imagenes/image3.png');
		$documento->replaceImage('../clientes/imagenes/','image3.png');
	}
	$documento->save('../documentacion/vs/'.$nombreFichero);
	
	if(!$correo){
		header("Content-Type: application/vnd.ms-docx");
    	header("Content-Disposition: attachment; filename=$nombreFichero");
    	header("Content-Transfer-Encoding: binary");
    }

    return '../documentacion/vs/'.$nombreFichero;
}



function generaDocumento8($codigo,$PHPWord,$correo){
    $dias=array('01'=>'uno','02'=>'dos','03'=>'tres','04'=>'cuatro','05'=>'cinco','06'=>'seis','07'=>'siete','08'=>'ocho','09'=>'nueve','10'=>'diez','11'=>'once','12'=>'doce','13'=>'trece','14'=>'catorce','15'=>'quince','16'=>'dieciseis','17'=>'diecisiete','18'=>'dieciocho','19'=>'diecinueve','20'=>'veinte','21'=>'veintiuno','22'=>'veintidos','23'=>'veintitres','24'=>'veinticuatro','25'=>'veinticinco','26'=>'veintiseis','27'=>'veintisiete','28'=>'veintiocho','29'=>'veintinueve','30'=>'treinta','31'=>'treinta y uno');
    $meses=array('01'=>'enero','02'=>'febrero','03'=>'marzo','04'=>'abril','05'=>'mayo','06'=>'junio','07'=>'julio','08'=>'agosto','09'=>'septiembre','10'=>'octubre','11'=>'noviembre','12'=>'diciembre');
	
	$datos=consultaBD("SELECT reconocimientos_medicos.codigo, CONCAT(empleados.nombre,' ',empleados.apellidos) AS nombreTrabajador, dni, clientes.EMPNOMBRE AS nombreEmpresa, reconocimientos_medicos.fecha, 
					   documentosVS.fechaCreacion, tipoReconocimiento, periodicidad, apto, checkClasificacion0, checkClasificacion1, checkClasificacion2, checkClasificacion3, checkClasificacion4,
					   checkClasificacion5, checkClasificacion6, checkClasificacion7, checkClasificacion8, checkClasificacion9, checkClasificacion10, puestos_trabajo.nombre AS puesto,
					   mesSiguienteReconocimiento, anioSiguienteReconocimiento
					   FROM documentosVS INNER JOIN contratos ON documentosVS.codigoContrato=contratos.codigo 
					   INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo 
					   INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
					   INNER JOIN empleados ON clientes.codigo=empleados.codigoCliente
					   INNER JOIN reconocimientos_medicos ON contratos.codigo=reconocimientos_medicos.codigoContrato AND empleados.codigo=reconocimientos_medicos.codigoEmpleado
					   INNER JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo
					   WHERE documentosVS.codigo='$codigo' AND reconocimientos_medicos.eliminado='NO';",false,true);

	$protocolos=obtieneNombresProtocolos($datos);
	$riesgos=obtieneRiesgosReconocimientoMedico($datos['codigo']);
	
	$nombreFichero="carta_aptitud.docx";
	$documento=$PHPWord->loadTemplate('../documentacion/vs/plantilla_'.$nombreFichero);

	$documento->setValue("nombreEmpresa",utf8_decode($datos['nombreEmpresa']));
	$documento->setValue("nombreTrabajador",utf8_decode($datos['nombreTrabajador']));
	$documento->setValue("dni",utf8_decode($datos['dni']));
	
	$fecha=explode('-', $datos['fechaCreacion']);
	$documento->setValue("dia",$fecha[2]);
	$documento->setValue("mes",utf8_decode($meses[$fecha[1]]));
	$documento->setValue("anio",$fecha[0]);

	$fecha=explode('-', $datos['fecha']);
	$documento->setValue("diaReconocimiento",$fecha[2]);
	$documento->setValue("mesReconocimiento",utf8_decode($meses[$fecha[1]]));
	$documento->setValue("anioReconocimiento",$fecha[0]);

	$documento->setValue("siguienteReconocimiento",utf8_decode($meses[$datos['mesSiguienteReconocimiento']]).' '.$datos['anioSiguienteReconocimiento']);
	
	$documento->setValue("tipoReconocimiento",utf8_decode($datos['tipoReconocimiento']));
	$documento->setValue("apto",utf8_decode($datos['apto']));
	$documento->setValue("puesto",utf8_decode($datos['puesto']));
	$documento->setValue("protocolos",utf8_decode($protocolos));
	$documento->setValue("riesgos",utf8_decode($riesgos));
	$documento->setValue("periodicidad",utf8_decode($datos['periodicidad']));

	$documento->save('../documentacion/vs/'.$nombreFichero);
	
	if(!$correo){
		header("Content-Type: application/vnd.ms-docx");
    	header("Content-Disposition: attachment; filename=$nombreFichero");
    	header("Content-Transfer-Encoding: binary");
    }

    return '../documentacion/vs/'.$nombreFichero;
}

function obtieneNombresProtocolos($datos){
	$res='';
	$protocolos=array('General','Conductores','Cargas mov. repetidos', 'Posturas forzadas','Neuropatia por presión','Ruido','Altura','Dermatosis','Asma laboral','Asma laboral: Soldador','Silicosis y otras neumoconiosis');

	for($i=0;$i<count($protocolos);$i++){
		if($datos['checkClasificacion'.$i]=='SI'){
			$res.=$protocolos[$i].', ';
		}
	}

	if($res!=''){
		$res=quitaUltimaComa($res);
	}

	return $res;
}

function obtieneRiesgosReconocimientoMedico($codigoReconocimiento){
	$res='';
	$consulta=consultaBD("SELECT nombre FROM riesgos INNER JOIN riesgos_reconocimiento_medico ON riesgos.codigo=riesgos_reconocimiento_medico.codigoRiesgo WHERE codigoReconocimiento='$codigoReconocimiento';");
	
	while($datos=mysql_fetch_array($consulta)){
		$res.=$datos['nombre'].', ';
	}

	if($res!=''){
		$res=quitaUltimaComa($res);
	}

	return $res;
}


function generaDocumento12($codigo,$PHPWord,$correo){
	$datos=consultaBD("SELECT clientes.EMPNOMBRE, clientes.EMPID, documentosVS.fechaCreacion, ficheroLogo ,fechaVsReal FROM documentosVS INNER JOIN contratos ON documentosVS.codigoContrato=contratos.codigo INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo INNER JOIN formularioPRL ON formularioPRL.codigoContrato=contratos.codigo WHERE documentosVS.codigo = ".$codigo,false,true);
	$nombreFichero="proteccion_maternidad.docx";
	$documento=$PHPWord->loadTemplate('../documentacion/vs/plantilla_'.$nombreFichero);
	$documento->setValue("nombreEmpresa",utf8_decode($datos['EMPNOMBRE']));
	if($datos['ficheroLogo']!='NO' && $datos['ficheroLogo']!=''){
		copy('../clientes/imagenes/'.$datos['ficheroLogo'],'../clientes/imagenes/image2.png');
		$documento->replaceImage('../clientes/imagenes/','image2.png');
	}
	$documento->save('../documentacion/vs/'.$nombreFichero);
	
	if(!$correo){
		header("Content-Type: application/vnd.ms-docx");
    	header("Content-Disposition: attachment; filename=$nombreFichero");
    	header("Content-Transfer-Encoding: binary");
    }

    return '../documentacion/vs/'.$nombreFichero;
}

//Fin parte de gestión Auditorías
