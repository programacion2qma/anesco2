<?php
  $seccionActiva=3;
  include_once("../cabecera.php");
  gestionDocumentos();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').selectpicker();
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
		$(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});

		$('#tipoDocumento,#codigoContrato').change(function(){
			oyenteEmpleados();
		});
	});

	function oyenteEmpleados(){
		var tipoDocumento=$('#tipoDocumento').val();
		var codigoContrato=$('#codigoContrato').val();

		if(tipoDocumento==8){
			var consulta=$.post('../listadoAjax.php?include=documentos-vs&funcion=obtieneCampoSelectEmpleadosContrato();',{'codigoContrato':codigoContrato});
			consulta.done(function(respuesta){
				$('#cajaEmpleados').html(respuesta).find('select').selectpicker('refresh');
				$('#cajaEmpleados').removeClass('hide');
			});
		}
		else{
			$('#cajaEmpleados').addClass('hide');
		}
	}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>