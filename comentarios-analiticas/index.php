<?php
  $seccionActiva=10;
  include_once('../cabecera.php');
  
  $res=operacionesComentariosAnaliticas();

  $eliminado='NO';
  if(isset($_GET['eliminado'])){
    $eliminado=$_GET['eliminado'];
  }

  $estadisticas=estadisticasGenericas('comentarios_analiticas',false,"eliminado='".$eliminado."'");
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema sobre comentarios de analíticas:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-tint"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Comentario/s</div>
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de comentarios de analíticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                  <?php
                  if($eliminado=='NO'){?>
                  
                  <a href="<?php echo $_CONFIG['raiz']; ?>comentarios-analiticas/gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nuevo comentario</span> </a>
                  <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
                  <a href="<?php echo $_CONFIG['raiz']; ?>comentarios-analiticas/index.php?eliminado=SI" class="shortcut"><i class="shortcut-icon icon-trash-o"></i><span class="shortcut-label">Eliminados</span> </a>
                <?php } else { ?>
                  <a href="<?php echo $_CONFIG['raiz']; ?>comentarios-analiticas/index.php?eliminado=NO" class="shortcut noAjax"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label"> Volver</span> </a>
                  <a href="javascript:void" id='reactivar' class="shortcut noAjax"><i class="shortcut-icon icon-check-circle"></i><span class="shortcut-label">Reactivar</span> </a>
                <?php } ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Comentarios registrados</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Comentario </th>
                  <th> Recomendaciones </th>
                  <th> Adjunto </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeComentariosAnaliticas($eliminado);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>