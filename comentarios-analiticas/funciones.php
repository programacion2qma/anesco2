<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión de comentarios de analíticas


function operacionesComentariosAnaliticas(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('comentarios_analiticas',time(),'../documentos/comentarios-analiticas/');
	}
	elseif(isset($_POST['comentario'])){
		$res=insertaDatos('comentarios_analiticas',time(),'../documentos/comentarios-analiticas/');
	}
	elseif(isset($_POST['elimina'])){
		$res=eliminaComentariosAnaliticas($_POST['elimina']);
	}

	mensajeResultado('comentario',$res,'comentario');
    mensajeResultado('elimina',$res,'comentario', true);
}



function gestionComentarioAnalitica(){
	operacionesComentariosAnaliticas();
	
	abreVentanaGestionConBotones('Gestión de comentario de analítica','index.php','span3','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('comentarios_analiticas');
	
	campoTexto('comentario','Comentario',$datos);
		
	cierraColumnaCampos();
	abreColumnaCampos();
	
	campoFichero('ficheroAdjunto','Adjunto',0,$datos,'../documentos/comentarios-analiticas/','Descargar');

	cierraColumnaCampos(true);
	abreColumnaCampos();

	areaTexto('recomendaciones','Recomendaciones',$datos,'areaInforme'); 
	campoOculto($datos,'eliminado','NO');
	
	cierraVentanaGestion('index.php',true);
}


function imprimeComentariosAnaliticas($eliminado){

	$consulta=consultaBD("SELECT * FROM comentarios_analiticas WHERE eliminado='$eliminado';",true);
	while($datos=mysql_fetch_assoc($consulta)){
		$adjunto='NO';
		if($datos['ficheroAdjunto']!='NO' && $datos['ficheroAdjunto']!=''){
			$adjunto='SI';
		}

		echo "
			<tr>
				<td>".$datos['comentario']."</td>
				<td>".$datos['recomendaciones']."</td>
				<td>".$adjunto."</td>
				<td class='centro'>
					<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-edit'></i> Editar</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
	        </tr>";
	}
}

function eliminaComentariosAnaliticas($valor){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();
    for($i=0;isset($datos['codigo'.$i]);$i++){
        $res=$res && cambiaValorCampo('eliminado',$valor,$datos['codigo'.$i],'comentarios_analiticas',false);
    }
    cierraBD();

    return $res;
}
//Fin parte de gestión de comentarios de analíticas