<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de gastos facturación

function operacionesGastos(){
	$res=true;

	if(isset($_POST['codigo'])){
		formateaImportesGastos();
		$res=actualizaDatos('facturacion_gastos');
	}
	elseif(isset($_POST['fecha'])){
		formateaImportesGastos();
		$res=insertaDatos('facturacion_gastos');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('facturacion_gastos');
	}

	mensajeResultado('fecha',$res,'gasto');
    mensajeResultado('elimina',$res,'gasto', true);
}

function formateaImportesGastos(){
	$_POST['importe']=formateaNumeroWeb($_POST['importe'],true);
	$_POST['total']=formateaNumeroWeb($_POST['total'],true);
}

function imprimeGastos(){

	$consulta=consultaBD("SELECT * FROM facturacion_gastos",true);
	$tipos=array('Estructural'=>'Estructural','Tecnico'=>'Técnico','Vigilancia de la Salud'=>'Vigilancia de la Salud');

	while($datos=mysql_fetch_assoc($consulta)){

		echo "
		<tr>
			<td>".formateaFechaWeb($datos['fecha'])."</td>
			<td>".$tipos[$datos['tipo']]."</td>
			<td>".$datos['concepto']."</td>
			<td>".formateaNumeroWeb($datos['total'])." €</td>
        	<td class='centro'>
				<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
			</td>
			<td class='centro'>
				<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' />
	        </td>
	    </tr>";
	}

}


function gestionGastos(){
	operacionesGastos();

	abreVentanaGestionConBotones('Gestión de Gastos','index.php','span3');
	$datos=compruebaDatos('facturacion_gastos');

		campoSelect('tipo','Tipo de gasto',array('Estructural','Técnico','Vigilancia de la Salud'),array('Estructural','Tecnico','Vigilancia de la Salud'),$datos);
		
	cierraColumnaCampos();
	abreColumnaCampos();

		campoFecha('fecha','Fecha',$datos);

	cierraColumnaCampos(true);
	abreColumnaCampos();

		campoTexto('concepto','Concepto',$datos,'span7');
		campoSelectContrato($datos);
		campoSelectConsulta('codigoUsuario','Usuario (opcional)','SELECT codigo, CONCAT(nombre," ",apellidos) AS texto FROM usuarios WHERE usuario!="soporte" AND tipo!="CLIENTE" AND tipo!="EMPLEADO" ORDER BY nombre;',$datos);

	cierraColumnaCampos(true);
	abreColumnaCampos();

		campoTextoSimbolo('importe','Importe','<i class="icon-euro"></i>',formateaNumeroWeb($datos['importe']));
		campoSelect('iva','IVA',array('0%','4%','10%','21%'),array(0,4,10,21),$datos,'selectpicker span1 show-tick','');
		campoTextoSimbolo('total','Total','<i class="icon-euro"></i>',formateaNumeroWeb($datos['total']),'input-mini pagination-right',0,true);

	cierraColumnaCampos(true);
	abreColumnaCampos();

		areaTexto('observaciones','Observaciones',$datos,'areaInforme'); 

	cierraVentanaGestion('index.php',true);
}

function campoSelectContrato($datos){
	
	$nombres=array('');
	$valores=array('NULL');

	$whereVigor='';
	if(!$datos){
		$whereVigor="AND enVigor='SI'";//Para que solo filtre por vigor cuando se está en la creación, de forma que no desaparezca el contrato cuando ya no está en vigor
	}

	$listado=consultaBD("SELECT contratos.codigo, contratos.codigoInterno, contratos.fechaInicio, EMPID, EMPCP, EMPNOMBRE 
						 FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo 
						 INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
						 WHERE contratos.eliminado='NO' $whereVigor
						 ORDER BY EMPNOMBRE, fechaInicio, contratos.codigoInterno",true);

	while($item=mysql_fetch_assoc($listado)){
		array_push($valores,$item['codigo']);
		array_push($nombres,formateaReferenciaContrato($item,$item).' - '.$item['EMPNOMBRE']);
	}

	campoSelect('codigoContrato','Contrato (opcional)',$nombres,$valores,$datos,'selectpicker span7 show-tick',"data-live-search='true'");
}

//Fin parte de gastos facturación