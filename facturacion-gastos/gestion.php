<?php
  $seccionActiva=46;
  include_once("../cabecera.php");
  gestionGastos();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>
<script src="../js/campoLogo.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
		$('.selectpicker').selectpicker();

		$('#importe,#iva').change(function(){
			calcular();
		});
	});

function calcular(){
	var importe=$('#importe').val();
	importe=formateaNumeroCalculo(importe);
	
	var iva=$('#iva').val();
	iva=formateaNumeroCalculo(iva);
	iva=importe*iva/100;

	var total=parseFloat(importe)+parseFloat(iva);
	
	$('#total').val(formateaNumeroWeb(total));
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>