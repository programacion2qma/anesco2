<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de satisfacción de clientes

function operacionesCuestionario(){
	$res=true;

	if(isset($_POST['pregunta1'])){
		$res=insertaDatos('satisfaccion');
	}

	mensajeResultadoEncuesta('pregunta1',$res);
}


function gestionCuestionario(){
	operacionesCuestionario();
	abreVentanaCuestionario();

	campoTexto('cliente','Cliente');
    campoFecha('fecha','Fecha');
    textoCuestionario();

    creaTablaSatisfaccion(); 
	creaPreguntaTablaEncuesta("La atención comercial prestada:",1);
	creaPreguntaTablaEncuesta("La atención del Técnico:",2);
	creaPreguntaTablaEncuesta("Profesionalidad del Técnico:",3);
	creaPreguntaTablaEncuesta("Comportamiento del Técnico:",4);
	creaPreguntaTablaEncuesta("Información sobre plazos y cobros:",5);
	creaPreguntaTablaEncuesta("Los plazos de respuesta:",6);
	creaAreaTextoTablaEncuesta();
	cierraTablaSatisfaccion();
	cierraVentanaCuestionario();
}

function cierraVentanaCuestionario(){
	echo "	<br />
			<div class='form-actions'>
	          <button type='submit' class='btn btn-propio'><i class='icon-share'></i> Enviar cuestionario</button> 
	        </div> <!-- /form-actions -->
	      </fieldset>
	    </form>
	    </div>
	</div>";
}

function mensajeResultadoEncuesta($indice,$res){
	if(isset($_POST[$indice]) && $res){
		mensajeOk('Cuestionario enviado correctamente. <br />Gracias por su colaboración.');
	}
	elseif(isset($_POST[$indice]) && !$res){
		mensajeOk('En estos momentos no podemos procesar su petición. Inténtelo de nuevo más tarde.');
	}
}

function abreVentanaCuestionario(){
	echo '<div class="widget">
            <div class="widget-content">
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="?" method="post">
                  <fieldset>';
}

//Fin parte de satisfacción de clientes