<?php
  include_once("cabecera.php");
?>
<br><br>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span12">
					<?php
            gestionCuestionario();
					?>
        </div>
      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<?php include_once('../pie.php'); ?>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-rating-input.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
  });

</script>


