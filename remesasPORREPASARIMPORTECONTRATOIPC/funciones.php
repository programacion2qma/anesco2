<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de tareas de remesas

function operacionesRemesas(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaRemesa();
	}
	elseif(isset($_POST['formato'])){
		$res=creaRemesa();
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoRemesas($_POST['elimina']);
	}
	elseif(isset($_GET['errorRemesa'])){
		mensajeError('ha habido un problema a generar la remesa. Compruebe la información introducida.<br />Si el problema persiste, contacte con webmaster@qmaconsultores.com.');
	}

	mensajeResultado('formato',$res,'Remesas');
    mensajeResultado('elimina',$res,'Remesas', true);
}

function creaRemesa(){
	$_POST['total']=formateaNumeroWeb($_POST['total'],true);
	$res=insertaDatos('remesas');

	if($res){
		$datos=arrayFormulario();

		conexionBD();
		$res=insertaVencimientosRemesa($res,$datos);
		cierraBD();
	}

	return $res;
}

function actualizaRemesa(){
	$_POST['total']=formateaNumeroWeb($_POST['total'],true);
	$res=actualizaDatos('remesas');

	/*if($res){
		$datos=arrayFormulario();

		conexionBD();
		insertaVencimientosRemesa($_POST['codigo'],$datos,true);
		cierraBD();
	}*/

	return $res;
}


function insertaVencimientosRemesa($codigoRemesa,$datos,$actualizacion=false){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM vencimientos_en_remesas WHERE codigoRemesa=$codigoRemesa");
	}
	$res=$res && consultaBD("DELETE FROM cobros_facturas WHERE codigoRemesa='$codigoRemesa' OR (codigoFormaPago IS NULL AND fechaCobro='0000-00-00')");//Elimina los cobros asociados a la remesa o vacíos


	$codigoCuentaPropia=$datos['codigoCuentaPropia'];


	foreach($datos['filasTabla'] as $vencimientoSeleccionado){
		$vencimientoSeleccionado=explode('&{}&',$vencimientoSeleccionado);
		$codigoVencimiento=$vencimientoSeleccionado[0];
		$importe=$vencimientoSeleccionado[1];

		$datosVencimiento=obtieneDatosVencimiento($codigoVencimiento,$datos['estado']);

		if($datos['mismoVencimiento']=='SI'){
			$fechaCobro=$datos['fechaVencimiento'];
		}
		else{
			$fechaCobro=$datosVencimiento['fechaVencimiento'];
		}

		if($datosVencimiento){
			$res=$res && consultaBD("INSERT INTO vencimientos_en_remesas VALUES(NULL,$codigoRemesa,$codigoVencimiento,$importe);");
			$res=$res && consultaBD("INSERT INTO cobros_facturas VALUES(NULL,".$datosVencimiento['codigoFactura'].",".$datosVencimiento['codigoFormaPago'].",'$fechaCobro',$importe,$codigoCuentaPropia,$codigoRemesa);");
		}
	}

	return $res;
}


function obtieneDatosVencimiento($codigoVencimiento,$estado){
	/*$estados=array(''=>'','DOMICILIADA'=>'DOMICILIADO','ANTICIPADA'=>'ANTICIPADO','DESCONTADA'=>'DESCONTADO','IMPAGADO DEFINITIVO','IMPAGADO GESTIÓN DE COBRO','COBRADA'=>'PAGADO','PENDIENTE'=>'PENDIENTE PAGO');
	$res=consultaBD("UPDATE vencimientos_facturas SET estado='".$estados[$estado]."' WHERE codigo='$codigoVencimiento';");//Actualización del estado del vencimiento/recibo con el estado correspondiente

	if($res){*/
		$res=consultaBD("SELECT codigoFactura, fechaVencimiento, codigoFormaPago FROM vencimientos_facturas WHERE codigo='$codigoVencimiento';",false,true);
	//}

	return $res;
}

function imprimeRemesas($eliminadas='NO'){	

	$consulta=consultaBD("SELECT remesas.codigo, remesas.fecha, remesas.fechaVencimiento, formato, IFNULL(COUNT(vencimientos_en_remesas.codigo),0) AS domiciliaciones, 
						  remesas.total, remesas.estado, financiado, codigoCuentaPropia, mismoVencimiento
						  FROM remesas LEFT JOIN vencimientos_en_remesas ON remesas.codigo=vencimientos_en_remesas.codigoRemesa 
						  LEFT JOIN vencimientos_facturas ON vencimientos_en_remesas.codigoVencimiento=vencimientos_facturas.codigo 
						  LEFT JOIN facturas ON vencimientos_facturas.codigoFactura=facturas.codigo
						  WHERE remesas.eliminado='$eliminadas'
						  GROUP BY remesas.codigo",true);


	while($datos=mysql_fetch_assoc($consulta)){
		echo "
			<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".formateaFechaWeb($datos['fechaVencimiento'])."</td>
				<td>".$datos['formato']."</td>
				<td>".$datos['domiciliaciones']."</td>
				<td><div class='pagination-right'>".formateaNumeroWeb($datos['total'])." €</div></td>
				<td>".$datos['estado']."</td>
				<td>".botonAcciones(array('Detalles','Descargar XML','Descargar Excel','Descargar PDF'),array('remesas/gestion.php?codigo='.$datos['codigo'],'remesas/generaRemesa.php?codigo='.$datos['codigo'],'remesas/generaExcel.php?codigo='.$datos['codigo'],'remesas/generaRemesaPDF.php?codigo='.$datos['codigo']),array("icon-search-plus",'icon-cloud-download','icon-cloud-download','icon-cloud-download'),array(0,2,2,1))."</td>
				<td class='centro'>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' />
	        	</td>
			</tr>";
	}
}


function gestionRemesa(){
	operacionesRemesas();
	
	abreVentanaGestionConBotones('Gestión de remesas','index.php');
	$datos=compruebaDatos('remesas');

	campoRadio('formato','Formato',$datos,'CORE',array('CORE','B2B'),array('CORE','B2B'));
	campoRadio('financiado','Remesa de descuento',$datos);
	campoSelectConsulta('codigoCuentaPropia','Cuenta de origen',"SELECT codigo, CONCAT(nombre,' (',iban,')') AS texto FROM cuentas_propias WHERE activo='SI' ORDER BY nombre",$datos,'selectpicker span6 show-tick');
	campoFecha('fecha','Fecha emisión',$datos);

	campoRadio('mismoVencimiento','Vencimiento global',$datos);
	echo "<div id='cajaVencimiento'>";
	campoFecha('fechaVencimiento','Vencimiento global',$datos);
	echo "</div>";

	$totalRemesa=creaTablaFacturasRemesa($datos);

	campoTextoSimbolo('total','Total remesa','€',formateaNumeroWeb($totalRemesa));

	campoSelect('estado','Estado',array('Pendiente','Domiciliada','Descontada','Anticipada','Cobrada'),array('PENDIENTE','DOMICILIADA','DESCONTADA','ANTICIPADA','COBRADA'),$datos);
	
	campoOculto($datos,'eliminado','NO');

	cierraVentanaGestionRemesa($datos);
}

function filtroRemesas(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoSelect(2,'Formato',array('','CORE','B2B'),array('','CORE','B2B'),false,'selectpicker span2 show-tick','');
	campoSelectSiNoFiltro(10,'De descuento');
	campoSelectConsulta(11,'Cuenta de origen',"SELECT codigo, CONCAT(nombre,' (',iban,')') AS texto FROM cuentas_propias WHERE activo='SI' ORDER BY nombre");
	campoFecha(0,'F. emisión desde');
	campoFecha(6,'Hasta');
	campoSelectSiNoFiltro(12,'Vencimiento global');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha(1,'F. vencimiento desde');
	campoFecha(7,'Hasta');
	campoSelectConsulta(9,'Administrativo/a',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE activo='SI' AND (tipo='ADMINISTRACION1' OR tipo='ADMINISTRACION2') ORDER BY nombre");
	campoTextoSimbolo(4,'Total','€');
	campoSelect(5,'Estado',array('','Pendiente','Domiciliada','Descontada','Anticipada','Cobrada'),array('','PENDIENTE','DOMICILIADA','DESCONTADA','ANTICIPADA','COBRADA'));
	campoSelectSiNoFiltro(8,'Activa');	

	cierraColumnaCampos();
	cierraCajaBusqueda();
}



function creaTablaFacturasRemesa($datos){
    echo "
    <div class='control-group'>                     
        <label class='control-label'>Vencimiento/s asociado/s:</label>
        <div class='controls'>
            <table class='table table-striped tabla-simple' id='tablaVencimientos'>
                <thead>
                    <tr>
                        <th> Vencimiento </th>
                        <th> Importe </th>";

            if(!$datos){
                echo "	<th> </th>";
            }

    echo "
                    </tr>
                </thead>
                <tbody>";

                	conexionBD();
        
		            $query=obtieneConsultaVencimiento($datos);
	                $consulta=consultaBD($query);

	                cierraBD();

	                $total=0;
	                while($datosF=mysql_fetch_assoc($consulta)){
	                    $total+=imprimeLineaTablaFacturasRemesa($datosF,$datos);
	                }

      
    echo "      </tbody>
            </table>
        </div>
    </div><br />";

    return $total;
}

function imprimeLineaTablaFacturasRemesa($datos,$detalles){
    $res=$datos['importe'];

    $validacion=validaLineaVencimiento($datos);
    $enlace='facturas';
    if(substr_count($datos['texto'],'PROFORMA')>0){
    	$enlace='proformas';
    }

    echo "<tr>
    		<td><a href='../$enlace/gestion.php?codigo=".$datos['codigoFactura']."' target='_blank' class='noAjax'>".$datos['texto']."</a> ".$validacion."</td>
    		<td class='aliDer'>".formateaNumeroWeb($datos['importe'])." € </td>";

    if(!$detalles){
    	echo "<td class='centro'><input type='checkbox' name='filasTabla[]' value='".$datos['codigo']."&{}&".$datos['importe']."'/></td>";
    }

    echo "</tr>";

    return $res;
}

function validaLineaVencimiento($datos){
	$res='';

	if(trim($datos['EMPIBAN'])==''){
		$res="<span class='label label-danger'><i class='icon-exclamation-circle'></i> Cliente sin IBAN </a>";		
	}
	elseif(trim($datos['bic'])==''){
		$res="<span class='label label-danger'><i class='icon-exclamation-circle'></i> Cliente sin BIC </a>";		
	}

	return $res;
}

//Esta función comprueba que se esté o no en la creación. Si se está en la creación de una nueva remesa, la consulta que devuelve filtra los vencimientos que ya pertenezcan a otra remesa:
function obtieneConsultaVencimiento($datos){
	if($datos){
		$res="SELECT vencimientos_facturas.codigo, CONCAT('Factura: ',IFNULL(CONCAT(serie,'-',numero),'PROFORMA'),' - Cliente: ',EMPNOMBRE,' - Fecha vencimiento: ',DATE_FORMAT(fechaVencimiento,'%d/%m/%Y')) AS texto, vencimientos_en_remesas.importe, clientes.EMPIBAN, clientes.bic, facturas.codigo AS codigoFactura FROM vencimientos_en_remesas INNER JOIN vencimientos_facturas ON vencimientos_en_remesas.codigoVencimiento=vencimientos_facturas.codigo INNER JOIN facturas ON vencimientos_facturas.codigoFactura=facturas.codigo LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo WHERE vencimientos_en_remesas.codigoRemesa='".$datos['codigo']."' ORDER BY fechaVencimiento DESC;";
	}
	else{
		$datos=arrayFormulario();//En la creación existen la fecha de inicio y la de fin, para filtrar las remesas

		//La siguiente consulta extrae los importes de los vencimientos, restándole en su caso el importe del abono asociado a la factura (la operación es de suma porque el importe es negativo):
		$res="SELECT vencimientos_facturas.codigo, CONCAT('Factura: ',IFNULL(CONCAT(serie,'-',numero),'PROFORMA'),' - Cliente: ',EMPNOMBRE,' - Fecha vencimiento: ',DATE_FORMAT(fechaVencimiento,'%d/%m/%Y')) AS texto, 
			  vencimientos_facturas.importe+IFNULL((SELECT SUM(total) FROM facturas WHERE tipoFactura='ABONO' AND codigoFacturaAsociada=vencimientos_facturas.codigoFactura)/(SELECT COUNT(vf2.codigo) FROM vencimientos_facturas vf2 WHERE vf2.codigoFactura=vencimientos_facturas.codigoFactura),0) AS importe, 
			  clientes.EMPIBAN, clientes.bic, facturas.codigo AS codigoFactura

			  FROM vencimientos_facturas INNER JOIN facturas ON vencimientos_facturas.codigoFactura=facturas.codigo 
			  LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo 
			  LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo 
			  LEFT JOIN formas_pago ON vencimientos_facturas.codigoFormaPago=formas_pago.codigo

 			  WHERE importe>0 AND facturas.activo='SI' AND tipoFactura!='ABONO' AND formas_pago.remesable='SI' AND fechaVencimiento>='".$datos['fechaInicio']."' 
 			  AND fechaVencimiento<='".$datos['fechaFin']."' 
 			  AND vencimientos_facturas.codigo NOT IN(
 			  	SELECT codigoVencimiento FROM vencimientos_en_remesas INNER JOIN remesas on vencimientos_en_remesas.codigoRemesa=remesas.codigo WHERE codigoVencimiento IS NOT NULL
 			  ) 

 			  HAVING importe>0

 			  ORDER BY fechaVencimiento DESC;";
	}
	
	return $res;
}


function consultaImporteVencimiento(){
	$codigoVencimiento=$_POST['codigoVencimiento'];

	$datos=consultaBD("SELECT importe FROM vencimientos_facturas WHERE codigo=$codigoVencimiento",true,true);

	echo formateaNumeroWeb($datos['importe']);
}


//Parte de generación de remesas en XML

function generaXMLRemesa($codigoRemesa){
	require_once("../../api/sepassd/SEPASDD.php");

	$datos=datosRegistro('remesas',$codigoRemesa);
	$emisor=datosRegistro('cuentas_propias',$datos['codigoCuentaPropia']);

	$fechaRemesa=compruebaFechaRemesa($datos['fecha']);
	$fechaCobro=compruebaFechaRemesa($datos['fechaVencimiento'],true);
	$lineasRemesa=creaRegistrosAdeudosRemesaXML($fechaRemesa,$fechaCobro,$datos['codigo'],$datos['mismoVencimiento']);

	//Parte de agrupación: si batch es true se agrupan todos los recibos en un único vencimiento
	$batch=true;
	if($datos['mismoVencimiento']=='NO'){
		$batch=false;
	}
	//Fin parte agrupación

	$configuracionRemesa=array(
		"name" => $emisor['razonSocial'],
        "IBAN" => $emisor['iban'],
        "BIC" => $emisor['bic'],
        "batch" => $batch,
        "creditor_id" => $emisor['referenciaAcreedor'],
        "currency" => "EUR",
		"tipoRemesa" => $datos['formato'],
		"id_cabecera" => $emisor['referenciaAcreedor'],
		"financiado" => $datos['financiado']
    );

    try{
    	$SEPASDD=new SEPASDD($configuracionRemesa);
		foreach ($lineasRemesa as $linea) {
			$SEPASDD->addPayment($linea);
		}
	    
	    $contenido=$SEPASDD->save();

	    $nombreFichero='Remesa-'.time().'.xml';
		$ficheroRemesa=fopen("../documentos/remesas/".$nombreFichero, "w");

		fwrite($ficheroRemesa,$contenido);
		fclose($ficheroRemesa);

		//Definición de headers
		header("Content-Type: application/xml");
		header("Content-Disposition: attachment; filename=$nombreFichero");
		header("Content-Transfer-Encoding: binary");

		//Descarga de fichero
		readfile('../documentos/remesas/'.$nombreFichero);
	}
	catch(Exception $e){
		error_log($e->getMessage());
		echo $e->getMessage();
		//header("Location: index.php?errorRemesa");
	}
}

function creaRegistrosAdeudosRemesaXML($fechaRemesa,$fechaCobro,$codigoRemesa,$vencimientoGlobal){
	$res=array();
	$i=0;

	conexionBD();

	$consulta=consultaBD("SELECT clientes.codigo AS codigoCliente, EMPNOMBRE, vencimientos_en_remesas.importe, IFNULL(CONCAT(serie,' ',numero),CONCAT('PROFORMA ',facturas.codigo)) AS referencia, 
						  facturas.fecha, clientes.EMPIBAN, clientes.bic, vencimientos_facturas.fechaVencimiento
						  FROM vencimientos_en_remesas INNER JOIN vencimientos_facturas ON vencimientos_en_remesas.codigoVencimiento=vencimientos_facturas.codigo
						  INNER JOIN facturas ON vencimientos_facturas.codigoFactura=facturas.codigo
						  INNER JOIN clientes ON facturas.codigoCliente=clientes.codigo
						  LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
						  WHERE vencimientos_en_remesas.codigoRemesa=$codigoRemesa
						  ORDER BY fechaVencimiento DESC");

	while($datos=mysql_fetch_assoc($consulta)){

		//Parte de comprobación de fecha vencimiento
		$fechaVencimiento=$fechaCobro;
		if($vencimientoGlobal=='NO'){
			$fechaVencimiento=compruebaFechaRemesa($datos['fechaVencimiento'],true);
		}
		//Fin parte de comprobación de fecha de vencimiento

		if($datos['EMPIBAN']!=''){//Valido que el IBAN no esté vacío, para que no pete la remesa
			$res[$i]=array(
				"name" => limpiaCadena($datos['EMPNOMBRE']),
	            "IBAN" => $datos['EMPIBAN'],
	            "BIC" => $datos['bic'],
	            "amount" => number_format($datos['importe'],2,'',''),
	            "type" => "RCUR",
	            "collection_date" => $fechaVencimiento,
	            "mandate_id" => $datos['referencia'],
	            "mandate_date" => $fechaRemesa,
	            "description" => 'Vencimiento factura n.: '.$datos['referencia'].' del: '.formateaFechaWeb($datos['fecha'])
		    );
		}

		$i++;
	}

	cierraBD();

	return $res;
}

function compruebaFechaRemesa($fecha,$posterior=false){//Para validar las fechas de las remesas
	if($fecha==date('Y-m-d') && !$posterior){
		$fecha=strtotime('-1 day',strtotime($fecha));//La de emisión debe ser al menos 1 día anterior a la actual
		$fecha=date('Y-m-d',$fecha );
	}
	elseif($fecha==date('Y-m-d') && $posterior){
		$fecha=strtotime('+1 day',strtotime($fecha));//La de cobro debe ser al menos 1 día posterior a la actual
		$fecha=date('Y-m-d',$fecha );
	}

	return $fecha;
}

//Fin parte de generación de remesas en XML


function seleccionFechaRemesas(){
	abreVentanaGestionConBotones('Filtro de selección de recibos','gestion.php','span12','icon-edit','',false,'noAjax','index.php',true,'Continuar','icon-chevron-right');

	echo '<div class="hidden">';
		campoSelectConsulta('codigoEmisor','Emisor',"SELECT codigo, razonSocial AS texto FROM emisores WHERE activo='SI' ORDER BY razonSocial",'','selectpicker span5 show-tick');
	echo '</div>';

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha('fechaInicio','Desde la fecha');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha('fechaFin','Hasta');

	cierraVentanaGestion('index.php',true,true,'Continuar','icon-chevron-right');
}




function generaExcelRemesa($documento,$codigoRemesa){
	$documento->setActiveSheetIndex(0);//Selección hoja
	$fila=2;

	conexionBD();
	$datos=consultaBD("SELECT * FROM remesas WHERE codigo=$codigoRemesa;",false,true);
	$cuenta=consultaBD("SELECT iban FROM cuentas_propias WHERE codigo='".$datos['codigoCuentaPropia']."';",false,true);

	//Parte de la remesa
	$documento->getActiveSheet()->SetCellValue('A'.$fila,$datos['formato']);
	$documento->getActiveSheet()->SetCellValue('B'.$fila,$datos['financiado']);
	$documento->getActiveSheet()->SetCellValue('C'.$fila,$cuenta['iban']);
	$documento->getActiveSheet()->SetCellValue('D'.$fila,formateaFechaWeb($datos['fecha']));

	if($datos['mismoVencimiento']=='NO'){
		$documento->getActiveSheet()->SetCellValue('E'.$fila,'NO');
	}
	else{
		$documento->getActiveSheet()->SetCellValue('E'.$fila,formateaFechaWeb($datos['fechaVencimiento']));
	}

	$documento->getActiveSheet()->SetCellValue('F'.$fila,$datos['total']);
	$documento->getActiveSheet()->SetCellValue('G'.$fila,$datos['estado']);
	//Fin parte de la remesa

	//Parte de los vencimientos asociados
	$fila=5;
	$consulta=consultaBD("SELECT IFNULL(CONCAT(serie,' ',numero),CONCAT('PROFORMA ',facturas.codigo)) AS numero, vencimientos_en_remesas.importe, EMPNOMBRE, fechaVencimiento FROM vencimientos_en_remesas INNER JOIN vencimientos_facturas ON vencimientos_en_remesas.codigoVencimiento=vencimientos_facturas.codigo INNER JOIN facturas ON vencimientos_facturas.codigoFactura=facturas.codigo LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo WHERE vencimientos_en_remesas.codigoRemesa='$codigoRemesa' ORDER BY fechaVencimiento DESC;");
	while($datos=mysql_fetch_assoc($consulta)){
		$documento->getActiveSheet()->SetCellValue('A'.$fila,$datos['numero']);
		$documento->getActiveSheet()->SetCellValue('B'.$fila,$datos['importe']);
		$documento->getActiveSheet()->SetCellValue('C'.$fila,$datos['EMPNOMBRE']);
		$documento->getActiveSheet()->SetCellValue('G'.$fila,formateaFechaWeb($datos['fechaVencimiento']));

		$fila++;
	}
	//Fin parte de los vencimientos asociados
	
	cierraBD();

	$columnas=array('A','B','C','D','E','F','G');
	for($i=0;$i<count($columnas);$i++){
		$documento->getActiveSheet()->getColumnDimension($columnas[$i])->setAutoSize(true);//Ajusta el ancho de la columna al contenido
	}

	$objWriter=new PHPExcel_Writer_Excel2007($documento);
	$objWriter->save('../documentos/remesas/Remesa.xlsx');
}



function cierraVentanaGestionRemesa($datos){
	$destino='seleccion-fechas.php';
	$botonesExtra='';
	if($datos){
		$destino='index.php';
		$botonesExtra="	<a href='generaRemesa.php?codigo=".$datos['codigo']."' class='btn btn-success noAjax'><i class='icon-cloud-download'></i> Descargar XML</a>
						<a href='generaExcel.php?codigo=".$datos['codigo']."' class='btn btn-success noAjax'><i class='icon-cloud-download'></i> Descargar Excel</a>
						<a href='generaRemesaPDF.php?codigo=".$datos['codigo']."' class='btn btn-success noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar PDF</a>";
	}


    echo "
                        <br />                      
                        <div class='form-actions'>
                        <a href='$destino' class='btn btn-default'><i class='icon-chevron-left'></i> Volver</a>
                        <button type='submit' name='Guardar' value='Guardar' class='btn btn-propio'><i class='icon-check'></i> Guardar</button>
						$botonesExtra
                    </div> <!-- /form-actions -->
                  </fieldset>
                 </form>
                </div>


               </div>
               <!-- /widget-content --> 
             </div>

          </div>
        </div>
        <!-- /container --> 
      </div>
      <!-- /main-inner --> 
    </div>
    <!-- /main -->";
}





function generaPDFRemesa($codigoRemesa){
	conexionBD();

	$datos=consultaBD("SELECT * FROM remesas WHERE codigo=$codigoRemesa;",false,true);
	$cuenta=consultaBD("SELECT iban FROM cuentas_propias WHERE codigo='".$datos['codigoCuentaPropia']."';",false,true);

	$recibos='';
	$consulta=consultaBD("SELECT IFNULL(CONCAT(serie,' ',numero),CONCAT('PROFORMA ',facturas.codigo)) AS numero, vencimientos_en_remesas.importe, EMPNOMBRE, fechaVencimiento FROM vencimientos_en_remesas INNER JOIN vencimientos_facturas ON vencimientos_en_remesas.codigoVencimiento=vencimientos_facturas.codigo INNER JOIN facturas ON vencimientos_facturas.codigoFactura=facturas.codigo LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo WHERE vencimientos_en_remesas.codigoRemesa='$codigoRemesa' ORDER BY fechaVencimiento DESC;");
	while($recibo=mysql_fetch_assoc($consulta)){
		$recibos.="<tr>
						<td>".$recibo['numero']."</td>
						<td>".formateaNumeroWeb($recibo['importe'])." €</td>
						<td colspan='4'>".$recibo['EMPNOMBRE']."</td>
						<td>".formateaFechaWeb($recibo['fechaVencimiento'])."</td>
				   </tr>";
	}

	cierraBD();

	if($datos['mismoVencimiento']=='NO'){
		$vencimientoGlobal='NO';
	}
	else{
		$vencimientoGlobal=formateaFechaWeb($datos['fechaVencimiento']);
	}
	

	$contenido = "
	<style type='text/css'>
	<!--
		.tablaRemesas{
			width:100%;
			border-collapse:collapse;
		}

		td{
			padding:10px;
			font-size:12px;
			border:1px solid #000;
		}

		.celdaCabecera{
			background-color:#EEE;
			font-weight:bold;
		}

		.centro{
			text-align:center;
		}
	-->
	</style>
	<page footer='page'>

		<table class='tablaRemesas'>
			<tr>
				<td class='celdaCabecera'>Formato</td>
				<td class='celdaCabecera'>Descuento</td>
				<td class='celdaCabecera'>Cuenta origen</td>
				<td class='celdaCabecera'>Emisión</td>
				<td class='celdaCabecera'>V. Global</td>
				<td class='celdaCabecera'>Total</td>
				<td class='celdaCabecera'>Estado</td>
			</tr>
			<tr>
				<td>".$datos['formato']."</td>
				<td>".$datos['financiado']."</td>
				<td>".$cuenta['iban']."</td>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$vencimientoGlobal."</td>
				<td>".formateaNumeroWeb($datos['total'])." €</td>
				<td>".$datos['estado']."</td>
			</tr>
			<tr>
				<td class='celdaCabecera centro' colspan='7'>Recibos</td>
			</tr>
			<tr>
				<td class='celdaCabecera'>Factura</td>
				<td class='celdaCabecera'>Importe</td>
				<td class='celdaCabecera' colspan='4'>Cliente</td>
				<td class='celdaCabecera'>F. Vencimiento</td>
			</tr>
			".$recibos."
		</table>
	    
	</page>";


	return $contenido;
}


function cambiaEstadoEliminadoRemesas($estado){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],'remesas',false);
	}
	cierraBD();

	return $res;
}

//Fin parte de tareas de remesas