<?php
  $seccionActiva=33;
  include_once("../cabecera.php");
  gestionRemesa();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$('select').selectpicker();


	$('#tablaVencimientos').find('input[type=checkbox]').change(function(){
		calculaTotal();
	});

	oyenteMismoVencimiento();
	$('input[name=mismoVencimiento]').change(function(){
		oyenteMismoVencimiento();
	});

	$('input[name=activo]').change(function(){
		oyenteCampoActivo($(this).val());
	});
});

function oyenteMismoVencimiento(){
	if($('input[name=mismoVencimiento]:checked').val()=='SI'){
		$('#cajaVencimiento').removeClass('hide');
	}
	else{
		$('#cajaVencimiento').addClass('hide');
	}
}

function calculaTotal(){
	var total=0;

	$('#tablaVencimientos').find('input[type=checkbox]:checked').each(function(){
		var importe=$(this).parent().prev().text();
		total+=formateaNumeroCalculo(importe);
	});

	$('#total').val(formateaNumeroWeb(total));
}

function asignaOyentesUltimaFila(){
	$('#tablaVencimientos tr:last .importe').change(function(){
		calculaTotal();
	});

	oyentePrecioFactura('#tablaVencimientos tr:last');
}

function oyentePrecioFactura(selector){
	$(selector).find('chec').change(function(){
		var codigoVencimiento=$(this).val();

		if(codigoVencimiento!='NULL'){
			var fila=obtieneFilaCampo($(this));

			var consulta=$.post('../listadoAjax.php?include=remesas&funcion=consultaImporteVencimiento();',{'codigoVencimiento':codigoVencimiento});
			consulta.done(function(respuesta){
				$('#importe'+fila).val(respuesta);
				calculaTotal();
			});
		}
	});
}


function oyenteCampoActivo(activo){
	if(activo=='NO'){
		if(!confirm('ATENCIÓN: si desactiva la remesa, los vencimientos asociados volverán a estar disponibles para remesar. ¿Desea continuar?')){
			$('input[name=activo][value=SI]').prop('checked',true);
		}
	}
	else{
		if(!confirm('ATENCIÓN: tenga en cuenta que si la remesa estaba desactivada, sus vencimientos asociados han podido girarse en otra remesa. ¿Desea continuar?')){
			$('input[name=activo][value=NO]').prop('checked',true);
		}	
	}
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>