<?php
  $seccionActiva=33;
  include_once('../cabecera.php');
  
  operacionesRemesas();
  $estadisticas=estadisticasGenericas('remesas',false,"eliminado='NO'");
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de remesas:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-file-code-o"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Remesa/s registrada/s</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Remesas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                  <a href="seleccion-fechas.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva remesa</span> </a>
                  <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-times-circle"></i><span class="shortcut-label">Eliminar</span> </a>
                  <a href="eliminados.php" class="shortcut"><i class="shortcut-icon icon-trash-o"></i><span class="shortcut-label">Eliminados</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Remesas registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable" id="tablaRemesas">
                <thead>
                  <tr>
                    <th> F. emisión </th>
                    <th> F. Vencimiento </th>
                    <th> Formato </th>
                    <th> Domiciliaciones </th>
                    <th class='sumatorio'> Total </th>
                    <th> Estado </th>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                      imprimeRemesas();
                    ?>
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../../api/js/filtroTabla.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>