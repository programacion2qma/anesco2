<?php
session_start();
include_once('funciones.php');
compruebaSesion();

//Carga de PHP Excel (usa el de la API para ahorrar espacio en el servidor)
require_once('../../api/phpexcel/PHPExcel.php');
require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');
require_once('../../api/phpexcel/PHPExcel/IOFactory.php');

//Carga de la plantilla
$objReader=new PHPExcel_Reader_Excel2007();
$documento=$objReader->load("../documentos/remesas/plantilla.xlsx");

generaExcelRemesa($documento,$_GET['codigo']);

// Definir headers
header("Content-Type: application/ms-xlsx");
header("Content-Disposition: attachment; filename=Remesa.xlsx");
header("Content-Transfer-Encoding: binary");

// Descargar archivo
readfile('../documentos/remesas/Remesa.xlsx');