<?php
  $seccionActiva=3;
  include_once("../cabecera.php");
  gestionProtocolizacion();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../../api/js/filasTabla.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.selectpicker').selectpicker();

	oyentePeriodicidad();
	$('#periodicidad').change(function(){
		oyentePeriodicidad();
	});

	$('#codigoCliente').change(function(){
		oyenteCliente($(this).val());
	});

	$('#codigoPuestoTrabajo').change(function(){
		oyentePuesto($(this).val());
	});
});

function oyentePeriodicidad(){
	if($('#periodicidad').val()=='Otros'){
		$('#divPeriodicidad').removeClass('hide');
	}
	else{
		$('#divPeriodicidad').addClass('hide');	
	}
}

function oyenteCliente(codigoCliente){
	if(codigoCliente!='NULL'){

		$.post('../listadoAjax.php?include=protocolizacion&funcion=obtienePuestosProtocolizacionCliente();',{'codigoCliente':codigoCliente},function(respuesta){
			
			$('#codigoPuestoTrabajo').html(respuesta.puestos).selectpicker('refresh');

		},'json');

	}
}


function oyentePuesto(codigoPuesto){
	if(codigoPuesto!='NULL'){
		var codigoCliente=$('#codigoCliente').val();

		$.post('../listadoAjax.php?include=protocolizacion&funcion=obtieneRiesgosProtocolizacionPuesto();',{'codigoCliente':codigoCliente,'codigoPuesto':codigoPuesto},function(respuesta){
			
			$('#riesgosAsociados').val(respuesta.riesgos);

		},'json');

	}
}

</script>
<script src="../js/desplegables.js" type="text/javascript"></script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>