<?php
    session_start();
    include_once("funciones.php");
    compruebaSesion();

    $contenido=generaProtocolizacion($_GET['codigo'],isset($_GET['empresa']));

    require_once('../../api/html2pdf/html2pdf.class.php');
    $html2pdf=new HTML2PDF('P','A4','es', true, 'UTF-8', array(0, 0, 0, 0));
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->WriteHTML($contenido);
    $html2pdf->Output('Protocolización.pdf');

?>