<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión de protocolización


function operacionesProtocolizacion(){
	$res=false;

	if(isset($_POST['codigo'])){
		$res=actualizaProtocolizacion();
	}
	elseif(isset($_POST['nombrePuesto'])){
		$res=creaProtocolizacion();
	}
	elseif(isset($_POST['elimina'])){
		$res=eliminaProtocolizaciones($_POST['elimina']);
	}

	mensajeResultado('nombrePuesto',$res,'Protocolización');
    mensajeResultado('elimina',$res,'Protocolización', true);
}


function creaProtocolizacion(){
	$res=insertaDatos('protocolizacion');

	if($res){
		$datos=arrayFormulario();
		$codigoProtocolizacion=$res;

		conexionBD();
		
		$res=$res && insertaProtocolosProtocolizacion($datos,$codigoProtocolizacion);
		$res=$res && insertaPruebasProtocolizacion($datos,$codigoProtocolizacion);
		
		cierraBD();
	}

	return $res;
}

function actualizaProtocolizacion(){
	$res=actualizaDatos('protocolizacion');

	if($res){
		$datos=arrayFormulario();
		$codigoProtocolizacion=$_POST['codigo'];

		conexionBD();
		
		$res=$res && insertaProtocolosProtocolizacion($datos,$codigoProtocolizacion,true);
		$res=$res && insertaPruebasProtocolizacion($datos,$codigoProtocolizacion,true);
		
		cierraBD();
	}

	return $res;
}


function insertaProtocolosProtocolizacion($datos,$codigoProtocolizacion,$actualizacion=false){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM protocolos_asociados_protocolizacion WHERE codigoProtocolizacion='$codigoProtocolizacion';");
	}

	for($i=0;$i<$datos['numeroProtocolos'];$i++){
		
		if(isset($datos['checkProtocolos'.$i])){
			$codigoProtocolo=$datos['checkProtocolos'.$i];

			$res=$res && consultaBD("INSERT INTO protocolos_asociados_protocolizacion VALUES(NULL,'$codigoProtocolizacion','$codigoProtocolo');");
		}

	}

	return $res;
}

function insertaPruebasProtocolizacion($datos,$codigoProtocolizacion,$actualizacion=false){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM pruebas_protocolizacion WHERE codigoProtocolizacion='$codigoProtocolizacion';");
	}

	for($i=0;isset($datos['otrasPruebas_'.$i]);$i++){
		
		if($datos['otrasPruebas_'.$i]!='' && $datos['otrasPruebas_'.$i]!='NULL'){
			$codigoPrueba=$datos['otrasPruebas_'.$i];
			$valoracionEconomica=formateaNumeroWeb($datos['valoracionEconomica'.$i],true);

			$res=$res && consultaBD("INSERT INTO pruebas_protocolizacion VALUES(NULL,'$codigoProtocolizacion','$codigoPrueba','$valoracionEconomica');");
		}

	}

	return $res;
}


function gestionProtocolizacion(){
	operacionesProtocolizacion();
	
	abreVentanaGestionConBotones('Gestión de protocolización','index.php');
	
	$datos=compruebaDatos('protocolizacion');

	abreColumnaCampos();
		campoSelectConsulta('codigoCliente','Empresa',"SELECT codigo, CONCAT(EMPID,' - ',EMPNOMBRE) AS texto FROM clientes WHERE activo='SI' AND eliminado='NO' ORDER BY EMPID, EMPNOMBRE;",$datos,'selectpicker show-tick span5'); 
		campoSelectPuestosTrabajo($datos);
	cierraColumnaCampos(true);
	abreColumnaCampos();

		campoTexto('nombrePuesto','Nombre puesto',$datos);
		campoSelect('obligatoriedad','Obligatoriedad',array('SI','NO'),array('SI','NO'),$datos,'selectpicker span1 show-tick',"data-live-search='true'");
		campoSelect('periodicidad','Periodicidad',array('','Anual','Semestral','Trimestral','Mensual','Otra'),array('','Anual','Semestral','Trimestral','Mensual','Otros'),$datos,'selectpicker span2 show-tick',"");
		campoTextoOtraPeriodicidad($datos);
		campoSelect('apto','APTO',array('APTO','NO APTO'),array('APTO','NO APTO'),$datos,'selectpicker span1 show-tick',"data-live-search='true'");	
		areaTexto('riesgosAsociados','Riesgos asociados',$datos,'areaInforme');
		areaTexto('resumenRiesgos','Resumen',$datos,'areaInforme');
	
	cierraColumnaCampos();
	abreColumnaCampos('columna-protocolos-protocolizacion');

		campoCheckProtocolos($datos);

	cierraColumnaCampos(true);

	creaTablaPruebas($datos);
	
	
	areaTexto('sensibles','Trabajadadores sensibles',$datos,'areaInforme');
	
	campoOculto($datos,'eliminado','NO');

	cierraVentanaGestion('index.php');
}

function campoSelectPuestosTrabajo($datos){
	if(!$datos){
		campoSelect('codigoPuestoTrabajo','Puesto base',array(''),array('NULL'),false,'selectpicker show-tick span5');
	}
	else{
		$query="SELECT puestos_trabajo.codigo, puestos_trabajo.nombre AS texto
				FROM funciones_formulario_prl INNER JOIN empleados ON funciones_formulario_prl.codigoEmpleado=empleados.codigo 
				INNER JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo
				WHERE empleados.codigoCliente='".$datos['codigoCliente']."'
				GROUP BY puestos_trabajo.codigo
				ORDER BY puestos_trabajo.nombre;";
		
		$consulta=consultaBD($query,true);
		if(mysql_num_rows($consulta)==0){//Hago esta comprobación para los casos de clientes que no tienen Evaluación de Riesgos (de puestos) hecha
			$query="SELECT codigo, nombre AS texto FROM puestos_trabajo ORDER BY nombre";
		}

		campoSelectConsulta('codigoPuestoTrabajo','Puesto base',$query,$datos,'selectpicker show-tick span5');
	}
}

function campoCheckProtocolos($datos){
	$codigos=array();
	$nombres=array();
	$valores=false;
	
	conexionBD();

	$consulta=consultaBD("SELECT codigo, nombre FROM protocolos WHERE eliminado='NO' ORDER BY nombre;");
	while($protocolo=mysql_fetch_assoc($consulta)){
		array_push($codigos,$protocolo['codigo']);
		array_push($nombres,$protocolo['nombre']);
	}

	if($datos){
		$valores=consultaArray("SELECT codigoProtocolo FROM protocolos_asociados_protocolizacion WHERE codigoProtocolizacion=".$datos['codigo']);
	}

	cierraBD();

	campoCheck('checkProtocolos','Protocolos aplicables',$valores,$nombres,$codigos,true,1);
	campoOculto(count($codigos),'numeroProtocolos');//Para poder hacer un for sin usar isset en la inserción/actualización
}

function campoTextoOtraPeriodicidad($datos){
	echo '<div id="divPeriodicidad" class="hide">';

	campoTexto('otraPeriodicidad','Indique periodicidad',$datos);

	echo '</div>';
}


function obtienePuestosProtocolizacionCliente(){
	$res=array(
		'puestos'	=>	"<option value='NULL'></option>"
	);

	$datos=arrayFormulario();
	extract($datos);

	//Aunque la tabla de evaluacion_general tiene un codigoPuestoTrabajo, ese código hace referencia al de la tabla funciones_formulario_prl, por eso hago la siguiente consulta:

	conexionBD();
	$consulta=consultaBD("SELECT puestos_trabajo.codigo, puestos_trabajo.nombre
						  FROM funciones_formulario_prl INNER JOIN empleados ON funciones_formulario_prl.codigoEmpleado=empleados.codigo 
						  INNER JOIN puestos_trabajo ON empleados.codigoPuestoTrabajo=puestos_trabajo.codigo
						  WHERE empleados.codigoCliente='$codigoCliente'
						  GROUP BY puestos_trabajo.codigo
						  ORDER BY puestos_trabajo.nombre;");

	if(mysql_num_rows($consulta)==0){//Hago esta comprobación para los casos de clientes que no tienen Evaluación de Riesgos (de puestos) hecha
		$consulta=consultaBD("SELECT codigo, nombre FROM puestos_trabajo ORDER BY nombre");
	}
	cierraBD();

	while($datos=mysql_fetch_assoc($consulta)){
		$res['puestos'].="<option value='".$datos['codigo']."'>".$datos['nombre']."</option>";
	}

	echo json_encode($res);
}


function obtieneRiesgosProtocolizacionPuesto(){
	$res=array(
		'riesgos'	=>	""
	);

	$datos=arrayFormulario();
	extract($datos);

	$consulta=consultaBD("SELECT descripcionRiesgo 
						  FROM riesgos_evaluacion_general 
						  WHERE descripcionRiesgo!='' AND 
						  codigoEvaluacion IN(
						  	
						  	SELECT codigo FROM evaluacion_general WHERE codigoCliente='$codigoCliente' AND codigoPuestoTrabajo IN(
						  		
						  		SELECT funciones_formulario_prl.codigo
								FROM funciones_formulario_prl INNER JOIN empleados ON funciones_formulario_prl.codigoEmpleado=empleados.codigo 
								WHERE empleados.codigoCliente='$codigoCliente' AND empleados.codigoPuestoTrabajo='$codigoPuesto'

						  	)

						  ) 
						  ORDER BY descripcionRiesgo;",true);

	while($datos=mysql_fetch_assoc($consulta)){
		$res['riesgos'].=trim($datos['descripcionRiesgo'])."\n";
	}

	if($res['riesgos']!=''){
		$res['riesgos']=quitaUltimaComa($res['riesgos']);
	}


	echo json_encode($res);
}



function imprimeProtocolizaciones($eliminado='NO'){
	global $_CONFIG;
	$consulta=consultaBD("SELECT protocolizacion.codigo, puestos_trabajo.nombre AS puestoBase, nombrePuesto, clientes.EMPID, clientes.EMPNOMBRE, resumenRiesgos, obligatoriedad, periodicidad, otraPeriodicidad, apto, sensibles, clientes.codigo AS codigoCliente 
						  FROM protocolizacion LEFT JOIN puestos_trabajo ON protocolizacion.codigoPuestoTrabajo=puestos_trabajo.codigo 
						  LEFT JOIN clientes ON protocolizacion.codigoCliente=clientes.codigo
						  WHERE protocolizacion.eliminado='$eliminado';",true);

	while($datos=mysql_fetch_assoc($consulta)){
		$protocolos='';
		$listado=consultaBD('SELECT protocolos.nombre FROM protocolos_asociados_protocolizacion INNER JOIN protocolos ON protocolos_asociados_protocolizacion.codigoProtocolo=protocolos.codigo WHERE codigoProtocolizacion='.$datos['codigo'],true);
		while($item=mysql_fetch_assoc($listado)){
			if($protocolos!=''){
				$protocolos.='<br/>';
			}
			$protocolos.=$item['nombre'];
		}
		$periodicidad=$datos['periodicidad']=='Otra'?$datos['otraPeriodicidad']:$datos['periodicidad'];
		echo "
			<tr>
				<td>".$datos['EMPID']." - ".$datos['EMPNOMBRE']."</td>
				<td>".$datos['nombrePuesto']."</td>
				<td>".nl2br($datos['resumenRiesgos'])."</td>
				<td>".nl2br($protocolos)."</td>
				<td class='centro'>".$datos['obligatoriedad']."<br/>".$periodicidad."</td>
				<td class='centro'>".$datos['apto']."<br/>".nl2br($datos['sensibles'])."</td>
				<td class='centro'>
					<div class='btn-group centro'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."protocolizacion/gestion.php?codigo=".$datos['codigo']."' class='noAjax'><i class='icon-search-plus'></i> Detalles</i></a></li> 
						    <li class='divider'></li>
						    <li><a href='".$_CONFIG['raiz']."protocolizacion/generaDocumento.php?codigo=".$datos['codigo']."' target='_blank' class='noAjax'><i class='icon-download'></i> Descargar<br/>por puesto</li> 
						    <li class='divider'></li>
						    <li><a href='".$_CONFIG['raiz']."protocolizacion/generaDocumento.php?codigo=".$datos['codigo']."&empresa' target='_blank' class='noAjax'><i class='icon-download'></i> Descargar<br/>por empresa</li> 
						</ul>
					</div>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
	        </tr>";
	}
}

function eliminaProtocolizaciones($valor){
    $res=true;
    $datos=arrayFormulario();

    conexionBD();
    for($i=0;isset($datos['codigo'.$i]);$i++){
        $res=$res && cambiaValorCampo('eliminado',$valor,$datos['codigo'.$i],'protocolizacion',false);
    }
    cierraBD();

    return $res;
}

function generaProtocolizacion($codigo,$empresa=false){
	conexionBD();

	$datos=consultaBD('SELECT protocolizacion.codigo, EMPRL, EMPNOMBRE, EMPDIR, EMPLOC, nombrePuesto, resumenRiesgos, obligatoriedad, periodicidad, otraPeriodicidad, apto, sensibles, clientes.codigo AS codigoCliente FROM protocolizacion INNER JOIN clientes ON protocolizacion.codigoCliente=clientes.codigo WHERE protocolizacion.codigo='.$codigo,false,true);
	$periodicidad=$datos['periodicidad']=='Otra'?$datos['otraPeriodicidad']:$datos['periodicidad'];

	$listado=array();
	if($empresa){
		$consulta=consultaBD('SELECT protocolizacion.codigo, nombrePuesto, resumenRiesgos, obligatoriedad, periodicidad, otraPeriodicidad, apto, sensibles FROM protocolizacion WHERE protocolizacion.codigoCliente='.$datos['codigoCliente']);
		while($item=mysql_fetch_assoc($consulta)){
			$item=creaItemProtocolizacion($item);
			array_push($listado, $item);
		}
	} else {
		$item=creaItemProtocolizacion($datos);
		array_push($listado, $item);
	}

	cierraBD();


    $contenido = "
	<style type='text/css'>
	<!--

		.cabecera{
			text-align:center;
			margin-top:0px;
			width:94%;
		}

		.logo{
			width:80px;
			position:absolute;
			left:50px;
			top:20px;
		}

		.cajaCliente{
			text-align:right;
			width:70%;
			margin-left:140px;
			margin-top:30px;
		}

		.contenedor{
			width:70%;
			margin-left:140px;
			text-align:justify;
			margin-top:60px;
			font-size:14px;
		}

		.cajaFirma{
			text-align:center;
			width:70%;
			margin-left:140px;
			margin-top:30px;
		}

		.cajaFirma img{
			width:200px;
		}

		.proto{
			width:98%;
			margin-left:10px;
			margin-top:10px;
			border:1px solid #000;
			border-collapse:collapse;
		}

		.proto th,.proto td{
			border:1px solid #000;
			padding:3px;
		}

		.proto th{
			background:#0062AD;
			color:#FFF;
			text-align:center;
		}

		.a10{
			width:10%;
		}
		.a20{
			width:20%;
		}

		.centro{
			text-align:center;
		}

	-->
	</style>
	<page>
		<div class='cabecera'>
			<img src='../img/logo.png' class='logo' />
		</div>
		<div class='cajaCliente'>
		".$datos['EMPRL']."<br/>
		".$datos['EMPNOMBRE']."<br/>
		".$datos['EMPDIR']."<br/>
		".$datos['EMPLOC']."<br/><br/><br/>
		Fecha: ".fecha()."
		</div>
		<div class='contenedor'>
		Estimados Sres.:<br/><br/>
		En relación al contrato de Medicina del Trabajo suscrito con Vds., les adjuntamos el documento de protocolización por puesto de trabajo en base a los riesgos detectados en la Evaluación de Riesgos de su empresa.<br/><br/>
		En base a lo dispuesto en el artículo 18 de la Ley 31/1995 de Prevención de Riesgos Laborales sobre información, consulta y participación de los trabajadores, rogamos hagan llegar este documento a los mismos o a sus representantes.<br/><br/>
		Quedamos a su disposición para cualquier consulta que deseen realizar.<br/><br/>
		Atentamente
		</div>
		<div class='cajaFirma'>
			<img src='../img/firmaAnesco.png' class='firma' /><br/><br/>
			<b>Dpto. Vigilancia Salud<br/>ANESCO</b>
		</div>
	</page>
	<page orientation='landscape'>
	<div style='margin-top:30px;margin-left:40px;'>
		<i>Protocolización efectuada a partir de la ER facilitada por tecnico de Anesco y CLC</i>
	</div>
	<table class='proto'>
		<tr>
			<th class='a20'>Puesto</th>
			<th class='a20'>Riesgos relevantes para V. de la Salud</th>
			<th class='a20'>Protocolo MSC</th>
			<th class='a20'>Analítica y / o Pruebas<br/>Consult. Externas</th>
			<th class='a10'>Obligatoriedad<br/>Periodicidad</th>
			<th class='a10'>APTO<br/>T. Sensibles</th>
		</tr>";
	foreach ($listado as $item) {
		$contenido.="
		<tr>
			<td class='a20 centro'>".$item['nombrePuesto']."</td>
			<td class='a20'>".nl2br($item['resumenRiesgos'])."</td>
			<td class='a20'>".nl2br($item['protocolos'])."</td>
			<td class='a20'>".$item['pruebas']."</td>
			<td class='a10 centro'>".$item['obligatoriedad']."<br/>".$item['periodicidad']."</td>
			<td class='a10 centro'>".$item['apto']."<br/>".nl2br($item['sensibles'])."</td>
		</tr>";
	}
	$contenido.="
	</table>
	</page>";

	return $contenido;
}

function creaItemProtocolizacion($datos){
	$item=array('nombrePuesto'=>$datos['nombrePuesto'],'resumenRiesgos'=>$datos['resumenRiesgos'],'obligatoriedad'=>$datos['obligatoriedad'],'apto'=>$datos['apto'],'sensibles'=>$datos['sensibles']);
	$item['periodicidad']=$datos['periodicidad']=='Otra'?$datos['otraPeriodicidad']:$datos['periodicidad'];
	$item['pruebas']=obtienePruebasProtocolizacion($datos['codigo']);
	$item['protocolos']=obtieneProtocolos($datos['codigo']);
	return $item;
}


function obtienePruebasProtocolizacion($codigoProtocolizacion){
	$res='';

	$consulta=consultaBD("SELECT desplegables.texto FROM desplegables INNER JOIN pruebas_protocolizacion ON desplegables.codigo=pruebas_protocolizacion.texto WHERE codigoProtocolizacion='$codigoProtocolizacion';");
	while($datos=mysql_fetch_assoc($consulta)){
		$res.=$datos['texto'].'<br />';
	}

	if($res!=''){
		$res=quitaUltimaComa($res,6);
	}

	return $res;
}

function obtieneProtocolos($codigo){
	$protocolos='';
	$listado=consultaBD('SELECT protocolos.nombre FROM protocolos_asociados_protocolizacion INNER JOIN protocolos ON protocolos_asociados_protocolizacion.codigoProtocolo=protocolos.codigo WHERE codigoProtocolizacion='.$codigo);
	while($item=mysql_fetch_assoc($listado)){
		if($protocolos!=''){
			$protocolos.='<br/>';
		}
		$protocolos.=$item['nombre'];
	}
	return $protocolos;
}


function creaTablaPruebas($datos){
        echo "
        <div class='control-group'>                     
            <label class='control-label'>Estudio de pruebas funcionales:</label>
            <div class='controls'>
                <table class='tabla-simple' id='tablaPruebas'>
                    <thead>
                        <tr>
                            <th> Otras pruebas </th>
                            <th> Valoración </th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>";
            
                $i=0;

                conexionBD();

                if($datos!=false){
                    $consulta=consultaBD("SELECT * FROM pruebas_protocolizacion WHERE codigoProtocolizacion='".$datos['codigo']."';");
                    while($item=mysql_fetch_assoc($consulta)){
                        imprimeLineaTablaPruebas($i,$item);
                        $i++;
                    }
                }
                
                if($i==0){
                    imprimeLineaTablaPruebas(0);
                }

                cierraBD();
          
        echo "      </tbody>
                </table>
                <button type='button' class='btn btn-small btn-success' onclick='insertaPrueba(\"tablaPruebas\");'><i class='icon-plus'></i> Añadir Prueba</button> 
                <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaPruebas\");'><i class='icon-trash'></i> Eliminar Prueba</button>
            </div>
        </div>";
}

function imprimeLineaTablaPruebas($i,$datos=false){
    $j=$i+1;

    echo "<tr>";
            
            campoSelectDesplegable('otrasPruebas_'.$i,'',$datos['texto'],'selectpicker span5 show-tick selectInserccion',"data-live-search='true'",'',1,false);
            campoTextoSimbolo('valoracionEconomica'.$i,'','€',formateaNumeroWeb($datos['valoracionEconomica']),'input-mini pagination-right',1);

    echo "<td><input type='checkbox' name='filasTabla[]' value='$j' /></td>
    </tr>";
}

//Fin parte de gestión de protocolización