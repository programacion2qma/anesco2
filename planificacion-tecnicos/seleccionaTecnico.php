<?php
  $seccionActiva=9;
  include_once("../cabecera.php");
?>
<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

         <div class="span12">
          <div class="widget cajaSelect">
            <div class="widget-header"> <i class="icon-file-text"></i>
              <h3>Selecciona técnico</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content centro">
              <h3>Seleccione el técnico:</h3><br><br>
				      <form action='index.php' method='post'>
      					<div class='control-group'>                     
      					  <div class='controls'>
                    <?php
                    campoSelectConsulta('codigoUsuarioTecnico','Técnico/Enfermera','SELECT codigo, CONCAT(nombre, " ", apellidos) AS texto FROM usuarios WHERE tipo IN("TECNICO","ENFERMERIA","MEDICO");');
                    ?>                              
      					  </div> <!-- /controls -->       
      					</div> <!-- /control-group -->

      					<br />
					
                <!--<a href='../inicio.php' class='btn btn-default'><i class='icon-chevron-left'></i> Volver </a>-->
    					  <button type="submit" class="btn btn-propio">Seleccionar <i class="icon-chevron-right"></i></button>
    				  </form>
            </div>
            <!-- /widget-content --> 
           </div>
          </div>
    </div>
		
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker();
  });
</script>

</div>

<?php include_once('../pie.php'); ?>