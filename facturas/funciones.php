<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de facturas


function operacionesFacturas(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaFactura();
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=creaFactura();
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoFacturas($_POST['elimina']);
		//$res=eliminaDatos('facturas');
	}
	elseif(isset($_POST['codigoFactura0'])){
		$res=actualizaFacturacionPendientes();
	}
	elseif(isset($_GET['suspender'])){
		$res=cambiaValorCampo('suspendido',$_GET['suspender'],$_GET['codigoCliente'],'clientes');
	}
	/*elseif(isset($_GET['codigoContrato'])){
		$res=creaFacturaContrato();
	}*/

	mensajeResultado('codigoCliente',$res,'Factura');
    mensajeResultado('elimina',$res,'Factura', true);
}

/*function creaFacturaContrato(){
	$res=true;
	$contrato=consultaBD('SELECT contratos.codigo, ofertas.codigoCliente, ofertas.subtotal, ofertas.total FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo WHERE contratos.codigo='.$_GET['codigoContrato'],true,true);
	$res=consultaBD('UPDATE contratos SET aceptado="SI" WHERE codigo='.$contrato['codigo'],true);
	$emisor=consultaBD('SELECT * FROM emisores');
	$emisor=mysql_fetch_assoc($emisor);
	$serie=consultaBD('SELECT * FROM series_facturas');
	$serie=mysql_fetch_assoc($serie);
	$numero=consultaNumeroSerieFactura2($serie['codigo']);
	$res=consultaBD("INSERT INTO facturas(codigo,fecha,codigoCliente,tipoFactura,baseImponible,total,activo,codigoEmisor,codigoSerieFactura,numero) VALUES(NULL,'".date('Y-m-d')."','".$contrato['codigoCliente']."','NORMAL','".$contrato['subtotal']."','".$contrato['total']."','SI','".$emisor['codigo']."','".$serie['codigo']."','".$numero."');",true);
	$codigoFactura=mysql_insert_id();
	$res=consultaBD("INSERT INTO contratos_en_facturas VALUES(NULL,".$contrato['codigo'].",".$codigoFactura.")",true);
	return $res;
}*/

function creaFactura(){
	formateaImportesFacturas();
	$res=insertaDatos('facturas');
	
	if($res){
		$res=insertaCamposAdicionalesFactura($res);
	}

	return $res;
}

function actualizaFactura(){
	formateaImportesFacturas();
	$res=actualizaDatos('facturas');
	
	if($res){
		$res=insertaCamposAdicionalesFactura($_POST['codigo'],true);
	}

	return $res;
}

function insertaCamposAdicionalesFactura($codigoFactura,$actualizacion=false){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	if($actualizacion && $datos['tipoFactura']!='LIBRE'){
		$res=$res && consultaBD("DELETE FROM contratos_en_facturas WHERE codigoFactura=$codigoFactura");
	}

	$res=$res && insertaVencimientosFactura($datos,$codigoFactura);
	$res=$res && insertaCobrosFactura($datos,$codigoFactura);

	if($datos['tipoFactura']!='LIBRE'){
		$res=$res && insertaConceptosFactura($datos,$codigoFactura);
		$res=$res && creaPlanificacion($codigoFactura);
	}
	
	cierraBD();

	return $res;
}

function insertaConceptosFactura($datos,$codigoFactura){
	$res=true;

	for($i=0;isset($datos['codigoContrato'.$i]);$i++){
		$codigoContrato=$datos['codigoContrato'.$i];

		$res=$res && consultaBD("INSERT INTO contratos_en_facturas VALUES(NULL,$codigoContrato,$codigoFactura);");
	}

	return $res;
}


function insertaCobrosFactura($datos,$codigoFactura){
	$res=true;

	for($i=0;isset($datos["codigoCobro$i"]) || isset($datos["codigoFormaPagoCobro$i"]);$i++){
		$codigoFormaPagoCobro=$datos['codigoFormaPagoCobro'.$i];
		$fechaCobro=$datos['fechaCobro'.$i];
		$importeCobro=formateaNumeroWeb($datos['importeCobro'.$i],true);
		$codigoCuentaPropia=$datos['codigoCuentaPropia'.$i];

		if(isset($datos['codigoCobro'.$i]) && isset($datos['codigoFormaPagoCobro'.$i])){//Actualización
			$codigoCobro=$datos['codigoCobro'.$i];

			$res=$res && consultaBD("UPDATE cobros_facturas SET codigoFormaPago=$codigoFormaPagoCobro, fechaCobro='$fechaCobro', importe='$importeCobro', codigoCuentaPropia=$codigoCuentaPropia WHERE codigo=$codigoCobro");
		}
		elseif(!isset($datos['codigoCobro'.$i]) && isset($datos['codigoFormaPagoCobro'.$i])){//Inserción
			$res=$res && consultaBD("INSERT INTO cobros_facturas VALUES(NULL,$codigoFactura,$codigoFormaPagoCobro,'$fechaCobro',$importeCobro,$codigoCuentaPropia,NULL);");
		}
		else{//Eliminación
			$res=$res && consultaBD("DELETE FROM cobros_facturas WHERE codigo='".$datos['codigoCobro'.$i]."';");
		}
	}

	return $res;
}

function cambiaEstadoEliminadoFacturas($estado){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],'facturas',false);
	}
	cierraBD();

	return $res;
}

function estadisticasFacturasEliminadas($anio=false){

	if($anio==false){
		$whereAnio=obtieneWhereEjercicioEstadisticas('facturas.fecha');
		$res=estadisticasGenericas('facturas',false,"tipoFactura='NORMAL' AND eliminado='SI' ".$whereAnio);		
	}else{
		$res=estadisticasGenericas('facturas',false,"tipoFactura='NORMAL' AND YEAR(fecha)='$anio' AND eliminado='SI'");
	}


	return $res;
}


function imprimeFacturas($anio=''){
	$whereAnio=obtieneWhereEjercicioEstadisticas('facturas.fecha');
	if($anio!=''){
		$whereAnio="AND facturas.fecha LIKE '$anio-%'";
	}

	
	$consulta=consultaBD("SELECT facturas.codigo, facturas.fecha, serie, facturas.numero, CONCAT(clientes.EMPNOMBRE,' (',clientes.EMPMARCA,')') AS cliente, clientes.EMPCIF AS cif, facturas.baseImponible, 
						  facturas.total, facturas.activo, facturas.facturaCobrada,
						  cobros_facturas.codigo AS codigoCobro, clientes.codigo AS codigoCliente, abonos.codigo AS codigoAbono, clientes.EMPCP, clientes.EMPID, contratos.fechaInicio, contratos.codigoInterno, contratos.enVigor, contratos.suspendido
						  FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
						  LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
						  LEFT JOIN cobros_facturas ON facturas.codigo=cobros_facturas.codigoFactura AND cobros_facturas.importe>0
						  LEFT JOIN facturas AS abonos ON facturas.codigo=abonos.codigoFacturaAsociada AND abonos.tipoFactura='ABONO'
						  LEFT JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
						  INNER JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
						  WHERE facturas.tipoFactura!='ABONO' AND facturas.tipoFactura!='PROFORMA' AND facturas.eliminado='NO' $whereAnio
						  GROUP BY facturas.codigo",true);

	$iconosEstado=array(
					'SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>',
					'PARCIAL'=>'<i class="icon-adjust iconoFactura icon-warning-color"></i>',
					'NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		$iconoAbono=obtieneIconoAbono($datos['codigoAbono']);
		$cobrada = $iconosEstado[obtienePagada($datos)];
		$textoEnvia='Enviar factura';
		$envio=consultaBD('SELECT COUNT(codigo) AS total FROM facturas_envios WHERE codigoFactura='.$datos['codigo'],true,true);
		if($envio['total']>0){
			$textoEnvia='Reenviar factura';
		}
		$vigor=$datos['enVigor']=='SI'?'En vigor':'Sin vigor';
		$concepto='Contrato Nº '.formateaReferenciaContrato($datos,$datos).' ('.$vigor.')';
		$suspendido=obtieneBotonSuspendido($datos);
		echo "<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['serie']."</td>
				<td>".$datos['numero']."</td>
				<td><a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."' class='noAjax' target='_blank'>".$datos['cliente']."</a></td>
				<td>".$datos['cif']."</td>
				<td class='nowrap pagination-right'>".formateaNumeroWeb($datos['baseImponible'])." €</td>
				<td class='nowrap pagination-right'>".$iconoAbono." ".formateaNumeroWeb($datos['total'])." €</td>
				<td class='centro'>".$concepto."</td>
				<td class='centro'>".$cobrada."</td>
				".botonAccionesConClase(array('Detalles','Descargar factura',$textoEnvia,'Generar abono'),array('facturas/gestion.php?codigo='.$datos['codigo'],'facturas/generaFactura.php?codigo='.$datos['codigo'],'facturas/generaFactura.php?envio&codigo='.$datos['codigo'],'abonos/gestion.php?codigoFactura='.$datos['codigo']),array("icon-search-plus",'icon-cloud-download','icon-envelope','icon-reply'),array(0,1,0,0,$suspendido['enlaceExterno']),true,'',array('','','enviaFactura noAjax',''))."
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
}

function imprimeFacturasEliminadas($anio=false){
	$whereAnio=obtieneWhereEjercicioEstadisticas('facturas.fecha');
	if($anio!=''){
		$whereAnio="AND facturas.fecha LIKE '$anio-%'";
	}

	
	$consulta=consultaBD("SELECT facturas.codigo, facturas.fecha, serie, facturas.numero, clientes.EMPNOMBRE AS cliente, clientes.EMPCIF AS cif, facturas.baseImponible, 
						  facturas.total, facturas.activo, facturas.facturaCobrada,
						  cobros_facturas.codigo AS codigoCobro, clientes.codigo AS codigoCliente, abonos.codigo AS codigoAbono, clientes.EMPCP, clientes.EMPID, contratos.fechaInicio, contratos.codigoInterno, contratos.enVigor
						  FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
						  LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
						  LEFT JOIN cobros_facturas ON facturas.codigo=cobros_facturas.codigoFactura AND cobros_facturas.importe>0
						  LEFT JOIN facturas AS abonos ON facturas.codigo=abonos.codigoFacturaAsociada AND abonos.tipoFactura='ABONO'
						  LEFT JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
						  INNER JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
						  WHERE facturas.tipoFactura!='ABONO' AND facturas.tipoFactura!='PROFORMA' AND facturas.eliminado='SI'  $whereAnio
						  GROUP BY facturas.codigo",true);

	$iconosEstado=array(
					'SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>',
					'PARCIAL'=>'<i class="icon-adjust iconoFactura icon-warning-color"></i>',
					'NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		$iconoAbono=obtieneIconoAbono($datos['codigoAbono']);
		$cobrada = $iconosEstado[obtienePagada($datos)];
		$concepto='Contrato Nº '.formateaReferenciaContrato($datos,$datos);
		echo "<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['serie']."</td>
				<td>".$datos['numero']."</td>
				<td><a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."' class='noAjax' target='_blank'>".$datos['cliente']."</a></td>
				<td>".$datos['cif']."</td>
				<td class='nowrap pagination-right'>".formateaNumeroWeb($datos['baseImponible'])." €</td>
				<td class='nowrap pagination-right'>".$iconoAbono." ".formateaNumeroWeb($datos['total'])." €</td>
				<td class='centro'>".$concepto."</td>
				<td class='centro'>".$cobrada."</td>
				".botonAcciones(array('Detalles','Descargar factura','Generar abono'),array('facturas/gestion.php?codigo='.$datos['codigo'],'facturas/generaFactura.php?codigo='.$datos['codigo'],'abonos/gestion.php?codigoFactura='.$datos['codigo']),array("icon-search-plus",'icon-cloud-download','icon-reply'),array(0,1,0),true,'')."
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
}

function imprimeFacturasDeBaja($anio=false){
	$whereAnio=obtieneWhereEjercicioEstadisticas('facturas.fecha');
	if($anio!=''){
		$whereAnio="AND facturas.fecha LIKE '$anio-%'";
	}

	
	$consulta=consultaBD("SELECT facturas.codigo, facturas.fecha, serie, facturas.numero, CONCAT(clientes.EMPNOMBRE,' (',clientes.EMPMARCA,')') AS cliente, clientes.EMPCIF AS cif, facturas.baseImponible, 
						  facturas.total, facturas.activo, facturas.facturaCobrada,
						  cobros_facturas.codigo AS codigoCobro, clientes.codigo AS codigoCliente, abonos.codigo AS codigoAbono, clientes.EMPCP, clientes.EMPID, contratos.fechaInicio, contratos.codigoInterno, contratos.enVigor
						  FROM vencimientos_facturas 
						  INNER JOIN facturas ON vencimientos_facturas.codigoFactura=facturas.codigo
						  LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
						  LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
						  LEFT JOIN cobros_facturas ON facturas.codigo=cobros_facturas.codigoFactura AND cobros_facturas.importe>0
						  LEFT JOIN facturas AS abonos ON facturas.codigo=abonos.codigoFacturaAsociada AND abonos.tipoFactura='ABONO'
						  LEFT JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
						  INNER JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
						  WHERE facturas.tipoFactura!='ABONO' AND facturas.eliminado='NO' AND codigoEstadoRecibo=6 $whereAnio
						  GROUP BY facturas.codigo",true);

	$iconosEstado=array(
					'SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>',
					'PARCIAL'=>'<i class="icon-adjust iconoFactura icon-warning-color"></i>',
					'NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		$iconoAbono=obtieneIconoAbono($datos['codigoAbono']);
		$cobrada = $iconosEstado[obtienePagada($datos)];
		$concepto='Contrato Nº '.formateaReferenciaContrato($datos,$datos);
		echo "<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['serie']."</td>
				<td>".$datos['numero']."</td>
				<td><a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."' class='noAjax' target='_blank'>".$datos['cliente']."</a></td>
				<td>".$datos['cif']."</td>
				<td class='nowrap pagination-right'>".formateaNumeroWeb($datos['baseImponible'])." €</td>
				<td class='nowrap pagination-right'>".$iconoAbono." ".formateaNumeroWeb($datos['total'])." €</td>
				<td class='centro'>".$concepto."</td>
				<td class='centro'>".$cobrada."</td>
				".botonAcciones(array('Detalles','Descargar factura','Generar abono'),array('facturas/gestion.php?codigo='.$datos['codigo'],'facturas/generaFactura.php?codigo='.$datos['codigo'],'abonos/gestion.php?codigoFactura='.$datos['codigo']),array("icon-search-plus",'icon-cloud-download','icon-reply'),array(0,1,0),true,'')."
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
}




function listadoFacturas(){

	$columnas=array(
		'fecha',
		'serie',
		'numero',
		'cliente',
		'cif',
		'baseImponible',
		'total',
		'codigoInterno',
		'facturaCobrada',
		'fecha',
		'codigoSerieFactura'
	);
	
	$iconosEstado=array(
		'SI'		=>	'<i class="icon-check-circle iconoFactura icon-success"></i>',
		'PARCIAL'	=>	'<i class="icon-adjust iconoFactura icon-warning-color"></i>',
		'NO'		=>	'<i class="icon-times-circle iconoFactura icon-danger"></i>'
	);

	$whereAnio=obtieneWhereEjercicioEstadisticas('fecha');
	$having1=obtieneWhereListado("HAVING tipoFactura!='ABONO' AND tipoFactura!='PROFORMA' AND eliminado='NO' $whereAnio",$columnas);
	$having2=obtieneWhereListado("HAVING eliminado='NO' $whereAnio",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();	


	$query="SELECT facturas.codigo, facturas.fecha, serie, facturas.numero, CONCAT(clientes.EMPNOMBRE,' (',clientes.EMPMARCA,')') AS cliente, clientes.EMPCIF AS cif, 
			facturas.baseImponible, facturas.total, facturas.activo, facturas.facturaCobrada,
		    cobros_facturas.codigo AS codigoCobro, clientes.codigo AS codigoCliente, abonos.codigo AS codigoAbono, clientes.EMPCP, clientes.EMPID, 
		    contratos.fechaInicio, contratos.codigoInterno, contratos.enVigor, contratos.suspendido, facturas.tipoFactura, facturas.eliminado, facturas.codigoSerieFactura,
		    contratos.eliminado AS contratoEliminado

		    FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
		    LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
		    LEFT JOIN cobros_facturas ON facturas.codigo=cobros_facturas.codigoFactura AND cobros_facturas.importe>0
		    LEFT JOIN facturas AS abonos ON facturas.codigo=abonos.codigoFacturaAsociada AND abonos.tipoFactura='ABONO'
		    LEFT JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
		    LEFT JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
		    
		    GROUP BY facturas.codigo

		    $having1

		    UNION ALL

		    SELECT facturas_reconocimientos_medicos.codigo, facturas_reconocimientos_medicos.fecha, serie, facturas_reconocimientos_medicos.numero, 
		    IFNULL(CONCAT(clientes.EMPNOMBRE,' (',clientes.EMPMARCA,')'),CONCAT(clientes2.EMPNOMBRE,' (',clientes2.EMPMARCA,')')) AS cliente, 
		    IFNULL(clientes.EMPCIF,clientes2.EMPCIF) AS cif,  facturas_reconocimientos_medicos.total AS baseImponible, facturas_reconocimientos_medicos.total, 'SI' AS activo, 
			facturas_reconocimientos_medicos.facturaCobrada, NULL AS codigoCobro, clientes.codigo AS codigoCliente, NULL AS codigoAbono, 
			clientes.EMPCP, clientes.EMPID, contratos.fechaInicio, contratos.codigoInterno, contratos.enVigor, contratos.suspendido, 'RECONOCIMIENTOS' AS tipoFactura, 
		    facturas_reconocimientos_medicos.eliminado, codigoSerieFactura, contratos.eliminado AS contratoEliminado

		    FROM facturas_reconocimientos_medicos LEFT JOIN contratos ON facturas_reconocimientos_medicos.codigoContrato=contratos.codigo
		    LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
		    LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo
		    LEFT JOIN clientes clientes2 ON facturas_reconocimientos_medicos.codigoClienteParaConceptoManual=clientes2.codigo
		    LEFT JOIN series_facturas ON facturas_reconocimientos_medicos.codigoSerieFactura=series_facturas.codigo
		    
		    GROUP BY facturas_reconocimientos_medicos.codigo

		    $having2";




	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){

		$datosExtra=obtieneDatosExtraFacturaListadoFacturas($datos,$iconosEstado);
		if($datos['tipoFactura']!='LIBRE'){
			$conceptos=obtieneConceptosFactura($datos['codigo'],$datos,'',$datos['total'],$datos['baseImponible']);
		}
		else{
			$conceptos=obtieneConceptosLibreFacturaPDF($datos);
		}

		// Para la comprobación, los valores no son exactamente los mismo, por lo que aumentamos la precision de la base imponible de conceptos
		// porque al hacerle formateaNumeroWeb lleva implicito un round
		// Esto se realiza en la comprobacion ya que una diferencia en el tercer decimal es posible y puede dar falsas alertas
		// Aun asi, en las factura, saldra saliendo la BI que es mas alta y redondeado a unas decimas.
		$baseImponibleConcepto = floor($conceptos['baseImponible']*100)/100;
		$baseTotalConcepto     = floor($conceptos['total']*100)/100;

		// Por ello, creamos dos campos extras de visualizacion, para segun cada caso, muestre el valor mas alto y si hay que hacerle alguna comprobación adicional
		$baseImponibleVista = $datos['baseImponible'];
		$totalVista 		= $datos['total'];

		$icono = "";
		if (($baseImponibleConcepto > $datos['baseImponible'] || $baseTotalConcepto > $datos['total']) && $_SESSION['ejercicio'] >= 2024) {
			$icono = '<i class="btn-danger btn btn-small icon-exclamation" title="Se ha detectado una anomalia entre totales y/o base imponible."></i>';
			$baseImponibleVista = $baseImponibleConcepto;
			$totalVista			= $baseTotalConcepto;
		}



		$fila=array(
			formateaFechaWeb($datos['fecha']),
			$datos['serie'].' '.$icono,
			$datos['numero'],
			"<a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."' class='noAjax' target='_blank'>".$datos['cliente']."</a>",
			$datos['cif'],
			formateaNumeroWeb($baseImponibleVista)." € ",
			$datosExtra['iconoAbono']." ".formateaNumeroWeb($totalVista)." €",
			$datosExtra['concepto'],
			"<div class='centro'>".$datosExtra['cobrada']."</div>",
			$datosExtra['botones'],
			$datosExtra['check'],
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	cierraBD();

	echo json_encode($res);
}



function obtieneDatosExtraFacturaListadoFacturas($datos,$iconosEstado){

	$iconoAbono=obtieneIconoAbono($datos['codigoAbono']);

	if($datos['tipoFactura']=='RECONOCIMIENTOS'){

		$cobrada = $iconosEstado[$datos['facturaCobrada']];//Las facturas de reconocimientos no tiene subtabla de cobros
		
		//Parte de comprobación de envío
		$textoEnvia='Enviar factura';
		$envio=consultaBD('SELECT COUNT(codigo) AS total FROM facturas_envios WHERE codigoFactura='.$datos['codigo'],false,true);
		if($envio['total']>0){
			$textoEnvia='Reenviar factura';
		}

		//Parte de concepto
		$concepto="Reconocimiento médico";

		//Definición de botones
		$botones=botonAccionesConClase(array('Detalles','Descargar factura',$textoEnvia),array('facturas-reconocimientos/gestion.php?codigo='.$datos['codigo'],'facturas-reconocimientos/generaFactura.php?codigo='.$datos['codigo'],'facturas-reconocimientos/generaFactura.php?envio&codigo='.$datos['codigo']),array("icon-search-plus",'icon-cloud-download','icon-envelope'),array(0,1,0),true,'',array('','','enviaFacturaReconocimiento noAjax'));

		$check='-';//Los reconocimientos se deben eliminar desde la subsección de facturas de reconocimientos
	}
	else{

		$cobrada = $iconosEstado[obtienePagada($datos,false)];
		
		$textoEnvia='Enviar factura';
		$envio=consultaBD('SELECT COUNT(codigo) AS total FROM facturas_envios WHERE codigoFactura='.$datos['codigo'],false,true);
		if($envio['total']>0){
			$textoEnvia='Reenviar factura';
		}

		if($datos['enVigor']=='SI' && $datos['contratoEliminado']=='NO'){
			$vigor='En vigor';
		}
		else{
			$vigor='Sin vigor';
		}

		$concepto='Contrato Nº '.formateaReferenciaContrato($datos,$datos).' ('.$vigor.')';

		$suspendido=obtieneBotonSuspendido($datos);

		$botones=botonAccionesConClase(array('Detalles','Descargar factura',$textoEnvia,'Generar abono'),array('facturas/gestion.php?codigo='.$datos['codigo'],'facturas/generaFactura.php?codigo='.$datos['codigo'],'facturas/generaFactura.php?envio&codigo='.$datos['codigo'],'abonos/gestion.php?codigoFactura='.$datos['codigo']),array("icon-search-plus",'icon-cloud-download','icon-envelope','icon-reply'),array(0,1,0,0,$suspendido['enlaceExterno']),true,'',array('','','enviaFactura noAjax',''));

		$check="<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' />";
	}

	return array(
		'iconoAbono'	=>	$iconoAbono,
		'cobrada'		=>	$cobrada,
		'concepto'		=>	$concepto,
		'botones'		=>	$botones,
		'check'			=>	$check
	);
}

function filtroFacturas(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(3,'Cliente',false,'span3');
	campoFecha(0,'Fecha desde');
	campoFecha(9,'Hasta');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectConsulta(10,'Serie',"SELECT codigo, serie AS texto FROM series_facturas WHERE serie!='A' ORDER BY serie",false,'selectpicker show-tick span1','');
	campoSelectSiNoFiltro(8,'Factura cobrada');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}











function estadisticasFacturasDeBaja($anio=false){
	$whereAnio=obtieneWhereEjercicioEstadisticas('facturas.fecha');
	$res=consultaBD('SELECT SUM(importe) AS total FROM vencimientos_facturas INNER JOIN facturas ON vencimientos_facturas.codigoFactura=facturas.codigo WHERE facturas.eliminado="NO" AND codigoEstadoRecibo="6" AND facturas.tipoFactura!="ABONO" '.$whereAnio,true,true);
	return $res;
}

function obtieneIconoAbono($codigoAbono){
	$res='';

	if($codigoAbono!=NULL){
		$res='<i class="icon-danger icon-exclamation-circle" title="Abono generado"></i>';
	}

	return $res;
}


function gestionFactura(){
	operacionesFacturas();

	abreVentanaGestion('Gestión de Facturas','index.php','span3','icon-edit','',false,'noAjax');
	if(isset($_GET['codigoFacturaProforma'])){
		$datos=datosRegistro('facturas',$_GET['codigoFacturaProforma']);
		$numero=consultaNumeroSerieFactura2($datos['codigoSerieFactura'],true,date('Y'));
		$tipo='NORMAL';
		$fecha=fechaBD();//Importante! Para que en los cambios de año, coja la numeración del año actual en que se confirma la factura, no al que pertenecía la proforma
		campoOculto($datos['codigo'],'codigo');
	} 
	else {
		$datos=compruebaDatos('facturas');
		$numero=$datos['numero'];
		$tipo=$datos['tipoFactura'];
		$fecha=$datos['fecha'];
	}

	compruebaAbono($datos);
	campoSelectConsulta('codigoCliente','Cliente',"SELECT codigo, CONCAT(EMPNOMBRE,' (',EMPMARCA,')') AS texto FROM clientes ORDER BY EMPNOMBRE",$datos);
	campoTexto('fecha','Fecha',formateaFechaWeb($fecha),'input-small datepicker hasDatepicker obligatorio');
	campoSelect('esModeloAntiguo','Modelo de facturacion',['Nuevo 2024','Antiguo 2023'],[true,false],true);

	/*campoSelectConsulta('codigoEmisor','Emisor','SELECT codigo, razonSocial AS texto FROM emisores WHERE activo="SI" ORDER BY razonSocial;',$datos,'selectpicker show-tick span3 obligatorio');*/

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectConsulta('codigoSerieFactura','Serie',"SELECT codigo, serie AS texto FROM series_facturas WHERE activo='SI' ORDER BY serie",$datos,'span1 selectpicker show-tick');

	if($_SESSION['codigoU']==147 || $_SESSION['codigoU']==152 || $_SESSION['codigoU']==1913){//soporte, Patricio y Marta pueden editar, temporalmente, los números de factura
		campoTexto('numero','Número',$numero,'input-mini pagination-right obligatorio');	
	}
	else{
		campoTexto('numero','Número',$numero,'input-mini pagination-right obligatorio',true);
	}
	

	cierraColumnaCampos();
	abreColumnaCampos('sinFlotar');
	cierraColumnaCampos();
	abreColumnaCampos('span11');

	tablaEnviosFacturas($datos);
	imprimeConceptosFactura($datos);

	cierraColumnaCampos();
	abreColumnaCampos('span5');

	campoTextoSimbolo('baseImponible','Base imponible','€',formateaNumeroWeb($datos['baseImponible']));
	campoTextoSimbolo('total','Total','€',formateaNumeroWeb($datos['total']),'input-mini pagination-right obligatorio');

	cierraColumnaCampos();
	abreColumnaCampos();

	areaTexto('comentariosLlamada','Comentarios de llamada',$datos);
	
	campoOculto($datos,'activo','SI');
	campoOculto('NULL','codigoFacturaAsociada');//Este campo es exclusivo para los abonos
	campoOculto($tipo,'tipoFactura','LIBRE');
	campoOculto($datos,'facturaCobrada','NO');
	campoOculto($datos,'informeEnviado','NO');
	campoOculto($datos,'eliminado','NO');
	campoOculto($datos,'codigoUsuarioFactura',$_SESSION['codigoU']);

	cierraColumnaCampos();
	abreColumnaCampos('span11');

	areaTexto('observaciones','Observaciones',$datos,'observacionesParaFactura');
	creaTablaVencimientos($datos);
	creaTablaCobros($datos);

	cierraVentanaGestion('index.php',true);
}

function compruebaAbono($datos){
	if($datos){
		$abono=consultaBD("SELECT codigo FROM facturas WHERE codigoFacturaAsociada=".$datos['codigo']." AND tipoFactura='ABONO'",true,true);
		if($abono){
			campoDato('Abono generado',"<a href='../abonos/gestion.php?codigo=".$abono['codigo']."' class='btn btn-small btn-danger noAjax' target='_blank'><i class='icon-exclamation-circle'></i> Ver abono</a>");
		}
	}
}


/*
	Reglas para obtener el vencimiento:
	+ A Bonificación: Ej: Si un curso finaliza el mes de Mayo, independientemente del día, tendría que aparecer la fecha 01 de Julio. 
	+ Quincenas: Si la factura tiene fecha del 01 al 15 el vencimiento es el 24 del mismo mes. 
	             Si la factura tiene fecha del 16 al 31, el vencimiento es el 09 del mes siguiente.
	+ Día 1 pasados 2 meses: Si la factura tiene fecha 28/06 sería el 1 de Septiembre.
	+ En la fecha: se define directamente la fecha del vencimiento.
*/
function obtieneVencimientoConcepto($tipoVencimiento,$fecha,$fechaFacturacion=false,$bdd=false){

	if($tipoVencimiento=='EN LA FECHA'){
		$arrayFecha=explode('-',$fechaFacturacion);
	}
	else{
		$arrayFecha=explode('-',$fecha);

		if($tipoVencimiento=='A BONIFICACIÓN'){
			$arrayFecha=sumaMesFecha($arrayFecha,2);
			$arrayFecha[2]='01';
		}
		elseif($tipoVencimiento=='QUINCENAS' && $arrayFecha[2]<16){
			$arrayFecha[2]='24';
		}
		elseif($tipoVencimiento=='QUINCENAS' && $arrayFecha[2]>15){
			$arrayFecha=sumaMesFecha($arrayFecha,1);
			$arrayFecha[2]='09';
		}
		else{
			$arrayFecha=sumaMesFecha($arrayFecha,3);
			$arrayFecha[2]='01';
		}
	}

	if(!$bdd){
		$res=$arrayFecha[2].'/'.$arrayFecha[1].'/'.$arrayFecha[0];
	}
	else{
		$res=$arrayFecha[0].'-'.$arrayFecha[1].'-'.$arrayFecha[2];
	}

	return $res;
}

function sumaMesFecha($arrayFecha,$suma){
	$arrayFecha[1]+=$suma;

	if($arrayFecha[1]>12){
		$arrayFecha[1]-=12;
		$arrayFecha[0]+=1;
	}

	if($arrayFecha[1]<10){
		$arrayFecha[1]='0'.$arrayFecha[1];
	}

	return $arrayFecha;
}


function imprimeConceptosFactura($datos){
	if($datos && $datos['tipoFactura']!='LIBRE'){
		creaTablaConceptos($datos);
		campoOculto('','conceptoManualFactura');
	}
	else{
		areaTexto('conceptoManualFactura','Concepto/s',$datos,'areaInforme');
	}
}

function creaTablaConceptos($datos){
	echo "
	<div class='control-group'>                     
		<label class='control-label'>Conceptos:</label>
	    <div class='controls'>
			<table class='table table-striped tabla-simple mitadAncho' id='tablaConceptos'>
			  	<thead>
			    	<tr>
			            <th> Contrato </th>
						<th> </th>
			    	</tr>
			  	</thead>
			  	<tbody>";

			  		$i=0;
			  		conexionBD();
			  		if($datos){
			  			$consulta=consultaBD("SELECT * FROM contratos_en_facturas WHERE codigoFactura=".$datos['codigo']);
			  			while($datosConceptos=mysql_fetch_assoc($consulta)){
			  				imprimeLineaTablaConceptos($datosConceptos,$i,$datos['codigoCliente']);
			  				$i++;
			  			}
			  		}

			  		if($i==0){
			  			imprimeLineaTablaConceptos(false,0,$datos['codigoCliente']);
			  		}
			  		cierraBD();

	echo "		</tbody>
			</table>
			<!--button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaConceptos\");'><i class='icon-plus'></i> Añadir concepto</button> 
			<button type='button' class='btn btn-small btn-danger' onclick='eliminaFilaFacturacion(\"tablaConceptos\");'><i class='icon-trash'></i> Eliminar concepto</button-->
		</div>
	</div>
	<br /><br />";
}

function imprimeLineaTablaConceptos($datos,$i,$codigoCliente){
	$j=$i+1;

	echo "
	<tr>";
		campoSelectContratoFactura($datos,$i,$codigoCliente);	
	echo "<td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td>
	</tr>";
}

function campoSelectContratoFactura($datos,$i,$codigoCliente){
	$nombres=array('');
	$valores=array('NULL');

	if($codigoCliente){
		$datosCliente=obtieneContratosCliente($codigoCliente);
		$nombres=$datosCliente['nombres'];
		$valores=$datosCliente['valores'];
	}

	campoSelect('codigoContrato'.$i,'',$nombres,$valores,$datos['codigoContrato'],'selectpicker span8 show-tick','data-live-search="true"',1);
}


function obtieneContratosCliente($codigoCliente){
	$res=array('nombres'=>array(''),'valores'=>array('NULL'));

	$consulta=consultaBD("SELECT contratos.codigo, contratos.codigoInterno, ofertas.total, contratos.fechaInicio, clientes.EMPCP, clientes.EMPID, ofertas.subtotal, ofertas.subtotalRM,
						  contratos.incrementoIPC, contratos.incrementoIpcAplicado
						  FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
						  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
						  WHERE ofertas.codigoCliente='$codigoCliente';");

	while($datos=mysql_fetch_assoc($consulta)){
		$numeroContrato=formateaReferenciaContrato($datos,$datos);

		

		$nombre='Nº: '.$numeroContrato.'. &nbsp; RRMM:  '.compruebaImporteContrato($datos['subtotalRM'],$datos).' €. &nbsp; Actividades: '.compruebaImporteContrato($datos['subtotal'],$datos).' €. &nbsp; Total: '.compruebaImporteContrato($datos['total'],$datos).' €';

		array_push($res['nombres'],$nombre);
		array_push($res['valores'],$datos['codigo']);
	}

	return $res;
}

function obtieneContratosClienteAjax(){
	$datos=arrayFormulario();
	extract($datos);

	$res="<option value='NULL'></option>";

	$consulta=consultaBD("SELECT contratos.codigo, contratos.codigoInterno, ofertas.total, contratos.fechaInicio, clientes.EMPCP, clientes.EMPID, ofertas.subtotal, ofertas.subtotalRM,
						  contratos.incrementoIPC, contratos.incrementoIpcAplicado
						  FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
						  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
						  WHERE ofertas.codigoCliente='$codigoCliente' AND codigoContrato NOT IN(SELECT codigoContrato FROM contratos_en_facturas WHERE codigoContrato IS NOT NULL AND codigoFactura IS NOT NULL);",true);

   	$esModeloAntiguo = $datos['esModeloAntiguo'];


	while($datos=mysql_fetch_assoc($consulta)){
		$numeroContrato=formateaReferenciaContrato($datos,$datos);

		// Aplicar viejo o nuevo modelo
		

		$nombre='Nº: '.$numeroContrato.'. &nbsp; RRMM:  '.compruebaImporteContrato($datos['subtotalRM'],$datos,$esModeloAntiguo).' €. &nbsp; Actividades: '.compruebaImporteContrato($datos['subtotal'],$datos,$esModeloAntiguo).' €. &nbsp; Total: '.compruebaImporteContrato($datos['total'],$datos,$esModeloAntiguo).' €';

		$res.="<option value='".$datos['codigo']."'>".$nombre."</option>";
	}

	echo $res;
}


function creaTablaVencimientos($datos){
	echo "<br />
	<div class='control-group'>                     
		<label class='control-label'>Vencimientos:</label>
	    <div class='controls'>
	    </div>
	</div>
	<div class='centro'>
		<div class='table-responsive'>
			<table class='table table-striped tabla-simple' id='tablaVencimientos'>
			  	<thead>
			    	<tr>
			            <th> Medio pago </th>
						<th> F. Vencimiento </th>
						<th> Importe </th>
						<th> Estado </th>
						<th> Concepto </th>
						<th> Observaciones </th>
						<th> </th>
			    	</tr>
			  	</thead>
			  	<tbody>";

			  		$i=0;

			  		$contrato=datosRegistro('contratos_en_facturas',$datos['codigo'],'codigoFactura');

			  		conexionBD();
			  		
			  		if($datos){
			  			$consulta=consultaBD("SELECT * FROM vencimientos_facturas WHERE codigoFactura=".$datos['codigo']);
			  			while($datosVencimiento=mysql_fetch_assoc($consulta)){
			  				imprimeLineaTablaVencimientos($datosVencimiento,$i,$datos);
			  				campoOculto($datosVencimiento['codigo'],'codigoVencimiento'.$i);
			  				$i++;
			  			}
			  			if($i==0){
			  				
			  				$factura=consultaBD('SELECT * FROM contratos_en_facturas WHERE codigoContrato='.$contrato['codigoContrato'].' AND codigoFactura!='.$datos['codigo'],false,true);
			  				if($factura){
			  					campoOculto($factura['codigoFactura'],'codigoFacturaVencimientos');
			  					$consulta=consultaBD("SELECT * FROM vencimientos_facturas WHERE codigoFactura=".$factura['codigoFactura']);
			  					while($datosVencimiento=mysql_fetch_assoc($consulta)){
			  						imprimeLineaTablaVencimientos($datosVencimiento,$i,$datos);
			  						campoOculto($datosVencimiento['codigo'],'codigoVencimiento'.$i);
			  						$i++;
			  					}
			  				}
			  			}
			  		}

			  		if($i==0){
			  			imprimeLineaTablaVencimientos(false,0,false);
			  		}
			  		cierraBD();

	echo "		</tbody>
			</table>
			<div class='centro'>
				<button type='button' class='btn btn-small btn-success' onclick='insertaFilaTablaOcultos(\"tablaVencimientos\");'><i class='icon-plus'></i> Añadir vencimiento</button> 
				<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaVencimientos\");'><i class='icon-trash'></i> Eliminar vencimiento</button> ";


				if($_SESSION['tipoUsuario']=='CONTABILIDAD' || $_SESSION['tipoUsuario']=='ADMIN'){
					echo "<!--button type='button' class='btn btn-small btn-primary' onclick='mostrarOcultos(\"#tablaVencimientos\");' estado='mostrar'><i class='icon-search'></i> Mostrar ocultos</button> 
						  <button type='button' class='btn btn-small btn-warning' onclick='ocultarSeleccionados(\"#tablaVencimientos\");'><i class='icon-eye-slash'></i> Ocultar seleccionado/s</button>
						  <button type='button' class='btn btn-small btn-info' onclick='desOcultarSeleccionados(\"#tablaVencimientos\");'><i class='icon-eye'></i> Des-ocultar seleccionado/s</button-->";
				}

	echo "
			</div>
		</div>
	</div><br /><br />";
}

function imprimeLineaTablaVencimientos($datosVencimiento,$i,$datosFactura){
	$j=$i+1;
	

	echo "
	<tr>";
		campoFormaPago($datosVencimiento['codigoFormaPago'],'codigoFormaPago'.$i,1,false);
		campoFechaTabla('fechaVencimiento'.$i,$datosVencimiento['fechaVencimiento']);
		campoTextoSimbolo('importeVencimiento'.$i,'','€',formateaNumeroWeb($datosVencimiento['importe']),'input-mini pagination-right',1);
		campoSelectEstadoVencimiento($datosVencimiento,$i,1,false,false);
		campoSelectConceptoVencimiento($datosVencimiento,$i,$datosFactura);
		areaTextoTabla('observacionesVencimiento'.$i,$datosVencimiento['observaciones'],''); 
		campoOculto('','fechaDevolucion'.$i);
		campoOculto('','gastosDevolucion'.$i);
		
	echo "<td class='centro'>";
			campoOculto($datosVencimiento['llamadaAsesoria'],'llamadaAsesoria'.$i,'NO');
			campoOculto($datosVencimiento['llamadaEmpresa'],'llamadaEmpresa'.$i,'NO');
	echo "	<input type='checkbox' name='filasTabla[]' value='$j'>
		  </td>
	</tr>";
}


function campoSelectConceptoVencimiento($datos,$i,$datosFactura){
	campoTextoTabla('conceptoVencimiento'.$i,$datos['concepto']);

	//Comentado temporalmente, a espera de ver si prefieren el campo de texto
	/*if(!$datos){
		campoSelect('codigoConcepto'.$i,'',array(),array(),false,'selectpicker span3 show-tick','data-live-search="true"',1);
	}
	else{
		$query=obtieneConsultaConceptosVencimiento($datosFactura['codigo'],$datosFactura['tipo']);
		campoSelectConsulta('codigoConcepto'.$i,'',$query,$datos['codigoConcepto'],'selectpicker span3 show-tick','data-live-search="true"','',1);
	}*/
}


function obtieneConsultaConceptosVencimiento($codigoFactura,$tipo){
	$res='';

	if($tipo=='FORMACION'){
		$res="SELECT grupos.codigo, CONCAT(accionFormativa,' - ',grupos.grupo,' - ',accion) AS texto FROM acciones_formativas INNER JOIN grupos ON acciones_formativas.codigo=grupos.codigoAccionFormativa INNER JOIN grupos_en_facturas ON grupos.codigo=grupos_en_facturas.codigoGrupo WHERE codigoFactura=$codigoFactura ORDER BY accionFormativa;";
	}
	elseif($tipo=='SERVICIOS'){
		$res="SELECT codigo, DATE_FORMAT(ventas_servicios.fecha,'%d/%m/%Y') AS texto FROM ventas_servicios INNER JOIN ventas_en_facturas ON ventas_servicios.codigo=ventas_en_facturas.codigoVentaServicio WHERE codigoFactura=$codigoFactura ORDER BY fecha;";
	}
	elseif($tipo=='OTROS'){
		$res="SELECT codigo, concepto AS texto FROM conceptos_libres_facturas WHERE codigoFactura=$codigoFactura ORDER BY concepto;";
	}

	return $res;
}


function creaTablaCobros($datos){
	echo "	
	<div class='control-group'>                     
		<label class='control-label'>Cobros:</label>
	    <div class='controls'>
			<div class='table-responsive'>
				<table class='table table-striped tabla-simple' id='tablaCobros'>
				  	<thead>
				    	<tr>
				            <th> Medio pago </th>
							<th> F. Cobro </th>
							<th> Importe </th>
							<th> Banco </th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		conexionBD();
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM cobros_facturas WHERE codigoFactura=".$datos['codigo']);
				  			while($datosCobro=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaCobros($datosCobro,$i);
				  				campoOculto($datosCobro['codigo'],'codigoCobro'.$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaCobros(false,0);
				  		}
				  		cierraBD();

	echo "			</tbody>
				</table>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='insertaFilaTablaOcultos(\"tablaCobros\");'><i class='icon-plus'></i> Añadir cobro</button> 
					<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaCobros\");'><i class='icon-trash'></i> Eliminar cobro</button> ";

					if($_SESSION['tipoUsuario']=='CONTABILIDAD' || $_SESSION['tipoUsuario']=='ADMIN'){
						echo "<!--button type='button' class='btn btn-small btn-primary' onclick='mostrarOcultos(\"#tablaCobros\");' estado='mostrar'><i class='icon-search'></i> Mostrar ocultos</button> 
							  <button type='button' class='btn btn-small btn-warning' onclick='ocultarSeleccionados(\"#tablaCobros\");'><i class='icon-eye-slash'></i> Ocultar seleccionado/s</button>
							  <button type='button' class='btn btn-small btn-info' onclick='desOcultarSeleccionados(\"#tablaCobros\");'><i class='icon-eye'></i> Des-ocultar seleccionado/s</button-->";
					}

	echo "
				</div>
			</div>
		</div>
	</div>";
}

function imprimeLineaTablaCobros($datos,$i){
	$j=$i+1;

	echo "
	<tr>";
	
		campoFormaPago($datos['codigoFormaPago'],'codigoFormaPagoCobro'.$i,1,false);
		campoFechaTabla('fechaCobro'.$i,$datos['fechaCobro']);
		campoTextoSimbolo('importeCobro'.$i,'','€',formateaNumeroWeb($datos['importe']),'input-mini pagination-right',1);
		campoSelectConsulta('codigoCuentaPropia'.$i,'',"SELECT codigo, nombre AS texto FROM cuentas_propias WHERE activo='SI' ORDER BY nombre;",$datos['codigoCuentaPropia'],'selectpicker span3 show-tick','data-live-search="true"','',1,false);
	
	echo "<td class='centro'>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		  </td>
	</tr>";
}



//Parte de generación de documentos

function generaPDFFactura($codigoFactura){
	conexionBD();
	$nombreFichero=array();
	
	$datos=consultaBD("SELECT facturas.*, serie, clientes.EMPNOMBRE AS cliente, clientes.EMPCIF AS cifCliente, clientes.EMPDIR AS domicilioCliente, 
    clientes.EMPCP AS cpCliente, clientes.EMPLOC AS localidadCliente, clientes.EMPPROV AS provinciaCliente,
    cuentas_propias.iban AS cuentaEmisor,
    facturas.numero, clientes.EMPIBAN,
    clientes.EMPCP, clientes.EMPID, contratos.fechaInicio, contratos.fechaFin, contratos.codigoInterno, IFNULL(formas_pago.codigo,'Pago') AS formaPago,
    contratos.incrementoIPC, contratos.incrementoIpcAplicado, vencimientosOfacturas	

    FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
    LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
    LEFT JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
    LEFT JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
    LEFT JOIN formas_pago ON contratos.codigoFormaPago=formas_pago.codigo
    LEFT JOIN cuentas_propias ON contratos.codigoCuentaPropiaContrato=cuentas_propias.codigo
    
    WHERE facturas.codigo=$codigoFactura
    GROUP BY facturas.codigo",false,true);
	
    $emisor=consultaBD("SELECT emisores.razonSocial AS emisor, emisores.cif AS cifEmisor, emisores.domicilio AS domicilioEmisor,
					    emisores.cp AS cpEmisor, emisores.localidad AS localidadEmisor, emisores.provincia AS provinciaEmisor,
					    emisores.telefono, emisores.fax, emisores.web, emisores.email, emisores.registro, emisores.ficheroLogo, emisores.iban, emisores.registro AS registroPie
					    FROM emisores 
					    WHERE codigo=1",false,true);

	$referenciaContrato=formateaReferenciaContrato($datos,$datos);

	$nombreFichero[0]=$datos['serie'].'-'.$datos['numero'].'-'.trim(limpiaCadena($datos['cliente']));
	
	if($datos['tipoFactura']!='LIBRE'){
		$conceptos=obtieneConceptosFactura($codigoFactura,$datos,$referenciaContrato,$datos['total'],$datos['baseImponible']);
	}
	else{
		$conceptos=obtieneConceptosLibreFacturaPDF($datos);
	}

	// if ($conceptos['total'] != $datos['total']) {
	// 	$datos['total'] = $conceptos['total'];
	// }

	$vencimientos=obtieneVencimientosFactura($datos['codigo'],$datos['EMPIBAN'],$datos['cuentaEmisor'],$datos['formaPago'],$datos);

	cierraBD();

	$observaciones=formateaObservacionesFactura($datos['observaciones']);

	$logo='../documentos/emisores/'.trim($emisor['ficheroLogo']);
	if(!file_exists($logo)){//Para evitar que la factura no se genere por no encontrar la imagen
		$logo='../img/logo.png';
	}

	$contenido = "
	<style type='text/css'>
	<!--
		.tablaCabecera{
			width:100%;
		}

		.tablaCabecera td{
			width:50%;
		}

		.tablaCabecera .emisor{
			text-align:right;
			font-weight:bold;
			line-height:15px;
		}

		.tablaEmisorCliente{
			margin-top:10px;
			font-size:12px;
			width:100%;
			border-collapse:collapse;
		}

		.tablaBottom{
			top:740px;
			position: absolute;
		}

		.tablaEmisorCliente td{
			border-bottom:1px solid #000;
		}

		.celdaEmisor{
			width:49%;
			padding:10px 30px 1px 30px;
			border:1px solid #000;
		}

		.celdaCliente{
			border:1px solid #000;
			width:49%;
			padding:10px 30px 1px 30px;
			line-height:15px;
			text-align:center;
		}

		.celdaTabla{
			padding:0px;
			width:49%;
		}
		.celdaBlanca{
			width:2%;
			border-top:0px;
			border-bottom:0px;
		}

		.cajaNumero{
			margin-top:20px;
			font-size:12px;
		}

		.cajaFecha{
			text-align:right;
			margin-right:240px;
		}

		.cabeceraConceptos, .tablaConceptos{
			margin-top:20px;
			border:1px solid #000;
			width:100%;
		}

		.celdaConcepto{
			width:85%;
			padding:10px;
		}
		.celdaImporte{
			width:15%;
			text-align:right;
			padding:10px;
		}

		.tablaConceptos{
			border-collapse:collapse;
			margin-top:0px;
		}

		.tablaConceptos td, .tablaConceptos th{
			font-size:12px;
			border:1px solid #000;
			line-height:18px;
		}

		.tablaConceptos th{
			background:#EEE;
		}

		.tablaConceptos .celdaConcepto{
			border-left:1px solid #000;
		}

		.tablaConceptos .celdaImporte{
			border-right:1px solid #000;
		}

		.tablaTotales{
			width:100%;
		}

		.tablaTotales .celdaConcepto{
			width:60%;
		}
		.tablaTotales .celdaImporte{
			width:40%;
		}

		.cajaMedioPago{
			max-width:100%;
			font-size:12px;
			line-height:18px;
			margin-top:5px;
		}

		.cajaMedioPago ol{
			margin-top:-15px;
		}

		.pie{
			font-size:10px;
			text-align:center;
			line-height:18px;
			color:#0062B3;
		}

		.textoAdvertencia{
			font-size:11px;
			text-align:justify;
			font-weight:bold;
		}
		.centrado{
			text-align:center;
		}

		.cajaObservaciones{
			width:100%;
		}

		.logo{
			width:30%;
		}

		.titulo{
			background:#CCC;
			width:100%;
			text-align:center;
			margin:0px;
			padding:0px;
			font-weight:bold;
			font-size:28px;
			font-style:italic;
		}

		.tablaDatos{
			width:100%;
			border-collapse: collapse;
		}

		.tablaDatos th{
			font-weight:normal;
			border:1px solid #000;
			background:#EEE;
			padding:5px;
		}

		.tablaDatos td{
			font-weight:normal;
			border:1px solid #000;
			padding:5px;
		}

		.tablaDatos .a5{
			width:5%;
			text-align:center;
		}

		.tablaDatos .a9{
			width:9%;
			text-align:center;
		}

		.tablaDatos .a17{
			width:17%;
			text-align:center;
		}

		.tablaDatos .a35{
			width:35%;
		}

		.tablaDatos .a100{
			width:100%;
			padding:10px;
		}

		.centro{
			text-align:center;
		}

		ul li{
			font-size:14px;
		}

		.h200{
			height:200px;
		}

		.textoLateralFactura{
			position:absolute;
			height:60%;
			left:-50px;
			top:300px;
		}

		.enlacePie{
            display:block;
            position:absolute;
            bottom:20px;
            left:33%;
            text-align:center;
            color:#0062B3;
            font-size:11px;
            line-height:14px;
        }

        .enlacePie a{
            text-decoration:none;
            color:#0062B3;
        }
	-->
	</style>
	<page footer='page'>
	    <table class='tablaCabecera'>
	    	<tr>
	    		<td><img src='".$logo."' class='logo' /><br /></td>
	    		<td class='emisor'>
		    		".$emisor['emisor']."<br />
		    		".$emisor['cifEmisor']."<br/>
				    ".$emisor['domicilioEmisor']."<br />
				    ".$emisor['cpEmisor']." ".$emisor['localidadEmisor']."<br/>".$emisor['provinciaEmisor']."<br />
				    
		    	</td>
		    </tr>
	    </table>
	    <br/>
	    <div class='titulo'>
	    	FACTURA
	    </div>
	    <table class='tablaEmisorCliente'>
	    	<tr>
	    		<td>Factura</td>
	    		<td class='celdaBlanca'></td>
	    		<td>Cliente</td>
	    	</tr>
		    <tr>
		    	<td class='celdaEmisor'>
		    		Factura: <strong>".$datos['serie']."/".$datos['numero']."</strong><br />
				    Fecha: <strong>".formateaFechaWeb($datos['fecha'])."</strong><br />
				    Contrato: <strong>".$referenciaContrato."</strong>
		    	</td>
		    	<td class='celdaBlanca' style='border-right:1px solid #000'></td>
		    	<td class='celdaCliente'>
		    		<strong>".$datos['cliente']."</strong><br />
				    ".$datos['domicilioCliente']."<br />
				    ".$datos['cpCliente']." ".$datos['localidadCliente']." (".$datos['provinciaCliente'].")<br />
				    CIF: ".$datos['cifCliente']."
		    	</td>
		    </tr>
	    </table>
	    <br/>
	    <table class='tablaDatos'>
	    	<tr>
	    		<th class='a5 centro'>Ud.</th>
	    		<th class='a35'>Concepto</th>
	    		<th class='a17 centro'>Sujeto I.V.A</th>
	    		<th class='a17 centro'>Exento I.V.A</th>
	    		<th class='a9 centro'>I.V.A</th>
	    		<th class='a17 centro'>Total</th>
	    	</tr>
	    	".$conceptos['html']."	
	    </table>

	    <table class='tablaEmisorCliente tablaBottom'>
	    <tr>
	    		<td>Observaciones</td>
	    		<td class='celdaBlanca'></td>
	    		<td style='border-bottom:0px'></td>
	    </tr>
	    <tr>
	    	<td class='celdaEmisor'>".$observaciones."".$conceptos['rm']."</td>
	    	<td class='celdaBlanca' style='border-right:0px solid #000'></td>
	    	<td class='celdaTabla'>
	    		<table class='tablaConceptos tablaTotales'>
	    			<tr>
	    				<td class='celdaConcepto'>Exento de IVA:</td>
	    				<td class='celdaImporte'>".formateaNumeroWeb($conceptos['baseExenta'])." €</td>
	    			</tr>
	    			<tr>
	    				<td class='celdaConcepto'>Sujeto a IVA:</td>
	    				<td class='celdaImporte'>".formateaNumeroWeb($conceptos['baseImponible'])." €</td>
	    			</tr>
	    			<tr>
	    				<td class='celdaConcepto'>I.V.A. (21%):</td>
	    				<td class='celdaImporte'>".formateaNumeroWeb($conceptos['importeIva'])." €</td>
	    			</tr>
	    			<tr>
	    				<td class='celdaConcepto'></td>
	    				<td class='celdaImporte'></td>
	    			</tr>
	    			<tr>
	    				<th class='celdaConcepto'><b>Total factura:</b></th>
	    				<th class='celdaImporte'>".formateaNumeroWeb($conceptos['total'])." €</th>
	    			</tr>
	    		</table>
	    	</td>
	    </tr>
	    </table>

	    <div class='cajaMedioPago'>
	    	<strong>
		    	".$vencimientos."
		    </strong>
		</div>
		<img src='../img/textoLateralFactura.png' class='textoLateralFactura' />

	    <page_footer>
	    	<div class='enlacePie'>
                <strong><i>ANESCO SERVICIO DE PREVENCION</i></strong><br />
                Tlf .954.10.93.93 <a href='mailto:administracion@anescoprl.es'>administracion@anescoprl.es</a><br />
                <a href='http://www.anescoprl.es'>www.anescoprl.es</a>
            </div>
            
	    	<div class='pie'>
	    	".$emisor['registroPie']."
	    	</div>
	    </page_footer>
	</page>";
	if($conceptos['anexo']!=''){
		$contenido.="<page footer='page'>";
		$contenido.="<table class='tablaDatos tablaAnexo'>
	    	<tr>
	    		<th class='a100'>Concepto</th>
	    	</tr>
	    	".$conceptos['anexo']."	
	    	</table>";
		$contenido.="<page_footer>
	    	<div class='enlacePie'>
                <strong><i>ANESCO SERVICIO DE PREVENCION</i></strong><br />
                Tlf .954.10.93.93 <a href='mailto:administracion@anescoprl.es'>administracion@anescoprl.es</a><br />
                <a href='http://www.anescoprl.es'>www.anescoprl.es</a>
            </div>
	    	<div class='pie'>
	    	".$emisor['registroPie']."
	    	</div>
	    </page_footer>
	    </page>";
	}

	$nombreFichero[1]=$contenido;

	return $nombreFichero;
}

//Fin parte generación documentos

function imprimeContratosPendientes(){
	global $_CONFIG;

	conexionBD();
	$where=defineWhereEjercicio('WHERE 1=1','contratos.fechaInicio');
	$consulta=consultaBD("SELECT clientes.EMPNOMBRE,clientes.codigo AS codigoCliente, ofertas.total, contratos.*
						  FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo 
						  ".$where." AND contratos.codigo NOT IN(SELECT codigoContrato FROM contratos_en_facturas WHERE codigoContrato IS NOT NULL AND codigoFactura IS NOT NULL);");
	cierraBD();

	$iconoC=array('SI'=>'<i style="color:#5DB521;font-size:24px;" class="icon-check"></i>','NO'=>'<i style="color:#FF0000;font-size:24px;" class="icon-close"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		$cliente=datosRegistro('clientes',$datos['codigoCliente']);
		conexionBD();
		$renovacion=consultaBD('SELECT * FROM contratos_renovaciones WHERE codigoContrato='.$datos['codigo'].' ORDER BY fecha DESC');
		cierraBD();
		$renovacion=mysql_fetch_assoc($renovacion);
		echo "
			<tr>
				<td>".formateaReferenciaContrato($cliente,$datos)."</td>
				<td>".formateaFechaWeb($datos['fechaInicio'])."</td>
				<td>".formateaFechaWeb($datos['fechaFin'])."</td>
				<td>".formateaFechaWeb($renovacion['fecha'])."</td>
				<td>".$datos['EMPNOMBRE']." (".$datos['EMPNOMBRE'].")</td>
				<td>".formateaNumeroWeb($datos['total'])." €</td>
				<td class='centro'>
					<a raiz='".$_CONFIG['raiz']."' codigoContrato='".$datos['codigo']."' href='#' class='btn btn-propio btnProforma noAjax'><i class='icon-file-text-o'> Crear Proforma</i></a>
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	/*<td class='centro'>
					<a href='".$_CONFIG['raiz']."proformas/index.php?codigoContrato=".$datos['codigo']."' class='btn btn-propio btnProforma noAjax'><i class='icon-file-text-o'> Crear Proforma</i></a>
				</td>*/
}



function ventasPendientesSeleccionadas(){
	abreVentanaGestion('Gestión de ventas pendientes','index.php');
	echo "<p class='justificado'>Indique por favor la serie, el número y el emisor de cada una de las facturas generadas:</p>

	<table class='table table-striped tabla-simple' id='tablaVentasRegistradas'>
        <thead>
          <tr>
          	<th>Contrato</th>
          	<th>Cliente</th>
          	<th>Fecha</th>
            <th>Base imponible</th>
            <th>Serie</th>
            <th>Número</th>
            <th>Emisor</th>
          </tr>
        </thead>
        <tbody>";

        	conexionBD();

        	$facturas=creaFacturasVentasPendientes();

        	$i=0;
        	foreach($facturas as $codigoFactura){
        		$datos=consultaBD("SELECT facturas.codigo, facturas.fecha, codigoSerieFactura, clientes.EMPNOMBRE AS cliente, 
        						   clientes.EMPCIF, clientes.EMPCP, clientes.EMPID, contratos.codigoInterno, facturas.baseImponible, 
        						   contratos.fechaInicio, facturas.total, facturas.activo, clientes.codigo AS codigoCliente
								   FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
								   LEFT JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
								   LEFT JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
								   WHERE facturas.codigo='$codigoFactura';",false,true);

        		imprimeLineaTablaVentasSeleccionadas($codigoFactura,$datos,$i);
        		$i++;
        	}

        	cierraBD();

	echo "          
    	</tbody>
    </table>";



	cierraVentanaGestion('index.php',false,true,'Guardar','icon-check',true,'Cancelar','icon-remove');
}

function imprimeLineaTablaVentasSeleccionadas($codigoFactura,$datos,$i){
	echo "<tr>
			<td>".formateaReferenciaContrato($datos,$datos)."</td>
			<td>".$datos['cliente']."</td>
			<td>".formateaFechaWeb($datos['fecha'])."</td>
			<td>".formateaNumeroWeb($datos['baseImponible'])." €</td>";
			campoSelectConsulta('codigoSerieFactura'.$i,'',"SELECT codigo, serie AS texto FROM series_facturas WHERE activo='SI' ORDER BY serie",false,'span2 selectpicker serie show-tick','data-live-search="true"','',1,false);
			campoTextoTabla('numero'.$i,false,'input-mini pagination-right');
			campoSelectConsulta('codigoEmisor'.$i,'','SELECT codigo, razonSocial AS texto FROM emisores WHERE activo="SI" ORDER BY razonSocial;',false,'selectpicker emisor show-tick span3','data-live-search="true"','',1,false);
	echo "</tr>";

	campoOculto($codigoFactura,'codigoFactura'.$i);
}


function creaFacturasVentasPendientes(){
	$res=array();
	$datos=arrayFormulario();
	$fecha=fechaBD();

	conexionBD();
	foreach($datos['codigoLista'] as $codigoContrato) {
		$datosCliente=consultaBD("SELECT ofertas.importe_iva,clientes.*, subtotal, subtotalRM, total, formaPago, contratos.incrementoIPC, contratos.incrementoIPCAplicado
								  FROM clientes INNER JOIN ofertas ON clientes.codigo=ofertas.codigoCliente
								  INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta
								  WHERE contratos.codigo='$codigoContrato';",false,true);


		// CUALQUIER FACTURA DE VENTA PENDIENTE SE LE APLICARA EL IPC NUEVO
		 $datosCliente['subtotal'] 		= compruebaImporteContrato($datos['subtotal'],$datos,false);
		 $datosCliente['importe_iva'] 	= compruebaImporteContrato($datos['importe_iva'],$datos,false);
         $datosCliente['subtotalRM'] 	= compruebaImporteContrato($datos['subtotalRM'], $datos, false);
		 $datosCliente['baseImponible'] = $datosCliente['subtotal'] + $datosCliente['subtotalRM'];
		 $datosCliente['total'] 		= $datos['importe_iva'] + $datosCliente['baseImponible'];



		$consulta=consultaBD("INSERT INTO facturas(codigo,fecha,codigoCliente,tipoFactura,baseImponible,total,activo,codigoUsuarioFactura)
							  VALUES(NULL,'$fecha','".$datosCliente['codigo']."','NORMAL','".$datosCliente['baseImponible']."',
							  '".$datosCliente['total']."','SI',".$_SESSION['codigoU'].");");

		if($consulta){
			$codigoFactura=mysql_insert_id();
			array_push($res,$codigoFactura);

			$consulta=$consulta && consultaBD("INSERT INTO contratos_en_facturas VALUES(NULL,$codigoContrato,$codigoFactura)");

			$consulta=$consulta && consultaBD("INSERT INTO vencimientos_facturas(codigo,codigoFactura,codigoFormaPago,importe) 
											   VALUES(NULL,$codigoFactura,NULL,".$datosCliente['total'].");");
		}
	}
	cierraBD();

	return $res;
}


function actualizaFacturacionPendientes(){
	$res=true;
	$_POST['codigoCliente']='';//Para que entre en mensajeResultado
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos['codigoFactura'.$i]);$i++){
		$codigoFactura=$datos['codigoFactura'.$i];
		$codigoSerieFactura=$datos['codigoSerieFactura'.$i];
		$numero=$datos['numero'.$i];
		$codigoEmisor=$datos['codigoEmisor'.$i];


		//Actualización de la factura creada con los datos obtenidos
		$res=$res && consultaBD("UPDATE facturas SET codigoSerieFactura=$codigoSerieFactura, numero=$numero, codigoEmisor=$codigoEmisor WHERE codigo=$codigoFactura");
	}

	cierraBD();

	return $res;
}



function eliminaFacturasVentasPendientes(){
	$res=true;
	$datos=arrayFormulario();

	/*conexionBD();
	foreach ($datos['facturas'] as $codigoFactura){
		$res=$res && consultaBD("DELETE FROM facturas WHERE codigo=$codigoFactura");
	}
	cierraBD();*/

	if($res){
		$res='ok';
	}
	else{
		$res='error';
	}

	echo $res;
}

function generaExcelFacturas($objPHPExcel){
	$iconosEstado=array(
		'SI'		=>	'<i class="icon-check-circle iconoFactura icon-success"></i>',
		'PARCIAL'	=>	'<i class="icon-adjust iconoFactura icon-warning-color"></i>',
		'NO'		=>	'<i class="icon-times-circle iconoFactura icon-danger"></i>'
	);

	$query=construyeConsultaExcelFacturas();

	conexionBD();
	$consulta=consultaBD($query);

	$i=2;
	while($datos=mysql_fetch_assoc($consulta)){


		$datosExtra=obtieneDatosExtraFacturaListadoFacturas($datos,$iconosEstado);
		
		if($datos['tipoFactura']!='LIBRE'){
			$conceptos=obtieneConceptosFactura($datos['codigo'],$datos,'',$datos['total'],$datos['baseImponible']);
		}
		else{
			$conceptos=obtieneConceptosLibreFacturaPDF($datos);
		}

		// Para la comprobación, los valores no son exactamente los mismo, por lo que aumentamos la precision de la base imponible de conceptos
		// porque al hacerle formateaNumeroWeb lleva implicito un round
		// Esto se realiza en la comprobacion ya que una diferencia en el tercer decimal es posible y puede dar falsas alertas
		// Aun asi, en las factura, saldra saliendo la BI que es mas alta y redondeado a unas decimas.
		$baseImponibleConcepto = floor($conceptos['baseImponible']*100)/100;
		$baseTotalConcepto     =floor($conceptos['total']*100)/100;

		// Por ello, creamos dos campos extras de visualizacion, para segun cada caso, muestre el valor mas alto y si hay que hacerle alguna comprobación adicional
		$baseImponibleVista = $datos['baseImponible'];
		$totalVista 		= $datos['total'];

		$estado = "OK";
		if (($baseImponibleConcepto > $datos['baseImponible'] || $baseTotalConcepto > $datos['total']) && $_SESSION['ejercicio'] >= 2024) {
			$estado = 'ERROR';
			$baseImponibleVista = $baseImponibleConcepto;
			$totalVista			= $baseTotalConcepto;
		}


		$objPHPExcel->getActiveSheet()->getCell('A'.$i)->setValue(formateaFechaWeb($datos['fecha']));
		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($datos['serie']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($datos['numero']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($datos['cliente']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue($datos['cif']);
		$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue($baseImponibleVista);
		$objPHPExcel->getActiveSheet()->getCell('G'.$i)->setValue($totalVista);
		$objPHPExcel->getActiveSheet()->getCell('H'.$i)->setValue($datosExtra['concepto']);
		$objPHPExcel->getActiveSheet()->getCell('I'.$i)->setValue($datos['facturaCobrada']);

		if($estado == "ERROR"){
			$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':I'.$i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFE6E6');//Les pone el fondo rojo
		}


		$i++;
	}
	cierraBD();

	foreach(range('A','I') as $columnID) {
		$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
	}     	
}


function construyeConsultaExcelFacturas(){

	$columnas=array(
		'fecha',
		'serie',
		'numero',
		'cliente',
		'cif',
		'baseImponible',
		'total',
		'codigoInterno',
		'facturaCobrada',
		'fecha',
		'codigoSerieFactura'
	);

	$whereAnio=obtieneWhereEjercicioEstadisticas('fecha');
	$having1=obtieneWhereListadoExcel("HAVING tipoFactura!='ABONO' AND tipoFactura!='PROFORMA' AND eliminado='NO' $whereAnio",$columnas);
	$having2=obtieneWhereListadoExcel("HAVING eliminado='NO' $whereAnio",$columnas);
	$orden=obtieneOrdenListadoExcel($columnas);


	$query="SELECT facturas.codigo, facturas.fecha, serie, facturas.numero, CONCAT(clientes.EMPNOMBRE,' (',clientes.EMPMARCA,')') AS cliente, clientes.EMPCIF AS cif, 
			facturas.baseImponible, facturas.total, facturas.activo, facturas.facturaCobrada,
		    cobros_facturas.codigo AS codigoCobro, clientes.codigo AS codigoCliente, abonos.codigo AS codigoAbono, clientes.EMPCP, clientes.EMPID, 
		    contratos.fechaInicio, contratos.codigoInterno, contratos.enVigor, contratos.suspendido, facturas.tipoFactura, facturas.eliminado, facturas.codigoSerieFactura,
			contratos.eliminado AS contratoEliminado

		    FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
		    LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
		    LEFT JOIN cobros_facturas ON facturas.codigo=cobros_facturas.codigoFactura AND cobros_facturas.importe>0
		    LEFT JOIN facturas AS abonos ON facturas.codigo=abonos.codigoFacturaAsociada AND abonos.tipoFactura='ABONO'
		    LEFT JOIN contratos_en_facturas ON facturas.codigo=contratos_en_facturas.codigoFactura
		    LEFT JOIN contratos ON contratos_en_facturas.codigoContrato=contratos.codigo
		    
		    GROUP BY facturas.codigo

		    $having1

		    UNION ALL

		    SELECT facturas_reconocimientos_medicos.codigo, facturas_reconocimientos_medicos.fecha, serie, facturas_reconocimientos_medicos.numero, 
		    IFNULL(CONCAT(clientes.EMPNOMBRE,' (',clientes.EMPMARCA,')'),CONCAT(clientes2.EMPNOMBRE,' (',clientes2.EMPMARCA,')')) AS cliente, 
		    IFNULL(clientes.EMPCIF,clientes2.EMPCIF) AS cif,  facturas_reconocimientos_medicos.total AS baseImponible, facturas_reconocimientos_medicos.total, 'SI' AS activo, 
			facturas_reconocimientos_medicos.facturaCobrada, NULL AS codigoCobro, clientes.codigo AS codigoCliente, NULL AS codigoAbono, 
			clientes.EMPCP, clientes.EMPID, contratos.fechaInicio, contratos.codigoInterno, contratos.enVigor, contratos.suspendido, 'RECONOCIMIENTOS' AS tipoFactura, 
		    facturas_reconocimientos_medicos.eliminado, codigoSerieFactura,
			contratos.eliminado AS contratoEliminado

		    FROM facturas_reconocimientos_medicos LEFT JOIN contratos ON facturas_reconocimientos_medicos.codigoContrato=contratos.codigo
		    LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
		    LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo
		    LEFT JOIN clientes clientes2 ON facturas_reconocimientos_medicos.codigoClienteParaConceptoManual=clientes2.codigo
		    LEFT JOIN series_facturas ON facturas_reconocimientos_medicos.codigoSerieFactura=series_facturas.codigo
		    
		    GROUP BY facturas_reconocimientos_medicos.codigo

		    $having2

		    $orden";

	return $query;
}






function compruebaNumeroFactura(){
	$res='ok';
	$datos=arrayFormulario();
	$ejercicio=obtieneEjercicioParaWhere();
	extract($datos);

	$consulta=consultaBD("SELECT codigo FROM facturas WHERE numero='$numeroFactura' AND codigoSerieFactura='$codigoSerieFactura' AND codigo!='$codigoFactura' AND YEAR(fecha)='$ejercicio' AND tipoFactura='NORMAL';",true);

	if(mysql_num_rows($consulta)>0){
		$res='repetido';
	}

	echo $res;
}

//Fin parte de facturas

/**
 * Como hay un lío de c**** con el tema del IPC, esta función
 * revisa las facturas y asigna el total correcto en aquellas
 * con 1 solo vencimiento cuyo importe es mayor que el total.
 *
 * @return void
 */
function compruebaTotalesIPC(){
	$res = true;
	$mensaje = '';
	$anio = date('Y');

	conexionBD();

	$consulta = consultaBD("SELECT * FROM facturas WHERE YEAR(fecha)=$anio AND tipoFactura!='ABONO' ORDER BY codigo;");

	while ($datos=mysql_fetch_assoc($consulta)) {
		
		$codigoFactura = $datos['codigo'];
		$subtotalFactura = $datos['baseImponible'];
		$totalFactura = $datos['total'];

		$vencimiento = consultaBD("SELECT importe FROM vencimientos_facturas WHERE codigoFactura='$codigoFactura' AND (importe-0.1)>'$totalFactura';", false, true);

		if($vencimiento){
			$totalVencimiento = $vencimiento['importe'];

			$ipcAplicado = round( ($totalVencimiento - $totalFactura) / $totalFactura, 4);
			$subtotalFactura = round($subtotalFactura * (1 + $ipcAplicado), 2);

			$mensaje .= "CORRECCION FACTURA ANESCO: IPC aplicado: $ipcAplicado - Total original: $totalFactura - Total con IPC: $totalVencimiento - ID: $codigoFactura - Nueva base imponible: $subtotalFactura - Nuevo total: $totalVencimiento<br />";

			$res = $res && consultaBD("UPDATE facturas SET baseImponible='$subtotalFactura', total='$totalVencimiento' WHERE codigo='$codigoFactura';");
		}
	}

	cierraBD();

	if($res && $mensaje!=''){
		enviaEmail('webmaster@qmaconsultores.com', 'Corrección factura - ANESCO', $mensaje);
	}
}