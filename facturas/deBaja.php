<?php
  $seccionActiva=30;
  include_once('../cabecera.php');
  
  $res=operacionesFacturas();
  $estadisticas=estadisticasFacturasDeBaja();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de Facturación:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-arrow-down"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Total facturado de baja</div>
                     <!--div class="stat"> <i class="icon-send"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Enviados a la agencia</div-->
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Facturas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				      <a href="<?php echo $_CONFIG['raiz']; ?>facturas/" class="shortcut"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label">Volver</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Facturas de baja</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Fecha </th>
                  <th> Serie </th>
                  <th> Nº </th>
                  <th> Cliente </th>
                  <th> CIF </th>
                  <th> Base Imp. </th>
                  <th> Total </th>
                  <th> Concepto </th>
                  <th> Cobrada </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeFacturasDeBaja();
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#reactivar').click(function(){//Eliminación de registros
        var valoresChecks=recorreChecks();
        if(valoresChecks['codigo0']==undefined){
            alert('Por favor, seleccione antes un registro del listado.');
        }
        else if(confirm('¿Está seguro/a de que desea restaurar los registros seleccionados?')){
            valoresChecks['elimina']='NO';
            creaFormulario(document.URL,valoresChecks,'post');//Cambiado el nombre del documento por la URL.
        }
    });
});
</script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>