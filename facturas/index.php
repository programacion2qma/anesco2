<?php
  $seccionActiva=30;
  include_once('../cabecera.php');
  
  operacionesFacturas();
  //compruebaTotalesIPC();
  $estadisticas=estadisticasFacturas();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de facturación <u>en el ejercicio seleccionado</u>:</h6>
                        
                        <div class="big_stats cf">
                            <div class="stat"> <i class="icon-euro"></i> <span class="value"><?php echo formateaNumeroWeb($estadisticas['facturacionTotal']);?></span><br />Total (definitivas)</div>
                        </div>

                        <?php
                            imprimeEstadisticasPorSeries($estadisticas['series']);
                        ?>

                        <div class="big_stats cf">
                            <div class="stat"> <i class="icon-check"></i> <span class="value"><?php echo formateaNumeroWeb($estadisticas['totalCobrado']);?></span><br />Total cobrado</div>
                            <div class="stat"> <i class="icon-times"></i> <span class="value"><?php echo formateaNumeroWeb($estadisticas['totalPendienteCobro']);?></span><br />Total pendiente de cobro</div>
                        </div>

                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Facturas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                  <a href="../proformas/" class="shortcut"><i class="shortcut-icon icon-file-text-o"></i><span class="shortcut-label">Proformas</span> <div class='badge avisoBotonGestion'><?=$estadisticas['numeroProformas']?></div> </a>                  
                  <a href="../facturas-reconocimientos/" class="shortcut"><i class="shortcut-icon icon-stethoscope"></i><span class="shortcut-label">Facturas RRMM</span> </a>
                  <a href="../series-facturas/" class="shortcut"><i class="shortcut-icon icon-list-ol"></i><span class="shortcut-label">Series</span> </a>
                  <a href="../abonos/" class="shortcut"><i class="shortcut-icon icon-reply"></i><span class="shortcut-label">Abonos</span> </a>
                  <br />
                  <a href="gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva factura</span> </a>
                  <a href="deBaja.php" class="shortcut"><i class="shortcut-icon icon-arrow-down"></i><span class="shortcut-label">De baja</span> </a>
                  <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-times-circle"></i><span class="shortcut-label">Eliminar</span> </a>
                  <a href="eliminadas.php" class="shortcut"><i class="shortcut-icon icon-trash-o"></i><span class="shortcut-label">Eliminadas</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

        <form action='generaExcel.php' method="post" class='noAjax'>
            <div class="span12">
              <div class="widget widget-table action-table">
                    <div class="widget-header"> <i class="icon-list"></i>
                        <h3>Facturas registradas (definitivas + reconocimientos médicos)</h3>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-success btn-small"><i class="icon-file-excel-o"></i> Descargar en Excel</button>
                            <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
                        </div>
                    </div>
                    <!-- /widget-header -->
                    <div class="widget-content">
                    <?php
                        filtroFacturas();
                        campoOculto('','global');
                        campoOculto('','columnaOrdenada');
                        campoOculto('','sentidoOrden');
                    ?>
                    <table class="table table-striped table-bordered datatable tablaTextoPeque" id="tablaFacturas">
                      <thead>
                        <tr>
                          <th> Fecha </th>
                          <th> Serie </th>
                          <th> Nº </th>
                          <th> Cliente </th>
                          <th> CIF </th>
                          <th> Base Imp. </th>
                          <th> Total </th>
                          <th> Concepto </th>
                          <th> Cobrada </th>
                          <th class="centro"></th>
                          <th class='centro'><input type='checkbox' id="todo"></th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                  <!-- /widget-content-->
                </div>
            </div>
        </form>
        
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>


<script type="text/javascript">
$(document).ready(function(){
  
    $('.hasDatepicker').val('').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    listadoTabla('#tablaFacturas','../listadoAjax.php?include=facturas&funcion=listadoFacturas();');
    
    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaFacturas');
});
</script>

<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>