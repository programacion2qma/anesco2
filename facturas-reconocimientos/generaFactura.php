<?php
    session_start();
    include_once("funciones.php");
    include_once('../../api/js/firma/signature-to-image.php');
    compruebaSesion();

    $contenido=generaPDFFactura($_GET['codigo']);
    require_once('../../api/html2pdf/html2pdf.class.php');
        $html2pdf=new HTML2PDF('P','A4','es',true,'UTF-8',array(20,10,10,15));
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->WriteHTML($contenido[1]);
    
    if(isset($_GET['envio'])){
        $html2pdf->Output($contenido[0].'.pdf','f');
        $codigoFactura=$_GET['codigo'];

        conexionBD();

        $factura=consultaBD("SELECT codigoClienteParaConceptoManual, codigoContrato FROM facturas_reconocimientos_medicos WHERE codigo='$codigoFactura';",false,true);
        
        if($factura['codigoClienteParaConceptoManual']!=NULL){
            $codigoCliente=$factura['codigoClienteParaConceptoManual'];
        }
        else{
            $contrato=consultaBD("SELECT codigoCliente FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='".$factura['codigoContrato']."';",false,true);
            $codigoCliente=$contrato['codigoCliente'];
        }

        $cliente=consultaBD("SELECT EMPEMAILPRINC AS email FROM clientes WHERE codigo='$codigoCliente';",false,true);

        cierraBD();


        $mensaje="Estimado cliente,<br/>
        Gracias por confiar en los servicios de <strong>ANESCO SERVICIO DE PREVENCIÓN</strong>. Adjuntamos la factura correspondiente al reconocimiento médico
        realizado al trabajador indicado. <br /><br />
        Muchas gracias por su atención.<br />
        Reciba un cordial saludo.<br /><br />
        <img src='https://crmparapymes.com.es/anesco2/img/logo.png' /><br />
        <div style='color:#075581;font-weight:bold'>
            <i>ANESCO SERVICIO DE PREVENCION</i><br />
            Tlf .954.10.92.93<br />
            info@anescoprl.es · www.anescoprl.es<br />
            C/ Murillo 1, 2ª P. 41001 - Sevilla.
        </div>";


        $adjunto=$contenido[0].'.pdf';
        
        if($cliente['email']==''){
            mensajeAdvertencia('el cliente no tiene definida cuenta de correo.');
        }
        elseif(enviaEmail($cliente['email'], 'Factura de reconocimiento - ANESCO', $mensaje,$adjunto)){
            mensajeOk('Factura enviada correctamente');
        }
        else{
            mensajeError('hubo un error al procesar el envío. Inténtelo de nuevo más tarde.');
        }

        unlink($adjunto);
    } 
    elseif(isset($_GET['descargaMasiva'])){
        $html2pdf->Output($contenido[0].'.pdf','D');
    }
    else {
        $html2pdf->Output($contenido[0].'.pdf');
    }

 