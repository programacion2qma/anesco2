<?php
  $seccionActiva=30;
  include_once('../cabecera.php');
  
  operacionesFacturas();
  $estadisticas=estadisticasGenericas('facturas_reconocimientos_medicos',false,defineWhereEjercicio("eliminado='NO'",'fecha'));
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas para el área de facturas de reconocimientos médicos:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-stethoscope"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Factura/s registrada/s</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de facturas para reconocimientos médicos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                  <a href="gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva factura</span> </a>
                  <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-times-circle"></i><span class="shortcut-label">Eliminar</span> </a>
                  <a href="eliminadas.php" class="shortcut"><i class="shortcut-icon icon-trash-o"></i><span class="shortcut-label">Eliminadas</span> </a>
                  <br />
                  <a href="../facturas/" class="shortcut"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label">Volver</span> </a>
                  <a href="pendientes-facturar.php" class="shortcut noAjax"><i class="shortcut-icon icon-flag"></i><span class="shortcut-label">Pendientes facturar</span> </a>
                  <a href="../control-reconocimientos/" class="shortcut"><i class="shortcut-icon icon-columns"></i><span class="shortcut-label">Control facturas</span> </a>   
                  <a href="ya-facturados.php" class="shortcut"><i class="shortcut-icon icon-list"></i><span class="shortcut-label">Facturados no seleccionados</span> </a>               
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

        <div class="span12">
          <div class="widget widget-table action-table">
              <div class="widget-header"> <i class="icon-list"></i>
                <h3>Facturas de reconocimientos médicos registradas</h3>
              </div>
              <!-- /widget-header -->
              <div class="widget-content">
                <table class="table table-striped table-bordered datatable tablaTextoPeque" id="tablaFacturas">
                  <thead>
                    <tr>
                      <th> Fecha </th>
                      <th> Serie </th>
                      <th> Nº </th>
                      <th> Emisor </th>
                      <th> Cliente </th>
                      <th> Contrato </th>
                      <th> Total </th>
                      <th> Cobrada </th>
                      <th class="centro"></th>
                      <th class='centro'><input type='checkbox' id="todo"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      imprimeFacturas();
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /widget-content-->
            </div>

        </div>
        
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/filtroTabla.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type='text/javascript'>
$(document).ready(function(){
    
    $(document).on('click','table .dropdown-menu li:nth-child(5) a',function(e){
        e.preventDefault();
        url=$(this).attr('href');
        var consulta=$.post(url);

        consulta.done(function(respuesta){
            $('#contenido').append(respuesta);

            setTimeout(function() {//Para hacer desaparecer la caja de resultado
                $(".errorLogin:not(:contains('Error'),:contains('Atención'))").fadeOut(3000);
            },3000);
        });
    });

});
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>