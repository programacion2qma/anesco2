<?php
  $seccionActiva=30;
  include_once('../cabecera.php');
  
  operacionesReconocimientosYaFacturados();
  $estadisticas=estadisticasGenericas('reconocimientos_medicos_ya_facturados',false,"codigoReconocimientoMedico IS NOT NULL");
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas para el área de facturas de reconocimientos médicos:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-list"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Facturados no seleccionados</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de facturas para reconocimientos médicos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                  <a href="../facturas-reconocimientos/" class="shortcut"><i class="shortcut-icon icon-chevron-left"></i><span class="shortcut-label">Volver</span> </a>
                  <a href="javascript:void(0);" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-eur"></i> <i class="shortcut-icon icon-reply"></i><span class="shortcut-label">Pasar a facturable</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>

        <div class="span12">
          <div class="widget widget-table action-table">
              <div class="widget-header"> <i class="icon-list"></i>
                <h3>Reconocimientos médicos facturados no seleccionados</h3>
              </div>
              <!-- /widget-header -->
              <div class="widget-content">
                <table class="table table-striped table-bordered datatable tablaTextoPeque" id="tablaReconocimientos">
                  <thead>
                    <tr>
                      <th> Fecha </th>
                      <th> Empleado </th>
                      <th> Empresa </th>
                      <th> Contrato </th>
                      <th> Incluido </th>
                      <th> Importe </th>
                      <th class='centro'><input type='checkbox' id="todo"></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
              <!-- /widget-content-->
            </div>

        </div>
        
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    
    listadoTabla('#tablaReconocimientos','../listadoAjax.php?include=facturas-reconocimientos&funcion=listadoReconocimientosYaFacturados();');
});
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>