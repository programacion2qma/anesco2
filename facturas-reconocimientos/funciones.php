<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de facturas


function operacionesFacturas(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaFactura();
	}
	elseif(isset($_POST['codigoContrato'])){
		$res=creaFactura();
	}
	elseif(isset($_GET['marcarFacturados'])){
		$res=marcarReconocimientosFacturados();
	}
	elseif(isset($_POST['codigosReconocimientos'])){
		$res=creaFacturasPendientes();
	}
	elseif(isset($_POST['elimina'])){
		$res=cambiaEstadoEliminadoFacturas($_POST['elimina']);
	}

	mensajeResultado('codigoContrato',$res,'Factura');
    mensajeResultado('elimina',$res,'Factura', true);
}


function creaFactura(){
	$_POST['total']=formateaNumeroWeb($_POST['total'],true);
	$res=insertaDatos('facturas_reconocimientos_medicos');
	
	if($res){
		$res=insertaReconocimientosMedicosFactura($res);
	}

	return $res;
}

function actualizaFactura(){
	$_POST['total']=formateaNumeroWeb($_POST['total'],true);
	$res=actualizaDatos('facturas_reconocimientos_medicos');
	
	if($res){
		$res=insertaReconocimientosMedicosFactura($_POST['codigo'],true);
	}

	return $res;
}

function insertaReconocimientosMedicosFactura($codigoFactura,$actualizacion=false){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	if($actualizacion){
		$res=$res && consultaBD("DELETE FROM reconocimientos_medicos_en_facturas WHERE codigoFacturaReconocimientoMedico=$codigoFactura");
	}

	for($i=0;isset($datos['codigoReconocimientoMedico'.$i]);$i++){
		$codigoReconocimientoMedico=$datos['codigoReconocimientoMedico'.$i];
		$precio=$datos['precio'.$i];
		$precio=formateaNumeroWeb($datos['precio'.$i],true);
		$incluidoContrato='NO';

		if(isset($datos['incluidoContrato'.$i])){
			$incluidoContrato=$datos['incluidoContrato'.$i];
		}
		
		//TODO: pasado un tiempo prudencial, eliminar el campo codigoEmpleado de la tabla reconocimientos_medicos_en_facturas, que tras la actualización no se usará mas

		$res=$res && consultaBD("INSERT INTO reconocimientos_medicos_en_facturas VALUES(NULL,$codigoReconocimientoMedico,NULL,'$incluidoContrato','$precio',$codigoFactura);");
	}

	cierraBD();

	return $res;
}

function cambiaEstadoEliminadoFacturas($estado){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$res=$res && cambiaValorCampo('eliminado',$estado,$datos['codigo'.$i],'facturas_reconocimientos_medicos',false);
	}
	cierraBD();

	return $res;
}


function imprimeFacturas($eliminadas='NO'){
	$where=defineWhereEjercicio("WHERE facturas_reconocimientos_medicos.eliminado='$eliminadas'",'facturas_reconocimientos_medicos.fecha');

	
	$consulta=consultaBD("SELECT facturas_reconocimientos_medicos.codigo, series_facturas.serie, facturas_reconocimientos_medicos.numero, emisores.razonSocial AS emisor,
						  clientes.EMPCP, clientes.EMPID, contratos.fechaInicio, contratos.codigoInterno, facturas_reconocimientos_medicos.total, facturas_reconocimientos_medicos.facturaCobrada,
						  contratos.codigo AS codigoContrato, facturas_reconocimientos_medicos.fecha, CONCAT(clientes.EMPNOMBRE,' (',clientes.EMPMARCA,')') AS cliente1, 
						  CONCAT(clientes2.EMPNOMBRE,' (',clientes2.EMPMARCA,')') AS cliente2, facturaContratos
						  FROM facturas_reconocimientos_medicos LEFT JOIN contratos ON facturas_reconocimientos_medicos.codigoContrato=contratos.codigo
						  LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
						  LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo
						  LEFT JOIN series_facturas ON facturas_reconocimientos_medicos.codigoSerieFactura=series_facturas.codigo
						  LEFT JOIN clientes clientes2 ON facturas_reconocimientos_medicos.codigoClienteParaConceptoManual=clientes2.codigo
						  LEFT JOIN emisores ON facturas_reconocimientos_medicos.codigoEmisor=emisores.codigo
						  $where",true);

	$iconosEstado=array(
					'SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>',
					'NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');

	while($datos=mysql_fetch_assoc($consulta)){
		$cobrada = $iconosEstado[$datos['facturaCobrada']];
		
		$contrato='-Concepto manual-';
		if($datos['codigoContrato']!=NULL){
			$contrato="<a href='../contratos/gestion.php?codigo=".$datos['codigoContrato']."' class='noAjax' target='_blank'>".formateaReferenciaContrato($datos,$datos)."</a>";
		}

		$cliente=$datos['cliente1'];
		if($datos['facturaContratos']=='NO'){
			$cliente=$datos['cliente2'];
		}

		echo "<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['serie']."</td>
				<td>".$datos['numero']."</td>
				<td>".$datos['emisor']."</td>
				<td>".$cliente."</td>
				<td>".$contrato."</td>
				<td class='nowrap pagination-right'> ".formateaNumeroWeb($datos['total'])." €</td>
				<td class='centro'>".$cobrada."</td>
				".botonAcciones(array('Detalles','Descargar factura','Enviar factura'),array('facturas-reconocimientos/gestion.php?codigo='.$datos['codigo'],'facturas-reconocimientos/generaFactura.php?codigo='.$datos['codigo'],'facturas-reconocimientos/generaFactura.php?envio&codigo='.$datos['codigo']),array("icon-search-plus",'icon-cloud-download','icon-send'),array(0,1,2),true,'')."
				<td class='centro'>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
}

function imprimeFacturasEliminadas($anio=false){
	$whereAnio=obtieneWhereEjercicioEstadisticas('facturas.fecha');
	if($anio!=''){
		$whereAnio="AND facturas.fecha LIKE '$anio-%'";
	}

	
	$consulta=consultaBD("SELECT facturas.codigo, facturas.fecha, serie, facturas.numero, emisores.razonSocial AS emisor, clientes.EMPNOMBRE AS cliente, clientes.EMPCIF AS cif, facturas.baseImponible, 
						  facturas.total, facturas.activo, facturas.facturaCobrada,
						  cobros_facturas.codigo AS codigoCobro, clientes.codigo AS codigoCliente, abonos.codigo AS codigoAbono
						  FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
						  LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
						  LEFT JOIN emisores ON facturas.codigoEmisor=emisores.codigo 
						  LEFT JOIN cobros_facturas ON facturas.codigo=cobros_facturas.codigoFactura AND cobros_facturas.importe>0
						  LEFT JOIN facturas AS abonos ON facturas.codigo=abonos.codigoFacturaAsociada AND abonos.tipoFactura='ABONO'
						  WHERE facturas.tipoFactura!='ABONO' AND facturas.tipoFactura!='PROFORMA' AND facturas.eliminado='SI'  $whereAnio
						  GROUP BY facturas.codigo",true);

	$iconosEstado=array(
					'SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>',
					'NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		$cobrada = $iconosEstado[$datos['facturaCobrada']];
		
		echo "<tr>
				<td>".formateaFechaWeb($datos['fecha'])."</td>
				<td>".$datos['serie']."</td>
				<td>".$datos['numero']."</td>
				<td>".$datos['emisor']."</td>
				<td><a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."' class='noAjax' target='_blank'>".$datos['cliente']."</a></td>
				<td>".$datos['cif']."</td>
				<td class='nowrap pagination-right'>".formateaNumeroWeb($datos['baseImponible'])." €</td>
				<td class='nowrap pagination-right'> ".formateaNumeroWeb($datos['total'])." €</td>
				<td class='centro'>".$cobrada."</td>
				".botonAcciones(array('Detalles','Descargar factura','Generar abono'),array('facturas/gestion.php?codigo='.$datos['codigo'],'facturas/generaFactura.php?codigo='.$datos['codigo'],'abonos/gestion.php?codigoFactura='.$datos['codigo']),array("icon-search-plus",'icon-cloud-download','icon-reply'),array(0,1,0),true,'')."
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
}

/*function obtieneIconoAbono($codigoAbono){
	$res='';

	if($codigoAbono!=NULL){
		$res='<i class="icon-danger icon-exclamation-circle" title="Abono generado"></i>';
	}

	return $res;
}
*/

function gestionFacturaRRMM(){
	operacionesFacturas();

	abreVentanaGestionConBotones('Gestión de factura de reconocimiento médico','index.php','span3','icon-edit','',false,'noAjax');
	$datos=compruebaDatos('facturas_reconocimientos_medicos');

	campoTexto('fecha','Fecha',formateaFechaWeb($datos['fecha']),'input-small datepicker hasDatepicker obligatorio');
	campoSelectConsulta('codigoEmisor','Emisor','SELECT codigo, razonSocial AS texto FROM emisores WHERE activo="SI" ORDER BY razonSocial;',$datos,'selectpicker show-tick span3 obligatorio');
	campoRadio('facturaContratos','Facturar contratos',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectConsulta('codigoSerieFactura','Serie',"SELECT codigo, serie AS texto FROM series_facturas WHERE activo='SI' ORDER BY serie",$datos,'span1 selectpicker show-tick');
	campoTexto('numero','Número',$datos,'input-mini pagination-right obligatorio');
	campoFecha('fechaRRMM','Fecha realización de los RRMM',$datos);

	cierraColumnaCampos();
	
	abreColumnaCampos('span11');

	echo "<div id='cajaContratos' class='hide'>";
	campoSelectContratoFactura($datos);
	creaTablaReconocimientosMedicos($datos);
	echo "</div>
	
	<div id='cajaConceptoManual' class='hide'>";
	campoSelectConsulta('codigoClienteParaConceptoManual','Cliente',"SELECT codigo, EMPNOMBRE AS texto FROM clientes WHERE activo='SI' AND eliminado='NO' ORDER BY EMPNOMBRE",$datos);
	areaTexto('conceptoManualFactura','Concepto/s',$datos,'areaInforme'); 
	echo "</div>";

	cierraColumnaCampos();
	abreColumnaCampos('span5');

	campoTextoSimbolo('total','Total','€',formateaNumeroWeb($datos['total']),'input-mini pagination-right obligatorio');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoRadio('facturaCobrada','Cobrada',$datos);

	cierraColumnaCampos();
	abreColumnaCampos('span11');

	areaTexto('observaciones','Observaciones',$datos,'observacionesParaFactura');

	campoOculto($datos,'eliminado','NO');
	divOculto('','precioReconocimientoIncluido');
	divOculto('','precioReconocimientoNoIncluido');

	cierraVentanaGestion('index.php',true);
}


function creaTablaReconocimientosMedicos($datos){
	echo "
	<div class='control-group'>                     
		<label class='control-label'>Reconocimientos a facturar:</label>
	    <div class='controls'>
			<table class='table table-striped tabla-simple' id='tablaReconocimientos'>
			  	<thead>
			    	<tr>
			            <th> Reconocimiento </th>
			            <th> En contrato </th>
			            <th> Precio </th>
						<th> </th>
			    	</tr>
			  	</thead>
			  	<tbody>";

			  		$i=0;
			  		$selec=0;
			  		conexionBD();
			  		if($datos){
			  			$consulta=consultaBD("SELECT * FROM reconocimientos_medicos_en_facturas WHERE codigoFacturaReconocimientoMedico=".$datos['codigo']);
			  			while($datosConceptos=mysql_fetch_assoc($consulta)){
			  				imprimeLineaTablaReconocimientosMedicos($datosConceptos,$i,$datos['codigoContrato'],$datos['codigo']);
			  				if($datosConceptos['incluidoContrato']=='SI'){
			  					$selec++;
			  				}
			  				$i++;
			  			}
			  		}

			  		if($i==0){
			  			imprimeLineaTablaReconocimientosMedicos(false,0,false,false);
			  		}
			  		cierraBD();

	echo "		</tbody>
			</table>";

	if(!$datos){
		echo "
			<button type='button' class='btn btn-small btn-success' onclick='insertaRM(\"tablaReconocimientos\");'><i class='icon-plus'></i> Añadir reconocimiento</button> 
			<button type='button' class='btn btn-small btn-danger' onclick='eliminaFilaFacturacion(\"tablaReconocimientos\");'><i class='icon-trash'></i> Eliminar reconocimiento</button>";
	}

	echo "
		</div>
	</div>
	<br /><br />";
	echo '<div class="hide">';
	campoTexto('rmTotales','RM Totales','');
	campoTexto('rmFacturas','RM Facturas','');
	campoTexto('rmSeleccionados','RM Select',$selec);
	echo '</div>';
}

function imprimeLineaTablaReconocimientosMedicos($datos,$i,$codigoContrato,$codigoFactura){
	$j=$i+1;

	echo "
	<tr>";
			campoSelectReconocimientoFactura($datos,$i,$codigoContrato,$codigoFactura);
			
			echo "<td class='centro enContrato'>";
				campoCheckIndividual('incluidoContrato'.$i,'',$datos['incluidoContrato'],'SI',false);
			echo "</td>";

			campoTextoSimbolo('precio'.$i,'','€',formateaNumeroWeb($datos['precio']),'input-mini pagination-right precioRM',1);
	echo "	<td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td>
	</tr>";
}

function campoSelectReconocimientoFactura($datos,$i,$codigoContrato,$codigoFactura){
	if(!$datos){
		campoSelect('codigoReconocimientoMedico'.$i,'',array(''),array('NULL'),false,'selectpicker span7 show-tick',"data-live-search='true'",1);
	}
	else{

		$query="SELECT reconocimientos_medicos.codigo, CONCAT(DATE_FORMAT(reconocimientos_medicos.fecha,'%d/%m/%Y'),' - ',nombre,' ',apellidos,' - DNI:',dni) AS texto 
			  	FROM reconocimientos_medicos INNER JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
			  	WHERE codigoContrato='$codigoContrato' AND reconocimientos_medicos.codigo NOT IN(
			  		SELECT codigoReconocimientoMedico 
			  		FROM reconocimientos_medicos_en_facturas INNER JOIN facturas_reconocimientos_medicos ON reconocimientos_medicos_en_facturas.codigoFacturaReconocimientoMedico=facturas_reconocimientos_medicos.codigo
			  		WHERE codigoReconocimientoMedico IS NOT NULL AND facturas_reconocimientos_medicos.eliminado='NO'
			  		AND facturas_reconocimientos_medicos.codigo!='$codigoFactura'
			  	)";

		campoSelectConsulta('codigoReconocimientoMedico'.$i,'',$query,$datos['codigoReconocimientoMedico'],'selectpicker span7 show-tick',"data-live-search='true'",'',1,false);
	}
}

/*
function campoSelectEmpleadoReconocimiento($datos,$i,$codigoContrato){
	if(!$datos){
		campoSelect('codigoEmpleado'.$i,'',array(''),array('NULL'),false,'selectpicker span7 show-tick',"data-live-search='true'",1);
	}
	else{
		campoSelectConsulta('codigoEmpleado'.$i,'',"SELECT reconocimientos_medicos.codigoEmpleado AS codigo, CONCAT(nombre,' ',apellidos,' - DNI:',dni) AS texto FROM reconocimientos_medicos INNER JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo WHERE codigoCliente=(SELECT codigoCliente FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='$codigoContrato');",$datos['codigoEmpleado'],'selectpicker span7 show-tick',"data-live-search='true'",'',1,false);
	}
}
*/

function campoSelectContratoFactura($datos){
	$nombres=array('');
	$valores=array('NULL');

	$contratos=obtieneContratos($datos);
	$nombres=$contratos['nombres'];
	$valores=$contratos['valores'];

	campoSelect('codigoContrato','Contrato',$nombres,$valores,$datos,'selectpicker span8 show-tick');
}


function obtieneContratos($datos){
	$res=array('nombres'=>array(''),'valores'=>array('NULL'));
	$yaFacturadas='1=1';
	if(!$datos){
		//$yaFacturadas="contratos.eliminado='NO' AND contratos.enVigor = 'SI' AND contratos.codigo NOT IN (SELECT codigoContrato FROM facturas_reconocimientos_medicos WHERE codigoContrato IS NOT NULL)";
		$yaFacturadas='contratos.eliminado="NO" AND contratos.enVigor = "SI"';
	} 

	$consulta=consultaBD("SELECT contratos.codigo, contratos.codigoInterno, ofertas.total, contratos.fechaInicio, clientes.EMPCP, clientes.EMPID, ofertas.subtotal, ofertas.subtotalRM, clientes.EMPNOMBRE
						  FROM contratos INNER JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
						  INNER JOIN clientes ON ofertas.codigoCliente=clientes.codigo
						  WHERE $yaFacturadas",true);
	while($datos=mysql_fetch_assoc($consulta)){
		$numeroContrato=formateaReferenciaContrato($datos,$datos);
		$nombre='Nº: '.$numeroContrato.'. &nbsp; Cliente: '.$datos['EMPNOMBRE'].'. &nbsp; RRMM:  '.formateaNumeroWeb($datos['subtotalRM']).' €. &nbsp; Actividades: '.formateaNumeroWeb($datos['subtotal']).' €. &nbsp; Total: '.formateaNumeroWeb($datos['total']).' €';

		array_push($res['nombres'],$nombre);
		array_push($res['valores'],$datos['codigo']);
	}

	return $res;
}

function obtieneReconocimientosContratoParaConceptos(){
	$res="<option value='NULL'></option>";

	$datos=arrayFormulario();
	extract($datos);


	$consulta=consultaBD("SELECT reconocimientos_medicos.codigo, CONCAT(DATE_FORMAT(reconocimientos_medicos.fecha,'%d/%m/%Y'),' - ',nombre,' ',apellidos,' - DNI:',dni) AS texto 
						  FROM reconocimientos_medicos INNER JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
						  WHERE reconocimientos_medicos.bloqueado='SI' AND reconocimientos_medicos.eliminado='NO' AND tipoReconocimiento!='Se niega' AND
						  codigoContrato='$codigoContrato' AND reconocimientos_medicos.codigo NOT IN(
						  	SELECT codigoReconocimientoMedico 
						  	FROM reconocimientos_medicos_en_facturas 
						  	WHERE codigoReconocimientoMedico IS NOT NULL AND codigoFacturaReconocimientoMedico IS NOT NULL
						  )",true);


	while($empleado=mysql_fetch_assoc($consulta)){
		$res.="<option value='".$empleado['codigo']."'>".$empleado['texto']."</option>";
	}

	echo $res;
}


/*
function obtieneEmpleadosContrato(){
	$res="<option value='NULL'></option>";

	$datos=arrayFormulario();
	$codigoContrato=$datos['codigoContrato'];


	$consulta=consultaBD("SELECT reconocimientos_medicos.codigoEmpleado AS codigo, CONCAT(nombre,' ',apellidos,' - DNI:',dni) AS texto 
						  FROM reconocimientos_medicos INNER JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo 
						  WHERE codigoCliente=(
						  	SELECT codigoCliente FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='$codigoContrato'
						  )
						  AND codigoEmpleado NOT IN(
						  	SELECT codigoEmpleado 
						  	FROM reconocimientos_medicos_en_facturas INNER JOIN facturas_reconocimientos_medicos ON reconocimientos_medicos_en_facturas.codigoFacturaReconocimientoMedico=facturas_reconocimientos_medicos.codigo
						  	WHERE codigoContrato='$codigoContrato'
						  ) AND tipoReconocimiento!='Se niega';",true);


	while($empleado=mysql_fetch_assoc($consulta)){
		$res.="<option value='".$empleado['codigo']."'>".$empleado['texto']."</option>";
	}

	echo $res;
}
*/

function obtienePreciosReconocimientosContrato(){
	$datos=arrayFormulario();
	$codigoContrato=$datos['codigoContrato'];
	$codigo=$datos['codigo'];


	$contrato=consultaBD("SELECT importeRM, importeRMExtra, numRM FROM ofertas INNER JOIN contratos ON ofertas.codigo=contratos.codigoOferta WHERE contratos.codigo='$codigoContrato';",true,true);
	$rm=consultaBD('SELECT COUNT(reconocimientos_medicos_en_facturas.codigo) AS total FROM reconocimientos_medicos_en_facturas INNER JOIN facturas_reconocimientos_medicos ON reconocimientos_medicos_en_facturas.codigoFacturaReconocimientoMedico=facturas_reconocimientos_medicos.codigo WHERE incluidoContrato="SI" AND codigoContrato='.$codigoContrato.' AND codigoFacturaReconocimientoMedico!='.$codigo,true,true);
	
	$res=formateaNumeroWeb($contrato['importeRM']).'&{}&'.formateaNumeroWeb($contrato['importeRMExtra']);
	$res.='&{}&'.$contrato['numRM'];
	$res.='&{}&'.$rm['total'];
	echo $res;
}

//Parte de generación de documentos

function generaPDFFactura($codigoFactura){
	conexionBD();
	$nombreFichero=array();
	
	$datos=consultaBD("SELECT facturas_reconocimientos_medicos.*, serie, emisores.razonSocial AS emisor, emisores.cif AS cifEmisor, emisores.domicilio AS domicilioEmisor,  emisores.iban AS cuentaEmisor,
	emisores.cp AS cpEmisor, emisores.localidad AS localidadEmisor, emisores.provincia AS provinciaEmisor, emisores.telefono, emisores.fax, emisores.web, emisores.email, emisores.registro, 
	emisores.ficheroLogo, emisores.iban, emisores.registro AS registroPie, contratos.fechaInicio, contratos.fechaFin, contratos.codigoInterno, contratos.fechaInicio, contratos.fechaFin, ofertas.opcion,
	COUNT(reconocimientos_medicos_en_facturas.codigo) AS numRRMM, formas_pago.forma As formaPago, facturaContratos, conceptoManualFactura, contratos.codigo AS codigoContrato,

	IFNULL(clientes.EMPNOMBRE,clientes2.EMPNOMBRE) AS cliente,
	IFNULL(clientes.EMPCIF,clientes2.EMPCIF) AS cifCliente,
	IFNULL(clientes.EMPDIR,clientes2.EMPDIR) AS domicilioCliente,
	IFNULL(clientes.EMPCP,clientes2.EMPCP) AS cpCliente,
	IFNULL(clientes.EMPLOC,clientes2.EMPLOC) AS localidadCliente,
	IFNULL(clientes.EMPPROV,clientes2.EMPPROV) AS provinciaCliente,
	IFNULL(clientes.EMPIBAN,clientes2.EMPIBAN) AS EMPIBAN,
	IFNULL(clientes.EMPCP,clientes2.EMPCP) AS EMPCP,
	IFNULL(clientes.EMPID,clientes2.EMPID) AS EMPID

	FROM facturas_reconocimientos_medicos LEFT JOIN contratos ON facturas_reconocimientos_medicos.codigoContrato=contratos.codigo
	LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
	LEFT JOIN clientes ON ofertas.codigoCliente=clientes.codigo
	LEFT JOIN emisores ON facturas_reconocimientos_medicos.codigoEmisor=emisores.codigo
	LEFT JOIN series_facturas ON facturas_reconocimientos_medicos.codigoSerieFactura=series_facturas.codigo
	LEFT JOIN reconocimientos_medicos_en_facturas ON facturas_reconocimientos_medicos.codigo=reconocimientos_medicos_en_facturas.codigoFacturaReconocimientoMedico
	LEFT JOIN formas_pago ON contratos.codigoFormaPago=formas_pago.codigo
	LEFT JOIN clientes clientes2 ON facturas_reconocimientos_medicos.codigoClienteParaConceptoManual=clientes2.codigo
	
	WHERE facturas_reconocimientos_medicos.codigo='$codigoFactura'",false,true);


	$nombreFichero[0]=$datos['serie'].'-'.$datos['numero'].'-'.trim(limpiaCadena($datos['cliente']));
	$empleados=obtieneReconocimientosMedicosFactura($codigoFactura);

	cierraBD();

	$observaciones=formateaObservacionesFactura($datos['observaciones']);
	$observaciones=str_replace('<br />','',$observaciones);

	$logo='../documentos/emisores/'.trim($datos['ficheroLogo']);
	if(trim($datos['ficheroLogo'])=='' || !file_exists($logo)){//Para evitar que la factura no se genere por no encontrar la imagen
		$logo='../img/logo.png';
	}

	$referenciaContrato='-';
	$concepto=nl2br($datos['conceptoManualFactura']);
	if($datos['facturaContratos']=='SI'){
		$referenciaContrato=formateaReferenciaContrato($datos,$datos);
		$concepto="Reconocimientos médicos realizados según listado adjunto.";
	}

	$contenido = "
	<style type='text/css'>
	<!--
		.tablaCabecera{
			width:100%;
		}

		.tablaCabecera td{
			width:50%;
		}

		.tablaCabecera .emisor{
			text-align:right;
			font-weight:bold;
			line-height:15px;
		}

		.tablaEmisorCliente{
			margin-top:10px;
			font-size:12px;
			width:100%;
			border-collapse:collapse;
		}

		.tablaEmisorCliente td{
			border-bottom:1px solid #000;
		}

		.celdaEmisor{
			width:49%;
			padding:10px 30px 1px 30px;
			border:1px solid #000;
		}

		.celdaCliente{
			border:1px solid #000;
			width:49%;
			padding:10px 30px 1px 30px;
			line-height:15px;
			text-align:center;
		}

		.celdaTabla{
			padding:0px;
			width:49%;
		}
		.celdaBlanca{
			width:2%;
			border-top:0px;
			border-bottom:0px;
		}

		.cajaNumero{
			margin-top:20px;
			font-size:12px;
		}

		.cajaFecha{
			text-align:right;
			margin-right:240px;
		}

		.cabeceraConceptos, .tablaConceptos{
			margin-top:20px;
			border:1px solid #000;
			width:100%;
		}
		.celdaConcepto{
			width:85%;
			padding:10px;
		}
		.celdaImporte{
			width:15%;
			text-align:right;
			padding:10px;
		}

		.tablaConceptos{
			border-collapse:collapse;
			margin-top:0px;
		}

		.tablaConceptos td, .tablaConceptos th{
			font-size:12px;
			border:1px solid #000;
			line-height:18px;
		}

		.tablaConceptos th{
			background:#EEE;
		}

		.tablaConceptos .celdaConcepto{
			border-left:1px solid #000;
		}

		.tablaConceptos .celdaImporte{
			border-right:1px solid #000;
		}

		.tablaTotales{
			width:100%;
		}

		.tablaTotales .celdaConcepto{
			width:60%;
		}
		.tablaTotales .celdaImporte{
			width:40%;
		}

		.cajaMedioPago{
			max-width:100%;
			font-size:12px;
			line-height:18px;
		}

		.cajaMedioPago ol{
			margin-top:-20px;
		}

		.pie{
			font-size:10px;
			text-align:center;
			line-height:18px;
			color:#0062B3;
		}

		.textoAdvertencia{
			font-size:11px;
			text-align:justify;
			font-weight:bold;
		}
		.centrado{
			text-align:center;
		}

		.cajaObservaciones{
			width:100%;
		}

		.logo{
			width:30%;
		}

		.titulo{
			background:#CCC;
			width:100%;
			text-align:center;
			margin:0px;
			padding:0px;
			font-weight:bold;
			font-size:28px;
			font-style:italic;
		}

		.tablaDatos{
			width:100%;
			border-collapse: collapse;
		}

		.tablaDatos th{
			font-weight:normal;
			border:1px solid #000;
			background:#EEE;
			padding:5px;
		}

		.tablaDatos td{
			font-weight:normal;
			border:1px solid #000;
			padding:5px;
		}

		.tablaDatos .a5{
			width:5%;
			text-align:center;
		}

		.tablaDatos .a9{
			width:9%;
			text-align:center;
		}

		.tablaDatos .a17{
			width:17%;
			text-align:center;
		}

		.tablaDatos .a35{
			width:35%;
		}

		.tablaDatos .a100{
			width:100%;
			padding:10px;
		}

		.centro{
			text-align:center;
		}

		ul li{
			font-size:14px;
		}

		.h200{
			height:200px;
		}

		.h400{
			height:400px;
		}

		.textoLateralFactura{
			position:absolute;
			height:60%;
			left:-50px;
			top:300px;
		}

		.enlacePie{
            display:block;
            position:absolute;
            bottom:20px;
            left:40%;
            text-align:center;
        }

        .enlacePie a{
            text-decoration:none;
        }

        .tablaEmpleados{
        	border-collapse:collapse;
        	width:100%;
        }
        .tablaEmpleados .primeraColumna{
        	border:1px solid #000;
        	width:70%;
        	padding:2px;
        }
        .tablaEmpleados .segundaColumna{
        	border:1px solid #000;
        	width:15%;
        	padding:2px;
        }

        .tablaEmpleados .filaPar td{
        	color:#1c4072;
        }

        .celdaEmpleados{
        	vertical-align:top;
        }

        .enlacePie{
            display:block;
            position:absolute;
            bottom:20px;
            left:33%;
            text-align:center;
            color:#0062B3;
            font-size:11px;
            line-height:14px;
        }

        .enlacePie a{
            text-decoration:none;
            color:#0062B3;
        }

        .observaciones{
        	font-size:7px;
        }

        .titulo-anexo{
        	font-style:italic;
        	border-bottom:2px solid #000;
        	font-weight:bold;
        	font-size:18px;
        	margin-bottom:15px;
        }
	-->
	</style>
	<page footer='page'>
	    <table class='tablaCabecera'>
	    	<tr>
	    		<td><img src='".$logo."' class='logo' /><br /></td>
	    		<td class='emisor'>
		    		".$datos['emisor']."<br />
		    		".$datos['cifEmisor']."<br/>
				    ".$datos['domicilioEmisor']."<br />
				    ".$datos['cpEmisor']." ".$datos['localidadEmisor']."<br/>".$datos['provinciaEmisor']."<br />
				    
		    	</td>
		    </tr>
	    </table>
	    <br/>
	    <div class='titulo'>
	    	FACTURA
	    </div>
	    <table class='tablaEmisorCliente'>
	    	<tr>
	    		<td>Factura</td>
	    		<td class='celdaBlanca'></td>
	    		<td>Cliente</td>
	    	</tr>
		    <tr>
		    	<td class='celdaEmisor'>
		    		Factura: <strong>".$datos['serie']."/".$datos['numero']."</strong><br />
				    Fecha: <strong>".formateaFechaWeb($datos['fecha'])."</strong><br />
				    Contrato: <strong>".$referenciaContrato."</strong>
		    	</td>
		    	<td class='celdaBlanca' style='border-right:1px solid #000'></td>
		    	<td class='celdaCliente'>
		    		<strong>".$datos['cliente']."</strong><br />
				    ".$datos['domicilioCliente']."<br />
				    ".$datos['cpCliente']." ".$datos['localidadCliente']." (".$datos['provinciaCliente'].")<br />
				    CIF: ".$datos['cifCliente']."
		    	</td>
		    </tr>
	    </table>
	    <br/>
	    <table class='tablaDatos'>
	    	<tr>
	    		<th class='a5 centro'>Ud.</th>
	    		<th class='a35'>Concepto</th>
	    		<th class='a17 centro'>Sujeto I.V.A</th>
	    		<th class='a17 centro'>Exento I.V.A</th>
	    		<th class='a9 centro'>I.V.A</th>
	    		<th class='a17 centro'>Total</th>
	    	</tr>
	    	<tr>
	    		<td class='a5'>".$datos['numRRMM']."</td>
	    		<td class='a35'>".$concepto."</td>
				<td class='a17'></td>
				<td class='a17'>".formateaNumeroWeb($datos['total'])."</td>
				<td class='a9'></td>
				<td class='a17'>".formateaNumeroWeb($datos['total'])."</td>
			</tr>
			<tr>
				<td colspan='6' class='a100 h200 celdaEmpleados'>
					<br />";
				
				if(substr_count($empleados,"<tr ")<6){
					$contenido.=$empleados;
				}
				else{
					$contenido.="Se adjunta anexo con los reconocimientos realizados.";
				}


		$contenido.="
				</td>
			</tr>
	    </table>

	    <table class='tablaEmisorCliente'>
	    <tr>
	    		<td>Observaciones</td>
	    		<td class='celdaBlanca'></td>
	    		<td style='border-bottom:0px'></td>
	    </tr>
	    <tr>
	    	<td class='celdaEmisor observaciones'>".$observaciones."</td>
	    	<td class='celdaBlanca' style='border-right:0px solid #000'></td>
	    	<td class='celdaTabla'>
	    		<table class='tablaConceptos tablaTotales'>
	    			<tr>
	    				<td class='celdaConcepto'>Exento de IVA:</td>
	    				<td class='celdaImporte'>".formateaNumeroWeb($datos['total'])." €</td>
	    			</tr>
	    			<tr>
	    				<td class='celdaConcepto'>Sujeto a IVA:</td>
	    				<td class='celdaImporte'></td>
	    			</tr>
	    			<tr>
	    				<td class='celdaConcepto'>I.V.A. (21%):</td>
	    				<td class='celdaImporte'></td>
	    			</tr>
	    			<tr>
	    				<td class='celdaConcepto'></td>
	    				<td class='celdaImporte'></td>
	    			</tr>
	    			<tr>
	    				<th class='celdaConcepto'><b>Total factura:</b></th>
	    				<th class='celdaImporte'>".formateaNumeroWeb($datos['total'])." €</th>
	    			</tr>
	    		</table>
	    	</td>
	    </tr>
	    </table>
	    <strong>Forma de pago: ".$datos['formaPago']."</strong>
		<img src='../img/textoLateralFactura.png' class='textoLateralFactura' />

	    <page_footer>
	    	<div class='enlacePie'>
                <strong><i>ANESCO SERVICIO DE PREVENCION</i></strong><br />
                Tlf .954.10.92.93 <a href='mailto:administracion@anescoprl.es'>administracion@anescoprl.es</a><br />
                <a href='http://www.anescoprl.es'>www.anescoprl.es</a>
            </div>
	    	<div class='pie'>
	    	".$datos['registroPie']."
	    	</div>
	    </page_footer>
	</page>";

	if(substr_count($empleados,"<tr ")>=6){
		$contenido.="
		<page footer='page'>
			<div class='titulo-anexo'>Anexo con reconocimientos facturados</div>
			<table class='tablaEmpleados'>
				<tr>
					<th class='segundaColumna'>Fecha</th>
					<th class='primeraColumna'>Empleado</th>
					<th class='segundaColumna'>DNI</th>
				</tr>
			</table>
			".$empleados."
		</page>";
	}

	$nombreFichero[1]=$contenido;

	return $nombreFichero;
}


function formateaFechaReconocimientoFactura($fecha){
	$meses=array(''=>'','00'=>'','01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');

	$res=explode('-',$fecha);

	return $meses[$res[1]].' de '.$res[0];
}

/*
function obtieneEmpleadosFactura($codigoFactura,$codigoContrato,$fechaFactura){
	$res="
	<table class='tablaEmpleados'>";

	$consulta=consultaBD("SELECT codigoEmpleado FROM reconocimientos_medicos_en_facturas WHERE codigoFacturaReconocimientoMedico='$codigoFactura';");
	
	$i=0;
	while($datos=mysql_fetch_assoc($consulta)){

		
		//Como los reconocimientos médicos no está vinculados directamente con las facturas, lo que hago es pasarle la fecha de la factura
		//por si diese la casualidad de que un mismo empleado se realiza más de un reconocimiento médico en el mismo contrato (caso factura RM-48)
		
		$empleado=consultaBD("SELECT nombre, apellidos, dni, reconocimientos_medicos.fecha
							  FROM empleados INNER JOIN reconocimientos_medicos ON empleados.codigo=reconocimientos_medicos.codigoEmpleado
							  WHERE reconocimientos_medicos.codigoContrato='$codigoContrato' AND reconocimientos_medicos.eliminado='NO'
							  AND reconocimientos_medicos.fecha<='$fechaFactura' AND empleados.codigo='".$datos['codigoEmpleado']."';",false,true);

		$claseFila='';
		if($i%2==0){
			$claseFila="class='filaPar'";
		}

		$res.="
		<tr $claseFila>
			<td class='segundaColumna'>".formateaFechaWeb($empleado['fecha'])."</td>
			<td class='primeraColumna'>".$empleado['nombre']." ".$empleado['apellidos']."</td>
			<td class='segundaColumna'>".$empleado['dni']."</td>
		</tr>";

		$i++;
	}

	$res.="</table>";

	if($i==0){
		$res='';
	}
	
	return $res;
}
*/

function obtieneReconocimientosMedicosFactura($codigoFactura){
	$res="
	<table class='tablaEmpleados'>";

	$consulta=consultaBD("SELECT nombre, apellidos, dni, reconocimientos_medicos.fecha
						  FROM reconocimientos_medicos_en_facturas INNER JOIN reconocimientos_medicos on reconocimientos_medicos_en_facturas.codigoReconocimientoMedico=reconocimientos_medicos.codigo
						  INNER JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
						  WHERE codigoFacturaReconocimientoMedico='$codigoFactura';");
	
	$i=0;
	while($datos=mysql_fetch_assoc($consulta)){

		$claseFila='';
		if($i%2==0){
			$claseFila="class='filaPar'";
		}

		$res.="
		<tr $claseFila>
			<td class='segundaColumna'>".formateaFechaWeb($datos['fecha'])."</td>
			<td class='primeraColumna'>".$datos['nombre']." ".$datos['apellidos']."</td>
			<td class='segundaColumna'>".$datos['dni']."</td>
		</tr>";

		$i++;
	}

	$res.="</table>";

	if($i==0){
		$res='';
	}
	
	return $res;
}


//Fin parte generación documentos


function eliminaFacturasVentasPendientes(){
	$res=true;
	$datos=arrayFormulario();

	/*conexionBD();
	foreach ($datos['facturas'] as $codigoFactura){
		$res=$res && consultaBD("DELETE FROM facturas WHERE codigo=$codigoFactura");
	}
	cierraBD();*/

	if($res){
		$res='ok';
	}
	else{
		$res='error';
	}

	echo $res;
}

function listadoFacturas($objPHPExcel){
	conexionBD();
	$consulta=consultaBD("SELECT * FROM facturas ORDER BY codigo;");
	cierraBD();

	$serieFactura=array(1=>'F',2=>'RM',3=>'PT',''=>'');

	$i=4;
	while($datos=mysql_fetch_assoc($consulta)){
		$cliente=datosRegistro('clientes',$datos['codigoCliente']);		

		$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($datos['codigo']);
		$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($cliente['EMPNOMBRE']);
		$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($datos['fecha']);
		$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue($serieFactura[$datos['codigoSerieFactura']]);
		$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue($datos['numero']);
		$objPHPExcel->getActiveSheet()->getCell('G'.$i)->setValue($datos['tipoFactura']);
		$objPHPExcel->getActiveSheet()->getCell('H'.$i)->setValue($datos['baseImponible']);
		$objPHPExcel->getActiveSheet()->getCell('I'.$i)->setValue($datos['total']);
		$objPHPExcel->getActiveSheet()->getCell('J'.$i)->setValue($datos['facturaCobrada']);
		$i++;
	}
	foreach(range('B','I') as $columnID) {
    	$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
	}	
}

//Parte de reconocimientos pendientes de facturar

function listadoReconocimientosPendientesFacturar(){
	$numeroReconocimientosContratoProcesados=array();//Tendrá el formato [codigoContrato]=>[numero], y almacenará el número de reconocimientos procesados por contrato, para determinar los que están incluidos y no

	$columnas=array(
		'fecha',
		'empleado',
		'cliente',
		'EMPID',
		'codigo',
		'fecha',
		'codigo'
	);

	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	//$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();

	//La siguiente consulta extrae el reconocimiento, empleado, empresa y contrato de todos los reconocimientos cerrados y no facturados:
	$query="SELECT reconocimientos_medicos.codigo, reconocimientos_medicos.fecha, CONCAT(empleados.codigoInterno,' - ',empleados.nombre,' ',empleados.apellidos) AS empleado,
			CONCAT(EMPID,' - ',EMPNOMBRE) AS cliente, EMPCP, EMPID, contratos.fechaInicio, contratos.codigoInterno, importeRM, importeRMExtra, numRM, empleados.codigo AS codigoEmpleado,
			contabilizacionEconomica, contratos.codigo AS codigoContrato, profesionales_externos.precio AS precioProfesionalExterno, checkRadiologia
			
			FROM reconocimientos_medicos LEFT JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
			LEFT JOIN clientes ON empleados.codigoCliente=clientes.codigo
			LEFT JOIN contratos ON reconocimientos_medicos.codigoContrato=contratos.codigo
			LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
			LEFT JOIN profesionales_externos ON reconocimientos_medicos.codigoProfesionalExterno=profesionales_externos.codigo
			
			WHERE reconocimientos_medicos.eliminado='NO' AND tipoReconocimiento!='Se niega' AND reconocimientos_medicos.codigo NOT IN(
				SELECT codigoReconocimientoMedico 
				FROM reconocimientos_medicos_en_facturas INNER JOIN facturas_reconocimientos_medicos ON reconocimientos_medicos_en_facturas.codigoFacturaReconocimientoMedico=facturas_reconocimientos_medicos.codigo
				WHERE codigoReconocimientoMedico IS NOT NULL AND facturas_reconocimientos_medicos.eliminado='NO'
			)
			AND reconocimientos_medicos.codigo NOT IN(
				SELECT codigoReconocimientoMedico FROM reconocimientos_medicos_ya_facturados WHERE codigoReconocimientoMedico IS NOT NULL
			) AND omitirFacturacion='NO'

			GROUP BY reconocimientos_medicos.codigo
			$having
			ORDER BY reconocimientos_medicos.fecha DESC";


	conexionBD();
	$consulta=consultaBD($query." $limite;");
	$consultaPaginacion=consultaBD($query);	

	$res=inicializaArrayListado($consulta,$consultaPaginacion);

	while($datos=mysql_fetch_assoc($consulta)){
		$referenciaContrato=formateaReferenciaContrato($datos,$datos);
		$reconocimiento=compruebaPrecioReconocimientoIncluidoContrato($datos,$numeroReconocimientosContratoProcesados);

		$fila=array(
			formateaFechaWeb($datos['fecha']),
			$datos['empleado'],
			$datos['cliente'],
			$referenciaContrato,
			$reconocimiento['incluidoContrato'],
			formateaNumeroWeb($reconocimiento['precio'])." €",
			"<div class='centro'><input type='checkbox' name='codigosReconocimientos[]' value='".$datos['codigo']."-".$reconocimiento['incluidoContrato']."-".$reconocimiento['precio']."-".$datos['codigoContrato']."-".formateaFechaWeb($datos['fecha'])."' /></div>",
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;

		registraReconocimientoProcesadoContrato($numeroReconocimientosContratoProcesados,$datos['codigoContrato']);
	}

	cierraBD();

	echo json_encode($res);
}


function compruebaPrecioReconocimientoIncluidoContrato($datos,$numeroReconocimientosContratoProcesados){
	$res=array(
		'incluidoContrato'	=>	'SI',
		'precio'			=>	0
	);

	$codigoContrato=$datos['codigoContrato'];

	$reconocimientosYaFacturados=consultaBD("SELECT IFNULL(COUNT(reconocimientos_medicos_en_facturas.codigo),0) AS total
											 FROM reconocimientos_medicos_en_facturas INNER JOIN facturas_reconocimientos_medicos ON reconocimientos_medicos_en_facturas.codigoFacturaReconocimientoMedico=facturas_reconocimientos_medicos.codigo
											 WHERE codigoReconocimientoMedico IS NOT NULL AND facturas_reconocimientos_medicos.codigoContrato='$codigoContrato';",false,true);


	if(isset($numeroReconocimientosContratoProcesados[$codigoContrato])){
		$reconocimientosYaFacturados['total']+=$numeroReconocimientosContratoProcesados[$codigoContrato];
	}


	if($reconocimientosYaFacturados['total']>=$datos['numRM']){
		$res['incluidoContrato']='NO';
		$res['precio']=$datos['importeRMExtra'];
	}


	//Parte de sobrescritura de precio en base a profesional externo
	if($datos['precioProfesionalExterno']!=NULL){
		$res['precio']=$datos['precioProfesionalExterno'];
	}
	//Fin parte de sobrescritura de precio en base a profesional externo



	//Parte de pruebas extras
	if($datos['checkRadiologia']=='SI'){
		$importeRadiologia=formateaNumeroWeb($datos['contabilizacionEconomica'],true);

		$res['precio']+=$importeRadiologia;
	}

	$otrasPruebasReconocimiento=consultaBD("SELECT IFNULL(SUM(valoracionEconomica),0) AS total FROM reconocimientos_medicos_otras_pruebas WHERE codigoRM='".$datos['codigo']."';",false,true);

	$res['precio']+=$otrasPruebasReconocimiento['total'];
	//Fin parte de pruebas extras

	return $res;
}

function registraReconocimientoProcesadoContrato(&$numeroReconocimientosContratoProcesados,$codigoContrato){
	if(!isset($numeroReconocimientosContratoProcesados[$codigoContrato])){
		$numeroReconocimientosContratoProcesados[$codigoContrato]=0;
	}

	$numeroReconocimientosContratoProcesados[$codigoContrato]++;
}


function creaFacturasPendientes(){
	$res=true;
	$datos=arrayFormulario();
	$codigosReconocimientos=$datos['codigosReconocimientos'];

	$contratos=array();//Formato: [codigoContrato1]=>array(concepto1,concepto2,concepto3,...),[codigoContrato2]=>array(...)

	conexionBD();

	$numeroFactura=consultaNumeroSerieFacturaReconocimientos2(2,false);//IMPORTANTE! El código 2 se corresponde a la serie "RM"

	foreach($codigosReconocimientos as $codigoReconocimiendoConInformacion) {
		
		$arrayCodigos=explode('-',$codigoReconocimiendoConInformacion);

		$codigoReconocimiento=$arrayCodigos[0];
		$incluidoContrato=$arrayCodigos[1];
		$precio=$arrayCodigos[2];
		$codigoContrato=$arrayCodigos[3];
		$fechaRM=formateaFechaBD($arrayCodigos[4]);

		if(!isset($contratos[$codigoContrato])){
			$contratos[$codigoContrato]=array();
		}

		$lineaContrato=array(
			'fechaRRMM'				=>	$fechaRM,
			'codigoReconocimiento'	=>	$codigoReconocimiento,
			'incluidoContrato'		=>	$incluidoContrato,
			'precio'				=>	$precio
		);

		array_push($contratos[$codigoContrato],$lineaContrato);
	}



	foreach($contratos as $codigoContrato => $lineasContrato) {
		$res=$res && creaFacturaPendiente($codigoContrato,$lineasContrato,$numeroFactura);

		$numeroFactura++;
	}


	cierraBD();

	if($res){
		mensajeOk('Se han registrado correctamente '.count($contratos).' facturas de reconocimientos.');
	}
	else{
		mensajeError('se ha producido un error al registrar una o más facturas.');
	}

	return $res;
}

function creaFacturaPendiente($codigoContrato,$lineasContrato,$numeroFactura){
	$fecha=date('Y-m-d');
	$codigoEmisor=1;//Emisor cualquiera
	$codigoSerieFactura=2;//IMPORTANTE! El código 2 corresponde a la serie RM
	$facturaContratos='SI';
	$facturaCobrada='NO';
	$eliminado='NO';

	$res=consultaBD("INSERT INTO facturas_reconocimientos_medicos(codigo,codigoEmisor,fecha,codigoSerieFactura,numero,facturaContratos,codigoContrato,facturaCobrada,eliminado) 
					 VALUES(NULL,'$codigoEmisor','$fecha','$codigoSerieFactura','$numeroFactura','$facturaContratos','$codigoContrato','$facturaCobrada','$eliminado');");

	if($res){
		$codigoFactura=mysql_insert_id();
		$totalFactura=0;

		foreach($lineasContrato as $lineaContrato){
			extract($lineaContrato);

			//TODO: pasado un tiempo prudencial, eliminar el campo codigoEmpleado de la tabla reconocimientos_medicos_en_facturas, que tras la actualización no se usará mas
			$res=$res && consultaBD("INSERT INTO reconocimientos_medicos_en_facturas VALUES(NULL,'$codigoReconocimiento',NULL,'$incluidoContrato','$precio','$codigoFactura');");

			$fechaReconocimientoFactura=$fechaRRMM;
			$totalFactura+=$precio;
		}

		$res=$res && consultaBD("UPDATE facturas_reconocimientos_medicos SET fechaRRMM='$fechaReconocimientoFactura', total='$totalFactura' WHERE codigo='$codigoFactura';");

	}

	return $res;
}


function filtroFacturasPendientes(){

	abreCajaBusqueda();
	abreColumnaCampos();

	campoFecha(0,'Fecha desde',' ');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha(5,'Hasta',' ');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}


function marcarReconocimientosFacturados(){
	$res=true;
	$datos=arrayFormulario();
	$fecha=fechaBD();

	$codigosReconocimientos=$datos['codigosReconocimientos'];
	
	conexionBD();

	foreach($codigosReconocimientos as $codigoReconocimiendoConInformacion){
			
		$arrayCodigos=explode('-',$codigoReconocimiendoConInformacion);

		$codigoReconocimiento=$arrayCodigos[0];

		$res=$res && consultaBD("INSERT INTO reconocimientos_medicos_ya_facturados VALUES(NULL,'$fecha',$codigoReconocimiento);");
	}

	cierraBD();

	if($res){
		mensajeOk("Reconocimientos actualizados correctamente.");
	}
	else{
		mensajeError('no se han podido procesar los reconocimientos. Código de error: '.mysql_errno());
	}

	return $res;
}


function listadoReconocimientosYaFacturados(){
	$numeroReconocimientosContratoProcesados=array();//Tendrá el formato [codigoContrato]=>[numero], y almacenará el número de reconocimientos procesados por contrato, para determinar los que están incluidos y no

	$columnas=array(
		'fecha',
		'empleado',
		'cliente',
		'EMPID',
		'codigo',
		'fecha',
		'codigo'
	);

	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();

	//La siguiente consulta extrae el reconocimiento, empleado, empresa y contrato de todos los reconocimientos cerrados y no facturados:
	$query="SELECT reconocimientos_medicos.codigo, reconocimientos_medicos.fecha, CONCAT(empleados.codigoInterno,' - ',empleados.nombre,' ',empleados.apellidos) AS empleado,
			CONCAT(EMPID,' - ',EMPNOMBRE) AS cliente, EMPCP, EMPID, contratos.fechaInicio, contratos.codigoInterno, importeRM, importeRMExtra, numRM, empleados.codigo AS codigoEmpleado,
			contabilizacionEconomica, contratos.codigo AS codigoContrato, profesionales_externos.precio AS precioProfesionalExterno, checkRadiologia
			
			FROM reconocimientos_medicos LEFT JOIN empleados ON reconocimientos_medicos.codigoEmpleado=empleados.codigo
			LEFT JOIN clientes ON empleados.codigoCliente=clientes.codigo
			LEFT JOIN contratos ON reconocimientos_medicos.codigoContrato=contratos.codigo
			LEFT JOIN ofertas ON contratos.codigoOferta=ofertas.codigo
			LEFT JOIN profesionales_externos ON reconocimientos_medicos.codigoProfesionalExterno=profesionales_externos.codigo
			
			WHERE reconocimientos_medicos.codigo IN(
				SELECT codigoReconocimientoMedico FROM reconocimientos_medicos_ya_facturados WHERE codigoReconocimientoMedico IS NOT NULL
			)

			GROUP BY reconocimientos_medicos.codigo
			$having";


	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);	

	$res=inicializaArrayListado($consulta,$consultaPaginacion);

	while($datos=mysql_fetch_assoc($consulta)){
		$referenciaContrato=formateaReferenciaContrato($datos,$datos);
		$reconocimiento=compruebaPrecioReconocimientoIncluidoContrato($datos,$numeroReconocimientosContratoProcesados);

		$fila=array(
			formateaFechaWeb($datos['fecha']),
			$datos['empleado'],
			$datos['cliente'],
			$referenciaContrato,
			$reconocimiento['incluidoContrato'],
			formateaNumeroWeb($reconocimiento['precio'])." €",
			"<div class='centro'><input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' /></div>",
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;

		registraReconocimientoProcesadoContrato($numeroReconocimientosContratoProcesados,$datos['codigoContrato']);
	}

	cierraBD();

	echo json_encode($res);
}

function operacionesReconocimientosYaFacturados(){
	
	if(isset($_POST['codigo0'])){
		$res=true;
		$datos=arrayFormulario();

		conexionBD();
		
		for($i=0;isset($datos['codigo'.$i]);$i++){

			$res=$res && consultaBD("DELETE FROM reconocimientos_medicos_ya_facturados WHERE codigoReconocimientoMedico='".$datos['codigo'.$i]."';");

		}

		cierraBD();

		if($res){
			mensajeOk('Reconocimientos marcados como facturables');
		}
		else{
			mensajeError("no se han procesado los reconocimientos seleccionados. Código de error: ".mysql_errno());
		}

	}

}

//Fin parte de reconocimientos pendientes de facturar

//Fin parte de facturas