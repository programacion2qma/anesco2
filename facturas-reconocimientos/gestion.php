<?php
  $seccionActiva=30;
  include_once("../cabecera.php");
  gestionFacturaRRMM();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>

<script type="text/javascript" src="../../api/js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-wysihtml5.es-ES.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('#observaciones').wysihtml5({locale: "es-ES"});

	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$('.selectpicker').selectpicker();

	oyentePreciosReconocimientos();

	//Oyentes
	$('#codigoSerieFactura').change(function(){
		oyenteSerieFactura();
	});

	$('#codigoContrato').change(function(){
		oyenteContrato($(this).val());
		oyentePreciosReconocimientos();
	});


	$(document).on('change','#tablaReconocimientos select, #tablaReconocimientos :checkbox',function(){
		//oyentePrecioLinea($(this));
	});

	$('.enContrato input[type=checkbox]').change(function(e){
		oyenteCheckContrato($(this));
	});

	$('.precioRM').change(function(e){
		oyenteTotal();
	});

	bloqueoEdicionConceptos();

	oyenteConceptoFactura();
	$('input[name=facturaContratos]').change(function(){
		oyenteConceptoFactura();
	});
	//Fin oyentes
});


function insertaRM(tabla){
	insertaFila(tabla);
	$('#tablaReconocimientos tr:last .enContrato input[type=checkbox]').change(function(e){
		oyenteCheckContrato($(this));
	});
	$('#tablaReconocimientos tr:last .precioRM').val($('#precioReconocimientoNoIncluido').text());
	$('#tablaReconocimientos tr:last .precioRM').change(function(e){
		oyenteTotal();
	});
	oyenteTotal();
}

function oyenteCheckContrato(campo){
	var id = obtieneFilaCampo(campo)
	var selec = $('#rmSeleccionados').val();
	var totalS = parseInt($('#rmSeleccionados').val()) + parseInt($('#rmFacturas').val());
	var total = $('#rmTotales').val();
	if(campo.attr('checked')=='checked'){
		if(totalS >= total){
			alert('Este contrato ya tiene facturados todos sus RRMM');
			campo.attr('checked',false);
		} else {
			selec++;
			$('#rmSeleccionados').val(selec);
			$('#precio'+id).val($('#precioReconocimientoIncluido').text());
		}
	} else {
		selec--;
		$('#rmSeleccionados').val(selec);
		$('#precio'+id).val($('#precioReconocimientoNoIncluido').text());
	}
	oyenteTotal();
}
//Parte general

function oyenteSerieFactura(){
	var codigoSerieFactura=$('#codigoSerieFactura').val();
	if(codigoSerieFactura!='NULL'){
		var consulta=$.post('../listadoAjax.php?funcion=consultaNumeroSerieFacturaReconocimientos();',{'codigoSerieFactura':codigoSerieFactura});
		consulta.done(function(respuesta){
			$('#numero').val(respuesta);
		});
	}
}

function oyenteContrato(codigoContrato){
	var consulta=$.post('../listadoAjax.php?include=facturas-reconocimientos&funcion=obtieneReconocimientosContratoParaConceptos();',{'codigoContrato':codigoContrato});
	
	consulta.done(function(respuesta){
		$('#tablaReconocimientos select').html(respuesta).selectpicker('refresh');
	});
}


//Parte de cálculos

function oyentePrecioLinea(campo){
	var fila=obtieneFilaCampo(campo);

	if($('#codigoEmpleado'+fila).val()!='NULL'){
		var precio=$('#precioReconocimientoNoIncluido').text();

		if($('#incluidoContrato'+fila).attr('checked')){
			precio=$('#precioReconocimientoIncluido').text();
		}
		

		$('#precio'+fila).val(precio);
	}
	else{
		$('#precio'+fila).val('');
	}

	oyenteTotal();
}

function eliminaFilaFacturacion(id){
	eliminaFila(id);
	oyenteTotal();
}

function oyenteTotal(){
	var total=0;

	$('#tablaReconocimientos').find('.input-mini').each(function(){
		total+=formateaNumeroCalculo($(this).val());
	});

	$('#total').val(formateaNumeroWeb(total));	
}

function oyentePreciosReconocimientos(){
	var codigoContrato=$('#codigoContrato').val();
	if($('#codigo').val()==undefined){
		codigo=0;
	} else {
		codigo=$('#codigo').val()
	}
	if(codigoContrato!='NULL'){
		var consulta=$.post('../listadoAjax.php?include=facturas-reconocimientos&funcion=obtienePreciosReconocimientosContrato();',{'codigoContrato':codigoContrato,'codigo':codigo});
	
		consulta.done(function(respuesta){
			respuesta=respuesta.split('&{}&');
			$('#precioReconocimientoIncluido').text(respuesta[0]);
			$('#precioReconocimientoNoIncluido').text(respuesta[1]);
			$('#rmTotales').val(respuesta[2]);
			$('#rmFacturas').val(respuesta[3]);
			if(codigo==0){
				$('.precioRM').val(respuesta[1]);
			}
			oyenteTotal();
		});
	}
}

function oyenteConceptoFactura(){
	if($('input[name=facturaContratos]:checked').val()=='SI'){
		$('#cajaContratos').removeClass('hide');
		$('#cajaConceptoManual').addClass('hide');
	}
	else{
		$('#cajaContratos').addClass('hide');
		$('#cajaConceptoManual').removeClass('hide');
	}
}

function bloqueoEdicionConceptos(){
	
	if($('#codigo').val()!=undefined){
		
		$("input[name=facturaContratos],#tablaReconocimientos :checkbox").click(function(e){
			e.preventDefault();
		});

		$('#codigoContrato,#tablaReconocimientos select').attr('readonly',true);
	}

}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>