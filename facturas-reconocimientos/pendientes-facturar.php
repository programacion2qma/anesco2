<?php
  $seccionActiva=30;
  include_once('../cabecera.php');
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <form action='index.php' method='post'>
          <div class="span12">
            <div class="widget widget-table action-table">
                <div class="widget-header"> <i class="icon-list"></i>
                  <h3>Facturas de reconocimientos médicos registradas</h3>
                  <div class='pull-right'>
                    <a href='index.php' class='btn btn-default btn-small'><i class='icon-chevron-left'></i> Volver</a>
                    <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
                    <button type='button' id='marcarFacturados' class='btn btn-propio btn-small'><i class='icon-history'></i> Marcar como ya facturados</button>
                    <button type='submit' class='btn btn-success btn-small'><i class='icon-check'></i> Registrar facturas</button>
                  </div>
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                  <?php
                    filtroFacturasPendientes();
                  ?>
                  <table class="table table-striped table-bordered datatable tablaTextoPeque" id="tablaReconocimientosPendientesFacturar">
                    <thead>
                      <tr>
                        <th> Fecha </th>
                        <th> Empleado </th>
                        <th> Empresa </th>
                        <th> Contrato </th>
                        <th> Incluido </th>
                        <th> Importe </th>
                        <th class='centro'><input type='checkbox' id="todo"></th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
                <!-- /widget-content-->
              </div>

          </div>
        </form>
        
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>
<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js?v=123123"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    listadoTabla('#tablaReconocimientosPendientesFacturar','../listadoAjax.php?include=facturas-reconocimientos&funcion=listadoReconocimientosPendientesFacturar();');
    
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaReconocimientosPendientesFacturar');
    $('#botonFiltro').trigger('click');
    realizaBusquedaFiltrada('#cajaFiltros','#tablaReconocimientosPendientesFacturar');

    $('#marcarFacturados').click(function(){
      $('form').attr('action','index.php?marcarFacturados=1');
      $('form').submit();
    });
});
</script>

<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>